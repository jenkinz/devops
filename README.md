# DevOps Project

## Prerequisites: Ubuntu Linux (64-bit)


### To install initially:

### FOR REDHAT 7.2:
```
$ eval "$(curl -fksSL https://bitbucket.org/jenkinz/sandel-chef-repo/raw/master/local-bootstrap.sh)"
```

### FOR UBUNTU 14.04:
```
$ eval "$(curl -fksSL http://dev.sandel.local:8081/sandel-development/devops/raw/master/bin/install.sh)"
```

This will download a small bash script and the dev tools to `/usr/local/devops`,
and symlink all the dev tools into `/usr/local/bin`. It will install apps, SDKs, 
EDKs, etc. to `/opt` and symlink (where appropriate) into `/usr/local/bin`. The 
script will install the tools as root via `sudo` so be prepared to enter the 
root password and make sure your current user is on the sudoers list.

### To update subsequently:

```
$ devops update
```
To update the DevOps tools to the latest version.

The script is idempotent, so it can be run over and over without fear of 
breaking anything. Running it after initial installation will simply update
anything that's out of date, or install any new tools and configurations that 
have been added.

```
$ devops help
```
For help and usage information.

```
$ devops scaffold
```
To create a new project template. After running this command, `new-project` will 
be created in the current directory. The project has the following structure:
```
|--new-project/
|--|--app
|--|--|--hello
|--|--|--|--src
|--|--|--|--|--hello.c
|--|--bld
|--|--|--config
|--|--|--|--bsp
|--|--bsp
|--|--|--default

```
The new project contains a hello world app. To build it and test your devops 
installation, run the following:
```
$ cd bld
$ mk
```
If there are no errors, you can test the app by running (from `bld` directory):
```
$ ../app/hello/bin/img/gcc/unix/hello
Hello, world!
```

## Other Initialization Tasks

### Configuring auto-mount of `graphicdev` file share

We use `//cicero/graphicdev` as an internal file share. Here's how to set it up:

#### Red Hat 7:
1. As root, run the following:
```
# mkdir -p /mnt/graphicdev
```

1. Create the file `.smbcredentials` in your home directory:
```
touch ~/.smbcredentials
```

1. Edit `~/.smbcredentials` and add your Windows/Outlook credentials:
```
username=your_sandel_outlook_username
password=your_sandel_outlook_password
```

1. Change the permissions of `~/.smbcredentials` for security:
```
$ chmod 600 ~/.smbcredentials
```

1. Add the following entry to `/etc/fstab` (edit as root), replacing `<username>` with ***your machine's*** username:
```
//10.0.0.203/graphicdev    /mnt/graphicdev   cifs  _netdev,credentials=/home/<username>/.smbcredentials,dir_mode=0777,file_mode=0777,uid=500,gid=500 0 0
```

1. Run `sudo mount -a` to immediately mount the share to `/mnt/graphicdev`. (This will also happen automatically upon restart.)

#### Ubuntu 14.04:

1. Run the following:
```
sudo apt-get install cifs-utils
```

2. Create the file `.smbcredentials` in your home directory:
```
touch ~/.smbcredentials
```

3. Edit `.smbcredentials` and add your Windows/Outlook credentials (domain is 
always sandel0):
```
username=your_outlook_username
password=your_outlook_password
domain=sandel0
```

4. Change the permissions on `.smbcredentials` to protect it from other users:
```
chmod 600 ~/.smbcredentials
```

5. Create the mount point for "graphicdev:"
```
sudo mkdir /media/graphicdev
```

6. Edit the following file:
```
sudo gedit /etc/fstab
```

7. Add the following to the end of `/etc/fstab`, replacing each occurrence of "username" with your actual computer username (`whoami`):
```
# mount SAMBA share //cicero/graphicdev
//10.0.0.203/graphicdev /media/graphicdev cifs noauto,credentials=/home/username/.smbcredentials,uid=username,gid=username 0 0
```

8. Edit the following file:
```
sudo gedit /etc/rc.local
```

9. Modify `/etc/rc.local` so it looks like this:
```
mount /media/graphicdev
exit 0
```

10. Restart your machine, and ensure that `graphicdev` gets automatically 
mounted after rebooting.

11. If you're ever off the network or VPN, and the drive isn't mounted and you 
come back on the internal network or VPN, you can manually mount `graphicdev`
without rebooting by running:
```
sudo mount -a
```

For RedHat: see https://access.redhat.com/solutions/448263

Hit CTRL-D on graphicdev to bookmark it

NTFS Config: https://access.redhat.com/solutions/23993

Virtualbox Install: http://tecadmin.net/install-oracle-virtualbox-on-centos-redhat-and-fedora/

Wine Install: http://www.tecmint.com/install-wine-in-rhel-centos-and-fedora/