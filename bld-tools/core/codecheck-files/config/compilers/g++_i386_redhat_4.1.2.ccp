
### CodeCheck Copyright(C)1988-2005 by Abraxas Software Inc (R). All rights reserved.

# GCC - G++ CONFIG

-K13

-D__builtin_va_list=char
-D__const=const

# The following defines get rid of fatal errors
-D__FLT_DENORM_MIN__=false
-D__DBL_DENORM_MIN__=false
-D__LDBL_DENORM_MIN__=false

# The following define gets rid of fatal error in basic_ios.h header:
-D__except=codecheck_local_var_1
# The following define gets rid of fatal error in 
-D__cs=codecheck_local_var_2

-D_LIBC_LIMITS_H_

# wow this is tacky -- address later.
#-Uisgreater
#-Uisgreaterequal
-Disgreater=max
-Disgreaterequal=max
-Disless=max
-Dislessequal=max
-Dislessgreater=max
-Disunordered=max


#DEFINE SECTION GCC

-D__STDC__=1
-D__LINUX__


-D_GNU_SOURCE


#INCLUDE SECTION GCC

-I/usr/lib/gcc/i386-redhat-linux/4.1.2/../../../../include/c++/4.1.2
-I/usr/lib/gcc/i386-redhat-linux/4.1.2/../../../../include/c++/4.1.2/i386-redhat-linux
-I/usr/lib/gcc/i386-redhat-linux/4.1.2/../../../../include/c++/4.1.2/backward
-I/usr/local/include
-I/usr/lib/gcc/i386-redhat-linux/4.1.2/include
-I/usr/include

### CodeCheck Copyright(C)1988-2005 by Abraxas Software Inc (R). All rights reserved.

