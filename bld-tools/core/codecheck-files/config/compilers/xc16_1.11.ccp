# XC16 v1.11 COMPILER CODECHECK CONFIG
#
# This configuration assumes that the xc16 compiler is intalled in the
# standard location at /opt/microchip/.
#
# Created by Brian Jenkins on 4/11/2013

# Only display rule violations (suppress Codecheck-generated warnings)
# Normally this should be commented, especially when trying to debug this
# configuration as commenting will suppress syntax errors Codecheck finds,
# which could be causing the larger Codecheck fatal errors.
#-J

# Generate check.lst listing file (for debugging)
# Uncomment the following options if Codecheck is displaying a "Fatal Error" and
# failing out. Run Codecheck again and check.lst will be generated. Inspect this
# file and search for the first warning/syntax error which is usually the
# starting point to solve the issue.
#-H
#-M

# Parse assuming ANSI C with extensions (C99)
-K3

# DEFINES
-D__builtin_va_list=char
-D__const=const
-D__STDC__=1
-D__LINUX
-D__inline__=
-Dinline=
-D__uint64_t=int
-Dasm=__asm__
-D__PTRDIFF_TYPE__=int
-D__SIZE_TYPE__=unsigned
-D__WCHAR_TYPE__=char
-D__WINT_TYPE__=int

# COMPILER INCLUDE PATHS
-I/opt/microchip/xc16/v1.11/include/lega-c
-I/opt/microchip/xc16/v1.11/include
-I/opt/microchip/xc16/v1.11/support/generic/h
-I/opt/microchip/xc16/v1.11/support/dsPIC30F/h
-I/opt/microchip/xc16/v1.11/support/peripheral_30F_24H_33F