
### CodeCheck Copyright(C)1988-2005 by Abraxas Software Inc (R). All rights reserved.

# GCC CONFIG

-K3

-D__builtin_va_list=char
-D__const=const

-D__inline__=inline

-DNDEBUG

# Resolves CHAR_BIT==8 issue - compiler internal resolved.
-D__CHAR_BIT__=8
# signal.h fix
-D__interrupt=intrpt 
# mathcalls.h fix
-Df= 
-D_r= 
# bits/socket.h fix
-D__uint64_t=long 

# offset takes a struct name, and variable - max isn't going to work.
# we could look at creating our own include with subtitutions as required
# and have these include files optional or look at ignore.
#-D__builtin_offsetof=max didn't work...

#INCLUDE SECTION GCC

-I/usr/local/include
-I/usr/lib/gcc/i386-redhat-linux/4.1.2/include
-I/usr/include

### CodeCheck Copyright(C)1988-2005 by Abraxas Software Inc (R). All rights reserved.
