
### CodeCheck Copyright(C)1988-2005 by Abraxas Software Inc (R). All rights reserved.

# GCC - G++ CONFIG

-K13

-D__builtin_va_list=char
-D__const=const


#DEFINE SECTION GCC

-D__STDC__=1
-D__LINUX__


-D_GNU_SOURCE

#-D__SIZE_TYPE__=unsigned int
#-D__PTRDIFF_TYPE__=int


#INCLUDE SECTION GCC

-I/usr/include/c++/4.8
-I/usr/include/x86_64-linux-gnu/c++/4.8
-I/usr/include/c++/4.8/backward
-I/usr/lib/gcc/x86_64-linux-gnu/4.8/include
-I/usr/local/include
-I/usr/lib/gcc/x86_64-linux-gnu/4.8/include-fixed
-I/usr/include/x86_64-linux-gnu
-I/usr/include

### CodeCheck Copyright(C)1988-2005 by Abraxas Software Inc (R). All rights reserved.

