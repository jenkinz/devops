# Specify the toolchain for default:
TOOLCHAIN ?= gcc

%include $(COREBASEDIR)/bld/toolchain/make-inc-priv-$(TOOLCHAIN).mak

DEBUG = -g
ARFLAGS = -r

DEFINES += -DXDEBUG -DXPRINTF

%ifdef NDEBUG
DEFINES += -DNDEBUG
%endif

CFLAGS += $(INCLUDES) $(DEFINES) $(DEBUG) -pedantic-errors -ansi -Wall
CPPFLAGS += $(CFLAGS)
ASFLAGS += $(DEBUG)

LDFLAGS = $(DEBUG)

BSP_COMPILER_DEFINE = 

