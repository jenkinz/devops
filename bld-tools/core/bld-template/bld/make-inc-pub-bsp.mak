# File: make-inc-pub-bsp.mak
#
# Description: Board Support Package (BSP) selection.
#              The selected BSP, if found, will be stored in the macro 
#              SELECTED_BSP.
#

BSPS_DISCOVERY_PATH = ../bsp

# Find all BSPs within the bsp/ directory
BSPS_SPEC = $(BSPS_DISCOVERY_PATH)/*
BSPS = $(BSPS_SPEC,*D,N"*.svn") # select BSPs but filter out .svn

# Now using BSPs - use Pattern matching - anything close will do.
# mk BSP=phytec will select THE_BSP_SELECTED = ../bsp/mpc565-phytec-evalbd
# mk BSP=565 will accomplish same thing - if 2 565 BSP's supported - be more
# complete in the BSP cmd line or env var selection. 
# -alternatively-
# export BSP=565|dspic and mk will select the BSP and indicate that an env var
# usage was detected.
# The SELECTED_BSP = ../bsp/dspicdem-evalbd is the default and can be set
# also as an env var. 
# SELECTED_BSP overrides even BSP and is very explicit on the path and bsp to
# to be used and is more of a debugging usage. 
SELECTED_BSP ?= ../bsp/default
%getenv SELECTED_BSP SELECTED_BSP_ENV_VAR
%if !null(SELECTED_BSP_ENV_VAR)
%echo -n Using specified environment variable:
%echo  SELECTED_BSP = $(SELECTED_BSP_ENV_VAR)
%else
%if !%null(BSP)
%getenv BSP BSP_ENV_VAR
%if !%null(BSP_ENV_VAR)
%echo Using specified environment variable BSP=$(BSP_ENV_VAR)
%endif
SELECTED_BSP = $(BSPS,M$(BSP))
%if %null(SELECTED_BSP)
%error No BSP found under $(BSPS_DISCOVERY_PATH) for specified BSP=$(BSP)
%endif
%endif
%endif

