# File: make-inc-pub-spec-descr.mak
# Description: Provides definitions of apps and libs SRCS and OBJS sets.
#              Dynamically discovers and generates targets for each `app' and 
#              `lib' existing in the directory tree for the project. 
#              Dynamic discovery is key to easing CI.

SRCS = 
OBJS = 

# Startup objects are specified first to the linker and must be force-linked in
STARTUP_OBJS ?= 

FILEDUPHDRS = 

# Will hold relative paths to each lib file, each lib separated by space
ALLLIBS = 

# Set the Deploy location for images and libs
DEPLOY_TOP_DIR = ../deploy
DEPLOY_DEBUG_DIR = $(DEPLOY_TOP_DIR)/debug
DEPLOY_RELEASE_DIR = $(DEPLOY_TOP_DIR)/release
DEPLOY_DIR = $(DEPLOY_DEBUG_DIR)
%if %null(DEBUG)
DEPLOY_DIR = $(DEPLOY_RELEASE_DIR)
%endif

LIBS_DISCOVERY_PATH = ../lib
APPS_DISCOVERY_PATH = ../app
#ROLE_DISCOVERY_PATH = ../role

# The generic source spec - i.e. what types of source files to look for:
SRC_SUFFIX_LIST = c cpp cc cxx asm s S
HDR_SUFFIX_LIST = h hpp hxx inc

GEN_SRCSPEC = $(SRC_SUFFIX_LIST,<*.)
GEN_HDRSPEC = $(HDR_SUFFIX_LIST,<*.)

# organize build artifacts in bin/ by obj/<compiler>/<os>, lib/<compiler>/<os>, 
# and img/<compiler>/<os> (for apps)
BINSEPDIR = $(CC,F)/$(OS,LC)

################################################################################
# Board Support Package (BSP) Targets
################################################################################
BSP_STARTUP_PATH = $(SELECTED_BSP,>/startup)
BSP_LIB_PATH = $(SELECTED_BSP,>/lib)

%if %exists($(BSP_STARTUP_PATH))
SRCSPEC_BSP_STARTUP = $(GEN_SRCSPEC,<$(BSP_STARTUP_PATH)/src/)
BSP_STARTUP_WILDCARD_SRCS := $(SRCSPEC_BSP_STARTUP,*F)
OBJS_BSP_STARTUP = $(BSP_STARTUP_WILDCARD_SRCS,F,B,>.$O)

#STARTUP_OBJS += $(OBJS_BSP_STARTUP,<$(BSP_STARTUP_PATH)/bin/obj/$(BINSEPDIR)/)
STARTUP_OBJS += $(OBJS_BSP_STARTUP,<$(DEPLOY_DIR)/obj/$(BINSEPDIR)/)

SRCS += $(BSP_STARTUP_WILDCARD_SRCS)
OBJS += $(OBJS_BSP_STARTUP)

HDRSPEC_BSP_STARTUP = $(GEN_HDRSPEC,<$(BSP_STARTUP_PATH)/inc/)
BSP_STARTUP_WILDCARD_HDRS := $(HDRSPEC_BSP_STARTUP,*F)
%endif

%if %exists($(BSP_LIB_PATH))
SRCSPEC_BSP_LIB = $(GEN_SRCSPEC,<$(BSP_LIB_PATH)/src/)
BSP_LIB_WILDCARD_SRCS := $(SRCSPEC_BSP_LIB,*F)
OBJS_BSP_LIB=$(BSP_LIB_WILDCARD_SRCS,F,B,>.$O)

SRCS += $(BSP_LIB_WILDCARD_SRCS)
OBJS += $(OBJS_BSP_LIB)

HDRSPEC_BSP_LIB = $(GEN_HDRSPEC,<$(BSP_LIB_PATH)/inc/)
BSP_LIB_WILDCARD_HDRS := $(HDRSPEC_BSP_LIB,*F)

# specify targets for BSP libs that depend on object macros generated above
$(SELECTED_BSP,F,<lib,>.$L) : $(OBJS_BSP_LIB)
LIBTARGETS += $(SELECTED_BSP,F,<lib,>.$L)
%endif

%if !%null(BSP_STARTUP_WILDCARD_HDRS)
FILEDUPHDRS += $(BSP_STARTUP_WILDCARD_HDRS)
%endif

%if !%null(BSP_LIB_WILDCARD_HDRS)
FILEDUPHDRS += $(BSP_LIB_WILDCARD_HDRS)
%endif

################################################################################
# Role lib Targets
################################################################################

#ROLE ?= slave
#%if %null(ROLE)
#%error ROLE can not be selected as empty as in mk ROLE=
#%endif

#%getenv ROLE ROLE_ENV_VAR
#%if !null(ROLE_ENV_VAR)
#%echo -n WARNING: Using specified environment variable:
#%echo  ROLE = $(ROLE_ENV_VAR)
#%endif

#ROLE_SPEC = $(ROLE_DISCOVERY_PATH)/*
#ROLES = $(ROLE_SPEC,*D,N"*.svn")

#SELECTED_ROLE = $(ROLES,M$(ROLE))
#%if %null(SELECTED_ROLE)
#%error ROLE must be slave -or- master or bad spec tree: $(ROLE_DISCOVERY_PATH)/* 
#%endif

##############################################################################

#SRCSPEC_ROLE = $(GEN_SRCSPEC,<$(SELECTED_ROLE)/src/)
#ROLE_WILDCARD_SRCS := $(SRCSPEC_ROLE,*F)
#OBJS_ROLE = $(ROLE_WILDCARD_SRCS,F,B,>.$O)

#SRCS += $(ROLE_WILDCARD_SRCS)
#OBJS += $(OBJS_ROLE)

#HDRSPEC_ROLE = $(GEN_HDRSPEC,<$(SELECTED_ROLE)/inc/)
#ROLE_WILDCARD_HDRS := $(HDRSPEC_ROLE,*F)

#FILEDUPHDRS += $(ROLE_WILDCARD_HDRS)

#$(SELECTED_ROLE,F,<lib,>.$L) : $(OBJS_ROLE)
#LIBTARGETS += $(SELECTED_ROLE,F,<lib,>.$L)

################################################################################
# lib (a.k.a subsystem) Targets
################################################################################
SUBSYSTEMS_SPEC = $(LIBS_DISCOVERY_PATH)/*
SUBSYSTEMS = $(SUBSYSTEMS_SPEC,*D,N"*.svn") # select libs but filter out .svn

# generate src and obj macros for each library
%foreach libdir in $(SUBSYSTEMS)
SRCSPEC_$(libdir,F) = $(GEN_SRCSPEC,<$(libdir)/src/) \
                      $(GEN_SRCSPEC,<$(libdir)/src/$(OS,LC)/)
LIB_WILDCARD_SRCS_$(libdir,F) := $(SRCSPEC_$(libdir,F),*F)
OBJS_$(libdir,F) = $(LIB_WILDCARD_SRCS_$(libdir,F),F,B,>.$O)

SRCS += $(LIB_WILDCARD_SRCS_$(libdir,F))
OBJS += $(OBJS_$(libdir,F))
%endfor

%foreach libdir in $(SUBSYSTEMS)
HDRSPEC_$(libdir,F) = $(GEN_HDRSPEC,<$(libdir)/inc/) \
                      $(GEN_HDRSPEC,<$(libdir)/inc/$(OS,LC)/)
LIB_WILDCARD_HDRS_$(libdir,F) := $(HDRSPEC_$(libdir,F),*F)

FILEDUPHDRS += $(LIB_WILDCARD_HDRS_$(libdir,F))
%endfor

# specify targets for libs that depend on object macros generated above
LIBPREPOST = F,<lib,>.$L
%foreach libdir in $(SUBSYSTEMS)
$(libdir,$(LIBPREPOST)) : $(OBJS_$(libdir,F))
%endfor

# actual build targets (for each lib<x>.$L) w/ no path
LIBTARGETS += $(SUBSYSTEMS,$(LIBPREPOST))

################################################################################
# Image (Application) Targets
################################################################################
IMAGES_SPEC = $(APPS_DISCOVERY_PATH)/*
IMAGES = $(IMAGES_SPEC,*D,N"*.svn") # select apps but filter out .svn

# generate src and obj macros for each app
%foreach appdir in $(IMAGES)
SRCSPEC_$(appdir,F) = $(GEN_SRCSPEC,<$(appdir)/src/) \
                      $(GEN_SRCSPEC,<$(appdir)/src/$(OS,LC)/)
APP_WILDCARD_SRCS_$(appdir,F) := $(SRCSPEC_$(appdir,F),*F)
OBJS_$(appdir,F) = $(APP_WILDCARD_SRCS_$(appdir,F),F,B,>.$O)

SRCS += $(APP_WILDCARD_SRCS_$(appdir,F))
OBJS += $(OBJS_$(appdir,F))
%endfor

%foreach appdir in $(IMAGES)
HDRSPEC_$(appdir,F) = $(GEN_HDRSPEC,<$(appdir)/inc/) \
                      $(GEN_HDRSPEC,<$(appdir)/inc/$(OS,LC)/)
APP_WILDCARD_HDRS_$(appdir,F) := $(HDRSPEC_$(appdir,F),*F)

FILEDUPHDRS += $(APP_WILDCARD_HDRS_$(appdir,F))
%endfor

# specify targets for apps that depend on object macros generated above
%foreach appdir in $(IMAGES)
$(appdir,F,>$E) : $(OBJS_$(appdir,F))
%endfor

# actual build targets (for each app) w/ no path
IMAGETARGETS = $(IMAGES,F,>$E)

################################################################################
# Path/Search Specs
#   i.e. where to find various types of files by extension/name
################################################################################
# Note: Do not put spaces after += when assigning the .PATH macros.
#
# Note: XSUBSYSTEM_INCLUDES must use := to cache result or includes apps on
# expansion - see opus make doc on = vs := 
################################################################################

%if %exists($(BSP_STARTUP_PATH))
.PATH.c += $(BSP_STARTUP_PATH,>/src,W;)
%endif
%if %exists($(BSP_LIB_PATH))
.PATH.c +=;$(BSP_LIB_PATH,>/src,W;)
%endif
#.PATH.c +=;$(SELECTED_ROLE,>/src,W;)
.PATH.c +=;$(SUBSYSTEMS,>/src,W;);$(SUBSYSTEMS,>/src/$(OS,LC),W;)
.PATH.c +=;$(IMAGES,>/src,W;);$(IMAGES,>/src/$(OS,LC),W;)
.PATH.cpp = $(.PATH.c)
.PATH.cxx = $(.PATH.c)
.PATH.cc = $(.PATH.c)
.PATH.asm = $(.PATH.c)
.PATH.s = $(.PATH.c)
.PATH.S = $(.PATH.c)

XSUBSYSTEM_INCLUDES := $(.PATH.c,/src=/inc,;= ,<-I)

.PATH.h := $(.PATH.c,/src=/inc)
.PATH.hpp := $(.PATH.h)

.PATH.$O = $(DEPLOY_DIR)/obj/$(BINSEPDIR)

.PATH.$L = $(DEPLOY_DIR)/lib/$(BINSEPDIR)

# The following LIBLOCATIONS and OBJLOCATIONS macros are used in the `clear', 
# .BEFORE and .AFTER rules to remove and create these locations
LIBLOCATIONS = 
OBJLOCATIONS = 

# dynamically specify location where each lib file will be placed

#.SEARCH $(SELECTED_ROLE)/bin/lib/$(BINSEPDIR) $(SELECTED_ROLE,F,<lib,>.$L)
#LIBLOCATIONS += $(SELECTED_ROLE)/bin/lib/$(BINSEPDIR)
#OBJLOCATIONS += $(SELECTED_ROLE)/bin/obj/$(BINSEPDIR)

#%if %exists($(BSP_LIB_PATH))
#.SEARCH $(BSP_LIB_PATH)/bin/lib/$(BINSEPDIR) $(SELECTED_BSP,F,<lib,>.$L)
#LIBLOCATIONS += $(BSP_LIB_PATH)/bin/lib/$(BINSEPDIR)
#OBJLOCATIONS += $(BSP_LIB_PATH)/bin/obj/$(BINSEPDIR)
#%if %exists($(BSP_STARTUP_PATH))
#OBJLOCATIONS += $(BSP_STARTUP_PATH)/bin/obj/$(BINSEPDIR)
#%endif
#%endif

%foreach libdir in $(SUBSYSTEMS)
#.SEARCH $(libdir)/bin/lib/$(BINSEPDIR) $(libdir,F,<lib,>.$L)
#.SEARCH $(DEPLOY_DIR)/lib/$(BINSEPDIR) $(libdir,F,<lib,>.$L)
ALLLIBS += $(DEPLOY_DIR)/lib/$(BINSEPDIR)/$(libdir,F,<lib,>.$L)
#LIBLOCATIONS += $(libdir)/bin/lib/$(BINSEPDIR)
#OBJLOCATIONS += $(libdir)/bin/obj/$(BINSEPDIR)
%endfor

LIBLOCATIONS += $(DEPLOY_DIR)/lib/$(BINSEPDIR)
OBJLOCATIONS += $(DEPLOY_DIR)/obj/$(BINSEPDIR)

# Append Role lib:
#ALLLIBS += $(SELECTED_ROLE)/bin/lib/$(BINSEPDIR)/$(SELECTED_ROLE,F,<lib,>.$L)

# Append BSP lib last:
#%if %exists($(BSP_LIB_PATH))
#ALLLIBS += $(BSP_LIB_PATH)/bin/lib/$(BINSEPDIR)/$(SELECTED_BSP,F,<lib,>.$L)
#%endif

#BINIMGDIR = $(IMAGES,>/bin/img/$(BINSEPDIR),W;)
IMGLOCATIONS = 

# dynamically specify location where each app img file will be placed
%foreach appdir in $(IMAGES)
%if !%null(E)
#.SEARCH $(appdir)/bin/img/$(BINSEPDIR) $(appdir,F,>.$E)
.SEARCH $(DEPLOY_DIR)/bin/$(BINSEPDIR) $(appdir,F,>.$E)
%else
#.SEARCH $(appdir)/bin/img/$(BINSEPDIR) $(appdir,F)
.SEARCH $(DEPLOY_DIR)/bin/$(BINSEPDIR) $(appdir,F)
%endif
#IMGLOCATIONS += $(appdir)/bin/img/$(BINSEPDIR)
#OBJLOCATIONS += $(appdir)/bin/obj/$(BINSEPDIR)
%endfor

IMGLOCATIONS += $(DEPLOY_DIR)/bin/$(BINSEPDIR)

# Do not use := on these macros - must be expanded at time of use!
SUBSYSTEM_INCLUDES = $(XSUBSYSTEM_INCLUDES)

# A specific image target objs - not all the images objs but just a specific
# image - this is a helper macro for use in the linking phase.
# Note: Do not use a object file collection - very specific to link phase helper
IMAGETARGET_OBJS = $(OBJS_$(@,R,F))
INCLUDEDEPSPATHS = $(.PATH.h,;= )

# Append to INCLUDES macro and therefore CFLAGS (compiler flags).
# INCLUDES and CFLAGS are normally initialized in `make-inc-pub-compiler.mak'

INCLUDES += $(SUBSYSTEM_INCLUDES)

