# File: make-inc-pub-pre-post-conds.mak
# Purpose: validates pre- and post- condition requirements for this build.

SHOW_FLAGS ?= 1

.BEFORE : find_duplicate_files $(OS_CC_DEPS_MAKEFILE)
#	start elapsed timer:
	$(VV)$(XELAPSEDTIME) /start
#	create deploy locations:
	%if !%null(OBJLOCATIONS)
	%foreach objlocation in $(OBJLOCATIONS)
	%if !%dir($(objlocation))
	$(VV)$(MKDIR) $(objlocation)
	%endif
	%endfor
	%endif
	%if !%null(LIBLOCATIONS)
	%foreach liblocation in $(LIBLOCATIONS)
	%if !%dir($(liblocation))
	$(VV)$(MKDIR) $(liblocation)
	%endif
	%endfor
	%endif
	%if !%null(IMGLOCATIONS)
	%foreach imglocation in $(IMGLOCATIONS)
	%if !%dir(imglocation)
	$(VV)$(MKDIR) $(imglocation)
	%endif
	%endfor
	%endif
#	create response files for build shell lines:
	%echo $(CFLAGS) > cflags.rsp
	%echo $(CPPFLAGS) > cppflags.rsp
	%echo $(ASFLAGS) > asflags.rsp
	%echo $(ARFLAGS) > arflags.rsp
	%echo $(LDFLAGS) > ldflags.rsp
	%if $(SHOW_FLAGS)
	$(VV) %echo CFLAGS = $(CFLAGS)
	$(VV) %echo
	$(VV) %echo CPPFLAGS = $(CPPFLAGS)
	$(VV) %echo
	$(VV) %echo ASFLAGS = $(ASFLAGS)
	$(VV) %echo
	$(VV) %echo ARFLAGS = $(ARFLAGS)
	$(VV) %echo
	$(VV) %echo LDFLAGS = $(LDFLAGS)
	%endif
	%echo


.AFTER :
#	stop elapsed timer and remove the timer temporary file:
	$(VV)$(XELAPSEDTIME) /stop
	$(VV)--rm starttime.out

