# File: make-inc-pub-compiler.mak
# Purpose: Abstracts compiler details.

# Notes: Ideally here - can support a variety of compilers and common 
# switches - user provides updates as required.

# This defines the SELECTED_BSP macro which determines which BSP to use
%include make-inc-pub-bsp.mak

# BSP support: if BSP not selected exit with error:
%if %null(SELECTED_BSP)
%error No BSP found under ../bsp for specified BSP=$(SELECTED_BSP)
%endif

# Use BSP's specified config:
%include config/bsp/make-inc-pub-$(SELECTED_BSP,F).mak

# Auto-generated header dependency makefile
OS_CC_DEPS_MAKEFILE = mkmf-inc-$(OS,LC)-$(CC,F)-deps.mak

show_bsp .PHONY :
	%echo Using BSP $(SELECTED_BSP,F) with toolchain $(TOOLCHAIN)
	%echo CC = $(CC)
	%echo CPP = $(CPP)
	%echo AS = $(AS)
	%echo LD = $(LD)
	%echo AR = $(AR)

