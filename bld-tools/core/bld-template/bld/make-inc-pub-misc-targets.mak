# File: make-inc-pub-misc-targets.mak
# Purpose: miscellaneous and utility targets for this build.

# convenience target to make all libs (mk lib or mk libs)
lib libs .PHONY : $(LIBTARGETS)
	%echo '$@ up to date'

# convenience target to make all apps (mk app or mk apps)
app apps .PHONY : $(IMAGETARGETS)
	%echo '$@ up to date'

clean_deps .PHONY : 
	--rm mkmf-inc*.mak

clean .PHONY : 
#	--rm -fr $(OBJLOCATIONS)
#	--rm -fr $(LIBLOCATIONS)
#	--rm -fr $(IMGLOCATIONS)
	--rm -rf $(DEPLOY_TOP_DIR)
	--rm -rf *.rsp
	%do clean_deps
#	--rm $(OS_CC_DEPS_MAKEFILE)

# Duplicate filename check
FINDDUPSSRCSARGS = -suffixes $(SRC_SUFFIX_LIST,<.,W) -caseinsensitive true
FINDDUPSHDRSARGS = -suffixes $(HDR_SUFFIX_LIST,<.,W) -caseinsensitive true
#FINDDUPSFILESET = $(SRCS,W\n) $(HDRS,W\n) $(EXTHDRS,W\n)
FINDDUPSSRCSFILESET = $(SRCS,W\n)
FINDDUPSHDRSFILESET = $(FILEDUPHDRS,W\n)

find_duplicate_files .PHONY :
	%echo $(FINDDUPSSRCSFILESET) > $(MAKEPID).tmp
	$(VV)--$(XLISTFINDDUP) $(FINDDUPSSRCSARGS) -inputfile $(MAKEPID).tmp
	$(VV)%set ERRSTAT=$(status)
	$(VV)--rm $(MAKEPID).tmp
	%if $(ERRSTAT)
	%echo
	%error Error: duplicate source file names not allowed - see above
	%endif
	%echo $(FINDDUPSHDRSFILESET) > $(MAKEPID).tmp
	$(VV)--$(XLISTFINDDUP) $(FINDDUPSHDRSARGS) -inputfile $(MAKEPID).tmp
	$(VV)%set ERRSTAT=$(status)
	$(VV)--rm $(MAKEPID).tmp
	%if $(ERRSTAT)
	%echo
	%error Error: duplicate header file names not allowed - see above
	%endif

# Version control shortcuts
git_status .PHONY :
	git status

git_log .PHONY :
	git log -5

git_add .PHONY :
	git add ..

git_commit .PHONY :
	%if %null(GIT_EDITOR)
	%setenv GIT_EDITOR = gvim -f
	%endif
	git commit

#git_svn_log .PHONY :
#	git svn log -l 5

#git_svn_update .PHONY :
#	git svn rebase

#git_svn_push .PHONY :
#	git svn dcommit

