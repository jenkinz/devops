# File: make-inc-priv-compliance.mak
# Purpose: 
#   Runs the tools for metrics and compliance - codecheck, lint, ... 
##   - Builds Codecheck object files (.cco) for .cc rulesets
##   - Runs Metrics and Coding Standards Compliance over given source modules
##      The current metrics available are:
##		    SLOC (Source Lines of Code)
##          McCabe
##          Halstead
##          Object-Oriented
##      The current coding standards available are:
##          JSF (Joint Strike Fighter RevC, (c)2005 Lockheed Martin Corporation)
##          MISRA 2004 ((c)2004 Motor Industry Software Reliability Association)
##
## Dependencies:
##      This makefile is intended to be included by a "master" makefile that 
##      handles the actual build. This makefile relies on the following macros
##      to be defined by the includer or the caller on the command line:
##
##      SRCS - the list of source modules to run metrics over
##      HDRS - the list of header file dependencies
##      EXTHDRS
##      INCLUDES - the list of include paths the source/header files depend on
##      DEFINES - any extra #defines needed by the compiler
##      
## System Requirements:
##      This makefile runs on Linux, UNIX and Windows platforms with Opus Make. 
##      It expects the following tool macros to be available, otherwise it will
##      attempt to use them from your PATH:
##
##      CODECHECK (check|chknt) - Abraxas CodeCheck
##      GLINT (flint|lint-nt) - Gimpel Lint
##      LINT2CS (lint2cs) - Lint to Coding Standard
##      GREP (grep)
##      SED (sed)
##      AWK (awk)
##      RM (rm)
##      CAT (cat)
##      
## Usage:
##      mk metrics_codingstd // run all metrics and coding standards available
##      mk metrics      // run all metrics (sloc, mccabe, halstead, oometric)
##      mk codingstd    // run all coding standards
##		mk sloc         // run SLOC metrics
##		mk mccabe       // run McCabe metrics
##		mk halstead     // run Halstead metrics
##      mk oometric     // run Object-Oriented metrics
##		mk jsf          // run JSF coding standard compliance
##      mk misra04      // run MISRA 2004 coding standard compliance
##
##		mk clean        // remove compiled CodeCheck object files
##
##      All of the targets above (with the exception of clean) can take the 
##      following optional arguments:
##      
##      CCK_BRIEF=1     - Do not display banner information in output
##      CCK_HELP=1      - Display help/info for each metric and exit
##      CCK_SILENT=1    - Do not output anything unless a violation occurs
##      CCK_FORCE=1     - Do not exit, even if a violation threshold is exceeded
##
##      CCK_SLOC_TYPE=PLOC|ELOC|LLOC (sloc only)
##                      - Indicate to the sloc rule which type of SLOC the max 
##                        threshold values below apply to (the default is PLOC)
##      CCK_MAX_SLOC_FCN=<integer> (sloc only)
##                      - Indicate to sloc the max allowable value for a 
##                        function (the default is 200)
##      CCK_MAX_SLOC_MOD=<integer> (sloc only)
##                      - Indicate to sloc the max allowable value for a module
##                        (the default is 10000)
##
##      CCK_MAX_MCCABE=<integer> (mccabe only)
##                      - Indicate to the mccabe rule the maximum allowable 
##                        traditional McCabe complexity for a function (the
##                        default is 20)
##      
##      CCK_MAX_HALSTEAD_VOL=<float> (halstead only)
##                      - Indicate to the halstead rule the maximum allowable
##                        Halstead volume for a function (the default is 2000.0)
##
##      CCK_CODING_STD_EXIT_VIOLATION (coding standard rules only)
##                      - Indicate to coding standard rules whether to exit or 
##                        keep going when violations in a module are found (the
##                        default is to keep going)
##
##      CODECHECK_PATH - the location of the CodeCheck rule files for the 
##                       CCRULES env var that CodeCheck requires
##
## Author: Brian Jenkins <brian@jenkinz.com>
## Date: September 14, 2015
################################################################################

# Internal use only - PATH for other opus make constructs
CCK_RULE_PATH ?= $(CODECHECK_PATH)/rules \
                 $(CODECHECK_PATH)/rules/include

.PATH.cc = $(CCK_RULE_PATH,$(PS)=,W;)

%setenv CCRULES=$(CCK_RULE_PATH,W$(PS))

# CodeCheck Configuration File
CCK_COMPILER_PATH ?= $(CODECHECK_PATH)/config/compilers

%if "$(OS,LC)" == "unix"
# TODO change path to be autodicsovered: [uname -s]/[uname -p]/gcc.ccp etc...
CCK_CONFIG_C_FILE ?= $(CCK_COMPILER_PATH)/linux/x86_64/$(CCK_CONFIG_C)
CCK_CONFIG_CPP_FILE ?= $(CCK_COMPILER_PATH)/linux/x86_64/$(CCK_CONFIG_CPP)
%elif "$(OS,LC)" == "nt"
%error Compliance: Windows not supported at this time
%else
%error Compliance: Platform not supported
%endif

#%if "$(CC)" == "dcc" || "$(CC)" == "dplus"
#CCK_CONFIG_C_FILE ?= $(CCK_COMPILER_PATH)/diab_5.2.0.0.ccp
#CCK_CONFIG_CPP_FILE ?= $(CCK_COMPILER_PATH)/diab_5.2.0.0.ccp
  # TODO - make xc16 config file work, having issues so postponing for now
  #		=> bjenkins 12/22/2014
  #%elseif "$(CC)" == "xc16-gcc"
  #CCK_CONFIG_C_FILE ?= $(CODECHECK_PATH)/config/compilers/xc16_1.21.ccp
  #CCK_CONFIG_CPP_FILE ?= $(CCK_CONFIG_C_FILE)
#%else
#CCK_CONFIG_C_FILE ?= $(CCK_COMPILER_PATH)/gcc_i386_redhat_4.1.2.ccp
#CCK_CONFIG_CPP_FILE ?= $(CCK_COMPILER_PATH)/g++_i386_redhat_4.1.2.ccp
#%endif
  # TODO - Depending on the compiler used, automatically assign config file:
  #CCK_CONFIG_FILE ?= 
  #$(CODECHECK_PATH)/config/compilers/gcc_i386_redhat_4.1.2.ccp
  #CCK_CONFIG_FILE ?= $(CODECHECK_PATH)/config/compilers/xc16_1.11.ccp
  #CCK_CONFIG_FILE ?= 
  #$(CODECHECK_PATH)/config/compilers/g++_i386_redhat_4.1.2.ccp

## TODO - fix on reg expression later on for each source file.
CCK_CONFIG_FILE ?= $(CCK_CONFIG_C_FILE)
CCK_DEBUG ?= 0

METRICS_TGTS = sloc prj_sloc mccabe halstead oometric
CODING_STD_TGTS = jsf tccs misra04 ga_legacy

# Targets
metrics_codingstd .PHONY : $(METRICS_TGTS) $(CODING_STD_TGTS)

metrics .PHONY : $(METRICS_TGTS)

codingstd .PHONY : $(CODING_STD_TGTS)

%foreach metric in $(METRICS_TGTS)
$(metric) .PHONY :
    %do codecheck CCK_RULE=$(metric).cco do_rule="$(metric)"
%endfor

%foreach codestd in $(CODING_STD_TGTS)
$(codestd) .PHONY :
    %do codecheck CCK_RULE=$(codestd).cco do_rule="$(codestd)"
%endfor

# Default Violation Flags & Thresholds
CCK_MAX_MCCABE ?= 10 # Exit when McCabe exceeds 10 for a function
CCK_MAX_HALSTEAD_VOL ?= 200.0 # Exit when Halstead Volume exceeds 200 for func.
CCK_SLOC_TYPE ?= PLOC # The type of SLOC to check against (PLOC|ELOC|LLOC)
CCK_MAX_SLOC_FCN ?= 200 # The max SLOC allowed per function
CCK_MAX_SLOC_MOD ?= 10000 # The max SLOC allowed per module
CCK_CODING_STD_EXIT_VIOLATION ?= 0 # Do not exit when a code std violation found

#Other options
CCK_HELP ?= 0 # Print help/usage information
CCK_BRIEF ?= 0 # Do not display banner

# Options with lint - lob's for performance.
NOLINTBANNER = -b
LINT_UNIT = -u
#LINTFLAGS = -e* -w3 +e1506 +e1509
#LINTCFGSPATHS = -i/opt/flint/supp/lnt -i/opt/flint/supp/gcc
LINTCFGSPATHS = -i$(COREBASEDIR)/lint-files/supp/lnt -i$(COREBASEDIR)/lint-files/supp/gcc
LINTCFGS =  $(LINTCFGSPATHS) co-gcc.lnt env-sled.lnt

# ?= allows for env var check as well - defined by user.
# Intentionally null here. If not env var override then use 
# #ifndef XLINTFLAGS ... #endif as guard - you have to, but env var good 
# "what if" usage and experimenting with lint without having to modify makefile
# or issue items on the command line as env var also takes precedence. 
XLINTFLAGS ?=  
LINTFLAGS = $(LINTCFGS) -w3 $(INCLUDES) $(DEFINES) $(XLINTFLAGS)
lint .PHONY : # lint configuration generate here - pre-requisite. 
	%if !%null(tgt)
	--$(XLINT) $(NOLINTBANNER) $(LINTFLAGS) $(LINT_UNIT) $(tgt)
	%else 
	--$(XLINT) $(NOLINTBANNER) $(LINTFLAGS) $(SRCS)
	%endif

LINT2GA_LEGACY_RULES=+e644 # Assignment before initialization

# Lint supplements
# Lint is used to supplement CodeCheck where appropriate, and where it can
# reliably check a given rule.
# TODO - consolidate code below for lint_*_supp targets - one change one place!

lint_ga_legacy_supp .PHONY :
	%if %null(tgt)
	%foreach src in $(SRCS)
		$(VV) --$(XLINT) -b -e* -w3 +e644 +e645 +e771 +e772 +e530 +e1401 +e1402 +e1403 $(src) >$(MAKEPID).lint2ga_legacy.tmp
		$(VV) %set LINTERRSTAT=$(status)
		$(VV) --$(XLINT2CS) $(MAKEPID).lint2ga_legacy.tmp $(COREBASEDIR)/lint2cs-files/src/lint2cs_ga_legacy.txt
		$(VV) rm $(MAKEPID).lint2ga_legacy.tmp
		%if $(status) && $(CCK_CODING_STD_EXIT_VIOLATION)
			%error Error: GA Legacy Violations found!
		%endif
		%echo
	%endfor
	%else 
		$(VV) --$(XLINT) -b -e* -w3 +e644 +e645 +e771 +e772 +e530 +e1401 +e1402 +e1403 $(tgt) >$(MAKEPID).lint2ga_legacy.tmp
		$(VV) %set LINTERRSTAT=$(status)
		$(VV) --$(XLINT2CS) $(MAKEPID).lint2ga_legacy.tmp $(COREBASEDIR)/lint2cs-files/src/lint2cs_ga_legacy.txt
		$(VV) rm $(MAKEPID).lint2ga_legacy.tmp
		%if $(status) && $(CCK_CODING_STD_EXIT_VIOLATION)
			%echo Error: GA Legacy Violations found!
			%error (Lint return code = $(status))
		%endif
		%echo
	%endif

lint_jsf_supp .PHONY :
    %if %null(tgt)
	%foreach src in $(SRCS)
		$(VV) --$(XLINT) -b -e* -w3 +e1506 +e1509 $(src) >$(MAKEPID).lint2jsf.tmp
		$(VV) %set LINTERRSTAT=$(status)
		$(VV) --$(XLINT2CS) $(MAKEPID).lint2jsf.tmp $(COREBASEDIR)/lint2cs-files/src/lint2cs_ga_legacy.txt
		$(VV) rm $(MAKEPID).lint2jsf.tmp
		%if $(status) && $(CCK_CODING_STD_EXIT_VIOLATION)
			%echo Error: JSF Violations found!
			%error (Lint return code = $(status))
		%endif
		%echo
	%endfor
	%else 
		$(VV) --$(XLINT) -b -e* -w3 +e1506 +e1509 $(tgt) >$(MAKEPID).lint2jsf.tmp
		$(VV) %set LINTERRSTAT=$(status)
		$(VV) --$(XLINT2CS) $(MAKEPID).lint2jsf.tmp $(COREBASEDIR)/lint2cs-files/src/lint2cs_ga_legacy.txt
		$(VV) rm $(MAKEPID).lint2jsf.tmp
		%if $(status) && $(CCK_CODING_STD_EXIT_VIOLATION)
			%echo Error: JSF Violations found!
			%error (Lint return code = $(status))
		%endif
	%echo
	%endif

# Optional flag (default is off) to run CodeCheck over entire project at once
# rather than on individual source files (for project-level stats)
CCK_PRJ ?= 0

# Serves for Metrics and Coding standards above
codecheck .PHONY :
    %if "$(CCK_RULE)" == "prj_sloc.cco" || $(CCK_PRJ) == 1
        %do prj_codecheck
    %elif %null(tgt)
	%foreach src in $(SRCS)
		%if "$(src,E)" == ".cpp" || "$(src,E)" == ".cc" || "$(src,E)" == ".cxx"
		$(VV) %set CCK_CONFIG_FILE = $(CCK_CONFIG_CPP_FILE)
		%elif "$(src,E)" == ".c"
		$(VV) %set CCK_CONFIG_FILE = $(CCK_CONFIG_C_FILE)
		%else
		%error CodeCheck is only supported on C and C++ source files
		%endif
		%do codecheck_filter CCK_ARGS="-R$(CCK_RULE) $(src)" ACTION="CodeCheck"
        %if "$(CCK_RULE)" == "ga_legacy.cco" 
        %do lint_ga_legacy_supp tgt=$(src)
        %elif "$(CCK_RULE)" == "jsf.cco" || "$(CCK_RULE)" == "tccs.cco"
        %do lint_jsf_supp tgt=$(src)
        %endif
	%endfor
	%else 
		%do codecheck_filter CCK_ARGS="-R$(CCK_RULE) $(tgt)" ACTION="CodeCheck"
        %if "$(CCK_RULE)" == "ga_legacy.cco" 
        %do lint_ga_legacy_supp
        %elif "$(CCK_RULE)" == "jsf.cco" || "$(CCK_RULE)" == "tccs.cco" 
        %do lint_jsf_supp
        %endif
	%endif

# TODO currently just processes .c files, update to include all sources...
prj_codecheck .PHONY :
    %do codecheck_filter CCK_ARGS="-R$(CCK_RULE) -Yprj_level $(SRCS,X,M\.c$$)" ACTION="CodeCheck"

codecheck_report .PHONY :
    %foreach src in $(SRCS)
    %echo
    %do codecheck_filter CCK_ARGS="-R$(CCK_RULE) $(src)" ACTION="CodeCheck"
    %endfor

# Filter out Abraxas CodeCheck banner messages
CCK_FILTER = | grep --invert-match Abraxas \
             | grep --invert-match "Checking .* file .* with rules from .*" \
             | grep --invert-match "File .* check complete." \
             | grep --invert-match "Warning C0000:" \
             | grep --invert-match "Warning C0049:" \
             | grep --invert-match "Warning C0047:" \
             | grep --invert-match "Unknown type" \
             | grep --invert-match "Expected comma, semicolon" \
             | sed -e '/^$$/d' # delete blank lines ($ is escaped with $$)
#            | awk "NR > 1"

CCK_LST =
%if $(CCK_DEBUG)
    CCK_LST = -H -M -D?
#    CCK_LST = -H -M
%endif

# The CodeCheck command. Note: `-S1' is the default option we want:
#  Process all includes with double quotes, not angle brackets by default.
#%if %null(INCLUDES)
#%error INCLUDES=$(INCLUDES) must be set
#%endif
#%if %null(DEFINES)
#%error DEFINES=$(DEFINES) must be set
#%endif

# CodeCheck Flags - brief, mccabe, sloc, halstead, etc. customizations...
CCK_FLAGS ?= 

%if $(CCK_BRIEF)
CCK_FLAGS += -Xbrief
%endif

%if $(CCK_HELP)
CCK_FLAGS += -Xhelp
%endif

CCK_SILENT ?= 0
%if $(CCK_SILENT)
%setenv CCK_SILENT=1
%endif

###

CCK_CMD = $(XCODECHECK) $(CCK_CONFIG_FILE) $(INCLUDES) $(DEFINES) -S0 \
          $(CCK_ARGS) $(CCK_FLAGS) $(CCK_LST) $(CCK_RULE_OPTS)

CCK_RULE_OPTS = 

do_rule ?= "nil"

codecheck_filter .PHONY :
	%if $(.TARGET) == "sloc" || $(do_rule) == "sloc"
		%if $(CCK_SLOC_TYPE)
		$(VV)%set CCK_RULE_OPTS += -Ysloc_type=$(CCK_SLOC_TYPE)
		%endif
		%if $(CCK_MAX_SLOC_FCN)
		$(VV)%set CCK_RULE_OPTS += -Vmax_fcn=$(CCK_MAX_SLOC_FCN)
		%endif
		%if $(CCK_MAX_SLOC_MOD)
		$(VV)%set CCK_RULE_OPTS += -Wmax_mod=$(CCK_MAX_SLOC_MOD)
		%endif
	%elif $(.TARGET) == "mccabe" || $(do_rule) == "mccabe"
		%if $(CCK_MAX_MCCABE)
		$(VV)%set CCK_RULE_OPTS += -Xmax_mccabe=$(CCK_MAX_MCCABE)
	%endif
	%elif $(.TARGET) == "halstead" || $(do_rule) == "halstead"
		%if $(CCK_MAX_HALSTEAD_VOL)
		$(VV)%set CCK_RULE_OPTS += -Vmax_halstead_vol=$(CCK_MAX_HALSTEAD_VOL)
		%endif
	%elif $(.TARGET) == "tccs" || $(.TARGET) == "jsf" || $(.TARGET) == "ga_legacy" || $(.TARGET) == "misra04" || $(do_rule) == "tccs" || $(do_rule) == "jsf" || $(do_rule) == "ga_legacy" || $(do_rule) == "misra04"
		%if $(CCK_CODING_STD_EXIT_VIOLATION)
		$(VV)%set CCK_RULE_OPTS += -Vexit_on_violation
		%endif
	%endif
	%if $(CCK_DEBUG)
		%echo $(CCK_CMD)
    %endif
	$(VV) --$(CCK_CMD) > $(MAKEPID).tmp 2>&1
	$(VV) %set CCK_ERRSTAT=$(status)
	$(VV) --$(CAT) $(MAKEPID).tmp $(CCK_FILTER)
	$(VV) --$(RM) $(MAKEPID).tmp
	%if $(CCK_ERRSTAT) != 0
		%error $(CCK_ERRSTAT) $(ACTION) failed; exiting compliance check 
	%endif
	$(VV)%set CCK_RULE_OPTS = 
	%if !$(CCK_SILENT)
	%echo
	%endif

help_cck .PHONY :
	$(XCODECHECK) --help

help_compliance .PHONY :
	%echo ---------------------------------------------------------------
	%echo ----- TM BUILD TOOLS H E L P ------
	%echo ---------------------------------------------------------------
	%echo Run metrics, code standards static analysis, and generate
	%echo documentation for entire code base or individual source files
	%echo 
	%echo mk sloc - Report SLOC per module and function
	%echo mk prj_sloc - Report SLOC per entire code set
	%echo mk mccabe - Report Modified and Traditional Cyclomatic Complexities
	%echo	per function
	%echo mk halstead - Report Halstead volume per function
	%echo mk oometric - Report Object-Oriented metrics per module
	%echo mk lint - Run static analysis on code set
	%echo mk doxygen - Generate Doxygen documentation for code set
	%echo 
	%echo For any of the above targets, the following options may be supplied 
	%echo 	to mk:
	%echo
	%echo tgt - Specify a single source file to run over
	%echo CCK_HELP=1 - Display help and usage information (cannot be used in
	%echo 				combination with CCK_BRIEF)
	%echo CCK_BRIEF=1 - Do not display banner info (cannot be used in 
	%echo 				combination with CCK_HELP)
	%echo -i -- keep going even if errors/violations/thresholds exceeded
	%echo
	%echo The options below are for specific targets only:
	%echo
	%echo CCK_SLOC_TYPE=PLOC|ELOC|LLOC (sloc only) - Indicate to the sloc rule
	%echo 		which type of SLOC the max threshold values below apply to (the
	%echo		default is PLOC)
	%echo CCK_MAX_SLOC_FCN (sloc only) - The max allowed SLOC per function [200]
	%echo CCK_MAX_SLOC_MOD (sloc only) - The max allowed SLOC per module [10000]
	%echo 
	%echo CCK_MAX_MCCABE (mccabe only) - The max allowed Modified McCabe [10]
	%echo 
	%echo CCK_MAX_HALSTEAD (halstead only) - The max allowed Halstead volume
	%echo 										[200.0]
	%echo 
	%echo CCK_CODING_STD_EXIT_VIOLATION (code std rules only) - Exit the build
	%echo		with error when violations are found in a module [0]
	%echo
	%echo Building targets - object files, libraries, images:
	%echo mk -- Build
	%echo mk -a -- Rebuild
	%echo 
	%echo Adminstration Functions:
	%echo mk clean
	%echo
	%echo mk -k -- keep going even if pre-requisite targets not built
	%echo mk -i -- ignore errors

