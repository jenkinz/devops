# File: make-inc-priv-host-platform.mak
# Purpose: 
# 	Hides platform differences transparently.
# 	Utilities are abstracted among common named variables or macros.

# Convenience Macros - defaults
VV ?= @@ # prepend to suppress shell line output
V ?= @

%if "$(OS,LC)" == "nt" # any version of Windows
O = obj
L = lib
E = .exe

## TODO - update below for Windows...

# Abraxas CodeCheck
XCODECHECK ?= 
# Gimpel PC-lint
XLINT ?= 
# lint2cs - converts lint warnings/errors to code standard warnings/errors
XLINT2CS ?=
# listfinddup - finds duplicate filenames in a directory tree
XLISTFINDDUP ?= 
# elapsedtime - simple timer for build
XELAPSEDTIME ?= 

GREP ?= 
SED ?= 
AWK ?= 
RM ?= 
CAT ?= 

# The CodeCheck installation root
CODECHECK_PATH ?= 

# .BEFORE rule will validate all tools optionally - including unix-win32 tools
# and a unix mkdir
MKDIRSPEC = which mkdir$E
MKDIR = $(MKDIRSPEC,SH) -p
PS = ;
DEVNULL = NUL

%elif "$(OS,LC)" == "unix" # Linux, Unix including OS X, etc.
O = o
L = a
E =

# Abraxas CodeCheck
XCODECHECK ?= check
# Gimpel Lint
XLINT ?= flint
# lint2cs - converts lint warnings/errors to code standard warnings/errors
XLINT2CS ?= lint2cs
# listfinddup - finds duplicate filenames in a directory tree
XLISTFINDDUP ?= listfinddup
# elapsedtime - simple timer for build
XELAPSEDTIME ?= elapsedtime

GREP ?= grep
SED ?= sed
AWK ?= awk
RM ?= rm
CAT ?= cat
MKDIR ?= mkdir -p

PS = :
DEVNULL = /dev/null

# The location for CodeCheck config files
CODECHECK_PATH ?= $(COREBASEDIR)/codecheck-files

%else
%error OS=$(OS) is unknown ... 
%endif

CAT = cat
RM = rm



