# File: make-inc-priv-core.mak

.NODEFAULT :

%include $(COREBASEDIR)/bld/make-inc-priv-host-platform.mak
%include make-inc-pub-compiler.mak
%include make-inc-pub-spec-descr.mak
%include $(COREBASEDIR)/bld/make-inc-priv-compliance.mak  
%include make-inc-priv-doxygen.mak

.DEFAULT :

