# Toolchain for Sage EDK
#
# Name: sage
#
# Notes:
#   1. SAGE_HOME env var must be set to the sage_edk installation directory.
#      If you install the devops tools, this is located by default in
#      `/usr/local/devops/apps/sage_edk`.
#

CC = $(SAGE_HOME)/tools/xgcc/bin/i386-elf-gcc
#CPP = $(SAGE_HOME)/tools/xgcc/bin/i386-elf-g++
CPP = g++
#AS = $(SAGE_HOME)/tools/xgcc/bin/i386-elf-as
#LD = $(SAGE_HOME)/tools/xgcc/bin/i386-elf-ld
AS = $(CC)
LD = $(CC)
AR = $(SAGE_HOME)/tools/xgcc/bin/i386-elf-ar

#CCK_CONFIG_C = gcc.ccp
#CCK_CONFIG_CPP = g++.ccp

TOOLCHAIN_LIBLOCATIONS =
TOOLCHAIN_LIBS =

