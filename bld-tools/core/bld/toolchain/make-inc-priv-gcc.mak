# Toolchain for gcc
#
# Name: gcc - GNU C Compiler
#
# TODO: Brian - move to tm_build_tools
#

# diab compiler license env var. Env Var Common to Windows and Linux
#%getenv LM_LICENSE_FILE ENV_LM_LIC
#%if %null(ENV_LM_LIC)
#%setenv LM_LICENSE_FILE = 27000@rex
#%endif

CC = gcc
CPP = g++
AS = $(CC)
LD = g++
AR = ar

CCK_CONFIG_C = gcc.ccp
CCK_CONFIG_CPP = g++.ccp

TOOLCHAIN_LIBLOCATIONS =
TOOLCHAIN_LIBS =

