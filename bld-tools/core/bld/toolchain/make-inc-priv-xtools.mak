# Toolchain for XTOOLS
#
# Name: xtools
#
# Notes:
#   1. DEVOPS_HOME env var must be set (DevOps tools must be installed, which
#      sets this env var to the appropriate location which is /usr/local/devops
#      by default).
#

TOOLCHAIN_NAME = "xtools"
TOOLCHAIN_PREFIX = $(DEVOPS_HOME)/toolchains/xtools/xtools_output

# Compiler, assembler, librarian, linker:
CC = i386-elf-gcc
CPP = i386-elf-c++
AS = i386-elf-as
AR = i386-elf-ar
LD = i386-elf-ld

# CodeCheck configuration:
#CCK_CONFIG_C = gcc.ccp
#CCK_CONFIG_CPP = g++.ccp

# Library locations:
TOOLCHAIN_LIBLOCATIONS = $(TOOLCHAIN_PREFIX)/i386-elf/lib \
                         $(TOOLCHAIN_PREFIX)/lib/gcc/i386-elf/4.8.4
			 
# Libraries:
TOOLCHAIN_LIBS = libstdc++.a libgcc.a libc.a

# Startup objects:
STARTUP_OBJS += $(TOOLCHAIN_PREFIX)/i386-elf/lib/cygmon-crt0.o
