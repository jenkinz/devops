# makefile to build filedup utility program.
IMAGE = filedup
XCFLAGS ?=
%if "$(OS,LC)" == "nt"
E = .exe
O = obj
CC = bcc32
DEBUG = -v
LDFLAGS = $(DEBUG)
XCLEANMASK = *.tr2 *.tds
%elif "$(OS,LC)" == "unix"
E = 
O = o
CC = g++
LD = $(CC)
LDFLAGS = $(DEBUG) -o $@
XCLEANMASK =
%else
%error OS=$(OS) is not recognized!
%endif

CFLAGS = $(DEBUG)
LD = $(CC)

$(IMAGE)$E : $(IMAGE,>.$O) 
	%echo Linking $@
	>$(LD) $(LDFLAGS) $^

.cpp.$O :
	%echo Compiling $<
	>$(CC) $(CFLAGS) $(XCFLAGS) -c $<	

FILEPATH = .			   
FILEMASK = *.c*	
test .ALWAYS :
	filedup $(FILEPATH) $(FILEMASK)

compile .ALWAYS :
	$(MAKE) $(tgt).$O

clean .ALWAYS :
	--rm *.$O $(IMAGE)$E 
	%if !%null(XCLEANMASK)
		%foreach mask in $(XCLEANMASK)
			%if %file($(mask))
				--rm $(mask)
			%endif
		%endfor
	%endif
