Utility: listfiledup
Andrew Scheurer

This utility certainly wasn't as easy as it looked to design and implement.
The use of the tool is with using a makefile that operates over a source code
set that may have duplicate source code filenames or worse duplicate include
filenames.

While a makefile can partition the includes and the object files into their
own directories and provide explicit pathing on the object files and/or put
them into libraries to remove conflicts - this is still a very tricky thing
to do. This tool can be used to indicate where duplicates exist and its 
recommended for many reason to eliminate duplicates as a source of major 
troubles and ambiguity.
Now there is no crc involved - no compare of contents, only by filename 
by both direct compare or equivalence using suffixes of source filenames.
The use of an implicit rule for example would cite suffixes, all of which 
produce an object file. If that object file is stored in 1 location than the
opportunity for conflicts is 100% if duplicate root names exist among those
suffixes built. The object files will overwrite each other, .c and .cpp with
same root name still produce an object file. The equivalence suffixes ideally
are replicated from the makefile and the implicit rule.

Duplicate include files are either separated or the use -I is used where the
first found include is used but again this is not easy to get completely right
and opportunities exist where this process will fail unpredictably with makefile
maintainence.

Whether this utility is used as a guard, for example if the object files go
to 1 directory or multiple include files provide a problem.
Or if its used informatively to partition the makefile where .PATH.ext might
not be used or if so there are multiple makefiles then this tool can inform
on checking to see that no duplicates are present.
See the Article "Recursive Makefiles considered harmful" as that is the basis
of this tool's usage.
However even that article didn't solve the issues of potential duplications
and problems therein - only a top down structure without breaking the dependency
tree.
Recursive Makefiles Considered Harmful:
see http://miller.emu.id.au/pmiller/books/rmch/

Usage:
listfinddup -suffixes .c.cpp.s -inputfile filenames
listfinddup -suffixes .c.cpp.s -inputfile filenames -caseinsensitive false
listfinddup [-suffixes .c.cpp [-suffixes .asm.s]...] -inputfile file
listfinddup [-suffixes .C.ASM.S.]...] < file 
echo x.c y.c a/x.c | listfinddup -suffixes $(SUFFIXES)
where SUFFIXES = .x.y.z in a makefile.

-caseinsensitive false override default which is true
Because Windows is inheriting DOS programs - turning off caseinsensitivity
is prudent. Even on Linux - case sensitivity on filenames is confusing and
error prone - the best policy is to make all filenames lower case and using the
_ character as required. On Windows #include "FILE.H" and "file.h" are the same
but on Linux #include "SourceFile.h" and "sourceFile.h" are quite different.
#include "source_file.h" or #include "sourcefile.h" or #include <sourcefile.h>
is more prudent.
By deafault on Linux and Windows caseinsensitive is true and will catch FILE.H
and file.h as being equivalent.

Note: It is never recommended to use upper case extensions
Always use lower case source file extensions!-and- lowercase filenames if possible for easier portability! 
program uses std::cin for interactive but recommend use of < input file of filenames

Compiling on Windows or Linux should be easy.
Using Borland on Windows but Microsoft or even GNU Compiler will work.
cl listfinddup.cpp will create listfinddup.exe
or 
g++ listfinddup.cpp -o listfinddup will create listfinddup.exe on Windows or
of course listfinddup on Linux

thats about it ...

