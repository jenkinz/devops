#include <string>
#include <iostream>
#include <map>
#include <vector>
#include <set>
#include <iterator>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <cctype>
#include <algorithm> 

////////////////////////////////////////////////////////////////////////////////
// Purpose:
// pipe in input... 
// Test Case: cat file | <this-image>
// or   echo foo1 foo2 foo3 | <this-image> [-suffixes .x.y]
// or could use file input : <this-image> [-suffixes .x.y]  < ifile - works also.
// or finally <this-image> [-suffixes .x.y] -inputfile ifile
////////////////////////////////////////////////////////////////////////////////

namespace {

std::string suffixes;
std::string inputfile;
bool stdin_used = true;
bool caseinsensitive = true;

std::string getext(const std::string& s, const char * delim = ".")
{
    std::string ext;
    const std::size_t n = s.find_last_of(delim);
    if (n != std::string::npos) 
        ext = s.substr(n);
    return ext;

}

std::string stripext(const std::string& s, const char * delim = ".")
{
    std::string s2(s);
    const std::size_t n = s2.find_last_of(delim);
    if (n != std::string::npos) 
        s2 = s.substr(0, n);
    return s2;

}

std::string strip2file(const std::string& s, const char * delim = "/\\")
{
    std::string s2(s);
    const std::size_t n = s2.find_last_of(delim);
    if (n != std::string::npos) 
        s2 = s.substr(n + 1);
    return s2;

}

std::string strip2base(const std::string& s)
{
    std::string s2(s);
    s2 = strip2file(s2);
    s2 = stripext(s2);
    return s2;
}

void help(const std::string& program_name)
{
    const std::string& prog = strip2base(program_name);
    std::cout << prog << " -suffixes .c.cpp.s -inputfile filenames"
                      << " -caseinsensitive true|false\n";
    std::cout << "Usage: " << prog  
              << " [-suffixes .c.cpp [-suffixes .asm.s]...] -inputfile file\n"
              << prog << " [-suffixes .C.ASM.S.]...] < file \n"
              << "add -caseinsensitive true|false - true for Windows \n\n";

    std::cout << "Note: It is never recommended to use upper case extensions\n"
              << "Always use lower case source file extensions!"
              << "\nUse lowercase filenames if possible for easier "
              << "portability! \nprogram uses std::cin for interactive but "
              << "recommend use of < file\nor -inputfile file"
              << std::endl;

    std::cout << "Defaults:"
              << " caseinsensitive " << (caseinsensitive ? "true" : "false")
              << " : " 
              << " suffixes " << (suffixes.empty() ? "empty" : suffixes.c_str())
              << std::endl;
}

void options(int argc, char *argv[])
{
    const char* prog = argv[0];

    // argc is always at least 1 - the program name itself but that must
    // trigger equivalent to help or -help as an explicit argument.
    // Note: argc == 1 implies use of std::in so use of boolean stdin_used
    // is not necessary strictly - only used to show behavior but superfluous
    if (argc == 1 && stdin_used)
        std::cout << "=> ";

    if (argc > 1 && std::string(argv[1]) == "-help") {
        help(prog);
        exit(0);
    }

    // argc = 3 or greater allows for -suffixes .c.cpp[.ext1.ext2 ... .extN]
    // However if argc == 2, then that may imply no suffixes passed or 
    // -inputfile and program uses indirection via listfinddup < dups.txt
    if (argc < 3) {
        if (argc > 1 && std::string(argv[1]) == std::string("-suffixes")) {
            std::cout << "-suffixes has missing arg."
                      << " Example -suffixes .c.cpp.asm\n\n";
            help(prog);
            exit(0);
        }
    }

    for (int i = 1; i < argc; ++i) {
        if (std::string(argv[i]) == std::string("-suffixes")) {
            if ((i + 1) < argc)  {
                suffixes += std::string(argv[++i]);
                const std::size_t n = suffixes.find("."); // reg exp .x better
                if (n == std::string::npos) {
                    help(prog);
                    exit(-1);
                }
            }
            else {
                std::cout << "-suffix option error - missing argument\n";
                help(prog);
                exit(-1);
            }
                
        }
        else if (std::string(argv[i]) == std::string("-inputfile")) {
            if ((i + 1) < argc) 
                inputfile = argv[++i];
            else {
                std::cout << "-inputfile option error - missing argument\n";
                help(prog);
                exit(-1);
            }
        }
        else if (std::string(argv[i]) == std::string("-caseinsensitive")) {
            if ((i + 1) < argc) {
                const std::string opt(argv[++i]);
                caseinsensitive = ((opt == "true") ? true : false);
            }
            else {
                std::cout << "-inputfile option error - missing argument\n";
                help(prog);
                exit(-1);
            }
        }
        else {
            std::cout << "option: " << argv[i] << "not recognized!\n";
            help(prog);
            exit(-1);
        }
    }
}

int mytolower(int c)
{
    // return ::tolower(c);
    return c |= (1 << 5);
}

void tolowercase(std::string& str)
{
    #if 0
    for (unsigned i = 0; i < str.length(); ++i) 
        str[i] = ::tolower(str[i]);
    #else
    // won't compile - ::tolower doesn't meet spec on operating on ints
    // not char - conversion is not proper.
    //std::transform(str.begin(), str.end(), str.begin(), ::tolower);
    // this works but I have to write my own character conversion.
    std::transform(str.begin(), str.end(), str.begin(), &mytolower);
    #endif
}

std::string transform_token(const std::string& token)
{
    std::string tok(token);
    if (caseinsensitive) 
        tolowercase(tok);

    // Note: If suffixes is an empty set then the ext will not be found
    //       suffixes.empty() true does not invalidate checking for matches
    //       I don't need a guard if (!suffixes.empty()) ... 
    const std::string ext = getext(tok);
    const std::size_t n = suffixes.find(ext);
    if (n != std::string::npos) // found in suffixes set
        tok = strip2base(tok);
    else
        tok = strip2file(tok);  // not found, no base reqd

    return tok;
}

// check if dup candidate and member of set are equivalent on suffix or 
// extension. Looking at set membership restricted to input suffixes
// file.c and file.cpp are equivalent if -suffixes .c.cpp[.cc.asm] is provided
// as input by user. No suffix provided only does an exact match
bool matchsuffix(const std::string& curr, const std::string& dup)
{
    assert(!(curr.empty() || dup.empty()));

    const std::string currext = getext(curr);
    const std::string dupext = getext(dup);

    // all suffixes, currext, and dupext are non-empty
    if (!(suffixes.empty() || currext.empty() || dupext.empty())) {
        const std::size_t n1 = suffixes.find(currext);
        const std::size_t n2 = suffixes.find(dupext);
        // check for equivalence match
        if (n1 != std::string::npos && n2 != std::string::npos) 
            return true;
    }

    return false;
}

// options: listfinddup -suffixes .c.cpp.cxx -caseinsensitive
// or       listfinddup has no suffixes (empty set) and case sensitive
bool dupfilename_filter(const int current_filenames_index, 
                        const std::vector<std::string>& filenames, 
                        std::set<unsigned>& indexset)
{
    // if filename member of suffixes and at least one member of set filename
    // member of suffixes then true
    // else look for exact match
    // otherwise false
    // iterate set looking for exact
    std::string curr = filenames[current_filenames_index];
    assert(!curr.empty());
    curr = strip2file(curr);
    bool dupfound = false;

    typedef std::set<unsigned> DictValue;
    for (DictValue::iterator i = indexset.begin(); i != indexset.end(); ++i) {
        std::string dup = filenames[*i];
        dup = strip2file(dup);
        if (caseinsensitive) 
            tolowercase(dup);
        if (dup == curr || matchsuffix(dup, curr)) {
            dupfound = true;
            break;
        }
    }

    return dupfound;
}


} // end anonymous namespace

// Identify duplicates via front filter or input indirection from file
int main(int argc, char *argv[])
{
    options(argc, argv);

    // Equivalent to while (std::cin >> token) { insert token into vector }
    std::vector<std::string> tokens;
    std::istream * input = &std::cin;
    std::ifstream * in = 0;
    if (!inputfile.empty()) {
        in = new std::ifstream(inputfile.c_str());
        if (!(*in)) {
            std::cout << "file: " << inputfile << " not found!" << std::endl;
            help(argv[0]);
            delete in;
            return -1;
        }
        input = in;
        stdin_used = false;     // std::in is implied, this isn't necessary
    }

    std::istream_iterator<std::string> file_iter(*input), end_of_stream;
    std::copy(file_iter, end_of_stream, std::back_inserter(tokens));

    typedef std::set<unsigned> DictValue;
    typedef std::map<std::string, DictValue> DictType;
    DictType dict;
    const int N = tokens.size();
    assert(N > 0);
    for (int i = 0; i < N; ++i) {
        const std::string token(tokens[i]);
        // std::cout << "vector token = " << token << std::endl;
        const std::string tok = transform_token(token);
        DictType::iterator it =  dict.find(tok);

        // if found in dictionary, test, filter, and add to set of indexes
        // indicating number of duplicates.
        // Otherwise - its a new entry and add to the dictionary to allow for
        // testing for any other duplicates that may occur later in the vector
        if (it != dict.end()) {
            // stripped filename found - insert index into value set subject
            // to the filter iterating over the set where set has more than
            // 1 entry - assertion of relationship between map and set
            // that is set has N entries
            assert(it->second.size() >= 1);
            const bool isdup = dupfilename_filter(i, tokens, it->second);
            if (isdup)
                it->second.insert(i);
        }
        else {
            // insert unique pathless filename into dictionary for later
            // lookup qualification.
            DictValue v; 
            v.insert(i);
            dict.insert(DictType::value_type(tok, v));
        }
    } // end for

    // Find duplicates if any and print out
    int dups_found = 0, dup_pairs_found = 0;
    for (DictType::iterator it = dict.begin(); it != dict.end(); ++it) {
        if (it->second.size() > 1) {
            if (dups_found == 0) { 
                std::cout << "Duplicates Found: "
                          << "  => Exact match or suffix match equivalency"
                          << "\n-----------------";
                if (caseinsensitive)
                    std::cout << "   => caseinsensitive true\n";
            }
            dups_found += it->second.size();    
            ++dup_pairs_found;
            for (DictValue::iterator i = it->second.begin(); 
                 i != it->second.end(); ++i) {
                const std::string& dup = tokens[*i];
                std::cout << '\t' << dup << "\n";
            }
            std::cout << std::endl;
        }
    }

    delete in;
    if (dup_pairs_found)
        std::cout << "duplicate pairs found = " << dup_pairs_found << std::endl;
    return dups_found;  // or return dup_pairs_found;

} // end main



