/*
 * Copyright (C)2011 by General Atomics Aeronautical Systems, Inc.
 * All Rights Reserved.
 */

/* File:   elapsedtime.cpp
 * Author: scheur
 * Date:   9/7/2011
 * Description: Utility to be used in timing processes. 
 * For example embedding in a makefile to time a complete build after make clean
 */

// This implementation assumes this program with /start and /stop is run from
// the same directory so it can access the file to read the start time and
// calculate the difference from the stop time.
// Primary usage is a makefile w/ some .BEFORE, .AFTER pre and post condition
// where the build time can be calculated.
// This is not using a precise clock as its not required and quasi second 
// accuracy is sufficient.

#include <string>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cassert>
using namespace std;

////////////////////////////////////////////////////////////////////////////////

#ifdef _WIN32
#include <windows.h>
#elif defined(__linux)  // presuming GNU compiler but will likely work w/ others
#include <sys/time.h>
const unsigned GetTickCount()
{
    struct timeval tv;
    if (gettimeofday(&tv, NULL) != 0)
        return 0;

    // get the current number of milliseconds since Jan 1st 1970
    return (tv.tv_sec * 1000) + (tv.tv_usec / 1000);
}
typedef unsigned long DWORD;
#else
#error "Compiler/OS not recognized - Platform not supported"
#endif

enum {USAGE_ERR = 1, MISSING_TMP_FILE};
static const char* const startTimeFilename = "starttime.out";
const std::string normalizeargv0(const char* argv0);
void displayDuration(const DWORD secs);

////////////////////////////////////////////////////////////////////////////////

int main(int argc, const char* argv[])
{
    if (argc == 1) {
        cout << "usage: " << argv[0] << " </start|/stop>" << endl;
        return USAGE_ERR;
    }

    if (argc > 1) {
        string cmd = argv[1];
        if (cmd == "/start") {
            const DWORD startTime = GetTickCount();
            ofstream out(startTimeFilename, ios::binary);
            out << startTime;
        }
        else if (cmd == "/stop") {
            const DWORD endTime = GetTickCount();
            ifstream in(startTimeFilename, ios::binary);
            if (!in) {
                cout << "\nFile missing in cwd: " << startTimeFilename << endl;
                const std::string program = normalizeargv0(argv[0]);
                cout << program << " /start followed by ";
                cout << program << " /stop" << endl;
                cout << "usage: " << program << " </start|/stop>" << endl;
                return MISSING_TMP_FILE;
            }
            DWORD startTime = 0;
            in >> startTime;
            in.close();
            if (const int stat = std::remove(startTimeFilename)) {
                cout << "Error deleting File " << startTimeFilename;
                cout << ": No such file exists. status = " << stat << endl;
            }
            const DWORD diff = endTime - startTime;
            assert(diff > 0);
            const DWORD secsElapsed = diff / 1000;
            //cout << "secs elapsed = " << secsElapsed << endl;
            displayDuration(secsElapsed);
        }
        else {
            cout << "usage: " << argv[0] << " </start|/stop>" << endl;
            return 2;
        }
    }
} // end main - return 0 implied in C++

////////////////////////////////////////////////////////////////////////////////

const std::string normalizeargv0(const char* argv0)
{
    assert(argv0 != 0);
    string program(argv0);
    size_t found = program.find_last_of("/\\"); // windows=\\, unix = / path sep

#ifdef DISPLAY
    cout << " folder: " << program.substr(0, found) << endl;
    cout << " file: " << program.substr(found + 1) << endl;    
#endif

    return program.substr(found + 1);
} // end normalizeargv0

////////////////////////////////////////////////////////////////////////////////

void displayDuration(const DWORD secs)
{
    const int minutes = secs / 60;
    if (minutes) {
        cout << "time elapsed - " << minutes << " minute(s) ";
        const int seconds = secs % 60;
        if (secs)
            cout << seconds << " second(s)" << endl;
        else
            cout << endl;
    }
    else {
        const int seconds = secs % 60;
        cout << "time elapsed - seconds: " << seconds << endl;
    }
} // end displayDuration

////////////////////////////////////////////////////////////////////////////////


