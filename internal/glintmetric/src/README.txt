Wed 02/23/2011
Update: Project put on hold so here is how glintmetric works.
Used with Make to created a file with a weighted metric that is a sum
of weights established per info, warning and error that are categories generated
by lint

Usuage:
.cpp.o :
    $(GLINT) $(GLINTFLAGS) $< | glintmetric [filename=$<] [-i wt -w wt -e wt]
    So filename or $< is optional and just aids in reporting information or
    making the output easy to understand.
    -i wt
    -w wt
    -e wt
    allow you to override pre-assigned weights hardcoded into the program via
    the cmd line.
    The defaults are 1, 2, 3 for info, warning, and error (i, w, e) respectively.
    They too are optional....



History:
- Updated to use g++ Windows compiler but this code will compile on Linux
very easily. The Makefile should make it work automatically.

- see notes on makefile changes, updates from notes


- add gcc compiler - cpp.exe to the .BEFORE

- add cpp.exe to filter out prior to applying McCabe - see pmccabe website 
(and see ref on this matter - Tim)

- add in .li, .mc, .ha, .po incremental files in relation to .c.xx : 
HDRS optional - yes for lint

- add in this makefile.mak changes for creation of the lint rule.


