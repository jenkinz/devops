/*
 * Copyright (C)2011 by General Atomics Aeronautical Systems, Inc.
 * All Rights Reserved.
 */

/* File:   glintmetric.cpp
 * Author: scheur
 * Date:   7/2/2011
 * Description: Proving a single number/metric for use w/ lint guards in a 
 * makefile. For example lint [args] file.cpp | glintmetric [-i n -w n' -e n''] 
 * generates a number based on weights. The -i info, -w warning, -e error 
 * weights are defaulted or overridden via command line. 
 * Based on this weighting method a lint metric could decide to continue a build 
 * or not. 
 */


/******************************************************************************
PROGRAM DESCRIPTION/Usage: glintmetric.exe(.out)() is designed to be
1.) portable - can port to Linux or Windows, uses std library only
2.) fast, efficient
3.) tunable - weights can be file driven or will eventually support - or pass
as arguments - defaults otherwise are established
4.) supports piped input - the output file can be typed or cat'd into the program
or the direct output of lint can be fed into the program. The program acts as
a filter.
5.) usage. -help will be supported or if any arguments are not recognized the
program will display usage and exit.

glint is Gimpel Lint - makers of pc-lint for Windows and flexelint for Linux
pc-lint is lint-nt.exe and flexelint is flint
This program expects only this input. If it receives input from another program
it will error out.            

The input has the following pattern:
glintmetric.cpp  42  Info 701: Shift left of signed quantity (int)
glintmetric.cpp  42  Error 59: Bad type
glintmetric.cpp  42  Error 40: Undeclared identifier 'endl'
glintmetric.cpp  42  Info 701: Shift left of signed quantity (int)
glintmetric.cpp  42  Warning 504: Unusual shift operation (left side unparenthesized)

-or- env-sled.lnt (slickedit)
glintmetric.cpp 1 error 1904: (Note -- Old-style C comment -- Effective C++ #4)

[filename] [num] [error|Error] num[:]       
 
[flint|lint-nt.exe] [args] file[files..] | glintmetric 
comes up w a single number 0 to N using default weights.
 
[flint|lint-nt.exe] [args] file[files..] | glintmetric -i 1 -w 2 -e 3 
comes up w a single number 0 to N using weights 1, 2, 3 
 
In both cases the 
sum = (Ne * We) + (Ni * Wi) + (Nw + Ww) 
where N[e,i,w] means number of lint errors, info, or warnings 
and W[e,i,w] is the assigned weights in the linear weighted sum. 
 
******************************************************************************/

#include "glintmetric.hpp"
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <cstring>
#include <cassert>
using namespace std;

//////////////////////////////////////////////////////////////////////////////

#ifndef BUF_SIZE
#define BUF_SIZE   2500                  // could be smaller but at least > 300 
#endif
#ifndef MAX_LINES_PROCESSED
#define MAX_LINES_PROCESSED   (5000 * 2) // some lint reports are huge.
#endif

static const int BufSize = BUF_SIZE;    // max length of a given line of input.
static const int MaxLinesProcessed = MAX_LINES_PROCESSED;

#ifdef XCOUT
#define xcout 0 && cout
#else
#define xcout true && cout
#endif

struct MetricsLookup {
    const char* category1;    // lint uses Error
    const char* category2;    // Lint uses error if slickedit - env-sled.lnt
    int weight;
    int total;
};

static MetricsLookup metrics[] = {
   /* normal  |  slickedit */
    {"Info",    "(Info",     1},
    {"Warning", "(Warning",  2},
    {"Error",   "(Error",    3}
};

static const int metricstablesz = sizeof(metrics)/sizeof(MetricsLookup);
static string filename;

void usage();
string addMetricReportRec(string& rec);
std::string finalizeMetricReport(const string& report);

//////////////////////////////////////////////////////////////////////////////
// Next Steps:
// Produce a single number for the module and output filename and thats it
// and make this an option. The long format is the default option.
//////////////////////////////////////////////////////////////////////////////

// look for info weight, warning weight, error weight options
// If I get garbage like 'a' or 'x' then atoi will assign 0
// I could use istringstream to error out if bad input but this is a
// lark of a program - atoi is good enough. Once the script is written
// this program should just work.
// If a filename was assigned, then lets hope it doesn't have a -i or -w or -e
// in the front of the filename - I just skip.
void extractWeights(int argc, char *argv[])
{
    assert(argc > 1);
    for (int i = 1; i < argc; ++i) {
        std::string opt(argv[i]);
        if (opt == "-i") {
            if ((i + 1) < argc) {
                metrics[0].weight = atoi(argv[i+1]);
            }
        }
        else if (opt == "-w") {
            if ((i + 1) < argc) {
                metrics[1].weight = atoi(argv[i+1]);
            }
        }
        else if (opt == "-e") {
            if ((i + 1) < argc) {
                metrics[2].weight = atoi(argv[i+1]);
            }
        }
    }
}

int main(int argc, char* argv[])
{
    xcout << "argv[0], argc " << argv[0] << " : " << argc << endl;
    if (argc > 1) {
        if (argv[1][0] != '-') {
            filename = argv[1];     // store for later use
            //cout << "Applying lint metrics to file: " << filename << endl;
        }
        // extract weights if any - looking for specific switches values.
        extractWeights(argc, argv);
    }

    int linecount = 0;

    // string str, report;
    std::string report;

    while(cin) {                     
        if (++linecount > MaxLinesProcessed) {
            if (!filename.empty()) {
                cout << "File: " << filename << " - ";
            }
            cout << "Fatal Error - line count = " << linecount;
            cout << " is too high" << endl;
            exit(-1);
        }

        std::string input_line; // char input_line[BufSize] = {0};
        std::getline(cin, input_line); // cin.getline(input_line, sizeof(input_line));

        // skip blank lines. check for a line with all blanks? 0x20 characters?
        // can't memcmp because I don't know how many blanks - I'd have to 
        // iterate the buffer up to '\0' and if all blank, then continue.
        //if (input_line[0] == '\0') 
        if (input_line.empty()) 
            continue;

        // I do want the linefeed for the final report, getline strips it.
        //input_line[strlen(input_line)] = '\n';
        input_line += '\n';
		//str = input_line;
        // xcout << "input line = " << str << endl;
        xcout << "input line = " << input_line << endl;

        // accumulate report record by record
        // report += addMetricReportRec((str);
        report += addMetricReportRec(input_line); // (str);
        xcout << "accum report = " << report << endl;
    }

    // Do linear weighted sum and add to the report string.
    std::string finalReport = finalizeMetricReport(report);
    cout << finalReport << endl;

    return 0;
}

//////////////////////////////////////////////////////////////////////////////

void usage()
{
    // TBD - right now not used
    // Support a -help argument for later uses.
}

//////////////////////////////////////////////////////////////////////////////

string addMetricReportRec(string& rec)
{
    string tmp;
    string token;

    int cnt = 0;
    istringstream strm(rec);
    bool found = false;
    while (strm) {
        strm >> token;
        if (!strm)
            break;

        if (cnt++ == 0){     // skip first token, filename usually
            xcout << "first token = " << token << endl;
            continue;
        }

        for (int i = 0; i < metricstablesz; ++i) {
            // Found a match
            if (token == metrics[i].category1 
                || token == metrics[i].category2)  {
                // validate input further post Warning|Info|Error lintcode 
                // problem - if env-sled.lnt used then num: not num is
                // read and convert and strip off : from number, then convert
                // and test if its a numeric value.
                strm >> token;
                if (!strm) {
                    xcout << "bad token = " << token << endl;
                    break;
                } 
                else {

                    #if 0 
                    // its all error w/ slickedit - lint in big (Error, etc.
                    if (token[token.length() - 1] == ':') {
                        token = token.substr(0, token.length() - 1);
                        xcout << "new token with : stripped = "<< token << endl;
                    }
                    #endif

                    // env-sled.lnt option not given to glint,
                    // check number follows
                    if (token != "--")  { 
                        istringstream in(token);
                        int lintcode = 0;
                        in >> lintcode;
                        if (!in) {
                            xcout << "bad conversion, lintcode = "<<lintcode <<endl;
                            break;
                        }
                        xcout << "(normal case only - no env-sled.lnt) lintcode = " << lintcode << endl;
                    }
                }

                // convert weight to text and add to report, add static total
                metrics[i].total += 1;      // add to running total
                ostringstream out;
                out << metrics[i].weight;
                tmp += "\t";
                tmp += out.str();
                tmp += "\t";
                found = true;
                break;
            }
        }
    }

    // if tmp.isempty() - there was a problem or the input just didn't match
    // find a set of options for lint to produce more consise terse reports.
    if (found)
        tmp += rec;

    return tmp;
}

//////////////////////////////////////////////////////////////////////////////

std::string finalizeMetricReport(const string& report)
{
    std::string tmp;

    // Look at the totals now for this given module's lint output.
    // Note: Expecting input from lint output piped into this program using |
    int linearweightedsum = 0;
    for (int i = 0; i < metricstablesz; ++i) {
        linearweightedsum += metrics[i].total * metrics[i].weight;
    }
    xcout << "linearweightedsum= " << linearweightedsum << endl;

    ostringstream out;
    out << linearweightedsum;
    std::string numstr = out.str();
    tmp += numstr;
    //tmp += "\t";    // first col separated by tab is the lint metric
    tmp += report;  // add to the report
    return tmp;
}

