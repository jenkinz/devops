glintlookup[.exe] works to find the msg.txt using the path or an environment
variable and either prints to std out the entire msg.txt file, from which you
can use | grep [token]
or you provide the lint msg number and it prints out text.
see glintlookup -help|--help|/?

On Building for either Windows or Linux:
----------------------------------------
There is no makefile - its really not needed although this stuff did go 
through lint on both Windows and Linux and was run through pmccabe on Windows
and Linux - although pmccabe wouldn't change on the OS
But flint and lint-nt(pc-lint) could differ based on different compiler plugins
but used the same switches.

Building is easy:
Linux Build - just issue g++ glintlookup.cpp -o glintlookup
              But any Linux C++ compiler will work
              
Windows Build - just issue cl|g++|bcc32 glintlookup.cpp
                Any of these windows compilers will work - just standard C++
                
                
