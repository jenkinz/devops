/*
 * Copyright (C)2012 by General Atomics Aeronautical Systems, Inc.
 * All Rights Reserved.
 */

/* File:   glintlookup.cpp
 * Author: scheur
 * Date:   3/29/2012
 * Description: Utility to be used in resolving lint errors|warn|info
 */

// Program Description
// Takes PC-Lint or Flint's msg.txt as input along with a number from the user
// and provides description as output.
// For example on either Windows or Linux
// [c:\|$] glintlookup 54 
// outputs
// 		"Division by 0 -- The constant 0 was used on the right hand side
// 	 	 of the division operator (/) or the remainder operator (%)." 
// This is text directly from lint. 
// If no numeric argument is given the result will stream out msg.txt using 
// the path from executing which flint|lint-nt depending on what operating
// system you're using. A shellexec on Win32 will of course be different than
// a system(...) call on Unix - or if available I'll use a portable function
// call within Windows and Unix to allow for whatever portability exists
// This implies that you need which.exe on your Windows system either through
// cygwin or natively through the GNU-Win32 unix utilities
// Unfortunately MinGW includes almost everything except which.exe so use
// GNUWin32 as your toolset and put in your path
// see http://gnuwin32.sourceforge.net/
// Downloads, Packages, etc... 
// The advantages of the GNU-Win32 toolset is that it doesn't replace your std
// Win32 shell or some call it DOS shell. GNU-Win32 runs natively - it is just
// a set of tools. This also means it can run in environments like Norton's
// File Tool or in the Slickedit Build Windows (a Virtual Win32 Shell) 
// Suggest heavily getting http://gnuwin32.sourceforge.net/
// Before the shellexec to find where lint-nt.exe or flint is located - this
// program will check for the existance of an environment variable called 
// LINTMSGPATH which can include the path or the name of the file as well.
// The file must have an extension however as in lintmsg2.txt or as the case
// with glint(gimpel lint) msg.txt - otherwise won't be able a distinguish a 
// path vs a filename within the environment variable.
// If the program can't find msg.txt or its equivalent through env or shellexec
// it will return an error code > 0 - say 1, 2, 3 etc... and also output that
// it couldn't find the lint msg.txt or the file named by the env var or it 
// couldn't find the number to output the text.
// Anytime the word Lint is used this means Gimpel Lint - in particular
// Gimpel PC-Lint or flint (lint-nt.exe and flint respectively.

// To make use of glintlookup.exe even easier under Windows - the following 
// package already exists to upgrade your Windows box.
// just xcopy the following into your bin directory or a directory you can
// add to your path
// see 
// [/asi/project/pmbuild-tools/windows/win32-unix] $ tree -d
//|-- bin
//`-- usr
//    `-- local
//        |-- include
//        |-- lib
//        |-- share
//        `-- wbin
// wbin has most of your unix utils that run natively in a Win32 shell.
// bin also should be added to your path.
// glintlookup.exe needs which.exe from wbin in particular to work.
// otherwise the system(cmdstr) will fail.
// 
// Whats interesting is that the msg.txt on Linux flint at version 9.00d 2009
// is identical to the version on lint-nt Windows which is version 9.00f 2010
// There is a patch I have to get to resolve - in the Actions Folder of outlook.

#include <sstream> 
#include <string>	//lint !e537 !e451
#include <fstream>
#include <iostream>
#include <cassert>
#include <cstdlib>	//lint !e537 !e451
#include <cerrno>

enum UserError {BadInput = 1, LintMsgFileNotFound, LintMsgNumNotFound};

// First look for the env var LINTMSGPATH which has path of msg.txt and/or 
// alternative filename included (extension required) or call system_exec()
// which flint if Unix or lint-nt.exe if Windows - must use pre-processor here
// since different builds will tell them apart - once found extract the path
// only, discard filename of flint or lint-nt.exe and from that path - attach
// msg.txt or filename and open the file for processing. 
// Once File is found return true. If false then put msg in errmsg and return 
// false

#ifndef LINTMSGPATH_ENV_VAR_NAME
#define LINTMSGPATH_ENV_VAR_NAME "LINTMSGPATH"       //lint !e1923 : const later
#endif
static const char * const LintMsgPathEnvVar = LINTMSGPATH_ENV_VAR_NAME;
static const char * const StdLintMsgFilename = "msg.txt"; 

// 'which' depends on them having lint-nt.exe or flint in their path. If they
// do not then I cannot very well search their entire disk. They can use the
// environment variable approach on Windows and Unix to get around this. Looking
// up the lint path via the environment variable is always tried first. 
// And they can copy the file and make their own changes to it if they want
// and supply the file name as part of the env var as long as it has an
// extension. Otherwise I'll expect lint-nt.exe or flint in the path and will
// use the standard msg.txt file as the one to look up user supplied msg numbers
static const char * const LintExecutablePathTmpFilename = "lintexecpath.txt";
#ifdef _WIN32
static const char * const LintExecutable = "lint-nt.exe";
#else	// Unix - Linux
static const char * const LintExecutable = "flint";
#endif

bool findLintmsgFile(std::ifstream& file, std::string& filepath)
{
	// Note: The LINT env var is used to supply args automatically invoking
	// Gimpel Lint so I cant use that as an environment name.
	const char * const filename = getenv(LintMsgPathEnvVar);
	if (filename) {
		filepath = filename;
        // Assume they didn't put . in the path anywhere other than file ext
        if (filepath.find(".") == std::string::npos) {
            if (filepath[filepath.length() - 1] != '/') {
                filepath += "/";    // should work on Windows also.
            }
            filepath += StdLintMsgFilename;
        }
	}
	else {
		// Maybe I hard code the paths for now but I could issue the cmd
		// to put the output in a tmp file - read that and get the path
		// that way... 
	
        // find file through which[.exe] if which.exe exists on Windows
		// or which on Unix, using shell exec. Get Path only and from there
		// append standard lint msg file name "msg.txt"
		std::string cmd("which "); 
		cmd += LintExecutable;
		cmd += ">";
		cmd += LintExecutablePathTmpFilename;
		
		// execute which (flint|lint-nt.exe)>lintexecpath.txt
		// read tmp file and get path
		int stat = system(cmd.c_str());
		if (stat == -1) {
			std::cout << "system(" << cmd << ") returned error == -1"
					  << ", errno = " << errno << std::endl;
			exit(stat); 	// bail out... 
		}
		// extract the dir path only.
		std::string path;
		std::ifstream tempfile(LintExecutablePathTmpFilename);
        assert(tempfile);		//lint !e725 !e1776 : lint positive indent
		tempfile >> path;
		tempfile.close();
		assert(!path.empty());	//lint !e1776
        if (path[path.length() - 1] == '\r') //lint !e830 !e875 : lint is out to lunch
            path = path.substr(0, path.length() - 1); 

		// extract directory only with directory separator as last character
		const std::size_t lastdirsep = path.find_last_of("\\/");  //lint !e539
		assert(lastdirsep != std::string::npos);   //lint !e1776
		filepath = path.substr(0, lastdirsep + 1);  // + 1 retain dir separator
        filepath += StdLintMsgFilename;             //lint !e725
		stat = std::remove(LintExecutablePathTmpFilename); 
		if (stat == -1) {
			std::cout << "remove(" << path << ") returned error == -1"
					  << ", errno = " << errno << std::endl;
			exit(stat); 	// bail out... 
		}	
	}

	// cout << "Opening File " << filepath << " for reading" << std::endl;
	file.open(filepath.c_str(), std::ios::binary);	//lint !e641 : bcc55 issue
	if (!file) {
		std::cout << "file found in env var: " << LintMsgPathEnvVar
					  << "; filename: " << filepath 
					  << " cannot be opened, check privileges" << std::endl;
		return false;
	}
	
	// found file - job completed. 
	return true;
}

void print(std::ifstream& file)
{
	std::string line;
	while (std::getline(file, line)) {
		if (line[line.length() - 1] == '\r') {
			line = line.substr(0, line.length() - 1);
		}
		std::cout << line << std::endl;
	}
}


bool printLintmsg(std::ifstream& file, int msgnum)
{
	assert(file && msgnum > 0);	//lint !e1776
	
	// How big is the file ? - I could find out and rewind but why does that
	// really matter all that much - may be of some interest but print out?
	// not the point here.
	bool found_msgnum = false;
	std::string line; 
	while (std::getline(file, line)) {
		std::istringstream iss(line);
		int n = 0; 
		iss >> n;	// found in first column 
		if (n == msgnum) {
			found_msgnum = true;
			std::cout << "\n" << line << std::endl;
			while (std::getline(file, line)) {
				#if 0
				// This is like a brief option  --- 
				// Stop at carriage return or new line in first column.
				const char c = line[0];
				if (c == '\r' || c == '\n')
					return found_msgnum;
				#else
				// Stop at next lint number or end of file.
				// I have to filter out cr, lf and blanks to get a number
				// and check it. iss > n for blanks, etc is a big number
				const char c = line[0];
				if (c != '\r' && c != '\n' && c != ' ') {
					iss >> n;
					if (n != 0)  	{// valid number found ..  
						//std::cout << "n = " << n << std::endl;
						return found_msgnum; 
					}
				}

				#endif
				// strip CR on Windows or possibly Linux
				if (line[line.length() - 1] == '\r')
					line = line.substr(0, line.length() - 1);
				std::cout << line << std::endl;	
			}
		}
	}
	if (!found_msgnum) {
		std::cout << "Lint msg number " << msgnum << " not found!" << std::endl;
	}
	return found_msgnum;
}



void help() 
{
	std::cout << "\nYou can set an env var " << LintMsgPathEnvVar; 
	std::cout << "=LintPath[/lintmsgfile.ext] " << std::endl;
	std::cout << "env var can indicate where lint message file is located\n";
	std::cout << "This env var can be path and/or filename if ext included\n";
	std::cout << "\nUsage:\n";
	std::cout << "glintlookup [#lintMsgNumber]" << std::endl;
	std::cout << "glintlookup w/ no arguments prints all of lint msg file\n";
}

void help(const UserError e, const std::string& errmsg) 
{
	if (e == BadInput) {
		std::cout << "Bad Input - see -help | --help | /?" << std::endl;		
	}
	std::cout << errmsg << std::endl;
	help();
}

int main(int argc, char* argv[])	//lint !e952 !e830
{
	std::string errmsg, filepath;
	//  enum Request {PrintLintMsgFile, LookupLintMsg} op = PrintLintMsgFile;
	std::string arg;
	if (argc > 1) {
		arg = argv[1];
	}
	if (arg == "-help" || arg == "--help" || arg == "/?") {
		help();
		return 0;
	}
	
	// First must locate where the file is - default or otherwise
	std::ifstream lintMsgFile;
	// Need name of the file returned also.
	bool found = findLintmsgFile(lintMsgFile, filepath);
	if (!found) {
		errmsg = std::string("File ") + filepath + std::string(" Not Found");
		help(LintMsgFileNotFound, errmsg);
		return int(LintMsgFileNotFound);
	}
	
	// no args means print out file - what else would it be
	// although I could make this an argument requirement but this seems 
	// convenient if the user wants to grep the output and its intuitive.
	if (arg.empty()) {
		print(lintMsgFile);
		return 0;
	}
	// too many arguments - I could ignore but why not indicate that they're 
	// not necessary. One argument is max and must be a number. 
	else if (argc > 2) {
		help(BadInput, std::string("Too many arguments provided on cmd line"));
		return int(BadInput);
	}
	// 1 argument and expecting it to be a lint error|warn|info msg number. 
	// Lookup number in file and display the text summary describing msg number.
	else if (argc == 2) {
		// Expecting a Gimpel Lint error|warning|info number as the argument
		// There are no 0 or negative error|warn|info numbers in Gimpel Lint
		// If user argv[0] was non-numeric, ascii input, or neg or 0 then error
		std::istringstream iss(arg);
		int msgnum = 0;
		iss >> msgnum;
		if (msgnum <= 0) {
			errmsg = "Arg was 0, negative or non-number";
			help(BadInput, errmsg);
			return int(BadInput);
		}
		
		// Look for msg number - if found - print out msg correspondance
		// if its not found - then print error message and exit program w/ error
		found = printLintmsg(lintMsgFile, msgnum);
		if (!found) {
			std::ostringstream oss;
			oss << "Msg Number " << msgnum
				<< " was not found in lint msg file: "
				<< filepath;
			errmsg = oss.str();
			help(LintMsgNumNotFound, errmsg);
			return int(LintMsgNumNotFound);
		}
		std::cout << "\nSee Lint Msg File: " << filepath;
		std::cout << " for more information!" << std::endl;
	}

	// successful
	return 0;
}   //lint !e818 !e952
 

 

//lint -e766    : gcc cassert not used ? used all over, lint is off here!
