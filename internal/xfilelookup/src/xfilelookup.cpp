// File: xfilelookup.cpp
// Purpose: given a token from the command line - lookup the token in the 
// associated input file. Note that this can only accept 1 file at a time for
// example and not multiple to keep the return code simple although could
// associated a count with the error code on found modules.
// There is no requirement for the search file to be sorted.

#include <string>
#include <iostream>
#include <fstream>

////////////////////////////////////////////////////////////////////////////////

void help(const std::string& program)
{
    std::string program_name(program);
    const std::size_t n = program_name.find_last_of("/\\");
    if (n != std::string::npos) {
        int extent = program_name.size() - n;
        const std::size_t pos = program_name.find_last_of(".");
        if (pos != std::string::npos)
            extent = pos - n - 1;
        program_name = program_name.substr(n + 1, extent);
    }

    std::cout << "usage:" << program_name << " recordfile lookupstring"
              << std::endl;
}

int file_length(std::ifstream& in)
{
    in.seekg(0, std::ios::end);
    const int len = in.tellg();
    in.seekg(0, std::ios::beg);
    return len;
}

const int RecFileIndex = 1, SearchTokenIndex = 2, MaxTokenLen = 1024;

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
{
    // 2 arguments required - 
    if (argc < 3)  {
        help(argv[0]);
        return -1;
    }

    std::string recordfile(argv[RecFileIndex]);
    std::ifstream in(recordfile.c_str());

    if (!in) {
        std::cout << "input record file " << recordfile << " not found!\n";
        return 2;
    }

    if (file_length(in) == 0) {
        std::cout << "input record file " << recordfile << " is 0 length!\n";
        return 3;
    }

    // Process each record in file and compare strings - they must match
    // exactly. 
    // Guard against a Windows text file being used on Linux
    // assert(line.find("\r", 0) == std::string::npos); is okay but 
    // process even Windows crlf files on Linux 
    // Linux will strip \n and Windows strip crlf when processing their 
    // native text file formats
    std::string line, search_token(argv[SearchTokenIndex]);
    for (int lineno = 1; std::getline(in, line); ++lineno) {
        const int N = line.length();
        if (N > MaxTokenLen) {
            std::cout << "Input file line length = " << N 
                      << " is suspcioiusly large\n";
            std::cout << "MaxTokenLen = " << MaxTokenLen << std::endl;
            return 4;
        }

        if (line[N - 1] == '\r') 
            line = line.substr(0, N - 1);

        // If the input file has duplicates then first one found is displayed.
        // Its expected input file has been sanitized prior to use. 
        // Not expecting duplicates but if there - no impact on use of this util
        // exact match is required.
        if (line == search_token) {
            std::cout << "\tsearch token \"" << search_token << "\" found!\n"
                      << "\tsee " << recordfile << " at line number: " << lineno 
                      << std::endl;
            return 1;
        }
    }

    // no match found - normally this is a good case.
    // script or makefile would ideally use not operator ! to invert meaning
    return 0;
} // end main
     


