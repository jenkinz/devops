/// File: testreadwritable.cpp
/// Author: Andrew Scheurer
/// Purpose: ./testreadwritable /<dirpath> shows if write access permitted on 
///          Windows or Linux - same syntax (note: using / on Windows)
/// Note: I could have hacked up a solution using a temporary file attemp but  
///       the access(..) function provided the right functionality & evidently 
///       is sufficiently portable to Windows builds and works w/ path sep '/'
///       The stat(path, &buf) is ANSI C and does provide permission
///       bits but its context is insufficient to determine if the user running
///       this process has write access. It could work but there is a lot more 
///       code involved in extracting the users permission setting and comparing
///       them. The access(const char* path, int mode) function for files and 
///       directories is much easier to work with as an implementation mechanism

#include <iostream>
#include <string>

#ifdef _WIN32
// Use either Borland C++ or GNU g++ for Windows or clang. 
// Visual Studio 9 does not have same result as GNU or Borland for Windows.
// Its unlikely it alone is correct and the other compilers wrong. 
// It sees no write access denial for directories marked with  attrib +r dir - 
// it always says okay - recognizing no read-only directories.
// The shell does same - but I want to keep at least the functionality to the 
// user so they can protect a directory if they so choose even if WinXP does not
// enforce it - this tool will support their option to mark dir as read-only 
// and no write access for putting files in that read-only directory.
#ifdef _MSC_VER
#error "Microsoft C does not correctly identify readonly dirs as non-write"
#endif
#include <io.h>         // access(const char* path, const int mode)
#ifndef W_OK
#define W_OK 2          // not defined w/ some Win32 compilers, Linux/Win val=2
#endif                    
#else
#include <unistd.h>     // access(const char* path, const int mode), W_OK 
#endif

int main(int argc, char* argv[])
{
    if (argc != 2) {
        std::cout << "error: supply 1 dir or filename path arg" << std::endl;
        return 1;
    }

    // Supply path as /<path> on Windows or Linux: eg. /opt works on either
    // platform. Do not use \ or rather \\ - not required and not portable here
    // I don't need to transform / to \\ on Windows with Borland and GNU 
    // compiler. Microsoft C++ compiler has a bug or inconsistency
    const std::string path(argv[1]);
    const int mode = W_OK;
    const int rc =  access(path.c_str(), mode);
    if (rc != 0) {
        std::cout << "error: user has no write permission to " 
                  << path << std::endl;
        return 2;   // make sees bad shell error code
    }

    // Write access verified - user has write access to directory path
    // make will see good return value and keep going.
    // no news is good news and no ouput is equivalent to a return 0 good exit
    // code
    return 0;

} // end main

