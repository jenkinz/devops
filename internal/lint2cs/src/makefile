# The tests may not compile on MacOS or with GNU make
# I need the -- and/or ~ to ignore the exit status from lint where even -e*
# will cause lint to return an exit status +1 - even if all source files had
# no errors. the -e* isn't liked by lint - can we suppress?

V = @
CC = g++
LD = $(CC)
DEBUG = -g
CFLAGS = $(DEBUG) -pedantic -Wall
LDFLAGS = $(DEBUG)

O = o
E = # .exe # for Windows

SRCS = lint2jsf.cpp
OBJS = $(SRCS:.cpp=.$O)


# Note: if supporting portability to Windows - define E = .exe and 
# append $E to lint2jsf where E = <nothing> on Linux.
# Windows build would never find lint2jsf.exe or lint2jsf$E unless you tell the
# right name of the image produced by the compiler, here GNU could run on 
# Windows
#IMAGE = lint2jsf$E
IMAGE = lint2cs$E

$(IMAGE) : $(OBJS)
	$(V)echo "Linking $@"
	$(LD) $(LDFLAGS) -o $@ $^
	$(V)echo 

.cpp.$O : 
	$(V)echo "Compiling $<"
	$(CC) $(CFLAGS) -c $<
	$(V)echo 

clean :
	rm $(OBJS) $(IMAGE)

# Test battery!
# see also ~ and ~~ as supporting combinations of behaviors with - and -- with
# opus make - they differ in important ways - ~ and - are not the same thing.
test0 : $(IMAGE)
	--lint-nt -b -e* -w3 +e1506 +e1509 jsf-test.cpp > tmpfile.tmp
#	-lint2jsf.exe  tmpfile.tmp  testlint2jsf1.txt
	-lint2cs.exe  tmpfile.tmp  testlint2jsf1.txt
	rm tmpfile.tmp

test1 : $(IMAGE)
	--lint-nt -b -e* -w3 +e1506 +e1509 nolinterrors.cpp > tmpfile.tmp
#	-lint2jsf.exe  tmpfile.tmp  testlint2jsf1.txt
	-lint2cs.exe  tmpfile.tmp  testlint2jsf1.txt
	rm tmpfile.tmp

# '~ 'ignore exit status but do not set exit status to lint2jsf unlike '-'
# here that doesn't matter but it could if lint-nt or flint is the exit status
# problem - lint-nt or flint sees -e* and increments return status +1 - it does
# not like -e*. Is there a way to suppress that behavior? Ask lint support folks
test2 : $(IMAGE)
	--lint-nt -b -e* -w3 +e1506 +e1509 jsf-test.cpp nolinterrors.cpp > tmpfile.tmp
#	~lint2jsf.exe  tmpfile.tmp  testlint2jsf2.txt
	~lint2cs.exe  tmpfile.tmp  testlint2jsf2.txt
	rm tmpfile.tmp

test3 : $(IMAGE)
	--lint-nt -b -e* -w3 +e1506 +e1509 nolinterrors.cpp nolinterrors2.cpp > tmpfile.tmp
#	-lint2jsf.exe  tmpfile.tmp  testlint2jsf2.txt
	-lint2cs.exe  tmpfile.tmp  testlint2jsf2.txt
	rm tmpfile.tmp

test4 : $(IMAGE)
	--lint-nt -b -e* -w3 +e1506 +e1509 jsf-test.cpp nolinterrors.cpp nolinterrors2.cpp > tmpfile.tmp
#	-lint2jsf.exe  tmpfile.tmp  testlint2jsf2.txt
	-lint2cs.exe  tmpfile.tmp  testlint2jsf2.txt
	rm tmpfile.tmp


tests : test0 test1 test2 test3 test4
	%echo '$@ up to date!'

$(OBJS) : makefile

