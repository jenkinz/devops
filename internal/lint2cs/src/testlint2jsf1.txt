1506 A class's virtual functions shall not be invoked from its destructor or any of its constructors (JSF(71.1))
1509 All base classes with a virtual function shall define a virtual destructor (JSF(78))
