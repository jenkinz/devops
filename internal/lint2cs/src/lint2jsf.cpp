// File: lint2jsf.cpp
// Version 1

#include <iostream>
#include <map>
#include <vector>
#include <set>
#include <algorithm>
#include <string>
#include <sstream>
#include <fstream>
#include <cassert>
#include <cstring>

// Table set up and validation
#define JSF_STR_P1 "(JSF"
#define JSF_STR_P2 ")"

// key is not listed first - to allow concatenation to be natural and support
// use of macro helpers used to simplify array initialization.
struct Lookup_txlat_table
{
    const char * msg;
    const char * appended_msg;
    unsigned key;
};


// Map JSF code to Gimpel Lint error|warning/info code
#define MK_JSF_CODESTR(CODE)    " " JSF_STR_P1 #CODE JSF_STR_P2 
#define JSF_PAIR(CODE, LINTCODE) MK_JSF_CODESTR(CODE), LINTCODE 

// These are just examples
#define JSF_MSG_71_1    {   "A class's virtual functions shall not be invoked" \
                            " from its destructor or any of its constructors", \
                            JSF_PAIR(71.1, 1506) \
                        }

#define JSF_MSG_78      {   "All base classes with a virtual function shall" \
                            " define a virtual destructor", \
                            JSF_PAIR(78, 1509) \
                        }

// Note: This could be const and defined in another file or static in another
// file with accessors to support returning pointer to the table and its 
// length or size. 
static const Lookup_txlat_table the_txlat_dictionary[] =
{ 
    JSF_MSG_71_1,
    JSF_MSG_78 // , add more by creating macro template above - boilerplate.
};

// number of rows or elements in array
static const std::size_t the_txlat_dictionary_numelts = 
                sizeof(the_txlat_dictionary) / sizeof(the_txlat_dictionary[0]); 


// Uncomment
// #define ZERO_EXIT_RETURN_STATUS
// 
// or Compile with -DZERO_EXIT_RETURN_STATUS to never return a 
// non-zero exit status
#ifdef ZERO_EXIT_RETURN_STATUS
static const int MaxReturnStatus = 0; 
#else
static const int MaxReturnStatus = 255; // Linux max - make Windows policy same
#endif

// Types
typedef std::map<unsigned, std::string> MsgTxlatDictionary;

// Functions
namespace {
void print_table(void);
void usage(std::string& program_name);
void create(MsgTxlatDictionary& mtd, std::ifstream& in);
void create(MsgTxlatDictionary& mtd);
int display_txlat(const std::vector<std::string>& recs,
                   const MsgTxlatDictionary& mtd,
                   const std::vector<std::string>& modulenames,
                   std::ostream& os = std::cout);

bool display_txlat_if_found(const std::string& filename, 
                            const std::string& linenumber, 
                            const unsigned code, 
                            const MsgTxlatDictionary& mtd,
                            std::ostream& os = std::cout);
} // end anonymous namespace

////////////////////////////////////////////////////////////////////////////////

// Generic solution for any integral number - base 10
template <class T>
unsigned num_digits(T number)
{
    unsigned digits = 0;
    while (number) {
        number /= 10;
        digits++;
    }
    return digits;
}

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char * argv[])
{                                                           
    if (argc == 1) {
        std::cout << "\nError: input file required for message translation\n\n";
        // print_table(); diagnostic to see internal tables.
        std::string prog(argv[0]);
        usage(prog);
        return 1;               // usage error w/ shell bad error exit status
    }

    // file to be used in translation.
    std::string inputfilename(argv[1]);
    std::ifstream inputfile(inputfilename.c_str());
    if (!inputfile) {
        std::cout << "inputfile: " << inputfilename << " not found!\n";
        return 2; 
    }

    MsgTxlatDictionary mtd;

    // file to be used as translator - may not exist.
    // if it does not exist - use internal table
    if (argc > 2) {
        std::string xlatfilename(argv[2]);
        std::ifstream xlatfile(xlatfilename.c_str());
        if (!xlatfile) {
            std::cout << "xlatfile: " << xlatfilename << " not found!\n";
            return 3;           // file not found w/ shell bad error exit status
        }
        // create dictionary from user specified xlatfile
        create(mtd, xlatfile);
    }
    else 
        create(mtd);

    // read in error input file for translation - parse contents for elts in set    
    // Not copying in token by token but record by record - so the following 
    // code will not work - anticipating records qualified by source filename
    // only. The following does not work for this problem:
    // std::istream_iterator<std::string> file_iter(inputfile), end_of_stream;
    // std::copy(file_iter, end_of_stream, std::back_inserter(records));
    std::vector<std::string> records, modulenames; 
    std::string record;
    while (std::getline(inputfile, record)) {
        const std::size_t len = record.length();
        if (!len || record[len - 1] == '\r' || record[len - 1] == '\n')
            continue;
         
        // filter out records if first token isn't a valid C/C++ filename
        std::istringstream iss(record);
        std::string token;
        iss >> token;

        // Looking for pattern: --- Module:   simple.c
        // lint will indicate the module under consideration after a blank 
        // record preceeded this record pattern. This is always the case and a
        // key invariant of lint message formatting of a given module
        // Note: Multiple modules may be passed to lint or just one.
        // insert into the vector an ordered set of modules to report passed
        // for each module in case vector is empty - all passed.
        // lint can only analyze C or C++ so .c -> .c* is a common extension
        if (token == "---") {
            iss >> token;               // get next token
            if (token == "Module:") {
                iss >> token;           // get next token - must be src filename
                assert(token.find(".c") != std::string::npos);
                modulenames.push_back(token);
                continue;                   // skip to next record - special case 
            }
        }

        // Filter on first token being a filename with valid extensions only.
        // All other file extensions are banned - such as .C and .H used in
        // DFCS - I'd expect that the extensions are corrected before running
        // this translator or txlat tool
        // .c implies .cpp, .cxx, and .c and conversely .h implies .hpp, .hxx
        // .inl is sometimes used for inline include files. The error reported
        // could be a source or header file.
        const bool found1 = (token.find(".c") != std::string::npos);
        const bool found2 = (token.find(".h") != std::string::npos);
        const bool found3 = (token.find(".inl") != std::string::npos);
        if (found1 || found2 || found3) 
            records.push_back(record); 
    }

    if (records.empty()) {
        const std::size_t n = modulenames.size();
        if (!n) {
            std::cout << "Error: No module names found to display as passed!\n";
            std::cout << "=> Was lint not invoked correctly?" << std::endl;
            std::cout << "\tExample: "
                      << "Error 305: Unable to see any linted C/C++ modules\n"
                      << "\tThis would result in no passed modules"<< std::endl;
            return 4;       // must be file input error - bad exit status.
        }

        // no news is good news - this may mean no problems
        // print out that 'all modules' passed - none affected.
        #if 0
        for (unsigned i = 0; i < n; ++i)  
            std::cout << "module: " 
                      << modulenames[i] << " passed!" << std::endl; 
        #endif

        // return success error code - no errors found
        return 0; 
    }

    // For given set of valid vector entries and map pairs - evaluate and
    // bridge or translate the messages from format A to format B.
    const int status = display_txlat(records, mtd, modulenames);

    // status could be an unsigned int but main requires a return of int
    // it represents a count so 0 .. N is the range
    // Unix represents return values of 0..255, Windows greater but support
    // least common denominator - 255 is the max count and besides if there
    // are that many problems - a higher count even if possible isn't going to
    // matter that much. Opus make or GNU make will return range of 0..255 only
    assert(status >= 0);
    return status > MaxReturnStatus ? MaxReturnStatus : status;

// Uncomment if zero exit status required.
//    return 0; if lint to be used for error status instead of lint2jsf return
//    Note: however that -e* suppression causes lint to issue an error code +1
          
} // end main

////////////////////////////////////////////////////////////////////////////////

namespace {

void print_table(void)
{
    std::cout << "Internal Table dump ...\n\n";
    for (unsigned i = 0; i < the_txlat_dictionary_numelts; ++i) {
        std::cout << "key: " << the_txlat_dictionary[i].key << "\n"
                  << "msg: " << the_txlat_dictionary[i].msg
                  << the_txlat_dictionary[i].appended_msg << "\n\n";
    }

}

////////////////////////////////////////////////////////////////////////////////

void usage(std::string& program_name)
{
    std::cout << "options: lint-input-file [msg txlat file]" << std::endl;
    std::cout << "if no msg txlat file is given - "
              << " internal tables are used" << std::endl;
    std::size_t pos = program_name.find_last_of("\\/");
    if (pos != std::string::npos) 
        program_name = program_name.substr(pos + 1); 
    std::cout << "\nExample: " << program_name << " lint-ouput.txt [txlat file]";
    std::cout << std::endl;
}

////////////////////////////////////////////////////////////////////////////////

void create(MsgTxlatDictionary& mtd)
{
  // pre-condition - map is new, empty
  assert(mtd.empty());

  // insert all elements into map for later lookup
  // side effect is a complete in memory dictionary
  for (unsigned i = 0; i < the_txlat_dictionary_numelts; ++i) {
      std::string complete_msg(the_txlat_dictionary[i].msg); 
      complete_msg += the_txlat_dictionary[i].appended_msg; 
      mtd[the_txlat_dictionary[i].key] = complete_msg; 
  }

} // end create

////////////////////////////////////////////////////////////////////////////////

void create(MsgTxlatDictionary& mtd, std::ifstream& in)
{
    // pre-condition - map is new, empty
    assert(mtd.empty());

    std::string text;
    unsigned record_process_count = 0, key = 0;
    while (std::getline(in, text))
    {
        // ignore empty line or spurious carriage return or line feed 
        // or a comment char '#' in first column. I'll support comments in a 
        // user supplied txlat file.
        if (text.empty() || text[0] == '\r' || text[0] == '\n' || 
            text[0] == '#')
            continue;

        // spaces are skipped in extraction. 
        std::istringstream iss(text);
        iss >> key;
        assert(key > 0);

        // Advance past the lint e-code number and space and set association to 
        // this key as the JSF coding rule 
        const unsigned numlen = num_digits(key);
        const std::string msg = text.substr(numlen + 1); // +1 for space 0x20

        // Create association between the lint e-code and the JSF message
        // association allows for translation between lint e-code and JSF rule
        // #'s and text rationale. 
        mtd[key] = msg; 

        // reset key for next record extraction and indicate that some records
        // were processed. 
        key = 0;
        ++record_process_count;
    }

    // If there are no JSF rules dictionary data records to be processed then 
    // why am I here in this function. There must be at least 1
    // lint jsf rule cross index for this program to make any sense. This is in
    // prep for lint processing only. A cross reference file or internal table
    // must have entries - this is a key invariant of the conops of this utility
    assert(record_process_count > 0);

} // end create

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// Format of lint output diagnostic files has the following form:
// simple.c 5 error 529: ...etc -- default
// simple.c 5 error 529: ...etc -- env-sled.lnt slickedit fmt option
// I will assume this format is invariant with respect to position.
// If position can not be relied upon then will have to recode
// filenames_passed_set represents a multiple file list of modules given to 
// lint and where lint provides a -- Module identifer for those files but no
// warnings were given for the e### rules provided. These modules represent a 
// pass condition where those that are cross referenced represent an implicit 
// fail condition.
////////////////////////////////////////////////////////////////////////////////
int display_txlat(const std::vector<std::string>& recs,
                   const MsgTxlatDictionary& mtd, 
                   const std::vector<std::string>& modulenames,
                   std::ostream& os)
{
    // pre-condition - vector and map must have data
    assert(!recs.empty());
    assert(!mtd.empty());

    //os << std::endl;

    int found_count = 0;
    std::set<std::string> files_not_passed_set;  // error files.
    enum {FilenameToken = 0, LinenumberToken = 1, CodeToken = 3};
    const unsigned numrecs = recs.size();
    for (unsigned i = 0; i < numrecs; ++i) {
        const std::string& record = recs[i];
        std::istringstream iss(record);
        std::string token, filename, linenumber, e_code;
        for (unsigned j = 0; iss >> token; ++j) {
            switch (j) {
                case FilenameToken:
                    filename = token;
                    break;

                case LinenumberToken:
                    linenumber = token;
                    break;

                case CodeToken:
                    // subtract off the : on the code - e.g. token 123: to 123
                    e_code = token.substr(0, token.size() - 1);
                    break;

                default:
                    break;
            }

            // logical equivalence law: ~a ^ ~b <==> ~(a v b)
            // that is the 'complete' bool expression equivalent to:
            //    !filename.empty() && !linenumber.empty() && !code.empty()
            const bool complete = !(filename.empty() || 
                                    linenumber.empty() || 
                                    e_code.empty()); 
            if (complete)
                break; 
                                         
        } // end inner for                                           

        // convert code to value in N: natural numbers and subject to constraint 
        // of lint msg.txt property where all lint codes are greater than 0
        // open/closed set so loosely: 0 < e_code <= M where M is the max error   
        // code in lint msg.txt that mirrors lint manual listed error codes.
        // Note: the e: error code may not be guaranteed continous - there
        // may be gaps so the set of error codes in N: natural number set
        // is not inductive or always incrementing by 1.
        // lint refers to e_code as -+eN where N is the value of the e_code
        std::istringstream iss2(e_code);
        unsigned numeric_code = 0;
        iss2 >> numeric_code;
        assert(numeric_code > 0);  // lint msg.txt codes always start with 1

        assert(!filename.empty());
        assert(!linenumber.empty());
        assert(!e_code.empty());

        // Lint may have errors but only interested in those that match the
        // JSF rules by cross reference. If a match - increment error count
        // if not a match then skip insertion into error set - there was no
        // lint e-code to jsf code match so no error from the lint2jsf program
        // perspective.
        const bool found = display_txlat_if_found(filename, linenumber, 
                                                  numeric_code, mtd, os); 
        if (found) 
            ++found_count;  // count errors and return as status, 0 is no errors
        else 
            continue;       // skip processing below that adds to error set.
                            // error set equivalent to files not passed set.

        // Note: filename error string could be an include file - I'm only
        // interested in flagging modules as not passed - not include files.
        // set up container for set difference after all modules processed.
        // The set insertion operation checks whether each inserted element 
        // is equivalent to an element already in the container, and if so, 
        // the element is not inserted returning iterator to that already
        // existant iterator value. All this means is that I don't need
        // a guard - it may address efficiency to have a guard but I don't 
        // need it here - set allows no duplicate keys or value - in a 
        // std::set keys are same as value - they are 1-1
        std::vector<std::string>::const_iterator iter = 
                                  std::find(modulenames.begin(), 
                                            modulenames.end(),
                                            filename);
        if (iter != modulenames.end()) 
            files_not_passed_set.insert(*iter);    // found: insert w/ no dups

    } // end outer for - iterating all records

    // Iterate set of modules with lint messages and infer those outside
    // the set have a passed status. Essentially doing a set difference
    // here - finding members NOT in the error set but registered as 
    // modules via lint's -- MODULE <filename> extraction.
    // List passed modules after all processing is completed.
    // a module name with no set membership means it passed lint JSF rules.
    const std::size_t setsize = files_not_passed_set.size();
    for (unsigned i = 0; (setsize > 0) && (i < modulenames.size()); ++i) {
        //if (i == 0 && found_count) 
            //std::cout << "\n";
        const std::string& str = modulenames[i];
        std::set<std::string>::const_iterator iter = 
                               files_not_passed_set.find(str);
        // not found implies module passed.
        #if 0
        if (iter == files_not_passed_set.end()) 
            std::cout << "module: " 
                      << modulenames[i] << " passed!" << std::endl;
        #endif
    }

    return found_count;

} // end display_txlat

////////////////////////////////////////////////////////////////////////////////

bool display_txlat_if_found(const std::string& filename, 
                            const std::string& linenumber, 
                            const unsigned code, 
                            const MsgTxlatDictionary& mtd,
                            std::ostream& os)
{
    MsgTxlatDictionary::const_iterator iter =  mtd.find(code);

    // Find element - if found, translate to other message format
    // If not found - then ignore or if debug print error message
    // I may not find anything.
    if (iter != mtd.end()) {
        const std::string msgtext(iter->second);

        // For all jsf messages to be substituted for lint messages 1-1
        // There must exist (JSF#) where # is a JSF ordinal number - 71.1, 78 
        // For example: (JSF71.1) or (JSF78) are expected at the end of the 
        // message:
        // On std::string manip - see the following url's.
        // http://www.cplusplus.com/reference/string/string/
        // substr => http://www.cplusplus.com/reference/string/string/substr/
        const std::size_t n = msgtext.find(JSF_STR_P1);
        assert(n != std::string::npos);

        const std::size_t m = msgtext.find_last_of(JSF_STR_P2);
        assert(m != std::string::npos);

        // Print out the lint->codecheck adaptor messages
        // adapted format to that of JSF codecheck per above.
        os << filename 
           << "(" << linenumber << "): " 
           << msgtext << std::endl;

        // error translation found
        return true;
    }

    // error translation not found
    return false;
}

} // end anonymous namespace


