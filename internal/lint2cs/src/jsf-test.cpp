
// Demonstrate translation from lint JSF rules detection to a specific JSF rule
// 
// Objective:
// Intentionally trigger a JSF rule to demonstrate translation with code below.
//
// Summary:
// The following are the respective test rules detected by lint and JSF.
// Using Lint as a bridge for rules that are difficult (but not impossible) to 
// detect and report with codecheck, the following lint rules are translated to
// JSF rules. Some makefile integration is required but only deferring status
// error code trigger until after the filter has taken place.
// 
// The following are the objective JSF rules:
// 
////////////////////////////////////////////////////////////////////////////////
/// AV Rule 71.1
///     A class's virtual functions shall not be invoked from its destructor or 
///     any of its constructors.
/// 
///     Rationale: A class�s virtual functions are resolved statically 
///                (not dynamically) in its constructors and destructor. 
///                See AV Rule 71.1 in Appendix_A for additional details
/// 
////////////////////////////////////////////////////////////////////////////////
///
/// AV Rule 78
///     All base classes with a virtual function shall define a virtual 
///     destructor.
/// 
///     Rationale: Prevent undefined behavior. If an application attempts to 
///                delete a derived class object through a base class pointer, 
///                the result is undefined if the base class�s destructor is 
///                non-virtual.
/// 
///     Note: This rule does not imply the use of dynamic memory 
///           (allocation/deallocation from the free store) will be used. 
///           See AV Rule 206. 
///           My example: overload new/delete and where no dynamic allocation is
///                       used except at startup. Memory is managed 
///                       deterministically and not externally fragmented 
///                       memory usage is bounded - no heap exhaustion proven as 
///                       in the case of proving no stack exhaustion.
/// 
/// Lint will report these rules given the code below as a trigger.
/// 
/// *** Gimpel Lint Rules equivalent: ***
/// 
/// 1506  Call to virtual function 'Symbol' within a constructor or
///       destructor  -- A call to a virtual function was found in a
///       constructor or a destructor of a class.  If this class is
///       a base class of some other class (why else make a virtual
///       call?), then the function called is not the overriding
///       function of the derived class but rather the function
///       associated with the base class.  If you use an explicit
///       scope operator this message will not be produced.   [20 
///       section 9]
/// 
/// 
/// 1512  destructor for base class 'Symbol' (Location) is not
///      virtual  -- In a final pass through all the classes, we
///      have found a class (named in the message) that is the base
///      class of a derivation and has a destructor but the
///      destructor is not virtual.  It is conventional for
///      inherited classes to have virtual destructors so that it
///      is safe to 'delete' a pointer to a base class.   [19]
///
///-----------------------------------------------------------------------------
///  
/// Goal: Translate Lint Rule 1506 to AV Rule 71.1 with text output message
///       Translate Lint Rule 1512 to AV Rule 78 with text output message
///       I must include the filename and line number from lint and translate
///       it to a form consistent with codecheck JSF rules formatting.
////////////////////////////////////////////////////////////////////////////////

class Base 
{ 
public:
    Base() : state_(0) {}
    ~Base() { func2(); }

    virtual void func1(void) = 0;
    virtual void func2(void) { state_ = 2; }
private:
    int state_;
};

////////////////////////////////////////////////////////////////////////////////

class Derived: public Base
{
public:
    Derived();
    Derived(int state);
    ~Derived(); 

    virtual void func1(void);
private:
    // Note: Myers rule: in presence of multiple constructors a private init
    // function is advised to control maintainability of state. Duplication with
    // member initialization list is not necessary
    void init();
    int state1_, state2_;
};

////////////////////////////////////////////////////////////////////////////////

Derived::Derived()
{
    // calling virtual function from constructor before init() called
    // see above rationale on private init per Meyers rule - not using member
    // initialization list intentionally.
    func1();

    // after func1() called - intentional error
    // normally init() is always called first - always! But even if called
    // first - the rule is still good - do not call virtual functions in dtors
    // or dtors
    init();
}

////////////////////////////////////////////////////////////////////////////////

Derived::Derived(int state)
{
    // calling virtual function from constructor before init() called
    // see above rationale on private init per Meyers rule - not using member
    // initialization list intentionally.
    func1();

    // after func1() called - intentional error
    // normally init() is always called first - always! But even if called
    // first - the rule is still good - do not call virtual functions in dtors
    // or dtors
    init();
    state1_ = state;
}

////////////////////////////////////////////////////////////////////////////////

Derived::~Derived()
{
    // calling virtual function from destructor - not good.
    func2();
}

////////////////////////////////////////////////////////////////////////////////

void Derived::init()
{
    state1_ = state2_ = 0;
}

////////////////////////////////////////////////////////////////////////////////

void Derived::func1(void) 
{
    if (state1_ == 10)
        ; // good state
    else 
        ; // unknown state - object completed initialization? 
}

////////////////////////////////////////////////////////////////////////////////

int main(void)
{
    Base * const b = new Derived(1);
    b->func1();
    delete b;

    return 0;
}



