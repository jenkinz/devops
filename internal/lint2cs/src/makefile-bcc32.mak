V = @
VV = @@ # CONSIDER $V$V

%if "$(USERNAME)" == "Andrew Scheurer"
%setenv PATH=c:\bcc55\bin;$(PATH)
%endif

UPPERBOUND = 31
%if "$(OS,LC)" == "nt"   # or 'unix" - includes mac os, and linux
NUMBERSEXPR = awk "BEGIN {for(n=0;n<$(UPPERBOUND);++n) print n+1;}"
%else
NUMBERSEXPR = awk 'BEGIN {for(n=0;n<$(UPPERBOUND);++n) print n+1;}'
%endif

O = obj
E = .exe	# I use .exe because linux uses either no extension or .out
			# GA has standardized on no linux extension rather than using .out

SRCSPEC = *.c*
SRCS := $(SRCSPEC,*F)
OBJS := $(SRCS,B,>.$O)
DEBUG = -v

LINT2JSF_TXLAT =  lint2jsf$E	# change name to lint2jsf
LINT2JSF_TXLAT_FILE = $(LINT2JSF_TXLAT,B,>.txt)

LINT_EC_VIRTUAL_CALL = 1506
LINT_EC_DTOR_NON_VIRTUAL = 1509 # 1512 is in the global wrap up and not relevant


# Default target - .NODEFAULT: required for cases where includes are used.
$(LINT2JSF_TXLAT) : $(OBJS) 
	%echo Linking $@
	> bcc32 $(DEBUG) -e$@ $*.$O # $** - don't mix - has other main

test_$(LINT2JSF_TXLAT,B) .PHONY :
	%do create_jsf_lint_txlat_txt 
	%do create_linttest_log
	lint2jsf $(@,B).log > file1.out
	>lint2jsf $(@,B).log $(@,B).txt > file2.out
	diff file1.out file2.out
	cat file1.out
	rm file1.out file2.out

# One could create this as read-only and not change.
# Could eventually change to a test of %readonly and ignore any further
# action at that point - makes sense to do it that way or rely on 
# internal tables to the program and forget about using an external file.
# +e$(LINT_EC_VIRTUAL_CALL) +e$(LINT_EC_DTOR_NON_VIRTUAL)
DEBUG_OUT ?= 0
create_jsf_lint_txlat_txt .PHONY :
	%if %file($(@,B).txt) && !%writable($(@,B).txt)
	%error ... $(@,B).txt input file must not be read-only
	%endif
	%echo << $(@,B).txt
$(LINT_EC_VIRTUAL_CALL) A class's virtual functions shall not be invoked from its destructor or any of its constructors (JSF(71.1))
$(LINT_EC_DTOR_NON_VIRTUAL) All base classes with a virtual function shall define a virtual destructor (JSF(78))
	<< KEEP
	%if $(DEBUG_OUT)
	> cat $(@,B).txt
	%echo --------------
	%endif
	%echo


create_linttest_log .PHONY :
	%if %file($(@,B).log) && !%writable($(@,B).log)
	%error $(@,B).log input file must not be read-only
	%endif
	%echo << $(@,B).log
--- Module:   simple.c
               _
    int y = 1/x;
simple.c  4  Warning 101: Possible division by 0 [Reference: file simple.c:
    line 3]
simple.c  3  Info 831: Reference cited in prior message
_
}
simple.c  5  Warning 529: Symbol 'y' (line 4) not subsequently referenced
simple.c  121  Info 121: Location cited in prior message
	<< KEEP

compile .PHONY :
	mk $(tgt).$O

##compile .PHONY : $(OBJS)
#	%do test.exe

test.exe : test.obj
	bcc32 -v test.obj

.cpp.obj .c.obj : 
	%echo Compiling $<
	> bcc32 $(DEBUG) -c $<

NUMBERS = $(NUMBERSEXPR,SH)
runtests .PHONY : test.exe
	%foreach number in $(NUMBERS)
	test $(number)
	%endfor

zip .PHONY :
	zip offset-src.zip make* *.c* *.txt
	clipd \

clean .PHONY :
	--rm *.o* *.exe *.zip *.V*

# Suggestion: change lint2jsf[.exe] to testlint2jsf[.exe] 
# 			  perhaps its a better name.
# Note: I tried to make lint2jsf as generic as possible but I had to follow
#		lint formatting to keep the utility program a reasonable length
# Warning: This program is tested with the default format and env-sled.lnt
#          "IF" another format is chosen the program may have to be rewritten
#			Right now - env-sled.lnt - created by Gimpel for Slickedit and the
#			default which is the default (no argument) are supported.
# 
# Guard for Linux vs Windows - same capabilities - just packaged differently
# Windows below is what I have but %if "$(OS,LC)" == "unix" would set GLINT
# and GLINT_TEST_FLAGS1 and GLINT_TEST_FLAGS2 differently.
# See glintmetrics[.exe] on /asi/project/pmbuild-tools/xplatform
# I only support default and env-sled.lnt in that tool as well.

# Problem:
# -e* +e$(LINT_EC_VIRTUAL_CALL) +e$(LINT_EC_DTOR_NON_VIRTUAL)
# works with the first error message but does global wrap on the second
# but if I run lint without +e1506 and +e1512 it does not suppress warning on
# 1512 - which is just odd ... 
# so I suppose I can suppress this flag and let the program pick out what it
# wants - this is a concern however - I don't want to produce a ton of text
# Read Lint manual and try to figure out suppressing that Global Wrap up stuff.

GLINT = lint-nt -b
# see lint-bug-readme.txt - -e* with +e flags doesn't work - 
# except Global wrap-up stuff - the tool is complex and unpredicable - let 
# the gimpel folks have it on the format extensions - append being insufficient
# as well.
# Okay now works - trick was 1512 is wrap up and 1506 is virtual destructor 
# warning and some odd thing about not being able disable global wrap up and so
# on - very ugly - so now it works with these options - and e* nag is gone - 
# just filtered out.
# If Gimpel had a better format append feature or replace - I wouldn't have to
# to have written this code!!!

GLINT_TEST_FLAGS1 = -e* -w3 +e$(LINT_EC_VIRTUAL_CALL) +e$(LINT_EC_DTOR_NON_VIRTUAL)
#GLINT_TEST_FLAGS1 = -w3
GLINT_TEST_FLAGS2 = -Ic:\lint\lnt env-sled.lnt $(GLINT_TEST_FLAGS1)

# Todo: assess requirement for $(LINT2JSF_TXLAT) to return exit status or depend 
# on lint for that - I should probably depend on lint exit status and the 
# control I get with configuration of that tool. 
# I don't do exit code's now with $(LINT2JSF_TXLAT) - and maybe I should return 
# error code of 0 if no match - modifying the software to return an error code 
# > 0 when is match is found - thats easy to do and 0 when no match or -pass
# is provided in the absence of any problems. Review w/ Brian Jenkins to decide
# on policy!
# Note: Policy can be via a command line option but that would add more 
# complexity with argument parsing but it can be done.
# an env variable is actually very easy with getenv() to determine behavior
# on returning a non-zero error code if match found - discuss w/ Brian.
# argc, argv is simple now - 1 or 2 filenames - 1 if internal table used.
# decide what policy changes to make. I could make flag the first flag optional
# and assume a default.

# Set to 1 on cmd line or env var to turn out output of lint tool.
DEBUG_LINT_OUTPUT ?= 0

testlint2jsf1 .PHONY :
	$(VV) $(MAKE) $(LINT2JSF_TXLAT)  			# rebuild only if needed.
	$(VV) %do create_jsf_lint_txlat_txt
	--$(GLINT) $(GLINT_TEST_FLAGS1) jsf-test.cpp > $(MAKEPID).TMP
	$(VV) %set GLINT_STATUS1=$(status)
	> $(LINT2JSF_TXLAT)  $(MAKEPID).TMP  $(@,B).txt
	%echo
	%echo
	%if $(GLINT_STATUS1)
	%echo lint would have returned exit status = $(GLINT_STATUS1)
	%echo Make a policy decision on lint error code or program error code return - argv flag?
	%endif
	%if $(DEBUG_LINT_OUTPUT)
	cat $(MAKEPID).TMP
	%endif

#	rm $(MAKEPID).TMP 

testlint2jsf2 .PHONY :
	$(VV) $(MAKE) $(LINT2JSF_TXLAT)  			# rebuild only if needed.
	$(VV) %do create_jsf_lint_txlat_txt
	--$(GLINT) $(GLINT_TEST_FLAGS2) jsf-test.cpp > $(MAKEPID).TMP
	$(VV) %set GLINT_STATUS2=$(status)
	> $(LINT2JSF_TXLAT)  $(MAKEPID).TMP  $(@,B).txt
	%echo
	%echo
	%if $(GLINT_STATUS2)
	%echo lint would have returned exit status = $(GLINT_STATUS2) 
	%echo Make a policy decision on lint error code or program error code return - argv flag?
	%endif
	%if $(DEBUG_LINT_OUTPUT)
	cat $(MAKEPID).TMP
	%endif

testlint2jsf3 .PHONY :
	$(VV) $(MAKE) $(LINT2JSF_TXLAT)  			# rebuild only if needed.
	$(VV) %do create_jsf_lint_txlat_txt
	--$(GLINT) $(GLINT_TEST_FLAGS2) jsf-test.cpp nolinterrors.cpp>$(MAKEPID).TMP
	$(VV) %set GLINT_STATUS2=$(status)
	> $(LINT2JSF_TXLAT)  $(MAKEPID).TMP  $(@,B).txt
	%echo
	%echo
	%if $(GLINT_STATUS2)
	%echo lint would have returned exit status = $(GLINT_STATUS2) 
	%echo Make a policy decision on lint error code or program error code return - argv flag?
	%endif
	%if $(DEBUG_LINT_OUTPUT)
	cat $(MAKEPID).TMP
	%endif


testlint2jsf4 .PHONY :
	$(VV) $(MAKE) $(LINT2JSF_TXLAT)  			# rebuild only if needed.
	$(VV) %do create_jsf_lint_txlat_txt
	--$(GLINT) $(GLINT_TEST_FLAGS2) nolinterrors.cpp nolinterrors2.cpp > $(MAKEPID).TMP
	$(VV) %set GLINT_STATUS2=$(status)
	> $(LINT2JSF_TXLAT)  $(MAKEPID).TMP  $(@,B).txt
	%echo
	%echo
	%if $(GLINT_STATUS2)
	%echo lint would have returned exit status = $(GLINT_STATUS2) 
	%echo Make a policy decision on lint error code or program error code return - argv flag?
	%endif
	%if $(DEBUG_LINT_OUTPUT)
	cat $(MAKEPID).TMP
	%endif


#rm $(MAKEPID).TMP 

testlint2jsf .PHONY : testlint2jsf1 testlint2jsf2
	%echo '$@ tests complete'

# Rebuild targets when this makefile is changed. Right now thats all obj's and
# that should be sufficient when the automatic dependencies are supported with
# mkmf and a sanity %restart for out of sequence builds.
$(OBJS) : $(MAKEFILE)	# slightly different from $(INPUTFILE)

help .PHONY :
	%echo mk [-nologo]
	%echo Note: set|export OPUSMAKEOPTS=-nologo sets -nologo implicitly
	%echo mk [testlint2jsf1|testlint2jsf2]
	%echo mk testlint2jsf is equivalent to testlint2jsf1|testlint2jsf2
	%echo mk testlint2jsf
	%echo mk testlint2jsf1
	%echo mk testlint2jsf2
	%echo mk testlint2jsf3
	%echo mk testlint2jsf4



		
