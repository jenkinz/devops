/*
 * Copyright (C)2011 by General Atomics Aeronautical Systems, Inc.
 * All Rights Reserved.
 */

/* File:   bytearray.hpp
 * Author: scheur
 * Date:   9/27/2011
 * Description: Interface definition for the bytearray module.
 */

// bytearray.hpp
#ifndef BYTEARRAY_HPP 
#define BYTEARRAY_HPP
#include <valarray>
#include <string>
#include "xdefs.hpp"

// -----------------------------------------------------------------------------

class ByteArray : public std::valarray<unsigned>
{
public:
    explicit ByteArray(const std::string& hexstr);                // hex string
    explicit ByteArray(const unsigned long* data, const unsigned numelts);
    explicit ByteArray(std::size_t n, Byte defaultval = 0); // empty
    std::string toString() const;
    operator std::string() const;
};

// -----------------------------------------------------------------------------

//
// Inline Function Definitions
//
inline ByteArray::operator std::string() const
{
    return ByteArray::toString();
}


#endif // BYTEARRAY_HPP
