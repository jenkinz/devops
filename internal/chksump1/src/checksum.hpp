/*
 * Copyright (C)2011 by General Atomics Aeronautical Systems, Inc.
 * All Rights Reserved.
 */

/* File:   checksum.hpp
 * Author: scheur
 * Date:   9/27/2011
 * Description: Interface definition for the checksum module.
 */

#ifndef CHECKSUM_HPP
#define CHECKSUM_HPP

#include "xdefs.hpp"

// ----------------------------------------------------------------------------

template <typename T, typename A> 
struct StdChecksum 
{
    void operator()(T& sum, const A& a) { sum += a; }
};

// ----------------------------------------------------------------------------

template <typename T, typename A> 
struct Plus1Checksum 
{
    void operator()(T& sum, const A& a) { sum += static_cast<A>(a + 1); }
};

// ----------------------------------------------------------------------------

// T: sum type, A: addend type, E expression functor
template <typename T, typename A, class E = StdChecksum<T, A> >
class Checksum {
public:
    Checksum(T initialValue = 0) { sum_ = initialValue; }
    T operator+=(const A& a) { expression(sum_, a); return sum_; }
    T sum() const { return sum_; }
    T operator+=(const class ByteArray& arr); 
    T operator~() { return sum_ = ~sum_;}

private:
    T sum_;
    E expression;
};

// -----------------------------------------------------------------------------

#endif // CHECKSUM_HPP

