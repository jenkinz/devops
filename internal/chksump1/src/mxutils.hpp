/*
 * Copyright (C)2011 by General Atomics Aeronautical Systems, Inc.
 * All Rights Reserved.
 */

/* File:   mxutils.hpp
 * Author: scheur
 * Date:   9/27/2011
 * Description: Interface definition for the mxutils module.
 */


/*-----------------------------------------------------------------------------
  HDR : mxutils.hpp

  DESCRIPTION:  
    
 
------------------------------------------------------------------------------*/
#ifndef MXUTILS_HPP
#define MXUTILS_HPP

#include <fstream>
#include <string>
#include <utility>

std::pair<std::string, std::string> extractIOFilenames(const int argc, 
                                                       char* argv[]);

const bool evaluateOptions(const int argc, const char* const argv[]);
void usage(const char* const programName = 0);
void version();
void error(const std::string& msg, const int err = -1);
void validate(std::fstream& out, const std::string& filename);

// Remove lf or crlf independent of whether the input file that creates these
// record uses text translation or binary mode - it will not matter.
// cr and/or lf will be stripped from the end of the string.
void normalize(std::string& rec);

#endif // MXUTILS_HPP


