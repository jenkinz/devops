/*
 * Copyright (C)2011 by General Atomics Aeronautical Systems, Inc.
 * All Rights Reserved.
 */

/* File:   mxfile.cpp
 * Author: scheur
 * Date:   9/27/2011
 * Description: Encapsulates the MX output file that is generated from an 
 *              SREcord input file but also adds its own record derived from
 *              the SRecord input file. These are utility functions written 
 *              to support S-Record Processing and Mx File output
 */
  
#include "mxfile.hpp"
#include "mxutils.hpp"
#include "srecord.hpp"
#include "checksum.hpp"
#include "bytearray.hpp"
#include <cassert>
#include <stdexcept>
#include <iomanip>
#include <xutils.hpp>

//-----------------------------------------------------------------------------

// static
bool MxFile::altfmt_ = false;

//-----------------------------------------------------------------------------

/*!
 * 
 * 
 * \author scheur (9/26/2011)
 * 
 * \param filename 
 * \param mode 
 */
MxFile::MxFile(const std::string& filename, std::ios_base::openmode mode)
      : std::fstream(filename.c_str(), mode)
{
    errstate_ = false;
    linecount_ = 0;
    checksumAddress_ = 0; 
    embeddedChecksum_ = originalChecksum_ = actualChecksum_ = 0;
    filename_ = filename;
}  

//-----------------------------------------------------------------------------

unsigned long MxFile::embeddedChecksum() const
{
    assert(embeddedChecksum_ != 0);     // possible but very unlikely
    return embeddedChecksum_;
}

//-----------------------------------------------------------------------------
unsigned long MxFile::originalChecksum() const
{
    assert(originalChecksum_ != 0);     // possible but very unlikely
    return originalChecksum_;
}

//-----------------------------------------------------------------------------

unsigned long MxFile::actualChecksum() const
{
    //assert(actualChecksum_ != 0);     // possible but very unlikely
    return actualChecksum_;
}

//-----------------------------------------------------------------------------

unsigned long MxFile::checksumAddr() const
{
    assert(checksumAddress_ != 0);
    return checksumAddress_;
}

//-----------------------------------------------------------------------------

const std::string& MxFile::filename() const
{
    assert(!filename_.empty());
    return filename_;
}

//-----------------------------------------------------------------------------

MxFile::operator bool()
{
    return !errstate_;
}

//-----------------------------------------------------------------------------

void MxFile::handleError(const SRecord& srec)
{
    const std::string linenum = bin2str(linecount_, false, 10);
    std::string msg("Error at input file record # ");
    msg += linenum;
    msg += " : ";

    assert(!srec);
    msg += srec.errmsg();

    errstate_ = false;
    throw std::runtime_error(msg);
}

//-----------------------------------------------------------------------------

void MxFile::handleError(const std::string& errstr)
{
    const std::string linenum = bin2str(linecount_, false, 10);
    std::string msg("Error at input file record # ");
    msg += linenum;
    msg += " : ";
    msg += errstr;
    errstate_ = false;
    throw std::runtime_error(msg);
}


//-----------------------------------------------------------------------------

MxFile::ChecksumAddress::ChecksumAddress()
{
    highestAddr_ = checksumAddr_ = 0;
    dataoffset_ = 0; 
    completed_ = false;
}

//-----------------------------------------------------------------------------

void MxFile::ChecksumAddress::operator()(const unsigned long addr, 
                                         const unsigned numDataBytes,
                                         const unsigned long linecount)
{
    if (addr < highestAddr_) {
        const std::string linenum = bin2str(linecount, false, 10);
        std::string msg("Error at input file record # ");
        msg += linenum;
        msg += "\nS-Records not sorted on address - ascending order required";
        throw std::runtime_error(msg);
    }

    // Looking for the highest address. Find it, store it and assoc data length
    if (addr > highestAddr_) {
        highestAddr_ = addr;
        dataoffset_ = numDataBytes;
    }
}

//-----------------------------------------------------------------------------

// Builder pattern: Finalization of the checksumAddress calculations. 
// Call only once - after the highAddress is found.
// test if checksumAddr_ is quad aligned and assign.
// remainder r > 0 implies data is forced to be quad aligned
// if (const unsigned r = checksumAddr_ % 4)      
//     checksumAddr_ += 4 - r;  
// -or-
// const unsigned r = n - ((n / m) * m); 
// const unsigned E = (r > 0) ? m - r : 0; or E = m * bool(r) - r;
// checksumAddr_ += E;
// if checksumAddr_ is n, n = ((n + m - 1) / m) * m; even if m is not a 
// power of 2 but it is ...

// apriori m is a power of 2
// assert(n && ((((n | (n - 1)) + 1) >> 1) == n));
// -or- count the bits with the guard ensuring its not == to 1 which is odd
// assert(n && std::bitset<32>(n).count() == 1);
void MxFile::ChecksumAddress::build(const unsigned m /* m: alignment pow of 2*/) 
{
    // build is called only once and marked complete.
    if (completed_) 
        return;

    // add the size of the data payload (bytes) to the address - make room
    checksumAddr_ = highestAddr_ + dataoffset_;
    assert(m && !(m & (m - 1)));                        // m is a power of 2

    // Equivalent expression below but works with any m, now just power of 2,
    // however only working with power of 2 alignments
    // if (const unsigned r = checksumAddr_ % m)      
    //     checksumAddr_ += m - r;  
    checksumAddr_ = (checksumAddr_ + m - 1) & ~(m - 1); // align on m multiples
    assert(checksumAddr_ % m == 0);

    completed_ = true;                   
}

//-----------------------------------------------------------------------------

// Theoretically a checksum address could be 0 but this is extremely unlikely.
// If such is really the case, some very strange things are potentially going on
unsigned long MxFile::ChecksumAddress::address() const
{
    if (checksumAddr_ == 0)
        throw std::runtime_error("checksum address cannot be 0");
    return checksumAddr_;
}

//-----------------------------------------------------------------------------

MxInputFile::MxInputFile(const std::string& filename)
           : MxFile(filename, std::ios::in)
{
    validate(*this, filename.c_str());
}

//-----------------------------------------------------------------------------

MxOutputFile::MxOutputFile(const std::string& filename)
            : MxFile(filename, std::ios::out|std::ios::binary)
{
    validate(*this, filename.c_str());
}

//-----------------------------------------------------------------------------

// 
// Prepare an S-Record with all prior S3 data.
// set the highest address with data offset and quad alignment
// to store the checksum plus 1 calculation stored in the data
// field. The loader will put the checksum calculation in that
// the checksum address to be validated during load of the image
//
void MxOutputFile::finalize(FlashChecksumType& flashcs, 
                            OriginalChecksumType& originalcs, 
                            SRecord& rec)
{
    // finalize address calculation with builder pattern. order is important
    // side effect is to calculate (quad align and add data offset) the 
    // final checksumAddress_ - Builder pattern.
    csaddr_.build();        

    // Create S3 S-Record and build record from address and data. 
    // The Byte count and checksum are recalculated as required.
    // Write out the remaining 2 S-Records, first checksum and then last
    // terminal record. The remaining file is loadable.
    SRecord csrec;          
    checksumAddress_ = csaddr_.address();   // valid only if post build() above
    embeddedChecksum_ = flashcs.sum();
    ByteArray data(&embeddedChecksum_, 1);
    csrec.build(checksumAddress_, &data); 
    *this << csrec;
    *this << rec;

    // Cache variables for display only - no effect on mx output file.
    // original is a simple checksum of the file - no + 1
    originalChecksum_ = originalcs.sum();

    // The actual checksum is calculated by adding the original plus the sum of 
    // bytes from the flash embeddedChecksum_ 32 bit value.
    originalcs += data;
    actualChecksum_ = originalcs.sum();
}

//-----------------------------------------------------------------------------

// Iterate input file extracting S-Records. For each S-Record look for 
// terminal record type - if found break out of loop, if not found then
// process S-Record to extract bytes to add to checksum and calculate
// highest address to locate checksum at that address + data offset.
MxOutputFile& MxOutputFile::operator<<(SRecordInputFile& in)
{
    FlashChecksumType flashcs;
    OriginalChecksumType originalcs;
    linecount_ = 0;         // reset
    try {
        while (in) {
            SRecord rec;
            in >> rec;
            ++linecount_;
            if (!rec) {
                handleError(rec);
                break;
            }
            if (rec.terminal()) {
                finalize(flashcs, originalcs, rec);
                break;
            }
            // Calculate MxFile State for terminal/finalize stage above
            const SRecord::Type type = rec.type();
            if (type == SRecord::S3) {
                const std::string& datastr = rec.datastr();
                ByteArray data(datastr);
                flashcs += data;    // add bytes sum to plus1 checksum
                originalcs += data; // add bytes sum to original std checksum
                const unsigned long addr = rec.loadAddress();
                const unsigned numbytes = data.size();
                csaddr_(addr, numbytes, linecount_);
            }
            // Write out the intermediate records.
            *this << rec;
        } // while
    }
    catch (const std::runtime_error& e) {
        handleError(e.what());
        return *this;
    }

    if (linecount_ < 3)      // at least 1 of each 3 : S0, S3, and S7
        errstate_ = true;    // client checks state; not throwing exception

    return *this;
} 

//-----------------------------------------------------------------------------

MxOutputFile& MxOutputFile::operator<<(SRecord& srec)
{
    const std::string& str = srec.record();
    assert(!str.empty());
    *this << str << "\r\n";
    return *this;
}

//-----------------------------------------------------------------------------

void MxOutputFile::writeRecord(const std::string& record)
{
    assert(!record.empty());
    *this << record << "\r\n";
}

//-----------------------------------------------------------------------------


std::ostream& operator<<(std::ostream& os, const MxFile& mx)
{
    // legacy format selected.
    // e.g. Embedded = 024a991c;  Original = 0279340c;  Actual = 0279350d
    if (!MxFile::altfmt_) {
        std::cout << std::hex << std::setfill('0');
        std::cout << "\nEmbedded = " << std::setw(8)
                  << mx.embeddedChecksum();
        std::cout << ";  Original = " << std::setw(8) 
                  << mx.originalChecksum();
        std::cout << ";  Actual = " << std::setw(8) 
                  << mx.actualChecksum() << std::endl;
        return os;
    }

    // print out the results of the mx output file creation in std format
    std::cout << std::hex << std::uppercase << std::setfill('0');
    std::cout << "\nMx File '" << mx.filename() << "' Summary:" << std::endl;
    std::cout << "   Embedded Checksum: 0x" << std::setw(8)
              << mx.embeddedChecksum();
    std::cout << " @ Address: 0x" << std::setw(8) 
              << mx.checksumAddr() << std::endl;
    std::cout << "   Original: 0x" << std::setw(8) 
              << mx.originalChecksum();
    std::cout << " ; Actual: 0x" << std::setw(8) 
              << mx.actualChecksum() << std::endl;
    return os;
}

//-----------------------------------------------------------------------------

