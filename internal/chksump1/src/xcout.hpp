/*
 * Copyright (C)2011 by General Atomics Aeronautical Systems, Inc.
 * All Rights Reserved.
 */

/* File:   xcout.hpp
 * Author: scheur
 * Date:   9/27/2011
 * Description: no associated module. Abstracts conditional compilation such 
 *              that output is suppressed depending on build. For example a
 *              release build would include -DNDEBUG to suppress asserts
 *              presuming that the code has been tested but would also
 *              *REMOVE* -DXCOUT so that output is suppressed. -DXCOUT is used
 *              to keep cout - could reverse it to be consistent with asserts
 *              but its expected that output is not the same as asserts in that
 *              it reports back to the developer status but does not actually
 *              check inputs like assert does.
 */

#ifndef XCOUT_HPP
#define XCOUT_HPP

#include <iostream>

#ifdef XCOUT
#define xcout std::cout
#else
#define xcout 0 && std::cout
#endif

#endif // XCOUT_HPP


