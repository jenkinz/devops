/*
 * Copyright (C)2011 by General Atomics Aeronautical Systems, Inc.
 * All Rights Reserved.
 */

/* File:   checksum.cpp
 * Author: scheur
 * Date:   9/27/2011
 * Description: module provides 2 kinds of checksums for the purposes of 
 *              producing an mx file. The checksum expression is a
 *              parameterization E such that 2 or more expressions can be
 *              supported with 1 single abstraction. This follows from the
 *              strategy pattern.
 */

#include <typeinfo>
#include "checksum.hpp"
#include "bytearray.hpp"

#include <iostream>
#include <fstream>

//------------------------------------------------------------------------------

template <typename T, typename A, class E>
T Checksum<T, A, E>::operator+=(const ByteArray& arr)
{
    if (arr.size() == 0) 
        return sum_;

    if (typeid(E) == typeid(StdChecksum<T, A>)) 
        expression(sum_, arr.sum());        // a simple sum added to total
    else {
        // Plus1Checksum<T, A> File/Flash Checksum 
        // or any other checksum parameterization is per byte
        const unsigned N = arr.size();
        for (unsigned i = 0; i < N; ++i) 
            expression(sum_, arr[i]);  // add byte to checksum expression
    }

    return sum_;
}

//------------------------------------------------------------------------------

// Explicit Instantiations required for a particular chksump1 program.
// Could be moved to another module and include this .cpp file in that module.
template class Checksum<unsigned long, unsigned long>;  // original
template class Checksum<Byte, Byte>;                    // S-Record byte sum fld
template class Checksum<unsigned long, Byte,            // embedded
                        Plus1Checksum<unsigned long, Byte> >;

