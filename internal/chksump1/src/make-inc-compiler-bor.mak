O = obj
DEBUGFLAG = -v
DEBUG ?= $(DEBUGFLAG)
CC = bcc32									# orig compiler gone but v5.5 free
INCLUDES = -I.
DEFINES = 
CFLAGS = $(DEBUG) $(INCLUDES) $(DEFINES) -o$@
LDFLAGS = $(DEBUG)
%if !%null(DEBUG)
CFLAGS += -Od
LDFLAGS += -Od
%endif	# %if !%null
LD = $(CC) -e$@
CLEANMASK = *.tds							# borland debug support file.
OS_DEPS_MAKEFILE = make-inc-bcc55win_deps.mak

# guard against a microsoft install - lint uses this include. Problem is
# that we are not using the microsoft compiler here. So its disabled for this
# particular compiler.
%setenv INCLUDE=	

