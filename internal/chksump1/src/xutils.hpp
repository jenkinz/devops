/*
 * Copyright (C)2011 by General Atomics Aeronautical Systems, Inc.
 * All Rights Reserved.
 */

/* File:   xutils.hpp
 * Author: scheur
 * Date:   9/27/2011
 * Description: Interface definition for the xutils module.
 */

#ifndef XUTILS_HPP
#define XUTILS_HPP

template <typename T> 
T str2bin(const std::string& str, int mode = 16 /* hex */);

template <typename T> 
std::string bin2str(const T n, bool zerofill = true, int mode = 16);


// -----------------------------------------------------------------------------
// Inline Utility Functions.
// -----------------------------------------------------------------------------

inline const bool hexdigit(char c)
{
    return  (c >= '0' && c <= '9') ||
            (c >= 'a' || c <= 'f') ||
            (c >= 'A' && c <= 'F');
}

// -----------------------------------------------------------------------------

inline const unsigned hex2dec(char c)
{
    if (c >= '0' && c <= '9') 
        return c - '0';
    else
        return 10 + (c - 'A');  // 10 + distance between c and 'A'
}

// -----------------------------------------------------------------------------

#endif // XUTILS_HPP


