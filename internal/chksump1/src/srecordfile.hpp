/*
 * Copyright (C)2011 by General Atomics Aeronautical Systems, Inc.
 * All Rights Reserved.
 */

/* File:   srecordfile.hpp
 * Author: scheur
 * Date:   9/27/2011
 * Description: Interface definition for the srecordfile module.
 */

#ifndef SRECORDFILE_HPP
#define SRECORDFILE_HPP

#include <iostream>
#include <fstream>
#include <string>

//-----------------------------------------------------------------------------

class SRecordFile : public std::fstream
{
public:
    SRecordFile(const std::string& filename, std::ios_base::openmode mode);
    operator bool ();   // if (!srecFile)
protected:
    bool eof_;
    int numrecs_;
};

//-----------------------------------------------------------------------------

class SRecordInputFile : public SRecordFile
{
public:
    SRecordInputFile(const std::string& filename);
    SRecordInputFile& operator>>(class SRecord& srec);
private:
    SRecordInputFile& operator>>(std::string& record);
};

//-----------------------------------------------------------------------------

class SRecordOutputFile : public SRecordFile
{
public:
    SRecordOutputFile(const std::string& filename);
    SRecordOutputFile& operator<<(std::string& record);
};

//-----------------------------------------------------------------------------

#endif // SRECORDFILE_HPP
