%if "$(OS)" == "NT"
DATE = date /t
TIME = time /t
DATE_STR = $(DATE,SH)
TIME_STR = $(TIME,SH)
DATE_TIME_STR = $(DATE_STR, =_,/=-)_$(TIME_STR, =,:=-)
RUN = start # space after start intentional
DEBUGGER = devenv 
DEBUGGER_ARGS = /debugexe
%elif "$(OS)" == "unix"
DATE = date
DATE_STR = $(DATE,SH)
DATE_TIME_STR = $(DATE_STR, =_,/=-,:=-)
RUN = ./
BACKGROUND = &
DEBUGGER = gdb # insight # gdb 
DEBUGGER_ARGS = 
%else
%error OS = $(OS) is not recognized
%endif

backup .ALWAYS :
	$(V)test -d $@ || mkdir $@
	$(V)zip -D $@/$(OS,LC)_$(DATE_TIME_STR).zip *.?pp make* *.bin *.vt* *.vp* \
												README.txt *.mx -x *.lck *.tmp
	$(V)ls -al $@

MCCABE = pmccabe
MCCABEFLAGS = -t -c

HALSTEAD = npath
HALSTEADFLAGS = 

METRICS_REPORT_FILES = sloc.txt

# Metric Limits.
MAX_MCCABE = 10
MAX_HALSTEAD = 200
MAX_GLINTMETRIC = 20

%if "$(OS,LC)" == "unix"	# UNIX
GLINT = --flint -b
GLINTCOMPILERFLAG = -i/opt/flint/supp/lnt/ 
OLD_GLINTEXCFLAGS = -width(0,0) -efile(537,memory,string) \
				-efile(537,cstring,cstdlib) \
				-efile(766,cassert) +macros -w3
GLINTEXCFLAGS = chksump1.lnt /opt/flint/supp/gcc/co-gcc.lnt
GLINTFLAGS = $(GLINTCOMPILERFLAG) $(GLINTEXCFLAGS) # env-sled.lnt # au-sm123.lnt au-sm3.lnt
%else						# WINDOWS
GLINT = lint-nt -b
GLINTCOMPILERFLAG = -iC:\lint\lnt co-bc5.lnt -w1 +libdir("c:\borland\bcc55\include") 
GLINTEXCFLAGS = -e322 -e7 -e10 -e40 -e49 -e19 -e1013 -e1010 -e601 -e1904
GLINTFLAGS = $(GLINTCOMPILERFLAG) $(GLINTEXCFLAGS) env-sled.lnt # au-sm123.lnt au-sm3.lnt
%endif	# %if/%else "$(OS,LC)" == "unix"


# approx - using std unix utility wc word count
sloc .ALWAYS : 
	%if %null(tgt)
	wc $(SRCS) $(HDRS) > $*.txt
	cat $*.txt
	%else
	wc $(tgt)
	%endif
	%echo :  lines   words  bytes

lint .ALWAYS :
	%if %null(tgt)
	$(GLINT) $(GLINTFLAGS) $(SRCS)
	%else
	%echo Linting Module: $(tgt)
	$(GLINT) $(GLINTFLAGS) -unit $(tgt)
	%endif
	%echo

# sort output from the highest McCabe function number to the lowest
# and the totals at the top instead of the bottom. Remove | sort -nr to revert
# to normal output from McCabe tool unsorted and based on module ordering.
mccabe .ALWAYS : 
	%if %null(tgt)
	$(MCCABE) $(MCCABEFLAGS) $(SRCS) | sort -nr
	%else
	%echo McCabing Module: $(tgt)
	$(MCCABE) $(MCCABEFLAGS) $(tgt) | sort -nr
	%endif
	%echo

halstead : 
	%if %null(tgt)
	$(HALSTEAD) $(HALSTEADFLAGS) $(SRCS)
	%else
	%echo Halsteading Module: $<
	$(HALSTEAD) $(HALSTEADFLAGS) $(tgt)
	%endif
	%echo



