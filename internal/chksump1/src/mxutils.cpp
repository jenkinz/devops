/*
 * Copyright (C)2011 by General Atomics Aeronautical Systems, Inc.
 * All Rights Reserved.
 */

/* File:   mxutils.cpp
 * Author: scheur
 * Date:   9/27/2011
 * Description: Utility module that supports free functions used throughout the 
 *              cksump1 program 
 *  
 */

#include <iostream>
#include <iomanip>
#include <sstream>
#include <cassert>
#include <cstdio>
#include <cstdlib>
#include "mxutils.hpp"
#include "xutils.hpp"
#include "xdefs.hpp"
#include "mxfile.hpp"

// ----------------------------------------------------------------------------

const bool evaluateOptions(const int argc, const char* const argv[])
{
    assert(argc != 0);  // not possible if called from main with argc, argv

    if (argc < 3) {
        if (argc == 2) {
            std::string cmd(argv[1]);
            const bool help = (cmd == "-help" || cmd == "--help");
            const bool ver = (cmd == "-v" || cmd == "--version");
            if (help) {
                usage(argv[0]);
                exit(0);
            }
            else if (ver) {
                version();
                exit(0);
            }
        }
        usage(argv[0]);
        return false;
    }

    // For Legacy reasons - the -af option must be at the tail end.
    if (argc >= 4 && (std::string(argv[argc-1]) == "-af"))
        MxFile::toggleAltfmt();

    
    return true;
}

// ----------------------------------------------------------------------------

void version()
{
    std::cout << "\nCopyright 2011-2012 : General Atomics Aeronautical, Inc.";
    std::cout << "\nVersion " PROGRAM_VERSION << std::endl;
    std::cout << "Build Date Time: " << __DATE__ << " - ";
    std::cout << __TIME__ << std::endl;
}

// ----------------------------------------------------------------------------

void usage(const char* const programName)
{
    if (programName) {

        // strip pathing with \\, or / for either OS
        // then remove .exe from Windows with no effect on Linux
        std::string image(programName);
        if (std::size_t n = image.find_last_of("\\/")) {
            if (n != std::string::npos) 
                image = image.substr(n + 1, image.length() - n - 1);
            if ((n = image.find(".exe")) != std::string::npos) 
                image = image.substr(0, n);
        }

        std::cout << "\nusage: ";
        std::cout << image << " [-help|--help|-v|--version]";
        std::cout << "\nusage: ";
        std::cout << image << " inputfile[.bin] outputfile[.mx] [-af]\n";
        std::cout << "\t option -af triggers alternative console output format";
        std::cout << "\n\t and includes quad aligned highest checksum address"
                  << std::endl;
    }

    std::cout << "\t inputfile[.bin]: S-Record formatted image" << std::endl; 
    std::cout << "\t outputfile[.mx]: Checksumed loadable mx image" << std::endl; 
    version();

    std::cout << "The output is the checksum of S3 data fields & other info:";
    std::cout << "\n The Embedded Checksum is a (byte + 1) sum for flash/file"; 
    std::cout << "\n Address is a quad-aligned highest address + data bytes"; 
    std::cout << "\n Original is a standard checksum of the data, no +1";
    std::cout << "\n Actual is Original combined with +1 checksum" << std::endl;
}

// ----------------------------------------------------------------------------

// Only 2 arguments are provided - the input file and output file.
std::pair<std::string, std::string> extractIOFilenames(const int argc, 
                                                       char* argv[])
{
    if (argc < 3) 
        error(std::string("must supply 2 program io file arguments"), 2);

    std::string inputfilename = argv[1];
    assert(!inputfilename.empty());

    std::string outputfilename = argv[2];
    assert(!outputfilename.empty());

    return std::make_pair(inputfilename, outputfilename);
}

// ----------------------------------------------------------------------------

void validate(std::fstream& file, const std::string& filename)
{
    if (!file) {
        std::string msg(filename);
        msg += " input/output file is missing, protected, or read-only";
        error(msg); // file could also be 0 length
    }
}

// ----------------------------------------------------------------------------

// Remove lf or crlf independent of whether the input file that creates these
// record uses text translation or binary mode - it will not matter.
// cr and/or lf will be stripped from the end of the string.
void normalize(std::string& rec)
{
    const int len = rec.length(), end = len - 1;
    if (rec[end] == '\r' || rec[end] == '\n') {     // windows or unix
        int pos = end, n = 1;
        if (rec[len-2] == '\r') {                   // windows only
            --pos; ++n;                             // set to remove both crlf
        }
        rec.erase(pos, n);                          // strip lf or crlf
    }
}

// ----------------------------------------------------------------------------

void error(const std::string& msg, const int err)
{
    std::cout << msg << std::endl;
    usage();
    exit(err);
}

// ----------------------------------------------------------------------------


