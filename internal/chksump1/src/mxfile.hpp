/*
 * Copyright (C)2011 by General Atomics Aeronautical Systems, Inc.
 * All Rights Reserved.
 */

/* File:   mxfile.hpp
 * Author: scheur
 * Date:   9/27/2011
 * Description: Interface definition for the mxfile module.
 */

#ifndef MXFILE_HPP
#define MXFILE_HPP

#include <iostream>
#include <fstream>
#include "srecordfile.hpp"
#include "checksum.hpp"
#include "xdefs.hpp"

// -----------------------------------------------------------------------------

class MxFile : public std::fstream 
{
public:
    MxFile(const std::string& filename, std::ios_base::openmode mode);  

    unsigned long embeddedChecksum() const;
    unsigned long checksumAddr() const;
    unsigned long originalChecksum() const;
    unsigned long actualChecksum() const;
        
    const std::string& filename() const;
    operator bool (); 

    static void toggleAltfmt() { altfmt_ = !altfmt_; }

protected:
    bool errstate_;                             // state of file object, bool op
    unsigned long linecount_;

    // embedded is the S-3 record plus 1 checksum, 
    // original is the same data but the normal checksum, no plus 1, 
    // and the actual is the addition of the embedded data checksump bytes 
    // added to the original std checksum
    unsigned long embeddedChecksum_, originalChecksum_, actualChecksum_; 
    unsigned long checksumAddress_;             // highest address assigned
    std::string filename_;

    class ChecksumAddress 
    {
    public:
        ChecksumAddress();
        void operator()(const unsigned long addr, const unsigned numDataBytes,
                        const unsigned long linecount);
        void build(const unsigned alignment = 4);  // quad alignment default
        unsigned long address() const;
    private:
        unsigned long checksumAddr_; 
        unsigned long highestAddr_;
        bool completed_;
        unsigned dataoffset_; 
    } csaddr_;

    // Used in calculating the original, embedded and actual checksum.
    typedef Checksum<unsigned long, unsigned long> OriginalChecksumType;
    typedef Checksum<unsigned long, Byte, 
                     Plus1Checksum<unsigned long, Byte> > FlashChecksumType;

    void handleError(const std::string& errstr);
    void handleError(const SRecord& srec);

private:
    static bool altfmt_;
    friend std::ostream& operator<<(std::ostream& os, const MxFile& mx);
};

// -----------------------------------------------------------------------------

class MxInputFile : public MxFile
{
public:
    MxInputFile(const std::string& filename);

private:
    void readRecord(std::string& record);
    MxInputFile& operator>>(class SRecord& srec);
};

// -----------------------------------------------------------------------------

class MxOutputFile : public MxFile
{
public:
    MxOutputFile(const std::string& filename);
    MxOutputFile& operator<<(SRecordInputFile& in); 
    MxOutputFile& operator<<(class SRecord& srec); 

private:
    void writeRecord(const std::string& record);
    void finalize(FlashChecksumType& flashcs, 
                  OriginalChecksumType& originalcs, 
                  class SRecord& rec);
};

// -----------------------------------------------------------------------------

#endif // MXFILE_HPP


