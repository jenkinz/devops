# File : make-inc-configuration.mak
# included by main makefile.

%if "$(OS,LC)" == "unix"	# UNIX
O = o
E = # .out is an option but not using it this time.
RUNLOCAL = ./
%include "make-inc-compiler-gnu.mak"
MKDIR = mkdir
MKDIRFLAGS = -p
COMPILER = GNU

%else						# WINDOWS

E = .exe
RUNLOCAL = .\\

# Option: set COMPILER=[BOR|MSC|GNU] on Windows/Linux but defaults work also.
# mk or omake will select the default compiler on both Linux and Windows.
COMPILER ?= BOR				# default is Borland 5.5, others are MSC and GNU
%if "$(COMPILER)" != "BOR" && "$(COMPILER)" != "MSC" && "$(COMPILER)" != "GNU"
%error COMPILER=$(COMPILER) not recognized, COMPILER=[BOR|MSC|GNU], default: BOR
%endif
%include "make-inc-compiler-$(COMPILER,LC).mak"

MKDIR = mkdir
MKDIRFLAGS =

%endif						# %if/%else "$(OS,LC)" == "unix"

.PATH.c = .
.PATH.cpp = $(.PATH.c)
.PATH.h = $(.PATH.c)
.PATH.hpp = $(.PATH.c)

