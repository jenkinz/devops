/*
 * Copyright (C)2011 by General Atomics Aeronautical Systems, Inc.
 * All Rights Reserved.
 */

/* File:   xutils.cpp
 * Author: scheur
 * Date:   9/27/2011
 * Description: Provides conversion and formatting free functions for general 
 *              use on integral data types. Uses explicit template
 *              instantiation to control the compilation cycles.
 */



#include <iostream>
#include <iomanip>
#include <sstream>
#include <cassert>
#include <iomanip>
#include <stdexcept>
#include <typeinfo>
#include <limits>
#include "xutils.hpp"
#include "xdefs.hpp"

// Let compiler drive this conditional compilation using $(CC) -DDEBUGOUT
// #define DEBUGOUT

/******************************************************************************
Formula for calculating max. This is possible using template meta programming 
during compilation recursively but compiler does not support. 
Example: 
template<unsigned B, unsigned long N>
struct base_digits_detail {
  enum { result = 1 + base_digits_detail<B, N/B>::result };
};
template<unsigned B>
struct base_digits_detail<B, 0> {
private:
  enum { result = 0 };

  template<unsigned, unsigned long>
  friend class base_digits_detail;
};

template<unsigned B, unsigned long N>
struct base_digits {
  enum { result = base_digits_detail<B, N>::result };
}; 
 
template<unsigned B>
struct base_digits<B, 0> {
  enum { result = 1 };
}; 
 
However this crude method works but is done at run time vs compile time per 
above and is compiler dependent in working with very large numbers like ~0UL 
This method provides the correct implementation to generate runtime results 
whose values can be put in a look up table and this is the approach taken! 
 
template <class T> T maxdigits(T number, int base)
{
    int c = 1;
    for (T x = number / base; x > 0; ++c) 
        x /= base;
    return c;
}
 
So referencing function above in the driver below and generating data for a 
a table and then using a table for the most basic types 
but keying off number of bits rather than using a chain of typeid(T) 
comparisons is the approach - the number of bits maps 
over and compresses bool and char|Byte into the same category for 
example. 
 
Driver - generates data for table ...  
void base(unsigned base)
{
    cout << "BASE " << base << endl;
    unsigned long nl = ~0;
    int c = maxdigits(nl, base);
    cout << "base " << base << " nl = " << c << endl;

    unsigned short ns = ~0;
    c = maxdigits(ns, base);
    cout << "base " << base << " ns = " << c << endl;

    Byte nc = ~0;
    c = maxdigits(nc, base);
    cout << "base " << base << " nc = " << c << endl;
    cout << endl;
} 
 
int main() 
{ 
   base(8);
   base(10);
   base(16);
} 
 
There may be an algorithmic way to do this but aside from Meta programming 
above which didn't work for the compilers we have (they are several versions 
old) using a table seems reasonable based not on type but number of bits which 
maps cleanly over to a number of types (int = long, Byte = bool, etc) 
as a contraction mapping - that is byte(Byte), and char map to the same 
number of bits - so number of bits maps 1-N to types. 
 
******************************************************************************/


// ----------------------------------------------------------------------------

#ifdef DEBUGOUT
void report(const unsigned N, const unsigned numbits, const unsigned base)
{
    std::cout << N << " " << numbits << " " << base << std::endl;
}
#endif

// ----------------------------------------------------------------------------

// Independent of specific value but one can assume here that base is implied
// for the max value T where this is most generally ~0. However thats 
// unnecessary - this only for formatting not counting digits in an actual value
// inlining is possible using inline keyword after template <typename T> inline
// but most compilers don't like the local struct Lookup
//
// Suppress warning from Borland C++ in release mode (-v- or no -v) 
// Whats odd is that I removed the inline keyword on the function definition
// and I still get a warning - evidently templates with Borland C++ are inlined
// if contextually it makes sense - but this warning makes no sense so I'm 
// going to suppress it with the required pragma option.
// Warning W8027 xutils.cpp 139: Functions containing local classes are not
// expanded inline
#ifdef __BORLANDC__
#pragma option -w-inl   // Suppressing Borland bcc32 Warning W8027 message
#endif
template <typename T> unsigned maxdigits(unsigned base)
{

    // unsigned values - no sign distinction of - or +
    // bool maps to Byte as well.
    struct Lookup 
    {
        unsigned base;      // hex, dec, oct
        unsigned numbits;   // template based
        unsigned maxdigits; // largest value of that type or ~0
    } table[] = {
        {16, 32,  8},  {16, 16, 4}, {16, 8, 2}, // hex long(int), short, char
        {10, 32, 10},  {10, 16, 5}, {10, 8, 3}, // dec long(int), short, char
        { 8, 32, 11},  { 8, 16, 6}, { 8, 8, 3}  // oct long(int), short, char
    };

    const int N = sizeof (table) / sizeof (table[0]);
    const unsigned bits = std::numeric_limits<Byte>::digits * sizeof(T);

    // Adjust for 64 bit Linux or Windows or any modulo 32 bit upgrade
    // conversion is for 32 bit S-Records as a target
    const unsigned numbits = (bits % 32 == 0 ? 32 : bits);
    assert(numbits == 8 || numbits == 16 || numbits == 32);

    #ifdef DEBUGOUT
    report(N, numbits, base);
    #endif

    // Linear Search - small data set allows search to be efficient.
    unsigned digits = 0;
    for (int i = 0; i < N; ++i) {
        if (table[i].base == base && table[i].numbits == numbits) {
            digits = table[i].maxdigits;
            break;
        }
    }

    if (!digits) 
        throw std::runtime_error("Table lookup error - maxdigits<T>");
    return digits;

}

// ----------------------------------------------------------------------------

template <typename T> 
T str2bin(const std::string& str, int mode)
{
    assert(!str.empty());   // could assign empty string to 0, but assume error.
    assert(mode == 16 || mode == 10 || mode == 8);

    std::istringstream buf(str);
    if (mode == 16) 
        buf >> std::hex;
    else if (mode == 10) 
        buf >> std::dec;
    else if (mode == 8)
        buf >> std::oct;
    else
        throw std::runtime_error("T str2bin mode argument not recognized");

    T n = 0; 
    if (typeid(T) == typeid(const Byte) 
        || typeid(T) == typeid(Byte)) {
        int N;
        buf >> N;  // mapping Byte to a byte interpretation.
        n = N;
    }
    else
        buf >> n; 
    return n;
}

// ----------------------------------------------------------------------------

template <typename T> 
std::string bin2str(const T n, bool zerofill, int mode)
{
    assert(mode == 16 || mode == 10 || mode == 8);

    std::ostringstream buf;
    if (mode == 16) 
        buf << std::hex;
    else if (mode == 10) 
        buf << std::dec;
    else if (mode == 8)
        buf << std::oct;
    else
        throw std::runtime_error("T str2bin mode argument not recognized");

    buf << std::uppercase;
    if (zerofill) {
        const unsigned width = maxdigits<T>(mode);
        buf << std::setw(width);
        buf << std::setfill('0');
    }

    if (typeid(T) == typeid(const Byte) 
        ||  typeid(T) == typeid(Byte)) 
        buf << int(n);  // mapping Byte to a byte interpretation.
    else 
        buf << n; 

    std::string str(buf.str());
    return str;
}

// ----------------------------------------------------------------------------

// Explicit template instantiations
// Note: // GNU compiler forces me to put explicit instantiations at the bottom 
// of this module, which is odd but required to avoid unresolved symbols during
// link. Most compilers don't care - top or bottom doesn't matter but GNU does
// matter, at least on the Linux platform. They may have updated this as its 
// really a bug in the compiler - it shouldn't matter where the explicit 
// instantiation is done but it does so thats why its here and not at the top
// where you can more easily see it.
template unsigned long str2bin<unsigned long>(const std::string& str, int mode);
template Byte str2bin<Byte>(const std::string& str, int mode);

template std::string bin2str<unsigned long>(const unsigned long n, 
                                            bool zerofill, int mode);
template std::string bin2str<Byte>(const Byte n, bool zerofill, int mode);

// ----------------------------------------------------------------------------


