/*
 * Copyright (C)2011 by General Atomics Aeronautical Systems, Inc.
 * All Rights Reserved.
 */

/* File:   chksump1.cpp
 * Author: scheur
 * Date:   9/27/2011
 * Description:
 *   main entry point for chksump1 image where chksump1 means chksum plus 1
 *   of the S3 data records in the input S-Record File.
 * 
 *   Reads and outputs a Motorola S-record file. The S-record file may contain
 *   any combination of S-records. All S0-records are output, but only the last 
 *   S9-record is output. The embedded S-record parsers are expecting a file 
 *   download with exactly 1 termination record, a header record as the first 
 *   record, and any combination of S0-, S1-, S2-, or S3-records.  
 *
 *   S7- & S8 records can be used to specify an address to which control 
 *   is to be passed - this passing of control is *NOT* implemented.
 *   
 *   A 32-bit check sum is calculated bytewise of all of the data bytes.
 *   In the output S-record file created, an S3-record with the checksum as data
 *   is added just before the termination record.
 *   A +1 is added to support flash where the blocks can be scattered but
 *   pre-initialized with bytes set 0xFF. Adding +1 will result in 0 which has
 *   no effect on the checksum. The end result is that a file checksum and a
 *   flash checksum after load will still come out the same value.
 *   Therefore the unsigned long checksum = (uchar)(checksumByte + 1) is always
 *   calculated. The checksum is byte by byte of the data only and big endian.
 *
 *   The checksum is put at the first quad-aligned address following the highest
 *   address found in the S-record file adding to it the size of the previous
 *   data field. This highest address + data bytes is then quad aligned such
 *   that the resulting address value modulo 4 is zero.
 *
 *   The program prints two checksums and the quad aligned address which
 *   contains the plus1 checksum of the S3 data records of the input file.
 *   The following are outputs: 
 *     1. Original/Embedded = the actual checksum of the data bytes of the input
 *          S-record file.
 *     2. Quad aligned address holding the checksum plus 1 value of input file.
 *     3. New/Actual = the actual checksum of the data bytes of the output
 *          S-record file (including the appended S3-record with the
 *          embedded checksum.)  
 *
 *   The former DOS based executable produces:
 *       Embedded = 024a991c;  Original = 0279340c;  Actual = 0279350d
 *   but the newer incarnation produces
 *       Original/Embedded = 0x024a991c @ 0x00079a10   New/Actual = 0x024a9a1d
 *   The output files match so I can only use what I know are actual values
 *   from the actual mx file itself.
 * 
 *   This module and others it depends on are portable and work identically
 *   when used in a Linux environment. This code was compiled on 4 different
 *   compilers :
 *   Windows:
 *      1.) Borland C++ 5.5(Original was 4.5)
 *      2.) Visual Studio 2008
 *      3.) GNU C/C++ - mingw
 *   Linux:
 *      1.) GNU 4.1.2 C/C++ compilers
 *   
 * REFERENCES:  
 *   Motorola M68000 Family Programmer's Reference Manual, 
 *     Appendix C: S-Record Output Format.
 *     http://en.wikipedia.org/wiki/SREC_(file_format)
 *     The first record (S0) may include arbitrary comments such as a program
 *     name or version number.[4] The last (termination) record (S7, S8, or S9)
 *     may include a starting address but currently not used.
 *     see ddump -Rv rom.out -o rom.mx
 *     This utility chksump1[.exe] processes rom.mx into rom565.mx such that
 *     the checksum record is inserted in next to last position - just in front
 *     of the S7 terminating record.
 *     All ASCII nybbles/bytes are in big endian format per the S-Record
 *     Specification.
 * 
 *     cf. http://www.amelek.gda.pl/avr/uisp/srecord.htm
 *     Which contains a copy of the unix man page for S-Records
 *
 *  Note: main returns 0 if no explicit return per ANSI/ISO C++ standard
 */

#include <iostream>         
#include <stdexcept>   
#include "mxutils.hpp"      
#include "srecordfile.hpp"  
#include "mxfile.hpp"       

// ----------------------------------------------------------------------------

int main(int argc, char* argv[])
{

    // Note: option to add another arg to put hdr.mx file as first record in mx 
    // instead of current practice using mv with cat in makefile. Enhancement ?? 
    if (!evaluateOptions(argc, argv)) 
        return 1;

    try {
        // get filenames
        std::pair<std::string, std::string> io = extractIOFilenames(argc, argv);
        SRecordInputFile in(io.first.c_str());
        MxOutputFile mx(io.second.c_str());
        mx << in;                           // create the mx image output file.
        if (mx) {
            std::cout << mx << std::endl;   // print out mx file summary
        }
        else {
            std::cout << "Error: The MX File is incomplete. Check the SRecord ";
            std::cout << "input file for consistency!" << std::endl;
        }
    }
    // Note: std::runtime_error is used intentionally as a way of re-using a 
    // derived class from std::exception, which maps to standard practice.
    catch (const std::runtime_error& ex) {
        std::cout << "runtime error: " << ex.what() << std::endl;
        return 1;
    }

    // unexpected occurence.
    catch (const std::exception& ex) {
        std::cout << "unexpected exception: " << ex.what() << std::endl;
        return 2;
    }

    // antithesis to core-dump - which I may want - this is just sugar coating.
    // if release build then this message will appear, otherwise debug builds
    // will do a core dump which I can then do later analysis with ddd debugger
    // I don't expect this unknown exception to ever occur. The above
    // catch clauses should catch everything and use of assert(...) also.
    #ifndef RELEASE
    catch (...) {
        std::cout << "Unknown exception ..." << std::endl;
    }
    #endif

} // end main  

// ----------------------------------------------------------------------------


