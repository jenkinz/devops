/*
 * Copyright (C)2011 by General Atomics Aeronautical Systems, Inc.
 * All Rights Reserved.
 */

/* File:   srecord.cpp
 * Author: scheur
 * Date:   9/27/2011
 * Description: Abstracts away Motorolla S-Record details. 
 *              see http://en.wikipedia.org/wiki/SREC_(file_format) for details 
 */



// File: srecord.cpp
// Author: Andrew Scheurer

// system includes always first to support preprocessor overrides if required.
#include <stdexcept>        // std::runtime_error
#include <sstream>          // std::ostringstream
#include <iomanip>          // std::setfill, std::setw, etc
#include <cassert>          // assert
#include <cstring>          // memcpy

#include "xcout.hpp"        // xcout conditionally display of console output
#include "xutils.hpp"       // str2bin, bin2str
#include "srecord.hpp"      // prototypes
#include "bytearray.hpp"    // ByteArray
#include "checksum.hpp"     // Checksum<T, A, E = expression>

using std::endl;

//-----------------------------------------------------------------------------

//
// Local Definitions.
//
namespace {
// cf. http://en.wikipedia.org/wiki/SREC_(file_format)
struct descriptor {
    int index;
    int len;
} dt[] = {
 // type       bytecount   loadaddr     data        chksum
    {0, 2},    {2, 2},     {4, 8},      {12, -1},   {-1, 2}
};

const int numfields = sizeof(dt) / sizeof (dt[0]); // num descriptors or fields

enum {
    TypeIndex,
    ByteCountIndex,
    AddressIndex,
    DataIndex,
    ChecksumIndex
};

} // end anonymous namespace, static data to module

//-----------------------------------------------------------------------------
SRecord::SRecord(const std::string& record) 
       : record_(record)
{
    assert(!record.empty());
    rawdata_.data = 0;           // variable length data.
    rawdata_.length = 0;
    tokenize(record);
    record_ = record;
}

//-----------------------------------------------------------------------------

SRecord::SRecord(const SRecord::Type t)
{
    type(t);
    rawdata_.data = 0;           // variable length data.
    rawdata_.length = 0;
    assert(record_.empty());
}

//-----------------------------------------------------------------------------

SRecord& SRecord::operator=(const std::string& record) 
{
    assert(!record.empty());
    tokenize(record);
    record_ = record;
    return *this;
}

//-----------------------------------------------------------------------------

SRecord::~SRecord()
{
    delete [] rawdata_.data;
}

//-----------------------------------------------------------------------------

SRecord::operator bool() const 
{
    return errmsg_.empty();
}

//-----------------------------------------------------------------------------

const std::string& SRecord::errmsg() const
{
    return errmsg_;
}

//-----------------------------------------------------------------------------

const std::string& SRecord::record()
{
    // record is assigned with no updates applied to this object
    if (!record_.empty()) 
        return record_;

    // not assigned a completed SRecord string but constructed in pieces then
    // enforced is a complete SRecord definition as a function of type.
    // checksum and data are optional depending on type
    // see http://en.wikipedia.org/wiki/SREC_(file_format) 
    const bool fieldsMissing = type_.empty() || byteCount_.empty() 
                               || loadAddress_.empty();
    if (fieldsMissing)        
        throw std::runtime_error("SRecord Incomplete - missing fields");

    // Based on type, data is a requirement.
    Type rt = type();
    if ((rt == S0 || rt == S1 || rt == S2 || rt == S3) && data_.empty()) 
        throw std::runtime_error("SRecord type requires data fields");

    build();
    assert(!record_.empty());
    return record_;
}

//-----------------------------------------------------------------------------

const std::string& SRecord::datastr() const
{
    return data_;
}

//-----------------------------------------------------------------------------

SRecord::Type SRecord::type() const 
{
    assert(type_.length() == 2 && type_[0] == 'S');
    const int t = type_[1] - '0';
    return Type(t);
}

//-----------------------------------------------------------------------------

Byte SRecord::byteCount() const 
{
    Byte n;
    if (byteCount_.empty()) {
        if (loadAddress_.empty() || data_.empty() || checksum_.empty()) 
            throw std::runtime_error("bytecount() const on incomplete record");
        n = loadAddress_.length() + data_.length() + checksum_.length();
        byteCount_ = bin2str(n);
        return n;
    }
    else {
        assert(byteCount_.length() == 2);
        n = str2bin<Byte>(byteCount_);   // int arg, not char or uchar
    }

    return n;
}

//-----------------------------------------------------------------------------

unsigned long SRecord::loadAddress() const 
{
    if (loadAddress_.empty()) 
        return 0;
    const long addr = str2bin<unsigned long>(loadAddress_);
    return addr;
}

//-----------------------------------------------------------------------------

const SRecord::Data& SRecord::data() const 
{
    // this S-Record has no data.
    if (rawdata_.length == 0) {
        assert(rawdata_.data == 0);
        return rawdata_;
    }

    // test if converted already - calling into function again.
    // S-Records are forced to be 1-1 with string through constructor.
    // Once converted - re-entering this function will return same.
    if (rawdata_.data) { 
        assert(rawdata_.length > 0);
        return rawdata_; 
    }

    // see tokenize private member func where rawdata_.length is set as a 
    // function of the S-Record string decoding.
    assert(rawdata_.length % 2 == 0);       // invariant - byte = 2 nybbles
    rawdata_.data = new Byte[rawdata_.length];    
    for (unsigned i = 0, j = 0; i < data_.length(); i += 2, j++) {
        const Byte byte = str2bin<Byte>(data_.substr(i, 2));   
        rawdata_.data[j] = byte;
    }

    return rawdata_;
}

//-----------------------------------------------------------------------------

Byte SRecord::checksum() const 
{
    if (checksum_.empty()) {
        const Byte cs = calculateChecksum();
        checksum_ = bin2str(cs);
        return cs;
    }
    const Byte chksum = str2bin<Byte>(checksum_);
    return chksum;
}

//-----------------------------------------------------------------------------

const bool SRecord::terminal() const
{
    const Type t = type();
    const bool term = (t == S7 || t == S8 || t == S9);
    return term;
}

//-----------------------------------------------------------------------------

void SRecord::type(const Type t) 
{
    const char c = int(t) + '0';
    type_ = 'S';
    type_ += c;
    record_.clear();        // invalidate existing record member if any
}

//-----------------------------------------------------------------------------

void SRecord::byteCount(const Byte cnt) 
{
    byteCount_ = bin2str(cnt);
    record_.clear();        // invalidate existing record member if any
}

//-----------------------------------------------------------------------------

void SRecord::loadAddress(const unsigned long addr) 
{
    loadAddress_ = bin2str(addr);
    record_.clear();        // invalidate existing record member if any
}

//-----------------------------------------------------------------------------

void SRecord::data(const std::string& str) 
{
    assert(!str.empty());
    data_ = str;
    record_.clear();        // invalidate existing record member if any
}

//-----------------------------------------------------------------------------
// Update data using an array based data structure. 
// Use ByteArray instead if possible.
void SRecord::data(const Data d) 
{
    assert(d.length != 0);
    assert(d.data != 0);
    data_.clear();
    for (unsigned i = 0; i < d.length; i += 2) {
        const std::string substr = bin2str<Byte>(d.data[i]);
        data_ += substr;
    }

    if (d.length != rawdata_.length) {
        delete [] rawdata_.data;
        rawdata_.length = d.length;
        rawdata_.data = new Byte[rawdata_.length];
    }
    assert(rawdata_.data != 0);
    std::memcpy(rawdata_.data, d.data, d.length);
    record_.clear();        // invalidate existing record member if any
}

//-----------------------------------------------------------------------------

// Too small a set to make a table, these values flow from the S-Record spec.
unsigned SRecord::addressSize() const
{
    unsigned numbytes = 0;
    Type t = type();

    if (t == S0 || t == S1 || t == S5 || t == S9) 
        numbytes =  2;
    else if (t == S2 || t == S8) 
        numbytes = 3;
    else if (t == S3 || t == S7) 
        numbytes = 4;

    return numbytes;
}

//-----------------------------------------------------------------------------

// Function requires all elements to be present except checksum
// although if checksum is there, I'll just recalculate and overwrite
// with a different or same value.
// q.v. http://en.wikipedia.org/wiki/SREC_(file_format) for description
// of algorithm.
// Sum bytes from low to high for address inclusive of byte count      
// Note: Using Byte to sum and then ~ operator applied. Could instead use
// unsigned long as parameter to template, where count could exceed 255, and 
// then take lsb or least significant byte and one's complement on that lsb
// However using a single byte and wrapping calculation on that byte is
// equivalent.
Byte SRecord::calculateChecksum() const 
{
    if (loadAddress_.empty()) 
        throw std::runtime_error("S-Record Address cant be empty");
    assert(loadAddress_.length() % 2 == 0);

    const Byte c = byteCount();
    Checksum<Byte, Byte> cs(c);   

    ByteArray addrbytes(loadAddress_);
    cs += addrbytes;

    // Not all S-Records have data - it depends on the type
    // see tokenize(const std::string&)
    if (!data_.empty()) {
        assert(data_.length() % 2 == 0);
        ByteArray databytes(data_);
        cs += databytes;
    }
    const Byte value = ~cs;     // ones complement and extract lsb
    return value;              
}

//-----------------------------------------------------------------------------

void SRecord::checksum(const Byte chksum) 
{
    checksum_ = bin2str(chksum);
    record_.clear();        // invalidate existing record member if any
}

//-----------------------------------------------------------------------------

void SRecord::pre_validate(const std::string& recstr)
{
    assert(!recstr.empty());
    const unsigned N = recstr.length();
    if (N % 2)
        throw std::runtime_error("S-Record is not organized in bytes");

    for (unsigned i = 0; i < N; ++i) {
        if (!hexdigit(recstr[i])) {
            errmsg_ = "S-Record contains non-hex digits";
            return;
        }
    }
    const int NF = numfields;
    unsigned minlength = 0;
    for (int i = 0; i < NF; ++i) {
        if (i == AddressIndex) 
            minlength += 4;
        else if (i == DataIndex) 
            minlength += 2;
        else 
            minlength += dt[i].len;
    }

    // without knowing the type - this is the only assertion that can be 
    // made. 
    if (recstr.length() < minlength) 
        throw std::runtime_error("S-Record is not a minimum length");
}

//-----------------------------------------------------------------------------

void SRecord::post_validate()
{
    assert(!checksum_.empty());
    Byte cs = str2bin<Byte>(checksum_);
    Byte tmp = calculateChecksum();

    // check to see if calculation matches. I could throw an exception here
    // instead however since the user can set the checksum value for testing
    // this may not make sense. checksum accessor function is public.
    if (cs != tmp)  
        errmsg_ = "S-Record Checksum is incorrect";     // mark invalid
}

//-----------------------------------------------------------------------------

void SRecord::tokenize(const std::string& rec)
{
    pre_validate(rec);

    type_ = rec.substr(dt[TypeIndex].index, dt[TypeIndex].len); 
    byteCount_ = rec.substr(dt[ByteCountIndex].index, dt[ByteCountIndex].len); 

    unsigned dataindex = dt[DataIndex].index;
    const bool typeHasNoData = (type_ == "S5" || type_ == "S7" ||
                                type_ == "S8" || type_ == "S9");
    if (typeHasNoData || type_ == "S0" || type_ == "S1") {
        // extract 2 byte address, 4 nybbles
        unsigned n = 0;
        if (type_ == "S0" || type_ == "S1" || type_ == "S5" || type_ == "S9")
            n = 4;          // address is 4 nybbles or 2 bytes
        else if (type_ == "S8")
            n = 2;          // address is 6 nybbles or 3 bytes
        const unsigned addrlen = dt[AddressIndex].len - n;
        loadAddress_ = rec.substr(dt[AddressIndex].index, addrlen); 
        dataindex -= n;
    }
    // process type where addr bytes == 3
    else if (type_ == "S2") {
        // extract 3 byte address, 6 nybbles
        dataindex -= 2;
        const unsigned len = dt[AddressIndex].len - 2;
        loadAddress_ = rec.substr(dt[AddressIndex].index, len); // fixed len
        return;
    }
    else {
        if (type_ != "S3") {
            // error - Data Records must be of S3 type in this sequence!
            std::string msg("Unknown Record Type: Data Record must be S3 type");
            throw std::runtime_error(msg);
        }
        // S3 - extract 4 address bytes - 32 bit, S3 : 8 nybbles.
        const unsigned len = dt[AddressIndex].len;
        loadAddress_ = rec.substr(dt[AddressIndex].index, len);   
    }

    const unsigned reclen = rec.length();
    if (typeHasNoData == false) {
        // S-Record data is variable length and has to be calculated from the 
        // existing fields of the record.
        const unsigned overhead = type_.length() + byteCount_.length() + 
                                  loadAddress_.length() + dt[numfields - 1].len;
        if (const unsigned len = (reclen - overhead)) 
            data_ = rec.substr(dataindex, len);
    }

    if (const unsigned len = reclen - dt[ChecksumIndex].len) 
        checksum_ = rec.substr(reclen - 2, len);

    post_validate();
}

//-----------------------------------------------------------------------------

void SRecord::build(const std::string& addr, const std::string& data)
{
    assert(!addr.empty());
    assert(!data.empty());
    loadAddress_ = addr;
    data_ = data;
    build();
}

//-----------------------------------------------------------------------------

void SRecord::build(unsigned long addr, const std::string& data)
{
    assert(!data.empty());
    loadAddress(addr);
    data_ = data;
    build();
}


//-----------------------------------------------------------------------------

//
// reconstruct record and assume changes to its state including
// checksum. There is a cost to calling this function multiple times.
// determining field by field if there was a change is possible but
// more complex than required for this object. type isn't part of the
// checksum calculation so checksum is required to be recomputed for
// a type update, however using update_ imply a recompute of checksum
// is simpler, it wouldn't have to be computed for a type change. 
// Also if a checksum is assigned by the user, I still compute the
// update. The sequence may have included an update to data after
// an update to the checksum. The operation sequence is not tracked
//
// Byte count is calculated from the fields in the record.
// Byte count definition: two hex digits or nybbles, indicating the 
// number of bytes (hex digit pairs) that follow in the rest of the 
// record (in the address, data and checksum fields).
//
void SRecord::build(unsigned long addr, const class ByteArray* d)
{
    // Validation
    assert(!type_.empty());             // not optional, invariant

    // pmccabe looks at asserts and increases complexity number.
    // Either use preprocessor output w/ -DNDEBUG set or modify pmccabe
    // to ignore asserts which it should anyways.
    // To get accurate McCabe numbers I should use preprocessor anyways
    // so that with -DNDEBUG to remove asserts seems a reasonable solution.
    // Type t = type();
    // assert(d || (!d && (t == S5 || t == S7 || t == S8 || t == S9))); 
    // assert(!d || (d && (t == S0 ||t == S1 || t == S2 || t == S3)));

    // Construct record
    loadAddress(addr);
    if (d) 
        data(*d);
    build();
}

//-----------------------------------------------------------------------------

// update derived fields - byteCount and checksum for any record type.
void SRecord::build()
{
    // set record type
    // record type set by construction. Every S-Record has x address bytes
    // where x depends on type
    assert(!type_.empty() && !loadAddress_.empty());

    // set byte count
    const unsigned addrSize = addressSize();
    const Byte thisByteCount = addrSize + (data_.size() >> 1) + 1;
    byteCount(thisByteCount);

    // set checksum 
    Byte cs = calculateChecksum();  
    checksum_ = bin2str(cs);

    // All record types have a load address but varying sizes of bytes
    // pad it out to the maximum address size for the given type.
    const unsigned numAddrNybbles = (addrSize << 1); // 2 * # bytes = # nybbles

    //
    // Build Record per S-Record Requirements
    // see http://en.wikipedia.org/wiki/SREC_(file_format) on S-Record spec.
    // Note: std::setw must be set for each inserted object while other settings
    // are sticky and remain that way until reset or set to another value
    //
    std::ostringstream oss;
    oss << std::setfill('0') << std::uppercase;         // sticky settings
    oss << std::setw(2) << type_;
    oss << std::setw(2) << byteCount_;
    oss << std::setw(numAddrNybbles) << loadAddress_;   // pad with 0's

    // S5, S7, S8, S9 types have no data
    if (!data_.empty())             
        oss << data_;

    oss << std::setw(2) << checksum_;

    // record_ = type_ + byteCount_ + loadAddress_ [+ data_] + checksum_;
    // where data is optional depending on record type
    record_ = oss.str();
    assert(!record_.empty());
}

//-----------------------------------------------------------------------------
      

