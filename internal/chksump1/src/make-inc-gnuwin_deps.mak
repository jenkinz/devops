! ifdef MKMF
.NODEBUG : 2 4
.HDRPATH.cpp = .
.HDRPATH.c = .
.TYPE.OBJ = .o
MKMF_SRCS = bytearray.cpp checksum.cpp mxfile.cpp mxutils.cpp srecord.cpp srecordfile.cpp xutils.cpp
! endif
HDRS		= bytearray.hpp checksum.hpp mxfile.hpp mxutils.hpp \
		  srecord.hpp srecordfile.hpp xcout.hpp xdefs.hpp xutils.hpp
### OPUS MKMF:  Do not remove this line!  Generated dependencies follow.

bytearray.o: bytearray.hpp xdefs.hpp xutils.hpp

checksum.o: bytearray.hpp checksum.hpp xdefs.hpp

mxfile.o: bytearray.hpp checksum.hpp mxfile.hpp mxutils.hpp srecord.hpp \
	 srecordfile.hpp xdefs.hpp

mxutils.o: checksum.hpp mxfile.hpp mxutils.hpp srecordfile.hpp xdefs.hpp \
	 xutils.hpp

srecord.o: bytearray.hpp checksum.hpp srecord.hpp xcout.hpp xdefs.hpp \
	 xutils.hpp

srecordfile.o: mxutils.hpp srecord.hpp srecordfile.hpp xdefs.hpp

xutils.o: xdefs.hpp xutils.hpp
