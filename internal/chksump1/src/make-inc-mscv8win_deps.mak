! ifdef MKMF
.NODEBUG : 2 4
.HDRPATH.cpp = .
.HDRPATH.c = .
.TYPE.OBJ = .obj
MKMF_SRCS = checksum.cpp mxutils.cpp xutils.cpp srecordfile.cpp srecord.cpp mxfile.cpp bytearray.cpp
! endif
HDRS		= bytearray.hpp checksum.hpp mxfile.hpp mxutils.hpp \
		  srecord.hpp srecordfile.hpp xcout.hpp xdefs.hpp xutils.hpp
### OPUS MKMF:  Do not remove this line!  Generated dependencies follow.

checksum.obj: bytearray.hpp checksum.hpp xdefs.hpp

mxutils.obj: checksum.hpp mxfile.hpp mxutils.hpp srecordfile.hpp xdefs.hpp \
	 xutils.hpp

xutils.obj: xdefs.hpp xutils.hpp

srecordfile.obj: mxutils.hpp srecord.hpp srecordfile.hpp xdefs.hpp

srecord.obj: bytearray.hpp checksum.hpp srecord.hpp xcout.hpp xdefs.hpp \
	 xutils.hpp

mxfile.obj: bytearray.hpp checksum.hpp mxfile.hpp mxutils.hpp srecord.hpp \
	 srecordfile.hpp xdefs.hpp

bytearray.obj: bytearray.hpp xdefs.hpp xutils.hpp
