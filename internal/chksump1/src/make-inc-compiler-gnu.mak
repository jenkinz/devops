O = o
DEBUGFLAG = -g
DEBUG ?= $(DEBUGFLAG)
INCLUDES = -I.
DEFINES = 
CC = g++ 
LD = $(CC) -o $@
# gcc is C only but g++ -x is C/C++
CFLAGS = $(DEBUG) $(INCLUDES) $(DEFINES) -x $(<,E,.=,pp=++,xx=++) -o $@
LDFLAGS = $(DEBUG)

CLEANMASK = 
%ifdef "$(OS,LC)" == "unix"
OS_DEPS_MAKEFILE = make-inc-gnulinux_deps.mak
%else
OS_DEPS_MAKEFILE = make-inc-gnuwin_deps.mak
# guard against a microsoft install - lint uses this include. Problem is
# that we are not using the microsoft compiler here. So its disabled for this
# particular compiler.
%setenv INCLUDE=	
%endif

