/*
 * Copyright (C)2011 by General Atomics Aeronautical Systems, Inc.
 * All Rights Reserved.
 */

/* File:   srecordfile.cpp
 * Author: scheur
 * Date:   9/27/2011
 * Description: Provides an abstraction for managing an iteration of S-Records 
 *              within a file. 
 */


#include <stdexcept>
#include "srecordfile.hpp"
#include "srecord.hpp"
#include "mxutils.hpp"

using std::ios_base;

// -----------------------------------------------------------------------------

SRecordFile::SRecordFile(const std::string& filename, ios_base::openmode mode)
           : std::fstream(filename.c_str(), mode)
{
    eof_ = false;
    numrecs_= 0;
}

// -----------------------------------------------------------------------------

SRecordFile::operator bool ()
{
    return !eof_;
}

// -----------------------------------------------------------------------------


SRecordInputFile::SRecordInputFile(const std::string& filename)
                : SRecordFile(filename, std::ios::in)
{
    validate(*this, filename.c_str());
}

// -----------------------------------------------------------------------------

SRecordInputFile& SRecordInputFile::operator>>(SRecord& srec)
{
    std::string str;
    std::getline(*this, str);
    if (eof() || str.empty()) {
        eof_ = true;
        return *this;
    }
    normalize(str);
    srec = str;
    if (++numrecs_ == 1) {
        if (srec.type() != SRecord::S0) {
            throw std::runtime_error("Bad Input File: Missing S0 Record");
        }
    }
    return *this;
}

// -----------------------------------------------------------------------------

SRecordOutputFile::SRecordOutputFile(const std::string& filename)
                 : SRecordFile(filename, std::ios::out)
{

}

// -----------------------------------------------------------------------------



