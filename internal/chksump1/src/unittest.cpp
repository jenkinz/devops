/*
 * Copyright (C)2011 by General Atomics Aeronautical Systems, Inc.
 * All Rights Reserved.
 */

/* 
 * File:   unittest.cpp
 * Author: scheur
 * Date:   9/27/2011
 * Description: Test various functions as part of a component based approach. 
 *  
 */



#include <string>
#include <iostream>
#include <cassert>
#include <stdexcept>        // std::runtime_error
#include <sstream>          // std::ostringstream
#include <iomanip>          // std::setfill, std::setw, etc

#include "bytearray.hpp"
#include "checksum.hpp"
#include "xutils.hpp"
#include "srecord.hpp"

// S00600004844521B
// S30500004844521B
// S319000799980005A95C0005A96400000000000000000000000092
// S70500002008D2

typedef Checksum<unsigned long, unsigned long> OriginalChecksumType;
typedef Checksum<unsigned long, Byte, 
                 Plus1Checksum<unsigned long, Byte> > FlashChecksumType;

int main()
{
    using std::cout;
    using std::endl;
    using std::hex;

    std::string data("0005A95C0005A964000000000000000000000000");
    ByteArray arr(data);

    unsigned sum = arr.sum();
    cout << std::hex << "cs = " << sum << endl;

    unsigned char a[] = {0x05, 0xA9, 0x5C, 0x00, 0x05, 0xA9, 0x64};
    unsigned sz = sizeof (a) / sizeof (a[0]);
    cout << "sz = " << sz << endl;

    std::valarray<unsigned> v((unsigned)(0), sz);
    const int N = sz;
    for (int i = 0; i < N; ++i) {
        v[i] = a[i];
    }

    cout << "v sum = 0x" << hex << (int)v.sum() << endl;

    assert(v.sum() == arr.sum());

    const int M = data.length();
    for (int i = 0; i < M; ++i) {
        assert(hexdigit(data[i]));
    }

    OriginalChecksumType ocs;
    ocs += arr;
    assert(arr.sum() == ocs.sum());

    FlashChecksumType fcs;
    fcs += arr;
    cout << "fcs sum = 0x" << hex << fcs.sum() << endl;

    // S30500002008D2 breaks down to 
    // S3 05 00002008 D2
    try {
        // S319000799FC00000000000000000000000000000000000000004A
        //#define STR "S319000799FC00000000000000000000000000000000000000004A"
        // #define STR "S30500002008D2"
        // #define STR "S00600004844521B"
        #define STR "S70500002008D2"
        std::string somerectext(STR);
        SRecord srec(somerectext);
        if (!srec) {
            cout << "SRecord errmsg = " << srec.errmsg() << endl;
        }
    }
    catch (const std::runtime_error& err) {
        cout << "runtime error " << err.what() << endl;
    }

}   

// "S319000799FC00000000000000000000000000000000000000004A"
//  S3 19 000799FC 00000000000000000000000000000000000000004A"
// 19 + 00 + 07 + 99 + FC
/******
0x19+0x7+0x99+0xFC= 0x1B5 = 437 (Dec)
0x1B5
~0x1B5= -0x1B6 = 0xFE4A (16 bit)
0xFE4A

S00600004844521B
S0 06 0000 484452 1B

06+48+44+52

S70500002008D2
S7 05 00002008 D2
      4 ADDR bytes...
******/


