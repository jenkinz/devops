! ifdef MKMF
.NODEBUG : 2 4
.HDRPATH.cpp = .
.HDRPATH.c = .
.TYPE.OBJ = .obj
MKMF_SRCS = bytearray.cpp checksum.cpp mxfile.cpp mxutils.cpp srecord.cpp srecordfile.cpp xutils.cpp
! endif
HDRS		= bytearray.hpp checksum.hpp mxfile.hpp mxutils.hpp \
		  srecord.hpp srecordfile.hpp xcout.hpp xdefs.hpp xutils.hpp
### OPUS MKMF:  Do not remove this line!  Generated dependencies follow.

bytearray.obj: bytearray.hpp xdefs.hpp xutils.hpp

checksum.obj: bytearray.hpp checksum.hpp xdefs.hpp

mxfile.obj: bytearray.hpp checksum.hpp mxfile.hpp mxutils.hpp srecord.hpp \
	 srecordfile.hpp xdefs.hpp

mxutils.obj: checksum.hpp mxfile.hpp mxutils.hpp srecordfile.hpp xdefs.hpp \
	 xutils.hpp

srecord.obj: bytearray.hpp checksum.hpp srecord.hpp xcout.hpp xdefs.hpp \
	 xutils.hpp

srecordfile.obj: mxutils.hpp srecord.hpp srecordfile.hpp xdefs.hpp

xutils.obj: xdefs.hpp xutils.hpp
