// Module: hexdump.cpp 
// Author: Andrew Scheurer
// I wrote this module to help with analysis of binary files and decomposing
// those binary files into a text file of hex digits. This program does 
// just that but uses I/O redirection to do it - hexdump binfile > hex.txt

#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cassert>

static const unsigned HexRecLen = 16;
using namespace std;

#ifndef Byte
typedef unsigned char Byte;
#endif

namespace Util 
{
void hexdump(std::string& str, void *bufptr, int buflen, unsigned *recno = 0);
}

// -------------------------------------------------------------------------------
// stringstream C++ code, buffered C++ code.
// output to file or screen w/ buffer of void* translated to a string object.
void Util::hexdump(std::string& str, void *bufptr, int buflen, unsigned *recno) 
{
    Byte *buf = static_cast<Byte*>(bufptr);
    unsigned rec = recno ? *recno : 0;
    const unsigned blocklen = 16;
    std::ostringstream stm;
    for (int i = 0; i < buflen; i += blocklen, ++rec)
    {
        // recno column
        stm << std::setw(8) << std::setfill('0');
        stm << std::uppercase << std::hex << (rec << 4) << " ";

        // hexdump columns
        for (int j = 0; j < blocklen && (i + j < buflen); ++j)
            stm << std::setw(2) << static_cast<unsigned>(buf[i + j]) << " ";

        // ascii dump column.
        // making more efficient by storing in buffer ahead instead of 
        // converting per character to stringstream
        // Note: ascii dump is not right aligned, it ends up always 1 space from
        // the raw binary dump intentionally.
        char strbuf[blocklen + 1] = {0};
        for (int k = 0; k < blocklen && (i + k < buflen) ; ++k)
            strbuf[k] = ::isprint(buf[i + k]) ? buf[ i + k] : '.';
        stm << strbuf;

    } // end outer for.

    // make a deep copy of translated buffer.
    str = stm.str();     
}

// -------------------------------------------------------------------------------

void help()
{
    cout << "Hexdumpf v0.0, A.Scheurer: Build Date/Time " << __DATE__ << " " 
		 << __TIME__ << endl;
    std::cout << "hexdumpf inputfile > outputfile" << std::endl;
}

// -------------------------------------------------------------------------------

// tellg() operates on a non-const reference, in can't be a const ref 
// unfortunately
const unsigned length(std::ifstream& in)
{
    const unsigned restorepos = in.tellg();
    in.seekg(0, std::ios::beg);
    in.seekg(0, std::ios::end);
    const unsigned n = in.tellg();
    in.seekg(restorepos); 
    return n;
}

// -------------------------------------------------------------------------------

int main(const int argc, const char* argv[])
{
    using std::ifstream;
    using std::ofstream;
    using std::cout;
    using std::endl;

    if (argc == 1)
    {
        help();
        exit(1);
    }
    std::string fn(argv[1]);
    ifstream in(fn.c_str(), std::ios::binary);
    if (!in)
    {     
        std::cout << "File: " << fn << " not found" << std::endl;
        help();
        exit(2);
    }
    // Calculate length of buffer.
    const unsigned len = length(in);
    if (!len)
    {
        std::cout << "Error: File " << fn << " has length = " << len << std::endl;
        help();
        exit(3);
    }

#ifdef DEBUG
    // process the buffer.
    cout << "Hexdump v0.0, A.Scheurer: Build Date/Time " << __DATE__ << " " << __TIME__ << endl;
    cout << "Input File: " << fn << " has length = " << len << endl << endl; 
#endif

    // equiv: const unsigned numiter = (len + HexRecLen + 1) / HexRecLen;
    const unsigned numiter = (len / HexRecLen) + (len % HexRecLen != 0);
    for (unsigned i = 0, recno = 0; i < numiter; ++i, ++recno)
    {  
       char buf[HexRecLen] = {0};
       in.read(buf, sizeof buf);
       std::string rec;
       Util::hexdump(rec, buf, in.gcount(), &recno);
       cout << rec << endl;
    }

    // successful completion.
    return 0;

} // end main

