#ifndef XDEFS_HPP
#define XDEFS_HPP

// History:
// "1.1.0" - updated with review - validated test.
// null padding issue on Data - fixed in bin2str<Byte> - 
// removed if (n == 0) return std::string(); -- evidently wasn't required.
// Updating to 1.1.1

#ifndef PROGRAM_VERSION
#define PROGRAM_VERSION "1.1.1"
#endif 


#ifndef Byte
typedef unsigned char Byte;
#endif

// Default compiler... 
#ifdef __BORLANDC__
#define display_compiler_version #pragma message \
                     You are compiling using Borland C++ version __BORLANDC__.
#endif

#endif // XDEFS_HPP


