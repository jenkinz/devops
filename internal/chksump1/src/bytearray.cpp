/*
 * Copyright (C)2011 by General Atomics Aeronautical Systems, Inc.
 * All Rights Reserved.
 */

/* File:   bytearray.cpp
 * Author: scheur
 * Date:   9/27/2011
 * Description: Supports sum and iteration of an ordered collection of bytes 
 *              All converted integral data is big endian.
 */


#include <cassert>
#include <limits>
#include "bytearray.hpp"
#include "xutils.hpp"

// ----------------------------------------------------------------------------
 
/*!
 * 
 * 
 * \author scheur (9/27/2011)
 * 
 * \param hexstr 
 */
ByteArray::ByteArray(const std::string& hexstr)
         : std::valarray<unsigned>(unsigned(0), hexstr.size() >> 1)
{
    const unsigned len = hexstr.length(); 

    // hexstr must consist of 2 nybbles or hex digits for every byte
    assert(len >= 2 && len % 2 == 0);   

    for (unsigned i = 0; i < len; i += 2) {
        const Byte byte = str2bin<Byte>(hexstr.substr(i, 2));   
        (*this)[i >> 1] = byte;         // step by 2 implies divide by 2
    }

    // Post-Condition
    const unsigned sz = size();
    assert(sz == (hexstr.size() >> 1));
}

// ----------------------------------------------------------------------------

ByteArray::ByteArray(std::size_t n, Byte defaultval)
         : std::valarray<unsigned>(defaultval, n)
{
    // empty implementation intentional!
}

// ----------------------------------------------------------------------------

// Iterate array and for each element extract low order byte and store it in
// big endian format
// numelts: size of the array - 1 if pointer to an unsigned long
ByteArray::ByteArray(const unsigned long* data, const unsigned numelts)
          : std::valarray<unsigned>(unsigned(0),numelts * sizeof(unsigned long))
{
    assert(data != 0 && numelts != 0);
    for (unsigned i = 0; i < numelts; ++i) {
        unsigned long value = data[i];
        const unsigned N = sizeof(value);
        for (unsigned j = 0; j < N; ++j) {
            const Byte byte = static_cast<Byte>(value);
            (*this)[N - (j + 1)] = byte;                        // store it
                                                                // big endian
            value >>= std::numeric_limits<Byte>::digits;  // get next byte
                                                                // shift 8 bits 
        }
    }

    // Post-Condition
    const unsigned sz = size();
    assert(sz == (numelts * sizeof(unsigned long)));
}
         
// ----------------------------------------------------------------------------
        
// zero padded automatically if single byte can be represented as 1 digit
// Always using nybbles or 2 hex digits as representation of any byte and
// with leading zero if required.
std::string ByteArray::toString() const
{
    const unsigned sz = size();
    assert(sz != 0);

    std::string hexstr;
    for (unsigned i = 0; i < sz; ++i) {
        const Byte byte = (*this)[i];
        std::string tmp = bin2str<Byte>(byte);
        assert(!tmp.empty());
        hexstr += tmp;
    }

    // The hex string length must be in proportion to twice the array size
    assert(hexstr.length() == sz << 1);
    return hexstr;
}

// ----------------------------------------------------------------------------


