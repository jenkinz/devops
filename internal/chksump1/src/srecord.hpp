/*
 * Copyright (C)2011 by General Atomics Aeronautical Systems, Inc.
 * All Rights Reserved.
 */

/* File:   srecord.hpp
 * Author: scheur
 * Date:   9/27/2011
 * Description: Interface definition for the srecord module.
 */


#ifndef SRECORD_HPP
#define SRECORD_HPP

#include <string>
#include "xdefs.hpp"

class SRecord
{
    friend class SRecordFile;
public:
    enum Type {S0, S1, S2, S3, S5 = 5, S7 = 7, S8, S9};
    struct Data {
        unsigned length;
        Byte *data;   // any length, no maximum
    };

    SRecord(const std::string& record);
    SRecord(const Type t = S3);
    SRecord& operator=(const std::string& record);
    ~SRecord();
    operator bool() const; 
    const std::string& errmsg() const;

    const std::string& record();
    Type type() const;
    Byte byteCount() const;
    unsigned long loadAddress() const;
    const Data& data() const;
    const std::string& datastr() const;
    
    Byte checksum() const;
    const bool terminal() const;

    void type(const Type t);
    void byteCount(const Byte cnt);
    void loadAddress(const unsigned long addr);
    void data(const std::string& str);
    void data(const Data d);
    void checksum(const Byte chksum);

    // update derived fields - byteCount and checksum for any record type.
    void build(const std::string& addr, const std::string& data);
    void build(unsigned long addr, const std::string& data);
    void build(unsigned long addr, const class ByteArray* data = 0);

    void pre_validate(const std::string& recstr);
    void post_validate();

private:
    unsigned addressSize() const;
    Byte calculateChecksum() const;
    void tokenize(const std::string& record);
    void build();      

    mutable std::string record_, byteCount_, checksum_;
    mutable Data rawdata_;      // value is cached, mutable applies on update.
    std::string type_;    
    std::string loadAddress_;
    std::string data_;
    std::string errmsg_;
};

#endif // SRECORD_HPP

