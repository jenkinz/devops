O = obj
DEBUGFLAG = /ZI
DEBUG ?= $(DEBUGFLAG)
CC = cl
INCLUDES = -I.
DEFINES = 
CFLAGS = /EHsc $(DEBUG) $(INCLUDES) $(DEFINES) /Fo$@
LDFLAGS = $(DEBUG)
%if !%null(DEBUG)
CFLAGS += /Od
LDFLAGS += /Od
%endif	# %if !%null
LD = $(CC) /Fe$@
CLEANMASK = *.idb *.pdb *.ilk
OS_DEPS_MAKEFILE = make-inc-mscv8win_deps.mak

