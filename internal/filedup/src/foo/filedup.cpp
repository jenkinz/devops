//#include <sstream> 
#include <string>	//lint !e537 !e451
#include <fstream>
#include <vector>
#include <map>
#include <set>
#include <iostream>
#include <cassert>
#include <cstdlib>	//lint !e537 !e451
#include <cstdio>
#include <cerrno>
#include <iterator> // On Linux only - include required for istream_iterator

// Debug only 
// -DDUMP_RECORDS
// #define DUMP_RECORDS  

// If using on Windows - assume find via cygwin or unix-win32 accessible.
#define PATH_TOKEN "<<path>>"
#ifndef CMD_SEARCH_FILE_LIST
#define CMD_SEARCH_FILE_LIST  "find " PATH_TOKEN " -name "
#endif

#ifndef SEARCH_FILE_LIST_TMP_FILE
#define SEARCH_FILE_LIST_TMP_FILE  "x--duplicate-file-records.txt"
#endif

// http://www.cs.uregina.ca/Links/class-info/330/ParsingLine/parsingline.html
// http://anaturb.net/C/iter_example.htm
// boost has a string library that could be useful for directories with spaces
// see http://www.boost.org/doc/libs/1_46_1/doc/html/string_algo/usage.html#id2728530

///////////////////////////////////////////////////////////////////////////////

namespace {

void help(const std::string& program)
{
    std::string program_name(program);
    std::cout << "Uses path and pattern search via OS find command" << std::endl;
    const std::size_t n = program_name.find_last_of("/\\");
    if (n != std::string::npos) {
        int extent = program_name.size() - n;
        const std::size_t pos = program_name.find_last_of(".");
        if (pos != std::string::npos)
            extent = pos - n - 1;
        program_name = program_name.substr(n + 1, extent);
    }

    std::cout << program_name 
              << " <<path>> file search pattern[*.c*]" << std::endl;

    std::cout << "\tFind duplicate source files from current directory\n";
    std::cout << "\tExample: " << program_name << " . *.c*" << "\n";
    std::cout << "Note: supports spaces in directories but not on print out\n";
    std::cout << std::endl;
}

int file_length(std::ifstream& in)
{
    in.seekg(0, std::ios::end);
    const int len = in.tellg();
    in.seekg(0, std::ios::beg);
    return len;
}

void remove_file(std::ifstream& in)
{
    assert(in.is_open());
    in.close();
    int err = std::remove(SEARCH_FILE_LIST_TMP_FILE);
    assert(err == 0);
}

} // end anonymous namespace

///////////////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
{
    if (argc != 3) {
        help(argv[0]);
        return -1;
    }

    // extract path component
    const std::string findpath(argv[1]);
    std::string cmd(CMD_SEARCH_FILE_LIST);
    const std::size_t pos = cmd.find(PATH_TOKEN);
    cmd.replace(pos, strlen(PATH_TOKEN), findpath);

    // extract search pattern
    std::string pattern(argv[2]);
    cmd += " " + pattern + " > " + SEARCH_FILE_LIST_TMP_FILE;

    //std::cout << cmd << std::endl;

    // create temp file from cmd output
    int err = ::system(cmd.c_str());
    if (err) {
        std::cout << cmd << " returned error " << err << std::endl;
        return err;
    }

    // read the temp file created via IO redirection of ::system(cmd) above
    std::ifstream in(SEARCH_FILE_LIST_TMP_FILE);
    if (!in) {
        std::cout << "internal system error - file: " 
                  << SEARCH_FILE_LIST_TMP_FILE << " expected, not found!";
        return -2;
    }

    // 0 length file has no interest - remove and report no dups
    if (!file_length(in)) {
        remove_file(in);
        return 0;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Copy output file into a vector for duplicates index lookup.
    //
    // iterate file - line by line - add to std::multi-map of filename, 
    // full path, count and string - 
    // iterate the map and count the occurences. 
    // Note: istream_iter is to going to copy words only not directories with
    // spaces but I don't expect that in a development environment anyways.
    // The solution is a loop with std::getline but thats more code with little
    // payoff. I don't expect developers to have directories with spaces
    // in them - a very bad practice indeed!
    ////////////////////////////////////////////////////////////////////////////

    std::vector<std::string> records;
    std::istream_iterator<std::string> file_iter(in), end_of_stream;
    std::copy(file_iter, end_of_stream, std::back_inserter(records));

    // dump records
#ifdef DUMP_RECORDS
    std::ostream_iterator<std::string> ositer(std::cout, "\n");
    std::copy(records.begin(), records.end(), ositer);
#endif

    // Insert vector into a map - counting duplicates as indexes into the set
    typedef std::set<unsigned> DictValue;
    typedef std::map<std::string, DictValue> DictType;
    DictType dict;
    const int N = records.size();
    for (int i = 0; i < N; ++i) {
        std::string filename = records[i];
        const std::size_t n = filename.find_last_of("/\\");
        if (n != std::string::npos) {
            filename = filename.substr(n + 1);
        }
        // std::cout << "stripped file = " << filename << std::endl;
        DictType::iterator it =  dict.find(filename);
        if (it != dict.end()) {
            // stripped filename found - insert index into value set
            it->second.insert(i);
        }
        else {
            // insert unique pathless filename into dictionary.
            DictValue v; 
            v.insert(i);
            dict.insert(DictType::value_type(filename, v));
        }
    }

    // Now iterate the map to find duplicates where the set has elements > 1
    // When found print out the duplicates with a newline to separate next 
    // set of duplicates.
    int dups_found = 0; //foo
    for (DictType::iterator it = dict.begin(); it != dict.end(); ++it) {
        if (it->second.size() > 1) {
            dups_found += it->second.size();
            for (DictValue::iterator i = it->second.begin(); 
                 i != it->second.end(); ++i) {
                const std::string& dup = records[*i];
                std::cout << dup << "\n";
            }
            std::cout << std::endl;
        }
    }

    // if no dups found delete empty temporary file, otherwise retain for
    // user to inspect and remove duplicates. Notify user file exists
    if (!dups_found) {
        remove_file(in);
    }
    else {
        std::cout << "\n\t *** select/exec cat " << SEARCH_FILE_LIST_TMP_FILE 
                  << " *** " << std::endl;
    }

    // make or shell to optionally look at main return code in terms of number 
    // of duplicates found.
    return dups_found;


} // end main


