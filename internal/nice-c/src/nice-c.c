/* link with wildargs.obj */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#ifndef __GNUC__
#include <io.h>
#endif /*__GNUC__*/
#ifndef _MAX_FNAME
#define _MAX_FNAME 80
#endif /*_MAX_FNAME*/
#ifndef _MAX_PATH
#define _MAX_PATH 256
#endif /*_MAX_PATH*/

#define COMMENTFIX

#define local static

#define level0 (structlevel+3)
#define maxlevel 64
#define maxindlevel 16
#define false 0
#define true 1

/* flags for newline */
#define nl_start 1 /* beginning of input line */
#define nl_ignore 2 /* ignore next \n from input file */
#define nl_noputln 4 /* don't write previous line */
#define nl_always 8 /* newline has been set because alwaysnewline */

/* flags for disabled */
#define disable_pragma_asm 1 /* disabled by #pragma asm */
#define disable_pragma_off 2 /* disabled by #pragma nicec off */
#define disable_asm 4 /* disabled by #asm */

typedef int bool;
typedef unsigned char byte;

typedef enum
 {
  tok_endline,
  tok_endfile,
  tok_comment,
  tok_cppcomment,
  tok_ident,
  tok_number,
  tok_keyword,
  tok_assign,
  tok_compare,
  tok_logical,
  tok_bitop,
  tok_addind,
  tok_operator,
  tok_component,
  tok_scope,
  tok_boolnot,
  tok_bitnot,
  tok_unary,
  tok_indir,
  tok_typindir,
  tok_address,
  tok_incdec,
  tok_condition,
  tok_colon,
  tok_semicolon,
  tok_comma,
  tok_quote,
  tok_wquote,
  tok_inslbrace,
  tok_lbrace,
  tok_rbrace,
  tok_lbracket,
  tok_rbracket,
  tok_lpar,
  tok_rpar,
  tok_langle,
  tok_rangle,
  tok_dots,
  tok_char
 } tokentyp;

typedef enum
 {
  key_null,
  key_misc,
  key_brace,
  key_brace1, /* left and right braces on same line */
  key_brace2, /* brace after case label */
  key_brace3, /* inserted brace */
  key_struct, /* struct, union or class */
  key_if,
  key_else,
  key_while,
  key_for,
  key_do,
  key_dowhile,
  key_switch,
  key_case,
  key_break,
  key_continue,
  key_return,
  key_new,
  key_delete,
  key_assert,
  key_catch,
  key_goto,
  key_asm,
  key_operator,
  key_template,
  key_cppcast,
  key_func,
  key_assign,
  key_varlist
 } keytyp;

typedef enum
 {
  par_unknown,
  par_if, /* if, while, catch */
  par_for,
  par_funcdecl,
  par_funccall,
  par_castcall,
  par_return,
  par_assert
 } partyp;

typedef enum
 {
  spc_null, /* the variable 'spaces' contains the number of spaces */
  spc_if, /* spaces after 'if', 'while', 'for', 'switch' */
  spc_templdecl, /* spaces between 'template' and '<' */
  spc_templcall, /* spaces between template class name and '<' */
  spc_cppcast, /* spaces between xxx_cast and '<' */
  spc_castcall, /* spaces between xxx_cast<x> and '(' */
  spc_angle, /* spaces after '<' and before '>' if template */
  spc_returnpar, /* spaces between 'return' and '(' */
  spc_return, /* spaces between 'return' and value */
  spc_funcdecl, /* spaces between function name and '(' if decl */
  spc_funccall, /* spaces between function name and '(' if call */
  spc_funcpar, /* spaces after '(' and before ')' if function */
  spc_funcnopar, /* spaces between '(' and ')' if function without params */
  spc_ifpar, /* spaces after '(' and before ')' if 'if' etc. */
  spc_castpar, /* spaces after '(' and before ')' if cast */
  spc_rpar, /* spaces after ')' if not followed by '{' */
  spc_par, /* spaces after '(' and before ')' */
  spc_cast, /* spaces after '(cast)' */
  spc_brace, /* spaces between '{' and statement */
  spc_ifbrace, /* spaces between ')' and '{' */
  spc_elsebrace, /* spaces between 'else' etc. and '{' */
  spc_index, /* spaces between identifier and '[' */
  spc_bracket, /* spaces after '[' and before ']' */
  spc_declcomma, /* spaces after ',' in function decl */
  spc_callcomma, /* spaces after ',' in function call */
  spc_templdeclcomma, /* spaces after ',' in template declaration */
  spc_templcallcomma, /* spaces after ',' in template class name */
  spc_forcomma, /* spaces after ',' in for statement */
  spc_initcomma, /* spaces after ',' in initialization */
  spc_comma, /* spaces after ',' otherwise */
  spc_semi, /* spaces after ';' */
  spc_classcolon1, /* spaces before ':' in classes */
  spc_classcolon2, /* spaces after ':' if classes */
  spc_casecolon1, /* spaces before ':' in case label */
  spc_casecolon2, /* spaces after ':' in case label */
  spc_emptyfor1, /* 1st spaces in 'for ( ; ; )' */
  spc_emptyfor2, /* 2nd spaces in 'for ( ; ; )' */
  spc_emptyfor3, /* 3rd spaces in 'for ( ; ; )' */
  spc_assign, /* spaces around '=' */
  spc_comp, /* spaces around '==', '!=', '<=', '>=', '<', '>' */
  spc_log, /* spaces around '||', '&&' */
  spc_bit, /* spaces around '|', '&', '^' */
  spc_cond, /* spaces around '?' and ':' */
  spc_boolnot, /* spaces after '!' */
  spc_bitnot, /* spaces after '~' */
  spc_unary, /* spaces after '+' or '-' */
  spc_indir2, /* spaces between '*' and expression */
  spc_addr2, /* spaces between '&' and expression */
  spc_typindir, /* spaces between identifier and '*', ')' */
  spc_indir, /* spaces between identifier and '*' */
  spc_indir1, /* spaces between '*' and identifier */
  spc_addr, /* spaces between identifier and '&' */
  spc_addr1, /* spaces between '&' and identifier */
  spc_addind, /* spaces around operator in '[]' if digit follows */
  spc_op, /* spaces around other operators */
  spc_comment, /* spaces before comments */
  spcmax
 } spctyp;

typedef enum
 {
  tab_other, /* tabs within other statements */
  tab_init, /* tabs within initialization */
  tab_comment, /* tabs before comment */
  /* check tab_other if more added!! */
  tabmax
 } tabtyp;

typedef enum
 {
  ind_level, /* indentation of each level */
  ind_levelstruct, /* indentation of each level in structs */
  ind_level0, /* additional indentation of first level */
  ind_levelstruct0, /* additional indentation of first level in structs */
  ind_switchstat, /* indentation of statements in switch */
  ind_rbrace, /* indentation of right brace */ /* don't change order! */
  ind_lbrace, /* indentation of left brace */
  ind_rbrace0, /* indentation of right brace at first level */
  ind_lbrace0, /* indentation of left brace at first level */
  ind_rbstruct, /* indentation of right brace in structs */
  ind_lbstruct, /* indentation of left brace in structs */
  ind_if, /* indentation between 'if (' etc. and ')'*/
  ind_colonfunc, /* indentation of ':' in function declaration */
  ind_colonstruct, /* indentation of ':' in structures */
  ind_assign, /* indentation after assignment */
  ind_varlist, /* indentation of list of variables (int a,b,c,d;) */
  ind_func, /* indentation between 'func(' and ')' if call */
  ind_decl, /* indentation between 'func(' and ')' if decl */
  ind_caselabel, /* indentation of case labels (negative) */
  ind_label, /* indentation of labels (negative) */
  ind_structlab, /* indentation of labels in structure (negative) */
  indmax
 } indtyp;

typedef enum
 {
  brace_struct, /* brace in structs */
  brace_else, /* brace after 'else' */
  brace_do, /* brace after 'do' */
  brace_init, /* brace in initializers */
  brace_level0, /* brace at first level */
  brace_level, /* other braces */
  bracemax
 } bracetyp;

typedef struct
 {
  long cnt_sum0; /* number */
  long cnt_sum1; /* sum */
  long cnt_sum2; /* sum of squares */
 } counttyp;

typedef struct
 {
  long sum_n;
  long sum_x;
  long sum_y;
  long sum_xx;
  long sum_yy;
  long sum_xy;
 } sumtyp;


/* parameters */
local int spc[spcmax]=
 {
  0, /* spc_null */
  1, /* spc_if */
  1, /* spc_templdecl */
  0, /* spc_templcall */
  0, /* spc_cppcast */
  0, /* spc_castcall */
  0, /* spc_angle */
  1, /* spc_returnpar */
  1, /* spc_return */
  0, /* spc_funcdecl */
  0, /* spc_funccall */
  0, /* spc_funcpar */
  0, /* spc_funcnopar */
  0, /* spc_ifpar */
  0, /* spc_castpar */
  1, /* spc_rpar */
  0, /* spc_par */
  0, /* spc_cast */
  0, /* spc_brace */
  1, /* spc_ifbrace */
  1, /* spc_elsebrace */
  0, /* spc_index */
  0, /* spc_bracket */
  1, /* spc_declcomma */
  1, /* spc_callcomma */
  1, /* spc_templdeclcomma */
  1, /* spc_templcallcomma */
  1, /* spc_forcomma */
  1, /* spc_initcomma */
  1, /* spc_comma */
  1, /* spc_semi */
  1, /* spc_classcolon1 */
  1, /* spc_classcolon2 */
  0, /* spc_casecolon1 */
  1, /* spc_casecolon2 */
  0, /* spc_emptyfor1 */
  0, /* spc_emptyfor2 */
  0, /* spc_emptyfor3 */
  1, /* spc_assign */
  1, /* spc_comp */
  1, /* spc_log */
  1, /* spc_bit */
  1, /* spc_cond */
  0, /* spc_boolnot */
  0, /* spc_bitnot */
  0, /* spc_unary */
  0, /* spc_indir2 */
  0, /* spc_addr2 */
  1, /* spc_typindir */
  1, /* spc_indir */
  0, /* spc_indir1 */
  1, /* spc_addr */
  0, /* spc_addr1 */
  0, /* spc_addind */
  1, /* spc_op */
  1, /* spc_comment */
 };

local bool tab[tabmax]=
 {
  false, /* tab_other */
  false, /* tab_init */
  false, /* tab_comment */
 };

local int ind[indmax]=
 {
  8, /* ind_level */
  8, /* ind_levelstruct */
  0, /* ind_level0 */
  0, /* ind_levelstruct0 */
  8, /* ind_switchstat */
  0, /* ind_rbrace */
  0, /* ind_lbrace */
  0, /* ind_rbrace0 */
  0, /* ind_lbrace0 */
  0, /* ind_rbstruct */
  0, /* ind_lbstruct */
  8, /* ind_if */
  8, /* ind_colonfunc */
  8, /* ind_colonstruct */
  8, /* ind_assign */
  8, /* ind_varlist */
  8, /* ind_func */
  8, /* ind_decl */
  -8, /* ind_caselabel */
  -8, /* ind_label */
  -8, /* ind_structlab */
 };

local int bracesameline[bracemax]={0}; /* left brace on same line if >0 */

local int itab=8; /* tab spacing in input file */
local int otab=8; /* tab spacing in output file */
local int elsesameline=0; /* 'else' on same line as right brace if >0 */
local int whilesameline=0; /* 'while' on same line as right brace if >0 */
local int colonfuncsameline=0; /* colon in func decl on same line if >=0 */
local int colonstructsameline=0; /* colon in structs on same line if >=0 */
local bool alignfuncind=false; /* align indentation after 'func(' if call */
local bool aligndeclind=false; /* align indentation after 'func(' if decl */
local bool alignifind=false; /* align indentation after 'if (' */
local bool alignassignind=false; /* align indentation after assignment */
local bool aligncasestat=false; /* align statement after 'case x:' */
local bool alwaysnewline=true; /* always new line after ; etc */
local bool alwaysbrace=false; /* always braces after if etc. */
local bool bracealwaysnewline=true; /* always new line after { */
local bool bracealwaysnewlinestruct=true; /* always new line after { */
local bool braceinclevel=false; /* a brace always incement level of indentation */
local bool labelfirst=-1; /* all labels start at first column (learning) */
local bool withformfeed=false; /* keep form feeds */

local int oldilini;
local int oldx;
local int oldreadspaces;
local int oldtokx;
local int oldwithtab;
local int oldc0;
local int oldc1;
local int oldop0;
local int oldop1;
local int oldop2;
local int ilini; /* index to input line */
local int ilinlen; /* length of input line inclusive \n */
local int olinlen; /* length of output line without \n */
local int x; /* current input x position (after c0) */
local int tokx; /* x position of token */
local int ox; /* output x position */
local int c0; /* current character from input file */
local int c1; /* next character in input file */
local int idlen;
local int op0; /* first char of operator */
local int op1; /* second char of operator */
local int op2; /* third char of operator */
local int level; /* current level */
local int iflevel; /* level of last 'if' for 'else' */
local int bracelevel; /* level of last non-inserted } */
local int switchlevel;
local int ppiflev; /* #if level */
local int iindent;
local int commentiindent; /* indentation of comment in input line */
local int commentoindent; /* indentation of comment in output line */
local int parindent; /* indentation between 'func(' or 'if (' etc. and ')' */
local int pariindent;
local int assignind; /* indentation between '=' and ';' */
local int assigning; /* 0: no, 1: first line, 2: follow inlines */
local int indirpos; /* position of last * or & in output line */
local int commentpos; /* beginning of comment in output line */
local int readspaces; /* number of spaces before the just read token */
local int lastspaces; /* number of spaces before last token */
local int spaces; /* spaces before next token */
local int parlevel; /* () level */
local int bracketlevel; /* [] level */
local int lbrace; /* { not yet written (variable contains indentation) */
local int oindent; /* indentation of output line */
local int indent; /* indentation of current level */
local int indent1; /* indentation of current level if brace increments level */
local int indentbeg; /* indentation at begining of the line (for comments) */
local int indentcolon; /* saved indentation if indented colon */
local int indentcolon1; /* saved indentation if indented colon if brace incr lev */
local int braceindent; /* indentation of brace a current level */
local int newline; /* beginning of line (2: ignore next \n) */
local int numid; /* number of identifier to determine if function declaration */
local int numid1; /* number of identifier to determine if it's an expression */
local int numassign; /* number of assignments in current statement */
local int structlevel; /* level of structure declaration */
local int extlevel; /* 2 if within extern "C" { } */
local int expr; /* 1: expression possible, 2: initializers */
local int caselabel;
local int enumdecl; /* within { } of enum */
local int insrbrace; /* insert } */
local int disabled; /* formatting is disabled */
local bool inslbrace; /* insert { if missing */
local bool debug; /* debug: don't rename temp file */
local bool withtab; /* there were tabs before the token */
local bool optlearn; /* option -l (learning) specified */
local bool learn; /* currently learning */
local bool forempty; /* for (;;) */
local bool first; /* at beginning of input line */
local bool formfeed1; /* there was a form feed in the previous input line */
local bool formfeed; /* there was a form feed in the current input line */
local bool braceonly; /* only a left brace on current line to current position  */
local bool newstat; /* beginning of new statement */
local bool structdecl; /* declaring structure (before '{') */
local bool structbody; /* declaring structure (betweem '{}') */
local bool classaccess; /* last id was 'private', 'protected' or 'public' */
local bool oper; /* token can be an operator */
local bool cast; /* last right parenthesis was cast */
local bool canbecast; /* can be cast */
local bool indcolon; /* indented colon */
local bool identindir; /* there is an identifier before '*' */
local bool identaddr; /* there is an identifier before '&' */
local bool rbracenewline; /* '}' was on a new line */
local bool asserting; /* within assert() */
local bool varlist; /* declaring variable list */
local spctyp spi; /* index to config for spaces before next token */
local tokentyp token; /* current token */
local tokentyp lasttoken1;
local tokentyp lasttoken2;
local keytyp keyword; /* last keyword */
local keytyp levelkeys[maxlevel]; /* keywords that created the levels */
local keytyp statement; /* current statement (for learning) */
local int indents[maxlevel]; /* indentation of the levels */
local int indents1[maxlevel]; /* indentation of the levels */
local partyp parkinds[maxlevel]; /* kind of parenthesis */
local int parpos[maxlevel]; /* position of left parenthesis in output line */
local int ppiflevel[maxlevel]; /* level at #if or #else */
local int ppififlevel[maxlevel]; /* iflevel at #if or #else */
local int ppifparlevel[maxlevel]; /* parlevel at #if or #else */
local int ppifindent[maxlevel]; /* indent at #if or #else */
local int ppifindent1[maxlevel]; /* indent at #if or #else */
local int ppifnumid[maxlevel]; /* numid at #if or #else */
local int ppifnumassign[maxlevel]; /* numassign at #if or #else */
local int ppifexpr[maxlevel]; /* expr at #if or #else */
local int ppifoper[maxlevel]; /* oper at #if or #else */
local int ppifinslbrace[maxlevel]; /* inslbrace at #if or #else */
local int ppifinsrbrace[maxlevel]; /* insrbrace at #if or #else */
local int ppifnewstat[maxlevel]; /* insrbrace at #if or #else */
local byte id[512];
local byte ilinbuf[2048];
local byte olinbuf[2048];
local char progname[_MAX_FNAME];
local char confname[_MAX_PATH];
local char tempname[_MAX_PATH];
local char backname[_MAX_PATH];
local char ibuf[16384],obuf[16384];
local FILE *ifil,*ofil;
local FILE* con; /* file handle for displayed text (stdout or stderr) */
local counttyp spacecounter[spcmax];
local counttyp indentcounter[2][indmax][maxindlevel];
local counttyp indentcounter1[2][indmax][maxindlevel];
local counttyp funcpar; /* indentation of continuation of function */
local counttyp declpar; /* indentation of continuation of function decl */
local counttyp ifpar; /* indentation of continuation of if */
local counttyp asgnpar; /* indentation of continuation of assignment */
local counttyp casepar; /* indentation of statement on same line as 'case x:' */
local sumtyp indentsum[2]; /* sums for calculating indentation of levels */
local sumtyp indentsum1[2]; /* sums for calculating indentation of levels */


local int
divide(long tot, long n)
 {
  if (n<0)
   {
    n=-n;
    tot=-tot;
   }
  if (tot>=0)
    return((int)((tot+n/2)/n));
  else
    return(-(int)((-tot+n/2)/n));
 }


local void
count(counttyp* cntp, int val)
 {
  cntp->cnt_sum0++;
  cntp->cnt_sum1+=val;
  cntp->cnt_sum2+=(long)val*val;
 }


local void
sum(sumtyp* sump, int x, int y)
 {
  sump->sum_n++;
  sump->sum_x+=x;
  sump->sum_y+=y;
  sump->sum_xx+=(long)x*x;
  sump->sum_yy+=(long)y*y;
  sump->sum_xy+=(long)x*y;
 }


local void
countindent(indtyp i, int ind, int ind1)
 {
  if ((unsigned)switchlevel<2)
   {
    if (switchlevel==0 && i<=ind_levelstruct)
     {
      if (ind>0)
	sum(&indentsum[i],ind,iindent);
      if (ind1>0)
	sum(&indentsum1[i],ind1,iindent);
     }
    else
     {
      if ((unsigned)ind<maxindlevel)
	count(&indentcounter[switchlevel][i][ind],iindent);
      if ((unsigned)ind1<maxindlevel)
	count(&indentcounter1[switchlevel][i][ind1],iindent);
     }
   }
 }


local void
writes(char* s)
 {
  fwrite(s,1,strlen(s),con);
 }


local void
getln(void)
 {
  int c;

  formfeed1=formfeed;
  formfeed=false;
  if (feof(ifil))
   {
    ilinbuf[0]=0;
    ilinbuf[1]=0;
    ilinlen=2;
   }
  else
   {
    ilinlen=0;
    while ((c=getc(ifil))!='\n')
     {
      if (c=='\f')
       {
	formfeed=true;
	break;
       }
      if (c==EOF || ilinlen>=sizeof(ilinbuf)-3)
	break;
      ilinbuf[ilinlen++]=c;
     }
    while (ilinlen>0 && isspace(ilinbuf[ilinlen-1]))
      ilinlen--;
   }
  ilinbuf[ilinlen++]='\n';
  ilinbuf[ilinlen]=' ';
  ilini=0;
  c1=ilinbuf[0];
 }


local int
get(void)
 {
  if (ilini>=ilinlen)
    getln();
  c0=ilinbuf[ilini++];
  c1=ilinbuf[ilini];
  switch (c0)
   {
    case '\t':
      x+=itab-x%itab;
      break;
    case '\n':
      x=0;
      break;
    case 0:
      if (feof(ifil))
       {
	c0=EOF;
	c1=EOF;
	return(EOF);
       }
      else
       {
	c0=' ';
	return(' ');
       }
    default:
      x++;
   }
  return(c0);
 }


local void
skipspaces(void)
 {
  int x1;

  if (ilini>=ilinlen)
    getln();
  x1=x;
  withtab=false;
  while (c1==' ' || c1=='\t')
   {
    get();
    if (c0=='\t')
      withtab=true;
   }
  tokx=x
#ifndef COMMENTFIX
    -iindent
#endif /*COMMENTFIX*/
    ;
  readspaces=x-x1;
 }


local void
savestate(void)
 {
  oldilini=ilini;
  oldx=x;
  oldreadspaces=readspaces;
  oldtokx=tokx;
  oldwithtab=withtab;
  oldc0=c0;
  oldc1=c1;
  oldop0=op0;
  oldop1=op1;
  oldop2=op2;
 }


local void
restorestate(void)
 {
  ilini=oldilini;
  x=oldx;
  readspaces=oldreadspaces;
  tokx=oldtokx;
  withtab=oldwithtab;
  c0=oldc0;
  c1=oldc1;
  op0=oldop0;
  op1=oldop1;
  op2=oldop2;
 }


local void
put(int c)
 {
  if (olinlen<sizeof(olinbuf))
    olinbuf[olinlen++]=c;
 }


local void
adjustfuncspaces(spctyp spci)
 {
  int i;
  int k;

  i=parpos[parlevel];
  if (i>=0)
   {
    k=2*(spc[spc_par]-spc[spc_funcpar]);
    if (k!=0 && (unsigned)(olinlen+k)<=sizeof(olinbuf))
     {
      memmove(olinbuf+i+1+spc[spc_par],olinbuf+i+1+spc[spc_funcpar],
	olinlen-i-1-spc[spc_funcpar]);
      memset(olinbuf+i+1,' ',spc[spc_par]);
      olinlen+=k;
      memset(olinbuf+olinlen-1-spc[spc_par],' ',spc[spc_par]);
      olinbuf[olinlen-1]=')';
     }
    k=1-spc[spci];
    if (k!=0 && olinlen>=i && (unsigned)(olinlen+k)<=sizeof(olinbuf))
     {
      memmove(olinbuf+i+k,olinbuf+i,olinlen-i);
      if (spc[spci]==0)
	olinbuf[i]=' ';
      olinlen+=k;
     }
   }
 }


local void
adjustretspaces(void)
 {
  int i;
  int k;

  i=parpos[parlevel];
  if (i>=0)
   {
    k=spc[spc_return]-spc[spc_returnpar];
    if (k!=0 && olinlen>=i && (unsigned)(olinlen+k)<=sizeof(olinbuf))
     {
      memmove(olinbuf+i+k,olinbuf+i,olinlen-i);
      if (k>0)
	memset(olinbuf+i,' ',k);
      olinlen+=k;
     }
   }
 }


local void
adjustcastspaces(void)
 {
  int i;
  int k;

  i=parpos[parlevel];
  if (i>=0)
   {
    k=2*(spc[spc_castpar]-spc[spc_par]);
    if (k!=0 && (unsigned)(olinlen+k)<=sizeof(olinbuf))
     {
      memmove(olinbuf+i+1+spc[spc_castpar],olinbuf+i+1+spc[spc_par],
	olinlen-i-1-spc[spc_par]);
      memset(olinbuf+i+1,' ',spc[spc_castpar]);
      olinlen+=k;
      memset(olinbuf+olinlen-1-spc[spc_castpar],' ',spc[spc_castpar]);
      olinbuf[olinlen-1]=')';
     }
   }
 }


local void
adjustindir(int sp1, int sp2, int c)
 {
  int i;

  i=indirpos-sp1+1;
  if (i>0 && (unsigned)(olinlen-sp1+1)<=sizeof(olinbuf))
   {
    memmove(olinbuf+i,olinbuf+indirpos,olinlen-indirpos);
    olinbuf[i-1]=' ';
    olinlen-=sp1-1;
    while (olinbuf[i]==c && i<olinlen)
      i++;
    memmove(olinbuf+i,olinbuf+i+sp2,olinlen-i-sp2);
    olinlen-=sp2;
   }
  indirpos=-1;
 }


local bool
structure(void)
 {
  return structdecl|structbody|enumdecl|(expr&2);
 }


local void
putln(void)
 {
  int i;
  int xact;
  int x1;
  int olini;

  if (caselabel && token!=tok_endline && token!=tok_comment
    && token!=tok_cppcomment)
    caselabel=0;
  commentoindent=-1;
  indirpos=-1;
  memset(parpos,-1,sizeof(parpos));

  if (learn)
   {
    if (formfeed1)
      withformfeed=true;
    olinlen=0;
    oindent=0;
    return;
   }

  while (olinlen>0 && isspace(olinbuf[olinlen-1]))
    olinlen--;

  if (olinlen>0)
   {
    for (i=oindent/otab; i>0; i--)
      putc('\t',ofil);
    for (i=oindent%otab; i>0; i--)
      putc(' ',ofil);
    xact=oindent;

    for (olini=0; olini<olinlen; olini++)
     {
      if (olinbuf[olini]==0)
       {
	olini++;
	commentoindent=
#ifndef COMMENTFIX
	  oindent+
#endif /*COMMENTFIX*/
	  *(short*)&olinbuf[olini];
	x1=commentoindent-commentoindent%otab;
	olini++;
	if (xact<commentoindent)
	 {
	  while (xact<x1)
	   {
	    putc('\t',ofil);
	    xact+=otab-xact%otab;
	   }
	  while (xact<commentoindent)
	   {
	    putc(' ',ofil);
	    xact++;
	   }
	 }
	else
	 {
	  putc(' ',ofil);
	  xact++;
	 }
       }
      else
       {
	putc(olinbuf[olini],ofil);
	if (olinbuf[olini]=='\t')
	  xact+=otab-xact%otab;
	else
	  xact++;
       }
     }
    olinlen=0;
   }
  oindent=0;
  if (formfeed1 && withformfeed)
    putc('\f',ofil);
  else
    putc('\n',ofil);
 }


local void
putspaces1(int n, int tabi)
 {
  if (asserting && lasttoken1!=tok_lpar)
   {
    for (n=readspaces; n>0; n--)
      put(' ');
   }
  else
   {
    if (learn && !first)
     {
      if ((newline&nl_always) && (token!=tok_keyword || keyword!=key_break
	|| lasttoken1!=tok_colon))
	alwaysnewline=false;
      if (braceonly)
	if (structure())
	  bracealwaysnewlinestruct=false;
	else
	  bracealwaysnewline=false;
     }
    if (newline)
     {
      if (!(newline&nl_noputln))
	putln();
      oindent=indent;
      newline=0;
     }
    else if (learn)
     {
      if (withtab)
	tab[tabi]=true;
     }
    else if (withtab && tab[tabi])
     {
      put(0);
      put((byte)tokx);
      put(*((byte*)&tokx+1));
      withtab=false;
     }
    else
      for ( ; n>0; n--)
	put(' ');
   }
  spaces=0;
  spi=spc_null;
 }


local void
putspaces(spctyp spci)
 {
  tabtyp tabi;

  if (spci!=spc_null)
   {
    if (expr&2)
      tabi=tab_init;
    else
      tabi=tab_other;
    if (!(newline&nl_start) && learn && (!withtab || spci==spc_brace))
      count(&spacecounter[spci],readspaces);
    putspaces1(spc[spci],tabi);
   }
  else
    putspaces1(spaces,tab_other);
 }


local void
putid(void)
 {
  int i;

  for (i=0; id[i]; i++)
    put(id[i]);
 }


local void
putop(void)
 {
  put(op0);
  if (op1)
   {
    put(op1);
    op1=0;
   }
  if (op2)
   {
    put(op2);
    op2=0;
   }
 }


local void
putopsp(spctyp i)
 {
  putspaces(i);
  putop();
  spi=i;
 }


local void
putunaryopsp(spctyp i)
 {
  putspaces(spi);
  putop();
  spi=i;
 }


local void
copyline(void)
 {
  int x0;
  int x1;
  int xact;
  bool tab;
  bool quote;
  bool comment;

  oindent=iindent;
  xact=oindent+olinlen;
  tab=false;
  quote=false;
  comment=false;
  while (c1!=EOF && (quote || comment || c1!='\n' || c0=='\\'))
   {
    x0=x+oindent-iindent;
    get();
    switch (c0)
     {
      case '\n':
	putln();
	iindent=0;
	xact=0;
	break;
      case '\t':
	tab=true;
      case ' ':
	break;
      default:
	x1=x0-x0%otab;
	if (!tab)
	  x1--;
	tab=false;
	while (xact<x1)
	 {
	  put('\t');
	  xact+=otab-xact%otab;
	 }
	while (xact<x0)
	 {
	  put(' ');
	  xact++;
	 }
	if (quote)
	 {
	  if (c0=='"')
	    quote=false;
	 }
	else if (comment)
	 {
	  if (c0=='*' && c1=='/')
	    comment=false;
	 }
	else if (c0=='"')
	  quote=true;
	else if (c0=='/' && c1=='*')
	  comment=true;
	put(c0);
	xact++;
     }
   }
  newline=nl_start|nl_noputln;
  x=0;
 }


local int
getox(void)
 {
  int i;
  int x1;

  x1=0;
  for (i=0; i<olinlen; i++)
   {
    if (olinbuf[i]!=0)
      x1++;
    else
     {
      i++;
      x1=*(short*)&olinbuf[i];
      i++;
     }
   }
  return(x1);
 }


local void
putind(int newx)
 {
  while (ox<newx)
   {
    putc(' ',ofil);
    ox++;
   }
 }


local void
putnsp(int n)
 {
  while (n-->0)
   {
    putc(' ',ofil);
    ox++;
   }
 }


local void
putchr0(int c)
 {
  putc(c,ofil);
  if (c=='\n')
    ox=0;
  else
    ox++;
 }


local void
puttab(void)
 {
  putc('\t',ofil);
  ox+=otab-ox%otab;
 }


local void
putindchr(int n, int c)
 {
  putind(n);
  putchr0(c);
 }


local void
putindstr(int n, char* s)
 {
  putind(n);
  while (*s)
    putchr0(*s++);
 }


local void
puttabstr(char* s)
 {
  puttab();
  while (*s)
    putchr0(*s++);
 }


local void
putchr(int n, int c)
 {
  putnsp(n);
  putchr0(c);
 }


local void
putstr(int n, char* s)
 {
  putnsp(n);
  while (*s)
    putchr0(*s++);
 }


local void
putlbrace(int nsp, int brace)
 {
  if (bracesameline[brace]>0)
    putstr(nsp,"{\n");
  else
   {
    putchr0('\n');
    putindstr(ind[ind_level0]+ind[ind_level]+ind[ind_lbrace],"{\n");
   }
 }


local void
putrbrace(void)
 {
  putindstr(ind[ind_level0]+ind[ind_level]+ind[ind_rbrace],"}\n");
 }


local void
putlbrace0(int nsp)
 {
  if (bracesameline[brace_level0]>0)
    putstr(nsp,"{\n");
  else
   {
    putchr0('\n');
    putindstr(ind[ind_lbrace0],"{\n");
   }
 }


local void
putrbrace0(void)
 {
  putindstr(ind[ind_rbrace0],"}\n");
 }


local void
putlbracestruct(int nsp)
 {
  if (bracesameline[brace_struct]>0)
    putstr(nsp,"{\n");
  else
   {
    putchr0('\n');
    putindstr(ind[ind_levelstruct0]+ind[ind_levelstruct]+ind[ind_lbstruct],
      "{\n");
   }
 }


local void
putrbracestruct(void)
 {
  putindstr(ind[ind_levelstruct0]+ind[ind_levelstruct]+ind[ind_rbstruct],
    "}\n");
 }


local void
putlbracestruct0(int nsp, int brace)
 {
  if (bracesameline[brace]>0)
    putstr(nsp,"{\n");
  else
   {
    putchr0('\n');
    putindstr(ind[ind_lbstruct],"{\n");
   }
 }


local void
putrbracestruct0(void)
 {
  putindstr(ind[ind_rbstruct],"};\n");
 }


local void
writeconfig(void)
 {
  int i;
  int k;
  int n;
  int ind1;
  int brindent;
  int levindent1;
  int levindent2;
  int levindent3;

  brindent=braceinclevel?ind[ind_level]:0;

  ofil=fopen(confname,"w");
  if (ofil==NULL)
   {
    perror(confname);
    return;
   }
  setvbuf(ofil,obuf,_IOFBF,sizeof(obuf));

  putstr(0,"template");
  putchr(spc[spc_templdecl],'<');
  putstr(spc[spc_angle],"class T,");
  putstr(spc[spc_templdeclcomma],"class U");
  putstr(spc[spc_angle],">\n");

  putstr(0,"class X");
  if (colonstructsameline>=0)
    putchr(spc[spc_classcolon1],':');
  else
   {
    putchr0('\n');
    putindchr(ind[ind_colonstruct],':');
   }
  putstr(spc[spc_classcolon2],"public Y");
  putlbracestruct0(spc[spc_elsebrace],brace_struct);

  levindent1=ind[ind_levelstruct0]+ind[ind_levelstruct];

  putindstr(levindent1,"T t");
  putstr(spc[spc_index],"[]");
  putchr(spc[spc_assign],'=');
  putstr(spc[spc_assign],"{0,");
  putstr(spc[spc_initcomma],"1,");
  if (tab[tab_init])
    puttabstr("2};");
  else
    putstr(spc[spc_initcomma],"2};");
  if (tab[tab_comment])
    puttabstr("//\n");
  else
    putstr(spc[spc_comment],"//\n");

  putindstr(levindent1,"struct");
  putlbracestruct(spc[spc_elsebrace]);
  levindent2=levindent1+ind[ind_levelstruct];
  putindstr(levindent2+ind[ind_structlab],"public:\n");
  putindstr(levindent2,"int i;\n");
  putrbracestruct();

  putrbracestruct0();

  putchr(0,'X');
  putchr(spc[spc_templcall],'<');
  putstr(spc[spc_angle],"int,");
  putstr(spc[spc_templcallcomma],"char");
  putstr(spc[spc_angle],"> v;\n");

  putstr(0,"int n");
  putstr(spc[spc_index],"[]");
  putchr(spc[spc_assign],'=');
  putlbracestruct0(spc[spc_assign],brace_init);
  putindstr(levindent1,"0\n");
  putrbracestruct0();

  putstr(0,"int a,\n");
  putindstr(ind[ind_varlist],"b;\n");

  putstr(0,"void calculate");
  putchr(spc[spc_funcdecl],'(');
  putstr(spc[spc_funcpar],"int a,\n");
  if (aligndeclind)
    ind1=14;
  else
    ind1=0;
  putindstr(ind[ind_decl]+ind1,"int b");
  putstr(spc[spc_funcpar],");\n");

  levindent1=ind[ind_level0]+ind[ind_level];
  levindent2=levindent1+ind[ind_level];
  levindent3=levindent2+ind[ind_level];

  if (withformfeed)
    putstr(0,"\f\n");

  putstr(0,"x::x");
  putchr(spc[spc_funcdecl],'(');
  putchr(spc[spc_funcnopar],')');
  if (colonfuncsameline>=0)
    putchr(spc[spc_classcolon1],':');
  else
   {
    putchr0('\n');
    putindchr(ind[ind_colonfunc],':');
   }
  putchr(spc[spc_classcolon2],'a');
  putchr(spc[spc_funccall],'(');
  putchr(spc[spc_funcnopar],')');
  putlbrace0(spc[spc_rpar]);
  putindstr(levindent1,"return");
  putstr(spc[spc_return],"0;\n");
  putrbrace0();

  putstr(0,"int f");
  putchr(spc[spc_funcdecl],'(');
  putstr(spc[spc_funcpar],"int");
  putchr(spc[spc_addr],'&');
  putstr(spc[spc_addr1],"a,");
  putstr(spc[spc_declcomma],"int");
  putchr(spc[spc_indir],'*');
  putchr(spc[spc_indir1],'b');
  putchr(spc[spc_funcpar],')');
  putlbrace0(spc[spc_rpar]);

  putindchr(levindent1,'a');
  if (tab[tab_other])
    puttabstr("b;\n");
  else
    putstr(1,"b;\n");

  putindstr(levindent1,"stat1;");
  if (alwaysnewline)
   {
    putchr(0,'\n');
    putindstr(levindent1,"stat2;\n");
   }
  else
    putstr(spc[spc_semi],"stat2;\n");

  putindstr(levindent1,"if");
  putchr(spc[spc_if],'(');
  putchr(spc[spc_ifpar],'!');
  putstr(spc[spc_boolnot],"ok");
  putstr(spc[spc_log],"&&");
  putchr(spc[spc_log],'i');
  putstr(spc[spc_comp],"==");
  putchr(spc[spc_comp],'k');
  putchr(spc[spc_bit],'&');
  putchr(spc[spc_bit],'~');
  putstr(spc[spc_bitnot],"2\n");

  if (alignifind)
    ind1=2;
  else
    ind1=0;
  putindstr(levindent1+ind[ind_if]+ind1,"||");
  putchr(spc[spc_log],'x');
  putchr(spc[spc_comp],'>');
  putchr(spc[spc_comp],'0');
  putchr(spc[spc_ifpar],')');
  putlbrace(spc[spc_ifbrace],brace_level);

  putindstr(levindent2+brindent,"for");
  putchr(spc[spc_if],'(');
  putchr(spc[spc_emptyfor1],';');
  putchr(spc[spc_emptyfor2],';');
  putstr(spc[spc_emptyfor3],")\n");

  putindstr(levindent3+brindent,"newresult");
  putchr(spc[spc_assign],'=');
  putchr(spc[spc_assign],'(');
  putchr(spc[spc_par],'b');
  putchr(spc[spc_index],'[');
  putchr(spc[spc_bracket],'i');
  putchr(spc[spc_addind],'+');
  putchr(spc[spc_addind],'1');
  putchr(spc[spc_bracket],']');
  putchr(spc[spc_op],'+');
  putchr(spc[spc_op],'k');
  putchr(spc[spc_par],')');
  putchr(spc[spc_op],'*');
  putstr(spc[spc_op],"n\n");

  if (alignassignind)
    ind1=10+spc[spc_assign];
  else
    ind1=0;
  putindchr(levindent3+ind[ind_assign]+ind1+brindent,'+');
  putstr(spc[spc_op],"static_cast");
  putchr(spc[spc_cppcast],'<');
  putstr(spc[spc_angle],"int");
  putchr(spc[spc_angle],'>');
  putchr(spc[spc_castcall],'(');
  putchr(spc[spc_funcpar],'x');
  putstr(spc[spc_funcpar],");\n");

  putindchr(levindent1+ind[ind_rbrace],'}');
  if (elsesameline>0)
    putstr(1,"else");
  else
   {
    putchr0('\n');
    putindstr(levindent1,"else");
   }
  putlbrace(spc[spc_elsebrace],brace_level);

  putindstr(levindent2+brindent,"while");
  putchr(spc[spc_if],'(');
  putstr(spc[spc_ifpar],"ok\n");

  if (alignifind)
    ind1=5;
  else
    ind1=0;
  putindstr(levindent2+ind[ind_if]+ind1+brindent,"&&");
  putchr(spc[spc_log],'i');
  putchr(spc[spc_comp],'>');
  putchr(spc[spc_comp],'0');
  putstr(spc[spc_ifpar],")\n");

  putindchr(levindent3+brindent,'f');
  putchr(spc[spc_funccall],'(');
  putchr(spc[spc_funcpar],'&');
  putstr(spc[spc_addr2],"a,");
  putchr(spc[spc_callcomma],'*');
  putchr(spc[spc_indir2],'b');
  putstr(spc[spc_funcpar],");\n");
  putrbrace();

  putindstr(levindent1,"switch");
  putchr(spc[spc_if],'(');
  putchr(spc[spc_ifpar],'i');
  putchr(spc[spc_ifpar],')');
  putlbrace(spc[spc_ifbrace],brace_level);
  ind1=levindent2+ind[ind_switchstat];
  putindstr(ind1+ind[ind_caselabel],"case 0");
  putstr(spc[spc_casecolon1],":\n");

  putindchr(ind1,'k');
  putchr(spc[spc_assign],'=');
  putchr(spc[spc_assign],'x');
  putchr(spc[spc_cond],'?');
  putchr(spc[spc_cond],'0');
  putchr(spc[spc_cond],':');
  putstr(spc[spc_cond],"1;\n");
  putindstr(ind1,"break;\n");

  if (!aligncasestat)
    n=4;
  else
   {
    n=spc[spc_casecolon2]-spc[spc_casecolon1]-8;
    if (n>4)
      n=4;
    if (n<=0)
      n=1;
   }
  for (k=n; k>=0; k-=n)
   {
    putindstr(ind1+ind[ind_caselabel],"case 1");
    for (i=0; i<k; i++)
      putchr0('0');
    putchr(spc[spc_casecolon1],':');
    putstr(spc[spc_casecolon2]-(aligncasestat?spc[spc_casecolon1]+7+k:0),
      "break;\n");
   }
  putrbrace();

  putindstr(levindent1,"for");
  putchr(spc[spc_if],'(');
  putchr(spc[spc_ifpar],'i');
  putchr(spc[spc_assign],'=');
  putstr(spc[spc_assign],"0;");
  putstr(spc[spc_semi],"ok;");
  putstr(spc[spc_semi],"i++,");
  putstr(spc[spc_forcomma],"k++");
  putchr(spc[spc_ifpar],')');

  if (bracealwaysnewline || bracesameline[brace_level]>0)
   {
    putlbrace(spc[spc_ifbrace],brace_level);
    putindstr(levindent2+brindent,"calculate");
   }
  else
   {
    putchr0('\n');
    putindchr(levindent1+ind[ind_lbrace],'{');
    putstr(spc[spc_brace],"calculate");
   }
  putchr(spc[spc_funccall],'(');
  putstr(spc[spc_funcpar],"v1,\n");
  if (alignfuncind)
    ind1=9;
  else
    ind1=0;
  putindstr(levindent2+ind[ind_func]+ind1+brindent,"v2");
  putstr(spc[spc_funcpar],");\n");
  putrbrace();

  putindstr(levindent1,"do");
  putlbrace(spc[spc_elsebrace],brace_level);

  putindstr(levindent2+ind[ind_label]+brindent,"lab:\n");

  putindchr(levindent2+brindent,'x');
  putchr(spc[spc_assign],'=');
  putchr(spc[spc_assign],'-');
  putstr(spc[spc_unary],"y,");
  putchr(spc[spc_comma],'y');
  putchr(spc[spc_assign],'=');
  putchr(spc[spc_assign],'(');
  putstr(spc[spc_castpar],"int");
  putchr(spc[spc_typindir],'*');
  putchr(spc[spc_castpar],')');
  putstr(spc[spc_cast],"z;\n");

  putindchr(levindent1+ind[ind_rbrace],'}');
  if (whilesameline>0)
    putstr(1,"while");
  else
   {
    putchr0('\n');
    putindstr(levindent1,"while");
   }
  putchr(spc[spc_if],'(');
  putstr(spc[spc_ifpar],"ok");
  putstr(spc[spc_ifpar],");");
  putstr(spc[spc_comment],"//\n");

  putindstr(levindent1,"return");
  putchr(spc[spc_returnpar],'(');
  putchr(spc[spc_par],'1');
  putstr(spc[spc_par],");\n");

  putrbrace0();

  fclose(ofil);

  writes(confname);
  writes(" written\n");
 }


local tokentyp
gettoken(bool next)
 {
  int i;
  int c;
  bool val;

  skipspaces();

  if (next && c1=='\n')
    return(tok_endline);

  op0=get();
  op1=0;
  op2=0;

  switch (c0)
   {
    case -1:
      return(tok_endfile);

    case '=':
      switch (c1)
       {
	case '=':
	  op1=get();
	  return(tok_compare);
	default:
	  return(tok_assign);
       }

    case '!':
      switch (c1)
       {
	case '=':
	  op1=get();
	  return(tok_compare);
	default:
	  return(tok_boolnot);
       }

    case '~':
      return(tok_bitnot);

    case '<':
    case '>':
      switch (c1)
       {
	case '=':
	  op1=get();
	  return(tok_compare);
	case '<':
	case '>':
	  if (c1==c0)
	   {
	    op1=get();
	    if (c1=='=')
	     {
	      op2=get();
	      return(tok_assign);
	     }
	    else
	      return(tok_operator);
	   }
	  /* fall thru */
	default:
	  if (!oper)
	    if (c0=='<')
	      return(tok_langle);
	    else
	      return(tok_rangle);
	  else
	    return(tok_compare);
       }

    case '|':
    case '&':
      switch (c1)
       {
	case '=':
	  op1=get();
	  return(tok_assign);
	case '|':
	case '&':
	  if ((oper || c0=='|') && c1==c0)
	   {
	    op1=get();
	    return(tok_logical);
	   }
	  /* contiued */
	default:
	  if (c0=='&' && (!oper || lasttoken1==tok_rpar && cast))
	    return(tok_address);
	  else
	    return(tok_bitop);
       }

    case '-':
      switch (c1)
       {
	case '>':
	  op1=get();
	  if (c1=='*')
	    op2=get();
	  return(tok_component);
	case '-':
	  op1=get();
	  return(tok_incdec);
	case '=':
	  op1=get();
	  return(tok_assign);
	default:
	  if (!oper)
	    return(tok_unary);
	  else if (bracketlevel!=0)
	   {
	    if (isdigit(c1))
	      return(tok_addind);
	    else
	     {
	      for (i=ilini+1; i<ilinlen && isspace(ilinbuf[i]); i++)
		;
	      if (i<ilinlen && isdigit(ilinbuf[i]))
		return(tok_addind);
	      else
		return(tok_operator);
	     }
	   }
	  else
	    return(tok_operator);
       }

    case '+':
      switch (c1)
       {
	case '+':
	  op1=get();
	  return(tok_incdec);
	case '=':
	  op1=get();
	  return(tok_assign);
	default:
	  if (!oper)
	    return(tok_unary);
	  else if (bracketlevel!=0)
	   {
	    if (isdigit(c1))
	      return(tok_addind);
	    else
	     {
	      for (i=ilini+1; i<ilinlen && isspace(ilinbuf[i]); i++)
		;
	      if (i<ilinlen && isdigit(ilinbuf[i]))
		return(tok_addind);
	      else
		return(tok_operator);
	     }
	   }
	  else
	    return(tok_operator);
       }

    case '*':
      switch (c1)
       {
	case '=':
	  op1=get();
	  return(tok_assign);
	default:
	  if (!oper || lasttoken1==tok_rpar && cast)
	    return(tok_indir);
	  else
	   {
	    if (parlevel>0)
	     {
	      val=false;
	      for (i=ilini; i<ilinlen; i++)
	       {
		c=ilinbuf[i];
		if (isspace(c))
		  continue;
		if (c=='*')
		 {
		  val=false;
		  continue;
		 }
		if (isalnum(c) || c=='_' || c=='$')
		 {
		  val=true;
		  continue;
		 }
		if (c==',')
		 {
		  if (!val)
		    return(tok_typindir);
		 }
		else if (c==')')
		 {
		  if (!val)
		    return(tok_typindir);
		  for (i++; i<ilinlen; i++)
		   {
		    c=ilinbuf[i];
		    if (isspace(c))
		      continue;
		    if (isalnum(c) || c=='_' || c=='$' || c=='(')
		      return(tok_typindir);
		    break;
		   }
		 }
		break;
	       }
	     }
	    if (bracketlevel!=0 && isdigit(c1))
	      return(tok_addind);
	    else
	      return(tok_operator);
	   }
       }

    case '%':
      switch (c1)
       {
	case '=':
	  op1=get();
	  return(tok_assign);
	default:
	  if (bracketlevel!=0 && isdigit(c1))
	    return(tok_addind);
	  else
	    return(tok_operator);
       }

    case '^':
      switch (c1)
       {
	case '=':
	  op1=get();
	  return(tok_assign);
	default:
	  return(tok_bitop);
       }

    case '/':
      switch (c1)
       {
	case '=':
	  op1=get();
	  return(tok_assign);
	case '/':
	  op1=get();
	  return(tok_cppcomment);
	case '*':
	  op1=get();
	  return(tok_comment);
	default:
	  if (bracketlevel!=0 && isdigit(c1))
	    return(tok_addind);
	  else
	    return(tok_operator);
       }

    case '.':
      switch (c1)
       {
	case '*':
	  op1=get();
	  return(tok_component);
	case '.':
	  op1=get();
	  if (c1=='.')
	    op2=get();
	  return(tok_dots);
	default:
	  if (isdigit(c1))
	    goto number;
	  return(tok_component);
       }

    case ':':
      switch (c1)
       {
	case ':':
	  op1=get();
	  return(tok_scope);
	default:
	  if (expr && !caselabel)
	    return(tok_condition);
	  else
	    return(tok_colon);
       }

    case '?':
      return(tok_condition);

    case ';':
      return(tok_semicolon);

    case ',':
      return(tok_comma);

    case '{':
      return(tok_lbrace);

    case '}':
      return(tok_rbrace);

    case '(':
      return(tok_lpar);

    case ')':
      return(tok_rpar);

    case '[':
      return(tok_lbracket);

    case ']':
      return(tok_rbracket);

    case '\n':
      return(tok_endline);

    case '"':
    case '\'':
      return(tok_quote);

    case 'L':
      if (c1=='"' || c1=='\'')
	return(tok_wquote);
      /* fall thru */

    default:
      if (isalpha(c0) || c0=='_' || c0=='$')
       {
	id[0]=c0;
	idlen=1;
	while (isalnum(c1) || c1=='_' || c1=='$')
	  id[idlen++]=get();
	id[idlen]=0;
	if (strcmp(id,"if")==0)
	  keyword=key_if;
	else if (strcmp(id,"else")==0)
	  keyword=key_else;
	else if (strcmp(id,"while")==0)
	  keyword=key_while;
	else if (strcmp(id,"for")==0)
	  keyword=key_for;
	else if (strcmp(id,"return")==0)
	  keyword=key_return;
	else if (strcmp(id,"break")==0)
	  keyword=key_break;
	else if (strcmp(id,"continue")==0)
	  keyword=key_continue;
	else if (strcmp(id,"switch")==0)
	  keyword=key_switch;
	else if (strcmp(id,"case")==0 || strcmp(id,"default")==0)
	  keyword=key_case;
	else if (strcmp(id,"do")==0)
	  keyword=key_do;
	else if (strcmp(id,"new")==0)
	  keyword=key_new;
	else if (strcmp(id,"delete")==0)
	  keyword=key_delete;
	else if (strcmp(id,"goto")==0)
	  keyword=key_goto;
	else if (strcmp(id,"operator")==0)
	  keyword=key_operator;
	else if (strcmp(id,"template")==0)
	  keyword=key_template;
	else if (strcmp(id,"const_cast")==0
	  || strcmp(id,"dynamic_cast")==0
	  || strcmp(id,"static_cast")==0
	  || strcmp(id,"reinterpret_cast")==0)
	  keyword=key_cppcast;
	else if (strcmp(id,"asm")==0 || strcmp(id,"__asm")==0)
	  keyword=key_asm;
	else if (strcmp(id,"catch")==0)
	  keyword=key_catch;
	else if (strcmp(id,"assert")==0)
	  keyword=key_assert;
	else
	  return(tok_ident);
	return(tok_keyword);
       }
      else if (isdigit(c0))
       {
	number:

	id[0]=c0;
	idlen=1;
	while (isdigit(c1))
	  id[idlen++]=get();
	if (idlen==1 && c0=='0' && ((c1|0x20)=='x' || (c1|0x20)=='b'))
	 {
	  do
	    id[idlen++]=get();
	  while (isxdigit(c1));
	  while (strchr("lLuU",c1)!=NULL)
	    id[idlen++]=get();
	 }
	else
	 {
	  if (c1=='.')
	   {
	    do
	      id[idlen++]=get();
	    while (isdigit(c1));
	   }
	  if ((c1|0x20)=='e')
	   {
	    id[idlen++]=get();
	    if (c1=='-' || c1=='+')
	      id[idlen++]=get();
	    while (isdigit(c1))
	      id[idlen++]=get();
	   }
	  while (strchr("fFlLuU",c1)!=NULL)
	    id[idlen++]=get();
	 }
	id[idlen]=0;
	return(tok_number);
       }
      else
	return(tok_char);
   }
 }


local tokentyp
nexttoken(void)
 {
  tokentyp token1;

  savestate();
  token1=gettoken(true);
  restorestate();
  return(token1);
 }


local bool
isbrace(int lev)
 {
  return(levelkeys[lev]>=key_brace && levelkeys[lev]<=key_brace3);
 }


local void
incindent(void)
 {
  if (structure())
   {
    if (indent==0)
      indent+=ind[ind_levelstruct0];
    indent+=ind[ind_levelstruct];
   }
  else
   {
    if (indent==0)
      indent+=ind[ind_level0];
    indent+=ind[ind_level];
   }
 }


local void
process(bool learning)
 {
  int i;
  int indent2;
  int indent3;
  int newindent;
  int bracelevel1;
  int outx;
  int quote;
  int templnumid; /* numid at '<' of template class name */
  bool ok;
  bool structure1;
  bool newline1;
  bool funcdecl; /* within () of function declaration */
  bool templdecl; /* within <> of template declaration */
  bool templcall; /* within <> of template class name */
  bool cppcast; /* within <> of xxx_cast<> */
  tokentyp token1;
  tokentyp lasttoken;
  keytyp lastkeyword;
  partyp parkind;

  learn=learning;
  if (learn)
   {
    ind[ind_level]=1;
    ind[ind_levelstruct]=1;
   }

  lasttoken=tok_endline;
  lasttoken1=tok_endline;
  lasttoken2=tok_endline;
  lastkeyword=key_null;
  newline=nl_start|nl_noputln;
  newstat=true;
  numid=0;
  numid1=0;
  numassign=0;
  formfeed=false;
  formfeed1=false;
  funcdecl=false;
  templdecl=false;
  templcall=false;
  cppcast=false;
  structdecl=false;
  structbody=false;
  enumdecl=0;
  expr=0;
  oper=false;
  parlevel=0;
  caselabel=0;
  bracketlevel=0;
  indirpos=-1;
  commentiindent=-1;
  commentoindent=-1;
  memset(parpos,-1,sizeof(parpos));
  getln();
  x=0;
  lbrace=-1;
  indent=0;
  indent1=0;
  assigning=0;
  varlist=false;
  switchlevel=0;
  level=0;
  extlevel=0;
  structlevel=-2;

  while (lasttoken!=tok_endfile)
   {
    if (newline&nl_start)
     {
      /* skip and count spaces */
      skipspaces();
      iindent=x;

      if (c1=='#' || disabled) /* preprocessor command or disabled */
       {
	if (c1=='#') /* preprocessor command */
	 {
	  savestate();
	  get();
	  *id=0;
	  gettoken(false);
	  if (strcmp(id,"if")==0 || strcmp(id,"ifdef")==0
	    || strcmp(id,"ifndef")==0)
	   {
	    ppiflevel[ppiflev]=level;
	    ppififlevel[ppiflev]=iflevel;
	    ppifparlevel[ppiflev]=parlevel;
	    ppifindent[ppiflev]=indent;
	    ppifindent1[ppiflev]=indent1;
	    ppifnumid[ppiflev]=numid;
	    ppifnumassign[ppiflev]=numassign;
	    ppifexpr[ppiflev]=expr;
	    ppifoper[ppiflev]=oper;
	    ppifinslbrace[ppiflev]=inslbrace;
	    ppifinsrbrace[ppiflev]=insrbrace;
	    ppifnewstat[ppiflev]=newstat;
	    if (ppiflev<maxlevel-1)
	      ppiflev++;
	   }
	  else if (strcmp(id,"else")==0 || strcmp(id,"elif")==0)
	   {
	    if (ppiflev>0)
	     {
	      level=ppiflevel[ppiflev-1];
	      iflevel=ppififlevel[ppiflev-1];
	      parlevel=ppifparlevel[ppiflev-1];
	      indent=ppifindent[ppiflev-1];
	      indent1=ppifindent1[ppiflev-1];
	      numid=ppifnumid[ppiflev-1];
	      numassign=ppifnumassign[ppiflev-1];
	      expr=ppifexpr[ppiflev-1];
	      oper=ppifoper[ppiflev-1];
	      inslbrace=ppifinslbrace[ppiflev-1];
	      insrbrace=ppifinsrbrace[ppiflev-1];
	      newstat=ppifnewstat[ppiflev-1];
	     }
	   }
	  else if (strcmp(id,"endif")==0)
	   {
	    if (ppiflev>0)
	      ppiflev--;
	   }
	  else if (strcmp(id,"pragma")==0)
	   {
	    *id=0;
	    gettoken(false);
	    if (strcmp(id,"asm")==0)
	      disabled|=disable_pragma_asm;
	    else if (strcmp(id,"endasm")==0)
	      disabled&=~disable_pragma_asm;
	    else if (strcmp(id,"nicec")==0)
	     {
	      *id=0;
	      gettoken(false);
	      if (strcmp(id,"on")==0)
		disabled&=~disable_pragma_off;
	      else if (strcmp(id,"off")==0)
		disabled|=disable_pragma_off;
	     }
	   }
	  else if (strcmp(id,"asm")==0)
	    disabled|=disable_asm;
	  else if (strcmp(id,"endasm")==0)
	    disabled&=~disable_asm;
	  restorestate();
	 }

	if (!(newline&nl_noputln))
	  putln();
	copyline();
	lasttoken=tok_endline;
	lasttoken1=tok_endline;
	lasttoken2=tok_endline;
	if (disabled)
	 {
	  getln();
	  putln();
	 }
	continue;
       }
      first=true;
      indentbeg=indent;
     }
    else
      first=false;

    if (inslbrace)
     {
      savestate();
      token=gettoken(false);
      switch (token)
       {
	case tok_comment:
	case tok_cppcomment:
	case tok_endline:
	  break;
	case tok_lbrace:
	  inslbrace=false;
	  break;
	default:
	  restorestate();
	  token=tok_inslbrace;
	  inslbrace=false;
       }
     }
    else if (insrbrace)
     {
      savestate();
      token=gettoken(false);
      switch (token)
       {
	case tok_comment:
	case tok_cppcomment:
	  if (!first)
	    break;
	  /* else continued */
	case tok_endline:
	  if (insrbrace==1 && iflevel>extlevel)
	    break;
	  /* else continued */
	default:
	  insrbrace=1;
	  bracelevel1=bracelevel;
	  if (token==tok_keyword)
	   {
	    switch (keyword)
	     {
	      case key_else:
		if (iflevel>extlevel)
		  bracelevel1=iflevel;
		break;
	      case key_while:
		/* check if 'while' belongs to 'do' */
		if (levelkeys[level]==key_do)
		  bracelevel1=level;
		break;
	     }
	   }
	  if (level>bracelevel1)
	   {
	    restorestate();
	    token=tok_rbrace;
	   }
	  else
	    insrbrace=0;
       }
     }
    else
      token=gettoken(false);

    if (learn)
     {
      if (first)
       {
	switch (token)
	 {
	  case tok_ident:
	  case tok_number:
	  case tok_compare:
	  case tok_logical:
	  case tok_addind:
	  case tok_operator:
	  case tok_bitop:
	  case tok_component:
	  case tok_boolnot:
	  case tok_bitnot:
	  case tok_unary:
	  case tok_indir:
	  case tok_typindir:
	  case tok_address:
	  case tok_incdec:
	  case tok_lbracket:
	  case tok_rbracket:
	  case tok_lpar:
	  case tok_rpar:
	    switch (statement)
	     {
	      case key_if:
		countindent(ind_if,indent,indent1);
		i=iindent-pariindent;
		count(&ifpar,i);
		break;
	      case key_func:
		i=iindent-pariindent;
		if (funcdecl)
		 {
		  countindent(ind_decl,indent-1,indent1-1);
		  count(&declpar,i);
		 }
		else
		 {
		  countindent(ind_func,indent-1,indent1-1);
		  count(&funcpar,i);
		 }
		break;
	      case key_assign:
		countindent(ind_assign,indent-1,indent1-1);
		i=iindent-pariindent;
		count(&asgnpar,i);
		break;
	      case key_varlist:
		countindent(ind_varlist,indent-1,indent1-1);
		break;
	     }
	    break;
	 }
       }
      if (spi==spc_casecolon2)
       {
	switch (token)
	 {
	  case tok_ident:
	  case tok_keyword:
	  case tok_incdec:
	  case tok_indir:
	  case tok_lpar:
	    count(&casepar,tokx-iindent);
	    break;
	 }
       }
     }

    if (lbrace>=0 && (token!=tok_comment && token!=tok_cppcomment
      || lasttoken1!=tok_rpar && lasttoken1!=tok_ident))
     {
      putln();
      oindent=lbrace;
      put('{');
      if (token==tok_endline && newline && !(newline&nl_ignore))
	newline=nl_start;
      else
	newline=nl_ignore;
      lbrace=-1;
     }

    /* left brace and following are on same line */
    if (braceonly)
     {
      if (learn || !(structure()?bracealwaysnewlinestruct:bracealwaysnewline))
       {
	switch (token)
	 {
	  case tok_endline:
	  case tok_comment:
	  case tok_cppcomment:
	    break;
	  default:
	    newline=0;
	    putspaces(spc_brace);
	 }
       }
      braceonly=false;
     }

    if (lasttoken==tok_assign && parlevel==0 && !templdecl && !structbody
      && !enumdecl && token!=tok_lbrace && token!=tok_endline && numassign==1
      && !newline)
     {
      if (learn)
       {
	statement=key_assign;
	pariindent=x-1-readspaces;
       }
      else
       {
	if (alignassignind)
	  assignind=getox()+ind[ind_assign];
	else
	  assignind=ind[ind_assign];
	assignind-=ind[ind_level];
	indent+=assignind;
	assigning=1;
       }
     }

    switch (token)
     {
      case tok_char:
	putspaces(spi);
	put(c0);
	newline=0;
	break;

      case tok_endline:
	if ((newline&nl_start) && !(newline&nl_noputln))
	  putln();
	newline=nl_start;
	if (assigning)
	  assigning=2;
	spaces=0;
	spi=spc_null;
	break;

      case tok_comment:
      case tok_cppcomment:
	spaces=0;
	spi=spc_null;
	newindent=-1;
	if ((newline&nl_ignore) && lasttoken!=tok_comment
	  && lasttoken!=tok_cppcomment)
	  newline=0;
	if (newline)
	 {
	  if (olinlen!=0)
	    indent2=commentoindent;
	  else
	    indent2=-1;
	  if (!(newline&nl_noputln))
	    putln();
	  if (commentoindent<0)
	    commentoindent=indent2;
	  if (lasttoken!=tok_comment && lasttoken!=tok_cppcomment
	    || x-2<commentiindent)
	    commentoindent=-1;
	  if (iindent!=0)
	    if (commentoindent>=0 && (token==tok_cppcomment || c1!='\n'))
	      oindent=commentoindent;
	    else
	      oindent=indent;
	  commentiindent=x-2;
	  commentpos=0;
	 }
	else
	 {
	  commentiindent=x-2;
	  commentpos=olinlen;
	  if (learn && withtab)
	    tab[tab_comment]=true;
	  if (withtab && tab[tab_comment])
	   {
	    newindent=commentiindent
#ifndef COMMENTFIX
	      -iindent
#endif /*COMMENTFIX*/
	      ;
	    put(0);
	    put((byte)newindent);
	    put(*((byte*)&newindent+1));
	   }
	  else
	   {
	    ok=true;
	    if (token==tok_comment)
	     {
	      savestate();
	      while (c1!='\n' && (c0!='*' || c1!='/'))
		get();
	      if (c1=='/')
		get();
	      while (c1==' ' || c1=='\t')
		get();
	      if (c1!='\n')
		ok=false;
	      restorestate();
	     }
	    if (ok) /* the comment is the last token in the line */
	      putspaces(spc_comment);
	    else
	      putspaces1(readspaces,tab_other);
	   }
	 }
	putop();
	if (token==tok_comment)
	 {
	  if (commentiindent==0)
	    newindent=0;
	  else if (newindent<0)
	    newindent=indentbeg+getox()-2;
	  while (c1!=EOF && (c0!='*' || c1!='/'))
	   {
	    get();
	    if (c0=='\n')
	     {
	      putln();
	      commentpos=0;
	      skipspaces();
	      if (x>iindent)
		oindent=newindent+x-commentiindent;
	      else
		oindent=newindent;
	     }
	    else
	      put(c0);
	   }
	  put(get());
	  if (c1==EOF || c1=='\n')
	   {
	    get();
	    newline=nl_start;
	   }
	  else
	    newline=0;
	  spaces=0;
	  spi=spc_null;
	 }
	else
	 {
	  while (c1!=EOF && c1!='\n')
	    put(get());
	  get();
	  newline=nl_start;
	 }
	break;

      case tok_assign:
	numassign++;
	putopsp(spc_assign);
	break;

      case tok_operator:
	putopsp(spc_op);
	break;

      case tok_addind:
	putopsp(spc_addind);
	break;

      case tok_bitop:
	putopsp(spc_bit);
	break;

      case tok_logical:
	putopsp(spc_log);
	break;

      case tok_compare:
	putopsp(spc_comp);
	break;

      case tok_boolnot:
	putunaryopsp(spc_boolnot);
	break;

      case tok_bitnot:
	putunaryopsp(spc_bitnot);
	break;

      case tok_unary:
	putunaryopsp(spc_unary);
	break;

      case tok_typindir:
      case tok_indir:
      case tok_address:
	if (lasttoken==tok_ident)
	 {
	  switch (token)
	   {
	    case tok_typindir:
	      identindir=true;
	      putspaces(spc_typindir);
	      break;
	    case tok_indir:
	      identindir=true;
	      putspaces(spc_indir);
	      break;
	    case tok_address:
	      identaddr=true;
	      putspaces(spc_addr);
	      break;
	   }
	  indirpos=olinlen;
	 }
	else
	 {
	  if (lasttoken==tok_indir || lasttoken==tok_typindir
	    || lasttoken==tok_address)
	   {
	    spaces=0;
	    putspaces(spc_null);
	   }
	  else
	   {
	    identindir=false;
	    identaddr=false;
	    putspaces(spi);
	   }
	 }
	putop();
	switch (token)
	 {
	  case tok_indir:
	    spi=spc_indir2;
	    break;
	  case tok_address:
	    spi=spc_addr2;
	    break;
	 }
	break;

      case tok_dots:
	putopsp(spc_declcomma);
	break;

      case tok_incdec:
	if (lasttoken==tok_ident)
	 {
	  spaces=0;
	  putspaces(spc_null);
	 }
	else
	  putspaces(spi);
	putop();
	break;

      case tok_inslbrace:
      case tok_lbrace:
	if (indcolon)
	 {
	  indent=indentcolon;
	  indent1=indentcolon1;
	  indcolon=false;
	 }
	if (caselabel && lasttoken1!=tok_colon && lasttoken1!=tok_endline)
	  caselabel=0;
	if (lasttoken1==tok_assign && !(expr&2))
	 {
	  if (level>0)
	   {
	    indent=indents[level-1]+ind[ind_levelstruct];
	    if (level==1)
	      indent+=ind[ind_levelstruct0];
	   }
	  expr|=2; /* beginning of initializers */
	 }
	if (enumdecl)
	  enumdecl=2;
	if (structure())
	  braceindent=ind[ind_lbstruct];
	else if (level<=level0)
	  braceindent=ind[ind_lbrace0];
	else
	  braceindent=ind[ind_lbrace];

	/* check if right brace is on same line*/
	bracelevel1=0;
	if (token!=tok_inslbrace
	  && (!alwaysnewline || (expr&2) || learn || enumdecl))
	 {
	  savestate();
	  while (bracelevel1>=0 && (token1=gettoken(false))!=tok_endline)
	   {
	    switch (token1)
	     {
	      case tok_lbrace:
		bracelevel1++;
		break;
	      case tok_rbrace:
		bracelevel1--;
		break;
	      case tok_wquote:
		get();
		/* fall thru */
	      case tok_quote:
		quote=c0;
		while (c1!='\n' && c1!=quote)
		 {
		  get();
		  if (c0=='\\')
		    get();
		 }
		if (c1!='\n')
		  get();
		break;
	      case tok_comment:
		while (c1!='\n' && (c0!='*' || c1!='/'))
		  get();
		if (c1!='\n')
		  get();
		break;
	      case tok_cppcomment:
		while (c1!='\n')
		  get();
		break;
	     }
	   }
	  restorestate();
	 }

	/* right brace on same line or 'extern "C" {'*/
	if (bracelevel1<0 || lasttoken1==tok_quote)
	 {
	  if ((expr&2) && level>level0)
	    braceindent=0;
	  /* similar to below */
	  if (level>0)
	    if ((expr&2) && (level>level0
	      || (structure()?ind[ind_lbrace0]:ind[ind_lbstruct])==0)
	      || isbrace(level-1))
	     {
	      indent+=braceindent;
	      indent1+=braceindent;
	     }
	    else
	     {
	      indent=indents[level-1]+braceindent;
	      indent1=indents1[level-1]+braceindent;
	     }
	  else
	   {
	    indent=braceindent;
	    indent1=braceindent;
	   }
	  if (spaces==0 && lasttoken!=tok_lbrace)
	    spaces=1;
	  putspaces(spi);
	  indent-=braceindent;
	  indent1-=braceindent;
	  if (lasttoken1==tok_quote)
	   {
	    extlevel=2;
	    structlevel=extlevel-2;
	   }
	  put('{');
	  if (bracelevel1<0)
	    levelkeys[level]=key_brace1;
	  else
	    levelkeys[level]=key_brace;
	  structbody=level>0 && levelkeys[level-1]==key_struct;
	  indents[level]=indent;
	  indents1[level]=indent1;
	  if (level<maxlevel)
	    level++;
	  break;
	 }

	braceonly=first && bracelevel1==0;

	if (expr&2)
	  i=brace_init;
	else if (structure())
	  i=brace_struct;
	else if (level<=level0-1)
	  i=brace_level0;
	else if (levelkeys[level-1]==key_else)
	  i=brace_else;
	else if (levelkeys[level-1]==key_do)
	  i=brace_do;
	else
	  i=brace_level;

	if (learn && (lasttoken==tok_rpar || lasttoken==tok_keyword
	  || lasttoken==tok_ident || lasttoken==tok_assign))
	 {
	  if (!first)
	    bracesameline[i]++;
	  else
	    bracesameline[i]--;
	 }

	if (bracesameline[i]>0
	  && lasttoken1!=tok_semicolon && lasttoken1!=tok_comma
	  && lasttoken1!=tok_lbrace && lasttoken1!=tok_rbrace
	  && lasttoken1!=tok_endline && lasttoken1!=tok_colon)
	 {
	  if (lasttoken==tok_comment || lasttoken==tok_cppcomment)
	   {
	    if (commentpos!=0)
	     {
	      if (olinlen<=sizeof(olinbuf)-2)
	       {
		memmove(olinbuf+commentpos+2,olinbuf+commentpos,
		  olinlen-commentpos);
		olinlen+=2;
		olinbuf[commentpos]=' ';
		olinbuf[commentpos+1]='{';
	       }
	     }
	    else
	     {
	      /* same as below */
	      if (level>0)
		if (isbrace(level-1))
		 {
		  indent2=indent;
		  indent3=indent1;
		 }
		else
		 {
		  indent2=indents[level-1];
		  indent3=indents1[level-1];
		 }
	      else
	       {
		indent2=0;
		indent3=0;
	       }
	      if (learn && first && statement==key_null)
		if (structure())
		  countindent(ind_lbstruct,indent2,indent3);
		else if (level<=level0)
		  countindent(ind_lbrace0,indent2,indent3);
		else
		  countindent(ind_lbrace,indent2,indent3);
	      lbrace=indent2+braceindent;
	      if (caselabel)
		lbrace-=ind[ind_level];
	     }
	   }
	  else
	   {
	    if (!learn || !(newline&nl_start))
	     {
	      newline=0;
	      if (lasttoken==tok_rpar)
	       {
		switch (parkinds[parlevel])
		 {
		  case par_for:
		  case par_if:
		    putspaces(spc_ifbrace);
		    break;
		  default:
		    putspaces(spc_rpar);
		    break;
		 }
	       }
	      else
		putspaces(spc_elsebrace);
	     }
	    put('{');
	   }
	 }
	else
	 {
	  /* same as above */
	  if (level>0)
	    if (isbrace(level-1))
	     {
	      indent2=indent;
	      indent3=indent1;
	     }
	    else
	     {
	      indent2=indents[level-1];
	      indent3=indents1[level-1];
	     }
	  else
	   {
	    indent2=0;
	    indent3=0;
	   }
	  if (learn && first && statement==key_null)
	    if (structure())
	      countindent(ind_lbstruct,indent2,indent3);
	    else if (level<=level0)
	      countindent(ind_lbrace0,indent2,indent3);
	    else
	      countindent(ind_lbrace,indent2,indent3);
	  lbrace=indent2+braceindent;
	  if (caselabel)
	    lbrace-=ind[ind_level];
	 }
	if (caselabel)
	  levelkeys[level]=key_brace2;
	else if (token==tok_inslbrace)
	 {
	  levelkeys[level]=key_brace3;
	  token=tok_lbrace;
	 }
	else
	  levelkeys[level]=key_brace;
	structbody=level>0 && levelkeys[level-1]==key_struct;

	if (level>0 && levelkeys[level-1]==key_switch)
	 {
	  switchlevel++;
	  if (!learn)
	    indent+=ind[ind_switchstat];
	  indents[level]=indents[level-1];
	  indents1[level]=indents1[level-1];
	 }
	else if (level>0 && !isbrace(level-1))
	 {
	  if (learn)
	   {
	    indent1++;
	    indents[level]=indents[level-1];
	    indents1[level]=indents1[level-1];
	   }
	  else
	   {
	    if (braceinclevel)
	      switch (levelkeys[level-1])
	       {
		case key_if:
		case key_else:
		case key_while:
		case key_catch:
		case key_for:
		case key_do:
		  indent+=ind[ind_level];
	       }
	    indents[level]=indents[level-1];
	   }
	 }
	else
	 {
	  if (learn)
	   {
	    indents[level]=indent;
	    indents1[level]=indent1;
	    if (!caselabel)
	      indent++;
	    indent1++;
	   }
	  else
	   {
	    indents[level]=indent;
	    if (!caselabel)
	      incindent();
	    else if (braceinclevel)
	      indent+=ind[ind_level];
	   }
	 }

	if (first)
	  indentbeg=lbrace;

	if (level<maxlevel)
	  level++;
	newline=nl_ignore;
	break;

      case tok_rbrace:
	rbracenewline=first;
	if (level>extlevel)
	  for (iflevel=level-1; iflevel>extlevel
	    && levelkeys[iflevel]!=key_if; iflevel--)
	    ;
	while (level>0 && !isbrace(level-1))
	 {
	  level--;
	  indent=indents[level];
	  indent1=indents1[level];
	 }
	if (level>0)
	 {
	  level--;
	  indent=indents[level];
	  indent1=indents1[level];
	 }
	if (level>0 && switchlevel>0 && levelkeys[level-1]==key_switch)
	  switchlevel--;

	if (learn && first && statement==key_null)
	  if (structure())
	    countindent(ind_rbstruct,indent,indent1);
	  else if (level<=level0)
	    countindent(ind_rbrace0,indent,indent1);
	  else
	    countindent(ind_rbrace,indent,indent1);

	newline=0;
	if (levelkeys[level]==key_brace || levelkeys[level]==key_brace3
	  || levelkeys[level]==key_brace2)
	 {
	  putln();
	  if (structure())
	   {
	    oindent=indent+ind[ind_rbstruct];
	    if (levelkeys[level]==key_brace2)
	      oindent-=ind[ind_levelstruct];
	   }
	  else
	   {
	    if (level<=level0)
	      oindent=indent+ind[ind_rbrace0];
	    else
	      oindent=indent+ind[ind_rbrace];
	    if (levelkeys[level]==key_brace2)
	      oindent-=ind[ind_level];
	    newline=nl_ignore;
	   }
	  indentbeg=oindent;
	 }
	else if (lasttoken1==tok_endline && lasttoken2==tok_endline)
	  putln(); /* new line if copyline() just has been called */

	put('}');
	while (level>0 && !isbrace(level-1) && levelkeys[level-1]!=key_do)
	 {
	  level--;
	  indent=indents[level];
	  indent1=indents1[level];
	 }
	if (level>0 && levelkeys[level-1]==key_do)
	 {
	  level--;
	  indent=indents[level];
	  indent1=indents1[level];
	 }
	structbody=level>1 && levelkeys[level-2]==key_struct;
	if (level==0)
	 {
	  extlevel=0;
	  structlevel=-2;
	 }
	else if (!learn)
	 {
	  if (levelkeys[level-1]==key_brace3)
	   {
	    if (level>iflevel)
	      insrbrace=2;
	    else
	      insrbrace=1;
	    bracelevel=level-1;
	    while (bracelevel>0 && (levelkeys[bracelevel-1]<key_brace
	      || levelkeys[bracelevel-1]>key_brace2))
	      bracelevel--;
	   }
	 }
	if (level<=extlevel || !isbrace(level-1) || levelkeys[level]==key_do)
	  newline=0;
	spaces=1;
	spi=spc_null;
	break;

      case tok_lpar:
	if (parlevel==0)
	 {
	  if ((numid>2 || numid>0 && level==extlevel+1 || structbody)
	    && (lasttoken1==tok_ident || lasttoken1==tok_rpar
	    || numid>=16384) && bracketlevel==0)
	   {
	    funcdecl=true;
	    expr&=~1;
	   }
	  else if (lasttoken1!=tok_rpar || parkinds[0]==par_if
	    || parkinds[0]==par_for)
	    expr|=1;
	  else if (numid>=0 && bracketlevel==0)
	    expr&=~1;
	 }
	numid1=0;

	if (funcdecl || lasttoken1==tok_ident || lasttoken1==tok_rangle)
	 {
	  outx=getox();
	  if (funcdecl)
	   {
	    parkind=par_funcdecl;
	    putspaces(spc_funcdecl);
	   }
	  else if (lasttoken1==tok_rangle)
	   {
	    parkind=par_castcall;
	    putspaces(spc_castcall);
	   }
	  else
	   {
	    parkind=par_funccall;
	    putspaces(spc_funccall);
	   }
	  put('(');
	  spi=spc_funcpar;
	  if (parlevel==0)
	   {
	    if (learn)
	     {
	      statement=key_func;
	      pariindent=x-1-readspaces;
	     }
	    else
	     {
	      if (funcdecl)
	       {
		parindent=ind[ind_decl];
		if (aligndeclind)
		  parindent+=outx;
	       }
	      else
	       {
		parindent=ind[ind_func];
		if (alignfuncind)
		  parindent+=outx;
	       }
	      switch (assigning)
	       {
		case 0:
		  if (structure())
		    parindent-=ind[ind_levelstruct];
		  else
		    parindent-=ind[ind_level];
		  break;
		case 1:
		  parindent-=ind[ind_level]+assignind;
		  break;
	       }
	      indent+=parindent;
	     }
	   }
	 }
	else
	 {
	  outx=getox();
	  if (lasttoken1==tok_keyword)
	   {
	    switch (lastkeyword)
	     {
	      case key_if:
	      case key_while:
	      case key_dowhile:
	      case key_catch:
	      case key_switch:
		parkind=par_if;
		break;
	      case key_for:
		parkind=par_for;
		forempty=true;
		expr=0;
		break;
	      case key_return:
		parkind=par_return;
		spi=spc_returnpar;
		break;
	      case key_assert:
		parkind=par_assert;
		spi=spc_funccall;
		break;
	      default:
		parkind=par_unknown;
	     }
	   }
	  else
	    parkind=par_unknown;
	  putspaces(spi);
	  put('(');
	  if (parkind==par_assert)
	   {
	    asserting=true;
	    spi=spc_funcpar;
	   }
	  else if (learn)
	   {
	    spaces=0;
	    spi=spc_null;
	   }
	  else
	    spi=spc_par;
	  if (parlevel==0)
	   {
	    if (parkind==par_for || parkind==par_if)
	     {
	      spi=spc_ifpar;
	      if (learn)
	       {
		statement=key_if;
		pariindent=x-1-readspaces;
	       }
	      else
	       {
		if (alignifind)
		  parindent=outx+ind[ind_if];
		else
		  parindent=ind[ind_if];
		indent+=parindent;
	       }
	     }
	   }
	 }
	parkinds[parlevel]=parkind;
	if (!withtab || !tab[tab_other])
	  parpos[parlevel]=olinlen-1;
	if (parlevel<maxlevel)
	  parlevel++;
	break;

      case tok_rpar:
	if (parlevel>0)
	  parlevel--;
	parkind=parkinds[parlevel];
	switch (parkind)
	 {
	  case par_funcdecl:
	  case par_funccall:
	  case par_castcall:
	    if (lasttoken!=tok_lpar)
	      putspaces(spc_funcpar);
	    else
	      putspaces(spc_funcnopar);
	    if (parlevel==0)
	     {
	      if (learn)
		statement=key_null;
	      else
		indent-=parindent;
	      if (parkind==par_funcdecl)
	       {
		oper=false;
		expr=0;
		numid=0;
	       }
	      if (funcdecl)
	       {
		if (level>=level0)
		 {
		  token1=nexttoken();
		  if (token1==tok_ident && strcmp(id,"throw")==0)
		   {
		    spaces=1; /* "f(x) throw(a,b)" */
		    spi=spc_null;
		   }
		  else if (token1!=tok_comma && token1!=tok_lpar)
		   {
		    level--;
		    indent=indents[level];
		    indent1=indents1[level];
		   }
		 }
		funcdecl=false;
	       }
	     }
	    break;
	  case par_if:
	  case par_for:
	    if (parkind==par_for)
	     {
	      if (lasttoken==tok_semicolon)
	       {
		if (forempty && learn)
		  putspaces(spc_emptyfor3);
		else
		  putspaces(spc_semi);
	       }
	      else
	       {
		forempty=false;
		putspaces(spc_ifpar);
	       }
	     }
	    else
	      putspaces(spc_ifpar);
	    if (level<=extlevel || levelkeys[level-1]!=key_dowhile)
	     {
	      if (alwaysnewline)
		newline=nl_ignore|nl_always;
	      if (alwaysbrace && !learn)
		inslbrace=true;
	     }
	    break;
	  case par_assert:
	    asserting=false;
	    putspaces(spc_funcpar);
	    break;
	  default:
	    if (!learn || !canbecast)
	      putspaces(spc_par);
	 }
	put(')');
	switch (parkind)
	 {
	  case par_for:
	    if (forempty && !learn)
	     {
	      if (parpos[parlevel]+spc[spc_emptyfor1]+spc[spc_emptyfor2]
		+spc[spc_emptyfor3]+4<=sizeof(olinbuf))
	       {
		olinlen=parpos[parlevel];
		olinbuf[olinlen++]='(';
		memset(olinbuf+olinlen,' ',spc[spc_emptyfor1]);
		olinlen+=spc[spc_emptyfor1];
		olinbuf[olinlen++]=';';
		memset(olinbuf+olinlen,' ',spc[spc_emptyfor2]);
		olinlen+=spc[spc_emptyfor2];
		olinbuf[olinlen++]=';';
		memset(olinbuf+olinlen,' ',spc[spc_emptyfor3]);
		olinlen+=spc[spc_emptyfor3];
		olinbuf[olinlen++]=')';
	       }
	     }
	    /* continued */
	  case par_if:
	    spi=spc_rpar;
	    if (parlevel==0)
	     {
	      indent+=ind[ind_level];
	      if (learn)
	       {
		indent1++;
		statement=key_null;
	       }
	      else
		indent-=parindent;
	      newstat=true;
	      numassign=0;
	      expr=0;
	      oper=false;
	      numid=-1;
	     }
	    break;

	  /* check if last parenthesis was really function call or decl */
	  case par_funccall:
	  case par_funcdecl:
	  case par_castcall:
	    if (!learn && lasttoken1!=tok_lpar && nexttoken()==tok_lpar)
	      adjustfuncspaces(parkind==par_funccall?spc_funccall
		:parkind==par_funcdecl?spc_funcdecl:spc_castcall);
	    break;

	  /* check if last parenthesis was really return() */
	  case par_return:
	    if (!learn && nexttoken()!=tok_semicolon)
	      adjustretspaces();
	    break;
	 }
	break;

      case tok_lbracket:
	if (lasttoken==tok_ident || lasttoken==tok_quote
	  || lasttoken==tok_wquote)
	  putspaces(spc_index);
	else
	  putspaces(spi);
	put('[');
	spi=spc_bracket;
	bracketlevel++;
	break;

      case tok_rbracket:
	if (lasttoken!=tok_lbracket)
	  putspaces(spc_bracket);
	put(']');
	bracketlevel--;
	break;

      case tok_langle:
	if (lasttoken1==tok_keyword && keyword==key_template)
	 {
	  spi=spc_templdecl;
	  templdecl=true;
	 }
	else if (lasttoken1==tok_keyword && keyword==key_cppcast)
	 {
	  spi=spc_cppcast;
	  cppcast=true;
	  expr=0;
	  templnumid=numid;
	 }
	else if (lasttoken1==tok_ident)
	 {
	  spi=spc_templcall;
	  templcall=true;
	  templnumid=numid;
	 }
	putspaces(spi);
	putop();
	if (templdecl || templcall || cppcast)
	  spi=spc_angle;
	break;

      case tok_rangle:
	if (templdecl || templcall || cppcast)
	  spi=spc_angle;
	putspaces(spi);
	putop();
	if (templdecl || templcall || cppcast)
	 {
	  if (cppcast)
	   {
	    numid=templnumid;
	    spi=spc_castcall;
	   }
	  else
	   {
	    if (templcall)
	      numid=templnumid;
	    else if (level>0)
	     {
	      level--;
	      indent=indents[level];
	      indent1=indents1[level];
	      newstat=true;
	     }
	    spaces=1;
	    spi=spc_null;
	   }
	  templdecl=false;
	  templcall=false;
	  cppcast=false;
	 }
	break;

      case tok_semicolon:
	if (indcolon)
	 {
	  indent=indentcolon;
	  indent1=indentcolon1;
	  indcolon=false;
	 }
	if (parlevel==0)
	  statement=key_null;
	if (lasttoken1==tok_rbrace)
	  newline=0;
	if (newline)
	 {
	  forempty=false;
	  putspaces(spi);
	 }
	else if (lasttoken1==tok_lpar || lasttoken1==tok_semicolon)
	 {
	  if (learn)
	   {
	    if (lasttoken2==tok_lpar && lasttoken1==tok_semicolon
	      && nexttoken()==tok_rpar)
	     {
	      oldreadspaces=readspaces;
	      readspaces=lastspaces;
	      putspaces(spc_emptyfor1);
	      readspaces=oldreadspaces;
	      putspaces(spc_emptyfor2);
	     }
	   }
	  else
	    putspaces(spc_semi);
	 }
	else
	 {
	  forempty=false;
	  if (lasttoken1==tok_colon)
	    putspaces(spc_semi);
	 }

	put(';');
	if (parlevel==0)
	 {
	  if (level>extlevel)
	    for (iflevel=level-1; iflevel>extlevel
	      && levelkeys[iflevel]!=key_if; iflevel--)
	      ;
	  if (level<=extlevel)
	   {
	    if (level>=0)
	     {
	      indent=indents[level];
	      indent1=indents1[level];
	     }
	   }
	  else
	   {
	    while (level>extlevel && !isbrace(level-1))
	     {
	      level--;
	      indent=indents[level];
	      indent1=indents1[level];
	      if (levelkeys[level]==key_do
		&& levelkeys[level+1]!=key_brace
		&& levelkeys[level+1]!=key_brace3
		&& levelkeys[level+1]!=key_brace2)
		break;
	     }
	   }
	  structbody=level>1 && levelkeys[level-2]==key_struct;
	  if (!learn && level>0 && levelkeys[level-1]==key_brace3)
	   {
	    insrbrace=2;
	    bracelevel=level-1;
	    while (bracelevel>0 && (levelkeys[bracelevel-1]<key_brace
	      || levelkeys[bracelevel-1]>key_brace2)
	      && levelkeys[bracelevel-1]!=key_do)
	      bracelevel--;
	   }
	  if (alwaysnewline)
	    newline=nl_ignore|nl_always;
	  assigning=0;
	  varlist=false;
	 }
	spi=spc_semi;
	break;

      case tok_comma:
	if (lasttoken1==tok_rbrace)
	  newline=0;
	if (newline)
	  putspaces(spi);
	putop();
	if ((enumdecl==2 || (expr&2)) && parlevel==0)
	 {
	  while (level>extlevel && !isbrace(level-1))
	   {
	    level--;
	    indent=indents[level];
	    indent1=indents1[level];
	   }
	  structbody=level>1 && levelkeys[level-2]==key_struct;
	 }
	if (parlevel>0)
	 {
	  switch (parkinds[parlevel-1])
	   {
	    case par_funcdecl:
	      spi=spc_declcomma;
	      break;
	    case par_funccall:
	    case par_castcall:
	      spi=spc_callcomma;
	      break;
	    case par_for:
	      spi=spc_forcomma;
	      break;
	    default:
	      spi=spc_comma;
	   }
	 }
	else if (templdecl)
	  spi=spc_templdeclcomma;
	else if (templcall)
	  spi=spc_templcallcomma;
	else if (expr&2)
	  spi=spc_initcomma;
	else
	 {
	  if (parlevel==0 && numid>=2)
	   {
	    if (learn)
	      statement=key_varlist;
	    else
	     {
	      indent+=ind[ind_varlist]-ind[ind_level];
	      varlist=true;
	     }
	   }
	  spi=spc_comma;
	 }
	if (lasttoken1==tok_ident && indirpos>=0 && parlevel==0)
	  if (lasttoken2==tok_indir
	    && (spc[spc_indir]!=1 || spc[spc_indir1]!=0))
	   {
	    adjustindir(spc[spc_indir],spc[spc_indir1],'*');
	   }
	  else if (lasttoken2==tok_address
	    && (spc[spc_addr]!=1 || spc[spc_addr1]!=0))
	   {
	    adjustindir(spc[spc_addr],spc[spc_addr1],'&');
	   }
	break;

      case tok_colon:
	if ((level>=level0 || lasttoken1!=tok_rpar)
	  && structbody && !structdecl)
	 {
	  put(':');
	  if (classaccess)
	   {
	    indentbeg-=oindent;
	    oindent+=ind[ind_structlab];
	    if (oindent<0)
	      oindent=0;
	    indentbeg+=oindent;
	    level--;
	    indent=indents[level];
	    indent1=indents1[level];
	    if (alwaysnewline)
	      newline=nl_ignore|nl_always;
	    spaces=1;
	   }
	  else
	    spaces=0;
	  spi=spc_null;
	 }
	else if (!structdecl && level<level0) /* "x::x() : y()" */
	 {
	  indentcolon=indent;
	  indentcolon1=indent1;
	  if (learn)
	   {
	    if (first)
	     {
	      colonfuncsameline--;
	      countindent(ind_colonfunc,indent,indent1);
	     }
	    else
	      colonfuncsameline++;
	   }
	  indent+=ind[ind_colonfunc];
	  indent1+=ind[ind_colonfunc];
	  indcolon=true;
	  if (colonfuncsameline<0)
	    newline=nl_ignore|nl_always;
#if 0
	  else
	    newline=0;
#endif
	  putspaces(spc_classcolon1);
	  put(':');
	  newline=0;
	  spi=spc_classcolon2;
	 }
	else if (structdecl && level>0) /* "class x : y" */
	 {
	  indentcolon=indent;
	  indentcolon1=indent1;
	  indent=indents[level-1];
	  indent1=indents1[level-1];
	  if (learn)
	   {
	    if (first)
	     {
	      colonstructsameline--;
	      countindent(ind_colonstruct,indent,indent1);
	     }
	    else
	      colonstructsameline++;
	   }
	  indent+=ind[ind_colonstruct];
	  indent1+=ind[ind_colonstruct];
	  indcolon=true;
	  if (colonstructsameline<0)
	    newline=nl_ignore|nl_always;
#if 0
	  else
	    newline=0;
#endif
	  putspaces(spc_classcolon1);
	  put(':');
	  newline=0;
	  spi=spc_classcolon2;
	 }
	else /* label */
	 {
	  indentbeg-=oindent;
	  if (caselabel)
	   {
	    if (caselabel==1)
	      oindent+=ind[ind_caselabel];
	   }
	  else
	    oindent+=ind[ind_label];
	  if (oindent<0)
	    oindent=0;
	  indentbeg+=oindent;
	  if (caselabel)
	    putspaces(spc_casecolon1);
	  put(':');
	  if (caselabel==0 && level>extlevel)
	   {
	    level--;
	    indent=indents[level];
	    indent1=indents1[level];
	   }
	  if (alwaysnewline)
	    newline=nl_ignore|nl_always;
	  if (!caselabel)
	   {
	    spaces=1;
	    spi=spc_null;
	   }
	  else if (aligncasestat)
	   {
	    spaces=spc[spc_casecolon2]-getox();
	    spi=spc_null;
	   }
	  else
	    spi=spc_casecolon2;
	  caselabel++;
	 }
	break;

      case tok_condition:
	putopsp(spc_cond);
	break;

      case tok_component:
      case tok_scope:
	spaces=0;
	putopsp(spc_null);
	break;

      case tok_quote:
      case tok_wquote:
	putspaces(spi);
	if (token==tok_wquote)
	 {
	  put(c0);
	  get();
	 }
	quote=c0;
	put(c0);
	while (c1!='\n' && c1!=quote)
	 {
	  put(get());
	  if (c0=='\\')
	    put(get());
	 }
	put(get());
	spaces=1;
	spi=spc_null;
	break;

      case tok_keyword:
	parlevel=0;
	bracketlevel=0;

	if (keyword==key_asm)
	 {
	  putspaces(spi);
	  putid();
	  if (nexttoken()==tok_lbrace)
	   {
	    gettoken(false);
	    put(' ');
	    put('{');
	    copyline();
	    getln();
	    while (nexttoken()!=tok_rbrace)
	     {
	      putln();
	      copyline();
	      getln();
	     }
	    putln();
	   }
	  copyline();
	  token=lasttoken=tok_endline;
	  break;
	 }

	/* check if 'while' belongs to 'do' */
	if (keyword==key_while && levelkeys[level]==key_do)
	  keyword=key_dowhile;

	if (learn && lasttoken==tok_rbrace && rbracenewline)
	 {
	  switch (keyword)
	   {
	    case key_else:
	      if (!(newline&nl_start))
		elsesameline++;
	      else
		elsesameline--;
	      break;
	    case key_dowhile:
	      if (!(newline&nl_start))
		whilesameline++;
	      else
		whilesameline--;
	      break;
	   }
	 }

	if (newline && lasttoken==tok_rbrace
	  && (elsesameline>0 && keyword==key_else
	  || whilesameline>0 && keyword==key_dowhile))
	 {
	  spaces=1;
	  spi=spc_null;
	  newline=0;
	 }

	if (keyword==key_case)
	 {
	  putspaces(spi);
	  putid();
	  caselabel++;
	  expr|=1;
	  numid1=0;

	  if (!first && !(lasttoken==tok_lbrace?bracealwaysnewline:alwaysnewline))
	    caselabel++;
	  if (learn && first && statement==key_null)
	    countindent(ind_caselabel,indent,indent1);
	 }
	else
	 {
	  if (keyword==key_else)
	   {
	    /* matching 'if' for 'else' */
	    level=iflevel;
	    iflevel=extlevel;
	    indent=indents[level];
	    indent1=indents1[level];
	   }
	  else if (keyword==key_operator)
	   {
	    spaces=1;
	    spi=spc_null;
	   }
	  else if (keyword==key_if && lastkeyword==key_else
	    && newline==(nl_ignore|nl_always))
	    newline=0;
	  if (keyword==key_operator)
	   {
	    indent2=indent;
	    indent3=indent1;
	    if (level>extlevel && parlevel==0 && numid>0)
	     {
	      indent=indents[level-1];
	      indent1=indents1[level-1];
	     }
	   }
	  if (learn && first && statement==key_null)
	    if (structure())
	      countindent(ind_levelstruct,indent,indent1);
	    else
	      countindent(ind_level,indent,indent1);
	  newline1=newline;
	  putspaces(spi);
	  if (keyword==key_operator)
	   {
	    indent=indent2;
	    indent1=indent3;
	   }
	  putid();
	  if (level>extlevel && !newline1 && keyword==key_if
	    && lastkeyword==key_else)
	   {
	    level--;
	    indent=indents[level];
	    indent1=indents1[level];
	   }
	  if (keyword!=key_operator)
	   {
	    levelkeys[level]=keyword;
	    indents[level]=indent;
	    indents1[level]=indent1;
	    if (level<maxlevel)
	      level++;
	   }
	 }

	switch (keyword)
	 {
	  case key_switch:
	  case key_if:
	  case key_while:
	  case key_dowhile:
	  case key_for:
	  case key_catch:
	    spi=spc_if;
	    break;
	  case key_template:
	  case key_cppcast:
	    break;
	  case key_case:
	  case key_operator:
	    spaces=1;
	    spi=spc_null;
	    break;
	  case key_new:
	    if (lasttoken==tok_assign)
	     {
	      spaces=1;
	      spi=spc_null;
	      break;
	     }
	    /* fall thru */
	  default:
	    incindent();
	    indent1++;
	    if (keyword==key_return)
	      spi=spc_return;
	    else
	     {
	      spaces=1;
	      spi=spc_null;
	     }
	    break;
	 }

	if (keyword==key_else || keyword==key_do)
	 {
	  if (alwaysbrace && !learn)
	    inslbrace=true;
	  if (alwaysnewline)
	    newline=nl_ignore|nl_always;
	  else
	    newline=0;
	 }
	else
	  newline=0;
	break;

      case tok_ident:
	if (lasttoken2==tok_ident && lasttoken1==tok_quote && level>extlevel)
	 {
	  /* extern "C" */
	  level--;
	  indent=indents[level];
	  indent1=indents1[level];
	  numid=0;
	  expr=0;
	  newstat=true;
	 }

	structure1=structure();

	if (strcmp(id,"enum")==0)
	  enumdecl=1;
	else if (parlevel==0 && !templdecl && !cppcast
	  && (strcmp(id,"struct")==0
	  || strcmp(id,"class")==0
	  || strcmp(id,"union")==0))
	 {
	  structdecl=true;
	  if (level>0 && levelkeys[level-1]==key_misc)
	    levelkeys[level-1]=key_struct;
	  if (structlevel<extlevel)
	    structlevel=level;
	 }
	if (structbody && parlevel==0 && (strcmp(id,"private")==0
	  || strcmp(id,"protected")==0 || strcmp(id,"public")==0))
	  classaccess=true;
	else
	  classaccess=false;

	indent2=indent;
	indent3=indent1;
	if (level>extlevel && parlevel==0)
	 {
	  if (numid>0)
	   {
	    indent=indents[level-1];
	    indent1=indents1[level-1];
	   }
#ifndef __GNUC__
	  else if (stricmp(id,"HANDLE_MSG")==0)
	   {
	    if (learn)
	     {
	      indent++;
	      indent1++;
	     }
	    else
	      indent+=ind[ind_caselabel];
	   }
#endif /*__GNUC__*/
	 }

	if (learn && first && statement==key_null)
	 {
	  if (nexttoken()==tok_colon)
	   {
	    if (structbody)
	      countindent(ind_structlab,indent,indent1);
	    else
	      countindent(ind_label,indent,indent1);
	    if (iindent!=0)
	      labelfirst=false;
	    else if (labelfirst<0)
	      labelfirst=true;
	   }
	  else
	    if (structure1)
	      countindent(ind_levelstruct,indent,indent1);
	    else
	      countindent(ind_level,indent,indent1);
	 }

	if (!expr)
	 {
	  switch (lasttoken)
	   {
	    case tok_indir:
	    case tok_typindir:
	      if (identindir)
		putspaces(spc_indir1);
	      else
		putspaces(spi);
	      break;
	    case tok_address:
	      if (identaddr)
		putspaces(spc_addr1);
	      else
		putspaces(spi);
	      break;
	    case tok_comment:
	      spaces=readspaces;
	      /* fall thru */
	    default:
	      putspaces(spi);
	   }
	 }
	else
	  putspaces(spi);
	indent=indent2;
	indent1=indent3;
	putid();
	spaces=1;
	spi=spc_null;
	newline=0;
	break;

      case tok_number:
	if (learn && first && statement==key_null && structure())
	  countindent(ind_levelstruct,indent,indent1);
	putspaces(spi);
	putid();
	spaces=1;
	spi=spc_null;
	newline=0;
	break;
     }

    switch (token)
     {
      case tok_keyword:
	switch (keyword)
	 {
	  case key_else:
	  case key_do:
	    newstat=true;
	    numassign=0;
	    expr=0;
	    break;
	  case key_operator:
	    if (newstat)
	     {
	      levelkeys[level]=key_operator;
	      indents[level]=indent;
	      indents1[level]=indent1;
	      if (level<maxlevel)
		level++;
	      indent+=ind[ind_level];
	      indent1++;
	      newstat=false;
	     }
	    break;
	  default:
	    newstat=false;
	 }
	break;
      case tok_rpar:
      case tok_rangle:
      case tok_endline:
      case tok_comment:
      case tok_cppcomment:
	break;
      case tok_colon:
	if (caselabel)
	  expr=0;
	if (!structure() && !indcolon)
	 {
	  newstat=true;
	  numassign=0;
	 }
	break;
      case tok_comma:
	if (parlevel==0 && (expr&2) && !indcolon)
	  newstat=true;
	break;
      case tok_semicolon:
	structdecl=false;
	if (level<=structlevel)
	  structlevel=extlevel-2;
	if (parlevel==0)
	 {
	  newstat=true;
	  numassign=0;
	  expr&=~1;
	 }
	else
	 {
	  expr|=1;
	  numid1=0;
	 }
	expr&=~2; /* end of initializers */
	break;
      case tok_rbrace:
	enumdecl=0;
	/* continued */
      case tok_lbrace:
	structdecl=false;
	if (level<=structlevel)
	  structlevel=extlevel-2;
	newstat=true;
	numassign=0;
	expr&=~1;
	break;
      default:
	if (newstat)
	 {
	  if (structdecl)
	    levelkeys[level]=key_struct;
	  else
	    levelkeys[level]=key_misc;
	  indents[level]=indent;
	  indents1[level]=indent1;
	  if (level<maxlevel)
	    level++;
	  incindent();
	  indent1++;
	  newstat=false;
	 }
     }

    switch (token)
     {
      case tok_assign:
	numid=-1;
	if (funcdecl || templdecl || cppcast)
	  break;
	/* continued */
      case tok_lbracket:
      case tok_condition:
	expr|=1;
	numid1=0;
	/* continued */
      case tok_operator:
      case tok_addind:
      case tok_bitop:
      case tok_logical:
      case tok_compare:
      case tok_boolnot:
      case tok_bitnot:
      case tok_unary:
      case tok_lpar:
      case tok_lbrace:
      case tok_rbrace:
      case tok_semicolon:
      case tok_colon:
      case tok_component:
      case tok_scope:
      case tok_indir:
      case tok_typindir:
      case tok_address:
	oper=false;
	break;
      case tok_incdec:
	if (lasttoken1!=tok_ident && lasttoken1!=tok_rpar)
	  oper=false;
	break;
      case tok_keyword:
	switch (keyword)
	 {
	  case key_return:
	  case key_assert:
	    expr|=1;
	    numid1=0;
	    numid=-1;
	    break;
	 }
	oper=false;
	break;
      case tok_comma:
	if (!funcdecl && !templdecl || level>=extlevel+2 && !structbody)
	 {
	  expr|=1;
	  numid1=0;
	 }
	if (parlevel==0)
	  expr&=~1;
	oper=false;
	break;
      case tok_endline:
      case tok_comment:
      case tok_cppcomment:
	break;
      default:
	if (expr)
	  oper=true;
	break;
     }

    if (token==tok_ident)
     {
      if (expr&1 && !canbecast && numid1++!=0)
       {
	expr=0;
	oper=false;
       }
     }
    else
      numid1=0;

    if (newstat || numid>=0)
      switch (token)
       {
	case tok_ident:
	  numid++;
	  break;
	case tok_semicolon:
	  if (parlevel==0)
	    numid=0;
	  break;
	case tok_lbrace:
	case tok_rbrace:
	  numid=0;
	  break;
	case tok_lpar:
	case tok_indir:
	case tok_typindir:
	case tok_address:
	  if (numid==0)
	    numid=-1;
	  break;
	case tok_rpar:
	case tok_lbracket:
	case tok_rbracket:
	case tok_langle:
	case tok_rangle:
	case tok_scope:
	case tok_comment:
	case tok_cppcomment:
	case tok_endline:
	case tok_quote:
	case tok_wquote:
	  break;
	case tok_keyword:
	  switch (keyword)
	   {
	    case key_operator:
	      numid=16384;
	      break;
	    case key_new:
	    case key_delete:
	      break;
	    default:
	      numid=-1;
	   }
	  break;
	case tok_colon:
	  if (classaccess)
	   {
	    numid=0;
	    newstat=true;
	    break;
	   }
	  /* fall thru */
	default:
	  if (numid<16384)
	    numid=-1;
       }

    switch (token)
     {
      case tok_lpar:
	if (parkinds[parlevel-1]==par_unknown)
	  canbecast=true;
	break;
      case tok_ident:
	if (lasttoken1!=tok_ident && lasttoken1!=tok_lpar
	  && lasttoken1!=tok_typindir)
	  canbecast=false;
	if (strcmp(id,"sizeof")==0)
	 {
	  canbecast=false;
	  oper=false;
	 }
	break;
      case tok_typindir:
      case tok_indir:
      case tok_address:
	if (lasttoken1==tok_lpar)
	  canbecast=false;
	break;
      case tok_rpar:
	switch (nexttoken())
	 {
	  case tok_ident:
	    if (level<level0) /* "x::x() const {}" */
	     {
	      spaces=1;
	      spi=spc_null;
	     }
	    /* fall thru */
	  case tok_number:
	  case tok_lpar:
	  case tok_address:
	  case tok_bitop:
	  case tok_quote:
	  case tok_wquote:
	    cast=canbecast;
	    if (cast)
	     {
	      if (learn)
		count(&spacecounter[spc_castpar],readspaces);
	      else
		adjustcastspaces();
	      oper=false;
	      spi=spc_cast;
	     }
	    break;
	  default:
	    cast=false;
	 }
	canbecast=false;
	break;
      default:
	canbecast=false;
     }

    if (token!=tok_endline)
     {
      lastspaces=readspaces;
      lasttoken=token;
      if (token!=tok_comment && token!=tok_cppcomment)
       {
	if (enumdecl==1 && token!=tok_ident)
	  enumdecl=0;
	lasttoken2=lasttoken1;
	lasttoken1=token;
	if (token==tok_keyword)
	  lastkeyword=keyword;
	else
	  lastkeyword=key_null;
       }
     }
   }
  if (olinlen>0)
    putln();
 }


local void
calcindent(sumtyp* sump, int* ap, int* bp, long* sp)
 {
  int a;
  int b;
  long d;

  if (sump->sum_n>0)
   {
    d=sump->sum_n*sump->sum_xx-sump->sum_x*sump->sum_x;
    if (d!=0)
      b=divide(sump->sum_n*sump->sum_xy-sump->sum_x*sump->sum_y,d);
    else
      b=divide(sump->sum_y,sump->sum_x);
    if (b<0)
      b=0;
    a=divide(sump->sum_y-b*sump->sum_x,sump->sum_n);
    if (a<0)
      a=0;
   }
  else
   {
    b=8;
    a=0;
   }
  if (sump->sum_n>2)
    *sp=(((long)a*a*sump->sum_n+(long)b*b*sump->sum_xx+sump->sum_yy
      +2*((long)a*b*sump->sum_x-(long)a*sump->sum_y-(long)b*sump->sum_xy))
      *sump->sum_n)/(sump->sum_n-2);
  else
    *sp=0;
  *bp=b;
  *ap=a;
 }


local void
calculate(void)
 {
  int a;
  int a1;
  int b;
  int b1;
  int i;
  int k;
  int m;
  int indent0;
  int indentd;
  spctyp spci;
  indtyp indi;
  long n;
  long tot;
  long tot1;
  counttyp* cntp;

  /* calculate spaces */
  for (spci=0; spci<spcmax; spci++)
   {
    n=spacecounter[spci].cnt_sum0;
    if (n!=0)
     {
      tot=spacecounter[spci].cnt_sum1;
      spc[spci]=divide(tot,n);
     }
   }
  if (spacecounter[spc_typindir].cnt_sum0==0)
    spc[spc_typindir]=spc[spc_indir];
  if (spacecounter[spc_addr].cnt_sum0==0)
    spc[spc_addr]=spc[spc_indir];
  if (spacecounter[spc_addr1].cnt_sum0==0)
    spc[spc_addr1]=spc[spc_indir1];
  if (spacecounter[spc_addr2].cnt_sum0==0)
    spc[spc_addr2]=spc[spc_indir2];
  if (spacecounter[spc_return].cnt_sum0==0 && spc[spc_returnpar]>0)
    spc[spc_return]=spc[spc_returnpar];

  /* check if brace increments level of indentation and */
  /* calculate level indentation */
  calcindent(&indentsum[ind_level],&a,&b,&tot);
  calcindent(&indentsum1[ind_level],&a1,&b1,&tot1);
  if (tot1<tot) /* brace increments level of indentation */
   {
    braceinclevel=true;
    memcpy(indentcounter,indentcounter1,sizeof(indentcounter));
    ind[ind_level0]=a1;
    ind[ind_level]=b1;
   }
  else
   {
    ind[ind_level0]=a;
    ind[ind_level]=b;
   }

  /* calculate level indentation in structs */
  if (indentsum[ind_levelstruct].sum_n!=0)
    calcindent(&indentsum[ind_levelstruct],&ind[ind_levelstruct0],
      &ind[ind_levelstruct],&tot);
  else
   {
    ind[ind_levelstruct0]=ind[ind_level0];
    ind[ind_levelstruct]=ind[ind_level];
   }

  /* calculate indentation of statements in switch statement */
  tot=0;
  n=0;
  for (i=0; i<maxindlevel; i++)
   {
    cntp=&indentcounter[1][ind_level][i];
    n+=cntp->cnt_sum0;
    tot+=cntp->cnt_sum1-cntp->cnt_sum0
      *((i>0?ind[ind_level0]:0)+ind[ind_level]*i);
   }
  if (n!=0)
    ind[ind_switchstat]=divide(tot,n);
  else
    ind[ind_switchstat]=ind[ind_level];

  /* calculate other indentations */
  for (indi=ind_rbrace; indi<indmax; indi++)
   {
    m=indi<=ind_lbstruct?1:2;
    tot=0;
    n=0;
    for (i=0; i<maxindlevel; i++)
      for (k=0; k<m; k++)
       {
	cntp=&indentcounter[k][indi][i];
	n+=cntp->cnt_sum0;
	if (i==0)
	 {
	  indent0=0;
	  indentd=ind[ind_level];
	 }
	else
	  switch (indi)
	   {
	    case ind_rbstruct:
	    case ind_lbstruct:
	    case ind_colonstruct:
	    case ind_structlab:
	      indent0=ind[ind_levelstruct0];
	      indentd=ind[ind_levelstruct];
	      break;
	    default:
	      indent0=ind[ind_level0];
	      indentd=ind[ind_level];
	      break;
	   }
	tot+=cntp->cnt_sum1-cntp->cnt_sum0
	  *(indent0+indentd*i+ind[ind_switchstat]*k);
       }
    if (n!=0)
      ind[indi]=divide(tot,n);
    else
     {
      switch (indi)
       {
	case ind_lbrace:
	  ind[indi]=ind[ind_rbrace];
	  break;
	case ind_rbrace0:
	  ind[indi]=ind[ind_rbrace];
	  break;
	case ind_lbrace0:
	  ind[indi]=ind[ind_lbrace];
	  break;
	case ind_rbstruct:
	  ind[indi]=ind[ind_rbrace];
	  break;
	case ind_lbstruct:
	  ind[indi]=ind[ind_lbrace];
	  break;
	case ind_caselabel:
	case ind_label:
	  ind[indi]=-ind[ind_level];
	  break;
	case ind_structlab:
	  ind[indi]=ind[ind_label];
	  break;
	case ind_if:
	case ind_colonfunc:
	case ind_colonstruct:
	case ind_assign:
	case ind_func:
	case ind_decl:
	  ind[indi]=ind[ind_level];
	  break;
	case ind_varlist:
	  ind[indi]=ind[ind_levelstruct];
	  break;
       }
     }
   }

  if (labelfirst>0)
    ind[ind_label]=-16384;

  /* check if alignfuncind is true */
  if (funcpar.cnt_sum0!=0)
   {
    i=divide(funcpar.cnt_sum1,funcpar.cnt_sum0);
    if (i>=spc[spc_funccall] && i<=spc[spc_funccall]+1+spc[spc_funcpar]
      && funcpar.cnt_sum2*funcpar.cnt_sum0-funcpar.cnt_sum1*funcpar.cnt_sum1
      <=funcpar.cnt_sum0*funcpar.cnt_sum0/4)
     {
      alignfuncind=true;
      ind[ind_func]=i;
     }
   }

  /* check if aligndeclind is true */
  if (declpar.cnt_sum0!=0)
   {
    i=divide(declpar.cnt_sum1,declpar.cnt_sum0);
    if (i>=spc[spc_funcdecl] && i<=spc[spc_funcdecl]+1+spc[spc_funcpar]
      && declpar.cnt_sum2*declpar.cnt_sum0-declpar.cnt_sum1*declpar.cnt_sum1
      <=declpar.cnt_sum0*declpar.cnt_sum0/4)
     {
      aligndeclind=true;
      ind[ind_decl]=i;
     }
   }

  /* check if alignifind is true */
  if (ifpar.cnt_sum0!=0)
   {
    i=divide(ifpar.cnt_sum1,ifpar.cnt_sum0);
    if (i>=spc[spc_if] && i<=spc[spc_if]+1+spc[spc_ifpar]
      && ifpar.cnt_sum2*ifpar.cnt_sum0-ifpar.cnt_sum1*ifpar.cnt_sum1
      <=ifpar.cnt_sum0*ifpar.cnt_sum0/4)
     {
      alignifind=true;
      ind[ind_if]=i;
     }
   }

  /* check if alignassignind is true */
  if (asgnpar.cnt_sum0!=0)
   {
    i=divide(asgnpar.cnt_sum1,asgnpar.cnt_sum0);
    if (i>=spc[spc_assign] && i<=spc[spc_assign]+1+spc[spc_assign]
      && asgnpar.cnt_sum2*asgnpar.cnt_sum0-asgnpar.cnt_sum1*asgnpar.cnt_sum1
      <=asgnpar.cnt_sum0*asgnpar.cnt_sum0/4)
     {
      alignassignind=true;
      ind[ind_assign]=i;
     }
   }

  /* check if aligncasestat is true */
  if (casepar.cnt_sum0>1)
   {
    if (casepar.cnt_sum2*casepar.cnt_sum0-casepar.cnt_sum1*casepar.cnt_sum1
      <casepar.cnt_sum0*casepar.cnt_sum0/4)
     {
      aligncasestat=true;
      spc[spc_casecolon2]=divide(casepar.cnt_sum1,casepar.cnt_sum0);
     }
   }
 }


local void
readconfig(void)
 {
  tabtyp i;

  ifil=fopen(confname,"r");
  if (ifil==NULL)
   {
    for (i=0; i<tabmax; i++)
      tab[i]=true;
    alwaysnewline=false;
    writeconfig();
    return;
   }
  setvbuf(ifil,ibuf,_IOFBF,sizeof(ibuf));
  process(true);
  fclose(ifil);

  calculate();
 }


local char*
basename(char* s)
 {
  char* p;

#ifdef __GNUC__
  p=strrchr(s,'/');
  if (p!=NULL)
    return p+1;
#else /*__GNUC__*/
  for (;;)
   {
    p=strpbrk(s,":\\/");
    if (p==NULL)
      break;
    s=p+1;
   }
#endif /*__GNUC__*/
  return s;
 }


local char*
fileextension(char* filename)
 {
  char* cp;

  cp=strrchr(basename(filename),'.');
  if (cp==NULL)
    cp=strchr(filename,0);
  return cp;
 }


local void
changeext(char* newfilename, const char* oldfilename, const char* ext)
 {
  strcpy(newfilename,oldfilename);
  strcpy(fileextension(newfilename),ext);
 }


int
main(int argc, char** args)
 {
  int argi;
  int argi1;
  char* cp;
  byte* argp;

  changeext(progname,basename(args[0]),"");
  changeext(confname,args[0],".cfg");

  if (!isatty(1))
    con=stderr;
  else
    con=stdout;

  if (argc<2 && isatty(0))
   {
    writes("C Source Formatter version 11 (Copyright 2004 Roger Moser Switzerland)\n\n"
      "Usage:   ");
    writes(progname);
    writes(" [options] filenames\n         ");
    writes(progname);
    writes(" [options] <input >output\n\n"
      "Options: -l                learn\n"
      "         -tn[,n]           tab spacing in input and output file\n"
      "         -tn,0             no tabs in output file\n"
      "         -b                if, else, for, while and do always with {}\n"
      "         -cfilename[.cfg]  configuration file\n\n"
      "Default: -t8,8\n"
      "         -c");
    writes(confname);
    writes("\n");

    return(1);
   }

  /* read options */
  for (argi=1; argi<argc; argi++)
   {
    argp=args[argi];
    if (argp[0]=='-' || argp[0]=='/')
     {
      switch (argp[1])
       {
	case 'b':
	case 'B':
	  argp[0]=0;
	  alwaysbrace=true;
	  break;
	case 'd':
	case 'D':
	  argp[0]=0;
	  debug=true;
	  break;
	case 'l':
	case 'L':
	  argp[0]=0;
	  optlearn=true;
	  break;
	case 'c':
	case 'C':
	  argp[0]=0;
	  argp+=2;
	  if (argi<argc-1 && argp[0]==0)
	    argp=args[++argi];
	  strcpy(confname,argp);
	  cp=strrchr(confname,'.');
#ifdef __GNUC__
	  if (cp==NULL || strchr(cp,'/')!=NULL)
#else /*__GNUC__*/
	  if (cp==NULL || strchr(cp,'/')!=NULL || strchr(cp,'\\')!=NULL)
#endif /*__GNUC__*/
	    strcat(confname,".cfg");
	  argp[0]=0;
	  break;
	case 't':
	case 'T':
	  argp[0]=0;
	  argp+=2;
	  if (argi<argc-1 && argp[0]==0 && isdigit(args[argi+1][0]))
	    argp=args[++argi];
	  if (isdigit(argp[0]))
	   {
	    itab=atoi(argp);
	    argp[0]=0;
	    if (itab==0)
	      itab=8;
	    otab=itab;
	    argp=strchr(argp+1,',');
	    if (argp!=NULL && isdigit(argp[1]))
	     {
	      otab=atoi(argp+1);
	      if (otab==0)
		otab=sizeof(olinbuf);
	     }
	   }
	  break;
       }
     }
   }

  if (!optlearn)
    readconfig();

  for (argi=1; argi<argc; argi++)
   {
    argp=args[argi];
    if (argp[0]==0) /* was an option */
      continue;

    writes(argp);
    ifil=fopen(argp,"r");
    if (ifil==NULL)
     {
      writes(" not found\n");
      continue;
     }
    setvbuf(ifil,ibuf,_IOFBF,sizeof(ibuf));
#ifdef __GNUC__
    changeext(tempname,argp,".~~~");
#else /*__GNUC__*/
    changeext(tempname,argp,".$$$");
#endif /*__GNUC__*/

    /* find a unique backup file name */
    changeext(backname,argp,".BAK");
    cp=fileextension(backname)+1;
    for (argi1=1; argi1<argi; argi1++)
     {
      if (strcmp(backname,args[argi1])==0)
       {
	if (cp[2]=='K')
	  strcpy(cp,"BK0");
	else
	  cp[2]++;
	argi1=0; /* restart */
       }
     }

    if (!optlearn)
     {
      ofil=fopen(tempname,"w");
      if (ofil==NULL)
       {
	perror(tempname);
	continue;
       }
      setvbuf(ofil,obuf,_IOFBF,sizeof(obuf));
     }

    process(optlearn);

    if (ferror(ifil))
     {
      perror(argp);
      continue;
     }
    fclose(ifil);
    if (!optlearn && ferror(ofil))
     {
      perror(tempname);
      continue;
     }

    if (optlearn)
      writes(" learned\n");
    else
     {
      writes(" done\n");
      if (fclose(ofil) || !debug && ((unlink(backname), rename(argp,backname))
	|| rename(tempname,argp)))
       {
	perror(tempname);
	continue;
       }
     }

    if (strlen(backname)>strlen(argp))
      args[argi]=strdup(backname);
    else
      strcpy(argp,backname);
   }

  if (!isatty(0))
   {
    ifil=stdin;
    ofil=stdout;
    process(optlearn);
   }

  if (optlearn)
   {
    calculate();
    writeconfig();
   }

  return(0);
 }
