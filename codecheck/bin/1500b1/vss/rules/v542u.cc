// Abraxas Software, Inc. (c) copyright codecheck rule-files 2006


/*
vss542 u. Has all constants other than 0 and 1 defined or enumerated, or shall have a
comment which clearly explains what each constant means in the context of
its use. Where �0� and �1� have multiple meanings in the code unit, even
they should be identified. Example: �0� may be used as FALSE, initializing
a counter to zero, or as a special flag in a non-binary category.
*/

#include "vss.cch"

int conval;

if ( lex_constant ) {

	//warn( 542, "LC lnm%d tok=%s lmt%d", lex_not_manifest, token(), lex_macro_token );

	if ( lex_constant == CONST_INTEGER ) {

		if ( isdigit(token()[0]) ) {
			conval = atoi(token());
		}
		else {
			conval = -1;
		}

		if ( conval > 1 && ( lex_not_manifest && lex_macro_token == 0 )	 ) {
			if 	 ( ( LINHAS("//") == 0 && LINHAS("/*") == 0 ) /*&& comseen != (lin_number-1)*/ )  {
				WARN( 542, "5.4.2 U", "Enumerate all constants not zero or one" );
			}
		}
	}

}

