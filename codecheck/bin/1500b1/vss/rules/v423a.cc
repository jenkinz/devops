/*
423a. Each module shall have a specific function that can be tested and verified
independently of the remainder of the code. In practice, some additional
modules (such as library modules) may be needed to compile the module
under test, but the modular construction allows the supporting modules to be
replaced by special test versions that support test objectives;
1 Some software languages and development environments use a different definition of
module but this principle still applies.
*/

#include "vss.cch"

if ( mod_end ) {

	if ( mod_functions > 1 ) {
		WARN( 423, "423B", "There can only be one function per module" );
	}
}