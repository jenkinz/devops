/*
423e. Each module shall have a single entry point, and a single exit point, for
normal process flow. For library modules or languages such as the objectoriented
languages, the entry point is to the individual contained module or
method invoked. The single exit point is the point where control is returned.
At that point, the data that is expected as output must be appropriately set.
The exception for the exit point is where a problem is so severe that
execution cannot be resumed. In this case, the design must explicitly protect
all recorded votes and audit log information and must implement formal
exception handlers provided by the language; 
*/

#include "vss.cch"

//This also applies to exit() calls.

int expt;
int retseen;

if ( fcn_begin ) {
	expt = 1;
}

if ( op_return ) {
	expt++;
	retseen = lin_number;
}

if ( idn_function ) {
	if ( strstr(idn_name(), "exit") ) {
		WARN( 423, "4.2.3 E", "Exceptional Exit Point Found" );

	}
}

if ( fcn_end ) {
//warn( 423, "FE expt%d ret%d", expt, retseen );

	if ( expt > 1 && retseen == (lin_number-1) ) {
		--expt;			// ignore case of explicit usage of return at implicit location
		if ( expt>1 ) {
			WARN( 423, "4.2.3 E", "Multiple explicit exit points not allowed." );
		}
	}
}