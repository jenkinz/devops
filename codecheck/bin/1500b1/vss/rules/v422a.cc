// 4.2.2 Software Integrity

/*
Self-modifying, dynamically loaded, or interpreted code is prohibited, except under
the security provisions outlined in section 6.4.e. This prohibition is to ensure that the
software tested and approved during the qualification process remains unchanged and
retains its integrity. External modification of code during execution shall be
prohibited. Where the development environment (programming language and
development tools) includes the following features, the software shall provide
controls to prevent accidental or deliberate attempts to replace executable code:
*/

// a Unbounded arrays or strings (includes buffers used to move data)

#include "vss.cch"

if ( dcl_variable ) {

	if ( dcl_level(0) == ARRAY ) {

	//	warn( 422, "dcl_array_dim(0)=%d", dcl_array_dim(0)    );
		if ( dcl_array_dim(0) < 0 ) {
			WARN( 422, "4.2.2a", "No Unbound Arrays Or Strings" );
		}

	}
}