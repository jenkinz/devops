/*
427 b. Descriptive comments shall be provided to identify objects and data types.
All variables shall have comments at the point of declaration clearly
explaining their use. Where multiple variables that share the same meaning
are required, the variables may share the same comment;
*/

#include "vss.cch"



if ( dcl_variable ) {

		if ( dcl_parameter == 0 && 	( LINHAS("//") == 0 && LINHAS("/*") == 0 ) 	 ) {

			WARN( 427, "4.2.7 B", "All variables shall have comments" );
		}
}
