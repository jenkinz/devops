// Abraxas Software, Inc. (c) copyright codecheck rule-files 2006

/*
vss 542s. Initializes every variable upon declaration where permitted
*/


#include "vss.cch"

if ( dcl_variable ) {
	if ( dcl_initializer == 0 && dcl_parameter == 0 && dcl_base <= ENUM_TYPE ) {
		WARN( 542, "5.4.2 S", "Initialize every variable upon declaration" );
	}
}