
// (c) copyright codecheck rule-files abraxas-software 2006

/*

5.4.2 Assessment of Coding Conventions
K. Has no line of code exceeding 80 columns in width (including comments and
tab expansions) without justification;

*/

#include "vss.cch"

if ( lin_end ) {

	if ( lin_length > 80 ) {

		WARN( 542, "5.4.2 K", "Lines must not exceed 80 characters" );
	}
}