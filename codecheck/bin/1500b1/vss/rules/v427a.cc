
/*
427 a. All modules shall contain headers. For small modules of 10 lines or less, the
header may be limited to identification of unit and revision information. Other
header information should be included in the small unit headers if not clear from
the actual lines of code.
*/

#include "vss.cch"

int funcom;

if ( dcl_global ) {
	
	if ( dcl_function == 0 ) {
		funcom = 0;				// ignore comments before global's
	}
}

if ( lin_has_comment ) {
	funcom++;
}

if ( fcn_begin ) {

	//warn( 427, "FB %s fcn_no_header=%d fc%d", fcn_name(), fcn_no_header, funcom      ); 

	//funcom = 0;
}

if ( fcn_end ) {

	if ( fcn_total_lines   > 10 && funcom < 32 ) {

		WARN( 427, "4.2.7 A", "Specify Header Block" );
	}
	funcom = 0;
}