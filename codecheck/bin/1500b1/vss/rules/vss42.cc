// (c) copyright codecheck rule-files abraxas-software 2006

/*
This document provides an overview of the Voting System Standards (the �Standards�),
developed by the Federal Election Commission (FEC). 
*/

// 4.2.2 Software Integrity

/*
Self-modifying, dynamically loaded, or interpreted code is prohibited, except under
the security provisions outlined in section 6.4.e. This prohibition is to ensure that the
software tested and approved during the qualification process remains unchanged and
retains its integrity. External modification of code during execution shall be
prohibited. Where the development environment (programming language and
development tools) includes the following features, the software shall provide
controls to prevent accidental or deliberate attempts to replace executable code:
*/

// a Unbounded arrays or strings (includes buffers used to move data)

#include "v422a.cc" 

// b Pointer variables

#include "v422b.cc"

// c Dynamic memory allocation and management.

#include "v422c.cc"

// 4.2.3 Software Modularity and Programming

/*
Voting system application software, including COTS software, shall be designed in a
modular fashion. However, COTS software is not required to be inspected for
compliance with this requirement.. For the purpose of this requirement1, �modules�
may be compiled or interpreted independently. Modules may also be nested. The
modularity rules described here apply to the component sub modules of a library.
The principle concept is that the module contains all the elements to compile or
interpret successfully and has limited access to data in other modules. The design
concept is simple replacement with another module whose interfaces match the
original module. A module is designed in accordance with the following rules:
*/

/*
423a. Each module shall have a specific function that can be tested and verified
independently of the remainder of the code. In practice, some additional
modules (such as library modules) may be needed to compile the module
under test, but the modular construction allows the supporting modules to be
replaced by special test versions that support test objectives;
1 Some software languages and development environments use a different definition of
module but this principle still applies.
*/

#include "v423a.cc"

/*
423b. Each module shall be uniquely and mnemonically named, using names that
differ by more than a single character. In addition to the unique name, the
modules shall include a set of header comments identifying the module�s
purpose, design, conditions, and version history, followed by the operational
code. Headers are optional for modules of fewer than ten executable lines
where the subject module is embedded in a larger module that has a header
containing the header information. Library modules shall also have a header
comment describing the purpose of the library and version information;
*/

#include "v423b.cc"

/*
423c. All required resources, such as data accessed by the module, should either be
contained within the module or explicitly identified as input or output to the
module. Within the constraints of the programming language, such resources
shall be placed at the lowest level where shared access is needed. If that
shared access level is across multiple modules, the definitions should be
defined in a single file (called header files in some languages, such as C)
where any changes can be applied once and the change automatically applies
to all modules upon compilation or activation;
*/

#include "v423c.c"

/*
423d. A module is small enough to be easy to follow and understand. Program
logic visible on a single page is easy to follow and correct. Volume II,
Section 5 provides testing guidelines for the ITA to identify large modules
subject to review under this requirement;
*/

/*
423e. Each module shall have a single entry point, and a single exit point, for
normal process flow. For library modules or languages such as the objectoriented
languages, the entry point is to the individual contained module or
method invoked. The single exit point is the point where control is returned.
At that point, the data that is expected as output must be appropriately set.
The exception for the exit point is where a problem is so severe that
execution cannot be resumed. In this case, the design must explicitly protect
all recorded votes and audit log information and must implement formal
exception handlers provided by the language; and
*/

#include "v423e.cc"

/*
423f. Process flow within the modules shall be restricted to combinations of the
control structures defined in Volume II, Section 5. These structures support
the modular concept, especially the single entry/exit rule above. They apply
to any language feature where program control passes from one activity to the
next, such as control scripts, object methods, or sets of executable statements,
even though the language itself is not procedural.g 
*/

//System Standards/

/*
4.2.4 Control Constructs
Voting system software shall use the control constructs identified in Volume II,
Section 5:
*/

/*
424a. Acceptable constructs are Sequence, If-Then-Else, Do-While, Do-Until, Case,
and the General loop (including the special case for loop);
4-6 Volume I � Section 4
Software Standards
*/

/*
424b. If the programming language used does not provide these control constructs,
the vendor shall provide them (that is, comparable control structure logic).
The constructs shall be used consistently throughout the code. No other
constructs shall be used to control program logic and execution;
*/

/*
424 c. While some programming languages do not create programs as linear
processes, stepping from an initial condition, through changes, to a
conclusion, the program components nonetheless contain procedures (such as
�methods� in object-oriented languages). Even in these programming
languages, the procedures must execute through these control constructs (or
their equivalents, as defined and provided by the vendor); and
*/

/*
424 d. Operator intervention or logic that evaluates received or stored data shall not
re-direct program control within a program routine. Program control may be
re-directed within a routine by calling subroutines, procedures, and functions,
and by interrupt service routines and exception handlers (due to abnormal
error conditions). Do-While (False) constructs and intentional exceptions
(used as GoTos) are prohibited.
*/

//4.2.5 Naming Conventions

//Voting system software shall use the following naming conventions:

/*
425 a. Object, function, procedure, and variable names shall be chosen so as to
enhance the readability and intelligibility of the program. Insofar as possible,
names shall be selected so that their parts of speech represent their use, such
as nouns to represent objects, verbs to represent functions, etc.;
*/


// 425 b. Names used in code and in documentation shall be consistent;

/*
425 c. Names shall be unique within an application. Names shall differ by more than
a single character. All single-character names are forbidden except those for
variables used as loop indexes. In large systems where subsystems tend to be
developed independently, duplicate names may be used where the scope of
the name is unique within the application. Names should always be unique
where modules are shared; and
*/

#include "v425c.cc"

/*
425 d. Language keywords shall not be used as names of objects, functions,
procedures, variables, or in any manner not consistent with the design of the
language.
*/

/*
4-7 Volume I � Section 4
Software Standards
*/

/*
4.2.6 Coding Conventions
Voting system software shall adhere to basic coding conventions. The coding
conventions used shall meet one of the following conditions:
*/

/*
426 a. The vendors shall identify the published, reviewed, and industry-accepted
coding conventions used and the ITAs shall test for compliance; or
*/

/*
426 b. The ITAs shall evaluate the code using the coding convention requirements
specified in Volume II, Section 5.
These standards reference conventions that protect the integrity and security of the
code, which may be language-specific, and language-independent conventions that
significantly contribute to readability and maintainability. Specific style conventions
that support economical testing are not binding unless adopted by the vendor.
*/


/*
4.2.7 Comment Conventions
Voting system software shall use the following comment conventions:
*/

/*
427 a. All modules shall contain headers. For small modules of 10 lines or less, the
header may be limited to identification of unit and revision information. Other
header information should be included in the small unit headers if not clear from
the actual lines of code. Header comments shall provide the following
information:
1) The purpose of the unit and how it works;
2) Other units called and the calling sequence;
3) A description of input parameters and outputs;
4) File references by name and method of access (read, write, modify ,
append, etc.);
5) Global variables used; and
6) dDate of creation and a revision record;
*/

#include "v427a.cc"

/*
427 b. Descriptive comments shall be provided to identify objects and data types.
All variables shall have comments at the point of declaration clearly
explaining their use. Where multiple variables that share the same meaning
are required, the variables may share the same comment;
*/

#include "v427b.cc"

/*
427 c. In-line comments shall be provided to facilitate interpretation of functional
operations, tests, and branching;
*/

/*
4-8 Volume I � Section 4
Software Standards
*/

/*
427 d. Assembly code shall contain descriptive and informative comments such that
its executable lines can be clearly understood; and
*/

/*
427 e. All comments shall be formatted in a uniform manner that makes it easy to
distinguish them from executable code
*/

// (c) copyright codecheck rule-files abraxas-software 2006
