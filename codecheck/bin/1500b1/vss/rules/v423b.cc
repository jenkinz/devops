/*
VSS 423b. Each module shall be uniquely and mnemonically named, using names that
differ by more than a single character. In addition to the unique name, the
modules shall include a set of header comments identifying the module�s
purpose, design, conditions, and version history, followed by the operational
code. Headers are optional for modules of fewer than ten executable lines
where the subject module is embedded in a larger module that has a header
containing the header information. Library modules shall also have a header
comment describing the purpose of the library and version information;
*/


#include "vss.cch"

if ( mod_begin ) {
	    strncpy( TEMPBUF, mod_name(), BIGSTR );
		pctemp = strchr( TEMPBUF, '.' );
		pctemp = 0;

		if ( strlen(TEMPBUF) == 1 ) {
			WARN( 423, "4.2.3 B", "Module Names Must be more than one character before suffix" );
		}
}

// This rule verify's that ALL functions are similiar in name, to their module name.

if ( dcl_function  ) {

		strncpy( TEMPBUF, fcn_name(), BIGSTR );
		pctemp = strchr( TEMPBUF, '_' );

		if ( pctemp ) {
			strcpy( TEMPBUF, pctemp+1 );
		}

		if ( strstr( mod_name(), TEMPBUF ) == 0 ) {
			WARN( 432, "4.2.3 B", "Functions declared within a module must have name similiar to module" );
		}
}


