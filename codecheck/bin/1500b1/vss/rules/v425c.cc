/*
425 c. Names shall be unique within an application. Names shall differ by more than
a single character. All single-character names are forbidden except those for
variables used as loop indexes. In large systems where subsystems tend to be
developed independently, duplicate names may be used where the scope of
the name is unique within the application. Names should always be unique
where modules are shared; 
*/

#include "vss.cch"

if ( idn_variable ) {

	if ( strlen(idn_name()) == 1  && inforloop == 0 ) {

		WARN( 425, "4.2.5 C", "Single character variable names are forbidden" );
	}
}