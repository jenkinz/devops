/*
542 v. Only contains the minimum implementation of the �a = b ? c : d� syntax.
Expansions such as �j=a?(b?c:d):e;� are prohibited.
*/

#include "vss.cch"

if ( op_cond ) {

	WARN( 542, "5.4.2 V", "Condition C/C++ may be considered prohibited" );
}
