// (c) copyright codecheck rule-files abraxas-software 2006


/*
5.4.2 Assessment of Coding Conventions
r. Has functions with fewer than six levels of indented scope, counted as
follows:
*/

#include "vss.cch"

if ( lin_end ) {
	if ( lin_nest_level > 5 ) {
		WARN( 542, "5.4.2 R", "No more than six levels of indented scope" );
	}
}
