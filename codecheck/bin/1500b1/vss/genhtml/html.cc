/* CodeCheck Copyright (c) 1988-2006 by Abraxas Software Inc. (R).  All rights reserved. */

#include <html.cch>
#define SUBDIR	""	// set subdirectory pathname, was "html"

FILE *fp;   // project fp
FILE *mfp;  // module fp

char modname[64];
char fname[32];
char path[64];
char *cp;

int infunction;

#include <htmlcom.cc>	// generate HTML from Comments
#include <toc.cc> 	// generate Table of Contents

if ( prj_begin )
{
  infunction = 0;	// don't out source line
 
  if ( strlen( SUBDIR ) > 0 ) {
  	sprintf( path, "%s\\vss-test.html", SUBDIR );  // make subdir/vss-index.html
  }
  else {
	strcpy( path, "vss-test.html" );
  }
//DEBS( path )
  fp = fopen( path, "w" );
  if ( fp == 0 ) fatal( -1,  "file vss-index.html open failure" );


  PRJ( O_HTML )  //  fprintf( fp,  "<html>\n" );
  	
  fprintf( fp,  "<h1><a href=""http://www.abxsoft.com/index.html"">Abraxas Software, Inc.</h1>" );


  PRJS ( "<title>Voting System Standards - CodeCheck Rule-Files</title>\n", prj_name()  );

  fprintf( fp,  "<body>\n" );

  fprintf( fp, "<pre><a href=""vss-test.html"">VSS Package Index</a>  " );
  fprintf( fp, "<a href=""vss-index.html"">Abraxas VSS Home</a> " );
  fprintf( fp, "<a href=""toc.html"">Table of Contents</a></pre>\n" );
 	
  fprintf( fp, "<hr>\n");
  fprintf( fp, "<h1>VSS Standards Test Suite - %s</h1>\n", prj_name() ); 
  fprintf( fp, "<hr>\n");
 
}

if ( prj_end )
{
    PRJ( HR )
//  PRJ( "<a name=_external></a>\n" );
//	PRJ( "<pre>Reference External to Project" )

  fprintf( fp, "<pre><a href=""vss-test.html"">VSS Package Index</a>  " );
  fprintf( fp, "<a href=""vss-index.html"">Abraxas VSS Home</a> " );
  fprintf( fp, "<a href=""toc.html"">Table of Contents</a></pre>\n" );

  PRJ(O_H3)
  PRJ("CodeCheck Copyright (c) 1988-2006 by Abraxas Software Inc. (R).  All rights reserved.")
  PRJ(C_H3)

 	fprintf( fp, "</body>\n" );
	fprintf( fp, "</html>\n" );

	fclose( fp );
}


if (mod_begin)
{
  
  strcpy( modname, mod_name() );		// convert module suffix to .html
  strcpy( strchr( modname, '.' ), ".html" );
//warn(0, "%s", modname );
  if ( strlen(SUBDIR) > 0 ) {
	sprintf( path,"%s\\%s", SUBDIR, modname );  // make subdir/mod.html
  }
  else {
	  strcpy( path, modname );
  }
 
DEBS( path );
  mfp = fopen( path, "w" );
  if ( mfp == 0 ) fatal( -1, "file [module].html open failure" );
  MOD( O_HTML );
  MOD( O_HEAD );
  MODS ( "<title>CodeCheck - C++ Coding Standards Test Suite C++ MODULE - %s </title>\n", mod_name()  );
  MOD( C_HEAD );

  fprintf( mfp, "<pre><a href=""vss-test.html"">VSS Package Index</a>  " );
  fprintf( mfp, "<a href=""vss.html"">Abraxas VSS Home</a> " );
  fprintf( mfp, "<a href=""toc.html"">Table of Contents</a></pre>\n" );

// home page

  fprintf( mfp, "<h1><a href=""http://www.abxsoft.com/index.html"">Abraxas Software, Inc.</h1>" );
  fprintf( fp,  "<h2><a href=""http://www.abxsoft.com/index.html"">Abraxas Software, Inc.</h1>" );

  MOD ( HR );
  PRJ( HR ) 
 // PRJ( O_H2 )

  fprintf( fp, "<h2><a href=%s>Module - %s</a></h2>\n", modname, modname );
  PRJ( HR )
  	
  MOD( O_BODY );
  fprintf( mfp, "<h1>CodeCheck Voting System Standards Test Suite - %s</h1>\n", mod_name() ); 

 
  fprintf( mfp, "<a name=""_top_""></a>" ); // mark top
}

if (mod_end)
{
//  MOD( HR );
//  fprintf( mfp, "<p>Module End - %s\n", mod_name() );

  MOD( HR );
  fprintf( mfp, "<pre><a href=""vss-test.html"">VSS Package Index</a>  " );

  fprintf( mfp, "<a href=""vss-index.html"">Abraxas VSS Home</a> " );

  fprintf( mfp, "<a href=""toc.html"">Table of Contents</a></pre>\n" );

  MOD(O_H3)
  MOD("CodeCheck Copyright (c) 1988-2006 by Abraxas Software Inc. (R).  All rights reserved.")
  MOD(C_H3)

  MOD( C_BODY );
  fclose ( mfp );  
}  


if (fcn_begin)
{
	strcpy( fname, fcn_name() );
 
	PRJ( "<pre>\n" );
 	PRJ( "Function Definition: " )
 	PRJS(  "<a name=%s></a>", fname );	   // tag

	fprintf( fp,  "<a href=%s#%s>%s</a>\n", modname,fname,fname ); // link

	MOD( HR )	// fprintf( mfp, "<hr>\n");

 	fprintf( mfp, "<pre>" );

	fprintf( mfp, "<a name=""%s""></a>\n", fname ); // tag
	fprintf( mfp, "<a name=""%s()""></a>\n", fname ); // tag()

	MOD( O_H3 )
	fprintf( mfp, "Function Name: %s()\n", fname, fname ); // Display f() name
	MOD( C_H3 );

//	MODS( "<pre>\n%s\n</pre>\n", line() );

	infunction = 1;		// output source line
}

if ( lin_end ) {


	if ( infunction ) {

		fprintf( mfp, "<pre>%s(%d): %s\n</pre>", file_name(), lin_number, line() );
	}
	else if ( lin_is_comment /*&& !lin_within_function*/  ) {

		fprintf( mfp, "<pre>%s\n</pre>", line() );
	}
    else {
        fprintf( mfp, "<pre>%s(%d): %s\n</pre>", file_name(), lin_number, line() );
    }
/*
	if (  lin_has_comment ) {		 // all lines of code are emitted to MOD

		MOD( O_H3 )
		MOD( line() );			 // comment
		MOD( C_H3 )
	}
*/
   	if ( lin_has_comment  ) {

		PRJ( O_PRE )
		PRJ( line() );			 // comment
		PRJ( C_PRE )
	}
}
		
if (fcn_end)
{	
//	MODS("<pre>\n%s\n</pre>\n", line() );

  	fprintf( mfp,"</pre>\n");
	infunction = 0;
}   


/* Don't Need to Log All function Call's
if ( op_call ) {

    strcpy ( fname, op_function() );	 // current function name as called
    
   	fprintf( mfp, "call <a href=""vss-index.html#%s"" >%s()</a>\n", fname, fname  );
}
*/