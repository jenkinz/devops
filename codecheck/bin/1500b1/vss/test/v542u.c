/*
vss542u. Has all constants other than 0 and 1 defined or enumerated, or shall have a
comment which clearly explains what each constant means in the context of
its use. Where �0� and �1� have multiple meanings in the code unit, even
they should be identified. Example: �0� may be used as FALSE, initializing
a counter to zero, or as a special flag in a non-binary category.
*/

// good cases

#define FOURLETTERWORD 100
#define NULL 0

char my_array[FOURLETTERWORD];	//	is fine

int beginTime;
 beginTime = time (NULL);

int array[100];	// good, has comment

// BAD CASES FOLLOW ....


char my_array2[4];			
int array[100];

int dog = 100;		

char my_array1[4];				

