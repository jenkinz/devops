/*
425 c. Names shall be unique within an application. Names shall differ by more than
a single character. All single-character names are forbidden except those for
variables used as loop indexes. In large systems where subsystems tend to be
developed independently, duplicate names may be used where the scope of
the name is unique within the application. Names should always be unique
where modules are shared; 
*/


int d = 0;  /* not allowed unless used in as a loop index	*/

v4() {

	int i;

	int dog;

	for ( ;; i ) {} // ok

	i++;	// bad

	dog++;	// good
}

v4a() {

//For example: 

	int i = 0; 
	i = getImportantValue(); 

	switch(i){	//not allowed 
		case 1:

		default:
	} 

}

