// 4.2.2 Software Integrity
/*
Self-modifying, dynamically loaded, or interpreted code is prohibited, except under
the security provisions outlined in section 6.4.e. This prohibition is to ensure that the
software tested and approved during the qualification process remains unchanged and
retains its integrity. External modification of code during execution shall be
prohibited. Where the development environment (programming language and
development tools) includes the following features, the software shall provide
controls to prevent accidental or deliberate attempts to replace executable code:
*/

// c Dynamic memory allocation and management.

v422c() {

	char *mp = malloc(100);	// illegal

	free(mp);				// illegal
}