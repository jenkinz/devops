/*
423c. All required resources, such as data accessed by the module, should either be
contained within the module or explicitly identified as input or output to the
module. Within the constraints of the programming language, such resources
shall be placed at the lowest level where shared access is needed. If that
shared access level is across multiple modules, the definitions should be
defined in a single file (called header files in some languages, such as C)
where any changes can be applied once and the change automatically applies
to all modules upon compilation or activation;
*/


int v423c ;	// no global data
