
// 4.2.2 Software Integrity

/*
Self-modifying, dynamically loaded, or interpreted code is prohibited, except under
the security provisions outlined in section 6.4.e. This prohibition is to ensure that the
software tested and approved during the qualification process remains unchanged and
retains its integrity. External modification of code during execution shall be
prohibited. Where the development environment (programming language and
development tools) includes the following features, the software shall provide
controls to prevent accidental or deliberate attempts to replace executable code:
*/

// a Unbounded arrays or strings (includes buffers used to move data)


// global
char foo[];	// bad unbounded

char foo1[100];	// good

// local
v1() {

char foo[];	// bad unbounded

char foo1[100];	// good

}