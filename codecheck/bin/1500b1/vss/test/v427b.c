/*
427 b. Descriptive comments shall be provided to identify objects and data types.
All variables shall have comments at the point of declaration clearly
explaining their use. Where multiple variables that share the same meaning
are required, the variables may share the same comment;
*/

// first foo is bad at line 9, no comment associated


int foo;


int fuu; /* ok */

// good
int foam;

int fubar;	//good

v6() {	// next foo is bad, no comment

	int foo;


	int fubar;	//good

}

//O.K.  Does this one also take into account - YES
/* good */ 
int foo = 0; 