/*
VSS 423b. Each module shall be uniquely and mnemonically named, using names that
differ by more than a single character. In addition to the unique name, the
modules shall include a set of header comments identifying the module�s
purpose, design, conditions, and version history, followed by the operational
code. Headers are optional for modules of fewer than ten executable lines
where the subject module is embedded in a larger module that has a header
containing the header information. Library modules shall also have a header
comment describing the purpose of the library and version information;
*/

vss_423b() {	// good function, is similiar to module name









}

test() {	// ok, small routine

}

testfun() {	// bad this function doesn't belong here









}