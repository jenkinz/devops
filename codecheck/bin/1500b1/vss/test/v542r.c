/*
5.4.2 Assessment of Coding Conventions
r. Has functions with fewer than six levels of indented scope, counted as
follows:
*/


int function_v542K()

{
if (a = true)
//1
{
if ( b = true )
//2
{
if ( c = true )
//3
{
if ( d = true )
//4
{
while(e > 0 )
//5
{
//code
}
}
}
}
}
}

int function_v542K_bad()

{
if (a = true)
//1
{
if ( b = true )
//2
{
if ( c = true )
//3
{
if ( d = true )
//4
{
while(e > 0 )
//5
{
	if (  f > 0 ) { exit(); }	// six, ILLEGAL
}
}
}
}
}
}