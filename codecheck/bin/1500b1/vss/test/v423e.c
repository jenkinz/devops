/*
423e. Each module shall have a single entry point, and a single exit point, for
normal process flow. For library modules or languages such as the objectoriented
languages, the entry point is to the individual contained module or
method invoked. The single exit point is the point where control is returned.
At that point, the data that is expected as output must be appropriately set.
The exception for the exit point is where a problem is so severe that
execution cannot be resumed. In this case, the design must explicitly protect
all recorded votes and audit log information and must implement formal
exception handlers provided by the language; and
*/

int Func1()
{
	int dog = 0;
	int clog = 0;

	/* Do some stuff with dog and clog here	*/

	if( 0 < dog )	{
		return dog;
	}else if( 0 < clog )	{
		exit(-1); 
	}
} 	/* This is not allowed two return points	*/

//This also applies to exit() calls.

int vss_423e()
{
	return;		// explicit return at end of function is ok, 
}