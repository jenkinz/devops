/*
542 v. Only contains the minimum implementation of the �a = b ? c : d� syntax.
Expansions such as �j=a?(b?c:d):e;� are prohibited.
*/

f542v() {

a = b ? c : d ;		// even the usage of this is questionable, the use of nested is illegal
}