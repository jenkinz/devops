/*
vss 542s. Initializes every variable upon declaration where permitted
*/

// global

int foo = 1;	//good

int foam;		// bad

// local
v8() {

	int foo = 1;	// good

	int foam;		// bad
}
