
/*
427 a. All modules shall contain headers. For small modules of 10 lines or less, the
header may be limited to identification of unit and revision information. Other
header information should be included in the small unit headers if not clear from
the actual lines of code.
*/


/* =========================================================================*/
/**                                                                           
    FUNCTION:      VSS_BeginTasks()

    PURPOSE:       Initialize all tasks.

    PARAMETERS:
		none

    OUTPUT PARAMETERS:
       	Integer

    RETURN VALUE:
       	int = 0 for success or positive value for failure 

    USE OF GLOBALS:


    FILES REFERENCED:
 

    FUNCTIONS CALLED:

        read

    REVISIONS:

*/

VSS_BeginTasks() {




// line 5



// line 10

}

// bad case, no header


VSS_BeginTask_BAD() {


	read();

// line 5



// line 10
	return(0);
}

v5tiny() {
	// ok, small
}