

#define COPYRIGHT "\n### CodeCheck Copyright(C)1988-2005 by Abraxas Software Inc (R). All rights reserved.\n\n"



/* generate CCP file from gnu-gcc verbose output */



#include <stdio.h>

#include <stdlib.h>

#include <string.h>



//#define DEBUG    // comment out this line to disable diagnostics



#define MAXLINE      8192

#define IDLEN        64



#define FATAL    -1



char line[MAXLINE];

char linBuf[MAXLINE];



char outname[128];



int linenum = 1;



int main( int argc, char ** argv );

void exception( int f, char * m );

void mapenv( FILE *, char * );



int define_seen = 0, include_seen = 0;



int main( int argc, char** argv )

{

    int i, j;

    FILE* fdin;

    FILE* fdout = stdout;

    char *lp;



#ifdef DEBUG

printf( "argc=%d argv[1]=%s\n", argc, argv[1] );

#endif

    if ( argc <= 1 ) exception( FATAL, "usage: mkccp test.c.txt [ test_c.ccp ] default to stdout" );



    fdin = fopen( argv[1], "r" );    // open input source [ test.txt ]

	

    if ( !fdin )

    {

        printf( "arg1 can not open file %s\n", argv[1] );

        exit (-1);

    }



	if ( argc > 2 ) {

    	fdout = fopen( argv[2], "w" );    // open output [ test.ccp ]

    	if ( !fdout )

    	{

        	printf( "arg2 can not open file %s\n", argv[2] );

        	exit (-1);

   		}

	}

	



    fprintf( fdout, COPYRIGHT );

	

	fprintf( fdout, "# GCC - G++ CONFIG\n\n" );

	fprintf( fdout, "-K13\n\n" );

	fprintf( fdout, "-D__builtin_va_list=char\n" );

	fprintf( fdout, "-D__const=const\n" );

     	

    while (  lp = fgets( line, MAXLINE, fdin ) )

    {



#ifdef DEBUG

//printf( "%d: [%d] %s\n", linenum, strlen(lp), lp );

#endif

        line[strlen( line ) - 1] = 0;

        linenum++; // increment line number



 // include termination

    if ( strstr( line, "End of search list." ) ) {

//printf( "*** #include END\n" );

        include_seen = 0;    // terminate search list

    }

        mapenv( fdout, line );



 // terminate inlude

        if ( strstr( line, "#include <...> search starts here:" ) ) {

//printf( "*** #include BEGIN\n");

            include_seen = 1;    // mark begin of include

        }

    } ;



    fclose ( fdin );

	

    fprintf( fdout, COPYRIGHT );

	

    fclose ( fdout );



    exit( 0 );

}



// this line contains real code - map all tokens to be mapped



void mapenv( FILE *fdout, char * buffer )

{

 char tmp[MAXLINE], outbuf[MAXLINE];

 char comments[MAXLINE]=" ", tmpbuf[MAXLINE];



 // the seps contains char's to break while parsing, if your path includes one of these, then remove char



 char seps[] = ",\t\b\n\v\f\r\\(){}[]!@#$%^&*:;,?~ ";



 char *token, newid[IDLEN];

 char *tokp;





// begin processing



    strcpy ( tmp, buffer );      // tokenization buffer



    token = strtok( tmp, seps ); // offset: current start location



// ouput #define & #include search list



    while  ( token != NULL  ) {

#ifdef DEBUG

//printf( "[TOK %s] D=%d I=%d\n", token, define_seen, include_seen );

#endif



        if ( strstr(token, "-D") ) {



            define_seen++;



            if ( define_seen == 1 ) {



                fprintf( fdout, "\n\n#DEFINE SECTION GCC\n\n" );

#ifdef __STDC__

fprintf( fdout,"-D__STDC__=1\n");

#endif



#ifdef __linux__

fprintf( fdout,"-D__LINUX__\n");

#endif



#ifdef __cplusplus

fprintf( fdout, "-D_cplusplus\n" );

#endif    



			fprintf( fdout, "\n\n" );

			

			}



            strcpy( outbuf, token );

            tokp = strpbrk( outbuf, " " );

            tokp = 0;



            fprintf( fdout, "%s\n", outbuf );

        }



		

        if ( include_seen ) {



            if ( include_seen == 1 ) {

                fprintf( fdout, "\n\n#INCLUDE SECTION GCC\n\n" );

            }



            strcpy( outbuf, token );

            tokp = strpbrk( outbuf, "/" );



//printf( "***INCLUDE %s\n", token );

           fprintf( fdout, "-I%s\n", outbuf );



            include_seen++;

        }



           token = strtok( NULL, seps );    // get next token

     }

}





void

exception( int f, char *m)

{

  fprintf( stderr, "Exception:: %s:%d\n", m, linenum );

  if (f==FATAL) exit(1);

}

