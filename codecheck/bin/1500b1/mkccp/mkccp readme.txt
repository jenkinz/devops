MKCCP

Make CCP file

To be used with GCC compilers, this DOES apply to CYGWIN on windows, ..

gcc -v test.c 2> test.c.txt

mkccp test.c.txt > config.ccp

./check config.ccp test

This is a simple way of teaching codecheck how to emulate your GCC compiler for ANY operating system.


To build mkccp, just type "cc mkccp.c", edit if you wish.
