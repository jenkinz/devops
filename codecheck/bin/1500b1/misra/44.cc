// Redundant explicit casts should not be used

#include "misra.cch"

if ( op_cast ) {

	WARN_MA( 44, "Redundant explicit casts should not be used." );

}