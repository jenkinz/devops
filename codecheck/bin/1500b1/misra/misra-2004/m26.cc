/*
 *	Rule 26:	Required
 *			
 *	If objects are declared more than once they shall be declared
 *	compatibly.
*/ 

#include "misra.cch"

if ( dcl_conflict ) {

	WARN_MR( 26, "8.4 If objects are declared more than once they shall be declared compatible" );

}