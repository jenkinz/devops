/*
 *	Rule 111:	Required
 *				
 *	Bit fields shall only be defined to be of type unsigned int or
 *	signed int.
*/ 


if ( dcl_bitfield ) {

	if (  !(dcl_signed || dcl_unsigned) ) {

		WARN( 111, "6.4 Bit fields shall be defined unsigned int or signed int" )
	}

}