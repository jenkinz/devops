/*
 *	Rule 37:	Required
 *			
 *	Bitwise operations shall not be performed on signed integer types.
*/ 

#include "misra.cch"

// 12.7 Bitwise operations shall not be performed on signed integer types.

if ( op_bitwise || op_right_shift || op_left_shift ) {

	if ( op_base(1) == INT_TYPE ) {
		
	  WARN_MR( 37, "12.7 Bitwise operations shall not be performed on signed integer types.");
	}

}
// 10.5 if bitwise operators ~ and << are applied, then use an explicit cast

int casttype;
int castseen;
int bitcast;

if ( op_cast ) {
	casttype = op_base(1) ;
	castseen = lin_number;
}

if ( op_bitwise ||  op_left_shift ) {

//warn( 37, "10.5 cs%d ct%d ob%d", castseen, casttype, op_base(1) );
		if ( op_base(1) == USHORT_TYPE || op_base(1) == UCHAR_TYPE ) {
			bitcast = lin_number;
		}
}

if ( op_assign ) {

	if ( bitcast ) {
			if ( castseen != lin_number || casttype != op_base(1) ) {
				WARN_MR( 37, "10.5 if bitwise operators ~ and << are applied, then use an explicit cast" );
			}
	}

	bitcast = 0;
	castseen = 0;
	casttype = 0;
}

