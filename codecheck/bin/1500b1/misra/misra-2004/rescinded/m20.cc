/*
 *	Rule 20:	Required
 *			
 *	All object and function identifiers shall be declared before use.
*/ 

	if ( idn_no_prototype ) {
		WARN_MR( 20, "All object and function identifiers shall be declared before use." )
	}