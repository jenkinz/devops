// COPYRIGHT (C) 2006 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.  


/*
 *	Rule 44:	Advisory
 *			
 *	Redundant explicit casts should not be used.
*/ 

#include "misra.cch"

if ( op_cast ) {

	WARN_MA( 44, "Redundant explicit casts should not be used." );

}

// COPYRIGHT (C) 2006 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.  
