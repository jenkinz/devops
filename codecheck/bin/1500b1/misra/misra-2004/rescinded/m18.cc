/*
 *	Rule 18:	Advisory
 *			
 *	Numeric constants should be suffixed to indicate type if an
 *	appropriate suffix is available.
*/ 

#include "misra.cch"


int lexsuf, longsuf;

if ( mod_begin ) { 
	
	lexsuf = 0;
	longsuf = 0;
}

if( lex_unsigned ) { lexsuf = UINT_TYPE; }

if ( lex_float ) { lexsuf = FLOAT_TYPE; }

if ( lex_lc_long ) longsuf = 'l';

if ( lex_uc_long ) longsuf = 'L';

if ( lex_long_float ) lexsuf = LONG_DOUBLE_TYPE;


if ( dcl_variable ) {

	if ( strcmp( token(), "=" ) == 0 ) { // followed by assignment?

		switch ( dcl_base ) {	// if mismatch set lexsuf to -1

			case UINT_TYPE:
				if ( lexsuf != UINT_TYPE ) lexsuf = -1;
				break;

			case ULONG_TYPE:
				if ( lexsuf != ULONG_TYPE || longsuf != 'L' ) 
					lexsuf = -1;
				break;

			case DOUBLE_TYPE:
				if ( longsuf == 'L' ) lexsuf = -1;
				break;

			case FLOAT_TYPE:
				if ( lexsuf != FLOAT_TYPE ) lexsuf = -1;
				break;

			case LONG_DOUBLE_TYPE:
				if ( longsuf != 'L' ) lexsuf = -1;
				break;

			case LONG_TYPE:
				if ( longsuf != 'L' ) lexsuf = -1;
				break;

			default:	
				lexsuf = 0;
				break;

		}

		if ( lexsuf == (-1) ) {	
			WARN_MA(18,"Numeric constants should be suffixed to indicate type.");				}

		lexsuf = 0;
		longsuf = 0;
	}
}