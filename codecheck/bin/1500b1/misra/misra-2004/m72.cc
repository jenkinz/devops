/*
 *	Rule 72:	Required
 *			
 *	For each function parameter the type given in the declaration and
 *	definition shall be identical, and the return types shall also be
 *	identical.
*/ 

#include "misra.cch"


if ( dcl_conflict ) {

	WARN_MR( 72, "8.3 Function parameter given in declaration/definition must be same.");
}