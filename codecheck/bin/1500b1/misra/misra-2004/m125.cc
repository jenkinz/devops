// COPYRIGHT (C) 2006 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.  


/*
 *	Rule 125:	Required
 *			
 *	The library functions atof, atoi and atol from header file
 *	<stdlib.h> shall not be used.
 *	
*/ 

if ( idn_function ) {

	if ( CMPPREVTOK("atof") || CMPPREVTOK("atoi") || CMPPREVTOK("atol") ) {
	
		WARN_MR( 125, "20.9 Library functions atof, atoi and atol should not be used." );
	}
}

// COPYRIGHT (C) 2006 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.  
