/*
 *	Rule 5:	Required
 *			
 *	Non ISO C characters.
*/ 

#include "misra.cch"


if ( lex_constant ) {


	if ( CMPTOK("\\c") ) {

		WARN_MR( 5, "4.1 Non ISO C characters.");
	}
}