/*
 *	Rule 83:	Required
 *			
 *	For functions with non-void return type:
 *		i) 	there shall be one return statement for every exit
 *			branch (including the end of the program),
 *		ii)	each return shall have an expression,
 *		iii)	the return expression shall match the declared return
 *			type.
*/ 

int curFbase;

if ( dcl_function ) {

	curFbase = dcl_base_root;
}

if ( stm_return_void ) {

	if ( curFbase != VOID_TYPE ) {

		WARN_MR( 83, "Non void return type must be explicit expression." );
	}


}

if ( op_return ) {

	if ( op_base(1) != curFbase ) {
	
		WARN_MR( 83, "Non Void return type must match." );
	}
}