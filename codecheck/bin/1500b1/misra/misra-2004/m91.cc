 /*
 *	Rule 91:	Required
 *			
 *	Macros shall not be #defined'd and #undef'd in a block.
*/

#include "misra.cch"

// a little too convoluted, a better algo would be #DEF in function/class

int inBrace;	// in curly brace

if ( op_open_brace ) inBrace++;

if ( op_close_brace ) inBrace--;

if ( lin_preprocessor ) {

	if ( inBrace &&  
		((lin_preprocessor==DEFINE_PP_LIN) || (lin_preprocessor==UNDEF_PP_LIN)) ){
		
			WARN_MR( 91, "19.5 Macros shall not be #defined'd and #undef'd in a block." )
	}
}