
/*
 *	Rule 15:	Advisory
 *			
 *	Floating point implementations should comply with a defined
 *	floating point standard.
 *
 *	Not automatically enforceable.  Requires use of packages such as
 *	paranoia.
*/ 

#include "misra.cch"

if ( dcl_variable ) {

	switch ( dcl_base ) {

		case	FLOAT_TYPE:

		WARN_MA( 15, "1.5 Floating point implementations should comply" );
				break;
	}
}