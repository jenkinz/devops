/*
 *	Rule 118:	Required
 *			
 *	Dynamic heap memory allocation shall not be used.
*/ 

if ( idn_function ) {
	if ( idn_name() ) {
		if ( strcmp( idn_name(), "malloc" ) == 0 
			|| strcmp( idn_name(), "realloc" ) == 0 
			|| strcmp( idn_name(), "calloc" ) == 0) {

			WARN_MR( 118, "20.4 Dynamic heap memory allocation shall not be used." );
		}
	}
}