/*
 *	Rule 13:	Advisory
 *			
 *	The basic types char, short, int, long, float and double should not
 *	be used.
*/ 

#include "misra.cch"


if ( dcl_local ) {

	switch ( dcl_base ) {

			case 	CHAR_TYPE:
			case 	SHORT_TYPE:
			case	INT_TYPE:
			case	LONG_TYPE:
			case	FLOAT_TYPE:
			case	DOUBLE_TYPE:
			
		WARN_MA( 13, "6.3 Typedefs should be used in place of basic types" );
				break;
	}
}