/*
 *	Rule 34:	Required
 *			
 *	The operands of a logical && or || shall be primary expressions.
*/ 

#include "misra.cch"

if ( op_log_or || op_log_and ) {

	if ( chkexp ) {

		if ( (op_parened_operand(1) & op_parened_operand(2)) == 0 ) {
				
			if ( all_digit( prev_token() ) ) {

			    WARN_MR( 34, "12.5 operands of && or || must be primary-expression.");
			}
		}
	}
}