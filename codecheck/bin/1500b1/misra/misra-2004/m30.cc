/*
 *	Rule 30:	Required
 *			
 *	All automatic variables shall have been assigned a value before
 *	being used.
 *
 *	This is not automatically detectable statically in all cases but
 *	the following should exercise a tool.
*/ 

if ( idn_no_init) {

	WARN_MR(30, "9.1 All automatic variables shall have been assigned a value" );
}