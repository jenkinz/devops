/*
 *	Rule 52:	Required
 *			
 *	There shall be no unreachable code.
*/ 


#include "misra.cch"

 
// This is no problem do, but its a whole rule-file within itself, contact abraxas for support



int pstm;

if ( stm_kind ) {
//warn( 6, "SK %d pst%d", stm_kind, pstm );

    if ( pstm == RETURN && ( stm_kind == EXPRESSION ||  stm_kind == RETURN ) ) {

        WARN_MR( 52, "14.1 There shall be no Unreachable Code" );
    }

    else if ( pstm == GOTO && ( stm_kind == EXPRESSION ||  stm_kind == RETURN ) ) {

        WARN_MR( 52, "14.1 There shall be no Unreachable Code" );
    }
    pstm = stm_kind ;
}