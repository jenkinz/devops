// Misra C Enforcement Testing
//
// Rule 16: Required
// Underlying floating point bit representations should not be used.
//
// Not automatically enforceable.
///


#include "misra.cch"

int floatseen;	

if ( dcl_member ) {

//warn( 16, "DM b%d mem%d", dcl_base, dcl_member );
	if ( dcl_base == FLOAT_TYPE && dcl_member == 1 ) {
		WARN_MR( 16, "12.12 Underlying floating point bit representations should not be used." );
        }
}