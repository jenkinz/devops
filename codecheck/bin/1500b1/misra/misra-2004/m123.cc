/*
 *	Rule 123:	Required
 *			
 *	The signal handling facilities of <signal.h> shall not be used
*/ 

if ( lex_macro ) {

	if ( CMPTOK("SIGINT") ) {
		
		WARN_MR( 123, "20.8 The signal handling facilities of shall not be used. { SIGINT }" );
	}

}

if ( header_name() ) {		// at #include

		if ( strcmp(header_name(), "signal.h" ) == 0 ) {
			WARN_MR( 123, "20.8 The signal handling facilities of <signal.h> shall not be used." );
		}
	}