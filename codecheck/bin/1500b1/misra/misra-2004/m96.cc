/*
 *	Rule 96:	Required
 *			
 *	In the definition of a function-like macro the whole definition,
 *	and each instance of a parameter, shall be enclosed in parentheses.
*/ 

#include "misra.cch"

if ( pp_arg_paren ) {
	WARN_MR( 96, "19.10 function-like macro parameter, shall be enclosed in paren" )
}
