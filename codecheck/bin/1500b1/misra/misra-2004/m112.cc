/*
 *	Rule 112:	Required
 *				
 *	Bit fields of type signed int shall be at least 2 bits long.
*/ 

#include "misra.cch"


if ( dcl_bitfield ) {

	if (  dcl_signed ) {

		if ( dcl_bitfield_size < 2 ) {

		 WARN( 112, "6.5 Bit fields of type signed int shall be at least 2 bits long." )
		}
	}
}