/*
 *	Rule 115:	Required
 *			
 *	Standard library function names shall not be reused.
*/ 

#include "misra.cch"


if ( dcl_variable || dcl_function ) {
//warn( 115, "DV-DF %s dc%d dd%d find%d ", dcl_name(), dcl_conflict, dcl_definition, find_root(dcl_name()) );
	if ( dcl_definition && find_root(dcl_name()) > 0 ) {
		WARN_MR( 115, "20.2 Reserved words and standard library function names shall not be used { DECL }" )
	}
}