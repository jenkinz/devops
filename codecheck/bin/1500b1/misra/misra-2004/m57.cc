/*
 *	Rule 57:	Required
 *			
 *	The continue statement shall not be used.
*/ 

#include "misra.cch"

if ( op_continue ) {

	WARN_MR( 57, "14.5 The continue statement shall not be used.");
}