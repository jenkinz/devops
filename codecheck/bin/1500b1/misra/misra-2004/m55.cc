/*
 *	Rule 55:	Advisory
 *			
 *	Labels should not be used, except in switch statements.
*/ 

#include "misra.cch"

if ( op_colon_1 ) {

	if (  stm_depth == 0 ) {

		WARN_MA( 55, " Labels should not be used, except in switch statements.");
	}

}