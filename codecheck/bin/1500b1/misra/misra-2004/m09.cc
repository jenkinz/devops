/*
 *	Rule 9:		Required
 *			
 *	No nested comments
*/ 

#include "misra.cch"

if ( lex_nested_comment ) {
	WARN_MR( 9, "2.3 No nested comments" )
}