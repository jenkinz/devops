/*
 *	Rule 92:	Advisory [ 2004 REQ ]
 *			
 *	#undef should not be used.
*/ 

#include "misra.cch"

if ( lin_preprocessor ) {

	if ( lin_preprocessor == UNDEF_PP_LIN ) {

		WARN_MR( 92, "19.6 #undef should not be used." );
	}
}