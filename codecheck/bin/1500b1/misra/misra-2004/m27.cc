/*
 *	Rule 27:	Advisory
 *			
 *	Objects should be declared as extern in only one file.
*/ 

#include "misra.cch"


if ( dcl_conflict ) {

	if ( dcl_storage_flags & EXTERN_SC ) {

		WARN_MR( 27, "8.8 Objects should be declared as extern in only one file. { CONFLICT }" );
	}
}

if ( dcl_extern ) {

	if ( curtype && strcmp(curname,file_name()) ) {

		WARN_MR( 27, "8.8 Objects should be declared as extern in only one file { Re-Use }." );

	}
}