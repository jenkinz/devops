/*
 *	Rule 75:	Required
 *			
 *	Every function shall have an explicit return type.
*/ 

#include "misra.cch"

if ( dcl_no_specifier ) {

	if ( dcl_level(0) == FUNCTION ) {

		WARN_MR(75, "8.2 Every function shall have an explicit return type.");
	}
}