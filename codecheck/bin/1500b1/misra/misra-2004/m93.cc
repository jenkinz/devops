/*
 *	Rule 93:	Advisory
 *			
 *	A function should be used in preference to a function-like macro
*/ 

#include "misra.cch"

if ( pp_arg_count > 0 && pp_lowercase ) {

	WARN_MA( 93, "19.7 A function should be used in preference to a function-like macro" )
}

if ( op_macro_call ) {

		WARN_MA( 93, "19.7 A function should be used in preference to a function-like macro" )
}

if ( pp_arg_count  ) {

	if ( pp_arg_count > 0 ) {

		WARN_MA( 93, "19.7 A function should be used in preference to a function-like macro" )
	}
}