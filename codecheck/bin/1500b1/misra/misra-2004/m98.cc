
/*
 *	Rule 98:	Required
 *			
 *	At most one of # or ## in a single macro definition
*/ 

#include "misra.cch"


int ppStr;
int ppPas;

if ( lin_end  ) {

	ppStr = 0;
	ppPas = 0;
}

if ( pp_paste ) { 

	ppPas++; 
	if ( ppPas > 1 ) {
		WARN_MR( 98, "19.12 At most one of # or #\# in a single macro definition.");
	}
}

if ( pp_stringize ) { 

 	ppStr++; 

	if ( ppStr > 0 ) {

		WARN_MR( 98, "19.13 At most one of # or #\# in a single macro definition.");
	}
}
