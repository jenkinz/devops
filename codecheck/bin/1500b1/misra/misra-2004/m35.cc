/*
 *	Rule 35:	Required
 *			
 *	Assignment operators shall not be used in expressions which return
 *	Boolean values.
*/ 

#include "misra.cch"


if ( op_assign ) {

	if ( chkexp ) {

		WARN_MR( 35, "13.1 Assignment operators shall not be used in expressions." );
	}
}