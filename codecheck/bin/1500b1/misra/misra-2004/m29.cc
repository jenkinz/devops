/*
 *	Rule 29:	Required
 *			
 *	The use of a tag should be consistent with the declaration.
 *
 *	misra-c:2004 5.4 A tagname shall be a unique identifier
*/ 

#include "misra.cch"

if ( tag_hidden ) {

	WARN_MR( 29, "5.4 A tagname shall be a unique identifier. { hidden tag }" );
}