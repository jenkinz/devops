/*
 *	Rule 67:	Advisory
 *			
 *	Numeric variables being used within a for loop for iteration
 *	counting should not be modified in the body of the loop.
 *
 *	This is in general difficult to detect automatically and statically.
 *	The following gives a simple case.
*/ 

#include "misra.cch"

char for_itername[MAXIDLEN];	// rule 67

if ( op_assign ) {
//warn( 67, "ASS for%d name=%s forcon%d", inforloop,  for_itername , inforcon );
	if ( inforloop && idn_name() ) {
		if ( inforcon /*&& strlen(for_itername) == 0*/ ) {
	
			strcpy( for_itername, idn_name() ); // get iter name
		}
		else if ( (inforcon == 0) && (strcmp(for_itername,idn_name())==0) ) {

			WARN_MA( 67, "13.6 For loop iteration variables should not be modified in body.");
		}
	}
}

if ( stm_cp_begin ) {
	if ( stm_kind == FOR ) {
		for_itername[0] = 0;
	}
}