/*
 *	Rule 99:	Required
 *			
 *	All uses of the #pragma directive shall be documented and
 *	explained.
*/ 

#include "misra.cch"

if ( lin_preprocessor ) {

	if ( lin_preprocessor == PRAGMA_PP_LIN ) {

		WARN_MR( 99, "3.4 All uses of the #pragma directive shall be documented." );
	}
}