// COPYRIGHT (C) 2006 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.  

/*
 *	Rule 45:	Required
 *			
 *	Type casting from any type to or from pointers shall not be used.
*/ 

if ( op_cast ) {

 	if ( op_level(1,0) == POINTER || op_level(2,0) == POINTER ) {

		WARN_MR(45, "11.2 Type casting from any type to or from pointers shall not be used.");
	}
}

// COPYRIGHT (C) 2006 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.  
