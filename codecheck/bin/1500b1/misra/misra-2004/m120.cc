/*
 *	Rule 120:	Required
 *			
 *	The macro offsetof shall not be used.
*/ 

if ( lex_macro ) {	// expand a macro

	if ( token() && strlen(token())>0 ) {
		if ( strcmp( token(), "offsetof" ) == 0 ) {

			WARN_MR( 120, "20.6 The macro offsetof shall not be used." );
		}
	}
}