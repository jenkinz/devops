/*
 *	Rule 97:	Advisory
 *			
 *	Identifiers in pre-processor directives should be defined before
 *	use.
*/ 

#include "misra.cch"

if ( pp_not_defined ) {
	WARN_MA( 97, "19.11 Identifiers in pre-processor directives should be defined" )
}

