// COPYRIGHT (C) 2006 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.  



/*
 *	Rule 47:	Required [ advised 2004 ]
 *			
 *	No dependence should be placed on C's operator precedence rules in
 *	expressions.
*/ 

// This rule requires -c option ON, Full Type Checking

#include "misra.cch"



if ( op_assign ) {

	if ( (binary_exp > 2) && in_paren == 0 ) {

		WARN_MA( 47, "12.1 No dependence should be placed on C's operator precedence" );
	}
}