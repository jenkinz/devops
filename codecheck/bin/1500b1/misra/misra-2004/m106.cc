/*
 *	Rule 106:	Required
 *			
 *	The address of an object with automatic storage shall not be
 *	assigned to an object which may persist after the object has ceased
 *	to exist.
*/ 

#include "misra.cch"

if ( idn_local ) {

	strncpy( curLocStr, idn_name(), MAXIDLEN );
}




if ( op_address ) {

	if ( CMPPREVTOK(curLocStr) ) {

	  WARN_MR( 106, "17.6 Address of object with automatic storage shall not be referenced.");
	}

}