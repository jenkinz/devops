// Misra C Enforcement Testing
//
// Rule 58: Required
// The break statement shall not be used ( except to terminate the
// cases of a switch statement ) .
///

#include "misra.h"

SI_32
rule58 ( void ) 
{
    
SI_32 c = 3;
SI_32 i = 3;
    
    switch ( i ) 
    {
        
        case 1:
        case 2:
        case 3:
        break; 
        
        default:
        break; 
        
    }
    
    for ( i = 0; i < 10; ++i ) 
    {
        
        break; // RULE 58 
        
    }
    
    while (  ( i++ ) < 100 ) 
    {
        
        break; // RULE 58 
        
    }
    
    do
    {
        
        break; // RULE 58 
        
    }
    while (  ( i++ ) < 200 ) ;
    
    return c;
    
}
