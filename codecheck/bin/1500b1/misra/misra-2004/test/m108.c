// Misra C Enforcement Testing
//
// Rule 108: Required
// In the specification of a structure or union type, all members of
// the structure or union shall be fully specified.
///

#include "misra.h"

static void
func108 ( void ) 
{

struct s v;
struct s {
        int mem;
	foo;	// rule 108 vacous member, e.g. no explicit type 18.2
    };

}
