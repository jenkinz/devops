/*  Misra C Enforcement Testing */

/*  Rule 78: Required */
/*  The number of parameters passed to a function shall match the */
/*  function prototype. */


#include "misra.h"

void func78a ( signed char c, short s, int i, long l, float f, double d ) ;

void func78c ( void ) 
{
    
signed char pc = '\0';
signed short ss = 0;
signed int si = 0;
signed long sl = 0l;
float ff = 0.f;
double dd = 0.fL;
    
    func78a ( pc, /*  RULE 78 - too few  */
    ss, si, sl, ff ) ;
    
    func78a ( pc, 
    ss, si, sl, ff, dd ) ;
    
    func78a ( pc, /*  RULE 78 - too many  */
    ss, si, sl, ff, dd, dd ) ;
    
}
