/*  Misra C Enforcement Testing */

/*  Rule 30: Required */
/*  All automatic variables shall have been assigned a value before */
/*  being used. */

/*  This is not automatically detectable statically in all cases but */
/*  the following should exercise a tool. */


#include "misra.h"

/*  The use of rule24 (  ) is deliberate below. There is another one */
/*  elsewhere. The checking tool should find this if it sees the */
/*  whole of the compliance suite at once. The other one is defined as */
/*  type SI_32. */

SI_32
rule28 ( void ) 
{
    
SI_32 x;
SI_32 d;
SI_32 e;
SI_32 a = 3;
    
    if ( a == 3 ) 
    {
        
        d = x; /*  RULE 30  */
        
    }
    
    for ( ; e < 23; ++e ) /*  RULE 30  */
    {
        
        a += 2;
        
    }
    
    return 1;
    
}
