/*  Misra C Enforcement Testing */

/*  Rule 16: Required */
/*  Underlying floating point bit representations should not be used. */

/*  Not automatically enforceable. */


union utag {

float f1;   /*  define a float */

float fbit ;   /*  define a float bit */

unsigned long i1 : 1 ;

} u ;

m16() {

	unsigned dummy;

	u.f1 = 1.0;

	dummy = u.i1 >> 31 ;	/*  don't grab bits from a float */

}
