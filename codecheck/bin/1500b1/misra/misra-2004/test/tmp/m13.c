/*  Misra C Enforcement Testing */

/*  Rule 13: Advisory */
/*  The basic types char, short, int, long, float and double should not */
/*  be used. */


#include "misra.h"

SI_32
rule13 ( void ) 
{
    
signed char c; /*  RULE 13  */
short s; /*  RULE 13  */
int i; /*  RULE 13  */
long l; /*  RULE 13  */
float f; /*  RULE 13  */
double d; /*  RULE 13  */
    
    return 1;
    
}
