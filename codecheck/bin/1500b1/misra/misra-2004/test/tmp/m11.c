/*  Misra C Enforcement Testing */

/* Rule 11: Required */
/* No reliance on more than 31 character significance. */


#include "misra.h"

SI_32
int
rule11 ( void ) 
{
    
int abcdefghijklmnopqrstuvwxyz01234A; /*  RULE 11  */
int abcdefghijklmnopqrstuvwxyz01234B; /*  RULE 11  */
    
int bcdefghijklmnopqrstuvwxyz01234A; 
int bcdefghijklmnopqrstuvwxyz01234B; 
    
    return 1;
    
}
