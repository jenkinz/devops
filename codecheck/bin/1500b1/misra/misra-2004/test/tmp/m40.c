/*  Misra C Enforcement Testing */

/*  Rule 40: Advisory */
/*  The sizeof operator should not be used on expressions that contain */
/*  side effects. */


#include "misra.h"

SI_32
rule40 ( void ) 
{
    
SI_32 a = 3;
SI_32 c = 3;
volatile SI_32 v;
    
    if ( sizeof ( a++ ) == 4 ) /*  RULE 40  */
    {
        
        c = 1;
        
    }
    
    if ( sizeof ( v ) == 4 ) /*  RULE 40  */
    {
        
        c = 1;
        
    }
    
    if ( sizeof ( f (  ) ) == 4 ) /*  ????  */
    {
        
        c = 1;
        
    }
    
    return c;
    
}
