/*  Misra C Enforcement Testing */

/*  Rule 109: Required */
/*  Overlapping variable storage shall not be used. */


#include "misra.h"

union U /*  RULE 109 18.2 */
{
    
UI_32 u;
SI_32 l;

} un;

static void
func109a ( SI_64 l ) 
{
    
    
 union U su;/*  rule 109 unrelated usage member 18.3 */


}
