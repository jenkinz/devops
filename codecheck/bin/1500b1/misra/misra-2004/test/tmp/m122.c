/*  Misra C Enforcement Testing */

/*  Rule 122: Required */
/*  The setjmp macro and the longjmp function shall not be used. */


#include <setjmp.h>

#include "misra.h"

static void
func122 ( void ) 
{
    
jmp_buf jbuf;
int val = 1;
    
    if ( setjmp ( jbuf ) == 0 ) /*  RULE 122  */
    {
        
        
    }
    else
    {
        
        
    }
    
    longjmp ( jbuf, val ) ; /*  RULE 122  */
    
}
