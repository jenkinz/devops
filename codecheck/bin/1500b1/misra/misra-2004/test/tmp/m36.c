/*  Misra C Enforcement Testing */

/*  Rule 36: Advisory */
/*  Logical operators should not be confused with bitwise operators. */

/*  Not automatically enforceable. Requires manual inspection techniques. */


main() {

 int i;

	if ( 1 & i ) {	/*  rule 36 */

		i++;
	}
}
