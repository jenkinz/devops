/*  Misra C Enforcement Testing */

/*  Rule 124: Required */
/* output header file <stdio.h> must not be used. */

/*  ( The standards says library, but this is a header file containing */
/* o functions amongst other things. )  */


#include <stdio.h> /*  RULE 124  */

#include "misra.h"

static int dummy;
