/*  Misra C Enforcement Testing */

/*  Rule 82: Advisory */
/*  A function should have a single point of exit. */


#include "misra.h"

static void func82a ( void ) 
{
    
SC_8 pc = '\0';
    
    switch ( pc ) 
    {
        
        case 0: break;
        case 1: break;
        case 2: break;
        case 3: return;
        case 4: break;
        case 5: break;
        case 6: break;
        default: break;
        
    }
    
    return; /*  RULE 82  */
    
}

static void func82b ( void ) 
{
    
SC_8 pc = '\0';
    
    switch ( pc ) 
    {
        
        case 0: break;
        case 1: break;
        case 2: break;
        case 3: break;
        case 4: break;
        case 5: break;
        case 6: break;
        default: break;
        
    }
    
    return; 
    
}

static void func82c ( void ) 
{
    
SC_8 pc = '\0';
    
    if ( pc == '\0' ) 
    {
        
        return;
        
    }
    
} /*  RULE 82  */
