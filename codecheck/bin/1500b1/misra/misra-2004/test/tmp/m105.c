/*  Misra C Enforcement Testing */

/*  Rule 105: Required */
/*  All the functions pointed by a single pointer function shall be */
/*  identical in the number and type of parameters and the return type. */


static
int func105a ( void ) 
{
    
    return 1;
    
}

static
long func105b ( void ) 
{
    
    return 1;
    
}

static
int func105c ( int a ) 
{
    
    return a;
    
}

static int ( *fp )  ( void ) ;

static void
func105 ( void ) 
{
    
int k = 1;
    
    if ( k == 1 ) 
    {
        
        fp = func105a; 
        
    }
    else if ( k == 2 ) 
    {
        
        fp = func105b; /*  RULE 105  */
        
    }
    else
    {
        
        fp = func105c; /*  RULE 105  */
        
    }
    
}
