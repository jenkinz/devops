/*  Misra C Enforcement Testing */

/*  Rule 34: Required */
/*  The operands of a logical && or || shall be primary expressions. */


#include "misra.h"

SI_32
rule34 ( void ) 
{
    
SI_32 a = 3;
SI_32 b = 3;
SI_32 c = 3;
    
    if ( ( a == 3 ) && ( b == 3 ) ) 
    {
        
        c = 1;
        
    }
    
    if ( ( a == 3 ) || b ) 
    {
        
        c = 1;
        
    }
    
    if ( ( a == 3 ) || b == 3 ) /*  RULE 34  */
    {
        
        c = 1;
        
    }
    
    return c;
    
}
