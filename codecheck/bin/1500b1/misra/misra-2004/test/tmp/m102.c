/*  Misra C Enforcement Testing */

/*  Rule 102: Advisory */
/*  No more than 2 levels of pointer indirection should be used. */


void
func102 ( void ) 
{
    
int i1[2] = {
        0,1 
    };
int i2[2][2] = {
        {
            0,1
        }, {
            2,3
        } 
    };
int i3[2][2][1] = {
        {
            0,1
        }, {
            2,3
        } 
    };
int *pi1 = &i1[0]; 
int **pi2 = &pi1; 
int ***pi3 = &pi2; /*  RULE 102  */
    
int k;
    
    k = ( *pi1 ) ; 
    k = ( * ( *pi2 )  ) ; 
    k = ( * ( * ( *pi3 )  )  ) ; /*  RULE 102  */
    
}
