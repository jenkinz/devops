/*  Misra C Enforcement Testing */

/*  Rule 72: Required */
/*  For each function parameter the type given in the declaration and */
/*  definition shall be identical, and the return types shall also be */
/*  identical. */


#include "misra.h"

static SI_32 func72a ( SI_32 ) ;
static UI_32 func72b ( SI_32 ) ;
static SI_32 func72c ( UI_32 ) ;

static SI_32
func72a ( SI_32 a ) 
{
    
    return a;
    
}

static SI_32
func72b ( SI_32 a ) /*  RULE 72  */
{
    
    return a;
    
}

static SI_32
func72c ( SI_32 a ) /*  RULE 72  */
{
    
    return a;
    
}
