/*  Misra C Enforcement Testing */

/*  Rule 79: Required */
/*  The values returned by void functions shall not be used. */


#include "misra.h"

void func79a ( SC_8 c ) ;

void func79c ( void ) 
{
    
SC_8 pc = '\0';
SI_32 i;
    
    i = 3*func79a ( pc ) ; /*  RULE 79  */
    i = func79a ( pc ) ; /*  RULE 79  */
    i = ( void ) func79a ( pc ) ; /*  RULE 79  */
    i = func78c (  ) ; /*  RULE 79 - global  */
    
    func79a ( pc ) ; 
    func78c (  ) ; 
    
}
