/*  Misra C Enforcement Testing */

/*  Rule 25: Required */
/*  An identifier with external linkage shall have exactly one */
/*  external definition. */


#include "misra.h"


/*  The use of rule24 (  ) is deliberate below. There is another one */
/*  elsewhere. The checking tool should find this if it sees the */
/*  whole of the compliance suite at once. */

SI_32
rule24 ( void ) /*  RULE 25  */
{
    
    return 1;
    
}
