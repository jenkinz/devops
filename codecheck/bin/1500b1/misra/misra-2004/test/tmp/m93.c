/*  Misra C Enforcement Testing */

/*  Rule 93: Advisory */
/*  A function should be used in preference to a function-like macro */


#define SQR ( x ) (  ( x ) * ( x )  ) /*  RULE 93  */

void
func93a ( void ) 
{
    int i;
	
	i = SQR( 2 );
    
}
