/*  Misra C Enforcement Testing */

/*  Rule 117: Required */
/*  The validity of values passed to library functions shall be */
/*  checked. */

/*  Not automatically enforceable by a simple rule although there are */
/*  some examples which are worth checking. */


#include <string.h>

#include "misra.h"

static void
func117 ( void ) 
{
    
SC_8 c[10];
    
    strcpy ( c, NULL ) ; /*  RULE 117  */
    memcpy ( c, NULL, 0 ) ; /*  RULE 117  */
   
}
