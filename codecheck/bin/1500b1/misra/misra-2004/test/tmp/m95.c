/*  Misra C Enforcement Testing */

/*  Rule 95: Required */
/*  Arguments to a function-like macro shall not contain tokens that */
/*  look like pre-processing directives. */


#define TIMES ( x,defined ) (  ( x ) * ( defined )  ) 

void
func95a ( void ) 
{
    
int j = TIMES ( 1,3 ) ; /*  RULE 95  */
    
}
