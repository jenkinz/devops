/*  Misra C Enforcement Testing */

/*  Rule 68: Required */
/*  Functions shall always be declared at file scope. */


#include "misra.h"

static void func68 ( void ) ; 

SI_32
rule68 ( void ) 
{
    
SI_32 func68a ( void ) ; /*  RULE 68  */
SI_32 c = 1;
    
    return c;
    
}
