/*  Misra C Enforcement Testing */

/*  Rule 60: Advisory */
/*  All if, else if constructs should contain a final else clause. */


#include "misra.h"

SI_32
rule60 ( void ) 
{
    
SI_32 c = 3;
SI_32 i = 3;
    
    if ( i == 3 ) /*  RULE 60  */
    {
        
        c = 3;
        
    }
    else if ( i == 3 ) 
    {
        
        c = 2;
        
    }
    
    if ( i == 3 ) 
    {
        
        c = 3;
        
    }
    else if ( i == 3 ) 
    {
        
        c = 2;
        
    }
    else
    {
        
        c = 1;
        
    }
    
    if ( i == 3 ) 
    {
        
        c = 3;
        
    }
    else
    {
        
        c = 1;
        
    }
    
    if ( i == 3 ) 
    {
        
        c = 3;
        
    }
    
    return c;
    
}
