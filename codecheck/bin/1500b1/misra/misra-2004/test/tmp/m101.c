/*  Misra C Enforcement Testing */

/*  Rule 101: Advisory */
/*  Pointer arithmetic should not be used. */


void
func101 ( void ) 
{
    
int m[10] = {
        0,1,2,3,4,5,6,7,8,9 
    };
int i = 1;
int *pi = &i;
int k;
    
    k = *pi; 
    k = * ( ++pi ) ; /*  RULE 101  */
    
    pi =&m[4];
    k = * ( pi+4 ) ; /*  RULE 101  */
    
}
