/*  Misra C Enforcement Testing */

/*  Rule 104: Required */
/*  Non-constant pointers to functions shall not be used. */


static
int func104a ( void ) 
{
    
    return 1;
    
}

static
int func104b ( void ) 
{
    
    return 1;
    
}

static int ( *fp )  ( void ) ;

void
func104 ( void ) 
{
    
int k = 1;
    
    if ( k == 1 ) 
    {
        
        fp = func104a; 
        
    }
    else
    {
        
        fp = ( int ( * )  (  )  ) func104b; /*  RULE 104  */
        
    }
    
}
