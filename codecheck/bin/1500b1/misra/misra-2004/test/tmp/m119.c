/*  Misra C Enforcement Testing */

/*  Rule 119: Required */
/*  The error indicator errno shall not be used. */


#include <errno.h> /*  RULE 119  */
#include <math.h>

#include "misra.h"

static void
func119 ( void ) 
{
    
double pd;
    
    errno = 0; /*  RULE 119  */
    
    pd = cos ( 1.0f ) ;
    
    if ( errno != 0 ) 
    {
        
        
    }
    
}
