// Misra C Enforcement Testing
//
// Rule 67: Advisory
// Numeric variables being used within a for loop for iteration
// counting should not be modified in the body of the loop.
//
// This is in general difficult to detect automatically and statically.
// The following gives a simple case.
///

#include "misra.h"

static void func67 ( SI_32 ) ;

static void
func67 ( SI_32 x ) 
{
    
    x = 3;
    
}

SI_32
rule67 ( void ) 
{
    
SI_32 c = 3;
    
SI_32 i = 3;
SI_32 *pi = &i;
    
    for ( i = 0; i < 10; ++i ) 
    {
        
        i = 5; // RULE 67 
        
    }
    
    for ( i = 0; i < 10; ++i ) 
    {
        
        ( *pi ) = 5; // RULE 67 
        
    }
    
    for ( i = 0; i < 10; ++i ) 
    {
        
        func67 ( i ) ; 
        // Arg passed by value 
        
    }
    
    return c;
    
}
