// Misra C Enforcement Testing
//
// Rule 59: Required
// The statements forming the body of an if, else if, else, while, do
// ... while or for statement shall always be enclosed in braces.
///

#include "misra.h"

SI_32
rule59 ( void ) 
{
    
SI_32 c = 3;
SI_32 i = 3;
    
    if ( i == 3 ) 
    c = 4; // RULE 59 
    else
    c = 5; // RULE 59 
    
    if ( i == 3 ) 
    {
        
        c = 3; 
        
    }
    else if ( i == 3 ) 
    c = 2; // RULE 59 
    
    while ( ( ++i ) < 10 ) 
    c = 2; // RULE 59 
    
    while ( ( ++i ) < 10 ) 
    {
        
        c = 2; 
        
    }
    
    do
    c = 2; // RULE 59 
    while ( ( ++i ) < 10 ) ;
    
    do
    {
        
        c = 2; 
        
    }
    while ( ( ++i ) < 10 ) ;
    
    for ( i = 0; i < 10; ++i ) 
    c = 2; // RULE 59 
    
    for ( i = 0; i < 10; ++i ) 
    {
        
        c = 2; 
        
    }
    
    return c;
    
}
