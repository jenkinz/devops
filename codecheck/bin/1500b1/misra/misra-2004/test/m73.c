// Misra C Enforcement Testing
//
// Rule 73: Required
// Identifiers shall be given either for all of the parameters in a
// function prototype declaration or for none.
///

#include "misra.h"

static SI_32 func73a ( SI_32, UI_32 ) ; 
static SI_32 func73b ( SI_32 a, UI_32 b ) ; 
static SI_32 func73c ( SI_32 a, UI_32 ) ; // RULE 73 
static SI_32 func73d ( SI_32, UI_32 b ) ; // RULE 73 
