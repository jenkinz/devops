// Misra C Enforcement Testing
//
// Rule 113: Required
// All the members of a structure ( or union ) shall be named and shall
// only be accessed via their name.
///

#include "misra.h"

static struct s
{
    
signed int p_a : 4; 
unsigned int p_b : 1; 
    signed int : 8; // RULE 113 
unsigned int p_c : 4; 

} st;

static void
func113 ( void ) 
{
    
    st.p_a = 0x1;
    
}
