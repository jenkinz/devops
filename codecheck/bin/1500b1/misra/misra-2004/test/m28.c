// Misra C Enforcement Testing
//
// Rule 28: Required
// The register storage class should not be used.
///

#include "misra.h"
//
// The use of rule24 (  ) is deliberate below. There is another one
// elsewhere. The checking tool should find this if it sees the
// whole of the compliance suite at once. The other one is defined as
// type SI_32.
///
SI_32
rule28 ( void ) 
{
    
register SI_32 x; // RULE 28 
    
    return 1;
    
}
