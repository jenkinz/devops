// Misra C Enforcement Testing
//
// Rule 52: Required
// There shall be no unreachable code.
///

#include "misra.h"

SI_32
rule52 ( void ) 
{
    
SI_32 c = 3;
SI_32 z = 3;
    
    switch ( c ) 
    {
        
        z = 2; // RULE 52 
        case 1:
        case 2:
        return;
        z = 3; // RULE 52 
        
        default:
        break;
        
    }
    
    goto label;
    z = 4; // RULE 52 
    
    label:
    
    while ( 1 == 0 ) // RULE 52 
    {
        
        z = 4;
        
    }
    
    if ( 1 == 0 ) // RULE 52 
    {
        
        z = 4;
        
    }
    
    goto label2;
    
    {
        
int zz = 4; // RULE 52 
        
        label2:
        
        z += zz;
        
    }
    
    return c;
    
    return c; // RULE 52 
    
}
