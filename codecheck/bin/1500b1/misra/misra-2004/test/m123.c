// Misra C Enforcement Testing
//
// Rule 123: Required
// The signal handling facilities of <signal.h> shall not be used
///

#include <signal.h>
#include <stdlib.h>

#include "misra.h"

volatile sig_atomic_t v;

static void func123a ( int ) ;

static void
func123a ( int signum ) 
{
    
    v = 0; 
    
    if ( signal ( SIGINT, func123a ) == SIG_ERR ) // RULE 123 
    {
        
        
    }
    
}

static void
func123 ( void ) 
{
    
    if ( signal ( SIGINT, func123a ) == SIG_ERR ) // RULE 123 
    {
        
        
    }
    
    while ( ++v < 100 ) 
    {
        
        
    }
    
    raise ( SIGINT ) ; // RULE 123 
    
}
