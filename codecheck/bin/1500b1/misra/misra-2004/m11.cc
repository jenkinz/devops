/*
 *	Rule 11:	Required
 *			
 *	No reliance on more than 31 character significance.
*/ 

#include "misra.cch"


	if ( dcl_global | dcl_local ) {

		if ( dcl_ident_length > 31 ) {
			WARN_MR( 11, "5.1 No reliance on more than 31 character significance." );
		}
	}