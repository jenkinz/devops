/*
 *	Rule 121:	Required
 *			
 *	<locale.h> and the setlocale function shall not be used.
*/ 


if ( idn_function ) {
	if ( strcmp(idn_name(), "setlocale" ) == 0 ) {
		WARN_MR( 121, "<locale.h> and the setlocale function shall not be used." );
	}
}