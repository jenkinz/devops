
/*
 *	Rule 23:	Advisory
 *			
 *	All declarations at file scope should be static where possible.
*/ 


#include "misra.cch"

if ( dcl_function ) {

//warn( 23, "DF ds%d", dcl_static );
	if ( dcl_static == 0 && dcl_definition == 0 ) {

		WARN_MA( 23, "8.10 All declarations at file scope should be static where possible.");
	}

}