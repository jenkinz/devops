/*
 *	Rule 1:		Required
 *			
 *	ISO 9899 conformance.
 *
 *	This requires a toolset to be run against the FIPS 160 compiler
 *	validation suite or equivalent.
*/ 

#include "misra.cch"

if ( lex_cpp_comment ) {
	WARN_MR( 1, "2.2 Source code shall only use C style comments, note c++ style" );
}