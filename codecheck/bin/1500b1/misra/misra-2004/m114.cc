/*
 *	Rule 114:	Required
 *				
 *	Reserved words and standard library function names shall not be
 *	redefined or undefined.
 *
 *	Note the interaction with rule 92 on #undef.
 *	We do not look for reserved words here as most occurrences lead to
 *	syntax error.
*/ 

#include "misra.cch"



if ( pp_macro_conflict ) {

	WARN_MR( 114, "20.1 Reserved words and standard library function names shall not be used { MACRO }" )

}