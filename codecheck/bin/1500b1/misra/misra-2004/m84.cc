/*
 *	Rule 84:	Required
 *			
 *	For functions with void return type, return statements shall not
 *	have an expression.
*/ 


#include "misra.cch"

if ( op_return ) {

	if ( curFbase == VOID_TYPE && op_base(1) != 0 ) {
	
		WARN_MR( 84, "Functions with void return shall not return expression." );
	}
}