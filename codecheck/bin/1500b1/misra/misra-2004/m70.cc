/*
 *	Rule 70:	Required
 *			
 *	Functions shall not call themselves either directly or indirectly.
 *
 *	It is trivial to detect direct recursion, but it can in some cases
 *	be impossible to detect indirect recursion statically.
*/ 

#include "misra.cch"

if ( op_open_funargs && op_declarator == 0 )
{
        if ( strcmp( fcn_name(), prev_token() ) == 0 ) {

                WARN_MR( 70, "16.2 Functions shall not call themselves" );
        }
}