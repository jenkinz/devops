/*
 *	Rule 74:	Required
 *			
 *	If identifiers are given for any of the parameters, then the
 *	identifiers used in the declaration and definition shall be
 *	identical.
*/ 


#include "misra.cch"

if ( dcl_no_prototype ) {

	WARN_MR( 74, "16.4 identifiers given for any of the parameters decl and/or defn must be same" );
}