/*
 *	Rule 17:	Required
 *			
 *	typedef names shall not be reused.
*/ 

#include "misra.cch"


if ( dcl_typedef_dup ) {
	//warn( 17, "DTD %s [%s] fr%d db%d", dcl_name(), dcl_base_name(), find_root( dcl_name() ), dcl_base );
	WARN_MR( 17, "5.3 typedef names shall not be reused. { typedef duplicate }" );
}

if ( dcl_typedef ) {

	//warn( 17, "DT %s [%s] ct%d db%d dbr%d", dcl_name(), dcl_base_name(), curtype, dcl_base, dcl_base_root );
	if (  curtype > 0  && curtype != dcl_base ) {
		WARN_MR( 17, "5.3 typedef names shall not be reused. { reuse }" );
	}
}

if ( dcl_conflict ) {

//warn( 17, "DC %s [%s] fr%d db%d", dcl_name(), dcl_base_name(), find_root( dcl_name() ), dcl_base );
	WARN_MR( 17, "5.3 typedef names shall not be reused. { conflict }" );
}

