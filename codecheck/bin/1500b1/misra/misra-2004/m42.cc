
/*
 *	Rule 42:	Required
 *			
 *	The comma operator shall not be used, except in the control
 *	expression of a for loop.
*/ 

#include "misra.cch"

if ( op_comma ) {
	
	if ( inforloop == 0 ) {
		WARN_MR( 42, "12.10 Comma operator shall not be used, except in the for-loop" );
	}
}