/*
 *	Rule 12:	Advisory
 *			
 *	Identifiers in different name spaces shall have different spelling.
*/ 

#include "misra.cch"

if ( dcl_variable ) {
//	warn( 12, "DV db%d ct%d dbr%d cs%d", dcl_base, curtype, dcl_base_root, curline );

	if (  curtype > 0  && curline != lin_number ) {
		WARN_MA( 12, "5.6 No Identifier should be re-used. { Variable }" );
	}
}

if ( dcl_member ) {
	if ( CMPSTR(dcl_name(), tag_name()) ) {
			WARN_MA( 12, "5.6 No Identifier should be re-used. { Member Name same as Tag Name }" );
	}
}

if ( op_goto ) {
	if ( curtype && curline != lin_number ) {
		
	}	WARN_MA( 12, "5.6 No Identifier should be re-used. { goto }" );
}

if ( dcl_conflict ) {

	WARN_MA( 12, "5.7 Identifiers in different name spaces shall have different spelling. { Conflict }" );
}

if ( tag_hidden ) {

	WARN_MA( 12, "5.7 Identifiers in different name spaces shall have different spelling. { TAG }" );

}


if ( dcl_hidden ) {

	WARN_MA( 12, "5.7 Identifiers in different name spaces shall have different spelling. { Hidden  }" );
}

if ( op_colon_1  ) {
//	warn( 12, "label db%d ct%d dbr%d cs%d", dcl_base, curtype, dcl_base_root, curline );
	if ( curline && curline != lin_number ) {
		WARN_MA( 12, "5.7 Identifiers in different name spaces shall have different spelling. { Label  }" );
	}
}



