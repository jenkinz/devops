/*
 *	Rule 10:	Advisory
 *			
 *	Code shall not be commented out.
 *
 *	Not automatically enforceable.  Requires manual inspection techniques.
*/ 

#include "misra.cch"

if ( lin_end ) {
	if ( lin_is_comment ) {

		if ( strpbrk(line(), ";+_-=|<>(){}" ) ) {
			WARN_MA( 10, "2.4 Code shall not be commented out." );
		}
	}
}