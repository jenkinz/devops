/*
 *	Rule 107:	Required
 *			
 *	The NULL pointer shall not be dereferenced.
 *
 *	This is not statically detectable in all cases.
*/ 

char derefnm[MAXIDLEN];
int deref;


if ( dcl_initializer ) {


	strcpy( derefnm, dcl_name() );
	if ( dcl_level(0) == 3 ) deref = -1;

}


if ( lex_initializer ) {


	if ( deref && atoi(prev_token()) == 0 ) {
		
		deref = lin_number;
	}
	else deref = 0;
}

if ( idn_variable ) {

	if ( deref > 0 ) {

		 WARN_MR( 107, "The null pointer shall not be de-referenced" );

		deref = 0;
	}
}

if ( mod_end ) deref = 0;