/*
 *	Rule 64:	Required
 *			
 *	Every switch statement shall have at least one case.
*/ 

#include "misra.cch"

if (stm_switch_cases) {

	if ( stm_switch_cases <= 1 ) {

		WARN_MR( 64, "15.5 Every switch statement shall have at least one case.");
	}
}