/*
 *	Rule 76:	Required
 *			
 *	Functions with no parameters shall be declared with parameter type
 *	void.
*/ 

#include "misra.cch"

if ( dcl_oldstyle ) {

	WARN_MR(76, "16.5 Functions with no parameters shall be declared with parameter type void.");

}