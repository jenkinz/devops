/*
 *	Rule 33:	Required
 *			
 *	The right hand side of a && or || operator shall not contain
 *	side-effects.
 *
 *	This is not automatically detectable statically in all cases but
 *	the following should exercise a tool.
*/ 

int side_effect_logical;

if ( op_postfix ) {

	side_effect_logical++;

}

if ( op_log_or || op_log_and ) {

	if ( side_effect_logical ) {

		WARN_MR(33, "12.4 Right hand side of a && or || operator shall not contain side-effect");
	}

	side_effect_logical = 0;
}