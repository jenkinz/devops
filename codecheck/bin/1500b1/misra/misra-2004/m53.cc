/*
 *	Rule 53:	Required
 *			
 *	All non-null statements shall have a side-effect.
*/ 

#include "misra.cch"

if ( lin_end ) {
//warn( 53, "LE lhc%d lie%d ldc%d", lin_has_code, lin_is_exec, lin_dcl_count );
	if ( lin_has_code && lin_is_exec == 0 && lin_dcl_count == 0 ) {
			if ( CMPPREVTOK(";") ) {
				WARN_MR( 53, "14.2 All non-null statements shall have a side-effect." );
			}
	}
}

if ( op_equal ) {

	if ( chkexp == 0 ) {
		WARN_MR( 53, "14.2 All non-null statements shall have a side-effect." );
	}
}
