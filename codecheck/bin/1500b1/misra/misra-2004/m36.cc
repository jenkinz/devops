/*
 *	Rule 36:	Advisory
 *			
 *	Logical operators should not be confused with bitwise operators.
 *
 *	Not automatically enforceable.  Requires manual inspection techniques.
*/ 

#include "misra.cch"

if ( op_bit_or || op_bit_and || op_bit_not ) {

	if ( chkexp ) {

		WARN_MA( 36, "12.6 Logical operators should not be confused with bitwise operators." );
	}
}