/*
 *	Rule 109:	Required
 *			
 *	Overlapping variable storage shall not be used.
*/ 


#include "misra.cch"

if ( tag_begin ) {

	if ( tag_kind == UNION_TAG ) {

		WARN_MR(109, "18.2 Overlapping variable storage shall not be used.");
	}
}

if ( dcl_local ) {
//warn( 109, "DL b%d r%d", dcl_base, dcl_base_root );
	if ( dcl_base == UNION_TYPE ) {
			WARN_MR(109, "18.3 An area of memory shall not be used.");
	}
}