/*
 *	Rule 85:	Advisory
 *			
 *	Functions called with no parameters should have empty parentheses.
*/ 


#include "misra.cch"

if ( idn_variable  ) {	// this looks like a variable, but it is a function

	if ( idn_level(0) == FUNCTION && strcmp( token(), "(" ) ) {

		WARN_MA( 85, "16.9 Functions with no parameters should have empty parentheses.");
	}
}