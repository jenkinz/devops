/*
 *	Rule 32:	Required
 *			
 *	In an enumerator list, the '=' construct shall not be used to
 *	explicitly initialise members other than the first, unless all
 *	items are explicitly initialised.
*/ 


#include "misra.cch"

int enum_init;		// count number of initialized enums
int enum_count;		// count number of contstant in enum
int enum_first;

if ( dcl_enum ) {
//warn(32,"DE %s", token() );
		enum_count++;		// count number seen
		if ( CMPNEXTTOK("=") ) {
			enum_init++;		// increment each	
			if ( enum_first == 0 ) {
				enum_first = enum_count;
			}
		}
}

if ( tag_begin ) {
	if ( tag_kind == ENUM_TAG ) {
		enum_init = 0;
		enum_count = 0;
		enum_first = 0;
	}
}

if ( tag_end ) {

//warn( 999, "TE enf%d eni=%d enc=%d",  enum_first, enum_init, enum_count );
	if ( tag_kind == ENUM_TAG ) {

		if ( enum_init ) {
			if ( enum_count != enum_init  || (enum_first != 1)  ) {
				WARN_MR( 32, "9.3 Enum initialzation must be on first or ALL" );
			}
		}
	}
}