/*
 *	Rule 31:	Required
 *			
 *	Braces shall be used to indicate and match the structure in the
 *	non-zero initialisation of arrays and structures.
*/ 

if ( dcl_array_size ) {

        if ( dcl_array_size > 0 ) {
                WARN_MR( 31, "9.2 Braces shall be used to indicate initialization structure");
        }
}