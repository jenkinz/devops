/*
 *	Rule 110:	Required
 *				
 *	Unions shall not be used to access the sub-parts of larger data
 *	types.
 *
 *	This seems slightly odd as Rule 109 effectively precludes the use
 *	of unions at all.
*/ 

#include "misra.cch"


if ( tag_end ) {

	if ( tag_kind == UNION_TAG ) {

		WARN_MR( 110, "18.4 Unions shall not be used to access sub-parts of larger data.");
	}
}