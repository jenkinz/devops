/*
 *	Rule 60:	Advisory
 *			
 *	All if, else if constructs should contain a final else clause.
*/ 


#include "misra.cch"


int select_else; // signal whether last select statement had else

if ( stm_need_comp ) {

	if ( stm_if_else == 0 && stm_kind == ELSE && select_else == 0 ) {
	
		WARN_MA( 60, "14.10 All if, else if constructs should contain a final else clause.");
	}
}


if ( stm_is_select ) {

	select_else = stm_if_else;
}