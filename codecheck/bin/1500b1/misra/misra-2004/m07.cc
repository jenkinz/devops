/*
 *	Rule 7:	Required
 *			
 *	No trigraphs
*/ 

#include "misra.cch"

if ( lex_str_trigraph ) {

	WARN_MR( 7, "4.2 No Trigraphs" );
}