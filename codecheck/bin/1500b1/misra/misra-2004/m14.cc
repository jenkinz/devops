/*
 *	Rule 14:	Required
 *			
 *	char should only be used with the signed or unsigned keyword.
*/ 

if ( dcl_local || dcl_global || dcl_function ) {
	
	
	if ( dcl_base == CHAR_TYPE ) {

		if ( !(dcl_signed || dcl_unsigned) ) {

		WARN_MR( 14, "6.2 char should only be used with the signed or unsigned keyword." );

		}	
	}
}