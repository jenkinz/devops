/*
 *	Rule 71:	Required
 *			
 *	Functions shall always have prototype declarations and the
 *	prototype shall be visible at both the function definition and the
 *	call.
*/ 

#include "misra.cch"

if ( idn_no_prototype ) {

	WARN_MR( 71, "8.1 Functions shall always have prototype declarations. {CALL}" );
}

if ( dcl_no_prototype ) {

	WARN_MR( 71, "8.1 Functions shall always have prototype declarations. {DEFN}" );
}