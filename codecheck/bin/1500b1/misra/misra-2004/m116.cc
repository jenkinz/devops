/*
 *	Rule 116:	Required
 *			
 *	All libraries used in production code shall be written to comply
 *	with the provisions of this document, and shall have been subject
 *	to appropriate validation.
 *
 *	Not automatically enforceable by a simple rule.
*/ 

#include "misra.cch"

if ( dcl_variable || dcl_function ) {
//warn( 116, "DV-DF %s dc%d dd%d find%d ", dcl_name(), dcl_conflict, dcl_definition, find_root(dcl_name()) );
	if ( dcl_definition == 0 && find_root(dcl_name()) != dcl_base ) {
		WARN_MR( 116, "3.6 Reserved words and standard library function names shall not be used { DECL }" )
	}
}