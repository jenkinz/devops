/*
 *	Rule 69:	Required
 *			
 *	Functions with variable numbers of arguments shall not be used.
*/ 

#include "misra.cch"

if ( op_macro_call ) {
	
	if ( CMPTOK("va_start") ||  CMPTOK("va_arg")  ||  CMPTOK("va_end") ) {
	  
	  WARN_MR( 69, "16.1 Functions with variable numbers of arguments shall not be used.");	
	}
}

if ( dcl_3dots ) {

	WARN_MR( 69, "16.1 Functions with variable numbers of arguments shall not be used.");
}