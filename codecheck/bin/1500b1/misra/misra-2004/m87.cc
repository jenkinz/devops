/*
 *	Rule 87:	Required [ 2004 advise ]
 *			
 *	#include statements in a file shall only be preceded by other
 *	pre-processor directives or comments.
*/ 

// 19.1 ADV #include statements in a file shall only be preceded by other
// pre-processor directives or comments.
///

// 8.5 REQ No defn's in a header file

#include "misra.cch"

int varCount;

if ( mod_begin ) varCount = 0;

if ( dcl_variable ) {

	if ( lin_source ) varCount++;	// only count source file decls, not headers

}

if ( pp_include ) {


	if ( varCount > 0 ) {

	  WARN_MA( 87, "19.1 #include statements only be preceded by pre-processor directives.");
	}
}

if ( dcl_definition ) {
	if ( lin_source == 0 ) { // header file
		WARN_MR( 87, "8.5 There shall be no definitions in a header file" );
	}
}
