/*
 *	Rule 25:	Required
 *			
 *	An identifier  with external linkage shall have exactly one
 *	external definition.
*/ 


#include "misra.cch"


if ( dcl_global ) {
//warn( 17, "DG %s [%s] fr%d db%d dbr%d", dcl_name(), dcl_base_name(), find_root( dcl_name() ), dcl_base, dcl_base_root );

	if ( dcl_definition && curtype && strcmp(curname,file_name()) ) {

       WARN_MR( 25, "8.9 An identifier with external linkage shall have exactly one definition." );
	}
}