/*
 *	Rule 59:	Required
 *			
 *	The statements forming the body of an if, else if, else, while, do
 *	... while or for statement shall always be enclosed in braces.
*/ 


#include "misra.cch"


if ( idn_variable ) {

	if ( stm_container <= SWITCH && chkexp == 0 ) {

	  WARN_MR( 59, "14.8 Statements in body of if, else if, else, while, do must be in braces.");
	}
}