// Misra C Enforcement Testing
//
// Rule 48: Advisory
// Mixed precision arithmetic should use explicit casting to generate
// the desired result.
//
// Not automatically detectable statically in all cases. The
// following should give an indication.
///

#include "misra.h"

SI_32
rule48 ( void ) 
{
    
SI_32 c = 3;
    
UI_32 i = 1u;
UI_32 j = 3u;
    
FL_64 d0 = i / j; // RULE 48 
    
    return c;
    
}
