// Misra C Enforcement Testing
//
// Rule 61: Required
// Every non-empty case clause in a switch statement shall be
// terminated with a break statement.
///

#include "misra.h"

SI_32
rule61 ( void ) 
{
    
SI_32 c = 3;
SI_32 i = 3;
    
    switch ( i ) 
    {
        
        case 1: c = 2; // RULE 61 
        case 2: c = 3; break;
        case 3: 
        case 4: c = 4; break;
        default: break;
        
    }
    
    return c;
    
}
