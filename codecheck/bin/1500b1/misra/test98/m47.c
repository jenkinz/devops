// Misra C Enforcement Testing
//
// Rule 47: Required
// No dependence should be placed on C's operator precedence rules in
// expressions.
///

#include "misra.h"

SI_32
rule47 ( void ) 
{
    
SI_32 c = 3;
SI_32 i = 3;
SI_32 x = 3;
SI_32 y = 3;
SI_32 z = 3;
    
    z = 3 * x + y / i; // RULE 47 
    
    z = (  ( 3 * x ) + ( y / i )  ) ; 
    
    return c;
    
}
