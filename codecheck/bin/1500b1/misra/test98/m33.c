// Misra C Enforcement Testing
//
// Rule 33: Required
// The right hand side of a && or || operator shall not contain
// side-effects.
//
// This is not automatically detectable statically in all cases but
// the following should exercise a tool.
///

#include "misra.h"

extern SI_32 func33 ( void ) ;

SI_32
rule33 ( void ) 
{
    
SI_32 a = 3;
SI_32 b = 3;
SI_32 c = 3;
    
    if ( ( a == 3 ) && b++ ) // RULE 33 
    {
        
        c = 1;
        
    }
    
    if ( ( a == 3 ) || b++ ) // RULE 33 
    {
        
        c = 1;
        
    }
    
    if ( ( a == 3 ) || func33 (  ) ) // ???? 
    {
        
        c = 1;
        
    }
    
    return c;
    
}
