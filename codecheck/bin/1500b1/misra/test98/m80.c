// Misra C Enforcement Testing
//
// Rule 80: Required
// Void expressions shall not be passed as function parameters.
///

#include "misra.h"

void func80a ( SC_8 c ) ;

void func80c ( void ) 
{
    
SC_8 pc = '\0';
    
    func80a ( ( void ) pc ) ; // RULE 80 
    func80a ( pc ) ; 
    
}
