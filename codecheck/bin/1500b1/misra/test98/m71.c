// Misra C Enforcement Testing
//
// Rule 71: Required
// Functions shall always have prototype declarations and the
// prototype shall be visible at both the function definition and the
// call.
///

#include "misra.h"

SI_32 // RULE 71 
rule71 ( SI_32 a ) 
{
    
SI_32 c;
    
    switch ( a ) 
    {
        
        case 0:
        case 1:
        c = 1;
        break;
        
        default:
        if ( a < 0 ) 
        {
            
            c = ( -1 ) ;
            
        }
        else
        {
            
            c = a * func71 ( a-1 ) ; // RULE 71 
            
        }
        break;
        
    }
    
    return c;
    
}
