// Misra C Enforcement Testing
//
// Rule 110: Required
// Unions shall not be used to access the sub-parts of larger data
// types.
//
// This seems slightly odd as Rule 109 effectively precludes the use
// of unions at all.
///

#include "misra.h"

union u
{
    
FL_32 f;
SI_16 s;

} un; // RULE 110 

static void
func110 ( void ) 
{
    
FL_32 f = 1.1f;
SI_16 s;
    
    un.f = f;
    s = un.s;
    
}
