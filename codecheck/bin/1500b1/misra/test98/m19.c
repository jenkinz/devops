// Misra C Enforcement Testing
//
// Rule 19: Required
// Octal constants ( other than zero ) shall not be used.
///

#include "misra.h"

SI_32
rule19 ( void ) 
{
    
SI_32 code[5];
    
    code[0] = 109; 
    code[1] = 052; // RULE 19 
    
    return 1;
    
}
