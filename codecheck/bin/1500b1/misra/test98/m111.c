// Misra C Enforcement Testing
//
// Rule 111: Required
// Bit fields shall only be defined to be of type unsigned int or
// signed int.
///

#include "misra.h"

static struct s
{
    
int p_a : 4; // RULE 111 
signed int p_b : 4; 
unsigned int p_c : 4; 
unsigned int p_d : 4; 
long p_e : 4; // RULE 111 

} st;

static void
func111 ( void ) 
{
    
    st.p_a = 0x1;
    
}
