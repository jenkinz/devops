// Misra C Enforcement Testing
//
// Rule 121: Required
// <locale.h> and the setlocale function shall not be used.
///

#include <locale.h>

#include "misra.h"

static void
func121 ( void ) 
{
    
SC_8 *pc;
    
    pc = ( SC_8 * ) setlocale ( LC_ALL, "C" ) ; // RULE 121 
    
}
