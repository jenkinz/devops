// Misra C Enforcement Testing
//
// Rule 8: Required
// No multibyte characters or wide string literals
///

#include "misra.h"

SI_32
int
rule8 ( void ) 
{
    
SI_32 i = 'abcd'; // RULE 8 
    wchar_t *m = L"Fred"; // RULE 8 
    
    return 1;
    
}
