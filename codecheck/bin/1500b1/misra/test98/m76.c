// Misra C Enforcement Testing
//
// Rule 76: Required
// Functions with no parameters shall be declared with parameter type
// void.
///

#include "misra.h"

static SI_32 func76a ( void ) ; 
static SI_32 func76b (  ) ; // RULE 76 
