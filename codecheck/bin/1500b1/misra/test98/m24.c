// Misra C Enforcement Testing
//
// Rule 24: Required
// Identifiers shall not have internal and external linkage
// simultaneously in the same translation unit.
///

#include "misra.h"

static SI_32 x;
extern SI_32 x; // RULE 24 

static UC_8 y;

SI_32
rule24 ( void ) 
{
    
extern UC_8 y;
    {
        
extern UC_8 y; // RULE 24 
        
    }
    
    return 1;
    
}
