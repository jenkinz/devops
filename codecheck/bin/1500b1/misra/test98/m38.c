// Misra C Enforcement Testing
//
// Rule 38: Required
// The right hand operand of a shift operator shall lie between zero
// and one less than the width in bits of the left hand operand
// ( inclusive ) .
///

#include "misra.h"

SI_32
rule38 ( void ) 
{
    
UI_32 a = 3;
UI_32 b;
SI_32 c = 3;
    
    b = a >> ( -1 ) ; // RULE 38 
    b = a << ( -1 ) ; // RULE 38 
    
    b = a >> 32; // RULE 38 
    b = a << 32; // RULE 38 
    
    b = a >> 31; 
    b = a << 31; 
    
    return c;
    
}
