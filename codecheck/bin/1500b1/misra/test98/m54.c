// Misra C Enforcement Testing
//
// Rule 54: Required
// A null statement shall only occur on a line by itself, and shall
// not have any other text on the same line.
///

#include "misra.h"

SI_32
rule54 ( void ) 
{
    
SI_32 c = 3;
SI_32 i = 3;
SI_32 j = 3;
SI_32 k = 3;
    
    for ( i = 0; i < 10; ++i ) ; // RULE 54 
    
    if ( j == k ) ; // RULE 54 
    {
        
        c = 1;; // RULE 54 
        
    }
    
    return c;
    
}
