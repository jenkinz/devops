// Misra C Enforcement Testing
//
// Rule 106: Required
// The address of an object with automatic storage shall not be
// assigned to an object which may persist after the object has ceased
// to exist.
///

#include "misra.h"

int *pi;
int fk;

static SC_8 *
func106a ( void ) 
{
    
    //
    // Returns pointer to automatic array.
    ///
SC_8 text[100];
    return text; // RULE 106 
    
}

static SC_8
func106b ( void ) 
{
    
    //
    // Returns actual value.
    ///
SC_8 text = 'a';
    return text; 
    
}

static SC_8 *
func106c ( void ) 
{
    
    //
    // Returns pointer to automatic.
    ///
SC_8 text;
SC_8 *pt=&text;
    return pt; // RULE 106 
    
}

static SC_8 *
func106d ( void ) 
{
    
    //
    // Returns pointer to static.
    ///
static SC_8 text;
SC_8 *pt=&text;
    return pt; 
    
}

static void
func106e ( void ) 
{
    
int k = 1;
    
    pi = ( &k ) ; // RULE 106 
    
    pi = ( &fk ) ; 
    
}
