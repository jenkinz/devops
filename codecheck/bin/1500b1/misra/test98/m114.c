// Misra C Enforcement Testing
//
// Rule 114: Required
// Reserved words and standard library function names shall not be
// redefined or undefined.
//
// Note the interaction with rule 92 on #undef.
// We do not look for reserved words here as most occurrences lead to
// syntax error.
///

#include "misra.h"


#define defined // RULE 114			20.1
#define __LINE__ ( 0 ) // RULE 114 
#define __FILE__ ( 0 ) // RULE 114 
#define __DATE__ ( 0 ) // RULE 114 
#define __TIME__ ( 0 ) // RULE 114 
#define __STDC__ ( 0 ) // RULE 114 
#define errno ( 0 ) // RULE 114 
#define assert ( 0 ) // RULE 114 

#undef defined // RULE 114 
#undef __LINE__ // RULE 114 
#undef __FILE__ // RULE 114 
#undef __DATE__ // RULE 114 
#undef __TIME__ // RULE 114 
#undef __STDC__ // RULE 114 
#undef errno // RULE 114 
#undef assert // RULE 114 
