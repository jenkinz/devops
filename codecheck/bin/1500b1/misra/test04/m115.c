/*  Misra C Enforcement Testing */

/*  Rule 115: Required */
/*  Standard library function names shall not be reused. */


#include "misra.h"

#include <stdlib.h> /* here we force the std defn's so the later will conflict */
#include <stdio.h>
#include <math.h>


/*  Same order as K&R II, appendix B. */
/*  --------------------------------- */
/*  File operations */


	/* now define the std's and cause a re-definition alert, aka misra-c:2004 20.2 */

static void fopen ( void ) {	
    
} /*  RULE 115  */
static void freopen ( void ) {
    
} /*  RULE 115  */
static void fflush ( void ) {
    
} /*  RULE 115  */
static void fclose ( void ) {
    
} /*  RULE 115  */
static void remove ( void ) {
    
} /*  RULE 115  */
static void rename ( void ) {
    
} /*  RULE 115  */
static void tmpfile ( void ) {
    
} /*  RULE 115  */
static void tmpnam ( void ) {
    
} /*  RULE 115  */
static void setvbuf ( void ) {
    
} /*  RULE 115  */
static void setbuf ( void ) {
    
} /*  RULE 115  */

/*  Formatted output. */

static void fprintf ( void ) {
    
} /*  RULE 115  */
static void printf ( void ) {
    
} /*  RULE 115  */
static void sprintf ( void ) {
    
} /*  RULE 115  */
static void vprintf ( void ) {
    
} /*  RULE 115  */
static void vfprintf ( void ) {
    
} /*  RULE 115  */
static void vsprintf ( void ) {
    
} /*  RULE 115  */

/*  Formatted input. */

static void fscanf ( void ) {
    
} /*  RULE 115  */
static void scanf ( void ) {
    
} /*  RULE 115  */
static void sscanf ( void ) {
    
} /*  RULE 115  */

/*  Character Input and Output functions */

static void fgetc ( void ) {
    
} /*  RULE 115  */
static void fgets ( void ) {
    
} /*  RULE 115  */
static void fputc ( void ) {
    
} /*  RULE 115  */
static void fputs ( void ) {
    
} /*  RULE 115  */
static void getc ( void ) {
    
} /*  RULE 115  */
static void getchar ( void ) {
    
} /*  RULE 115  */
static void gets ( void ) {
    
} /*  RULE 115  */
static void putc ( void ) {
    
} /*  RULE 115  */
static void putchar ( void ) {
    
} /*  RULE 115  */
static void puts ( void ) {
    
} /*  RULE 115  */
static void ungetc ( void ) {
    
} /*  RULE 115  */

/*  Direct Input and Output functions */

static void fread ( void ) {
    
} /*  RULE 115  */
static void fwrite ( void ) {
    
} /*  RULE 115  */

/*  File positioning functions */

static void fseek ( void ) {
    
} /*  RULE 115  */
static void ftell ( void ) {
    
} /*  RULE 115  */
static void rewind ( void ) {
    
} /*  RULE 115  */
static void fgetpos ( void ) {
    
} /*  RULE 115  */
static void fsetpos ( void ) {
    
} /*  RULE 115  */

/*  Error functions */

static void clearerr ( void ) {
    
} /*  RULE 115  */
static void feof ( void ) {
    
} /*  RULE 115  */
static void ferror ( void ) {
    
} /*  RULE 115  */
static void perror ( void ) {
    
} /*  RULE 115  */

/*  Character class tests */

static void isalnum ( void ) {
    
} /*  RULE 115  */
static void isalpha ( void ) {
    
} /*  RULE 115  */
static void iscntrl ( void ) {
    
} /*  RULE 115  */
static void isdigit ( void ) {
    
} /*  RULE 115  */
static void isgraph ( void ) {
    
} /*  RULE 115  */
static void islower ( void ) {
    
} /*  RULE 115  */
static void isprint ( void ) {
    
} /*  RULE 115  */
static void ispunct ( void ) {
    
} /*  RULE 115  */
static void isspace ( void ) {
    
} /*  RULE 115  */
static void isupper ( void ) {
    
} /*  RULE 115  */
static void isxdigit ( void ) {
    
} /*  RULE 115  */

static void tolower ( void ) {
    
} /*  RULE 115  */
static void toupper ( void ) {
    
} /*  RULE 115  */

/*  String functions */

static void strcpy ( void ) {
    
} /*  RULE 115  */
static void strncpy ( void ) {
    
} /*  RULE 115  */
static void strcat ( void ) {
    
} /*  RULE 115  */
static void strncat ( void ) {
    
} /*  RULE 115  */
static void strcmp ( void ) {
    
} /*  RULE 115  */
static void strncmp ( void ) {
    
} /*  RULE 115  */
static void strchr ( void ) {
    
} /*  RULE 115  */
static void strrchr ( void ) {
    
} /*  RULE 115  */
static void strspn ( void ) {
    
} /*  RULE 115  */
static void strcspn ( void ) {
    
} /*  RULE 115  */
static void strpbrk ( void ) {
    
} /*  RULE 115  */
static void strstr ( void ) {
    
} /*  RULE 115  */
static void strlen ( void ) {
    
} /*  RULE 115  */
static void strerror ( void ) {
    
} /*  RULE 115  */
static void strtok ( void ) {
    
} /*  RULE 115  */

static void memcpy ( void ) {
    
} /*  RULE 115  */
static void memmove ( void ) {
    
} /*  RULE 115  */
static void memcmp ( void ) {
    
} /*  RULE 115  */
static void memchr ( void ) {
    
} /*  RULE 115  */
static void memset ( void ) {
    
} /*  RULE 115  */

/*  Mathematical functions */

static void sin ( void ) {
    
} /*  RULE 115  */
static void cos ( void ) {
    
} /*  RULE 115  */
static void tan ( void ) {
    
} /*  RULE 115  */
static void asin ( void ) {
    
} /*  RULE 115  */
static void acos ( void ) {
    
} /*  RULE 115  */
static void atan ( void ) {
    
} /*  RULE 115  */
static void atan2 ( void ) {
    
} /*  RULE 115  */
static void sinh ( void ) {
    
} /*  RULE 115  */
static void cosh ( void ) {
    
} /*  RULE 115  */
static void tanh ( void ) {
    
} /*  RULE 115  */
static void exp ( void ) {
    
} /*  RULE 115  */
static void log ( void ) {
    
} /*  RULE 115  */
static void log10 ( void ) {
    
} /*  RULE 115  */
static void pow ( void ) {
    
} /*  RULE 115  */
static void sqrt ( void ) {
    
} /*  RULE 115  */
static void ceil ( void ) {
    
} /*  RULE 115  */
static void floor ( void ) {
    
} /*  RULE 115  */
static void fabs ( void ) {
    
} /*  RULE 115  */
static void ldexp ( void ) {
    
} /*  RULE 115  */
static void frexp ( void ) {
    
} /*  RULE 115  */
static void modf ( void ) {
    
} /*  RULE 115  */
static void fmod ( void ) {
    
} /*  RULE 115  */

/*  Utility functions */

static void atof ( void ) {
    
} /*  RULE 115  */
static void atoi ( void ) {
    
} /*  RULE 115  */
static void atol ( void ) {
    
} /*  RULE 115  */
static void strtod ( void ) {
    
} /*  RULE 115  */
static void strtol ( void ) {
    
} /*  RULE 115  */
static void strtoul ( void ) {
    
} /*  RULE 115  */
static void rand ( void ) {
    
} /*  RULE 115  */
static void srand ( void ) {
    
} /*  RULE 115  */
static void calloc ( void ) {
    
} /*  RULE 115  */
static void malloc ( void ) {
    
} /*  RULE 115  */
static void realloc ( void ) {
    
} /*  RULE 115  */
static void free ( void ) {
    
} /*  RULE 115  */
static void abort ( void ) {
    
} /*  RULE 115  */
static void exit ( void ) {
    
} /*  RULE 115  */
static void atexit ( void ) {
    
} /*  RULE 115  */
static void system ( void ) {
    
} /*  RULE 115  */
static void getenv ( void ) {
    
} /*  RULE 115  */
static void bsearch ( void ) {
    
} /*  RULE 115  */
static void qsort ( void ) {
    
} /*  RULE 115  */
static void abs ( void ) {
    
} /*  RULE 115  */
static void labs ( void ) {
    
} /*  RULE 115  */
static void div ( void ) {
    
} /*  RULE 115  */
static void ldiv ( void ) {
    
} /*  RULE 115  */

/*  Non-local Jumps */

static void setjmp ( void ) {
    
} /*  RULE 115  */
static void longjmp ( void ) {
    
} /*  RULE 115  */

/*  Signals */

static void signal ( void ) {
    
} /*  RULE 115  */
static void raise ( void ) {
    
} /*  RULE 115  */

/*  Date and time functions */

static void clock ( void ) {
    
} /*  RULE 115  */
static void time ( void ) {
    
} /*  RULE 115  */
static void difftime ( void ) {
    
} /*  RULE 115  */
static void mktime ( void ) {
    
} /*  RULE 115  */
static void asctime ( void ) {
    
} /*  RULE 115  */
static void ctime ( void ) {
    
} /*  RULE 115  */
static void gmtime ( void ) {
    
} /*  RULE 115  */
static void localtime ( void ) {
    
} /*  RULE 115  */
static void strftime ( void ) {
    
} /*  RULE 115  */
