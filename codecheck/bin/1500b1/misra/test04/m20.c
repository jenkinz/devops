/*  Misra C Enforcement Testing */

/*  Rule 20: Required */
/*  All object and function identifiers shall be declared before use. */


#include "misra.h"

void func ( void ) ;

SI_32
rule20 ( void ) 
{
    
    func (  ) ; 
    funcy (  ) ; /*  RULE 20  */
    
    return 1;
    
}
