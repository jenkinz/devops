/*  Misra C Enforcement Testing */

/*  Rule 29: Required */
/*  The use of a tag should be consistent with the declaration. */

/*  This is covered by Rule 1. */

struct stag { int  a; int b; };

struct stag a1 = { 0, 0 };

void f29( void ) {
	struct stag { int a; } ;	/* rule 5.4 [ m29 ] */

}