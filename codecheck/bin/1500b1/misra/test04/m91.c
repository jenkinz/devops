/*  Misra C Enforcement Testing */

/*  Rule 91: Required */
/*  Macros shall not be #defined'd and #undef'd in a block. */


#define SQR ( x ) (  ( x ) * ( x )  ) 
#undef SQR ( x ) 
#define PI ( 3.14159265f ) 

void
func91a ( void ) 
{
    
    #define HELLO SAILOR /*  RULE 91  */
    #undef SQR ( x ) /*  RULE 91  */
    #undef PI /*  RULE 91  */
    
}
