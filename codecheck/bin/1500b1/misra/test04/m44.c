/*  Misra C Enforcement Testing */

/*  Rule 44: Advisory */
/*  Redundant explicit casts should not be used. */


#include "misra.h"

SI_32
rule44 ( void ) 
{
    
SI_16 a = 3;
SI_32 i = 3;
SI_32 j = 3;
SI_32 c = 3;
    
    j = ( SI_32 ) i; /*  RULE 44  */
    j = i * ( SI_32 ) j; /*  RULE 44  */
    j = ( i + j ) * ( SI_32 ) a; 
    
    c = j;
    
    return c;
    
}
