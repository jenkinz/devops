/*  Misra C Enforcement Testing */

/*  Rule 56: Required */
/*  The goto statement shall not be used. */


#include "misra.h"

SI_32
rule56 ( void ) 
{
    
SI_32 c = 3;
SI_32 i = 3;
    
    switch ( i ) 
    {
        
        case 1: 
        case 2: 
        case 3: 
        break;
        
        default: 
        goto blob; /*  RULE 56  */
        break;
        
    }
    
    blob: /*  RULE 55  */
    
    return c;
    
}
