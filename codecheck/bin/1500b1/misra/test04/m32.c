/*  Misra C Enforcement Testing */

/*  Rule 32: Required */
/*  In an enumerator list, the '=' construct shall not be used to */
/*  explicitly initialise members other than the first, unless all */
/*  items are explicitly initialised. */


#include "misra.h"

static enum en1 {
    a,b,c,d 
}; 
static enum en2 {
    a=1,b,c,d 
}; 
static enum en3 {
    a=0,b=1,c=2,d=3 
}; 

static enum en4 {
    a,b=1,c,d 
}; /*  RULE 32  */
static enum en5 {
    a,b=1,c=2,d 
}; /*  RULE 32  */
static enum en6 {
    a,b=1,c=2,d=3 
}; /*  RULE 32  */
static enum en7 {
    a=43,b,c=2,d=3 
}; /*  RULE 32  */
