/*  Misra C Enforcement Testing */

/*  Rule 81: Advisory */
/*  const qualification should be used on function parameters which are */
/*  passed by reference, where it is intended that the function will */
/*  not modify the parameter. */



/*  Not statically enforceable. It is very difficult in practice to */
/*  divine this intent automatically. */


#include "misra.h"

void myfunction ( const SI_16 * myparam )	/*  ok */
{	
	* myparam = 2;	/*  81 */

}

void myfunction1 (  SI_16 * myparam1 )	/*  81 */
{	
	* myparam1 = 2;	/*  81 */

}
