/*  Misra C Enforcement Testing */

/*  Rule 46: Required */
/*  The value of an expression shall be the same under any order of */
/*  evaluation that the standard permits. */


#include "misra.h"

static void func46 ( SI_32 m, SI_32 n ) ;

static SI_32
func46a ( SI_32 m, SI_32 n ) 
{
    
    return m + n;
    
}

static struct st
{
    
int st_m;
int st_n;

} local_st;

SI_32
rule46 ( void ) 
{
    
SI_32 i = 3;
SI_32 x = 3;
SI_32 y = 3;
SI_32 z = 3;
    
struct st this_st;
    
    this_st.st_m = 1;
    this_st.st_n = 2;
    
    z = ( y=i,++y ) + i++; /*  RULE 46  */
    
    z = ++i + ( y=x,++y ) ; 
    
    z = ++i + ( y=i,++y ) ; /*  RULE 46  */
    
    z = ++i + ( 1 || i++ ) ; 
    
    y = func46a ( x, ( x=3,x++ )  ) ; /*  RULE 46  */
    
    y = func46a ( x, ( i=2,i+3 )  ) ; 
    
    z = i + i++; /*  RULE 46  */
    
    z = ( y=x,++y ) + i++; 
    
    z = ( i = 3 ) + i + 8; /*  RULE 46  */
    
    z = ( this_st.st_m = 3 ) + 
    this_st.st_m + 8; /*  RULE 46  */
    
    z = ( this_st.st_m = 3 ) + 
    this_st.st_n + 8; 
    
    z = ++i + (  ( y += 2,y ) ,y++ ) ;
    
    z = (  ( ++i+i ) >0 ) ? ++i : --i;/*  RULE 46  */
    
    z = ( i>0 ) ? ++i : --i; 
    
    z = ++i + ( 3*8*1 && i++ ) ; /*  RULE 46  */
    
    z = ++i + ( y, y++ ) ; 
    
    z = ++i + ( 3*8*0 || i++ ) ; /*  RULE 46  */
    
    z = ++i + ( i, y++ ) ; 
    
    return z;
    
}
