/*  Misra C Enforcement Testing */

/*  Rule 23: Advisory */
/*  All declarations at file scope should be static where possible. */


#include "misra.h"

SI_32 func23 ( void ) ; /*  RULE 23  */

SI_32
func23 ( void ) 
{
    
    return 1;
    
}

SI_32
rule23 ( void ) 
{
    
    return func23 (  ) ;
    
}
