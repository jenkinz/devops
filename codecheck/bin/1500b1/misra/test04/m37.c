/*  Misra C Enforcement Testing */

/*  Rule 37: Required */
/*  Bitwise operations shall not be performed on signed integer types. */


#include "misra.h"

SI_32
rule37 ( void ) 
{
    
SI_32 a = 3;
SI_32 b = 3;
SI_32 c = 3;
    
UI_16	u;

	a = (~u );			/* Rule37 misra-c:2004 10.5 use explict cast of the same type */
	a = ( UI_16 ) (~u) ;	

	a = (u<<2 );			/* Rule37 misra-c:2004 10.5 use explict cast of the same type */
	a = ( UI_16 ) (u<<2) ;

    c = a >> 2; /*  RULE 37  */
    c = a << 2; /*  RULE 37  */
    c = a & b; /*  RULE 37  */
    c = a | b; /*  RULE 37  */
    c = a ^ b; /*  RULE 37  */
    c = ~a; /*  RULE 37  */
    


    return c;
    
}
