/*  Misra C Enforcement Testing */

/*  Rule 63: Advisory */
/*  A switch expression should not represent a Boolean value. */


#include "misra.h"

SI_32
rule63 ( void ) 
{
    
SI_32 c = 3;
SI_32 i = 3;
    
    switch ( i == 3 ) /*  RULE 63  [ very similiar to rule 53 ] */
    {
        
        case 3:
        case 4: c = 4; break;
        default: break;
        
    }
    
    switch ( i ) 
    {
        
        case 3:
        case 4: c = 4; break;
        default: break;
        
    }
    
    return c;
    
}
