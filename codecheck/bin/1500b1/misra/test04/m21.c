/*  Misra C Enforcement Testing */

/*  Rule 21: Required */
/*  Identifiers in an inner scope shall not mask identifiers in an */
/*  outer scope. */


#include "misra.h"

static SI_32 i;
static SI_32 func21 ( SI_32 j ) ;

static SI_32
func21 ( SI_32 j ) 
{
    
SI_32 i = 1; /*  RULE 21  */
    
    if ( j > 3 ) 
    {
        
SI_32 j = 4; /*  RULE 21  */
        
        i = j;
        
    }
    
    return i;
    
}

static SI_32
func21a ( SI_32 j ) 
{
    
    return i;
    
}
