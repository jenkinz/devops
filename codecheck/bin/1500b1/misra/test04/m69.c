/*  Misra C Enforcement Testing */

/*  Rule 69: Required */
/*  Functions with variable numbers of arguments shall not be used. */


#include <stddef.h>
#include <stdarg.h>

#include "misra.h"

static void rule69 ( SC_8 *fmt, ... ) ; /*  RULE 69  */

static SI_32
rule69 ( SC_8 *fmt, ... ) /*  RULE 69  */
{
    
SI_32 c = 1;
va_list ap;
SC_8 *pc;
SI_32 ival;
    
    va_start ( ap, fmt ) ; /*  RULE 69  */
    
    for ( pc = fmt; ( *pc != NULL ) ; pc++ ) 
    {
        
        switch ( *pc ) 
        {
            
            case 'd':
            ival = va_arg ( ap, SI_32 ) ; /*  RULE 69  */
            break; 
            
            default:
            break;
            
        }
        
    }
    
    va_end ( ap ) ; /*  RULE 69  */
    
    return c;
    
}
