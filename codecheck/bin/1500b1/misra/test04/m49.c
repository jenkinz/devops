/*  Misra C Enforcement Testing */

/*  Rule 49: Advisory */
/*  Tests of a value against zero should be made explicit, unless the */
/*  operator is effectively Boolean. */

/*  Not automatically detectable statically in all cases. Tools can */
/*  either warn on all or no occurrences in general. */


#include "misra.h"

SI_32
rule49 ( void ) 
{
    
SI_32 c = 3;
SI_32 d;
    
    if ( c ) /*  RULE 49  */
    {
        
        d = 1;
        
    }
    
    if ( c == 3 ) 
    {
        
        d = 1;
        
    }
    
    if ( ( c == 3 ) || d ) /*  RULE 49  */
    {
        
        d = 1;
        
    }
    
    if ( ( c == 3 ) || ( d == 1 ) ) 
    {
        
        d = 1;
        
    }
    
    if ( d == sizeof ( int ) ) 
    {
        
        d = 1;
        
    }
    
    if ( c == ( int ) 1. ) 
    {
        
        d = 1;
        
    }
    
    if ( ( int ) 145 ) 
    {
        
        d = 1;
        
    }
    
    if ( ( int ) 1. ) /*  RULE 49  */
    {
        
        d = 1;
        
    }
    
    return c;
    
}
