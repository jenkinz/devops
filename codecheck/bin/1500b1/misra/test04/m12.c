/*  Misra C Enforcement Testing */

/*  Rule 12: Advisory */
/*  Identifiers in different name spaces shall have different spelling. */


#include "misra.h"


/*  x is first used in function prototype namespace. */

static int func12 ( SI_32 x ) ;

/*  x is now used in structure tag and structure member namespace. */

struct x /*  RULE 12  */
{
    
SI_32 x; /*  RULE 12  */

} xx;


 static int func12 ( SI_32 x )
{
    struct x /*  RULE 12 tag dup */
	{
    
		SI_32 x; /*  RULE 12  */

	} xx; /* RULE 12 hidden */

    /*  x is now used in the remaining name space. */

SI_32 x; /*  RULE 12  */

    /*  x is used in the label name space */

    if ( 1 ) goto x; /*  RULE 12  */
    x: /*  RULE 12  */
    return 1;
    
}
