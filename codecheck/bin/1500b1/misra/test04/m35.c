/*  Misra C Enforcement Testing */

/*  Rule 35: Required */
/*  Assignment operators shall not be used in expressions which return */
/*  Boolean values. */


#include "misra.h"

SI_32
rule35 ( void ) 
{
    
SI_32 a = 3;
SI_32 b = 3;
SI_32 c = 3;
    
    if ( a == 3 ) 
    {
        
        c = 1;
        
    }
    
    if ( ( a = b ) != 0 ) /*  RULE 35  */
    {
        
        c = 1;
        
    }
    
    if ( a = b ) /*  RULE 35  */
    {
        
        c = 1;
        
    }
    
    return c;
    
}
