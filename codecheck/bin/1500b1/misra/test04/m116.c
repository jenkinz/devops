/*  Misra C Enforcement Testing */

/*  Rule 116: Required */
/*  All libraries used in production code shall be written to comply */
/*  with the provisions of this document, and shall have been subject */
/*  to appropriate validation. */

/*  Not automatically enforceable by a simple rule. */


#include "misra.h"

#include <stdlib.h> /* here we force the std defn's so the later will conflict */
#include <stdio.h>

/*  verify that the prototypes agree with the stdlib's misra-c:2004 3.6 */

static void fopen ( void ) ;	/*  misra-c:2004 3.6 RULE 116 [ real fopen return a * to struct, not void ] */
