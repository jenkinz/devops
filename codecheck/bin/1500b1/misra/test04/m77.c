/*  Misra C Enforcement Testing */

/*  Rule 77: Required */
/*  The unqualifed type of parameters passed to a function shall be */
/*  compatible with the unqualified expected types defined in the */
/*  function prototype. */


#include "misra.h"

void func77a ( char c, short s, int i, long l, float f, double d ) ;
void func77b ( const unsigned char *puc ) ;

void func77c ( void ) 
{
    
char pc = '\0';
unsigned char uc = '\0';
signed char sc = '\0';
signed short ss = 0;
signed int si = 0;
signed long sl = 0l;
float ff = 0.f;
double dd = 0.fL;
    
unsigned char *puc = &uc;
const unsigned char *cpuc = &uc;
int *pi = &si;
    
    func77a ( sc, /*  RULE 77  */
    ss, si, sl, ff, dd ) ;
    
    func77a ( pc, 
    ss, si, sl, ff, dd ) ;
    
    func77a ( ( const ) pc, 
    ss, si, sl, ff, dd ) ;
    
    func77b ( pi ) ; /*  RULE 77  */
    
    func77b ( puc ) ; 
    func77b ( cpuc ) ; 
    
}
