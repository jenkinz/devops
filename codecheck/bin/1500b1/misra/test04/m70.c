/*  Misra C Enforcement Testing */

/*  Rule 70: Required */
/*  Functions shall not call themselves either directly or indirectly. */

/*  It is trivial to detect direct recursion, but it can in some cases */
/*  be impossible to detect indirect recursion statically. */


#include "misra.h"

SI_32
rule70 ( SI_32 a ) 
{
    
SI_32 c;
    
    switch ( a ) 
    {
        
        case 0:
        case 1:
        c = 1;
        break;
        
        default:
        if ( a < 0 ) 
        {
            
            c = ( -1 ) ;
            
        }
        else
        {
            
            c = a * rule70 ( a-1 ) ; /*  RULE 70  */
            
        }
        break;
        
    }
    
    return c;
    
}
