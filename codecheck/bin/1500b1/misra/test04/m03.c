/*  Misra C Enforcement Testing */

/*  Rule 3: Advisory */
/*  Interfaces to other assembly languages. */

/*  Inline assembly language violates Rule 1. */

/*  Calling a function is not automatically enforceable and */
/*  requires manual inspection techniques. */


static int dummy;
