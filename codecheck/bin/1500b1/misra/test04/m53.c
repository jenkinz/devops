/*  Misra C Enforcement Testing */

/*  Rule 53: Required */
/*  All non-null statements shall have a side-effect. */


#include "misra.h"

SI_32
rule53 ( void ) 
{
    
SI_32 c = 3;
SI_32 i = 3;
SI_32 j = 3;
SI_32 k = 3;
volatile SI_32 v;
    
    k = j + ( i,j ) ; /*  RULE 53		[ abraxas-misra rule 42 will catch this ] */
    k; /*  RULE 53  */
    i == k; /*  RULE 53  */
    
    return c;
    
}
