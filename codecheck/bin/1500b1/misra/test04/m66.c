/*  Misra C Enforcement Testing */

/*  Rule 66: Advisory */
/*  Only expressions concerned with loop control should appear within a */
/*  for statement. */

/*  This is in general difficult to detect automatically and statically. */
/*  The following gives a simple case. */


#include "misra.h"

SI_32
rule66 ( void ) 
{
    
SI_32 c = 3;
    
SI_32 i = 3;
SI_32 x = 3;
    
    for ( i = 0; i < 10; ++i, ( x = 8 ) ) /*  RULE 66  */
    {
        

        /*  DO NOTHING */

        
    }
    
    return c;
    
}
