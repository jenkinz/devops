/*  Misra C Enforcement Testing */

/*  Rule 96: Required */
/*  In the definition of a function-like macro the whole definition, */
/*  and each instance of a parameter, shall be enclosed in parentheses. */


#define TIMES1 ( x,y ) (  ( x ) * ( y )  ) 
#define TIMES2 ( x,y ) ( x* ( y )  ) /*  RULE 96  */
#define TIMES3 ( x,y ) (  ( x ) *y ) /*  RULE 96  */
#define TIMES4 ( x,y ) ( x ) * ( y ) /*  RULE 96  */

#define myabs0 ( x ) (  (  ( x ) >=0 ) ? ( x ) : - ( x )  ) 
#define myabs1 ( x ) (  ( x ) >=0 ) ? ( x ) : - ( x ) /*  RULE 96  */
#define myabs2 ( x ) (  ( x>=0 ) ? ( x ) : - ( x )  ) /*  RULE 96  */
#define myabs3 ( x ) (  (  ( x ) >=0 ) ? x : - ( x )  ) /*  RULE 96  */
#define myabs4 ( x ) (  (  ( x ) >=0 ) ? ( x ) : -x ) /*  RULE 96  */

#define myabs5 -10 

void
func96 ( void ) 
{
    
int i = TIMES1 ( 1,3 ) ;
int j = TIMES2 ( 1,3 ) ;
int k = TIMES3 ( 1,3 ) ;
int l = TIMES4 ( 1,3 ) ;
    
}
