/*  Misra C Enforcement Testing */

/*  Rule 103: Required */
/*  Relational operators shall not be applied to pointer types except */
/*  where both operands are of the same type and point to the same */
/*  array, structure or union. */


void
func103 ( void ) 
{
    
int m[10] = {
        0,1,2,3,4,5,6,7,8,9 
    };
int n[10] = {
        0,1,2,3,4,5,6,7,8,9 
    };
    
int *pm, *pmm;
    
    pm = &m[1];
    pmm = &m[3];
    
    if ( pm < pmm ) 
    {
        
        
    }
    
    pmm = &n[3];
    
    if ( pm < pmm ) /*  RULE 103  */
    {
        
        
    }
    
}
