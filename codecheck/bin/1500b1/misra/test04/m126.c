/*  Misra C Enforcement Testing */

/*  Rule 126: Required */
/*  The library functions abort, exit, getenv and system from */
/*  <stdlib.h> shall not be used. */
/*   */


#include <stdlib.h>

#include "misra.h"

static void
func126 ( void ) 
{
    
int i;
char *pc;
    
    i = system ( "ls" ) ; /*  RULE 126  */
    pc = getenv ( "ENVAL" ) ; /*  RULE 126  */
    
    if ( i == 1 ) 
    {
        
        abort (  ) ; /*  RULE 126  */
        
    }
    else
    {
        
        exit ( EXIT_SUCCESS ) ; /*  RULE 126  */
        
    }
    
}
