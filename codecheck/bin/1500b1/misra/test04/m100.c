/*  Misra C Enforcement Testing */

/*  Rule 100: Required */
/*  The defined pre-processor operator shall only be used in one of the */
/*  two standard forms. */


#if defined d 
#endif
#if defined ( d ) 
#endif
#if defined "d" /*  RULE 100  */
#endif

void
func100 ( void ) 
{
    
int i = 1;
    
}
