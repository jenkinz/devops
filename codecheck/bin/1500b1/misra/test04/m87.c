/*  Misra C Enforcement Testing */

/*  Rule 87: Required [ 2004 adv ] */

/*  19.1 ADV #include statements in a file shall only be preceded by other */
/*  pre-processor directives or comments. */


/*  8.5 No defn's in a header file */

int i; /*  RULE 87  */

#include "misra.h"

int func87a ( void ) 
{
    
    return 1;
    
}
