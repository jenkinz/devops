/*  Misra C Enforcement Testing */

/*  Rule 99: Required */
/*  All uses of the #pragma directive shall be documented and */
/*  explained. */


#pragma asm ( 1,3 ) /*  RULE 99  */

void
func99 ( void ) 
{
    
    int i = 1;
    
}
