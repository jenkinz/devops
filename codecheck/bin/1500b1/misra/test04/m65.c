/*  Misra C Enforcement Testing */

/*  Rule 65: Required */
/*  Floating point variables shall not be used as loop counters. */


#include "misra.h"

SI_32
rule65 ( void ) 
{
    
SI_32 c = 3;
    
FL_32 f;
    
    for ( f = 0.1f; f < 3.8f; ++f ) /*  RULE 65  */
    {
        

        /*  DO NOTHING */

        
    }
    
    return c;
    
}
