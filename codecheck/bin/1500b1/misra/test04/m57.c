/*  Misra C Enforcement Testing */

/*  Rule 57: Required */
/*  The continue statement shall not be used. */


#include "misra.h"

SI_32
rule57 ( void ) 
{
    
SI_32 c = 3;
SI_32 i = 3;
    
    for ( i = 0; i < 10; ++i ) 
    {
        
        continue;
        
    }
    
    return c;
    
}
