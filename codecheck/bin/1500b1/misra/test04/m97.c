/*  Misra C Enforcement Testing */

/*  Rule 97: Advisory */
/*  Identifiers in pre-processor directives should be defined before */
/*  use. */


#define x ( 0 ) 
#if x > 0 
#endif

#if y > 0 /*  RULE 97  */
#endif

void
func97 ( void ) 
{
    
int i = 1;
    
}
