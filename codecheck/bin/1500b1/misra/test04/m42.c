/*  Misra C Enforcement Testing */

/*  Rule 42: Required */
/*  The comma operator shall not be used, except in the control */
/*  expression of a for loop. */


#include "misra.h"

SI_32
rule42 ( void ) 
{
    
SI_32 a = 3;
SI_32 i = 3;
SI_32 j = 3;
SI_32 c = 3;
    
    a = i++, i+j; /*  RULE 42  */
    a = (  (  (  ( i++, i+j )  )  )  ) ; /*  RULE 42  */
    a = (  (  (  ( i++ ) , ( i+j )  )  )  ) ; /*  RULE 42  */
    
    for ( i = 0, j = 0; i < 10; ++i, ++j ) 
    {
        

        /*  DO NOTHING */

        
    }
    
    c = j;
    
    return c;
    
}
