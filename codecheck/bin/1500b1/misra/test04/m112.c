/*  Misra C Enforcement Testing */

/*  Rule 112: Required */
/*  Bit fields of type signed int shall be at least 2 bits long. */


#include "misra.h"

static struct s
{
    
signed int p_a : 4; 
unsigned int p_b : 1; 
signed int p_c : 1; /*  RULE 112  */

} st;

static void
func112 ( void ) 
{
    
    st.p_a = 0x1;
    
}
