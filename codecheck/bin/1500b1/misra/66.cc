#include "misra.cch"
int inforloop;  // signal in for loop

if ( op_for ) {
        inforloop = 1;

}

if ( stm_cp_begin ) {

        if ( stm_kind == FOR ) {

                inforloop = 0;
        }
}

int in_paren;

if ( op_open_paren ) in_paren++;
if (op_close_paren ) in_paren--;

int for_assign_count;

if ( op_for ) for_assign_count = 0;

if ( op_assign ) {

DUMP("ASSIGN")
	if ( inforloop && in_paren ) for_assign_count++;

warn(0, "paren=%d infor=%d", in_paren, inforloop );
	if ( inforloop && for_assign_count > 1 ) {
		WARN_MA( 66, "Only expressions concerned with loop control should appear.");
	}
}

