// Misra C Enforcement Testing
//
// Rule 16: Required
// Underlying floating point bit representations should not be used.
//
// Not automatically enforceable.
///


#include "misra.cch"

if ( op_bit_and ) {
  //  DEBO("OA")

        if ( op_base(1) == FLOAT_TYPE || op_base(2) == FLOAT_TYPE ) {
            WARN_MR( 16, "Underlying floating point bit representations should not be used." );
        }
}