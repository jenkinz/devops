#include "misra.cch"

// This Rule Requires -C type checking "ON" to be invoked.

if ( cnv_truncate ) {

	WARN_MR( 43, "Implicit conversions which may result in a loss of information" );
}
