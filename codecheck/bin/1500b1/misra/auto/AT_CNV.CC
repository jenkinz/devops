/*
***************************************************************************
*                                                                         *
* FILE NAME:	cnv.cc                                                    *
* COPYRIGHT (C) 1994 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.            *
* AUTHOR:	Shuming Tan                     July 15, 1994             *
* PURPOSE:	This a CodeCheck rule file used to test the predefined    *
*               variables in cnv_ group.                                  *
*                                                                         *
***************************************************************************
*/

if(cnv_any_to_ptr)
  warn(1001, "A non-pointer converted to a pointer implicitly");

if (cnv_const_to_any) 
  warn(1002, "A const type converted to a non-const implicitly");

if (cnv_float_to_int)
  warn(1003, "A float converted to an integer implicitly");

if (cnv_int_to_float)
  warn(1004, "An integer converted to a float implicitly");

if (cnv_ptr_to_ptr) 
  warn(1005, "A pointer converted to a pointer implicitly");

if (cnv_signed_to_any)
  warn(1006, "A signed integer converted to an unsigned implicitly");

if (cnv_truncate)
  warn(1007, "An integer or a float truncated implicitly");

