/*
***************************************************************************
*                                                                         *
* FILE NAME:	lin.cc                                                    *
* COPYRIGHT (C) 1994 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.            *
* PURPOSE:	This a CodeCheck rule file used to test the predefined    *
*               variables in lin_ group.                                  *
*                                                                         *
***************************************************************************
*/

#define STATS		1	/*	to include statistics on each line	*/
#define COMMENTS	1	/*	to include comment variables		*/

if ( lin_continuation )
  warn (1001, "lin_continuation");

if ( lin_continues )
  warn (1002, "lin_continues");

if ( lin_dcl_count )
  warn (1003, "lin_dcl_count: %d", lin_dcl_count);

if ( lin_end )
  warn (1004, "lin_end");

if ( lin_has_code )
  warn (1005, "lin_has_code");

#ifdef COMMENT
if ( lin_has_comment )
  warn (1006, "lin_has_comment");
#endif

if ( lin_header )
 {
  switch (lin_header)
   {
     case 1:
        warn (1007, "lin_header - project header file");
        break;

     case 2:
        warn (1007, "lin_header - system header file");
        break;

     default:
        warn (1007, "lin_header - unknown origin <%d>", lin_header);
        break;
   }
 }

if ( lin_indent_space )
  warn (1008, "lin_indent_space: %d", lin_indent_space);

if ( lin_indent_tab )
  warn (1009, "lin_indent_tab: %d", lin_indent_tab);

#ifdef COMMENTS
if ( lin_is_comment )
  warn (1010, "lin_is_comment");
#endif

if ( lin_is_white )
  warn (1011, "lin_is_white");

if ( lin_is_exec )
  warn (1012, "lin_is_exec");

#ifdef STATS
if ( lin_length )
  warn (1013, "lin_length: %d", lin_length);
#endif

if ( lin_nest_level )
  warn (1014, "lin_nest_level: %d", lin_nest_level);

#ifdef COMMENTS
if ( lex_nested_comment )
  warn (1015, "lex_nested_comment");

if ( lex_cpp_comment)
  warn (1016, "lex_cpp_comment");
#endif

if ( lin_number )
  warn (1017, "lin_number: %d", lin_number);

#ifdef STATS
if ( lin_operands )
  warn (1018, "lin_operands: %d", lin_operands);

if ( lin_operators )
  warn (1019, "lin_operators: %d (before macro expansion)", lin_operators);
#endif

if ( lin_preprocessor )
  warn (1020, "lin_preprocessor");

if ( lin_source )
  warn (1021, "lin_source");

if ( lin_suppressed )
  warn (1022, "lin_suppressed");

#ifdef STATS
if ( lin_tokens )
  warn (1023, "lin_tokens: %d", lin_tokens);
#endif

if ( lin_within_class )
 {
  switch (lin_within_class)
   {
     case 1:
        warn (1024, "lin_within_class - class definintion");
        break;

     case 2:
        warn (1024, "lin_within_class - member function definition");
        break;

     default:
        warn (1024, "lin_within_class - unknown value <%d>", lin_within_class);
        break;
   }
 }

if ( lin_within_function )
  warn (1025, "lin_within_function");

if ( lin_within_tag )
 {
  switch (lin_within_tag)
   {
     case 1:
        warn (1026, "lin_within_tag - ENUM");
        break;

     case 2:
        warn (1026, "lin_within_tag - UNION");
        break;
 
     case 3:
        warn (1026, "lin_within_tag - STRUCT");
        break;

     case 4:
        warn (1026, "lin_within_tag - CLASS");
        break;

      default:
        warn (1026, "lin_within_tag - unknown value <%d>", lin_within_tag);
        break;
   }
 }


