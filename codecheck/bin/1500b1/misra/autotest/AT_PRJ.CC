/*
***************************************************************************
*                                                                         *
* FILE NAME:	prj.cc                                                    *
* COPYRIGHT (C) 1994 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.            *
* AUTHOR:	Shuming Tan                     July 26, 1994             *
* PURPOSE:	This a CodeCheck rule file used to test the predefined    *
*               variables in prj_ group.                                  *
*                                                                         *
***************************************************************************
*/

if (prj_begin)
{
  printf("PROJECT::%s\n",prj_name());
  printf("--------------------------------------------------------------\n");
}

if (prj_end)
{
  printf("PROJECT::%s\n",prj_name());
  printf("--------------------------------------------------------------\n");
  printf("number of tokens                           : %d\n",prj_tokens);
  printf("number of external global simple variables : %d\n",prj_simple);
  printf("number of external aggregate variables     : %d\n",prj_aggr);
  printf("number of external global variables        : %d\n",prj_globals);
  printf("number of external global array elements   : %d\n",prj_array);
  printf("number of external tag members             : %d\n",prj_members);
  printf("number of unused external variables        : %d\n",prj_unused);
  printf("number of distinct macros                  : %d\n",prj_macros);
  printf("number of conflicting macro definitions    : %d\n",prj_conflicts);
  printf("number of functions                        : %d\n",prj_functions);
  printf("number of Halstead operators               : %d\n",prj_H_operators);
  printf("number of Halstead operands                : %d\n",prj_operands);
  printf("number of unique Halstead operators        : %d\n",prj_uH_operators);
  printf("number of unique Halstead operands         : %d\n",prj_uH_operands);
  printf("number of standard operators               : %d\n",prj_operators);
  printf("number of standard operands                : %d\n",prj_operands);
  printf("number of unique operators                 : %d\n",prj_u_operators);
  printf("number of unique operands                  : %d\n",prj_u_operands);
  printf("number of binary decision points           : %d\n",prj_decisions);
  printf("number of non executable statements        : %d\n",prj_nonexec);
  printf("number of low level statements             : %d\n",prj_low);
  printf("number of high level statements            : %d\n",prj_high);
  printf("number of all lines                        : %d\n",prj_total_lines);
  printf("number of blank lines                      : %d\n",prj_white_lines);
  printf("number of pure comment lines               : %d\n",prj_com_lines);
  printf("number of executable lines                 : %d\n",prj_exec_lines);
  printf("number of warning messages                 : %d\n",prj_warnings);
  printf("number of header files                     : %d\n",prj_headers);
  printf("number of modules                          : %d\n",prj_modules);
}
