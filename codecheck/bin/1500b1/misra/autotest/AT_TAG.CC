/*
***************************************************************************
*                                                                         *
* FILE NAME:	tag.cc                                                    *
* COPYRIGHT (C) 1994 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.            *
* AUTHOR:	Shuming Tan                     July 25, 1994             *
* PURPOSE:	This a CodeCheck rule file used to test the predefined    *
*               variables in tag_ group.                                  *
*                                                                         *
***************************************************************************
*/

#include <check.cch>

if (tag_begin)
{
  printf("BEGINNING OF TAG::%s\n",tag_name());
  printf("-----------------------------------------------------\n");
}

 if (tag_hidden)
      printf("A local tag hides another tag.\n");

if (tag_end)
{
  printf("END OF TAG::%s\n",tag_name());
  printf("-----------------------------------------------------\n");

  switch  (tag_kind)
  {
    case ENUM_TAG   :
      printf("This is a enumerate tag.\n");
      break;
    case UNION_TAG  :
      printf("This is a union tag.\n");
      break;
    case STRUCT_TAG :
      printf("This is a struct tag.\n");
      break;
    case CLASS_TAG  :
      printf("This is a class tag.\n");
      break;
    default         :
      printf("This is a unknown tag.\n");
   }
  

  if (tag_base_access) 
    printf("A base class does not have an explicit access.\n");
  if (tag_global) 
     printf("This tag has file scope.\n");
  if (tag_local)
    printf("This tag has local scope (within a function).\n");
  if (tag_nested)
    printf("This tag is defined within another tag.\n");
  if (tag_distance)
    switch (tag_distance)
    {
      case 1:
        printf("This is a Borland C++ _near tag.\n");
        break;
      case 2:
        printf("This is a Borland C++ _far tag.\n");
        break;
      case 3:
        printf("This is a Borland C++ _huge tag.\n");
        break;
      case 4:
        printf("This is a Borland C++ _export tag.\n");
    }
  printf("There are %d enumerate constants\n",tag_constants);
  
  if (tag_kind>ENUM_TAG)
  {  
    printf("There are %d lines.\n",tag_lines);
    printf("There are %d tokens.\n",tag_tokens);
    printf("There are %d base classes.\n",tag_bases);
    printf("There are %d constructors.\n",tag_constructors);
    printf("There are %d named classes nested within.\n",tag_classes);
    printf("There are %d friend functions declared.\n",tag_fcn_friends);
    printf("There are %d friend classes declared.\n",tag_friends);
    printf("There are %d member functions.\n",tag_functions);
    printf("There are %d data members defined.\n",tag_members);
    printf("There are %d operator functions declared.\n",tag_operators);
    printf("There are %d identifiers declared with private access.\n",tag_private);
    printf("There are %d identifiers declared with protected access.\n",tag_protected);
    printf("There are %d identifier declared with public access.\n",tag_public);
    printf("There are %d static member functions declared.\n",tag_static_fcn);
    printf("There are %d static data member declared.\n",tag_static_mem);
    printf("There are %d template parameters.\n",tag_template);
    printf("There are %d typedef names defined.\n",tag_types);
    if (tag_has_assign)
      printf("This class has an operator=.\n");
    if (tag_has_copy)
      printf("This class has a copy constructor.\n");
    if (tag_has_default)
      printf("This class has a default constructor.\n");
    if (tag_has_destr)
      printf("This class has a destructor.\n");
    if (tag_mem_access) 
      printf("The first member of this class does not have an access label.\n");
    if (tag_anonymous)
      printf("This is an anonymous(unnamed) tag.\n");
    if (tag_abstract)
      printf("This is an abstract class.\n");
    printf("There are %d constants with any access\n",tag_components(0,0));
    printf("There are %d constants with public only access\n",tag_components(0,1));
    printf("There are %d constants with protected only access\n",tag_components(0,2));
    printf("There are %d constants with private only access\n",tag_components(0,3));
    printf("There are %d members with any access\n",tag_components(1,0));
    printf("There are %d members with public only access\n",tag_components(1,1));
    printf("There are %d members with protected only access\n",tag_components(1,2));
    printf("There are %d members with private only access\n",tag_components(1,3));
    printf("There are %d functions with any access\n",tag_components(2,0));
    printf("There are %d functions with public only access\n",tag_components(2,1));
    printf("There are %d functions with protected only access\n",tag_components(2,2));
    printf("There are %d functions with private only access\n",tag_components(2,3));
    printf("There are %d types with any access\n",tag_components(3,0));
    printf("There are %d types with public only access\n",tag_components(3,1));
    printf("There are %d types with protected only access\n",tag_components(3,2));
    printf("There are %d types with private only access\n",tag_components(3,3));
    printf("There are %d base classed with any access\n",tag_components(4,0));
    printf("There are %d base classes with public only access\n",tag_components(4,1));
    printf("There are %d base classes with protected only access\n",tag_components(4,2));
    printf("There are %d base classes with private only access\n",tag_components(4,3));
  }
  printf("-----------------------------------------------------\n");
}  
      
  




