/*
***************************************************************************
*                                                                         *
* FILE NAME:	exp.cc                                                    *
* COPYRIGHT (C) 1994 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.            *
* AUTHOR:	Shuming Tan                     July 21, 1994             *
* PURPOSE:	This a CodeCheck rule file used to test the predefined    *
*               variables in exp_ group.                                  *
*                                                                         *
***************************************************************************
*/

if (exp_empty_initializer)
  warn(3001,"This is an empty initializer");

if (exp_not_ansi)
  warn(3002,"This a non ANSI expression");

if (exp_operands)
  warn(3003,"This expression has %d oprands",exp_operands);

if (exp_operators)
  warn(3004,"This expression has %d operators",exp_operators);

if (exp_tokens)
  warn(3005,"This expression has %d tokens",exp_tokens);
