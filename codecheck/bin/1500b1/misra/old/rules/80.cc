#include "misra.cch"

int inArglist;

if ( op_call ) inArglist = 0;

if ( idn_function ) inArglist = 1;

if ( op_cast ) {

	if ( inArglist ) {

		if ( op_base(2) == VOID_TYPE ) {

			WARN_MR( 80, "Void expressions shall not be passed as function parameters.");
		}
	}

}