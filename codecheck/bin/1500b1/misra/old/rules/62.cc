#include "misra.cch"

int seen_default;

if ( stm_no_default ) {

//warn(0,"no default");

	WARN_MR( 62, "All switch statements should contain a final default clause.");
}

if ( keyword("default") ) {
	seen_default = 1;
//DUMP("default")
}

if (stm_switch_cases) {
//warn(1,"sw-case=%d seen_def=%d", stm_switch_cases, seen_default );
	seen_default = 0;

}

if ( keyword("case") ) {
//warn(0,"CASE stm_cases=%d seen_default=%d", stm_cases, seen_default );

	if ( seen_default ) {

		WARN_MR( 62, "All switch statements should contain a final default clause.")
	}
}

/*
if ( op_break ) {

	DUMP("BREAK")
}
*/