#include "misra.cch"

if ( dcl_local || dcl_global || dcl_function ) {
	
	
	if ( dcl_base == CHAR_TYPE ) {

		if ( !(dcl_signed || dcl_unsigned) ) {

		WARN( 14, "char should only be used with the signed or unsigned keyword." );

		}	
	}
}
