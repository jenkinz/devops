#include "misra.cch"

int init_type;	// type at dcl-init
int arith_type;	// type of arithmetic if used

if ( mod_begin ) {

	init_type = 0;
	arith_type = 0;
}

if ( op_div || op_add || op_mul || op_subt ) {

	arith_type = op_base(1);
}

if ( dcl_initializer ) {

	init_type = dcl_base_root;
	
}

if ( op_semicolon ) {

	if ( init_type && arith_type ) {

		if ( init_type != arith_type ) {

		 WARN_MA(48, "Mixed precision arithmetic should use explicit casting" );
		}
	}
	
	init_type = 0;
	arith_type = 0;
}


