#include "misra.cch"

if (stm_switch_cases) {
warn(1,"sw-case=%d cases=%d", stm_switch_cases, stm_cases );

	if ( stm_switch_cases <= 1 ) {

		WARN_MR( 64, "Every switch statement shall have at least one case.");

	}

}