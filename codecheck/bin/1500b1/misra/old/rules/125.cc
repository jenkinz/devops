#include "misra.cch"

if ( idn_function ) {

	if ( CMPPREVTOK("atof") || CMPPREVTOK("atoi") || CMPPREVTOK("atol") ) {
	
		WARN_MR( 125, "Library functions atof, atoi and atol should not be used." );
	}
}