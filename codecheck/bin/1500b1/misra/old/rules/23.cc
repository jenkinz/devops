#include "misra.cch"

if ( dcl_global ) {
DEBD("DG")
}

if ( dcl_function ) {
DEBD("DF")
	if ( (dcl_storage_flags & STATIC_SC) == 0 ) {

		WARN_MA( 23, "All declarations at file scope should be static where possible.");
	}

}