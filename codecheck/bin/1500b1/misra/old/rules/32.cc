#include "misra.cch"

int enum_first;		// signal enum -1, and 1 if first is initialized
int enum_init;		// count number of initialized enums
int enum_count;		// count number of contstant in enum


if ( lex_initializer ) {

	if ( enum_first ) {

		enum_init++;		// increment each	

		if ( enum_count == 1 ) enum_first = 1;	// first was initialized
	}
}

if ( dcl_enum ) {
	if ( enum_first ) {
		enum_count++;		// count number seen
	}

}

if ( tag_begin ) {

	if ( tag_kind == ENUM_TAG ) {

		enum_first = -1;	// signal begin of enum
		enum_init = 0;
		enum_count = 0;
	}
}

if ( tag_end ) {

//warn( 999, "TE enf=%d eni=%d enc=%d", enum_first, enum_init, enum_count );
	if ( tag_kind == ENUM_TAG ) {


		if ( (enum_first!=1) || (enum_count > enum_init) ) {
			if ( enum_init && enum_init != enum_first )
				WARN_MR( 32, "Enum initialzation must be on first or ALL" );
		}
		enum_first = 0;		// signal end of enum tag
	}
}