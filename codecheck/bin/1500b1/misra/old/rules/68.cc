#include "misra.cch"

int inFunction;

if ( fcn_begin ) inFunction = 1;

if ( fcn_end ) inFunction = 0;

if ( dcl_function ) {

	if ( inFunction ) {

		WARN_MR( 68, "Functions shall always be declared at file scope.");

	}
}