#include "misra.cch"

//      * Rule 34 (The operands of a logical && or || shall be primary expressions).
// if ( (a == 3) || b == 3 ), both sides must be simple or have parens


int chkexp;

if ( mod_begin ) chkexp = 0;

if ( keyword("if") ) {
	chkexp = 1;

}

if ( op_if ) {

	chkexp = 0;
}

if ( op_log_or || op_log_and ) {



	if ( chkexp ) {

		if ( (op_parened_operand(1) & op_parened_operand(2)) == 0 ) {
				
			if ( all_digit( prev_token() ) ) {

			    warn( 34, "operands of && or || must be pri-expr");
			}
		}
	}
			
}