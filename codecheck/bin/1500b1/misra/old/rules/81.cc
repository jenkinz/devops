#include "misra.cch"

if ( dcl_parameter ) {

	if ( dcl_level(0) == POINTER && (dcl_level_flags(dcl_levels) & CONST_FLAG) == 0 ) {

		WARN_MA( 81, "const should be used on function reference parameters" );
	}

}