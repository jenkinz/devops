// Example implementation of Misra Coding Standard
// COPYRIGHT (C) 2005 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.  


#include "misra.cch"

int in_paren;

if ( op_open_paren ) in_paren++;
if (op_close_paren ) in_paren--;

char for_itername[MAXIDLEN];

if ( op_assign ) {

	if ( inforloop && idn_name() ) {
		if ( in_paren && strlen(for_itername) == 0 ) {
	
			strcpy( for_itername, idn_name() ); // get iter name
		}
		else if ( (in_paren == 0) && (strcmp(for_itername,idn_name())==0) ) {

			WARN_MA( 67, "For loop iteration variables should not be modified in body.");
		}
	}
}

/*
char for_itername[MAXIDLEN];

if ( op_assign ) {

	if ( inforloop && in_paren && strlen(for_itername) == 0 ) {
	
		strcpy( for_itername, idn_name() ); // get iter name
	}
	else if ( inforloop && (in_paren == 0) && (strcmp(for_itername,idn_name())==0) ) {

		WARN_MA( 67, "For loop iteration variables should not be modified in body.");

	}
}
*/
