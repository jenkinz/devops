#include "misra.cch"

if ( dcl_variable ) {

	if ( dcl_base_root == UNION_TYPE ) {

		WARN_MR(110,"Unions shall not be used to access sub-parts of larger data.");
	}
}