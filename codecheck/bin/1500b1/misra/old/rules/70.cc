#include "misra.cch"

//      * Rule 70 (Functions shall not call themselves, either directly or
//              indirectly).


if ( op_open_funargs && op_declarator == 0 )
{
        if ( strcmp( fcn_name(), prev_token() ) == 0 ) {

                WARN_MR( 70, "Functions shall not call themselves" );
        }
}
