#include "misra.cch"


int binary_exp;
int in_paren;


if ( mod_begin ) {
	in_paren = 0;
	binary_exp = 0;
}
if ( op_semicolon ) {

	in_paren = 0;
	binary_exp = 0;
}

if ( op_open_paren ) in_paren++;
if (op_close_paren ) in_paren--;

if ( op_add||op_subt||op_mul||op_div ) {	// binary op

		
	if ( op_parened_operand(1) || op_parened_operand(2) ) in_paren++;

	binary_exp++;
}

if ( op_assign ) {

	if ( (binary_exp > 2) && in_paren == 0 ) {

		WARN_MA( 47, "No dependence should be placed on C's operator precedence" );
	}
}