// Misra C Enforcement Testing
//
// Rule 64: Required
// Every switch statement shall have at least one case.
///

#include "misra.h"

SI_32
rule64 ( void ) 
{
    
SI_32 c = 3;
SI_32 i = 3;
    
    switch ( i == 3 ) // RULE 64 
    {
        
        default: break;
        
    }
    
    switch ( i ) 
    {
        
        case 4: c = 4; break;
        default: break;
        
    }
    
    return c;
    
}
