// Misra C Enforcement Testing
//
// Rule 41: Advisory
// The implementation of integer division in the chosen compiler
// should be determined, documented and taken into account.
//
// This is not enforceable but there are some troublesome cases which
// are worth flagging if the static analyser is sufficiently
// sophisticated.
///

#include "misra.h"

SI_32
rule41 ( void ) 
{
    
SI_32 a = 3;
SI_32 c = 3;
    
    a = 3 / -2; // RULE 41 
    a = -3 / 2; // RULE 41 
    a = 3%-2; // RULE 41 
    a = -3%2; // RULE 41 
    a /= -2; // RULE 41 
    a %= -2; // RULE 41 
    
    
    return c;
    
}
