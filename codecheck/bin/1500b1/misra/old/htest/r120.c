// Misra C Enforcement Testing
//
// Rule 120: Required
// The macro offsetof shall not be used.
///

#include <stddef.h>

#include "misra.h"

static struct s
{
    
int mem;

} st;

static void
func120 ( void ) 
{
    
size_t of;
    
    of = offsetof ( struct s, mem ) ; // RULE 120 
    
}
