// Misra C Enforcement Testing
///***************************************************************
// Rule 51: Advisory
// Evaluation of constant unsigned integer expressions should not lead
// to wrap-around.
///

#include "misra.h"

#if ( 1u - 2u ) 
//# error

#pragma message "misra 51 error"
#endif
