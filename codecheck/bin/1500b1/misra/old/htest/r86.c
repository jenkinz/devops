// Misra C Enforcement Testing
//
// Rule 86: Advisory
// If a function returns error information, then that error
// information shall be tested.
///

//
// Not statically enforceable. The method of testing is not
// sufficiently well-defined.
///

int f86() ;	// here prototype indicates that return contains data that must be tested

int m86() {

	int x;

	x = f86();	// RULE 86

	x++;		// bad, x not tested before using x 

	x = f86();

	if ( x > 0 ) x++;		// good
}
