// Misra C Enforcement Testing
//
// Rule 31: Required
// Braces shall be used to indicate and match the structure in the
// non-zero initialisation of arrays and structures.
///

#include "misra.h"

static SI_32 x[] = {
    1,2,3 
}; 

static FL_32 y[4][3] = // RULE 31 
{
    
    {
        1, 3, 5 
    },
    {
        2, 4, 6 
    },
    {
        3, 5, 7 
    },
    
};

static FL_32 yy[4][3] = // RULE 31 
{
    
    1, 3, 5, 2, 4, 6, 3, 5, 7
    
};

static FL_32 z[4][3] = // RULE 31 
{
    
    {
        1 
    }, {
        2 
    }, {
        3 
    }, {
        4 
    }
    
};

static struct // RULE 31 
{
    
int a[3], b;

} w[] = {
    {
        1 
    }, 2 
};

static SI_16 q[4][3][2] = // RULE 31 
{
    
    {
        1 
    },
    {
        2, 3 
    },
    {
        4, 5, 6 
    }
    
};

static SI_16 qq[4][3][2] = // RULE 31 
{
    
    1, 0, 0, 0, 0, 0,
    2, 3, 0, 0, 0, 0,
    4, 5, 6, 0, 0, 0
    
};

static SI_16 qqq[4][3][2] = 
{
    
    {
        
        {
            1 
        },
        
    },
    {
        
        {
            2, 3 
        },
        
    },
    {
        
        {
            4, 5 
        },
        {
            6 
        },
        
    }
    
};

static SI_16 qqqq[4][3][2] = // RULE31 
{
    
    1, 0, 0, 0, 0, 0, 0,
    2, 3, 0, 0, 0, 0,
    4, 5, 6, 0, 0, 0
    
};
