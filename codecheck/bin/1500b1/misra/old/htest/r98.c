// Misra C Enforcement Testing
//
// Rule 98: Required
// At most one of # or ## in a single macro definition
///

#include "misra.h"

#define CONCAT2 ( x,y ) ( x ## y ) 
#define CONCAT3 ( x,y,z ) ( x ## y ## z ) // RULE 98 
#define STRINGS ( x,y ) #x ## #y // RULE 98 

void
func98 ( void ) 
{
    
SC_8 msg1[] = CONCAT2 ( "Hello", "World" ) ;
//SC_8 msg2[] = STRINGS ( Hello, World ) ;
SI_32 i = CONCAT3 ( 1,2,3 ) ;
SI_32 j = CONCAT2 ( 1,2 ) ;
    
}
