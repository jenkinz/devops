// Misra C Enforcement Testing
//
// Rule 55: Advisory
// Labels should not be used, except in switch statements.
///

#include "misra.h"

SI_32
rule55 ( void ) 
{
    
SI_32 c = 3;
SI_32 i = 3;
    
    switch ( i ) 
    {
        
        case 1: 
        case 2: 
        case 3: 
        break;
        
        default: 
        goto blob; // RULE 56 
        break;
        
    }
    
    blob: // RULE 55 
    
    return c;
    
}
