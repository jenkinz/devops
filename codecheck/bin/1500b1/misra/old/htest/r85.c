// Misra C Enforcement Testing
//
// Rule 85: Advisory
// Functions called with no parameters should have empty parentheses.
///

#include "misra.h"

static int func85a ( void ) 
{
    
    return 1;
    
}

int func85b ( void ) 
{
    
int a = 0;
    
    if ( func85a ) // RULE 85 
    {
        
        a = 1;
        
    }
    
    if ( func85a (  ) ) 
    {
        
        a = 1;
        
    }
    
    return a;
    
}
