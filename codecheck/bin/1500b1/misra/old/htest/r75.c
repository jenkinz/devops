// Misra C Enforcement Testing
//
// Rule 75: Required
// Every function shall have an explicit return type.
///

#include "misra.h"

static void 
func75a ( void ) 
{
    
    
}

func75b ( void ) // RULE 75 
{
    
    return 1;
    
}
