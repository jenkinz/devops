// Misra C Enforcement Testing
//
// Rule 10: Advisory
// Code shall not be commented out.
//
// Not automatically enforceable. Requires manual inspection techniques.
///

static int dummy;	// this is ok

/*
 *	Rule 10:	Advisory
 *			
 *	Code shall not be commented out.
 *
 *	Not automatically enforceable.  Requires manual inspection techniques.
*/ 


/* int foo; */

/*

main() {

int important;

return;


*/