// Misra C Enforcement Testing
//
// Rule 94: Required
// A function-like macro shall not be 'called' without all its
// arguments.
///

#define DIV ( x,y ) ((x)/(y))

void
func94a ( void ) 
{
    
int i = DIV ( 1,3 ) ; 
    
int j = DIV ( 1 ) / 3; // RULE 94 
    
}
