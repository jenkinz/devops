// Misra C Enforcement Testing
//
// Rule 15: Advisory
// Floating point implementations should comply with a defined
// floating point standard.
//
// Not automatically enforceable. Requires use of packages such as
// paranoia.
///

float foo;	// bad

typedef float FLOAT;	// float's must have a defined type	IEEE ...

FLOAT foo;	// good

