// Misra C Enforcement Testing
//
// Rule 39: Required
// The unary minus operator shall not be applied to an unsigned
// expression.
///

#include "misra.h"

SI_32
rule39 ( void ) 
{
    
UI_32 a = 3;
SI_64 b = 3;
UI_64 d = 3;
UI_16 e = 3;
    
SI_32 c = 3;
    
    if ( sizeof ( short ) == sizeof ( int ) ) 
    {
        
        a = - e; 
        
    }
    else if ( sizeof ( unsigned int ) == sizeof ( long ) ) 
    {
        
        d = - ( a + b ) ; // RULE 39 
        
    }
    else
    {
        
        //
        // error - Test failed.
        ///
        
    }
    
    return c;
    
}
