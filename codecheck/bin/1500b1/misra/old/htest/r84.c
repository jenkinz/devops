// Misra C Enforcement Testing
//
// Rule 84: Required
// For functions with void return type, return statements shall not
// have an expression.
///

#include "misra.h"

void func84a ( void ) 
{
    
SC_8 pc = '\0';
    
    return pc; // RULE 84 
    
}

void func84b ( void ) 
{
    
SC_8 pc = '\0';
    
    return; 
    
}
