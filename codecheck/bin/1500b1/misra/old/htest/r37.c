// Misra C Enforcement Testing
//
// Rule 37: Required
// Bitwise operations shall not be performed on signed integer types.
///

#include "misra.h"

SI_32
rule37 ( void ) 
{
    
SI_32 a = 3;
SI_32 b = 3;
SI_32 c = 3;
    
    c = a >> 2; // RULE 37 
    c = a << 2; // RULE 37 
    c = a & b; // RULE 37 
    c = a | b; // RULE 37 
    c = a ^ b; // RULE 37 
    c = ~a; // RULE 37 
    
    return c;
    
}
