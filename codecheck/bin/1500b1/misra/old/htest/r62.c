// Misra C Enforcement Testing
//
// Rule 62: Required
// All switch statements should contain a final default clause.
///

#include "misra.h"

SI_32
rule62 ( void ) 
{
    
SI_32 c = 3;
SI_32 i = 3;
    
    switch ( i ) // RULE 62 
    {
        
        case 3:
        case 4: c = 4; break;
        
    }
    
    switch ( i ) 
    {
        
        case 3:
        default: break; // RULE 62 
        case 4: c = 4; break;
        
    }
    
    switch ( i ) 
    {
        
        case 3:
        case 4: c = 4; break;
        default: break; 
        
    }
    
    return c;
    
}
