// Misra C Enforcement Testing
//
// Rule 107: Required
// The NULL pointer shall not be dereferenced.
//
// This is not statically detectable in all cases.
///

#include "misra.h"

static void
func107 ( void ) 
{
    
int k;
int *pi = ( void * ) 0;
    
    k = ( *pi ) ; // RULE 107 
    
    pi = ( &k ) ;
    
    k = ( *pi ) ; 
    
}
