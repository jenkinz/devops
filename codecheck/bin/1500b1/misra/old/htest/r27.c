// Misra C Enforcement Testing
//
// Rule 27: Advisory
// Objects should be declared as extern in only one file.
///

#include "misra.h"
//
// xi is also declared extern in misra_r26.c
///
extern SI_32 xi; // RULE 27 
