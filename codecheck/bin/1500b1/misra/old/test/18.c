 
unsigned  int     ui      =       1234;           /*      RULE    18 */
 unsigned  int     ui1     =       1234u;          /*      OK */
 signed    long    sl      =       1234;           /*      RULE    18 */
 signed    long    sl1     =       1234l;          /*      RULE 18 (L not l)       */
 signed    long    sl2     =       1234L;          /*      OK*/
 unsigned  long    ul      =       1234;           /*      RULE    18 */
 unsigned  long    ul1     =       1234u;          //*      RULE 18
 unsigned  long    ul2     =       1234L;          //*      RULE 18
 unsigned  long    ul3     =       1234lu;         /*      RULE 18 (L not l)       */
float           fl2     =       0.1234F;                //*      OK
double          dl      =       0.1234;         /*      OK                              */
double          dl1     =       0.1234L;                /*      RULE 18                 */
long double     ldl     =       0.1234;         /*      RULE    18                      */
