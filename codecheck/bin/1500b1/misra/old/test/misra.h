#ifndef	MISRA_H
#define	MISRA_H

typedef	signed char		SC_8;
typedef	unsigned char		UC_8;
typedef	signed short		SI_16;
typedef	unsigned short		UI_16;
typedef	signed int		SI_32;
typedef	unsigned int		UI_32;
typedef	signed long		SI_64;
typedef	unsigned long		UI_64;
typedef	float 			FL_32;
typedef	double 			FL_64;

typedef char	wchar_t ;

#endif
