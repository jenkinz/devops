// Misra C Enforcement Testing
//
// Rule 16: Required
// Underlying floating point bit representations should not be used.
//
// Not automatically enforceable.
///

// In this test case explicitly assumes that the high bit of the floating point number
// is set when negative, these types of assumptions are not to be used.


float f16;   // define a float

m16() {

    f16 = -1;     // create a negative

    if ( f16 & 0xf000 ) {
        // ... bad
    }
}