
/*
 *	Rule 98:	Required
 *			
 *	At most one of # or ## in a single macro definition
*/ 

#include "misra.cch"


int ppStr;

if ( lin_end  ) {

	ppStr = 0;
}

if ( pp_paste ) { 

	ppStr++; 
	if ( ppStr > 1 ) {
		WARN_MR( 98, "At most one of # or #\# in a single macro definition.");
	}
}

if ( pp_stringize ) { 

 	ppStr++; 

	if ( ppStr > 1 ) {

		WARN_MR( 98, "At most one of # or #\# in a single macro definition.");
	}
}
