/*
 *	Rule 12:	Advisory
 *			
 *	Identifiers in different name spaces shall have different spelling.
*/ 

#include "misra.cch"

if ( dcl_variable ) {
//	warn( 12, "DV db%d root%d dbr%d", dcl_base, find_root(dcl_name()), dcl_base_root );

	temp = find_root(dcl_name()) ;
	if (  temp > 0  && temp != dcl_base_root ) {
		WARN_MA( 12, "Identifiers in different name spaces shall have different spelling. { Variable }" );
	}
}

if ( dcl_conflict ) {

	WARN_MA( 12, "Identifiers in different name spaces shall have different spelling. { Conflict }" );
}

if ( dcl_hidden ) {

	WARN_MA( 12, "Identifiers in different name spaces shall have different spelling. { Hidden  }" );
}

if ( dcl_parm_hidden ) {

	WARN_MA( 12, "Identifiers in different name spaces shall have different spelling. { Parm Hidden }" );
}

if ( dcl_enum_hidden ) {

	WARN_MA( 12, "Identifiers in different name spaces shall have different spelling. { Enum Hidden }" );
}

if ( dcl_typedef_dup ) {

	WARN_MA( 12, "Identifiers in different name spaces shall have different spelling. { typedef duplicate }" );
}


if ( tag_hidden ) {

	WARN_MA( 12, "Identifiers in different name spaces shall have different spelling. { Tag }" );
}

