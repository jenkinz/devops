// COPYRIGHT (C) 2006 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.  



/*
 *	Rule 47:	Required
 *			
 *	No dependence should be placed on C's operator precedence rules in
 *	expressions.
*/ 

// This rule requires -c option ON, Full Type Checking

#include "misra.cch"



if ( op_assign ) {

	if ( (binary_exp > 2) && in_paren == 0 ) {

		WARN_MR( 47, "No dependence should be placed on C's operator precedence" );
	}
}