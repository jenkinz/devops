/*
 *	Rule 40:	Advisory
 *			
 *	The sizeof operator should not be used on expressions that contain
 *	side effects.
*/ 

#include "misra.cch"

int sizeof_side_effect;

if ( op_postfix ) {

	sizeof_side_effect=1;

}


if ( op_sizeof ) {

	if ( sizeof_side_effect ) {
		WARN_MA( 40, "sizeof operator should not be used on side-effects" );
	}

	sizeof_side_effect = 0;
}