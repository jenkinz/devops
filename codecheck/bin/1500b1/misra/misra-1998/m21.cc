
/*
 *	Rule 21:	Required
 *			
 *	Identifiers in an inner scope shall not mask identifiers in an
 *	outer scope.
*/ 

if ( dcl_hidden ) {

	WARN_MR(21,"Identifiers in an inner scope shall not mask identifiers in an outer.");
}