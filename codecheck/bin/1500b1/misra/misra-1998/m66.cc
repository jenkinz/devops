/*
 *	Rule 66:	Advisory
 *			
 *	Only expressions concerned with loop control should appear within a
 *	for statement.
 *
 *	This is in general difficult to detect automatically and statically.
 *	The following gives a simple case.
*/ 

#include "misra.cch"

int for_assign_count;

if ( op_for ) for_assign_count = 0;

if ( op_assign ) {
//	warn( 66, "ASS for%d par%d", inforloop, in_paren );

	if ( inforloop  ) for_assign_count++;

	if ( inforloop && for_assign_count > 1 ) {
		WARN_MA( 66, "Only expressions concerned with loop control should appear { ASSIGN }.");
	}
}

if ( op_comma ) {
	//warn( 66, "OC for%d par%d", inforloop, in_paren );

//	if ( inforloop && in_paren ) for_assign_count++;

	if ( inforloop  ) {
		WARN_MA( 66, "Only expressions concerned with loop control should appear. { COMMA }");
	}
}

