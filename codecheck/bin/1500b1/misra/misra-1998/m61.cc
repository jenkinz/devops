/*
 *	Rule 61:	Required
 *			
 *	Every non-empty case clause in a switch statement shall be
 *	terminated with a break statement.
*/ 

#include "misra.cch"

if (stm_no_break) {

	WARN_MR( 61, "Every non-empty case in switch must have break.");
}
