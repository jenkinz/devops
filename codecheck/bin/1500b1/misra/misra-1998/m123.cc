/*
 *	Rule 123:	Required
 *			
 *	The signal handling facilities of <signal.h> shall not be used
*/ 

if ( lex_macro ) {

	if ( CMPTOK("SIGINT") ) {
		
		WARN_MR( 123, "The signal handling facilities of shall not be used." );
	}

}