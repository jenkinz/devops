// Misra C Enforcement Testing
// Rule 51: Advisory
// Evaluation of constant unsigned integer expressions should not lead
// to wrap-around.


#include "misra.cch"


int seen_unsigned;

if ( lex_unsigned ) {
	seen_unsigned++;
}

if ( pp_if_depth ) {

	if ( seen_unsigned ) {

		WARN_MA( 51, "Evaluation of constant unsigned integer expressions." );
	}
}