/*
 *	Rule 114:	Required
 *				
 *	Reserved words and standard library function names shall not be
 *	redefined or undefined.
 *
 *	Note the interaction with rule 92 on #undef.
 *	We do not look for reserved words here as most occurrences lead to
 *	syntax error.
*/ 

#include "misra.cch"

if ( dcl_conflict ) {

	if ( dcl_variable ) {
		WARN_MR( 114, "Reserved words and standard library function names shall not be used { DECL }" )
	}
}

if ( pp_macro_conflict ) {

	WARN_MR( 114, "Reserved words and standard library function names shall not be used { MACRO }" )

}