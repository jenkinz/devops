/*
 *	Rule 41:	Advisory
 *			
 *	The implementation of integer division in the chosen compiler
 *	should be determined, documented and taken into account.
 *
 *	This is not enforceable but there are some troublesome cases which
 *	are worth flagging if the static analyser is sufficiently
 *	sophisticated.
*/ 

#include "misra.cch"


	if ( op_div_assign || op_rem_assign ) {

		WARN_MA( 41, "implementation of integer division" );
	}