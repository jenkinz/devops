
/*
 *	Rule 63:	Advisory
 *			
 *	A switch expression should not represent a Boolean value.
*/ 

#include "misra.cch"

if ( stm_relation ) {

	if ( stm_kind == SWITCH ) {
		WARN_MA(63, "A switch expression should not represent a Boolean value.");

	}
}