/*
 *	Rule 58:	Required
 *			
 *	The break statement shall not be used (except to terminate the
 *	cases of a switch statement).
*/ 

#include "misra.cch"


int in_iter;

if (op_do || op_for || op_while_1 || op_while_2 ) in_iter = 1;
if (  op_switch || op_if ) in_iter = 0;

if ( op_break ) {

	if ( in_iter ) {

		WARN_MA( 58, "Break statement shall not be used, except in switch.");
	}

}