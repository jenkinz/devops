/*
 *	Rule 127:	Required
 *			
 *	The time handling functions of <time.h> shall not be used.
*/ 

#include "misra.cch"


if ( idn_function ) {

	if ( CMPPREVTOK("clock")||CMPPREVTOK("time")||CMPPREVTOK("difftime")||CMPPREVTOK("mktime")
		||CMPPREVTOK("asctime")||CMPPREVTOK("ctime")||CMPPREVTOK("gmtime") 
		||CMPPREVTOK("localtime")||CMPPREVTOK("strftime")  ) {
	
		WARN_MR( 127, "Time handling functions of <time.h> shall not be used" );
	}
}