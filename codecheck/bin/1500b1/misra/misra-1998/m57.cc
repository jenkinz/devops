/*
 *	Rule 57:	Required
 *			
 *	The continue statement shall not be used.
*/ 

#include "misra.cch"

if ( op_continue ) {

	WARN_MR( 57, "The continue statement shall not be used.");
}