/*
 *	Rule 87:	Required
 *			
 *	#include statements in a file shall only be preceded by other
 *	pre-processor directives or comments.
*/ 

#include "misra.cch"

int varCount;

if ( mod_begin ) varCount = 0;

if ( dcl_variable ) {

	if ( lin_source ) varCount++;	// only count source file decls, not headers

}

if ( pp_include ) {


	if ( varCount > 0 ) {

	  WARN_MR( 87, "#include statements only be preceded by pre-processor directives.");
	}


}