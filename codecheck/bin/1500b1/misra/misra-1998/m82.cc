/*
 *	Rule 82:	Advisory
 *			
 *	A function should have a single point of exit.
*/ 

#include "misra.cch"

int returnCount;

if ( fcn_begin ) returnCount = 0;

if ( op_return ) returnCount++;

if ( fcn_end ) {

	if ( returnCount > 1 ) {


		WARN_MA( 82, "A function should have a single point of exit." );
	}
}
