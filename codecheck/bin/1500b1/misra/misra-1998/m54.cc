/*
 *	Rule 54:	Required
 *			
 *	A null statement shall only occur on a line by itself, and shall
 *	not have any other text on the same line.
*/ 

#include "misra.cch"

if ( stm_semicolon ) {

	WARN_MR( 54, "Null statement shall only occur on a line by itself." );
}