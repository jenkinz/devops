/*
 *	Rule 65:	Required
 *			
 *	Floating point variables shall not be used as loop counters.
*/ 

// inforloop is defined [ misra.cch ] also used by rule 42 

#include "misra.cch"


if ( op_assign ) {
 if ( inforloop && (op_base(1) == FLOAT_TYPE || op_base(1)==DOUBLE_TYPE) ) {

	WARN_MR(65, "Floating point variables shall not be used as loop counters.");

 }
}