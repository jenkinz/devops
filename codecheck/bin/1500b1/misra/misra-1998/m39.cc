/*
 *	Rule 39:	Required
 *			
 *	The unary minus operator shall not be applied to an unsigned
 *	expression.
*/ 

#include "misra.cch"

if ( op_negate ) {

   if ( strcmp( prev_token(), ")" ) == 0 ) {
	 if ( (op_base(1) >= UCHAR_TYPE) && (op_base(1) <= ULONG_TYPE) ) {
		WARN_MR( 39, "Unary minus shall not be applied to unsigned expr" )
	 }
   }
}