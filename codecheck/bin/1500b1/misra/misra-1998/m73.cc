/*
 *	Rule 73:	Required
 *			
 *	Identifiers shall be given either for all of the parameters in a
 *	function prototype declaration or for none.
*/ 

#include "misra.cch"

int parm_id;

if ( dcl_parameter ) {

	if ( dcl_parameter > 1 ) {

		if ( parm_id != ((dcl_name()[0])>0) ) {

			WARN_MR(73, "Identifiers shall be given for all parameters." );
		}
	}

	if ( (dcl_name()[0]) > 0 ) 
		parm_id = 1;
	else 
		parm_id = 0;


}