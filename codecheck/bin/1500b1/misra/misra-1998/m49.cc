// Misra C Enforcement Testing
//
// Rule 49: Advisory
// Tests of a value against zero should be made explicit, unless the
// operator is effectively Boolean.
//
// Not automatically detectable statically in all cases. Tools can
// either warn on all or no occurrences in general.
///

#include "misra.cch"

if ( idn_variable ) {

	//warn( 49, "IV if%d tok=%s next=%s", chkexp, token(), next_token() );
	
	if ( next_token() && chkexp ) {
		if ( CMPTOK("==") == 0 ) {
			WARN_MA( 49, "Tests of a value against zero should be made explicit" );
		}
	}
}

