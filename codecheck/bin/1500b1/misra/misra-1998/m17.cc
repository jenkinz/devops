/*
 *	Rule 17:	Required
 *			
 *	typedef names shall not be reused.
*/ 

#include "misra.cch"

if ( dcl_typedef_dup ) {
//warn( 17, "DT %s [%s] dtd%d db%d", dcl_name(), dcl_base_name(), find_root( dcl_name() ), dcl_base );
	WARN_MR( 17, "typedef names shall not be reused." );
}