/*
 *	Rule 38:	Required
 *			
 *	The right hand operand of a shift operator shall lie between zero
 *	and one less than the width in bits of the left hand operand
 *	(inclusive).
*/ 

#include "misra.cch"


int curConstant;

if (  op_right_shift || op_left_shift ) {

	
	if ( ( curConstant < 0 || curConstant > 31 )  ) {


	  WARN_MR( 38, "Right hand operand shift operator shall lie between 0 & 31.");
	}
	
}


if ( lex_constant ) {

	if ( lex_constant == CONST_INTEGER ) {

		curConstant = atoi(token());

		if ( CMPPREVTOK( "-" ) ) curConstant = (-curConstant);

	}

	else {

		curConstant = -1;
	}
}