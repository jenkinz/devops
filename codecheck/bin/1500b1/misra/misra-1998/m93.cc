/*
 *	Rule 93:	Advisory
 *			
 *	A function should be used in preference to a function-like macro
*/ 

#include "misra.cch"

if ( pp_arg_count > 0 && pp_lowercase ) {

	WARN( 93, "A function should be used in preference to a function-like macro" )
}