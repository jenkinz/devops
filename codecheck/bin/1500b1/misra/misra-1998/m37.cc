/*
 *	Rule 37:	Required
 *			
 *	Bitwise operations shall not be performed on signed integer types.
*/ 

#include "misra.cch"


if ( op_bitwise || op_right_shift || op_left_shift ) {

	if ( op_base(1) == INT_TYPE ) {
		
	  WARN_MR( 37, "Bitwise operations shall not be performed on signed integer types.");
	}
}
