/*
 *	Rule 25:	Required
 *			
 *	An identifier  with external linkage shall have exactly one
 *	external definition.
*/ 

if ( dcl_conflict ) {

       WARN_MR( 25, "An identifier with external linkage shall have exactly one definition." );
}