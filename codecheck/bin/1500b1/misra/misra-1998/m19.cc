/*
 *	Rule 19:	Required
 *			
 *	Octal constants (other than zero) shall not be used.
*/ 

#include "misra.cch"

if ( lex_radix ) { // check if octal and value > 0

	if ( lex_radix == 8 && strlen(token()) > 1 ) {

		if ( (atoi(token()+1)) > 0 ) {
			WARN_MA( 19, "Octal constants (other than zero) shall not be used." )
		}
	}
}
