/*
 *	Rule 13:	Advisory
 *			
 *	The basic types char, short, int, long, float and double should not
 *	be used.
*/ 

#include "misra.cch"


if ( dcl_local ) {

	switch ( dcl_base ) {

			case 	CHAR_TYPE:
			case 	SHORT_TYPE:
			case	INT_TYPE:
			case	LONG_TYPE:
			case	FLOAT_TYPE:
			case	DOUBLE_TYPE:
			
		WARN_MA( 13, "types char, short, int, long, float, double should not be used" );
				break;
	}
}