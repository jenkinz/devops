/*
 *	Rule 104:	Required
 *			
 *	Non-constant pointers to functions shall not be used.
*/ 

#include "misra.cch"

if ( op_cast_to_ptr ) {

	if ( idn_level(0) == FUNCTION ) {

		WARN_MR( 104, "Non-constant pointers to functions shall not be used. ");
	}

}