/*
 *	Rule 92:	Advisory
 *			
 *	#undef should not be used.
*/ 

#include "misra.cch"

if ( lin_preprocessor ) {

	if ( lin_preprocessor == UNDEF_PP_LIN ) {

		WARN_MA( 92, "#undef should not be used." );
	}
}