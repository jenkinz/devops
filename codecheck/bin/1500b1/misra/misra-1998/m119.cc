
/*
 *	Rule 119:	Required
 *			
 *	The error indicator errno shall not be used.
*/ 

	if ( header_name() ) {		// at #include

		if ( strcmp(header_name(), "errno.h" ) == 0 ) {
			WARN_MR( 119, "The error indicator errno shall not be used." );
		}
	}

	if ( idn_global ) {

		if ( idn_name() && strcmp(idn_name(), "errno" ) == 0 ) {
			WARN_MR( 119, "The error indicator errno shall not be used." );
		}
	}