/*
 *	Rule 110:	Required
 *				
 *	Unions shall not be used to access the sub-parts of larger data
 *	types.
 *
 *	This seems slightly odd as Rule 109 effectively precludes the use
 *	of unions at all.
*/ 

#include "misra.cch"


if ( dcl_variable ) {

	if ( dcl_base_root == UNION_TYPE ) {

		WARN_MR(110,"Unions shall not be used to access sub-parts of larger data.");
	}
}