/*
 *	Rule 122:	Required
 *			
 *	The setjmp macro and the longjmp function shall not be used.
*/ 

if ( idn_function ) {

	if ( strcmp(idn_name(), "setjmp" ) == 0	|| strcmp(idn_name(), "longjmp" ) == 0 ) {

		WARN_MR( 122, "The setjmp macro and the longjmp function shall not be used." );
	}
}