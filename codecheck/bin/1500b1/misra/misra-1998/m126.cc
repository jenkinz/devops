/*
 *	Rule 126:	Required
 *			
 *	The library functions abort, exit, getenv and system from
 *	<stdlib.h> shall not be used.
 *	
*/ 

#include "misra.cch"

if ( idn_function ) {

	if ( CMPPREVTOK("abort")||CMPPREVTOK("exit")||CMPPREVTOK("getenv")||CMPPREVTOK("system") ) {
	
		WARN_MR( 126, "Library functions abort, exit, getenv and system should not be used." );
	}
}