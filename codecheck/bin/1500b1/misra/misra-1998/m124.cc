/*
 *	Rule 124:	Required
 *			
 *	The input/output header file <stdio.h> must not be used.
 *
 *	(The standards says library, but this is a header file containing
 *	references to i/o functions amongst other things.)
*/ 

if ( header_name() ) {		// at #include

		if ( strcmp(header_name(), "stdio.h" ) == 0 ) {
			WARN_MR( 124, "The input/output header file <stdio.h> must not be used." );
		}
	}