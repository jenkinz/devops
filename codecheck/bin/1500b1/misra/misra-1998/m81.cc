/*
 *	Rule 81:	Advisory
 *			
 *	const qualification should be used on function parameters which are
 *	passed by reference, where it is intended that the function will
 *	not modify the parameter.
*/ 

#include "misra.cch"

if ( dcl_parameter ) {

	if ( dcl_level(0) == POINTER && (dcl_level_flags(dcl_levels) & CONST_FLAG) == 0 ) {

		WARN_MA( 81, "const should be used on function reference parameters" );
	}

}