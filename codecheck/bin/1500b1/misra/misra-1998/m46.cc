// COPYRIGHT (C) 2006 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.  


/*
 *	Rule 46:	Required
 *			
 *	The value of an expression shall be the same under any order of
 *	evaluation that the standard permits.
*/ 

#include "misra.cch"

int line_side_effect;

if ( op_postfix || op_prefix ) {

	++line_side_effect;
}


if ( lin_end ) {

	if ( line_side_effect > 1 ) {
		WARN_MR( 46, "value of an expression shall be the same under any order" );
	}

	line_side_effect = 0;
}