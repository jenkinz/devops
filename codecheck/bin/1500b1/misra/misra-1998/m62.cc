/*
 *	Rule 62:	Required
 *			
 *	All switch statements should contain a final default clause.
*/ 

#include "misra.cch"

int seen_default;	// keep track of last default seen for second case of this rule

if ( stm_no_default ) {	// std default not in switch

	WARN_MR( 62, "All switch statements should contain a final default clause.");
}

if ( keyword("default") ) {
	seen_default = 1;
}

if (stm_switch_cases) {
	seen_default = 0;

}

if ( keyword("case") ) {	// there is a case after a default

	if ( seen_default ) {

		WARN_MR( 62, "All switch statements should contain a final default clause.")
	}
}