/*
 *	Rule 50:	Required
 *			
 *	Floating point variables shall not be tested for exact equality or
 *	inequality.
*/ 

#include "misra.cch"

if ( op_equal || op_not_eq ) {

	if ( op_base(1) == FLOAT_TYPE || op_base(2)==FLOAT_TYPE ) {
		WARN_MR(50, "Floating point variables shall not be tested for exact equality");
	}

}