/*
 *	Rule 43:	Required
 *			
 *	Implicit conversions which may result in a loss of information
 *	shall not be used.
*/ 

// This Rule Requires -C type checking "ON" to be invoked.

#include "misra.cch"

if ( cnv_truncate ) {

	WARN_MR( 43, "Implicit conversions which may result in a loss of information" );
}
