/*
 *	Rule 109:	Required
 *			
 *	Overlapping variable storage shall not be used.
*/ 


#include "misra.cch"

if ( tag_begin ) {

	if ( tag_kind == UNION_TAG ) {

		WARN_MR(109, "Overlapping variable storage shall not be used.");
	}
}