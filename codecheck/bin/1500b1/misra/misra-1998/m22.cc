/*
 *	Rule 22:	Advisory
 *			
 *	Declarations should be at function scope unless a wider scope is
 *	necessary.
 *
 *	This rule is incorrectly worded, only labels have function scope in
 *	C.  It is unenforceable in its present form.
*/ 

#include "misra.cch"

if ( dcl_variable ) {

	if ( dcl_global ) {
		WARN_MA( 22, "Declarations should be at function scope" );
	}
}
