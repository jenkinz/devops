/*
 *	Rule 56:	Required
 *			
 *	The goto statement shall not be used.
*/ 

#include "misra.cch"

if ( op_goto ) {

	WARN_MR( 56, "The goto statement shall not be used.");
}