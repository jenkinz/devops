/*
 *	Rule 113:	Required
 *				
 *	All the members of a structure (or union) shall be named and shall
 *	only be accessed via their name.
*/ 

#include "misra.cch"

if ( dcl_member ) {

	if ( dcl_ident_length == 0 ) {

		WARN( 113, "members of a structure (or union) shall be named" )
	}
}