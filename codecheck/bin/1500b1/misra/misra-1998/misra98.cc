// Example implementation of Misra Coding Standard

// Abraxas - MISRA 1998 Checking Tools [ We also have MISRA 2004 by Request ]

// COPYRIGHT (C) 2006 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.  

#include <misra.cch>

/*
 *	Rule 1:		Required
 *			
 *	ISO 9899 conformance.
 *
 *	This requires a toolset to be run against the FIPS 160 compiler
 *	validation suite or equivalent.
*/ 

	if ( prj_begin ) {
	
// THIS SHOULD BE ENABLED FOR MISRA TESTING - When USING REAL ANSI-C compiler, e.g. NOT MICROSOFT ...

	//	set_option( 'k', 1 );	// for strict ansi-c 

		set_option( 'c', 1 );	// turn on full type check info
	}
/*
 *	Rule 2:		Advisory
 *			
 *	Interfaces to other languages.
 *
 *	Not automatically enforceable.  Requires manual inspection techniques.
*/ 

/*
 *	Rule 3:	Advisory
 *			
 *	Interfaces to other assembly languages.
 *
 *	Inline assembly language violates Rule 1.
 *
 *	Calling a function is not automatically enforceable and
 *	requires manual inspection techniques.
*/ 

/*
 *	Rule 4:	Advisory
 *			
 *	Provision should be made for run-time checking.
 *
 *	Not automatically enforceable.  Requires manual inspection techniques.
*/ 

// INFORMATIONAL Rule 04

/*
CodeCheck can accomplish 'run-time' checking, but we need a sample of code to know what we're looking for,
This rule should be considered 'undefined' at this time.
*/

/*
 *	Rule 5:	Required
 *			
 *	Non ISO C characters.
*/ 

#include "m05.cc"

/*
 *	Rule 6:	Required
 *			
 *	ISO 10646-1 conformance.
 *
 *	Not automatically enforceable.  Requires manual inspection techniques.
*/ 

// basically ansi char strings ... codecheck would catch non-ansi in the C/C++

/*
 *	Rule 7:	Required
 *			
 *	No trigraphs
*/ 

#include "m7.cc"

/*
 *	Rule 8:	Required
 *			
 *	No multibyte characters or wide string literals
*/ 

// wide char check, L"string"

/*
 *	Rule 9:		Required
 *			
 *	No nested comments
*/ 

// rule 9 comment in comment, supported by codecheck -N option

#include "m09.cc"
/*
 *	Rule 10:	Advisory
 *			
 *	Code shall not be commented out.
 *
 *	Not automatically enforceable.  Requires manual inspection techniques.
*/ 

// interesting, could be done with two pass



/*
 *	Rule 13:	Advisory
 *			
 *	The basic types char, short, int, long, float and double should not
 *	be used.
*/ 

#include "m13.cc"


/*
 *	Rule 14:	Required
 *			
 *	char should only be used with the signed or unsigned keyword.
*/ 

#include "m14.cc"

/*
 *	Rule 15:	Advisory
 *			
 *	Floating point implementations should comply with a defined
 *	floating point standard.
 *
 *	Not automatically enforceable.  Requires use of packages such as
 *	paranoia.
*/ 

// need better definition of problem

/*
 *	Rule 16:	Required
 *			
 *	Underlying floating point bit representations should not be used.
 *
 *	Not automatically enforceable.
*/ 

#include "m16.cc"

/*
 *	Rule 17:	Required
 *			
 *	typedef names shall not be reused.
*/ 

#include "m17.cc"

/*
 *	Rule 18:	Advisory
 *			
 *	Numeric constants should be suffixed to indicate type if an
 *	appropriate suffix is available.
*/ 

#include "m18.cc"

/*
 *	Rule 19:	Required
 *			
 *	Octal constants (other than zero) shall not be used.
*/ 


#include "m19.cc"



// This rule isn't defined as an objective rule.

/*
 *	Rule 20:	Required
 *			
 *	All object and function identifiers shall be declared before use.
*/ 

#include "m20.cc"

/*
 *	Rule 21:	Required
 *			
 *	Identifiers in an inner scope shall not mask identifiers in an
 *	outer scope.
*/ 

#include "m21.cc"

/*
 *	Rule 22:	Advisory
 *			
 *	Declarations should be at function scope unless a wider scope is
 *	necessary.
 *
 *	This rule is incorrectly worded, only labels have function scope in
 *	C.  It is unenforceable in its present form.
*/ 

#include "m22.cc"

/*
 *	Rule 23:	Advisory
 *			
 *	All declarations at file scope should be static where possible.
*/ 

#include "m23.cc"

/*
 *	Rule 24:	Required
 *			
 *	Identifiers shall not have internal and external linkage
 *	simultaneously in the same translation unit.
*/ 

#include "m24.cc"

/*
 *	Rule 25:	Required
 *			
 *	An identifier  with external linkage shall have exactly one
 *	external definition.
*/ 

#include "m25.cc"

/*
 *	Rule 26:	Required
 *			
 *	If objects are declared more than once they shall be declared
 *	compatibly.
*/ 


/*
 *	Rule 27:	Advisory
 *			
 *	Objects should be declared as extern in only one file.
*/ 


/*
 *	Rule 28:	Required
 *			
 *	The register storage class should not be used.
*/ 


/*
 *	Rule 29:	Required
 *			
 *	The use of a tag should be consistent with the declaration.
 *
 *	This is covered by Rule 1.
*/ 





/*
 *	Rule 30:	Required
 *			
 *	All automatic variables shall have been assigned a value before
 *	being used.
 *
 *	This is not automatically detectable statically in all cases but
 *	the following should exercise a tool.
*/ 

#include "m30.cc"

/*
 *	Rule 31:	Required
 *			
 *	Braces shall be used to indicate and match the structure in the
 *	non-zero initialisation of arrays and structures.
*/ 

#include "m31.cc"


/*
 *	Rule 32:	Required
 *			
 *	In an enumerator list, the '=' construct shall not be used to
 *	explicitly initialise members other than the first, unless all
 *	items are explicitly initialised.
*/ 

#include "m32.cc"

/*
 *	Rule 33:	Required
 *			
 *	The right hand side of a && or || operator shall not contain
 *	side-effects.
 *
 *	This is not automatically detectable statically in all cases but
 *	the following should exercise a tool.
*/ 

#include "m33.cc"

/*
 *	Rule 34:	Required
 *			
 *	The operands of a logical && or || shall be primary expressions.
*/ 

#include "m34.cc"

/*
 *	Rule 35:	Required
 *			
 *	Assignment operators shall not be used in expressions which return
 *	Boolean values.
*/ 

#include "m35.cc"

/*
 *	Rule 36:	Advisory
 *			
 *	Logical operators should not be confused with bitwise operators.
 *
 *	Not automatically enforceable.  Requires manual inspection techniques.
*/ 

#include "m36.cc"

/*
 *	Rule 37:	Required
 *			
 *	Bitwise operations shall not be performed on signed integer types.
*/ 

#include "m37.cc"

/*
 *	Rule 38:	Required
 *			
 *	The right hand operand of a shift operator shall lie between zero
 *	and one less than the width in bits of the left hand operand
 *	(inclusive).
*/ 

#include "m38.cc"


/*
 *	Rule 39:	Required
 *			
 *	The unary minus operator shall not be applied to an unsigned
 *	expression.
*/ 

#include "m39.cc"



/*
 *	Rule 40:	Advisory
 *			
 *	The sizeof operator should not be used on expressions that contain
 *	side effects.
*/ 

#include "m40.cc"

/*
 *	Rule 41:	Advisory
 *			
 *	The implementation of integer division in the chosen compiler
 *	should be determined, documented and taken into account.
 *
 *	This is not enforceable but there are some troublesome cases which
 *	are worth flagging if the static analyser is sufficiently
 *	sophisticated.
*/ 

#include "m41.cc"

/*
 *	Rule 42:	Required
 *			
 *	The comma operator shall not be used, except in the control
 *	expression of a for loop.
*/ 


#include "m42.cc"

/*
 *	Rule 43:	Required
 *			
 *	Implicit conversions which may result in a loss of information
 *	shall not be used.
*/ 

// This Rule Requires -C type checking "ON" to be invoked.

#include "m43.cc"

/*
 *	Rule 44:	Advisory
 *			
 *	Redundant explicit casts should not be used.
*/ 

#include "m44.cc"

/*
 *	Rule 45:	Required
 *			
 *	Type casting from any type to or from pointers shall not be used.
*/ 

#include "m45.cc"

/*
 *	Rule 46:	Required
 *			
 *	The value of an expression shall be the same under any order of
 *	evaluation that the standard permits.
*/ 

#include "m46.cc"

/*
 *	Rule 47:	Required
 *			
 *	No dependence should be placed on C's operator precedence rules in
 *	expressions.
*/ 

// This rule requires -c option ON, Full Type Checking

#include "m47.cc"

/*
 *	Rule 48:	Advisory
 *			
 *	Mixed precision arithmetic should use explicit casting to generate
 *	the desired result.
 *
 *	Not automatically detectable statically in all cases.  The
 *	following should give an indication.
*/ 

// requires -c option, full type checking

#include "m48.cc"


/*
 *	Rule 49:	Advisory
 *			
 *	Tests of a value against zero should be made explicit, unless the
 *	operator is effectively Boolean.
 *
 *	Not automatically detectable statically in all cases.  Tools can
 *	either warn on all or no occurrences in general.
*/ 

#include "m49.cc"



/*
 *	Rule 50:	Required
 *			
 *	Floating point variables shall not be tested for exact equality or
 *	inequality.
*/ 


#include "m50.cc"

// Misra C Enforcement Testing
// Rule 51: Advisory
// Evaluation of constant unsigned integer expressions should not lead
// to wrap-around.

#include "m51.cc"

/*
 *	Rule 52:	Required
 *			
 *	There shall be no unreachable code.
*/ 

//TODO 
// This is no problem do, but its a whole rule-file within itself, contact abraxas for support

/*
 *	Rule 53:	Required
 *			
 *	All non-null statements shall have a side-effect.
*/ 

#include "m53.cc"

/*
 *	Rule 54:	Required
 *			
 *	A null statement shall only occur on a line by itself, and shall
 *	not have any other text on the same line.
*/ 

#include "m54.cc"


/*
 *	Rule 55:	Advisory
 *			
 *	Labels should not be used, except in switch statements.
*/ 

#include "m55.cc"

/*
 *	Rule 56:	Required
 *			
 *	The goto statement shall not be used.
*/ 

#include "m56.cc"

/*
 *	Rule 57:	Required
 *			
 *	The continue statement shall not be used.
*/ 

#include "m57.cc"


/*
 *	Rule 58:	Required
 *			
 *	The break statement shall not be used (except to terminate the
 *	cases of a switch statement).
*/ 

#include "m58.cc"

/*
 *	Rule 59:	Required
 *			
 *	The statements forming the body of an if, else if, else, while, do
 *	... while or for statement shall always be enclosed in braces.
*/ 

#include "m59.cc"


/*
 *	Rule 60:	Advisory
 *			
 *	All if, else if constructs should contain a final else clause.
*/ 

#include "m60.cc""

/*
 *	Rule 61:	Required
 *			
 *	Every non-empty case clause in a switch statement shall be
 *	terminated with a break statement.
*/ 

#include "m61.cc"

/*
 *	Rule 62:	Required
 *			
 *	All switch statements should contain a final default clause.
*/ 

#include "m62.cc"

/*
 *	Rule 63:	Advisory
 *			
 *	A switch expression should not represent a Boolean value.
*/ 

#include "m63.cc"

/*
 *	Rule 64:	Required
 *			
 *	Every switch statement shall have at least one case.
*/ 

#include "m64.cc"

/*
 *	Rule 65:	Required
 *			
 *	Floating point variables shall not be used as loop counters.
*/ 

// inforloop is defined by rule 42

#include "m65.cc"

/*
 *	Rule 66:	Advisory
 *			
 *	Only expressions concerned with loop control should appear within a
 *	for statement.
 *
 *	This is in general difficult to detect automatically and statically.
 *	The following gives a simple case.
*/ 

#include "m66.cc"

/*
 *	Rule 67:	Advisory
 *			
 *	Numeric variables being used within a for loop for iteration
 *	counting should not be modified in the body of the loop.
 *
 *	This is in general difficult to detect automatically and statically.
 *	The following gives a simple case.
*/ 


#include "m67.cc"

/*
 *	Rule 68:	Required
 *			
 *	Functions shall always be declared at file scope.
*/ 

#include "m68.cc"

/*
 *	Rule 69:	Required
 *			
 *	Functions with variable numbers of arguments shall not be used.
*/ 

#include "m69.cc"



/*
 *	Rule 70:	Required
 *			
 *	Functions shall not call themselves either directly or indirectly.
 *
 *	It is trivial to detect direct recursion, but it can in some cases
 *	be impossible to detect indirect recursion statically.
*/ 

#include "m70.cc"

/*
 *	Rule 71:	Required
 *			
 *	Functions shall always have prototype declarations and the
 *	prototype shall be visible at both the function definition and the
 *	call.
*/ 

#include "m71.cc"

/*
 *	Rule 72:	Required
 *			
 *	For each function parameter the type given in the declaration and
 *	definition shall be identical, and the return types shall also be
 *	identical.
*/ 

#include "m72.cc"

/*
 *	Rule 73:	Required
 *			
 *	Identifiers shall be given either for all of the parameters in a
 *	function prototype declaration or for none.
*/ 

#include "m73.cc"


/*
 *	Rule 74:	Required
 *			
 *	If identifiers are given for any of the parameters, then the
 *	identifiers used in the declaration and definition shall be
 *	identical.
*/ 

//TODO requires two pass

/*
 *	Rule 75:	Required
 *			
 *	Every function shall have an explicit return type.
*/ 


#include "m75.cc"

/*
 *	Rule 76:	Required
 *			
 *	Functions with no parameters shall be declared with parameter type
 *	void.
*/ 

#include "m76.cc"

/*
 *	Rule 77:	Required
 *			
 *	The unqualifed type of parameters passed to a function shall be
 *	compatible with the unqualified expected types defined in the
 *	function prototype.
*/ 

// parm verification compare proto types to types used at call

/*
 *	Rule 78:	Required
 *			
 *	The number of parameters passed to a function shall match the
 *	function prototype.
*/ 

// parm count compare actual to proto

/*
 *	Rule 79:	Required
 *			
 *	The values returned by void functions shall not be used.
*/ 

// don't call void type functions


/*
 *	Rule 80:	Required
 *			
 *	Void expressions shall not be passed as function parameters.
*/ 

#include "m80.cc"

/*
 *	Rule 81:	Advisory
 *			
 *	const qualification should be used on function parameters which are
 *	passed by reference, where it is intended that the function will
 *	not modify the parameter.
*/ 

#include "m81.cc"

/*
 *	Rule 82:	Advisory
 *			
 *	A function should have a single point of exit.
*/ 

#include "m82.cc"

/*
 *	Rule 83:	Required
 *			
 *	For functions with non-void return type:
 *		i) 	there shall be one return statement for every exit
 *			branch (including the end of the program),
 *		ii)	each return shall have an expression,
 *		iii)	the return expression shall match the declared return
 *			type.
*/ 

#include "m83.cc"

/*
 *	Rule 84:	Required
 *			
 *	For functions with void return type, return statements shall not
 *	have an expression.
*/ 

#include "m84.cc"

/*
 *	Rule 85:	Advisory
 *			
 *	Functions called with no parameters should have empty parentheses.
*/ 

#include "m85.cc"

/*
 *	Rule 86:	Advisory
 *			
 *	If a function returns error information, then that error
 *	information shall be tested.
*/ 

/* RULE 86 comment
Contact Abraxas Software for a solution, we have many solutions to this problem 
1.) Verify that ALL system routines that return non-void are tested, we have this detection.
2.) Verify that ALL prototyped functions that return a non-void are tested, this requires a two pass algorithm 
Both solutions required a static table be built ahead of time for all functions to be verifed that the
return value is tested, before used.
*/

/*
 *	Rule 87:	Required
 *			
 *	#include statements in a file shall only be preceded by other
 *	pre-processor directives or comments.
*/ 

#include "m87.cc"

// Misra C Enforcement Testing
///***************************************************************
// Rule 88: Required
// Non-standard characters shall not occur in header file names in
// #include directives.
///

//BUILT-IN codecheck will vlalidate file names automatically

// misra rule 89 #include include must be one of the following two forms
/*
#include "misra.h"		// these are the only two legal forms in codecheck
#include <misra.h>
*/

//BUILT-IN codecheck will verify RULE 89 automatically



/*
 *	Rule 90:	Required
 *			
 *	C macros shall only be used for symbolic constants, function-like
 *	macros, type qualifiers and storage class specifiers.
*/ 


#include "m90.cc"

/*
 *	Rule 91:	Required
 *			
 *	Macros shall not be #defined'd and #undef'd in a block.
*/

#include "m91.cc"

/*
 *	Rule 92:	Advisory
 *			
 *	#undef should not be used.
*/ 

#include "m92.cc"

/*
 *	Rule 93:	Advisory
 *			
 *	A function should be used in preference to a function-like macro
*/ 

if ( pp_arg_count > 0 && pp_lowercase ) {

	WARN( 93, "A function should be used in preference to a function-like macro" )
}

/*
 *	Rule 94:	Required
 *			
 *	A function-like macro shall not be 'called' without all its
 *	arguments.
*/ 

// BUILTIN CodeCheck Warning

/*
 *	Rule 95:	Required
 *			
 *	Arguments to a function-like macro shall not contain tokens that
 *	look like pre-processing directives.
*/ 

// BUILTIN CodeCheck Warning Warning W1050: Runtime defined

/*
 *	Rule 96:	Required
 *			
 *	In the definition of a function-like macro the whole definition,
 *	and each instance of a parameter, shall be enclosed in parentheses.
*/ 

#include "m96.cc"

/*
 *	Rule 97:	Advisory
 *			
 *	Identifiers in pre-processor directives should be defined before
 *	use.
*/ 

#include "m97.cc"

/*
 *	Rule 98:	Required
 *			
 *	At most one of # or ## in a single macro definition
*/ 

#include "m98.cc"

/*
 *	Rule 99:	Required
 *			
 *	All uses of the #pragma directive shall be documented and
 *	explained.
*/ 

#include "m99.cc"

/*
 *	Rule 100:	Required
 *			
 *	The defined pre-processor operator shall only be used in one of the
 *	two standard forms.
*/ 

//	CodeCheck Built-in :: Warning C0028: Invalid argument for defined() function.

/*
 *	Rule 101:	Advisory
 *			
 *	Pointer arithmetic should not be used.
*/ 

//TODO 101.cc start

/*
 *	Rule 102:	Advisory
 *			
 *	No more than 2 levels of pointer indirection should be used.
*/ 

//TODO 102.CC EASY

/*
 *	Rule 103:	Required
 *			
 *	Relational operators shall not be applied to pointer types except
 *	where both operands are of the same type and point to the same
 *	array, structure or union.
*/ 

#include "m103.cc"


/*
 *	Rule 104:	Required
 *			
 *	Non-constant pointers to functions shall not be used.
*/ 

#include "m104.cc"


/*
 *	Rule 105:	Required
 *			
 *	All the functions pointed by a single pointer function shall be
 *	identical in the number and type of parameters and the return type.
*/ 

//TODO requires two pass

/*
 *	Rule 106:	Required
 *			
 *	The address of an object with automatic storage shall not be
 *	assigned to an object which may persist after the object has ceased
 *	to exist.
*/ 

#include "m106.cc"


/*
 *	Rule 107:	Required
 *			
 *	The NULL pointer shall not be dereferenced.
 *
 *	This is not statically detectable in all cases.
*/ 

#include "m107.cc"

/*
 *	Rule 108:	Required
 *			
 *	In the specification of a structure or union type, all members of
 *	the structure or union shall be fully specified.
*/ 

// BUILTIN CodeCheck Warning C0011:

/*
 *	Rule 109:	Required
 *			
 *	Overlapping variable storage shall not be used.
*/ 

#include "m109.cc"

/*
 *	Rule 11:	Required
 *			
 *	No reliance on more than 31 character significance.
*/ 

#include "m11.cc"

/*
 *	Rule 110:	Required
 *				
 *	Unions shall not be used to access the sub-parts of larger data
 *	types.
 *
 *	This seems slightly odd as Rule 109 effectively precludes the use
 *	of unions at all.
*/ 

#include "m110.cc"

/*
 *	Rule 111:	Required
 *				
 *	Bit fields shall only be defined to be of type unsigned int or
 *	signed int.
*/ 

#include "m111.cc"

/*
 *	Rule 112:	Required
 *				
 *	Bit fields of type signed int shall be at least 2 bits long.
*/ 

#include "m112.cc"


/*
 *	Rule 113:	Required
 *				
 *	All the members of a structure (or union) shall be named and shall
 *	only be accessed via their name.
*/ 

#include "m113.cc"

/*
 *	Rule 114:	Required
 *				
 *	Reserved words and standard library function names shall not be
 *	redefined or undefined.
 *
 *	Note the interaction with rule 92 on #undef.
 *	We do not look for reserved words here as most occurrences lead to
 *	syntax error.
*/ 

#include "m114.cc"

/*
 *	Rule 115:	Required
 *			
 *	Standard library function names shall not be reused.
*/ 

// same argument as rule 114 see codecheck rule-file "posix.cc" for implementation

/*
 *	Rule 116:	Required
 *			
 *	All libraries used in production code shall be written to comply
 *	with the provisions of this document, and shall have been subject
 *	to appropriate validation.
 *
 *	Not automatically enforceable by a simple rule.
*/ 

// See "posix.cc" & "posix2.cc" for implementation of std-lib testing

/*
 *	Rule 117:	Required
 *			
 *	The validity of values passed to library functions shall be
 *	checked.
 *
 *	Not automatically enforceable by a simple rule although there are
 *	some examples which are worth checking.
*/ 

// see posix2.cc

/*
 *	Rule 118:	Required
 *			
 *	Dynamic heap memory allocation shall not be used.
*/ 

#include "m118.cc"


/*
 *	Rule 119:	Required
 *			
 *	The error indicator errno shall not be used.
*/ 

#include "m119.cc"

/*
 *	Rule 12:	Advisory
 *			
 *	Identifiers in different name spaces shall have different spelling.
*/ 

#include "m12.cc"

/*
 *	Rule 120:	Required
 *			
 *	The macro offsetof shall not be used.
*/ 

#include "m120.cc"

/*
 *	Rule 121:	Required
 *			
 *	<locale.h> and the setlocale function shall not be used.
*/ 

#include "m121.cc"

/*
 *	Rule 122:	Required
 *			
 *	The setjmp macro and the longjmp function shall not be used.
*/ 

#include "m122.cc"

/*
 *	Rule 123:	Required
 *			
 *	The signal handling facilities of <signal.h> shall not be used
*/ 

#include "m123.cc"

/*
 *	Rule 124:	Required
 *			
 *	The input/output header file <stdio.h> must not be used.
 *
 *	(The standards says library, but this is a header file containing
 *	references to i/o functions amongst other things.)
*/ 

#include "m124.cc"

/*
 *	Rule 125:	Required
 *			
 *	The library functions atof, atoi and atol from header file
 *	<stdlib.h> shall not be used.
 *	
*/ 

#include "m125.cc"

/*
 *	Rule 126:	Required
 *			
 *	The library functions abort, exit, getenv and system from
 *	<stdlib.h> shall not be used.
 *	
*/ 

#include "m126.cc"

/*
 *	Rule 127:	Required
 *			
 *	The time handling functions of <time.h> shall not be used.
*/ 

#include "m127.cc"

// COPYRIGHT (C) 2006 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.  
