/*
 *	Rule 24:	Required
 *			
 *	Identifiers shall not have internal and external linkage
 *	simultaneously in the same translation unit.
*/ 

if ( dcl_local ) {

	if ( dcl_storage_flags & EXTERN_SC ) {

		WARN_MR( 24, "Identifiers shall not have internal and external linkage.");
	}
}