/*
 *	Rule 125:	Required
 *			
 *	The library functions atof, atoi and atol from header file
 *	<stdlib.h> shall not be used.
 *	
*/ 

if ( idn_function ) {

	if ( CMPPREVTOK("atof") || CMPPREVTOK("atoi") || CMPPREVTOK("atol") ) {
	
		WARN_MR( 125, "Library functions atof, atoi and atol should not be used." );
	}
}