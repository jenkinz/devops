#include "misra.cch"
int inforloop;	// signal in for loop

if ( keyword("for") ) {
	inforloop = 1;

}

if ( stm_cp_begin ) {

	if ( stm_kind == FOR ) {

		inforloop = 0;
	}
}



if ( op_comma ) {
	
	if ( inforloop == 0 ) {
		WARN_MR( 42, "Comma operator shall not be used, except in the for-loop" );

	}
}