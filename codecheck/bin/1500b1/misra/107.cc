#include "misra.cch"

//	* Rule 107 (The null pointer shall not be de-referenced).
//		Can�t be done by others (some migth):

char derefnm[MAXIDLEN];
int deref;


if ( dcl_initializer ) {

//DUMP("DI")
	strcpy( derefnm, dcl_name() );
	if ( dcl_level(0) == 3 ) deref = -1;

}


if ( lex_initializer ) {

//DUMP("LI")
	if ( deref && atoi(prev_token()) == 0 ) {
		
		deref = lin_number;
	}
	else deref = 0;
}

if ( idn_variable ) {
//DUMP("IV")
	if ( deref > 0 ) {

		 WARN_MR( 107, "The null pointer shall not be de-referenced" );

		deref = 0;
	}
}

if ( mod_end ) deref = 0;