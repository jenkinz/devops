// Misra C Enforcement Testing
//
// Rule 125: Required
// The library functions atof, atoi and atol from header file
// <stdlib.h> shall not be used.
// 
///

#include <stdlib.h>

#include "misra.h"

static void
func125a ( void ) 
{
    
SC_8 af[] = "1.1";
SC_8 ai[] = "1";
SC_8 al[] = "1";
    
FL_32 f = atof ( af ) ; // RULE 125 
SI_32 i = atoi ( ai ) ; // RULE 125 
SI_64 l = atol ( al ) ; // RULE 125 
    
}
