// Misra C Enforcement Testing
//
// Rule 86: Advisory
// If a function returns error information, then that error
// information shall be tested.
///

//
// Not statically enforceable. The method of testing is not
// sufficiently well-defined.
///

static int dummy;
