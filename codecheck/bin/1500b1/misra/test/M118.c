// Misra C Enforcement Testing
//
// Rule 118: Required
// Dynamic heap memory allocation shall not be used.
///

#include <stdlib.h>

#include "misra.h"

static void
func118 ( void ) 
{
    
double *pd;
    
    pd = malloc ( sizeof ( double )  ) ; // RULE 118 
    pd = calloc ( 1,sizeof ( double )  ) ; // RULE 118 
    pd = realloc ( pd,2*sizeof ( double )  ) ; // RULE 118 
    realloc ( pd,2*sizeof ( double )  ) ; // RULE 118 
    // Doubly dangerous 
    // pd might move 
    
}
