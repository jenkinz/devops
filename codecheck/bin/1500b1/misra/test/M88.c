// Misra C Enforcement Testing
///***************************************************************
// Rule 88: Required
// Non-standard characters shall not occur in header file names in
// #include directives.
///

#include "misra.h" 

#pragma message "misra 88, non standard char's in #include"


//#include <m'sra.h> ///RULE 88 
//#include <m\sra.h> ///RULE 88 
//#include <m"sra.h> ///RULE 88 
//#include <m///ra.h> /* RULE 88 

//#include "m'sra.h" ///RULE 88 
//#include "m\sra.h" ///RULE 88 
//#include "m///ra.h" /* RULE 88 
