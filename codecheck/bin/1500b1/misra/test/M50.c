// Misra C Enforcement Testing
//
// Rule 50: Required
// Floating point variables shall not be tested for exact equality or
// inequality.
///

#include "misra.h"

SI_32
rule50 ( void ) 
{
    
SI_32 c = 3;
    
FL_32 f = 0.1234f;
FL_32 g = 0.1234f;
    
    if ( f == g ) // RULE 50 
    {
        
        c = 1;
        
    }
    
    return c;
    
}
