#include "misra.cch"

if ( pp_macro ) {


	if ( CMPTOK("{") || CMPTOK("(") || CMPTOK("int") || CMPTOK("long") || CMPTOK("void") 
		|| CMPTOK("signed") || CMPTOK("unsigned") || CMPTOK("float") ) {
		
		WARN_MR( 90, "C macros shall only be used for symbolic constants, function-like macros.");


	}

}