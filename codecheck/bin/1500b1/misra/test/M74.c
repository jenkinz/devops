// Misra C Enforcement Testing
//
// Rule 74: Required
// If identifiers are given for any of the parameters, then the
// identifiers used in the declaration and definition shall be
// identical.
///

#include "misra.h"

static SI_32 func74a ( SI_32, UI_32 ) ;
static SI_32 func74b ( SI_32 a, UI_32 b ) ;

static SI_32
func74a ( SI_32 a, UI_32 b ) 
{
    
    return a;
    
}

static SI_32
func74b ( SI_32 c, UI_32 d ) // RULE 74 
{
    
    return c;
    
}
