// Misra C Enforcement Testing
//
// Rule 22: Advisory
// Declarations should be at function scope unless a wider scope is
// necessary.
//
// This rule is incorrectly worded, only labels have function scope in
// C. It is unenforceable in its present form.
///

static int dummy;
