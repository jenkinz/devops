// Misra C Enforcement Testing
//
// Rule 18: Advisory
// Numeric constants should be suffixed to indicate type if an
// appropriate suffix is available.
///

#include "misra.h"

int i = 1234; 
signed int si = 1234; 
unsigned int ui = 1234; // RULE 18 
unsigned int ui1 = 1234u; 

signed long sl = 1234; // RULE 18 
signed long sl1 = 1234l; // RULE 18 ( L not l ) 
signed long sl2 = 1234L; 

unsigned long ul = 1234; // RULE 18 
unsigned long ul1 = 1234u; // RULE 18 
unsigned long ul2 = 1234L; // RULE 18 
unsigned long ul3 = 1234lu; // RULE 18 ( L not l ) 
unsigned long ul4 = 1234LU; 

float fl = 0.1234; // RULE 18 
float fl1 = 0.1234f; 
float fl2 = 0.1234F; 

double dl = 0.1234; 
double dl1 = 0.1234L; // RULE 18 

long double ldl = 0.1234; // RULE 18 
long double ldl1 = 0.1234L; 
long double ldl2 = 0.1234l; // RULE 18 ( L not l ) 

static void
rule18 ( void ) 
{
    
signed char c;
unsigned char uc;
    
    c = ( signed char ) 12; 
    uc = ( unsigned char ) 12; 
    
}
