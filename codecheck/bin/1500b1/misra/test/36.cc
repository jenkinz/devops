if ( op_bit_or || op_bit_and || op_bit_not ) {

	if ( chkexp ) {

	  WARN_MR( 36, "Logical operators should not be confused with bitwise operators." );
	}
}