// Misra C Enforcement Testing
//
// Rule 15: Advisory
// Floating point implementations should comply with a defined
// floating point standard.
//
// Not automatically enforceable. Requires use of packages such as
// paranoia.
///

static int dummy;
