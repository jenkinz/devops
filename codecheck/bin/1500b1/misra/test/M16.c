// Misra C Enforcement Testing
//
// Rule 16: Required
// Underlying floating point bit representations should not be used.
//
// Not automatically enforceable.
///

static int dummy;

float f1;   // define a float
