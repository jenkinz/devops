// Misra C Enforcement Testing
//
// Rule 87: Required
// #include statements in a file shall only be preceded by other
// pre-processor directives or comments.
///

int i; // RULE 87 

#include "misra.h"

int func87a ( void ) 
{
    
    return 1;
    
}
