#include "misra.cch"

int var_overload;	// signal an overload found

if ( dcl_local || dcl_parameter || dcl_member ) {

DEBD("DV")
	DUMP("DV TOK")
	strncpy( curLocStr, dcl_name(), MAXIDLEN );
}

if ( tag_begin ) {

DUMP( "TAG" );
	if ( CMPPREVTOK(curLocStr) ) {

	 WARN_MR(12, "Identifiers in different name spaces shall have different spelling.");
	}

}


if ( op_colon_1 ) {

DUMP("LABEL")
}

if ( op_goto ) {

DUMP("GOTO")

DEBO("GO")
}

if ( dcl_hidden ) {

DEBD("HIDDEN")

}