#include "misra.cch"

if ( op_bitwise || op_right_shift || op_left_shift ) {

//DEBO( "BIT" )

	if ( op_base(1) == INT_TYPE ) {
		
	  WARN_MR( 37, "Bitwise operations shall not be performed on signed integer types.");
	}
}