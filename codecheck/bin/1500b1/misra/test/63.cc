#include "misra.cch"

int switch_exp;	// signal whether in switch expression

if ( keyword("switch") ) switch_exp = 1;

if ( op_switch ) switch_exp = 0;

if ( idn_variable ) {
	
	if ( switch_exp ) {
		DEBIL("IV")
	}
}

if ( op_equal ) {

	DEBO("TEST")


	if ( switch_exp ) {
		WARN_MA(63, "A switch expression should not represent a Boolean value.");

	}

}

if ( stm_relation ) {

warn(0,"REL kind=%d", stm_kind);

	if ( stm_kind == SWITCH ) {
		WARN_MA(63, "A switch expression should not represent a Boolean value.");

	}
}