#include "misra.cch"

int side_effect_logical;

if ( op_postfix ) {

side_effect_logical++;
DEBO("OP POST")
}

if ( op_log_or || op_log_and ) {
DEBO("LOG")
	if ( side_effect_logical ) {

	WARN_MR(33, "Right hand side of a && or || operator shall not contain side-effect");
	}

	side_effect_logical = 0;
}