#include "misra.cch"

int select_else; // signal whether last select statement had else

if ( stm_need_comp ) {

	if ( stm_if_else == 0 && stm_kind == ELSE && select_else == 0 ) {
	
	 WARN_MA( 60, "All if, else if constructs should contain a final else clause.");
	}

warn(2,"NEED_COMP if_else=%d kind=%d contain=%d", stm_if_else, stm_kind, stm_container );
DUMP("COMP")

}


if ( stm_is_select ) {
warn(1,"SEL kind =%d if_else=%d", stm_kind, stm_if_else );
	select_else = stm_if_else;
}
/*
if ( stm_container ) {

warn(1,"CONTAINER kind =%d if_else=%d", stm_kind, stm_if_else );
}
if ( stm_if_else ) {
warn(2,"IF_ELSE if_else=%d kind=%d", stm_if_else, stm_kind );

}

if ( stm_end ) {
warn(1,"END kind =%d if_else=%d", stm_kind, stm_if_else );
}

if ( stm_kind ) {

	if ( stm_kind == IF ) {
warn(2,"STM_KIND if_else=%d kind=%d", stm_if_else, stm_kind );
	}
}

if ( op_else ) {

DUMP("ELSE")
}

if ( op_if ) {
DUMP("IF")
}
*/