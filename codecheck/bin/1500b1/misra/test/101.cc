//#include <misra.cch>


int inindirect;

if ( mod_begin ) inindirect = 0;

if ( op_indirect ) inindirect = 1;

if (  op_add || op_subt || op_postfix || op_prefix  ) {

	if ( inindirect > 1 ) 
		warn( 101,"indirect arithmetic should not be used." );
}

if ( op_open_paren ) {
	if ( inindirect ) inindirect++;
}

if ( op_close_paren ) {
	if ( inindirect > 1 ) inindirect--; 
	else inindirect = 0;			// turn it off
}
