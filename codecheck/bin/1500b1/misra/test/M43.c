// Misra C Enforcement Testing
//
// Rule 43: Required
// Implicit conversions which may result in a loss of information
// shall not be used.
///

#include "misra.h"

SI_32
rule43 ( void ) 
{
    
SI_16 a = 3;
SI_32 i = 3;
SI_32 j = 3;
SI_32 c = 3;
    
    a = ( i + j ) * ( i + j ) ; // RULE 43 
    j = ( i * i * ( a = i ) * ( i + j )  ) ; // RULE 43 
    a = ( i + j ) * (  ( SI_16 ) ( i + j )  ) ; // RULE 43 
    
    a = ( SI_16 ) (  ( i + j ) * ( i + j )  ) ; 
    
    j = ( i + j ) * ( i + j ) ; 
    
    c = j;
    
    return c;
    
}
