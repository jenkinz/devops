// Misra C Enforcement Testing
//
// Rule 14: Required
// char should only be used with the signed or unsigned keyword.
///

#include "misra.h"

static char rule14a ( const char * ) ; // RULE 14 
static signed char rule14b ( const char * ) ; // RULE 14 
static char rule14c ( const signed char * ) ; // RULE 14 
static signed char rule14d ( const signed char * ) ; 

SI_32
rule14 ( void ) 
{
    
unsigned char u; 
signed char s; 
char c; // RULE 14 
    
    return 1;
    
}
