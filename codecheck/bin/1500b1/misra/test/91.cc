#include "misra.cch"

int inBrace;	// in curly brace

if ( op_open_brace ) inBrace++;

if ( op_close_brace ) inBrace--;

if ( lin_preprocessor ) {

	if ( inBrace &&  
		((lin_preprocessor==DEFINE_PP_LIN) || (lin_preprocessor==UNDEF_PP_LIN)) ){
		
		 WARN( 91, "Macros shall not be #defined'd and #undef'd in a block." )
	}
}