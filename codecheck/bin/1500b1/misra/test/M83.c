// Misra C Enforcement Testing
//
// Rule 83: Required
// For functions with non-void return type:
// i ) there shall be one return statement for every exit
// branch ( including the end of the program ) ,
// ii ) each return shall have an expression,
// iii ) the return expression shall match the declared return
// type.
///

#include "misra.h"

SC_8 func83a ( void ) 
{
    
SC_8 pc = '\0';
    
    // return; 
    
}

SC_8 func83b ( void ) 
{
    
SC_8 pc = '\0';
    
    return; // RULE 83 
    
}

SC_8 func83c ( void ) 
{
    
SC_8 pc = '\0';
    
    return ( long ) pc; // RULE 83 
    
}

SC_8 func83d ( void ) 
{
    
SC_8 pc = '\0';
    
    return pc; 
    
}
