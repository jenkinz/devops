// Misra C Enforcement Testing
//
// Rule 26: Required
// If objects are declared more than once they shall be declared
// compatibly.
///

#include "misra.h"
//
// See also rule 27 for xi declaration.
///
extern SI_32 xi;

static SI_32 i;
static SI_16 i; // RULE 26 
//
// The use of rule24 (  ) is deliberate below. There is another one
// elsewhere. The checking tool should find this if it sees the
// whole of the compliance suite at once. The other one is defined as
// type SI_32.
///
SI_16
rule24 ( void ) // RULE 26 
{
    
    return 1;
    
}
