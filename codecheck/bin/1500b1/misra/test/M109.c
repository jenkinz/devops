// Misra C Enforcement Testing
//
// Rule 109: Required
// Overlapping variable storage shall not be used.
///

#include "misra.h"

union u // RULE 109 
{
    
UI_32 u;
SI_32 l;

} un;

static void
func109a ( SI_64 l ) 
{
    
    
}
