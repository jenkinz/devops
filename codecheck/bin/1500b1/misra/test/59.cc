#include "misra.cch"

if ( stm_simple ) warn(0,"simple");

if ( idn_variable ) {  // exclude if ( var ) case

	if ( stm_container <= SWITCH ) {

	 WARN_MR( 59, "Statements in body of if, else if, else, while, do must be in braces.");
	}
	warn(1,"con=%d dep=%d", stm_container, stm_depth );
}