/* CodeCheck Copyright (c) 1988-2006 by Abraxas Software Inc. (R).  All rights reserved. */

// Generate HTML Comments from C/C++

#define DOCTAG(t,m) fprintf( mfp, "<dt><b>%s:</b>\n<dd>%s\n", t, m+strlen(t)+1 );
#define BUFSIZ 256

char comment[BUFSIZ], *ap, *tp;
int ocom;

if ( mod_begin ) ocom = 0;


if ( lin_has_comment ) {	// init def list 

	strncpy( comment, line(), BUFSIZ );

	if ( strstr( comment, "/**" ) ) {	// open comment
		MOD( "<dl>\n" ); ocom = 1;		// signal open
	}
	else if ( strstr( comment, "*/" ) ) {	// close comment
		if ( ocom ) MOD( "</dl>\n" ); ocom = 0;	// signal close
	}
}

if ( lin_is_comment ) {
 
   strncpy( comment, line(), BUFSIZ );

   if ( ap = strchr( comment,'@' ) ) {
	
	if ( tp = strstr( ap, "author" ) ) {
	   
		MOD( "<dt><b>Author:</b>\n" );
		MODS( "<dd>%s\n", tp+7);

	} 
	else if ( tp = strstr( ap, "return" ) ) {
	
		MOD("<dt><b>Return:</b>\n"  );
		MODS( "<dd>%s\n", tp + 7 );
	}
	else if ( tp = strstr( ap, "version" ) ) {
		
		DOCTAG( "Version", tp );
	}
	else if ( tp = strstr( ap, "param" ) ) {
	
		MOD( "<dt><b>Parameters:</b>\n" );
		MODS( "<dd>%s\n", tp+6 );
	}
	else if ( tp = strstr( ap, "exception" ) ) {
	
	   fprintf( mfp,"%s\n", tp+10 );
	}
	else if ( tp = strstr( ap, "deprecated" ) ) {
	
		DOCTAG( "Deprecated", tp );

	}
	else if ( tp = strstr( ap, "since" ) ) {

		MOD( "<dt><b>Since:</b>\n" );
		MODS( "<dd>%s\n", tp+6 );
	}
	else if ( tp = strstr( ap, "see" ) ) { // see anchored tag
	
	  MOD( "<dt><b>See:</b>\n" );
	  fprintf( mfp, "<dd><a href=""%s"">%s</a>\n", tp+4, tp+4 ); 

	}
   }
   else { 	// * comment

	if ( ap = strchr(comment,'*') ) { 

	   if ( tp = strchr( comment, '<' ) ) { // *** pass raw html tags
		MODS( "%s\n", tp );
	   }	
	   else {
		 MODS( "<dd>%s\n", ap+1 );  // treat as def-def
	   }
	}
   }
}