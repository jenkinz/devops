// Example implementation of Misra Coding Standard
// COPYRIGHT (C) 2005 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.  

#include <misra.cch>

/*
 *	Rule 1:		Required
 *			
 *	ISO 9899 conformance.
 *
 *	This requires a toolset to be run against the FIPS 160 compiler
 *	validation suite or equivalent.
*/ 

	if ( prj_begin ) {
	
	//	set_option( 'k', 1 );	// for strict ansi-c

		set_option( 'c', 1 );	// turn on full type check info
	}

/*
 *	Rule 10:	Advisory
 *			
 *	Code shall not be commented out.
 *
 *	Not automatically enforceable.  Requires manual inspection techniques.
*/ 

// interesting, could be done with two pass

/*
 *	Rule 100:	Required
 *			
 *	The defined pre-processor operator shall only be used in one of the
 *	two standard forms.
*/ 

//	CodeCheck Built-in :: Warning C0028: Invalid argument for defined() function.

/*
 *	Rule 101:	Advisory
 *			
 *	Pointer arithmetic should not be used.
*/ 

//TODO 101.cc start

/*
 *	Rule 102:	Advisory
 *			
 *	No more than 2 levels of pointer indirection should be used.
*/ 

//TODO 102.CC EASY

/*
 *	Rule 103:	Required
 *			
 *	Relational operators shall not be applied to pointer types except
 *	where both operands are of the same type and point to the same
 *	array, structure or union.
*/ 

if ( op_equal||op_not_eq||op_more||op_more_eq||op_less||op_less_eq ) {
	if ( (op_level(1,0) == POINTER) || (op_level(2,0) == POINTER) ) {

	 	WARN_MR(103, "Relational operators shall not be applied to pointer types.");
	}


}

/*
 *	Rule 104:	Required
 *			
 *	Non-constant pointers to functions shall not be used.
*/ 



if ( op_cast_to_ptr ) {

	if ( idn_level(0) == FUNCTION ) {

		WARN_MR( 104, "Non-constant pointers to functions shall not be used. ");
	}

}
/*
 *	Rule 105:	Required
 *			
 *	All the functions pointed by a single pointer function shall be
 *	identical in the number and type of parameters and the return type.
*/ 

//TODO requires two pass

/*
 *	Rule 106:	Required
 *			
 *	The address of an object with automatic storage shall not be
 *	assigned to an object which may persist after the object has ceased
 *	to exist.
*/ 


if ( idn_local ) {

	strncpy( curLocStr, idn_name(), MAXIDLEN );
}




if ( op_address ) {

	if ( CMPPREVTOK(curLocStr) ) {

	  WARN_MR( 106, "Address of object with automatic storage shall not be referenced.");
	}

}
/*
 *	Rule 107:	Required
 *			
 *	The NULL pointer shall not be dereferenced.
 *
 *	This is not statically detectable in all cases.
*/ 

char derefnm[MAXIDLEN];
int deref;


if ( dcl_initializer ) {


	strcpy( derefnm, dcl_name() );
	if ( dcl_level(0) == 3 ) deref = -1;

}


if ( lex_initializer ) {


	if ( deref && atoi(prev_token()) == 0 ) {
		
		deref = lin_number;
	}
	else deref = 0;
}

if ( idn_variable ) {

	if ( deref > 0 ) {

		 WARN_MR( 107, "The null pointer shall not be de-referenced" );

		deref = 0;
	}
}

if ( mod_end ) deref = 0;

/*
 *	Rule 108:	Required
 *			
 *	In the specification of a structure or union type, all members of
 *	the structure or union shall be fully specified.
*/ 

// BUILTIN CodeCheck Warning C0011:

/*
 *	Rule 109:	Required
 *			
 *	Overlapping variable storage shall not be used.
*/ 

if ( tag_begin ) {

	if ( tag_kind == UNION_TAG ) {

		WARN_MR(109, "Overlapping variable storage shall not be used.");
	}
}

/*
 *	Rule 11:	Required
 *			
 *	No reliance on more than 31 character significance.
*/ 
	if ( dcl_global | dcl_local ) {

		if ( dcl_ident_length > 31 ) 
			WARN( 11, "No reliance on more than 31 character significance." );
	}

/*
 *	Rule 110:	Required
 *				
 *	Unions shall not be used to access the sub-parts of larger data
 *	types.
 *
 *	This seems slightly odd as Rule 109 effectively precludes the use
 *	of unions at all.
*/ 

if ( dcl_variable ) {

	if ( dcl_base_root == UNION_TYPE ) {

		WARN_MR(110,"Unions shall not be used to access sub-parts of larger data.");
	}
}

/*
 *	Rule 111:	Required
 *				
 *	Bit fields shall only be defined to be of type unsigned int or
 *	signed int.
*/ 


if ( dcl_bitfield ) {

	if (  !(dcl_signed || dcl_unsigned) ) {

		WARN( 111, "Bit fields shall be defined unsigned int or signed int" )
	}

}

/*
 *	Rule 112:	Required
 *				
 *	Bit fields of type signed int shall be at least 2 bits long.
*/ 

if ( dcl_bitfield ) {

	if (  dcl_signed ) {

		if ( dcl_bitfield_size < 2 ) {

		 WARN( 112, "Bit fields of type signed int shall be at least 2 bits long." )
		}
	}
}


/*
 *	Rule 113:	Required
 *				
 *	All the members of a structure (or union) shall be named and shall
 *	only be accessed via their name.
*/ 

if ( dcl_member ) {

	if ( dcl_ident_length == 0 ) {

		WARN( 113, "members of a structure (or union) shall be named" )
	}
}

/*
 *	Rule 114:	Required
 *				
 *	Reserved words and standard library function names shall not be
 *	redefined or undefined.
 *
 *	Note the interaction with rule 92 on #undef.
 *	We do not look for reserved words here as most occurrences lead to
 *	syntax error.
*/ 

// Obviously usage of std-lib functions will cause a codecheck syntax error see rule file "posix.cc"


/*
 *	Rule 115:	Required
 *			
 *	Standard library function names shall not be reused.
*/ 

// same argument as rule 114 see codecheck rule-file "posix.cc" for implementation

/*
 *	Rule 116:	Required
 *			
 *	All libraries used in production code shall be written to comply
 *	with the provisions of this document, and shall have been subject
 *	to appropriate validation.
 *
 *	Not automatically enforceable by a simple rule.
*/ 

// See "posix.cc" & "posix2.cc" for implementation of std-lib testing

/*
 *	Rule 117:	Required
 *			
 *	The validity of values passed to library functions shall be
 *	checked.
 *
 *	Not automatically enforceable by a simple rule although there are
 *	some examples which are worth checking.
*/ 

// see posix2.cc

/*
 *	Rule 118:	Required
 *			
 *	Dynamic heap memory allocation shall not be used.
*/ 

	if ( idn_function ) {

		if ( strcmp( idn_name(), "malloc" ) == 0 
			|| strcmp( idn_name(), "realloc" ) == 0 
			|| strcmp( idn_name(), "calloc" ) == 0) {

			WARN( 118, "Dynamic heap memory allocation shall not be used." );
		}
	}

/*
 *	Rule 119:	Required
 *			
 *	The error indicator errno shall not be used.
*/ 

	if ( header_name() ) {		// at #include

		if ( strcmp(header_name(), "errno.h" ) == 0 ) {
			WARN( 119, "The error indicator errno shall not be used." );
		}
	}

	if ( idn_global ) {

		if ( strcmp(idn_name(), "errno" ) == 0 ) {
			WARN( 119, "The error indicator errno shall not be used." );
		}
	}
/*
 *	Rule 12:	Advisory
 *			
 *	Identifiers in different name spaces shall have different spelling.
*/ 

//TODO multiple pass

/*
 *	Rule 120:	Required
 *			
 *	The macro offsetof shall not be used.
*/ 

if ( lex_macro ) {	// expand a macro

		if ( strcmp( token(), "offsetof" ) == 0 ) {

			WARN( 120, "The macro offsetof shall not be used." );
		}
	}

/*
 *	Rule 121:	Required
 *			
 *	<locale.h> and the setlocale function shall not be used.
*/ 


if ( idn_function ) {

	if ( strcmp(idn_name(), "sestlocale" ) == 0 ) {
		WARN( 121, "<locale.h> and the setlocale function shall not be used." );
	}
}

/*
 *	Rule 122:	Required
 *			
 *	The setjmp macro and the longjmp function shall not be used.
*/ 

if ( idn_function ) {

	if ( strcmp(idn_name(), "setjmp" ) == 0
		|| strcmp(idn_name(), "longjmp" ) == 0 ) {
		WARN( 122, "The setjmp macro and the longjmp function shall not be used." );
	}
}

/*
 *	Rule 123:	Required
 *			
 *	The signal handling facilities of <signal.h> shall not be used
*/ 

if ( lex_macro ) {

	if ( CMPTOK("SIGINT") ) {
		
		WARN_MR( 123, "The signal handling facilities of shall not be used." );
	}

}

/*
 *	Rule 124:	Required
 *			
 *	The input/output header file <stdio.h> must not be used.
 *
 *	(The standards says library, but this is a header file containing
 *	references to i/o functions amongst other things.)
*/ 

if ( header_name() ) {		// at #include

		if ( strcmp(header_name(), "stdio.h" ) == 0 ) {
			WARN( 124, "The input/output header file <stdio.h> must not be used." );
		}
	}

/*
 *	Rule 125:	Required
 *			
 *	The library functions atof, atoi and atol from header file
 *	<stdlib.h> shall not be used.
 *	
*/ 

if ( idn_function ) {

	if ( CMPPREVTOK("atof") || CMPPREVTOK("atoi") || CMPPREVTOK("atol") ) {
	
		WARN_MR( 125, "Library functions atof, atoi and atol should not be used." );
	}
}

/*
 *	Rule 126:	Required
 *			
 *	The library functions abort, exit, getenv and system from
 *	<stdlib.h> shall not be used.
 *	
*/ 

if ( idn_function ) {

	if ( CMPPREVTOK("abort")||CMPPREVTOK("exit")||CMPPREVTOK("getenv")||CMPPREVTOK("system") ) {
	
		WARN_MR( 126, "Library functions abort, exit, getenv and system should not be used." );
	}
}

/*
 *	Rule 127:	Required
 *			
 *	The time handling functions of <time.h> shall not be used.
*/ 

if ( idn_function ) {

	if ( CMPPREVTOK("clock")||CMPPREVTOK("time")||CMPPREVTOK("difftime")||CMPPREVTOK("mktime")
		||CMPPREVTOK("asctime")||CMPPREVTOK("ctime")||CMPPREVTOK("gmtime") 
		||CMPPREVTOK("localtime")||CMPPREVTOK("strftime")  ) {
	
		WARN_MR( 127, "Time handling functions of <time.h> shall not be used" );
	}
}

/*
 *	Rule 13:	Advisory
 *			
 *	The basic types char, short, int, long, float and double should not
 *	be used.
*/ 

if ( dcl_local ) {

	switch ( dcl_base ) {

			case 	CHAR_TYPE:
			case 	SHORT_TYPE:
			case	INT_TYPE:
			case	LONG_TYPE:
			case	FLOAT_TYPE:
			case	DOUBLE_TYPE:
			
		WARN_MA( 13, "types char, short, int, long, float, double should not be used" );
				break;
	}
}


/*
 *	Rule 14:	Required
 *			
 *	char should only be used with the signed or unsigned keyword.
*/ 

if ( dcl_local || dcl_global || dcl_function ) {
	
	
	if ( dcl_base == CHAR_TYPE ) {

		if ( !(dcl_signed || dcl_unsigned) ) {

		WARN( 14, "char should only be used with the signed or unsigned keyword." );

		}	
	}
}

/*
 *	Rule 15:	Advisory
 *			
 *	Floating point implementations should comply with a defined
 *	floating point standard.
 *
 *	Not automatically enforceable.  Requires use of packages such as
 *	paranoia.
*/ 

// need better definition of problem

/*
 *	Rule 16:	Required
 *			
 *	Underlying floating point bit representations should not be used.
 *
 *	Not automatically enforceable.
*/ 

#include "16.cc"

/*
 *	Rule 17:	Required
 *			
 *	typedef names shall not be reused.
*/ 

if ( dcl_conflict ) {

	WARN_MR( 17, "typedef names shall not be reused." );
}

/*
 *	Rule 18:	Advisory
 *			
 *	Numeric constants should be suffixed to indicate type if an
 *	appropriate suffix is available.
*/ 
int lexsuf, longsuf;

if ( mod_begin ) { 
	
	lexsuf = 0;
	longsuf = 0;
}

if( lex_unsigned ) { lexsuf = UINT_TYPE; }

if ( lex_float ) { lexsuf = FLOAT_TYPE; }

if ( lex_lc_long ) longsuf = 'l';

if ( lex_uc_long ) longsuf = 'L';

if ( lex_long_float ) lexsuf = LONG_DOUBLE_TYPE;


if ( dcl_variable ) {

	if ( strcmp( token(), "=" ) == 0 ) { // followed by assignment?

		switch ( dcl_base ) {	// if mismatch set lexsuf to -1

			case UINT_TYPE:
				if ( lexsuf != UINT_TYPE ) lexsuf = -1;
				break;

			case ULONG_TYPE:
				if ( lexsuf != ULONG_TYPE || longsuf != 'L' ) 
					lexsuf = -1;
				break;

			case DOUBLE_TYPE:
				if ( longsuf == 'L' ) lexsuf = -1;
				break;

			case FLOAT_TYPE:
				if ( lexsuf != FLOAT_TYPE ) lexsuf = -1;
				break;

			case LONG_DOUBLE_TYPE:
				if ( longsuf != 'L' ) lexsuf = -1;
				break;

			case LONG_TYPE:
				if ( longsuf != 'L' ) lexsuf = -1;
				break;

			default:	
				lexsuf = 0;
				break;

		}

		if ( lexsuf == (-1) ) {	
			WARN(18,"Numeric constants should be suffixed to indicate type.");				}

		lexsuf = 0;
		longsuf = 0;
	}
}

/*
 *	Rule 19:	Required
 *			
 *	Octal constants (other than zero) shall not be used.
*/ 


if ( lex_radix ) { // check if octal and value > 0


	if ( lex_radix == 8 && strlen(token()) > 1 ) {

		if ( (atoi(token()+1)) > 0 ) {
			WARN( 19, "Octal constants (other than zero) shall not be used." )
		}
	}


}

/*
 *	Rule 2:		Advisory
 *			
 *	Interfaces to other languages.
 *
 *	Not automatically enforceable.  Requires manual inspection techniques.
*/ 

// Need More Info

/*
 *	Rule 20:	Required
 *			
 *	All object and function identifiers shall be declared before use.
*/ 

	if ( idn_no_prototype ) {
		WARN_MR( 20, "All object and function identifiers shall be declared before use." )
	}

/*
 *	Rule 21:	Required
 *			
 *	Identifiers in an inner scope shall not mask identifiers in an
 *	outer scope.
*/ 

if ( dcl_hidden ) {

	WARN_MR(21,"Identifiers in an inner scope shall not mask identifiers in an outer.");
}

/*
 *	Rule 22:	Advisory
 *			
 *	Declarations should be at function scope unless a wider scope is
 *	necessary.
 *
 *	This rule is incorrectly worded, only labels have function scope in
 *	C.  It is unenforceable in its present form.
*/ 

// Problem not correctly defined. 

/*
 *	Rule 23:	Advisory
 *			
 *	All declarations at file scope should be static where possible.
*/ 

if ( dcl_function ) {

	if ( (dcl_storage_flags & STATIC_SC) == 0 ) {

		WARN_MA( 23, "All declarations at file scope should be static where possible.");
	}

}

/*
 *	Rule 24:	Required
 *			
 *	Identifiers shall not have internal and external linkage
 *	simultaneously in the same translation unit.
*/ 

if ( dcl_local ) {

	if ( dcl_storage_flags & EXTERN_SC ) {

		WARN_MR( 24, "Identifiers shall not have internal and external linkage.");
	}
}

/*
 *	Rule 25:	Required
 *			
 *	An identifier  with external linkage shall have exactly one
 *	external definition.
*/ 

if ( dcl_conflict ) {

       WARN_MR( 25, "An identifier with external linkage shall have exactly one definition." );
}

/*
 *	Rule 26:	Required
 *			
 *	If objects are declared more than once they shall be declared
 *	compatibly.
*/ 


/*
 *	Rule 27:	Advisory
 *			
 *	Objects should be declared as extern in only one file.
*/ 


/*
 *	Rule 28:	Required
 *			
 *	The register storage class should not be used.
*/ 


/*
 *	Rule 29:	Required
 *			
 *	The use of a tag should be consistent with the declaration.
 *
 *	This is covered by Rule 1.
*/ 


/*
 *	Rule 3:	Advisory
 *			
 *	Interfaces to other assembly languages.
 *
 *	Inline assembly language violates Rule 1.
 *
 *	Calling a function is not automatically enforceable and
 *	requires manual inspection techniques.
*/ 


/*
 *	Rule 30:	Required
 *			
 *	All automatic variables shall have been assigned a value before
 *	being used.
 *
 *	This is not automatically detectable statically in all cases but
 *	the following should exercise a tool.
*/ 

if ( idn_no_init) {

	WARN_MR(30, "All automatic variables shall have been assigned a value" );
}

/*
 *	Rule 31:	Required
 *			
 *	Braces shall be used to indicate and match the structure in the
 *	non-zero initialisation of arrays and structures.
*/ 

if ( dcl_array_size ) {

        if ( dcl_array_size > 0 ) {
                WARN_MR( 31, "Braces shall be used to indicate initialization structure");

        }

}


/*
 *	Rule 32:	Required
 *			
 *	In an enumerator list, the '=' construct shall not be used to
 *	explicitly initialise members other than the first, unless all
 *	items are explicitly initialised.
*/ 


int enum_first;		// signal enum -1, and 1 if first is initialized
int enum_init;		// count number of initialized enums
int enum_count;		// count number of contstant in enum


if ( lex_initializer ) {

	if ( enum_first ) {

		enum_init++;		// increment each	

		if ( enum_count == 1 ) enum_first = 1;	// first was initialized
	}
}

if ( dcl_enum ) {
	if ( enum_first ) {
		enum_count++;		// count number seen
	}

}

if ( tag_begin ) {

	if ( tag_kind == ENUM_TAG ) {

		enum_first = -1;	// signal begin of enum
		enum_init = 0;
		enum_count = 0;
	}
}

if ( tag_end ) {

//warn( 999, "TE enf=%d eni=%d enc=%d", enum_first, enum_init, enum_count );
	if ( tag_kind == ENUM_TAG ) {


		if ( (enum_first!=1) || (enum_count > enum_init) ) {
			if ( enum_init && enum_init != enum_first )
				WARN_MR( 32, "Enum initialzation must be on first or ALL" );
		}
		enum_first = 0;		// signal end of enum tag
	}
}

/*
 *	Rule 33:	Required
 *			
 *	The right hand side of a && or || operator shall not contain
 *	side-effects.
 *
 *	This is not automatically detectable statically in all cases but
 *	the following should exercise a tool.
*/ 

int side_effect_logical;

if ( op_postfix ) {

side_effect_logical++;

}

if ( op_log_or || op_log_and ) {

	if ( side_effect_logical ) {

	WARN_MR(33, "Right hand side of a && or || operator shall not contain side-effect");
	}

	side_effect_logical = 0;
}

/*
 *	Rule 34:	Required
 *			
 *	The operands of a logical && or || shall be primary expressions.
*/ 

int chkexp;

if ( mod_begin ) chkexp = 0;

if ( keyword("if") ) {
	chkexp = 1;

}

if ( op_if ) {

	chkexp = 0;
}

if ( op_log_or || op_log_and ) {



	if ( chkexp ) {

		if ( (op_parened_operand(1) & op_parened_operand(2)) == 0 ) {
				
			if ( all_digit( prev_token() ) ) {

			    warn( 34, "operands of && or || must be primary-expression.");
			}
		}
	}
		
}

/*
 *	Rule 35:	Required
 *			
 *	Assignment operators shall not be used in expressions which return
 *	Boolean values.
*/ 

if ( op_assign ) {

	if ( chkexp ) {

		WARN_MR( 35, "Assignment operators shall not be used in expressions." );
	}
}

/*
 *	Rule 36:	Advisory
 *			
 *	Logical operators should not be confused with bitwise operators.
 *
 *	Not automatically enforceable.  Requires manual inspection techniques.
*/ 

if ( op_bit_or || op_bit_and || op_bit_not ) {

	if ( chkexp ) {

		WARN_MA( 36, "Logical operators should not be confused with bitwise operators." );
	}
}

/*
 *	Rule 37:	Required
 *			
 *	Bitwise operations shall not be performed on signed integer types.
*/ 

if ( op_bitwise || op_right_shift || op_left_shift ) {

	if ( op_base(1) == INT_TYPE ) {
		
	  WARN_MR( 37, "Bitwise operations shall not be performed on signed integer types.");
	}
}

/*
 *	Rule 38:	Required
 *			
 *	The right hand operand of a shift operator shall lie between zero
 *	and one less than the width in bits of the left hand operand
 *	(inclusive).
*/ 

int curConstant;

if (  op_right_shift || op_left_shift ) {

	
	if ( ( curConstant < 0 || curConstant > 31 )  ) {


	  WARN_MR( 38, "Right hand operand shift operator shall lie between 0 & 31.");
	}
	
}


if ( lex_constant ) {

	if ( lex_constant == CONST_INTEGER ) {

		curConstant = atoi(token());

		if ( CMPPREVTOK( "-" ) ) curConstant = (-curConstant);

	}

	else {

		curConstant = -1;
	}
}


/*
 *	Rule 39:	Required
 *			
 *	The unary minus operator shall not be applied to an unsigned
 *	expression.
*/ 

if ( op_negate ) {

   if ( strcmp( prev_token(), ")" ) == 0 ) {
	 if ( (op_base(1) >= UCHAR_TYPE) && (op_base(1) <= ULONG_TYPE) ) {
		WARN_MR( 39, "Unary minus shall not be applied to unsigned expr" )
	 }
   }
}

/*
 *	Rule 4:	Advisory
 *			
 *	Provision should be made for run-time checking.
 *
 *	Not automatically enforceable.  Requires manual inspection techniques.
*/ 

// INFORMATIONAL

/*
 *	Rule 40:	Advisory
 *			
 *	The sizeof operator should not be used on expressions that contain
 *	side effects.
*/ 

int sizeof_side_effect;

if ( op_postfix ) {

	sizeof_side_effect=1;

}


if ( op_sizeof ) {

	if ( sizeof_side_effect ) {
		WARN_MA( 40, "sizeof operator should not be used on side-effects" );
	}

	sizeof_side_effect = 0;
}

/*
 *	Rule 41:	Advisory
 *			
 *	The implementation of integer division in the chosen compiler
 *	should be determined, documented and taken into account.
 *
 *	This is not enforceable but there are some troublesome cases which
 *	are worth flagging if the static analyser is sufficiently
 *	sophisticated.
*/ 

	if ( op_div_assign || op_rem_assign ) {

		WARN_MA( 41, "implementation of integer division" );
	}

/*
 *	Rule 42:	Required
 *			
 *	The comma operator shall not be used, except in the control
 *	expression of a for loop.
*/ 

int inforloop;	// signal in for loop

if ( keyword("for") ) {
	inforloop = 1;

}

if ( stm_cp_begin ) {

	if ( stm_kind == FOR ) {

		inforloop = 0;
	}
}



if ( op_comma ) {
	
	if ( inforloop == 0 ) {
		WARN_MR( 42, "Comma operator shall not be used, except in the for-loop" );

	}
}

/*
 *	Rule 43:	Required
 *			
 *	Implicit conversions which may result in a loss of information
 *	shall not be used.
*/ 

// This Rule Requires -C type checking "ON" to be invoked.

if ( cnv_truncate ) {

	WARN_MR( 43, "Implicit conversions which may result in a loss of information" );
}


/*
 *	Rule 44:	Advisory
 *			
 *	Redundant explicit casts should not be used.
*/ 


if ( op_cast ) {

	WARN_MA( 44, "Redundant explicit casts should not be used." );

}

/*
 *	Rule 45:	Required
 *			
 *	Type casting from any type to or from pointers shall not be used.
*/ 

if ( op_cast ) {

 	if ( op_level(1,0) == POINTER || op_level(2,0) == POINTER ) {

		WARN_MR(45, "Type casting from any type to or from pointers shall not be used.");
	}
}

/*
 *	Rule 46:	Required
 *			
 *	The value of an expression shall be the same under any order of
 *	evaluation that the standard permits.
*/ 

//TODO requires TWO-PASS

/*
 *	Rule 47:	Required
 *			
 *	No dependence should be placed on C's operator precedence rules in
 *	expressions.
*/ 

// This rule requires -c option ON, Full Type Checking

int binary_exp;
int in_paren;


if ( mod_begin ) {
	in_paren = 0;
	binary_exp = 0;
}
if ( op_semicolon ) {

	in_paren = 0;
	binary_exp = 0;
}

if ( op_add||op_subt||op_mul||op_div ) {	// binary op

		
	if ( op_parened_operand(1) || op_parened_operand(2) ) in_paren++;

	binary_exp++;
}

if ( op_assign ) {

	if ( (binary_exp > 2) && in_paren == 0 ) {

		WARN_MA( 47, "No dependence should be placed on C's operator precedence" );
	}
}

/*
 *	Rule 48:	Advisory
 *			
 *	Mixed precision arithmetic should use explicit casting to generate
 *	the desired result.
 *
 *	Not automatically detectable statically in all cases.  The
 *	following should give an indication.
*/ 

// requires -c option, full type checking

int init_type;	// type at dcl-init
int arith_type;	// type of arithmetic if used

if ( mod_begin ) {

	init_type = 0;
	arith_type = 0;
}

if ( op_div || op_add || op_mul || op_subt ) {

	arith_type = op_base(1);
}

if ( dcl_initializer ) {

	init_type = dcl_base_root;
	
}

if ( op_semicolon ) {

	if ( init_type && arith_type ) {

		if ( init_type != arith_type ) {

		 WARN_MA(48, "Mixed precision arithmetic should use explicit casting" );
		}
	}
	
	init_type = 0;
	arith_type = 0;
}


/*
 *	Rule 49:	Advisory
 *			
 *	Tests of a value against zero should be made explicit, unless the
 *	operator is effectively Boolean.
 *
 *	Not automatically detectable statically in all cases.  Tools can
 *	either warn on all or no occurrences in general.
*/ 


/*
 *	Rule 5:	Required
 *			
 *	Non ISO C characters.
*/ 

if ( lex_constant ) {


	if ( CMPTOK("\\c") ) {

		WARN_MR( 5, "Non ISO C characters.");
	}
}

/*
 *	Rule 50:	Required
 *			
 *	Floating point variables shall not be tested for exact equality or
 *	inequality.
*/ 


if ( op_equal || op_not_eq ) {

	if ( op_base(1) == FLOAT_TYPE || op_base(2)==FLOAT_TYPE ) {
		WARN_MR(50, "Floating point variables shall not be tested for exact equality");
	}

}

// Misra C Enforcement Testing
// Rule 51: Advisory
// Evaluation of constant unsigned integer expressions should not lead
// to wrap-around.

int seen_unsigned;

if ( lex_unsigned ) {
	seen_unsigned++;
}

if ( pp_if_depth ) {

	if ( seen_unsigned ) {

		WARN_MA( 51, "Evaluation of constant unsigned integer expressions." );
	}
}

/*
 *	Rule 52:	Required
 *			
 *	There shall be no unreachable code.
*/ 

//TODO 

/*
 *	Rule 53:	Required
 *			
 *	All non-null statements shall have a side-effect.
*/ 

//TODO

/*
 *	Rule 54:	Required
 *			
 *	A null statement shall only occur on a line by itself, and shall
 *	not have any other text on the same line.
*/ 

if ( stm_semicolon ) {

	WARN_MR( 54, "Null statement shall only occur on a line by itself." );
}


/*
 *	Rule 55:	Advisory
 *			
 *	Labels should not be used, except in switch statements.
*/ 

if ( op_colon_1 ) {

	if (  stm_depth == 0 ) {

		WARN_MA( 55, " Labels should not be used, except in switch statements.");
	}

}

/*
 *	Rule 56:	Required
 *			
 *	The goto statement shall not be used.
*/ 

if ( op_goto ) {

	WARN_MR( 56, "The goto statement shall not be used.");
}

/*
 *	Rule 57:	Required
 *			
 *	The continue statement shall not be used.
*/ 

if ( op_continue ) {

	WARN_MR( 57, "The continue statement shall not be used.");
}

/*
 *	Rule 58:	Required
 *			
 *	The break statement shall not be used (except to terminate the
 *	cases of a switch statement).
*/ 

int in_iter;

if (op_do || op_for || op_while_1 || op_while_2 ) in_iter = 1;
if (  op_switch || op_if ) in_iter = 0;

if ( op_break ) {

	if ( in_iter ) {

		WARN_MA( 58, "Break statement shall not be used, except in switch.");
	}

}

/*
 *	Rule 59:	Required
 *			
 *	The statements forming the body of an if, else if, else, while, do
 *	... while or for statement shall always be enclosed in braces.
*/ 
if ( idn_variable ) {

	if ( stm_container <= SWITCH && chkexp == 0 ) {

	  WARN_MR( 59, "Statements in body of if, else if, else, while, do must be in braces.");
	}
}

/*
 *	Rule 6:	Required
 *			
 *	ISO 10646-1 conformance.
 *
 *	Not automatically enforceable.  Requires manual inspection techniques.
*/ 

// statement of fact

/*
 *	Rule 60:	Advisory
 *			
 *	All if, else if constructs should contain a final else clause.
*/ 

int select_else; // signal whether last select statement had else

if ( stm_need_comp ) {

	if ( stm_if_else == 0 && stm_kind == ELSE && select_else == 0 ) {
	
		WARN_MA( 60, "All if, else if constructs should contain a final else clause.");
	}
}


if ( stm_is_select ) {

	select_else = stm_if_else;
}

/*
 *	Rule 61:	Required
 *			
 *	Every non-empty case clause in a switch statement shall be
 *	terminated with a break statement.
*/ 

if (stm_no_break) {

	WARN_MR( 61, "Every non-empty case in switch must have break.");
}

/*
 *	Rule 62:	Required
 *			
 *	All switch statements should contain a final default clause.
*/ 

int seen_default;	// keep track of last default seen for second case of this rule

if ( stm_no_default ) {	// std default not in switch

	WARN_MR( 62, "All switch statements should contain a final default clause.");
}

if ( keyword("default") ) {
	seen_default = 1;
}

if (stm_switch_cases) {
	seen_default = 0;

}

if ( keyword("case") ) {	// there is a case after a default

	if ( seen_default ) {

		WARN_MR( 62, "All switch statements should contain a final default clause.")
	}
}

/*
 *	Rule 63:	Advisory
 *			
 *	A switch expression should not represent a Boolean value.
*/ 

if ( stm_relation ) {


	if ( stm_kind == SWITCH ) {
		WARN_MA(63, "A switch expression should not represent a Boolean value.");

	}
}

/*
 *	Rule 64:	Required
 *			
 *	Every switch statement shall have at least one case.
*/ 

if (stm_switch_cases) {

	if ( stm_switch_cases <= 1 ) {

		WARN_MR( 64, "Every switch statement shall have at least one case.");

	}

}

/*
 *	Rule 65:	Required
 *			
 *	Floating point variables shall not be used as loop counters.
*/ 

// inforloop is defined by rule 42

if ( op_assign ) {
 if ( inforloop && (op_base(1) == FLOAT_TYPE || op_base(1)==DOUBLE_TYPE) ) {

	WARN_MR(65, "Floating point variables shall not be used as loop counters.");

 }
}

/*
 *	Rule 66:	Advisory
 *			
 *	Only expressions concerned with loop control should appear within a
 *	for statement.
 *
 *	This is in general difficult to detect automatically and statically.
 *	The following gives a simple case.
*/ 

int for_assign_count;

if ( op_for ) for_assign_count = 0;

if ( op_assign ) {

	if ( inforloop && in_paren ) for_assign_count++;

	if ( inforloop && for_assign_count > 1 ) {
		WARN_MA( 66, "Only expressions concerned with loop control should appear.");
	}
}

/*
 *	Rule 67:	Advisory
 *			
 *	Numeric variables being used within a for loop for iteration
 *	counting should not be modified in the body of the loop.
 *
 *	This is in general difficult to detect automatically and statically.
 *	The following gives a simple case.
*/ 

char for_itername[MAXIDLEN];

if ( op_assign ) {

	if ( inforloop && in_paren && strlen(for_itername) == 0 ) {
	
		strcpy( for_itername, idn_name() ); // get iter name
	}
	else if ( inforloop && (in_paren == 0) && (strcmp(for_itername,idn_name())==0) ) {

		WARN_MA( 67, "For loop iteration variables should not be modified in body.");

	}
}

/*
 *	Rule 68:	Required
 *			
 *	Functions shall always be declared at file scope.
*/ 

int inFunction;

if ( fcn_begin ) inFunction = 1;

if ( fcn_end ) inFunction = 0;

if ( dcl_function ) {

	if ( inFunction ) {

		WARN_MR( 68, "Functions shall always be declared at file scope.");

	}
}

/*
 *	Rule 69:	Required
 *			
 *	Functions with variable numbers of arguments shall not be used.
*/ 

if ( op_macro_call ) {
	
	if ( CMPTOK("va_start") ||  CMPTOK("va_arg")  ||  CMPTOK("va_end") ) {
	  
	  WARN_MR( 69, "Functions with variable numbers of arguments shall not be used.");	
	}

}



if ( dcl_3dots ) {

	WARN_MR( 69, "Functions with variable numbers of arguments shall not be used.");
}

/*
 *	Rule 7:	Required
 *			
 *	No trigraphs
*/ 

if ( lex_str_trigraph ) {

	WARN_MR( 7, "No Trigraphs" );
}


/*
 *	Rule 70:	Required
 *			
 *	Functions shall not call themselves either directly or indirectly.
 *
 *	It is trivial to detect direct recursion, but it can in some cases
 *	be impossible to detect indirect recursion statically.
*/ 

if ( op_open_funargs && op_declarator == 0 )
{
        if ( strcmp( fcn_name(), prev_token() ) == 0 ) {

                WARN_MR( 70, "Functions shall not call themselves" );
        }
}

/*
 *	Rule 71:	Required
 *			
 *	Functions shall always have prototype declarations and the
 *	prototype shall be visible at both the function definition and the
 *	call.
*/ 

if ( idn_no_prototype ) {

	WARN_MR( 71, "Functions shall always have prototype declarations." );
}

/*
 *	Rule 72:	Required
 *			
 *	For each function parameter the type given in the declaration and
 *	definition shall be identical, and the return types shall also be
 *	identical.
*/ 

if ( dcl_conflict ) {

	WARN_MR( 72, "Function parameter given in declaration/definition must be same.");
}

/*
 *	Rule 73:	Required
 *			
 *	Identifiers shall be given either for all of the parameters in a
 *	function prototype declaration or for none.
*/ 

int parm_id;


if ( dcl_parameter ) {


	if ( dcl_parameter > 1 ) {

		if ( parm_id != ((dcl_name()[0])>0) ) {

			WARN_MR(73, "Identifiers shall be given for all parameters." );
		}
	}

	if ( (dcl_name()[0]) > 0 ) parm_id = 1;
	else parm_id = 0;


}


/*
 *	Rule 74:	Required
 *			
 *	If identifiers are given for any of the parameters, then the
 *	identifiers used in the declaration and definition shall be
 *	identical.
*/ 

//TODO requires two pass

/*
 *	Rule 75:	Required
 *			
 *	Every function shall have an explicit return type.
*/ 


if ( dcl_no_specifier ) {

	if ( dcl_level(0) == FUNCTION ) {

		WARN_MR(75, "Every function shall have an explicit return type.");
	}
}

/*
 *	Rule 76:	Required
 *			
 *	Functions with no parameters shall be declared with parameter type
 *	void.
*/ 

if ( dcl_oldstyle ) {

	WARN_MR(76, "Functions with no parameters shall be declared with parameter type void.");

}

/*
 *	Rule 77:	Required
 *			
 *	The unqualifed type of parameters passed to a function shall be
 *	compatible with the unqualified expected types defined in the
 *	function prototype.
*/ 


/*
 *	Rule 78:	Required
 *			
 *	The number of parameters passed to a function shall match the
 *	function prototype.
*/ 


/*
 *	Rule 79:	Required
 *			
 *	The values returned by void functions shall not be used.
*/ 


/*
 *	Rule 8:	Required
 *			
 *	No multibyte characters or wide string literals
*/ 


/*
 *	Rule 80:	Required
 *			
 *	Void expressions shall not be passed as function parameters.
*/ 

int inArglist;

if ( op_call ) inArglist = 0;

if ( idn_function ) inArglist = 1;

if ( op_cast ) {

	if ( inArglist ) {

		if ( op_base(2) == VOID_TYPE ) {

			WARN_MR( 80, "Void expressions shall not be passed as function parameters.");
		}
	}

}

/*
 *	Rule 81:	Advisory
 *			
 *	const qualification should be used on function parameters which are
 *	passed by reference, where it is intended that the function will
 *	not modify the parameter.
*/ 

if ( dcl_parameter ) {

	if ( dcl_level(0) == POINTER && (dcl_level_flags(dcl_levels) & CONST_FLAG) == 0 ) {

		WARN_MA( 81, "const should be used on function reference parameters" );
	}

}

/*
 *	Rule 82:	Advisory
 *			
 *	A function should have a single point of exit.
*/ 

int returnCount;

if ( fcn_begin ) returnCount = 0;

if ( op_return ) returnCount++;

if ( fcn_end ) {

	if ( returnCount > 1 ) {


		WARN_MA( 82, "A function should have a single point of exit." );
	}
}


/*
 *	Rule 83:	Required
 *			
 *	For functions with non-void return type:
 *		i) 	there shall be one return statement for every exit
 *			branch (including the end of the program),
 *		ii)	each return shall have an expression,
 *		iii)	the return expression shall match the declared return
 *			type.
*/ 

int curFbase;

if ( dcl_function ) {

	curFbase = dcl_base_root;
}

if ( stm_return_void ) {

	if ( curFbase != VOID_TYPE ) {

		WARN_MR( 83, "Non void return type must be explicit expression." );
	}


}

if ( op_return ) {

	if ( op_base(1) != curFbase ) {
	
		WARN_MR( 83, "Non Void return type must match." );
	}
}

/*
 *	Rule 84:	Required
 *			
 *	For functions with void return type, return statements shall not
 *	have an expression.
*/ 

if ( op_return ) {

	if ( curFbase == VOID_TYPE && op_base(1) != 0 ) {
	
		WARN_MR( 84, "Functions with void return shall not return expression." );
	}
}

/*
 *	Rule 85:	Advisory
 *			
 *	Functions called with no parameters should have empty parentheses.
*/ 

if ( idn_variable  ) {



	if ( idn_level(0) == FUNCTION && strcmp( token(), "(" ) ) {

		WARN_MA( 85, "Functions with no parameters should have empty parentheses.");
	}

}

/*
 *	Rule 86:	Advisory
 *			
 *	If a function returns error information, then that error
 *	information shall be tested.
*/ 

//TODO poor problem statement - see misra

/*
 *	Rule 87:	Required
 *			
 *	#include statements in a file shall only be preceded by other
 *	pre-processor directives or comments.
*/ 


int varCount;

if ( mod_begin ) varCount = 0;

if ( dcl_variable ) {

	varCount++;

}

if ( pp_include ) {


	if ( varCount > 0 ) {

	  WARN_MR( 87, "#include statements only be preceded by pre-processor directives.");
	}


}

/*
 *	Rule 9:		Required
 *			
 *	No nested comments
*/ 


if ( lex_nested_comment ) WARN_MR( 9, "No nested comments" )

/*
 *	Rule 90:	Required
 *			
 *	C macros shall only be used for symbolic constants, function-like
 *	macros, type qualifiers and storage class specifiers.
*/ 


if ( pp_macro ) {


	if ( CMPTOK("{") || CMPTOK("(") || CMPTOK("int") || CMPTOK("long") || CMPTOK("void") 
		|| CMPTOK("signed") || CMPTOK("unsigned") || CMPTOK("float") ) {
		
		WARN_MR( 90, "C macros shall only be used for constants and function macros.");


	}

}

/*
 *	Rule 91:	Required
 *			
 *	Macros shall not be #defined'd and #undef'd in a block.
*/
 
int inBrace;	// in curly brace

if ( op_open_brace ) inBrace++;

if ( op_close_brace ) inBrace--;

if ( lin_preprocessor ) {

	if ( inBrace &&  
		((lin_preprocessor==DEFINE_PP_LIN) || (lin_preprocessor==UNDEF_PP_LIN)) ){
		
		 WARN( 91, "Macros shall not be #defined'd and #undef'd in a block." )
	}
}

/*
 *	Rule 92:	Advisory
 *			
 *	#undef should not be used.
*/ 

if ( lin_preprocessor ) {

	if ( lin_preprocessor == UNDEF_PP_LIN ) {

		WARN_MA( 92, "	#undef should not be used." );
	}
}

/*
 *	Rule 93:	Advisory
 *			
 *	A function should be used in preference to a function-like macro
*/ 

if ( pp_arg_count > 0 && pp_lowercase ) {

	WARN( 93, "A function should be used in preference to a function-like macro" )
}

/*
 *	Rule 94:	Required
 *			
 *	A function-like macro shall not be 'called' without all its
 *	arguments.
*/ 

// BUILTIN CodeCheck Warning

/*
 *	Rule 95:	Required
 *			
 *	Arguments to a function-like macro shall not contain tokens that
 *	look like pre-processing directives.
*/ 

// BUILTIN CodeCheck Warning Warning W1050: Runtime defined

/*
 *	Rule 96:	Required
 *			
 *	In the definition of a function-like macro the whole definition,
 *	and each instance of a parameter, shall be enclosed in parentheses.
*/ 

if ( pp_arg_paren ) WARN( 96, "function-like macro parameter, shall be enclosed in paren" )

/*
 *	Rule 97:	Advisory
 *			
 *	Identifiers in pre-processor directives should be defined before
 *	use.
*/ 

if ( pp_not_defined ) WARN( 97, "Identifiers in pre-processor directives should be defined" )

/*
 *	Rule 98:	Required
 *			
 *	At most one of # or ## in a single macro definition
*/ 


int ppStr;

if ( lin_end  ) {

	ppStr = 0;
}



if ( pp_paste ) { 

	ppStr++; 


	if ( ppStr > 1 ) {

		WARN_MR( 98, "At most one of # or #\# in a single macro definition.");
	}

}

if ( pp_stringize ) { 


 	ppStr++; 

	if ( ppStr > 1 ) {

		WARN_MR( 98, "At most one of # or #\# in a single macro definition.");
	}


}



/*
 *	Rule 99:	Required
 *			
 *	All uses of the #pragma directive shall be documented and
 *	explained.
*/ 

if ( lin_preprocessor ) {

	if ( lin_preprocessor == PRAGMA_PP_LIN ) {

		WARN_MR( 99, "All uses of the #pragma directive shall be documented." );
	}
}
