//  60)	Avoid allocating and deallocating memory in different modules.

// two files 60.cpp that defines and allocates object, and 60a.cpp that de-allocates

class C60 {};

void
f60() {
   C60 *c60 = new C60 ;   // allocate c60 in 60.cpp
}