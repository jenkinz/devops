//  58)	Keep types and functions in separate namespaces unless they're specifically intended to work together.

#include <vector>
/*
namespace std {
template <class t> class vector {
};
}
*/
namespace N {

    struct X {};

    template < class t > int * operator+(t){}
}
int f58(void);

int f58(void) 
{
    std::vector<N::X> v(100);
    v[0];
}