//  61)	Don't define entities with linkage in a header file.

int i61;    // bad61

class string;
string order("new world");

void f61() {}

// good 61
extern char c61;

void f61();  // ok decl, not defn