//  42)	Don't give away your internals.

#include "ccs.cch"

int privid;

if ( fcn_begin ) {
    privid = 0;
}
if ( idn_member ) {

//warn( 42, "IM %s r%d a%d", idn_name(), idn_base, idn_access );

    if ( idn_access != PUBLIC_ACCESS ) {
        privid = lin_number;    
    }
}

if ( op_return ) {

    if ( privid == lin_number ) {
        CCS_E( 42, "Don't give away your internals." );
    }
}