//  16)	Avoid macros.

#include "ccs.cch"

// warn on use of macros
if (  lex_macro_token ) {

    CCS_C( 16, "Avoid macro - Usage." );
}

// warn on definition of macros

if ( lin_preprocessor ) {
//warn(16, "LP %d", lin_preprocessor );
    if ( lin_preprocessor == DEFINE_PP_LIN ) {

        CCS_C( 16, "Avoid macro - Definition." );
    }
}