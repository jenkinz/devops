//  18)	Declare variables as locally as possible.

#include "ccs.cch"

/*
The theory here is two problems, NOT only the decl, but the usage, the usage of the global
is as important, if NOT more important than the declaration, .e.g. if you mission is to fix code.
*/

if ( dcl_variable ) {

    if ( dcl_global && lin_within_namespace == 0 ) {

        CCS_C( 18, "Declare variables as locally as possible. [ declaration ]" );
    }
}

if ( idn_variable ) {

    if ( idn_global ) {

        CCS_C( 18, "Declare variables as locally as possible. [ usage ]" );
    }
}