// 59.	Don't write namespace usings in a header file or before an #include.


namespace moo{}	// 59.h

// bad, don't use 'using' in header-files

using moo;		// 59.h
