/*
37)	Public inheritance is substitutability. Inherit, not to reuse, but to be reused.
*/

#include "ccs.cch"



if ( op_init ) {    // cons-init

 DUMP("CONS-INIT")
}

if ( dcl_function ) {
DEBD("DF")
warn( 37,"DF %s da%d", dcl_name(), dcl_access );
}

if ( tag_bases ) {
DUMP("TB")
}

if ( dcl_parameter ) {
    DEBD("DP")
}
