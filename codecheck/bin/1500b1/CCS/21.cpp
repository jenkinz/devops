/*
21)	Avoid initialization dependencies across compilation units.
*/

namespace {
// bad 21, global declare in a namespace
    int N21 = NULL;
};