//  61)	Don't define entities with linkage in a header file.

#include "ccs.cch"


if ( dcl_definition ) {

//DEBD("DD")
    if ( lin_header && (dcl_storage_flags & EXTERN_SC)==0 ) {
        CCS_G( 61, "Don't define entities with linkage in a header file." );
    }
}