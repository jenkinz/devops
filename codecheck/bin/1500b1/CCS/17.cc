//  17)	Avoid magic numbers.


#include "ccs.cch"


if ( lex_constant ) {
//warn( 17, "LC %d k%d", lex_constant, tag_kind );

// verifty raw # and not within a tag { enum, class, ... }
    if ( lex_macro_token == 0 && tag_kind == 0  && isdigit(token()[0]) ) {

        CCS_C( 17, "Avoid magic numbers." );
    }
}