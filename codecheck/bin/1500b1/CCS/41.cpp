/*
41)	Make data members private, except in behaviorless aggregates (C-stylestructs).
*/

// The essence here is that member's that are derived from class must be private

class B41;
struct S41;

class C41 {

public:
    B41 mem41_pub;      // don't put member in public

    S41 mem41_pubstr;   // ok, C style public struct member
    int mem41_intpub;   // not a good idea to make data publicly available
private:
    B41 mem41_priv;     // good, member private
    int mem41_intpriv;
};