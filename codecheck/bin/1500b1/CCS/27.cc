/*
27)	Prefer the canonical forms of arithmetic and assignment operators.
*/

#include "ccs.cch"


if ( op_open_funargs ) {

    if ( (strncmp(op_function(),"operator",8)==0) && (strlen(token())==1) ) {
        strcpy( TEMPBUF, op_function() ); // find operator@
        strcat( TEMPBUF, "=" );           // append operator@=

// If an operator@ is seen then verify that the operator@= case is in class

        if (  lin_within_class==0 && find_scoped_root( dcl_base_name(),TEMPBUF ) == 0 ) 
        {
            CCS_D( 27, "Prefer the canonical forms of arithmetic and assignment operators." );
        }
    }

}