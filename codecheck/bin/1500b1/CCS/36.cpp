//  36)	Prefer providing abstract interfaces.

// if a class doesn't have a base, then verify that pure abstraction is used

class F36;  // dummy fwd decl
class A36 {
protected:
    A36 & operator=(const A36& rhs );
    virtual  ~A36() = 0;    // good pure abstract interface
};

class B36 : public A36, private F36 {

public:
     B36& operator=(const B36& rhs );
};

// bad abstract class without pure interface
class E36 {
protected:
    E36& operator=(const E36& rhs );
};