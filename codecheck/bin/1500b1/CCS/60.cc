//  60)	Avoid allocating and deallocating memory in different modules.

#include "ccs.cch"

// essence of algorithm is that a delete must not be to a extern allocation

int seen_delete;

if ( op_delete ) {

    seen_delete = lin_number; // record where delete seen
}

if ( idn_variable ) {
//DEBI("IV")

    if ( seen_delete == lin_number && (idn_storage_flags & EXTERN_SC) ) {
            
        CCS_G( 60, "Avoid allocating and deallocating memory in different modules." );
        
    }
}


