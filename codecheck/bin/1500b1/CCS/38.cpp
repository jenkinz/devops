//  38)	Practice safe overriding.

class B38 {
    virtual void f38( int a = 0 );  // correct
};

class D38 : public B38 {
    virtual void f38( int x = 1 );  // incorrect
    void f38a(int);
};