//  62)	Don't allow exceptions to propagate across module boundaries.

#include "ccs.cch"

// basic algorithm here is to detect that catch is only used in main() per stroustrup
// also don't throw objects that have external linkage

int inthrow;

if ( mod_begin ) {
    inthrow = 0;
}

// don't throw with external linkage objects
if ( op_throw ) {

    inthrow = 0;        // clear
}

if ( keyword("throw") ) {

    inthrow = 1;        // set
}


if ( idn_variable ) {

//warn( 62, "IV %s dsf%d inthrow%d", idn_name(), idn_storage_flags, inthrow );

    if ( inthrow && (idn_storage_flags & EXTERN_SC) ) {
        
        CCS_G( 62, "Don't allow exceptions to propagate across module boundaries." );
    }
}
// only issue catch in main()
if ( op_catch ) {

//        warn( 62, "OC fn=%s", fcn_name() );
    if ( lin_within_function ) {

        if ( strncmp( fcn_name(), "main", 4 ) != 0 ) {
            
            CCS_G( 62, "Don't allow exceptions to propagate across module boundaries." );
        }
    }

}