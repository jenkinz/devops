//  38)	Practice safe overriding.

#include "ccs.cch"

int initzero;

if ( tag_begin ) {
    initzero = lin_number;
}

if ( dcl_parameter ) {

    if ( isdigit(prev_token()[0]) ) {
        if ( atoi( prev_token() ) == 0 ) {
            initzero = lin_number ;
        }
    }
}



if ( dcl_virtual ) {

 //warn( 38, "DV %s iz=%d", dcl_name(), initzero );
    if ( initzero != lin_number ) {
        CCS_E( 38, "Practice safe overriding." );
    }
}
