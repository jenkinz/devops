//  22)	Minimize definitional dependencies. Avoid cyclic dependencies.

#include "ccs.cch"

int type;
if ( dcl_member ) {

    if ( lin_within_class && lin_source ) {
        // set idn_filename() to class-name, rather than member-name
        type = find_root( dcl_base_name() );

//warn( 22, "B:%s t%d C:%s t%d", dcl_base_name(), dcl_base, class_name(), type );
    
        if ( type && dcl_simple == 0 ) 
        {
            if ( strncmp(file_name(),idn_filename(),BIGIDLEN) ) 
            {
             CCS_C( 22, "Minimize definitional dependencies. Avoid cyclic dependencies." );
            }
        }
    }
}