// 31)	Don't write code that depends on the order of evaluation of function arguments.

#include "ccs.cch"

char pidn_name[MAXIDLEN+1];

if ( op_open_funargs ) {
    pidn_name[0] = 0;
}

if ( op_pre_incr  ) {

    if ( incall ) 
    {
        if ( strncmp(idn_name(),pidn_name,MAXIDLEN)==0 ) {
            CCS_D( 31, "Don't write code that depends on the order of evaluation of function arguments." );
        }
        strncpy( pidn_name, idn_name(), MAXIDLEN );
    }
    
}

if ( op_return ) {

    if ( strstr(next_token(),"++") ) {
        CCS_D( 31, "Don't write code that depends on the order of evaluation of function arguments." );
    }
}