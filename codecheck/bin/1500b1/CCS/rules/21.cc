/*
21)	Avoid initialization dependencies across compilation units.
*/

#include "ccs.cch"

if ( dcl_variable ) {

    if ( lin_within_namespace ) {

        CCS_C( 21, "Avoid initialization dependencies across compilation units." );
    }
}