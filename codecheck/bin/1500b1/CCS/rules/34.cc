/* 
34)	Prefer composition to inheritance.
*/

#include "ccs.cch"


if ( dcl_access_adjust ) {
//warn(342, "DAA %s::%s da=%d tba=%d", dcl_scope_name(), dcl_name(), dcl_access, tag_base_access );
    CCS_E( 34, "Prefer composition to inheritance." );
}