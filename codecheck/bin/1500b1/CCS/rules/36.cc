//  36)	Prefer providing abstract interfaces.


#include "ccs.cch"

int pureseen;

if ( tag_begin  ) {

    if ( tag_kind == CLASS_TAG ) {
       pureseen = 0;
    }
}

if ( tag_end ) {

    if ( tag_kind == CLASS_TAG ) {
//warn(36,"TE  p=%d tb=%d",  pureseen, tag_bases );
        if ( tag_bases == 0 && pureseen == 0 ) {
            CCS_E( 36, "Prefer providing abstract interfaces." );
        }
        pureseen = 0;
    }
}

if ( dcl_pure ) {

    pureseen = lin_number;
}





    
