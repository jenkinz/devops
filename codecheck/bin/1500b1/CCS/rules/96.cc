/*
96)	Don't memcpy or memcmp non-PODs.
*/
#include "ccs.cch"

if ( idn_function ) {
//DEBI("IF")

    if ( CMPPREVTOK("memcpy") || CMPPREVTOK("memcmp") ) {

        CCS_K( 96, "Don't memcpy or memcmp non-PODs." );
    }
}