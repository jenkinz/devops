//  23)	Make header files self-sufficient.

#include "ccs.cch"

if ( lin_preprocessor ) {

    if ( lin_header && lin_header == PRJ_HEADER ) {
//warn( 23, "LP-LH %d", lin_preprocessor );

        if ( lin_preprocessor == INCLUDE_PP_LIN ) {

          CCS_C( 23, "Make header files self-sufficient." );
        }
    }
}