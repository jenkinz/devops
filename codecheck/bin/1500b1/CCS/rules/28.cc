/*
28)	Prefer the canonical form of ++ and --. Prefer calling the prefix forms.
*/

#include "ccs.cch"

if ( op_open_funargs ) {

    if ( (strncmp(op_function(),"operator",8)==0) && (strlen(token())==2) ) {
        strcpy( TEMPBUF, op_function() );   // find operator@
//        strcat( TEMPBUF, "=" );           // append operator@=
//warn( 128, "TEMP %s", TEMPBUF );
// If an operator@ is seen then verify that the operator@= case is in class

        if (  lin_within_class==0 && find_scoped_root( dcl_base_name(),TEMPBUF ) == 0 ) 
        {
            CCS_D( 27, "Prefer the canonical forms of arithmetic and assignment operators." );
        }
    }

}


if ( dcl_parameter ) {
warn( 228, "DP %s", dcl_name() );
DEBD("DP")
}

if ( dcl_function ) {
  //  if ( dcl_definition ) {

DEBD("DF")
DEBDL("DF")
  //  }
}

if ( dcl_parm_count ) {
warn(528, "DPC %d", dcl_parm_count );
}

if ( op_call ) {
DEBO("OC")
}

if( op_call_overload ) {
    DEBO("OCO")
}

if ( idn_variable ) {
 warn(428, "IV %s %d bn=%s", idn_name(), idn_base, idn_base_name() );
 DEBD("IV")
 DEBDL("IV")
}
