
//  63)	Use sufficiently portable types in a module's interface.

#include "ccs.cch"

// algorithm: verify that return and/or arg of function are either simple or from std::
// the algorithm assumes that ALL cstdlib scope interface is explicit, e.g. std::

// user trigger determine whether abstract object is derived from cstdlib
int verify_port;    // user trigger

// function param decl
if ( dcl_parameter  ) {

//warn(62, "DP %s::%s b%d", dcl_scope_name(), dcl_name(), dcl_base );

    if ( dcl_base >= ENUM_TYPE ) 
        verify_port = 1;            // fire
}

// function dcl
if ( dcl_function ) {   

//warn(62, "DF %s::%s b%d", dcl_scope_name(), dcl_name(), dcl_base );
    if ( dcl_base >= ENUM_TYPE ) 
        verify_port = 1;            // fire
}

// if abstract interface then explicit cstdlib interface scope required
if ( verify_port ) {

    if ( dcl_scope_name() != 0 ) {          // explicit scope::
        if ( strncmp( dcl_scope_name(), "std", 3) != 0 ) {
            CCS_G( 63, "Use sufficiently portable types in a module's interface." );
        }
    }
    else {                                  // no explicit scope
        CCS_G( 63, "Use sufficiently portable types in a module's interface." );
    }
}






















