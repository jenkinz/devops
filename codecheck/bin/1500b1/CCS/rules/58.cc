//  58)	Keep types and functions in separate namespaces unless they're specifically intended to work together.

#include "ccs.cch"

if ( dcl_template ) {
 DEBD("DT")
}
if ( dcl_variable ) {
DEBD("DV")
}
if ( dcl_parameter ) {
DEBD("DP")
}

if ( dcl_conflict ) {

DEBD("DC")
}

if ( dcl_overload ) {

DEBD("DO")

}

if (dcl_local_dup) {

DEBD("DD")

}

if ( dcl_no_prototype ) {

DEBD("DNP")
}