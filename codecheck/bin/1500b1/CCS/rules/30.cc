// 30)	Avoid overloading &&, ||, or , (comma).

#include "ccs.cch"


if ( dcl_function ) {
//warn(30, "DF %s %d", fcn_name(), dcl_base );
    if ( lin_within_class && strncmp(dcl_name(),"operator",8)==0 ) {
        if ( strstr(dcl_name(),"&&") || strstr(dcl_name(),"||") || strstr(dcl_name(),",") ) 
        {
            CCS_D( 30, "Avoid overloading &&, ||, or , (comma).");
        }
    }
}


