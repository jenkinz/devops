
//25)	Take parameters appropriately by value, (smart) pointer, or reference.

#include "ccs.cch"

if ( dcl_parameter ) {

    if ( dcl_base == CLASS_TYPE ) {

        if ( dcl_level(0) != REFERENCE ) {
            CCS_D( 25, "Take parameters appropriately by value, (smart) pointer, or reference." );
        }
    }
}