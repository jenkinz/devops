//  62)	Don't allow exceptions to propagate across module boundaries.

void f62a( char *) throw (  char *  ); // defined in 62a.cpp

extern char *cp;

char * f62a( char * chp ) throw ( char * )
{
    try {
// bad this throw is based on object outside of this module
        throw f62a( cp );
        throw cp ;
    }
    catch(...) {}

}