/*
26)	Preserve natural semantics for overloaded operators.
*/


#include "ccs.cch"



if ( op_open_funargs ) {

    if ( lin_within_class && strncmp(op_function(),"operator",8)==0 ) {
        if ( strchr(op_function(),'+') || strchr(op_function(),'*')  ) 
        {
            CCS_D( 26, "Preserve natural semantics for overloaded operators.");
        }
    }
}