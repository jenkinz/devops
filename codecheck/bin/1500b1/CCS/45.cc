//  45)	Always provide new and delete together.

#include "ccs.cch"
int newseen;
int deleteseen;

if ( tag_begin && tag_kind == CLASS_TAG ) {
    newseen = deleteseen =0;
}

if ( tag_end && tag_kind == CLASS_TAG ) {
    if ( newseen > 0 && newseen != deleteseen ) {

        CCS_E( 45, "Always provide new and delete together." );
    }
    newseen = deleteseen = 0;
}

if ( dcl_function ) {
  //warn(40, "DF %s f%d", dcl_name(), dcl_function_flags );
  if ( dcl_function_flags & OPERATOR_FCN ) {

        if ( strstr(dcl_name(), "_new") ) newseen++;
        else if ( strstr(dcl_name(), "_delete") ) deleteseen++;
    }
}