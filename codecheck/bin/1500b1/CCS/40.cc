/*
40)	Avoid providing implicit conversions.
*/

// flag operator overloading of real types - int,float,void, ...

#include "ccs.cch"

if ( dcl_function ) {

//    warn(40, "DF %s f%d", dcl_name(), dcl_function_flags );

    if ( dcl_function_flags & OPERATOR_FCN ) {

        if ( dcl_base >= VOID_TYPE && dcl_base < ENUM_TYPE ) {   // primitive type's
            CCS_E( 40, "Avoid providing implicit conversions." );
        }
    }
}