//  33)	Prefer minimal classes to monolithic classes.
#include "ccs.cch"


/* 
the theory here is that if any of the five major components of a class are greater than
the miller-number, then the class is too big.
*/

if ( tag_end && tag_kind==CLASS_TAG ) {

        i = 5;     
        while ( i>=0 ) {

//warn( 33, "TE %d c%d", i, tag_components(i,0)  );

            if ( tag_components(i,0) > MILLER_NUM ) {

                CCS_E( 33, "Prefer minimal classes to monolithic classes." );
                i = 0;  // issue first message seen only
            }

            i--;
        };

}