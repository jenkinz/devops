//  33)	Prefer minimal classes to monolithic classes.

/* 
the theory here is that if any of the five major components of a class are greater than
the miller-number [ 7 ], then the class is too big. the definition of 'monolithic class' is
user selectable.
*/

class ostream;
class vect {

public:
    vect( int n = 10 );
    vect( const vect & v);
    ~vect();
    int ub() const;
    int lb() const;
    int & operator[](int i) const;
    vect & operator=(const vect& v);
    ostream & operator<<(ostream& out, const vect&v);
private:
       int *p;
       int size;
       int limit;
       int min;

};


