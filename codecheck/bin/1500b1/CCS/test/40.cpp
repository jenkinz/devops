/*
40)	Avoid providing implicit conversions.
*/

class C40 {
public:
     explicit C40 (  int n = 0 );
// bad 40, implicit type conversion associatd with this class
    operator double() const;
};

C40 c40(1);