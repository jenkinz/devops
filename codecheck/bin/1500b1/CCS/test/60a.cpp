//  60)	Avoid allocating and deallocating memory in different modules.

// two files 60.cpp that defines and allocates object, and 60a.cpp that de-allocates

class  C60;                 // fwd decl
extern C60 *c60;

  // C60 *c60 = new C60;   // allocate c60 in 60.cpp
void
f60a() {
// bad c60 not defined in this module
    delete  c60;
    delete (c60);
}