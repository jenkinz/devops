//  22) Minimize definitional dependencies. Avoid cyclic dependencies.

// fwd decl Child
class Child;

// define parent in header 22p.h

class Parent {
// bad 22, Parent depends on Child
    Child * child;
};
