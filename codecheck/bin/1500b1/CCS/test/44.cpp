//  44)	Prefer writing nonmember nonfriend functions.


class C44 {
    friend C44 operator+( const C44&, const C44& );
    friend C44& operator=( const C44& );
public:
    C44();
} ;