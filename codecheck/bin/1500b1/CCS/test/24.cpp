//  24)	Always write internal #include guards. Never write external #include guards.

// verify that 24.h has guard
// good 24, 24.h has correct guard
#include "24.h"

// bad 24
#include "24bad.h"