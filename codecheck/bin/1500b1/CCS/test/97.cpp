// 97)	Don't use unions to reinterpret representation.

union {
    long l;
    char *pv;
};

char ch, *pch=&ch;
long int lv, *lvp=&lv;

f97 () {
// bad 97
pch = pv;

// bad 97
lv = l;

}

