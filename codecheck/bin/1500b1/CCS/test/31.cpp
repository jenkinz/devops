// 31)	Don't write code that depends on the order of evaluation of function arguments.

int pow31( int, int );
// bad 31, don't return side-effect
int inc31( int &x ) { return ++x; }

int
f31( ) {
 int k=1;
// bad 31, don't call with multiple side-effect on same object
 k = pow31( ++k, ++k ) ;
 k = pow31( inc31(k),inc31(k) );

 return k ;
 }