/*
26)	Preserve natural semantics for overloaded operators.
*/

class setint {
// bad 26, dangerous to overload primitive operators in local project code
    friend setint& operator+(setint&);
    friend setint& operator*(setint&);
};