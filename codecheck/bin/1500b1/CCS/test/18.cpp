//  18)	Declare variables as locally as possible.

/*
The theory here is two problems, NOT only the decl, but the usage, the usage of the global
is as important, if NOT more important than the declaration, .e.g. if you mission is to fix code.
*/

// bad 18, declaration
int G18;

void
F18() {
// good 18    
    int g18;

// bad 18, using a local
    ++G18;

// good 18
    ++g18;
}