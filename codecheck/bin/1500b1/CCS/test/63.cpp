//  63)	Use sufficiently portable types in a module's interface.

//#include <iostream>     // load namespace std;

//using namespace std;  // here its explicit

// for simplictiy create our own silly little namespace std;

namespace std {
    class string;
}

class C63;  // dummy fwd decl

// good portable, stdlib param and return
std::string F63a( const std::string& );

// good portable low level types
int F63b( int, char*, int ** );

// not portable, two warnings, function and parameter are abstract and not stdlib
C63 *F63a( C63 & );

