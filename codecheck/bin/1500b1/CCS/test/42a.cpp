//  42)	Don't give away your internals.


class C42 {

public:
    int f42() const ;
int C42::f42() const { 

// here the public function f42 is 'giving away' private data
    return spoon_; 
}

private:
    int spoon_;         //  loader for feeding OS, restricted use

};

