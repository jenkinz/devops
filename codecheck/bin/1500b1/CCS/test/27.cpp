/*
27)	Prefer the canonical forms of arithmetic and assignment operators.
*/

/* 
If the "L@R" case is overloaded, then the "L@=R" MUST be overloaded, and L@R MUST
be defined in terms of L@=R 
*/

class T {
   T& operator+= (T &lhs, const T& rhs ) ;
   T operator+ (T lhs, const T& rhs );

// bad 27, the minus is intentionally missing the -= case
   T operator- (T lhs, const T& rhs );
};
// good 27
T& operator+= (T &lhs, const T& rhs ) 
{
}
T operator+ (T lhs, const T& rhs )
{
}
// bad 27, no operator-=
 T operator- (T lhs, const T& rhs )
 {
}