/* 
34)	Prefer composition to inheritance.
*/

class B34{
public:

    B34();
//private:
    void f34(int);
    
};

class C34 : private B34 {

public:
    C34();

    B34::f34;       // adjustment public->private->public 
};

void C34::f34(int) {
}
