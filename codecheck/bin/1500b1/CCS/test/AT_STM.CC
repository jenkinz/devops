/*
***************************************************************************
*                                                                         *
* FILE NAME:	stm.cc                                                    *
* COPYRIGHT (C) 1994 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.            *
* AUTHOR:	Shuming Tan                     July 22, 1994             *
* PURPOSE:	This a CodeCheck rule file used to test the predefined    *
*               variables in stm_ group.                                  *
*                                                                         *
***************************************************************************
*/
#include <check.cch>
#define DOIT 0

int kind;

#if DOIT
if (stm_kind)
{
  kind=stm_kind;
  switch (kind)
  {
    case IF:
      warn(9001,"This is an IF statement");
      break;
    case ELSE:
      warn(9001,"This is an ELSE statement");
      break;
    case WHILE:
      warn(9001,"This is a WHILE statement");
      break;
    case DO:
      warn(9001,"This is a DO statement");
      break;
    case FOR:
      warn(9001,"This is a FOR statement");
      break;
    case SWITCH:
      warn(9001,"This is a SWITCH statement");
      break;
    case FCN_BODY:
      warn(9001,"This is a function body");
      break;
    case COMPOUND:
      warn(9001,"This is a compound statement");
      warn(9101,"There are %d local variables declared",stm_locals);
      warn(9102,"There are %d simple variables declared",stm_simple);
      warn(9103,"There are %d aggregate variables declared",stm_aggr);
      warn(9104,"There are %d local array elements",stm_array);
      warn(9105,"There are %d local tag members",stm_members);
      warn(9106,"There are %d never used variable",stm_unused);
      break;
    case EXPRESSION:
      warn(9001,"This is an expression statement");
      break;
    case BREAK:
      warn(9001,"This is a BREAK statement");
      break;
    case CONTINUE:
      warn(9001,"This is a CONTINUE statement");
      break;
    case RETURN:
      warn(9001,"This is a RETURN statement");
      break;
    case GOTO:
      warn(9001,"This is a GOTO statement");
      break;
    case DECLARE:
      warn(9901,"This is a declaration statement");
      break;
    case EMPTY:
      warn(9001,"This is an empty statement");
      break;
    default:
      warn(9001,"This is an unknown statement");
  }
}
  
  if (stm_cases)
    warn(9201,"There are %d case labels attached to the statement",stm_cases);

  if (stm_switch_cases)
    warn(9202,"There are %d cases found in SWITCH statement",stm_switch_cases);

  if (stm_is_expr)
    warn(9002,"This is an expression");

  if (stm_is_jump)
    warn(9003,"This is a jump statement");

  if (stm_is_high)
    warn(9004, "This is a high statement");

  if (stm_is_iter)
    warn(9005,"This is an iteration statement");

  if (stm_is_low)
  {
    warn(9006,"This is a low statement");
    warn(9203,"There are %d compound assignment operators",stm_cp_assign);
  }

  if (stm_is_nonexec)
    warn(9007,"This is a local declaration");
 
  if (stm_is_select)
    warn(9008,"This is a selection statement");

  if (stm_end)
  {
    warn(9301,"There are %d lines in this statement",stm_lines);
    warn(9302,"There are %d tokens in the statement",stm_tokens);
    warn(9303,"There are %d operators in the statement",stm_operators);
    warn(9304,"There are %d operands in the statement",stm_operands);
    warn(9305,"There are %d boolean relation operators in the statement",stm_relation);
    warn(9306,"The logical depth is %d",stm_depth);
  }
#endif

  if (stm_goto)
    warn(9401,"GOTO enters a block having auto or register initializer");
 
  if (stm_loop_back)
    warn(9402,"GOTO found transmit control back to previous label");
  
  if (stm_no_break)
    warn(9403,"The previous case did not terminate with a transfer of control");

  if (stm_no_default)
    warn(9404,"Switch statement has no default case");

  if (stm_semicolon)
    warn(9405,"A suspicious semicolon found");

  if (stm_bad_label)
    warn(9406,"A label(list) found not attached to any statement");

  if (stm_return_paren)
    warn(9407,"Return has a value not enclosed in parentheses");

  if (stm_return_void)
    warn(9408,"Return does not match the function definition");


