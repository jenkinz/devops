/*
37)	Public inheritance is substitutability. Inherit, not to reuse, but to be reused.
*/

class B37 {
public:
    B37( int a );
};

// verify that inheritance specializes the usage

class C37 : public B37 {

public:
    C37( int a ) : B37( a )     // good specialization of base class
    {
    }
};