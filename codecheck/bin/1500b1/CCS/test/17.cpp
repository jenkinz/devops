//  17)	Avoid magic numbers.

// bad 17, magic number
int i17 = 101;

// bad 17, magic
double d17 = 3.14;

// good 17
enum { e17=102 } E17;

// good 17.
int ei17 = e17;

class C17 {
// good 17
    static const SC17 = 103;
};