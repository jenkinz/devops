// 30)	Avoid overloading &&, ||, or , (comma).

class C30 {

public:
// bad 30
    C30 operator&&() ;

    C30 operator||() ;

    C30 operator,() ;

    // ok 30
    C30 operator+=();
};