//  15)	Use const proactively.

void f15( int x );

// bad 15
void f15( const int y );

// ok 15
void f15( const int z ) {
    // ...
}