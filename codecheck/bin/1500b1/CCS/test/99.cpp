/*
99)	Don't use invalid objects. Don't use unsafe functions.
*/

void m99(void)
{
 char *buf;
// bad 99, don't use unsafe functions
    sprintf(buf,"");
    strcpy( buf, buf );
    strncpy( buf, buf, 0 );
}