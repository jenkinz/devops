//  45)	Always provide new and delete together.

typedef int size_t;

// verify that the new & deletes balance

class C45A {

    static void * operator new(size_t);
    static void  operator delete(void*, size_t);
};

class C45B {

    static void * operator new(size_t);

// bad 45, for C45B the delete is missing
   // static void * operator delete(void*, size_t);
};