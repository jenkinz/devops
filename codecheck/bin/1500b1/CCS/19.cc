//   19)	Always initialize variables.

#include "ccs.cch"

// trigger on the case of a non-init being used.
if ( idn_no_init ) {    
    
    CCS_C( 19, "Always initialize variables - usage." );
}

// trigger on the case of a declare without an init
if ( dcl_variable ) {
//warn( 19, "DL %s di%d", dcl_name(), dcl_initializer ) ;
 
    if ( dcl_initializer == 0 && dcl_simple ) {

       CCS_C( 19, "Always initialize variables - declare." );
    }
}

