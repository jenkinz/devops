/*
***************************************************************************
*                                                                         *
* FILE NAME:	lex.cc                                                    *
* COPYRIGHT (C) 1994 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.            *
* PURPOSE:	This a CodeCheck rule file used to test the predefined    *
*               variables in lex_ group.                                  *
*                                                                         *
***************************************************************************
*/

if ( lex_ansi_escape )
	warn( 1001, "lex_ansi_escape = \\%c", lex_ansi_escape );

if ( lex_backslash )
	warn( 1003, "lex_backslash" );

if ( lex_bad_call )
	{
	if ( lex_bad_call > 0 )
		warn( 1004, "%d extra macro arguments found.", lex_bad_call );
	else
		warn( 1004, "%d macro arguments missing.", -lex_bad_call );
	}

if ( lex_big_octal )
	warn( 1005, "%d is not a proper octal digit!", lex_big_octal );

if ( lex_char_empty )
	warn( 1006, "empty character constant" );

if ( lex_char_long )
	warn( 1007, "Multicharacter" );

if ( lex_enum_comma )
	warn( 1008, "This comma is not portable." );

if ( lex_float )
	warn( 1009, "Token %s is a float.", token() );

if ( lex_hex_escape )
	warn( 1010, "Hexadecimal escape sequence = %d (decimal)", lex_num_escape );

if ( lex_initializer )
	{
	switch ( lex_initializer )
		{
	case 1:
		warn( 1011, "Zero initializer" );
		break;
	case 2:
		warn( 1011, "Non-zero integer initializer" );
		break;
	case 3:
		warn( 1011, "Boolean initializer" );
		break;
	case 4:
		warn( 1011, "Character initializer" );
		break;
	case 5:
		warn( 1011, "Float initializer" );
		break;
	case 6:
		warn( 1011, "String initializer" );
		break;
	default:
		warn( 1011, "Other initializer" );
		break;
		}
	}

/* use option -K4 to test this rule */
if ( lex_invisible )
	warn( 1012, "Tag %s is invisible in C++ 3.0", token() );

if ( lex_key_no_space )
	warn( 1013, "Place a space after keyword %s", token() );

if ( lex_lc_long )
	warn( 1014, "lower-case L suffix" );

if ( lex_long_float )
	warn( 1015, "Suffix L on float." );

if ( lex_macro_token )
	warn( 1016, "Token %s from macro expansion.", token() );

if ( lex_nl_eof )
	warn( 1017, "This file should end with a newline." );

if ( lex_nonstandard )
	warn( 1018, "Nonstandard character: %c", lex_nonstandard );

if ( lex_not_KR_escape )
	warn( 1019, "\\%c is not a K&R escape character.", lex_not_KR_escape );

if ( lex_not_manifest )
	warn( 1020, "%s is not a manifest constant.", token() );

if ( lex_null_arg )
	warn( 1021, "Empty macro arguments are not portable." );

if ( lex_num_escape )
	warn( 1022, "Numeric escape sequence = %d (decimal)", lex_num_escape );

if ( lex_punct_after )
	warn( 1023, "Space needed after comma or semicolon" );

if ( lex_punct_before )
	warn( 1024, "Remove space before comma or semicolon" );

if ( lex_radix )
	warn( 1025, "Radix = %d", lex_radix );

if ( lex_str_concat )
	warn( 1026, "String concatenation, length = %d", lex_str_concat );

if ( lex_str_length )
	warn( 1027, "string length = %d", lex_str_length );

if ( lex_str_macro )
	warn( 1028, "Macro name found within string." );

if ( lex_str_trigraph )
	warn( 1029, "Trigraph found within string." );

if ( lex_suffix )
	warn( 1030, "This numeric literal has a suffix." );

if ( lex_trigraph )
	warn( 1031, "Trigraph found." );

if ( lex_unsigned )
	warn( 1032, "Suffix U" );

if ( lex_wide )
	warn( 1033, "Wide string or character literal." );
