/*
***************************************************************************
*                                                                         *
* FILE NAME:	func.cc                                                   *
* COPYRIGHT (C) 1994 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.            *
* AUTHOR:	Shuming Tan                     July 27, 1994             *
* PURPOSE:	This a CodeCheck rule file used to test the predefined    *
*                functions.                                               *
*                                                                         *
***************************************************************************
*/
#include <check.cch>

#define IDENTIFIER   "id"
#define PREFIX       "p_"
#define SUFFIX       "_s"
#define HEADER       "func.h"
#define IGNORE       "const"
float input,input1;
int c;

if (mod_begin)
{
  printf("File name is : %s\n",file_name());
  printf("Time is : %s.\n",time_stamp());
  exec("ps");

  /*ignore(IGNORE);*/
  if (option('R'))
    printf("Option -R already set\n");
  printf("Option -R has value %s.\n",str_option('R'));

  set_option('m',1);
  set_str_option('L',"my");
  define("NEED","");
  undefine("NOT");

 /* test mathematical functions
  printf("Input X function LOG2(X) : ");
  scanf("%f",&input);
  printf("LOG2(%f)=%f\n",input,log2(input));

  printf("Input X function POWER(X,Y) : ");
  scanf("%f",&input);
  printf("Input Y function POWER(X,Y) : ");
  scanf("%f",&input1);
  printf("POWER(%f,%f)=%f\n",input,input1,pow(input,input1));

  printf("Input X function SQRT(X) : ");
  scanf("%f",&input);
  printf("SQRT(%f)=%f\n",input,sqrt(input));

  test character functions
  printf("Input a character : ");
  scanf("%c",&c);
  if (isalpha(c))
    printf("%c is a alphabetic character\n",c);
  if (isdigit(c))
    printf("%c is a digital character.\n",c);
  if (islower(c))
    printf("%c is a lower case character.\n",c);
  if (isupper(c))
    printf("%c is an upper case character.\n",c);
  printf("After TOUPPER %c becomes %c.\n",c, toupper(c));
  printf("After TOLOWER %c becomes %c.\n",c, tolower(c));
*/
  /* test string functions */
  if (strcmp("RIGHT","WRONG")!=0)
    printf("RIGHT!=WRONG is right\n");
  if (strcmp("WRONG","WRONG")==0)
    printf("WRONG==WRONG is right\n");
  if (strequiv("RIGHT","RIGHT"))
    printf("RIGHT==RIGHT is always right.\n");
  printf("File name %s has %d characters in it.\n",file_name(),strlen(file_name()));
  printf("The extension name of file %s is %s.\n",file_name(),strstr(file_name(),"."));
}

if (included(HEADER))
  printf("Header file func.h included.\n");

if (header_name())
{
  printf("The header file name is : %s\n",header_name());
  if (header_path())
    printf("The header path name is : %s\n",header_path());
  else 
    printf("The header path name is current directory.\n");
}

if (identifier(IDENTIFIER))
  printf("Found identifier %s at line %d.\n",token(),lin_number);

/* after ignore("const") this rule should have been disabled */
if (keyword(IGNORE)) printf("*@(*IEJEJMDNFNNBF5843963*(*&*#$&\n");

if (keyword("int"))
  printf("Found keyword %s at line %d.\n",token(),lin_number);

if (prefix(PREFIX))
{
  printf("The root of identifier %s is %s.\n",prev_token(),root());
  printf("The next character is : %c\n",next_char());
}

if (suffix(SUFFIX))
  printf("The root of identifier %s is %s.\n",prev_token(),root());

if (test_needed("malloc"))
  printf("Function call malloc need to be tested at line\n%d\t%s\n",lin_number,line());

if (macro("MACRO"))
  printf("Macro name \"MACRO\" to be expanded.\n");

if (dcl_conflict)
  printf("conflict happens in file %s at line %d.\n",conflict_file(),conflict_line);

if (lin_within_class)
  printf("TAG name:%s    CLASS name:%s\n",tag_name(),class_name());

if (mod_end)
{
  /* test statistical functions */
  printf("MAXIMUM  of FCN_OPERATORS          : %f\n",maximum(fcn_operators));
  printf("MINIMUM  of FCN_OPERATORS          : %f\n",minimum(fcn_operators));
  printf("MEAN     of FCN_OPERATORS          : %f\n",mean(fcn_operators));
  printf("MEDIAN   of FCN_OPERATORS          : %f\n",median(fcn_operators));
  printf("MODE     of FCN_OPERATORS          : %f\n",mode(fcn_operators));
  printf("STDEV    of FCN_OPERATORS          : %f\n",stdev(fcn_operators));
  printf("VARIANCE of FCN_OPERATORS          : %f\n",variance(fcn_operators));
  printf("NCASES   of FCN_OPERATORS          : %f\n",ncases(fcn_operators));
  printf("CORR of FCN_OPERATORS and OPERANDS : %f\n",corr(fcn_operators,fcn_operands));
  printf("QUANTILE of FCN_OPERATORS          : %f\n",quantile(fcn_operators,mod_functions,mod_functions));
  histogram(fcn_operators,0,0,0);
}

