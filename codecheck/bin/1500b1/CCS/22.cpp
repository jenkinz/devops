//  22)	Minimize definitional dependencies. Avoid cyclic dependencies.

// fwd decl Child
// class Child;

/*
class Parent {
// bad 22, Parent depends on Child
    Child * child;
};
*/

// define Parent in 22p.h

#include "22p.h"    

class Child {
// bad 22, Child depends on Parent, but parent is in different module
// trigger on member decl parent, as base-name Parent was defined out-of-module 
    Parent * parent;
};

