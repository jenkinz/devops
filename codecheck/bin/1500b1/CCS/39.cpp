/*
39)	Consider making virtual functions nonpublic, and public functions nonvirtual.
*/

class C39 {

public:
    virtual void f39va(int);    // bad
    void f39a(int);             // good

private:
    virtual void f39vb(int);    // good
    void f39b(int);            // bad
};