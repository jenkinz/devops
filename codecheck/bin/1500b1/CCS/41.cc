/*
41)	Make data members private, except in behaviorless aggregates (C-stylestructs).
*/

#include "ccs.cch"

if ( dcl_member ) {

//warn( 41, "DM %s dm%d a%d b%d", dcl_name(), dcl_member, dcl_access, dcl_base );

     if ( dcl_access == PUBLIC_ACCESS 
         && ( dcl_base == CLASS_TYPE || dcl_base < ENUM_TYPE ) ) {

        CCS_E( 41, "Make data members private, except in behaviorless aggregates" );
     }
}