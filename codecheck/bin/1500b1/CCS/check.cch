/* CodeCheck Copyright(C)1988-2005 by Abraxas Software Inc (R). All rights reserved. */
/*
********************************************************************

	This is the standard header for rule files based on
	CodeCheck version 11.14B1 or later.

	Copyright (c) 1988-2005 by Abraxas Software, Inc.
	All rights reserved, worldwide.

	This file defines manifest constants for the
	following CodeCheck variables and functions:
	
		dcl_base
        dcl_base_root
		dcl_function_flags
		dcl_level()
		dcl_level_flags()
		dcl_storage_flags
		lin_header
        lin_preprocessor
		lin_within_tag
		op_base()
		op_level()
		op_level_flags()
        pp_error_severity()
		stm_container
		stm_cp_begin
		stm_is_comp
		stm_kind
		tag_kind

********************************************************************
*/
/* CodeCheck Copyright(C)1988-2004 by Abraxas Software Inc (R). All rights reserved. */

#if ( CODECHECK < 1114 || CODECHECK == 1114 && BETA < 1 )
#    error Header file check.cch requires CodeCheck version 11.14b1 or higher!
#else

#define NULL	0
#define TRUE	1
#define FALSE	0


//  The values of lex_constant:

#define CONST_BOOL        1    /*  boolean constant     */
#define	CONST_ENUM        2    /*  enumerated constant  */
#define	CONST_CHAR        3    /*  character constant   */
#define	CONST_INTEGER     4    /*  integer constant     */
#define	CONST_FLOAT       5    /*  float constant       */
#define	CONST_STRING      6    /*  string constant      */


//  This values of lex_initializer:

#define INIT_ZERO         1
#define	INIT_INTEGER      2    /*  any non-zero integer  */
#define INIT_BOOL         3
#define	INIT_CHAR         4
#define	INIT_FLOAT        5
#define	INIT_STRING       6
#define	INIT_OTHER        7


//  The declarator base types for dcl_base, dcl_base_root
//  and op_base():
//  The result of explicit usage on identifier with find_root() or find_scoped_root().

#define VOID_TYPE            1
#define BOOL_TYPE            2
#define CHAR_TYPE            3
#define SHORT_TYPE           4
#define WCHAR_TYPE           5    // C++ wide char type wchar_t
#define INT_TYPE             6
#define LONG_TYPE            7
#define LONG_LONG_TYPE       8    //  Mainframe long long type
#define EXTRA_INT_TYPE       9    //  Nonstandard integer
#define UCHAR_TYPE          10    //  unsigned char
#define USHORT_TYPE         11    //  unsigned short
#define UINT_TYPE           12    //  unsigned int
#define ULONG_TYPE          13    //  unsigned long
#define EXTRA_UINT_TYPE     14    //  Nonstandard unsigned
#define FLOAT_TYPE          15
#define SHORT_DOUBLE_TYPE   16    //  Symantec short double type
#define DOUBLE_TYPE         17
#define LONG_DOUBLE_TYPE    18
#define INT8_TYPE           19    //  MSVC++, Borland C++ and IBM
#define INT16_TYPE          20    //  VisualAge for C++ extended
#define INT32_TYPE          21    //  integral types __int8, __int16
#define INT64_TYPE          22    //  __int32 and __int64
#define EXTRA_FLOAT_TYPE    23    //  Nonstandard float
#define ENUM_TYPE           24
#define UNION_TYPE          25
#define STRUCT_TYPE         26
#define CLASS_TYPE          27    //  C++ class
#define DEFINED_TYPE        28    //  Typedef name
#define EXTRA_PTR_TYPE      29    //  Nonstandard pointer
#define CONSTRUCTOR_TYPE    30    //  C++ constructor
#define DESTRUCTOR_TYPE     31    //  C++ destructor
#define TEMPLATE_TYPE       32    //  C++ template parameter
#define THREE_DOT_TYPE		33	  //  Variable Argument ... Type
#define NAMESPACE_TYPE		34	  //  C++ namespace name

#define COMP_TYPE           EXTRA_INT_TYPE      //  Macintosh extension
#define EXTENDED_TYPE       LONG_DOUBLE_TYPE    //  Macintosh extension
#define DERIVED_TYPE        DEFINED_TYPE        //  OBSOLETE: use DEFINED_TYPE
#define SEGMENT_TYPE        EXTRA_PTR_TYPE      //  Microsoft extension


//  The values of dcl_function_flags:

#define	INLINE_FCN              1
#define	VIRTUAL_FCN             2
#define	PURE_FCN                4
#define	PASCAL_FCN              8
#define	CDECL_FCN              16
#define	INTERRUPT_FCN          32
#define	LOADDS_FCN             64
#define	SAVEREGS_FCN          128
#define	FASTCALL_FCN          256
#define EXPORT_FCN            512
#define EXPLICIT_FCN         1024
#define OPERATOR_FCN	     2048			// class operator function

//  The values of dcl_level() and op_level()

#define SIMPLE          0
#define FUNCTION        1
#define REFERENCE       2
#define POINTER         3
#define ARRAY           4


//  The values of dcl_level_flags() and op_level_flags():

#define CONST_FLAG          1  //  ANSI constant pointer
#define VOLATILE_FLAG       2  //  ANSI volatile pointer
#define NEAR_FLAG           4
#define FAR_FLAG            8
#define HUGE_FLAG          16
#define EXPORT_FLAG        32  //  Windows only
#define BASED_FLAG         64  //  Microsoft only
#define SEGMENT_FLAG      128  //  Borland & Microsoft


//  The values of dcl_storage_flags:

#define EXTERN_SC      1
#define STATIC_SC      2
#define TYPEDEF_SC     4
#define AUTO_SC        8
#define REGISTER_SC   16
#define MUTABLE_SC    32
#define GLOBAL_SC     64


//  The values of lin_header:

#define PRJ_HEADER    1  //  Project header (filename in quotes)
#define SYS_HEADER    2  //  System header (filename in angle brackets)


//  Values for any of these variables:
//      stm_kind
//      stm_container
//      stm_is_comp
//      stm_cp_begin

#define IF           1   // if statement
#define ELSE         2   // else statement
#define WHILE        3   // while statement
#define DO           4   // do statement
#define FOR          5   // for statement
#define SWITCH       6   // switch statement
#define TRY          7   // try statement
#define CATCH        8   // catch statement
#define FCN_BODY     9   // function definition
#define COMPOUND    10   // compound statement
#define EXPRESSION  11   // expression statement
#define BREAK       12   // break statement
#define CONTINUE    13   // continue statement
#define RETURN      14   // return statement
#define GOTO        15   // goto statement
#define DECLARE     16   // declaration statement
#define EMPTY       17   // empty statement

/*
	Note: stm_kind is never set to FCN_BODY.
	FCN_BODY is the "container" of the outer-
	most compound statement of a function.
*/

//  The values of tag_kind and lin_within_tag: 

#define ENUM_TAG      1
#define UNION_TAG     2
#define STRUCT_TAG    3
#define CLASS_TAG     4

//  Severity levels controlling if program terminates on
//  #error directive they are passed to function call
//  pp_error_severity() as actual parameter.

#define INFO_PP       0  // not to terminate
#define ERROR_PP      1  // terminate (default)

//  The values of lin_preprocessor
#define DEFINE_PP_LIN       1
#define UNDEF_PP_LIN        2
#define INCLUDE_PP_LIN      3
#define IF_PP_LIN           4
#define IFDEF_PP_LIN        5
#define IFNDEF_PP_LIN       6
#define ELSE_PP_LIN         7
#define ELIF_PP_LIN         8
#define ENDIF_PP_LIN        9
#define PRAGMA_PP_LIN      10
#define LINE_PP_LIN        11
#define ERROR_PP_LIN       12
#define ASM_PP_LIN         13
#define ENDASM_PP_LIN      14
#define IMPORT_PP_LIN      15
#define CINCLUDE_PP_LIN    16
#define RINCLUDE_PP_LIN    17
#define RCINCLUDE_PP_LIN   18
#define INC_NEXT_PP_LIN    19
#define OPTION_PP_LIN      20

// C++ class access mechanisms

#define       PUBLIC_ACCESS    (0)
#define       PROTECTED_ACCESS (1)
#define       PRIVATE_ACCESS   (2)
#define       DEFAULT_ACCESS   (3)

// WARN Message Format Over-Ride

#define WARN_FORMAT_MSDEV	(0)
#define WARN_FORMAT_EMACS	(1)


#endif	/* CodeCheck version test */
