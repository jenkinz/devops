From: 	Leary-Coutu, Chanda[SMTP:Chanda.Leary-Coutu@AWL.com]
Sent: 	Wednesday, December 15, 2004 1:59 PM
To: 	Gordon, Peter; 'Pat'
Cc: 	Boedigheimer, Kim; Nicholls, Timothy
Subject: 	RE: C++ Std Coding Book - CopyRight Question

Hi all,

Here is the link:

http://www.awprofessional.com/title/0321113586

Thanks,
Chanda

Chanda Leary-Coutu
Senior Marketing Manager; Computer Security, C/C++, Networking, XML/Web
Services
Addison-Wesley Professional and Prentice Hall PTR
617/848-6541 (phone)
chanda.leary-coutu@pearsoned.com


>  -----Original Message-----
> From: 	Gordon, Peter  
> Sent:	Wednesday, December 15, 2004 4:00 PM
> To:	'Pat'
> Cc:	Boedigheimer, Kim; Nicholls, Timothy; Gordon, Peter; Leary-Coutu,
> Chanda
> Subject:	RE: C++ Std Coding Book - CopyRight Question
> 
> Patrick--
> 
> Sounds fine. Tim will give you the formal permission. The book's marketing
> manager, Chanda Leary-Coutu, will give you the link. Thanks for clarifying
> everything.
> 
> Btw, the TOC does appear on our site with a copyright notice.
> 
> --Peter
> 
> 	----------
> 	From: 	Pat
> 	Sent: 	Wednesday, December 15, 2004 5:14 AM
> 	To: 	Gordon, Peter
> 	Cc: 	Boedigheimer, Kim; Nicholls, Timothy
> 	Subject: 	RE: C++ Std Coding Book - CopyRight Question
> 
> 	<<File: c++ coding TOC.txt>>
> 	Peter,
> 
> 	Here are my thoughts, given that the TOC [ attached ] is widely
> available on
> 	the internet, I suggest that we place the TOC on our web-site with
> our
> 	related material and that there be a 'link' to purchase the book
> directly
> 	from the publisher, just like 'amazon.com' has or any other seller
> of books.
> 	Lot's of people don't know what the book 'is-is' so its useful to
> have a
> 	brief overview of the book, just like amazon.com has. We generally
> tell
> 	people not to bother with our SW if they have NOT bought the book.
> 
> 	We are NOT in the business of selling books.
> 
> 	It is also of my opinion that your TOC should contain an explicit
> copyright
> 	notice,
> 
> 	Again our intent is to use NO other part of the book other than the
> TOC, in
> 	fact we don't even need to see the book in order to do what we do,
> at this
> 	point in time its rather clear the the TOC is/has been de-coupled
> from the
> 	book?
> 
> 
> 	sincerely, patrick conley
> 
> 	 <<c++ coding TOC.txt>> 
> 	-----------------------------------
> 
> 	Abraxas Software, Inc.
> 	4726 SE Division St, Portland, Oregon 97206 USA
> 	TEL 503.232.0540 FAX 503.232.0543
> 	URL:     www.abraxas-software.com
> 	EMAIL:  support@abraxas-software.com
> 
> 	Most Pcyacc Examples and/or CodeCheck Rule Files, and manuals are
> available
> 	on our web/ftp site.
> 
> 
> 
> 
> 
> 
****************************************************************************
This email may contain confidential material.
If you were not an intended recipient, 
please notify the sender and delete all copies.
We may monitor email to and from our network.

****************************************************************************
