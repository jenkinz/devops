/* CodeCheck Copyright (c) 1988-98 by Abraxas Software Inc. (R).  All rights reserved. */
 
// Generate HTML INDEX from C/C++ file

#define TOCFIL "toc.sym"
FILE *symbol;
int rc;
 
if ( prj_begin )
{

    symbol = fopen( TOCFIL, "w" ); /* init symbol table */

    if ( symbol == 0 )
    {
        printf( "can not open file sym.tmp,  exit.\n" );
        exit( -1 );
    }
  
    fprintf( symbol, "PN %s\n", prj_name() );  
}

if ( prj_end ) {
	
    fclose ( symbol );

    rc = exec( "toc", TOCFIL, "toc.html" );

    if ( rc != 0 ) fatal( rc, "Toc.Exe Execution" );
    
    rc = exec( "del", TOCFIL );

    if ( rc != 0 ) fatal( rc, "TOC Deletion" );
}

if ( mod_begin ) {

  fprintf( symbol, "MN %s %s\n", mod_name(), file_name() );
}


if ( dcl_definition ) {

   if ( dcl_function )  fprintf( symbol, "FD %s %s\n", fcn_name(), dcl_base_name() );

   else if ( dcl_global ) fprintf( symbol, "GD %s %s\n", dcl_name(), dcl_base_name() );
}

