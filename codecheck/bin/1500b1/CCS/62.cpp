//  62)	Don't allow exceptions to propagate across module boundaries.


class C62{
};

class C62& c62;

static char *cp;

char * f62a( char *) throw( char* ) ; // defined in 62a.cpp
void main() throw();

void
main() throw()
{
    try {

        throw f62a( cp );

        throw cp ;

        throw( c62 );
    }
    catch( int ) {  // int handler
    }
    catch( C62 & ) {     // class handler C62
    }

}