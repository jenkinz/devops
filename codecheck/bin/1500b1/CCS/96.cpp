/*
96)	Don't memcpy or memcmp non-PODs.
*/

template <class t> class T96 {};
void
f96(void) {

    T96<int> m1(new int), m2(new int);

// bad 96, don't use memcpy
    memcpy( &m1, &m2, sizeof(m1) );

// bad 96, don't use memcmp
    if ( memcmp(m1, m2, sizeof(m1)) ) {

    }
}


