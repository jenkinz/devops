//  43)	Pimpl judiciously.

#include "ccs.cch"

int pimplseen;

if ( mod_begin ) {
    pimplseen = 0;
}

if ( dcl_parameter ) {
//warn(43,"DP dbn=%s", dcl_base_name() );
    if ( CHKSTR(dcl_base_name()) ) {
        if ( dcl_base == CLASS_TYPE && strstr(dcl_base_name(),"shared_ptr") ) 
            pimplseen++;
    }
}

if ( dcl_local ) {
//warn(43,"DL dbn=%s", dcl_base_name() );
    if ( CHKSTR(dcl_base_name()) ) {
        if ( dcl_base == CLASS_TYPE && strstr(dcl_base_name(),"shared_ptr") ) 
            pimplseen++;
    }
}

if ( mod_end ) {
    if ( pimplseen < MILLER_NUM ) {
        CCS_E( 43, "Pimpl judiciously. [ use shared_ptr<> ]" );
    }
}


