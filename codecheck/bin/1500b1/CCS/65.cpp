//  65)	Customize intentionally and explicitly.

template <class t> class T65 {  // dummy template
};

void f65i(int); // non-specialized function

template<typename T>
void f65s(T& t); // specialized function

template <typename T>
void f65(T& t) {

    f65s( t );      // intentional customization or not?
    f65i( t );
}


