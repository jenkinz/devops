// 97)	Don't use unions to reinterpret representation.

#include "ccs.cch"

if ( idn_member ) {
//warn( 97, "IM %s %d", tag_name(), idn_base );
    if ( strncmp( tag_name(), "<anonymous>", MAXIDLEN )==0 ) {
        CCS_K( 97, "Don't use unions to reinterpret representation." );
    }
}

