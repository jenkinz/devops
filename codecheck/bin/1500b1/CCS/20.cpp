//  20)	Avoid long functions. Avoid deep nesting.


F20() {
 int f20=true;

 // bad 20, > miller number of decisions
    if (f20) {}
    while (f20) {}
     if (f20) {}
    while(f20) {}
        if (f20) {}
    while (f20) {}
     if (f20) {}
    while(f20) {}
 // bad 20, if function > one page of code

    if (f20) {  
        if (f20) {  
            if (f20) {
// bad 20, deep nesting [ USER SELECTABLE - DEFAULT 3 ]
                if (f20) {
                }
            }
        }
    }
}