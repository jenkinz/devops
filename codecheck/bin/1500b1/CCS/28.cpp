/*
28)	Prefer the canonical form of ++ and --. Prefer calling the prefix forms.
*/

// if your class builds ++foo, then handle the foo++ case.

class T {
public:
   T& operator++ ();    // prefix 
   const T operator++ (int);  // postfix

// bad 28, the minus is intentionally missing postfix case
   T& operator-- ();
};

/*

// good 28
T& operator++ ( ) {}        // ++foo
const T operator++ ( int ) {}      // foo++

// bad 28, no postfix case
 T& operator-- ( ) {} // prefix imp

 main() {
     T i;
     ++i;
     i.operator++();	// prefix actuality

     i++;
     i.operator++(0);	// postfix actuality
 }

*/