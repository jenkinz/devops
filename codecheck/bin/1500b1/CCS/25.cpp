
//25)	Take parameters appropriately by value, (smart) pointer, or reference.

class c25 {

c25( );

};


// bad 25, never pass a user type by value
c25  f25a( c25 p ) {
    return ( p );
}

c25 f25b( c25 & p ) {
    return( p );
}

c25 f25c( int p )
{
    return( p );
}