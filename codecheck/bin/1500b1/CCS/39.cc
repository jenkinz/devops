/*
39)	Consider making virtual functions nonpublic, and public functions nonvirtual.
*/
#include "ccs.cch"

if ( dcl_function ) {

//warn(39, "%s %s dff=%d da=%d", class_name(), dcl_name(), dcl_function_flags, dcl_access );

    if ( dcl_member ) {

        if ( dcl_access == PUBLIC_ACCESS && dcl_function_flags & VIRTUAL_FCN ) {
            CCS_E( 39, "Consider making virtual functions nonpublic" );
        }
        else if ( dcl_access != PUBLIC_ACCESS && (dcl_function_flags & VIRTUAL_FCN)==0 ) {
            CCS_E( 39, "Consider making non-virtual functions public" );
        }
    }
}

