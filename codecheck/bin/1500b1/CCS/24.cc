//  24)	Always write internal #include guards. Never write external #include guards.


#include "ccs.cch"

int guard_seen;
char wrapper_name[MAXIDLEN+1];


if ( lin_preprocessor ) {

warn(24, "LP %d %s gs=%d", lin_preprocessor, pp_name(), guard_seen );

       switch ( lin_preprocessor ) {
        case INCLUDE_PP_LIN:
            if ( guard_seen )
                guard_seen = 0;
            else
                guard_seen = -1;

            break ;
    
        case IFNDEF_PP_LIN:         // #ifndef
            if ( guard_seen < 0 ) {
                guard_seen = lin_number;
                strncpy( wrapper_name, pp_name(), MAXIDLEN );
            }
            break;

        case DEFINE_PP_LIN:         // #define
            if ( strncmp(pp_name(),wrapper_name,MAXIDLEN)==0 
                &&  (guard_seen+1) == lin_number ) {

//warn( 24, "CORRECT");    
            }
            else {
                CCS_C( 24, "Always write internal #include guards." );
            }
            guard_seen = 0;
            break;

        default:            
            // catch all case, empty headers ... external
            if ( guard_seen ) {

                CCS_C( 24, "Always write internal #include guards." );
            }
            guard_seen = 0;         // done
            break;
    }
}