//   19)	Always initialize variables.

void
f19() {

// bad 19 declare
// trigger on the case of a declare without an init
    int l19;

// good 19
    int l19ok = 1;

// bad 19   usage
// trigger on the case of a non-init being used.
    if ( l19 ) { 
    }

// good 19
    if ( l19ok ) {
    }
}