//  43)	Pimpl judiciously.



template <class t> class shared_ptr ;

class C43 {
    C43& operator=(const C43&) {
        shared_ptr<Impl>   temp( new Impl() );
    }
private:
    struct Impl;
    shared_ptr<Impl> pimpl_;
};


// boost example

/*
namespace boost {
    template <class t> class shared_ptr ;
}
struct Foo;

typedef boost::shared_ptr<Foo> FooPtr;


struct FooPtrOps
{
  bool operator()( const FooPtr & a, const FooPtr & b )
    {  }
  void operator()( const FooPtr & a )
    {  }
};
*/