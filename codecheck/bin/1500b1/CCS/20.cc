//  20)	Avoid long functions. Avoid deep nesting.


#include "ccs.cch"

int deep_nest;

if ( fcn_begin ) {

    deep_nest = 0;
}

if ( op_open_brace ) {
    ++deep_nest;

    if ( deep_nest > NEST_DEPTH ) {

        CCS_C( 20, "Avoid long functions. Avoid deep nesting. [ DEEP_NEST ]" );
    }
}

if ( op_close_brace ) {
    --deep_nest;
}

if ( fcn_end ) {

    if ( fcn_total_lines > LONGFUNC ) {

        CCS_C( 20, "Avoid long functions. Avoid deep nesting. [ LONGFUNC ]" );
    }

    if ( fcn_decisions > MILLER_NUM ) {

        CCS_C( 20, "Avoid long functions. Avoid deep nesting. [ MILLER_NUM ]" );
    }

}

