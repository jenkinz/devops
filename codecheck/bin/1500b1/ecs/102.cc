/*
102.   Declare a private operator new() to prohibit dynamic allocation.
*/

#include "ecs.cch"

// not the best implementation, but this makes it clear how its done

if ( dcl_member ) {
warn(102, "DM %s %d", dcl_name(), dcl_access );

  if ( strstr( dcl_name(), "operator_new" ) ) {
  
    if ( dcl_access != PRIVATE_ACCESS ) {
      WARN( 102, "Declare a private operator new() to prohibit dynamic allocation." );
    }
  }
  
}