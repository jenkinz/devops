/*
104.   Declare single-parameter constructors as explicit to avoid unexpected type conversions.
*/


class class104 {

public:
  // good 104
  explicit class104( int n );
};

class class104_ {
  
  public:
// bad 104, no explicit    
    class104_(int n);
};
  
class class104__ {
  
  public:
 
    class104__(int n, int m);
};