/*
88.    Use inline functions instead of macros.
*/

#include "ecs.cch"

if ( op_macro_call ) {

//DUMP("OMC")
  if ( lin_within_function ) {
      WARN( 88, "Use inline functions instead of macros." );
  }
}