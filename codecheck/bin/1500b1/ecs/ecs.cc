// CodeCheck Rule-File for "The Elements of C++ Style" - ECS - Misfeldt, Bumgardner, Gray

/* 
  ECS: "Elements of C++ Style" - This rule file can be applied to C or C++

  Use of this rule-file assumes that you have bought the book! GOTO www.amazon.com & buy the book!
*/

// Abraxas Software (C) Copyright 2005 - Rule-File Code

// Examples of output messaging are CSV, XML, HTML, the default of course is ASCII to stdout.
// Edit ecs.cch if your wish to customize destination of output & format

#include "ecs.cch"    // messaging control - if you wish to modifiy the messaging - edit this file.

/*
1.     General Principles

1.       Adhere to the style of the original.
2.       Adhere to the Principle of Least Astonishment.
3.       Do it right the first time.
4.       Document any deviations.

2.         Formatting Conventions

5.      Use indented block statements.
6.      Indent statements after a label.
7.       Choose one style for brace placement.
8.      Break long statements into multiple lines.
*/

if ( lin_end ) {
	if ( lin_length > 92 ) WARN( 8, "Break long statements into multiple lines." );
}

/*
9.       Include white space.
*/

if ( lex_key_no_space ) {

  // space after return is not required, there could be others.
  	if ( !( CMPTOK("return") ) ) {
          WARN( 9, "Include white space." );
        }
}

/*
10.    Do not use "hard" tabs.
*/

if ( lin_indent_tab ) {
	
	 WARN( 10, "Do not use \"hard\" tabs." );
}

/*

//[   3.         Naming Conventions         ]

11.    Use UPPERCASE and underscores for preprocessor macro names.
*/

if ( pp_lowercase ) {

	WARN( 11, "Use UPPERCASE and underscores for preprocessor macro names." );
}

/*
12.    Add a unique prefix to macro names.
*/
if ( pp_macro ) {
//warn(0,"PM %s", pp_name() );	
	if ( islower(pp_name()[0]) || strchr(pp_name(),'_')==0 ) {
		WARN( 12, "Add a unique prefix to macro names." );
	}
}
/*
13.    Use UpperCamelCase for classes, constants, structures, enumerations, and typedefs.
14.    Use nouns to name compound types.
15.    Pluralize the names of collections.
*/

if ( dcl_variable ) {

	if ( dcl_level(0) == ARRAY ) {
//warn(0, "array %s %d [%c]", dcl_name(), strlen(dcl_name()),dcl_name()[strlen(dcl_name())-1] );	

		if ( dcl_name()[strlen(dcl_name())-1] != 's' ) {
		
			WARN( 15, "Pluralize the names of collections." );
		}
	}
}

/*
16.    Use lowerCamelCase for function names.
*/

if ( dcl_function ) {
	if ( isupper(dcl_name()[0]) ) {
		WARN(16, "Use \"lowerCamelCase\" for function names." );
	}
}

/*
17.    Use verbs to name functions.
18.    Use is, set, and get to name accessor and mutator functions.
19.    Use lowerCamelCase for variable and function parameter names.
*/

if ( dcl_variable || dcl_parameter ) {
	if ( isupper(dcl_name()[0]) ) {
		WARN(19, "Use \"lowerCamelCase\" for variable and function parameter names." );
	}
}

/*
20.    Use nouns to name variables.
21.    Add a prefix or suffix to member variable names to distinguish them from other variables.
*/

if ( dcl_member ) {
//warn(0,"PM %s", pp_name() );	
	if ( islower(dcl_name()[0]) || strchr(dcl_name(),'_')==0 ) {
		WARN( 21, "Add a prefix or suffix to member variable names to distinguish them." );
	}
}
/*
22.    Name all function parameters.
*/

if ( dcl_parameter ) {

    if ( strlen(dcl_name())==0 ) {

        WARN( 22, "Name all function parameters.");
    }
}
/*
23.    Use "other" for parameter names in copy constructors and assignment operators.
*/

char m_name[MAXIDLEN];  // save method name

if ( dcl_parameter ) {
//DEBD("DP")
//warn(1, "m-name = %s", m_name );
    if ( dcl_base == CLASS_TYPE ) {
        if ( strcmp(m_name,"operator=")==0 || strcmp(m_name,"constructor")==0 ) {

            if ( strcmp(dcl_name(),"other") ) 
                 WARN( 23, "Use \"other\" for parameter names in copy constructors and assignment operators." );
        }
    }   
}

if ( op_open_funargs && lin_within_class ) {
//DUMP("OOF")
//warn(0,"OOF %s() %s", dcl_name(), dcl_base_name() );
    strcpy( m_name, dcl_name() );

    if ( strchr( dcl_name(), '=' ) ) {  // overload assignment 
        strcpy( m_name, dcl_name() );
    }
    else {
        strcpy( m_name, dcl_base_name() );
    }

}
/*
24.    Give function parameters the same name as the member variables you assigned them to.
*/


/*
25.    Use meaningful names.
26.    Use familiar names.
27.    Avoid the use of digits within names.
*/

if ( dcl_variable || dcl_member || dcl_function ) {

	if ( strpbrk(dcl_name(),"0123456789") ) {
		WARN( 27, "Avoid the use of digits within names.");
	}
}

/*
28.    Avoid excessively long names.
*/
if ( dcl_variable || dcl_member ) {
	if ( dcl_ident_length > 32 ) WARN( 28, "Avoid excessively long names." );
}

/*
29.    Join the vowel generationuse complete words.
30.    Use lowerCamelCase for abbreviations.
*/
if ( dcl_variable ) {
//DEBD("DT")
	if ( dcl_base == DEFINED_TYPE ) {	// typedef
		if ( strstr(dcl_name(), dcl_base_name()) ) {
			WARN( 30, "Use \"lowerCamelCase\" for abbreviations." );
		}
	}
}
/*
31.    Do not use case to differentiate names.
*/

if ( dcl_variable ) {
//DEBD("DT")
	if ( dcl_base == DEFINED_TYPE ) {	// typedef
		if ( strstr(dcl_name(), dcl_base_name()) ) {
			WARN( 31, "Do not use case to differentiate names." );
		}
	}
}

/*
4.         Documentation Conventions

32.   Document your software interface for those who must use it.
33.   Document your implementation for those who must maintain it.
34.    Keep your comments and code synchronized.
35.   Embed API reference documentation in your source code.
36.   Generate API reference documentation directly from the source code.
37.   Document all significant software elements.
38.   Document software elements as early as possible.
39.    Use block comments to describe the programming interface.
*/

/*
40.    Use one-line comments to explain implementation details.
*/

#define COMMENT_THRESHOLD	2

int comment_seen ;		// last comment seen

if ( lin_is_comment )  {
	comment_seen = lin_number;
}

if ( stm_cp_begin ) {

//warn(40, "SCB cs=%d sk=%d sd=%d",  comment_seen, stm_kind,stm_depth );
	if ( stm_is_low && stm_kind == COMPOUND && (lin_number-comment_seen) > COMMENT_THRESHOLD ) {
	
		WARN( 40,"Use one-line comments to explain implementation details." );
	}

}

/*
41.    Use a single consistent format and organization for all documentation comments.
*/


/*
42.   Provide a summary description of every declared element.
*/

// ecs 42 uses control data 'comment_seen' from rule 40

if ( dcl_variable ) {
//warn(42, "DV cs=%d", comment_seen );
	if ( (lin_number-comment_seen) > COMMENT_THRESHOLD ) {
	
		WARN( 42,"Provide a summary description of every declared element." );
	}

}

/*
43.   Document the interface exposed by every function.
*/

if ( fcn_begin ) {
//warn(43, "DV cs=%d sk=%d sd=%d", comment_seen, stm_kind, stm_depth );
	if ( (lin_number-comment_seen) > COMMENT_THRESHOLD ) {
	
		WARN( 43,"Document the interface exposed by every function." );
	}

}

/*
44.   Document thread synchronization requirements.
45.   Provide examples to illustrate common and proper usage.
46.   Document important preconditions, postconditions, and invariant conditions.
47.   Document known defects and deficiencies.
48.    Use the active voice to describe actors and passive voice to describe actions.
49.    Use this rather than the when referring to instances of the current class.
50.   Explain why the code does what it does.
51.    Avoid the use of end-line comments.
*/

if ( lin_has_comment ) {
	
	if ( lin_has_code ) WARN( 51, " Avoid the use of end-line comments." );
}

/*
52.    Label closing braces in highly nested control structure.
*/

int brace_depth;

if ( mod_begin ) brace_depth = 0;

if ( op_open_brace ) {
	brace_depth++;
}

if ( op_close_brace ) {

//warn( 52, "OCB bd=%d", brace_depth );

	if ( brace_depth > 2 && strstr(line(),"//") == 0 ) {
	
		WARN( 52, "Label closing braces in highly nested control structure." );
	}
	
	brace_depth--;
}

/*
53.    Add a fall-through comment between two case labels if no break statement separates those labels.
54.    Use keywords to mark pending work, unresolved issues, defects, and bug fixes.

*/

// 5.         Programming Principles

/*
55.    Do not be afraid to do engineering.
56.    Choose simplicity over elegance.
57.    Do not use a feature of C++ just because it is there.
58.    Recognize the cost of reuse.
59.    Program by contract.
*/

/*
60.    Keep classes simple.
*/

if ( tag_end ) {

	if ( tag_lines > 50 ) WARN( 60, "Keep classes simple." );
}

/*
61.    Define subclasses so they may be used anywhere their superclasses may be used.
62.    Use inheritance for is a relationships and containment for has a relationships.
63.    Avoid multiple inheritance.
64.    Design for reentrancy.
*/

if ( idn_global ) {
	
	if ( lin_within_function ) {
		WARN( 64,  "Design for reentrancy." );
	}
}

/*
65.    Use threads only where appropriate.
66.    Avoid unnecessary synchronization.
67.    Do not synchronize access to code that does not change shared state.
*/


// 6.         Programming Conventions


/*
68.    Use #include  for collocated header files and #include <> for external header files.
*/

if ( header_name() ) {
//warn(0, "LH  %d %s %s", pp_include, header_path(), header_name() );
	if ( pp_include > 2 ) {		// #include <...> type
		if ( strlen(header_path()) == 0 )
			WARN( 68, "Use #include <...> for external header files." );
	}
	else {							// #include "..." type
		if ( strlen(header_path()) )
			WARN( 68, "Use #include \"...\" for collocated header files." );
	}
}

/*
69.    Place preprocessor include guards in header files.
*/

//ALERT  simple approach below for professional-approach use wrapper.cc from codecheck rule-file suite

int wrapchk;

if ( lin_preprocessor  ) {
  
//warn(69, "LP %d %s w%d", lin_preprocessor, pp_name(), wrapchk );

  if ( lin_preprocessor == INCLUDE_PP_LIN ) {
    wrapchk = 1;
  }
  else {
    
    if ( wrapchk && lin_header ) {
      
      if ( lin_preprocessor != IFDEF_PP_LIN ) {
        WARN( 69, "Place preprocessor include guards in header files." );
      }
    }
    wrapchk = 0;
  }
}

/*
70.    Use #if..#endif  and #ifdef..#endif instead of comments to hide blocks of code.
71.    Use macros sparingly.
*/
int num_macro;

if ( mod_end )  {

  if ( mod_macros > 6 ) {
			
      WARN( 71, "Use macros sparingly." );
  }
}

/*
72.    Add a semicolon after every statement expression macro.
*/

if ( lex_macro ) {
//DUMP("LM")
	if ( next_char() != ';' ) 
		WARN( 72, "Add a semicolon after every statement expression macro." );
}

/*
73.    Use macros to capture the current file name and line number.
74.    Do not use "#define" to define constantsdeclare static const variables instead.
*/
if ( lex_macro_token ) {
	if  ( isdigit(token()[0]) && strlen(token())>1 ) {
//DUMP("LMT")
		WARN( 74, "Do not use \"#define\" to define constants." );
	}
}

/*
75.    Use portable types for portable code.
*/

if ( dcl_typedef && dcl_name()!=0 ) {

//DEBD("DT")

  if ( strcmp(dcl_name(),"int16")==0 || strcmp(dcl_name(),"int32")==0 ) {
    
    WARN( 75, "Use portable types for portable code." );
  }

}

/*
76.    Use typedefs to simplify complicated type expressions.
*/

if ( dcl_variable && lin_within_class ) {
  
 //DEBD("DL CL") 
 //DEBDL("DL CL LEV")
     
     if ( dcl_levels > 0 &&  dcl_level_flags(dcl_levels) == CONST_FLAG ) {
   
       WARN( 76, "Use typedefs to simplify complicated type expressions." );
     }
}

/*
77.   Create a zero-valued enumerator to indicate an uninitialized, invalid, unspecified, or default state.
*/

int default_enum_seen;

if ( tag_begin && tag_kind == ENUM_TAG ) {
//warn(77, "TB %s", tag_name() );  
  default_enum_seen = 1;  
  
}

if ( tag_end ) default_enum_seen = 0;

if ( dcl_enum ) {

//DEBD("DE")
//DUMP("DE")
  if ( default_enum_seen ) {
  
    if ( default_enum_seen == 1 && !(CMPTOK("Default") || CMPTOK("None")) ) {
      
      WARN( 77, "Create a zero-valued enumerator to indicate an uninitialized." );
    }
    
    default_enum_seen++;
  }

}

/*
78.    Do not define enumerations using macros or integer constants.
*/

if ( lex_constant ) {

//warn( 78, "LC %d lmt=%d", lex_constant, lex_macro_token ) ;

  if ( strlen(token())>0 && isdigit(token()[0]) ) {
    
      WARN( 78, "Do not define enumerations using macros or integer constants." );
  }
}

/*
79.   Declare enumerations within a namespace or class.
*/

if ( dcl_enum ) {
  
//warn( 79, "DE %s lwc=%d lwn=%d", dcl_name(), lin_within_class, lin_within_namespace );  

  if ( ! ( lin_within_class || lin_within_namespace ) ) {
   
    WARN( 79, "Declare enumerations within a namespace or class." );
  }
}


/*
80.   Declare global functions, variables, or constants as static members of a class.
*/

if ( dcl_global ) {

//DEBD("DG")

  WARN( 80, "Declare global functions, variables, or constants as static members of a class." );

}


/*
81.   Declare for-loop iteration variables inside of for statements.
*/


int dcl_infor;
int infor;

 if ( op_for ) {
    dcl_infor = 0;    // reset
    infor = 1;       // signal in for
 }
 
 if ( stm_is_iter  ) {
   
   infor = 0;      // reset
 //  warn(0, "stm=%d", stm_kind );   
    if ( stm_kind == FOR ) {
   
      if ( dcl_infor == 0 ) WARN( 81, "Declare for-loop iteration variables inside of for statements" );
  }
   
 }
 
 if ( dcl_local ) {
   if ( infor ) dcl_infor++;
   
 }


/*
82.    Use an enumeration instead of a Boolean to improve readability.
*/

int incall;

if ( op_open_funargs ) {
//warn( 82, "OOF" );  
  incall++;
}

if ( op_close_funargs ) {
  incall--;
}

if ( keyword("true") || keyword("false" ) ) {
//warn( 82, "bool" );  
  if ( incall ) {
    WARN( 82, "Use an enumeration instead of a Boolean to improve readability." );
  }
}

/*
83.    Use an object pointer instead of a reference if a function stores a reference or pointer to the object.
*/

/*
84.   Accept objects by reference and primitive or pointer types by value.
*/

if ( dcl_parameter ) {
//warn( 84, "DM %s %d %d", dcl_name(), dcl_base, dcl_level(0) );

  if ( dcl_level(0)==REFERENCE && dcl_base<=ENUM_TYPE ) {
  
    WARN( 84, " Accept objects by reference and primitive or pointer types by value." );
  }
}

// 85.    Use a const char* for narrow character string parameters.

// 86.    Pass enumerator values, not integer constants.


// 87.    Do not use void* in a public interface.

if ( dcl_member ) {

//warn( 87, "DM %s base=%d dl0=%d dl1=%d acc=%d", dcl_name(), dcl_base, dcl_level(0), dcl_level(1), dcl_access );

  if ( dcl_level(1) == POINTER && dcl_access == PUBLIC_ACCESS ) {
    
      WARN( 87, "Do not use void* in a public interface." );
  }
}

/*
88.    Use inline functions instead of macros.
*/

if ( op_macro_call ) {

//DUMP("OMC")
  if ( lin_within_function ) {
      WARN( 88, "Use inline functions instead of macros." );
  }
}

/*
89.    Inline only the simplest of functions.
*/


/*
90.   Factor functions to allow inlining of trivial cases.
*/


// 91.    Define small classes and small methods.

/*

92.    Build fundamental classes from standard types.
93.    Avoid the use of virtual base classes in user-extensible class hierarchies.
94.   Declare the access level of all members.
95.   Declare all member variables private.
*/

// 96.    Avoid the use of friend declarations.

if ( tag_end ) {
  if ( tag_friends ) {
   
      WARN( 96, "Avoid the use of friend declarations." ); 
  }  
}

/*
97.   Declare an explicit default constructor for added clarity.
*/

if ( tag_end ) {
  if ( tag_has_default == 0 ) {
  
      WARN(97, "Declare an explicit default constructor for added clarity." );
  }
}

/*
98.   Always declare a copy constructor, assignment operator, and destructor if the class can be instantiated.
99.   Always implement a virtual destructor if your class may be subclassed.
100.  Make constructors protected to prohibit direct instantiation.
101.  Make constructors private to prohibit derivation.
*/

/*
102.   Declare a private operator new() to prohibit dynamic allocation.
*/

if ( dcl_member ) {
//warn(102, "DM %s %d", dcl_name(), dcl_access );

  if ( strstr( dcl_name(), "operator_new" ) ) {
  
    if ( dcl_access != PRIVATE_ACCESS ) {
      WARN( 102, "Declare a private operator new() to prohibit dynamic allocation." );
    }
  }
  
}

/*
103.   Declare a protected or private destructor to prohibit static or automatic allocation.
*/

/*
104.   Declare single-parameter constructors as explicit to avoid unexpected type conversions.
*/

int parmseen;

if ( dcl_parameter ) {
 
//warn( 104, "DP %d %d", dcl_parameter, dcl_parm_count );  
  parmseen = dcl_parameter;
}


if ( dcl_function ) {
//warn( 104, "DF dpc%d f%d p%d", dcl_parm_count, dcl_function_flags, dcl_parameter ); 

  if ( (dcl_function_flags & EXPLICIT_FCN)==0 && parmseen == 1 ) {
    
    WARN( 104, "Declare single-parameter constructors as explicit to avoid unexpected type conversions." );
    
  }
}

/*
105.  Use default arguments to reduce the number of constructors.
106.  Do not overload non-virtual methods in subclasses.
107.   Declare virtual methods protected and call them from public non-virtual methods.
108.  Keep your functions "const-correct."
109.  Use object pointers and references in class declarations.
110.   Adhere to the natural semantics of operators.
111.  Do not overload operator&&() or operator||().
112.   Invoke the superclass assignment operator(s) in the assignment operator of a subclass.
113.   Implement copy-safe and exception-safe assignment operators.
114.  Define binary operators outside of a class.
115.   Implement a Boolean operator in terms of its opposite.
116.  Use templates instead of macros to create parameterized code.
117.  Do not use CV-qualified types as template parameters.
*/


// 118.  Use C++ casting operators instead of C-style casts.

if ( op_cast ) {

//DUMP( "OC" )

  WARN( 118, "Use C++ casting operators instead of C-style casts." );
}

/*
119.  Avoid type casting and do not force others to use it.
120.  Use static_cast<> to expose non-intuitive implicit conversions.
121.  Do not use reinterpret_cast<> in portable code.
122.  Only use const_cast<> on "this" or when dealing with non-const-correct code.
123.  Never use dynamic_cast<> as a substitute for polymorphism.
124.  Use dynamic_cast<> to restore lost type information.
125.   Always treat string literals as const char*.
*/

/*
126.  Use C++ streams instead of stdio functions for type safety.
*/

if ( idn_function ) {
      
   if ( strlen(idn_name())>0 && strcmp(idn_name(),"printf")==0 ) {
   
    WARN( 126, " Use C++ streams instead of stdio functions for type safety." );
  }
}


/*
127.  Test all type conversions.
128.   Initialize all variables.
129.  Do not rely on the order of initialization of global objects.
130.   Always construct objects in a valid state.
131.   Initialize member variables in the initializer list.
132.   Initialize member variables in the order they are declared.
133.   Indicate when the declaration order of data members is significant.
134.   Always list any superclass constructors in the initializer list of a subclass constructor.
135.  Do not call virtual functions in constructors and destructors.
136.   Declare and initialize static variables within functions.
137.  Zero pointers after deletion.
138.  Use the new and delete operators instead of malloc() and free().
139.  Do not rely on operator precedence in complex expressions.
140.  Use block statements in control flow constructs.
141.  Do not test for equality with true.
142.   Replace repeated, non-trivial expressions with equivalent methods.
143.  Use size_t variables for simple loop iteration and array subscripts.
144.  Use a dummy template function to eliminate warnings for unused variables.
*/

/*
145.  Avoid break and continue in iteration statements.
*/


int iter_seen ;    // stack depth of if, zero when out of 'IF...FI'

if ( op_do || op_while_1 || op_for ) {
//warn( 145, "ITER" );

  iter_seen++;    // push IF stack
  
}

if ( stm_end ) {
//warn(135, "SE kind=%d", stm_kind );

  if ( stm_kind >= WHILE && stm_kind <= FOR ) {
   
     iter_seen--;    // pop if stack
  }
}

if ( op_continue || op_break ) {
  
  if ( iter_seen ) {
  
     WARN( 145, "Avoid break and continue in iteration statements." );   
  }
}

/*
146.  Avoid multiple return statements in functions.
*/

int return_count;

if ( fcn_begin ) return_count = 0;

if ( op_return ) return_count++;

if ( fcn_end ) {
  if ( return_count > 1 ) {
     WARN( 146, "Avoid multiple return statements in functions." );
  } 
}

/*
147.  Do not use goto.
*/
if ( op_goto ) {
  
  WARN( 147, "  Do not use goto." );
}

/*
148.  Do not use try..throw..catch to manage control flow.
149.  Never use setjmp() or longjmp() in a C++ program.
150.   Always code a break statement in the last case of a switch statement.
151.  Use return codes to report expected state changes.
152.  Use assertions to enforce a programming contract.
153.  Do not silently absorb or ignore unexpected runtime errors.
154.  Use assertions to report unexpected or unhandled runtime errors.
155.  Use exceptions to report errors that may occur under normal program execution.
156.   Manage resources with RAII for exception safety.
157.  Catch exceptions by reference, not by value.
158.  Do not discard exception information if you throw a new exception within a catch block.
159.  Avoid throwing exceptions in destructors.
160.  Use lazy evaluation and initialization.
161.  Reuse objects to avoid reallocation.
162.  Leave optimization for last.

*/


//7.         Packaging Conventions

/*
163.  Use unnamed namespaces instead of static to hide local functions and variables.
*/


/*
164.  Tread lightly on the global namespace.
165.  Place types that are commonly used, changed, and released together, or mutually dependent on each other, into the same package.
166.  Isolate unstable classes in separate packages.
167.  Avoid making difficult-to-change packages dependent on packages that are easy to change.
168.   Maximize abstraction to maximize stability.
169.   Capture high-level design and architecture as stable abstractions organized into stable packages.
*/

/*
170.  Use the class name as the filename.
*/

if ( tag_begin ) {
//warn( 170, "TB %s %s", tag_name(), class_name() );
  if ( tag_kind == CLASS_TAG ) {
    
    if ( strstr( mod_name(), class_name() ) == 0 ) {
      WARN( 170, "Use the class name as the filename." );
    }
  }
}

/*
171.  Use separate files for each namespace-scope class, struct, union, enumeration, function, or overloaded function group.
*/

if ( mod_end ) {
//warn(171,"ME c%d m%d f%d", mod_classes, mod_members, mod_functions );   
  if ( mod_classes > COMPLEX_MOD || mod_functions > COMPLEX_MOD ) {
   
    WARN( 171, "Use separate files for each class, struct, union, enumeration, function, or overload" );
  }
}

/*
172.  Use #include sparingly in header files.
*/

if ( pp_include ) {
//warn( 172, "PI %d %d", pp_include, lin_header );
  
  if ( lin_header && pp_include <= 2 ) {
    
    WARN( 172, "Use #include sparingly in header files" );
  }

}

/*
173.   Implement class methods outside of their class declaration block.
*/

if ( dcl_definition ) {

  if ( dcl_function && lin_within_class ) {
    
   WARN( 173, " Implement class methods outside of their class declaration block." );
  }

}

// 174.  Do not name files containing tests or examples the same as template header file names.

if ( idn_local ) {

//warn( 174, "ID %s %s %d k%d", idn_name(), idn_base_name(), dcl_base, stm_kind );

  if ( idn_base_name() && strstr(mod_name(),idn_base_name() ) ) {
    
      if ( stm_kind <= SWITCH )
        WARN( 174, "Do not name files containing tests or examples the same as template header file names." );
  }
}

// 175.  Do not put global-scope using or using namespace declarations in a header file.

if ( keyword("using") ) {

  if ( lin_header ) {
    
    WARN( 175, "Do not put global-scope using or using namespace declarations in a header file." );
  }
}