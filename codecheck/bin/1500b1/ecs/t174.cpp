// 174.  Do not name files containing tests or examples the same as template header file names.

template <class t> class t174;

main() {
 t174<int> t;
 
 if ( t ) {}
 
 while (t ) {}
 
 
}