/*
52.    Label closing braces in highly nested control structure.
*/

#include "ecs.cch"

int brace_depth;

if ( mod_begin ) brace_depth = 0;

if ( op_open_brace ) {
	brace_depth++;
}

if ( op_close_brace ) {

//warn( 52, "OCB bd=%d", brace_depth );

	if ( brace_depth > 2 && strstr(line(),"//") == 0 ) {
	
		WARN( 52, "Label closing braces in highly nested control structure." );
	}
	
	brace_depth--;
}

