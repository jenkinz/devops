/*
23.    Use "other" for parameter names in copy constructors and assignment operators.
*/


class A {
    
    A( const A& other );    // ok
    A& operator= (const A& other ); // ok

    A( const A& foo );    // bad
    A& operator= (const A& fum ); // bad

};
