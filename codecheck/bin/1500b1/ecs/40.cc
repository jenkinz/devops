/*
40.    Use one-line comments to explain implementation details.
*/

#include "ecs.cch"

#define COMMENT_THRESHOLD	2

int comment_seen ;		// last comment seen

if ( lin_is_comment )  {
	comment_seen = lin_number;
}

if ( stm_cp_begin ) {
//warn(0, "ln=%d SCB cs=%d", lin_number, comment_seen );
	if ( (lin_number-comment_seen) > COMMENT_THRESHOLD ) {
	
		WARN( 40,"Use one-line comments to explain implementation details." );
	}

}