/*
***************************************************************************
*                                                                         *
* FILE NAME:	pp.cc                                                     *
* COPYRIGHT (C) 1994 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.            *
* PURPOSE:	This a CodeCheck rule file used to test the predefined    *
*               variables in pp_ group.                                   *
*                                                                         *
***************************************************************************
*/
  
#include <check.cch>
                                       
if ( pp_ansi )
  warn (1001, "pp_ansi");

if ( pp_arg_count )
  warn (1002, "pp_arg_count: %d", pp_arg_count);

if ( pp_arg_multiple )
  warn (1003, "pp_arg_multiple");

if ( pp_arg_paren )
  warn (1004, "pp_arg_paren");

if ( pp_arg_string )
  warn (1005, "pp_arg_string");

if ( pp_arith )
  warn (1006, "pp_arith");

if ( pp_assign )
  warn (1007, "pp_assign");

if ( pp_bad_white )
  warn (1008, "pp_bad_white");

if ( pp_benign )
  warn (1009, "pp_benign");

if ( pp_comment )
  warn (1010, "pp_comment");

if ( pp_const )
  warn (1011, "pp_const");

if ( pp_defined )
  warn (1012, "pp_defined");

if ( pp_depend )
  warn (1013, "pp_depend");

if ( pp_elif )
  warn (1014, "pp_elif");

if ( pp_endif )
  warn (1015, "pp_endif");

if ( pp_empty_arglist )
  warn (1016, "pp_empty_arglist");

if ( pp_empty_body )
  warn (1017, "pp_empty_body");

if ( pp_error )
  warn (1018, "pp_error");

if ( pp_if_depth )
  warn (1019, "pp_if_depth: %d", pp_if_depth);

if ( pp_include )
		{
    switch (pp_include)
     {
       case 1:
          warn (1020, "pp_include - in quotes, from macro");
          break;

       case 2:
          warn (1020, "pp_include - in quotes, not from macro");
          break;

       case 3:
          warn (1020, "pp_include - in angles, from macro");
          break;

       case 4:
          warn (1020, "pp_include - in angles, not from macro");
          break;

       case 5:
          warn (1020, "pp_include - not enclosed - metaware");
          break;

       case 6:
          warn (1020, "pp_include - not enclosed - vax");
          break;

       default:
          warn (1020, "pp_include - unknown value: %d", pp_include);
          break;

    }
 } 

if ( pp_include_depth )
  warn (1021, "pp_include_depth: %d", pp_include_depth);

if ( pp_include_white )
  warn (1022, "pp_include_white");

if ( pp_keyword )
  warn (1023, "pp_keyword");

if ( pp_length )
  warn (1024, "pp_length: %d", pp_length);

if ( pp_lowercase )
  warn (1025, "pp_lowercase");

if ( pp_macro )
  warn (1026, "pp_macro - length %d, name <%s>", pp_macro, pp_name());

if ( pp_macro_conflict )
  warn (1027, "pp_macro_conflict");

if ( pp_macro_dup )
  warn (1028, "pp_macro_dup");

if ( pp_not_ansi )
  warn (1029, "pp_not_ansi");

if ( pp_not_defined )
  warn (1030, "pp_not_defined");

if ( pp_overload )
  warn (1031, "pp_overload");

if ( pp_paste )
  warn (1032, "pp_paste");

if ( pp_pragma )
  warn (1033, "pp_pragma");

if ( pp_recursive )
  warn (1034, "pp_recursive");

if ( pp_semicolon )
  warn (1035, "pp_semicolon");

if ( pp_sizeof )
  warn (1036, "pp_sizeof");

if ( pp_stack )
  warn (1037, "pp_stack");

if ( pp_stringize )
  warn (1038, "pp_stringize");

if ( pp_sub_keyword )
  warn (1039, "pp_sub_keyword");

if ( pp_trailer )
  warn (1040, "pp_trailer");

if ( pp_undef )
  warn (1041, "pp_undef");

if ( pp_unknown )
  warn (1042, "pp_unknown");

if ( pp_unstack )
  warn (1043, "pp_unstack");

if ( pp_white_before )
  warn (1044, "pp_white_before: %d", pp_white_before);

if ( pp_white_after )
  warn (1045, "pp_white_after: %d", pp_white_after);

if ( macro ("RUNTIME") )
  warn (1046, "macro() - 'RUNTIME'");

if ( op_macro_call )
		warn (1047, "op_macro_call - name <%s>", op_macro());

if (prj_begin)
 {
   define ("RUNTIME", "1");
   warn (1050, "Runtime defined");
   undefine ("RUNTIME");
	}

if ( pragma("MY_PRAGMA") )
  warn (1049, "pragma() 'MY_PRAGMA' found");

if (pp_relative)
  warn(1051,"pp_relative");

if (pp_not_found)
  warn(1052,"pp_not_found");


