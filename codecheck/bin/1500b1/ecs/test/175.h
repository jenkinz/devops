// 175.  Do not put global-scope using or using namespace declarations in a header file.

// bad 175, using should not be used in a header file
using stdfoo::noop;