/*
79.   Declare enumerations within a namespace or class.
*/

// bad, global space
enum { foo1 };

// good nmsp

namespace n79 {
  enum { foo2 };
}

// good class space

class c79{
  
  enum { foo3 };
};