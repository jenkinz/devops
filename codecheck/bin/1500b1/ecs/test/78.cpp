/*
78.    Do not define enumerations using macros or integer constants.
*/

#define NUM  1

// BAD don't use macro constants, use enum

int inum = NUM;

static int sinum = NUM;

// BAD also ..

int num = 1;

