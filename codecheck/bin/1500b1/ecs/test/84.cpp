/*
84.   Accept objects by reference and primitive or pointer types by value.
*/


class BIG;

//GOOD 84
void foo ( const BIG & parm );

//BAD 84, primitive types must be passed by value
void fum ( const char & cparm );