/*
82.    Use an enumeration instead of a Boolean to improve readability.
*/

class c82 {
  enum alarm { silent, aloud };
  
  void noop(bool);  //BAD 82 DECL
  void noop(alarm); 
};



void fooop( ) {
  
 typedef class c82 foo; 
 
 //BAD 82 Usage
 foo.noop(true);    // this has questionable meaning
 
 //GOOD 82
 foo.noop(foo::aloud); // this is clear?
 
}