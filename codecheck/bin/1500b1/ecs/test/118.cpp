// 118.  Use C++ casting operators instead of C-style casts.

int i = 1;

float j = 1.0;

// BAD 118, explicit old-style cast

i = (int) j;

// good 118
i = static_cast<int> (j);

