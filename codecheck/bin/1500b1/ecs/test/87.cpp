// 87.    Do not use void* in a public interface.

class foocl {

public:
  void * foo();  // BAD 87
  
  int foint();

private:
  void * foam();  // GOOD 87
};