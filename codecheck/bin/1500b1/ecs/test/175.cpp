// 175.  Do not put global-scope using or using namespace declarations in a header file.

namespace stdfoo {
 int noop;
}

// GOOD 175, using ok in c++ file
using stdfoo::noop;

#include "175.h"
