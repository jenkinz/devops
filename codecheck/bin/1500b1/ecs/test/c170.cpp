/*
170.  Use the class name as the filename.
*/

// GOOD 170 class name like file name
class c170 {

};

// BAD 170 file name not like class name
class c170_unique {
  
  
};