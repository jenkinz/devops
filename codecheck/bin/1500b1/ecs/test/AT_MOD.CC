/*
***************************************************************************
*                                                                         *
* FILE NAME:	mod.cc                                                    *
* COPYRIGHT (C) 1994 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.            *
* AUTHOR:	Shuming Tan                     July 22, 1994             *
* PURPOSE:	This a CodeCheck rule file used to test the predefined    *
*               variables in mod_ group.                                  *
*                                                                         *
***************************************************************************
*/
int i;

if (mod_begin)
{
  printf("MODULE::%s\n",mod_name());
  printf("--------------------------------------------------------------\n");
}

if (mod_end)
{
  printf("number of tokens                         : %d\n",mod_tokens);
  printf("number of simple variables               : %d\n",mod_simple);
  printf("number of agggregat variables            : %d\n",mod_aggr);
  printf("number of global variables               : %d\n",mod_globals);
  printf("number of global static variables        : %d\n",mod_static);
  printf("number of external global variables      : %d\n",mod_extern);
  printf("number of global array elements          : %d\n",mod_array);
  printf("number of local tag members              : %d\n",mod_members);
  printf("number of unused global static variables : %d\n",mod_unused);
  printf("number of classes defined                : %d\n",mod_classes);
  printf("number of macros                         : %d\n",mod_macros);
  printf("number of functions                      : %d\n",mod_functions);
  printf("number of Halstead operators             : %d\n",mod_H_operators);
  printf("number of Halstead operands              : %d\n",mod_operands);
  printf("number of unique Halstead operators      : %d\n",mod_uH_operators);
  printf("number of unique Halstead operands       : %d\n",mod_uH_operands);
  printf("number of standard operators             : %d\n",mod_operators);
  printf("number of standard operands              : %d\n",mod_operands);
  printf("number of unique operators               : %d\n",mod_u_operators);
  printf("number of unique operands                : %d\n",mod_u_operands);
  printf("number of binary decision points         : %d\n",mod_decisions);
  printf("number of non executable statements      : %d\n",mod_nonexec);
  printf("number of low level statements           : %d\n",mod_low);
  printf("number of high level statements          : %d\n",mod_high);
  printf("number of all lines                      : %d\n",mod_total_lines);
  printf("number of blank lines                    : %d\n",mod_white_lines);
  printf("number of pure comment lines             : %d\n",mod_com_lines);
  printf("number of executable lines               : %d\n",mod_exec_lines);
  printf("number of warning messages               : %d\n",mod_warnings);
  i=0;
  while (i<mod_classes)
  {
    printf("class %d:%s has %d lines and %d tokens\n",i+1,mod_class_name(i),mod_class_lines(i),mod_class_tokens(i));
    i++;
  }
}
