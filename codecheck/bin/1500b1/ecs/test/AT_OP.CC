/*
***************************************************************************
*                                                                         *
* FILE NAME:	op.cc                                                    *
* COPYRIGHT (C) 1994 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.            *
* AUTHOR:	Shuming Tan                     July 27, 1994             *
* PURPOSE:	This a CodeCheck rule file used to test the predefined    *
*               variables in op_ group.                                  *
*                                                                         *
***************************************************************************
*/
#include <check.cch>

#define DETAILED 1

int binary,unary,trinary;
int loop,i,j;

#if DETAILED
/* about assignment operator */
if (op_assign) 
{
  warn(7000,"An assignment operator");
  binary=1;
}

/* about unary arithmetic operators */
if (op_negate||op_plus)
{
  warn(7001,"A unary arithmetic operator",token());
  unary=1;
}

/* about arithmetic operators */
if (op_add||op_subt||op_mul||op_div||op_rem)
{
  warn(7002,"A binary arithmetic operator",token());
  binary=1;
}

/* about compound arithmetic assign operators */
if (op_add_assign||op_sub_assign||op_mul_assign||op_div_assign||op_rem_assign)
{
  warn(7003,"A compound arithmetic operator");
  binary=1;
}

/* about binary comparison operators */
if (op_equal||op_not_eq||op_more||
    op_more_eq||op_less||op_less_eq)
{
  warn(7004, "A binary comparison operator");
  binary=1;
}

/* about unary bitwise operator */
if (op_bit_not)
  warn(7005,"A unary bitwise operator");

/* about binary bitwise operators */
if (op_bit_and||op_bit_or||op_bit_xor)
{
  warn(7006,"A binary bitwise operator");
  binary=1;
}

/* about compound bitwise assign operators */
if (op_and_assign||op_or_assign||op_xor_assign)
{
  warn(7007,"A compound bitwise assign operator");
  binary=1;
}

/* about unary logical operator */
if (op_log_not)
{
  warn(7008,"A unary logical operator");
  unary=1;
}

/* about binary logical operators */
if (op_log_and||op_log_or)
{
  warn(7009,"A binary logical operator");
  binary=1;
}

/* about shift operators */
if (op_left_shift||op_right_shift)
{
  warn(7010,"A shift operator");
  binary=1;
}

/* about compound shift assign operators */
if (op_left_assign||op_right_assign)
{
  warn(7011,"A compound shift assign operator");
  binary=1;
}

/* about prefix arithmetic operators */
if (op_pre_incr||op_pre_decr)
{
  warn(7012,"A prefix arithmetic operator");
  unary=1;
}

/* about postfix arithmetic operators */
if (op_post_decr||op_post_incr)
{
  warn(7013,"A postfix arithmetic operator");
  unary=1;
}
#endif

/* about cast operators */
if (op_cast||op_cast_to_ptr)
{
  warn(7014,"A cast operator");
  unary=1;
}

/* about sizeof operator */
if (op_sizeof)
{
  warn(7015,"A sizeof operator");
  unary=1;
}

if (op_open_paren||op_close_paren)
  warn(7016,"A parenthesis");

if (op_open_brace||op_close_brace)
  warn(7017,"A brace operator");

if (op_open_funargs||op_close_funargs)
  warn(7018,"A function argument list parenthesis");

if (op_open_bracket||op_close_bracket)
  warn(7019,"A bracket operator");

if (op_open_angle||op_close_angle)
  warn(7020,"A template delimit operator");

if (op_colon_1)
  warn(7021,"A unary colon after a label");

if (op_colon_2)
  warn(7022,"A binary colon in conditional expression");

if (op_comma)
  warn(7023,"A comma operator");

if (op_separator)
  warn(7024,"A comma separator");

if (op_cond)
{
  warn(7025,"A conditional operator");
  trinary=1;
}

if (op_subscript)
  warn(9026,"A subscript operator");

#if DETAILED
if (op_semicolon)
  warn(7027,"A semicolon operator");
#endif

if (op_pointer)
  warn(7028,"A pointer-to declaration operator *");

if (op_indirect)
  warn(7029,"A indirection operator *");

if (op_address)
  warn(7030,"A address-of operator &");

if (op_reference)
  warn(7031,"A C++ reference-to declaration operator &");

if (op_member)
{
  warn(7032,"A member-of operator");
  unary=1;
}

if (op_arrow)
  warn(7033,"A indirect member selector operator");

if (op_memptr)
  warn(7034,"A C++ member-pointer operator");

if (op_memsel)
  warn(7035,"A C++ member-selector operator");

if (op_new||op_delete)
  warn(7036,"A C++ dynamic memory operator");

if (op_scope)
  warn(7037,"A C++ scope operator");

if (op_destroy)
  warn(7038,"A C++ destructor symbol");

/* about control keyword operators */
if (op_do||op_else||op_for||op_goto||op_if||op_return
    ||op_continue||op_break||op_while_1||op_while_2||op_switch)
  warn(7039,"A control keyword operator");

#if DETAILED
if (op_declarator)
  warn(7040,"Any operator found within a declarator");

if (op_init)
  warn(7041,"An initializer operator");

if (op_executable)
  warn(7042,"An operator located within executable code");

if (op_infix)
  warn(7043,"A infix operator");

if (op_prefix)
  warn(7044,"A prefix operator");

if (op_postfix)
  warn(7045,"A postfix operator");

if (op_punct)
  warn(7046,"A punctuation operator");

if (op_white_before)
  warn(7047,"An operator preceded by whitespace");

if (op_white_after)
  warn(7048,"An operator followed by white space");

if (op_low)
  warn(7049,"A low precedence operator");

if (op_medium)
  warn(7050,"A medium precedence operator");

if (op_high)
  warn(7051,"A high precedence operator");
#endif

if (op_keyword)
  warn(7052,"A executable keyword");

if (op_call)
  warn(9053,"Function %s called here with %d arguments",op_function(),op_operands);

if (op_macro_call)
  warn(9054,"Macro function %s to be expanded",op_macro());

if (op_based)
  warn(9055,"A MicroSoft based operator :>");

if (op_assoc)
  warn(9056,"A Metaware association operator");

if (op_iterator)
  warn(9057,"A Metaware iteration-definition operator");

if (op_iterator_call)
  warn(7058,"A Metaware iterator-call operator");

if (unary)
{
  warn(7060,"The type of operand 1 is %d:%s",op_base(1),op_base_name(1));
  unary=0;
  loop=1;
}

if (binary)
{
  warn(7060,"The type of operand 1 is %d:%s",op_base(1),op_base_name(1));
  warn(7060,"The type of operand 2 is %d:%s",op_base(2),op_base_name(2));
  binary=0;
  loop=2;
}

if (trinary)
{
  warn(7060,"The type of operand 1 is %d:%s",op_base(1),op_base_name(1));
  warn(7060,"The type of operand 2 is %d:%s",op_base(2),op_base_name(2));
  warn(7060,"The type of operand 3 is %d:%s",op_base(1),op_base_name(3));
  trinary=0;
  loop=3;
}

if (loop)
{
  i=1;
  while (i<=loop)
  {
    j=0;
    while (j<=op_levels(i))
    {
      switch (op_level(i,j))
      {
         case SIMPLE:
           warn(7070,"The kind of level %d of operand %d is SIMPLE",j,i);
           break;
         case FUNCTION:
           warn(7070,"The kind of level %d of operand %d is FUNCTION",j,i);
           break;
         case REFERENCE:
           warn(7070,"The kind of level %d of operand %d is REFERENCE",j,i);
           break;
         case POINTER:
           warn(7070,"The kind of level %d of operand %d is POINTER",j,i);
           break;
         case ARRAY:
           warn(7070,"The kind of level %d of operand %d is ARRAY",j,i);
           warn(7071,"The dimension of operand %d at level %d is %d",i,j,op_array_dim(i,j));
      }
      if (op_level_flags(i,j)&CONST_FLAG)
        warn(7080,"The flag of level %d of operand %d is CONST",j,i);
      if (op_level_flags(i,j)&VOLATILE_FLAG)
        warn(7080,"The flag of level %d of operand %d is VOLATILE",j,i);
      if (op_level_flags(i,j)&NEAR_FLAG)
        warn(7080,"The flag of level %d of operand %d is NEAR",j,i);
      if (op_level_flags(i,j)&FAR_FLAG)
        warn(7080,"The flag of level %d of operand %d is FAR",j,i);
      if (op_level_flags(i,j)&HUGE_FLAG)
        warn(7080,"The flag of level %d of operand %d is HUGE",j,i);
      if (op_level_flags(i,j)&EXPORT_FLAG)
        warn(7080,"The flag of level %d of operand %d is EXPORT",j,i);
      if (op_level_flags(i,j)&BASED_FLAG)
        warn(7080,"The flag of level %d of operand %d is BASED",j,i);
      if (op_level_flags(i,j)&SEGMENT_FLAG)
        warn(7080,"The flag of level %d of operand %d is SEGMENT",j,i);
      j++;
    }
    i++;
  }
  loop=0;
}

