/*
97.   Declare an explicit default constructor for added clarity.
*/


class myclass {
// bad 97, no con
};

class meclass {

// good 97, has con
meclass();

};
