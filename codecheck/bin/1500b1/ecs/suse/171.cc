/*
171.  Use separate files for each namespace-scope class, struct, union, enumeration, function, or overloaded function group.
*/

#include "ecs.cch"

if ( mod_end ) {
warn(171,"ME c%d m%d f%d", mod_classes, mod_members, mod_functions );   
  if ( mod_classes > COMPLEX_MOD || mod_functions > COMPLEX_MOD ) {
   
    WARN( 171, "Use separate files for each class, struct, union, enumeration, function, or overload" );
  }
}