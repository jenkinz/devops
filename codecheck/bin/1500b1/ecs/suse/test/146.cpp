/*
146.  Avoid multiple return statements in functions.
*/
main() {

  if ( 1 ) return;  // too many return path's
  
  if ( 2 ) return;
  
  return( 0 );
}