/*
88.    Use inline functions instead of macros.
*/

#define _SQRT(x) sqrt(x)
inline double SQRT(int x) {
return ( sqrt(x) );
}

main() {
  
double hypo;

  hypo = _SQRT ( 2 );  // BAD 88, don't use macros
  
  hypo = SQRT ( 2 );    // GOOD 88, inline 
  
}