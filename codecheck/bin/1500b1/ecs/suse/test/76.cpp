/*
76.    Use typedefs to simplify complicated type expressions.
*/

template <class t> class vector {};

class bad {
  public:
    
        const vector<char  *> &  log;    // bad 
    
  bad() {

    const vector<char  *> &  log;    // bad 
  };
  private:
    log log_;
};

class good {
  public:
    typedef vector<char *> logtype;
      logtype &  log;            // good
  good() {
  
    logtype &  log;            // good
  };
  private:
    log log_;
};