/*
102.   Declare a private operator new() to prohibit dynamic allocation.
*/

class myclass1 {
public:
// bad 102
 void * operator new(int);
};

class myclass2 {
public:
  
private:
//good 102    
 void * operator new(int);
};