#include <limits.h>

#include "gen.h"

#include <gen.h>	// bad

#define foo	143849839483 // 11. Use UPPERCASE and underscores for preprocessor macro names.
#define FOO_foo	2 // 

#define ABX_foo	3 // 12.    Add a unique prefix to macro names.

#define NOP	nop();
NOP		// bad 72.    Add a semicolon after every statement expression macro.
NOP;	// good

class foo1; // fwd decl

Foo() {}	// DUMMY Fun def

namespace std {

int h57;	// 27. Avoid the use of digits within names.

typedef int FOO; // 30.    Use "lowerCamelCase"
FOO	FOO_GLO;

class fop {

 int foomem = foo ; // big magic number -Do not use "#define" to define constants

};

class foo1 { };
class foofoo { };

int Foofun() { 		// 16. Use lowerCamelCase for function names.
 int locfoo;

  FOO_GLO ++;		// 64. Design for reentrancy.
}

}
