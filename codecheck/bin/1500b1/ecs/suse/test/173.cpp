/*
173.   Implement class methods outside of their class declaration block.
*/


class foo {

// bad 173, a definition should NOT be made within a class
// Very nasty practice as this is a 'deferred function' and not actually processed until end-of-class
 foo() {
  }
 
 // good 173, just a decl
 ~foo();
 
 };