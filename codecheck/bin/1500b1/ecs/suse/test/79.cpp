/*
79.   Declare enumerations within a namespace or class.
*/

// bad, global space
enum { foo1 };

// good nmsp

namespace foo {
  enum { foo2 };
}

// good class space

class foo {
  
  enum { foo3 };
};