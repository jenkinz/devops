/*
126.  Use C++ streams instead of stdio functions for type safety.
*/

#include <iostream>
#include <stdio.h>  // don't use

main() {

// don't use stdio.h

printf( "hello\n" );

// good

cout << "hello\n";

}