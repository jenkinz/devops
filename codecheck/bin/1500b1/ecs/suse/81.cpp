/*
81.   Declare for-loop iteration variables inside of for statements.
*/

// a loop iterator variable must be local to for-loop

int
main() {

 int i;
 
  for ( i=0; i<1; i++ ) {   // BAD 81
  
  }
  
  for ( int i=0; i<10; i++ ) {  // GOOD 81
  
  }
  
}