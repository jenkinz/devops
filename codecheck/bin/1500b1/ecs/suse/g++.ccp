
### CodeCheck Copyright(C)1988-2005 by Abraxas Software Inc (R). All rights reserved.

# GCC CONFIG

-K8

-D__builtin_va_list=char
-D__const=const


#DEFINE SECTION GCC

-D__STDC__=1
-D__LINUX__


-D__GNUC__=3
-D__GNUC_MINOR__=3
-D__GNUC_PATCHLEVEL__=3
-D_GNU_SOURCE
-D__GNUG__=3


#INCLUDE SECTION GCC

-I/usr/include/g++
-I/usr/include/g++/x86_64-suse-linux
-I/usr/include/g++/backward
-I/usr/local/include
-I/usr/lib64/gcc-lib/x86_64-suse-linux/3.3.3/include
-I/usr/x86_64-suse-linux/include
-I/usr/include

### CodeCheck Copyright(C)1988-2005 by Abraxas Software Inc (R). All rights reserved.

