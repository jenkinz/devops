/*
77.   Create a zero-valued enumerator to indicate an uninitialized, invalid, unspecified, or default state.
*/

#include "ecs.cch"

int default_enum_seen;

if ( tag_begin && tag_kind == ENUM_TAG ) {
//warn(77, "TB %s", tag_name() );  
  default_enum_seen = 1;  
  
}

if ( tag_end ) default_enum_seen = 0;

if ( dcl_enum ) {

//DEBD("DE")
//DUMP("DE")
  if ( default_enum_seen ) {
  
    if ( default_enum_seen == 1 && !(CMPTOK("Default") || CMPTOK("None")) ) {
      
      WARN( 77, "Create a zero-valued enumerator to indicate an uninitialized." );
    }
    
    default_enum_seen++;
  }

}
