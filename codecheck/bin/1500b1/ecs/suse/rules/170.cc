/*
170.  Use the class name as the filename.
*/

#include "ecs.cch"

if ( tag_begin ) {
//warn( 170, "TB %s %s", tag_name(), class_name() );
  if ( tag_kind == CLASS_TAG ) {
    
    if ( strstr( mod_name(), class_name() ) == 0 ) {
      WARN( 170, "Use the class name as the filename." );
    }
  }
}