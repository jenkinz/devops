// 87.    Do not use void* in a public interface.

#include "ecs.cch"

if ( dcl_member ) {

//warn( 87, "DM %s base=%d dl0=%d dl1=%d acc=%d", dcl_name(), dcl_base, dcl_level(0), dcl_level(1), dcl_access );

  if ( dcl_level(1) == POINTER && dcl_access == PUBLIC_ACCESS ) {
    
      WARN( 87, "Do not use void* in a public interface." );
  }
}