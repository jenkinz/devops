// 175.  Do not put global-scope using or using namespace declarations in a header file.

#include "ecs.cch"


if ( keyword("using") ) {

  if ( lin_header ) {
    
    WARN( 175, "Do not put global-scope using or using namespace declarations in a header file." );
  }
}