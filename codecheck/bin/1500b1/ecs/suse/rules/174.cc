// 174.  Do not name files containing tests or examples the same as template header file names.

#include "ecs.cch"

if ( idn_local ) {

//warn( 174, "ID %s %s %d k%d", idn_name(), idn_base_name(), dcl_base, stm_kind );

  if ( idn_base_name() && strstr(mod_name(),idn_base_name() ) ) {
    
      if ( stm_kind <= SWITCH )
        WARN( 174, "Do not name files containing tests or examples the same as template header file names." );
  }
}