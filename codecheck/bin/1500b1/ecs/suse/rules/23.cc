/*
23.    Use "other" for parameter names in copy constructors and assignment operators.
*/
#include "ecs.cch"

char m_name[MAXIDLEN];  // save method name

if ( dcl_parameter ) {
//DEBD("DP")
//warn(1, "m-name = %s", m_name );
    if ( dcl_base == CLASS_TYPE ) {
        if ( strcmp(m_name,"operator=")==0 || strcmp(m_name,"constructor")==0 ) {

            if ( strcmp(dcl_name(),"other") ) 
            	WARN( 23, "Use \"other\" for param-names in copy constructors and assignment operators." );
        }
    }   
}

if ( op_open_funargs && lin_within_class ) {
//DUMP("OOF")
//warn(0,"OOF %s() %s", dcl_name(), dcl_base_name() );
    strcpy( m_name, dcl_name() );

    if ( strchr( dcl_name(), '=' ) ) {  // overload assignment 
        strcpy( m_name, dcl_name() );
    }
    else {
        strcpy( m_name, dcl_base_name() );
    }

}


