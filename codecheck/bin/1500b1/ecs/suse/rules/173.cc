/*
173.   Implement class methods outside of their class declaration block.
*/

#include "ecs.cch"

if ( dcl_definition ) {

  if ( dcl_function && lin_within_class ) {
    
   WARN( 173, " Implement class methods outside of their class declaration block." );
  }

}