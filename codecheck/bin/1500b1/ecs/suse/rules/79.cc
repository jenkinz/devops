/*
79.   Declare enumerations within a namespace or class.
*/


#include "ecs.cch"




if ( dcl_enum ) {
  
warn( 79, "DE %s sco=%s", dcl_name(), idn_base_name() );  

  if ( ! ( lin_within_class || lin_within_namespace ) ) {
   
    WARN( 79, "Declare enumerations within a namespace or class." );
  }
}
