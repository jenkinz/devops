/*
22.    Name all function parameters.
*/
class myclass {

public:
    myclass (void);     //ok

    myclass(int);       //bad

    void method2();     // inconsistent
};

myclass::myclass(int) {

}
