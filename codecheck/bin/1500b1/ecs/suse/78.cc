/*
78.    Do not define enumerations using macros or integer constants.
*/

#include "ecs.cch"

if ( lex_constant ) {

//warn( 78, "LC %d lmt=%d", lex_constant, lex_macro_token ) ;

  if ( strlen(token())>0 && isdigit(token()[0]) ) {
    
      WARN( 78, "Do not define enumerations using macros or integer constants." );
  }
}

