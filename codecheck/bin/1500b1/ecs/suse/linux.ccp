# config suse linux compiler emulation [ intrinsic macros built into the gnu-gcc compiler ]

# -k8 std gnu-gcc extended C/C++ for PURE ANSI use -k3 C and -k4 C++

-k8

# remove defaults we don't need [ codecheck intrinsics that may get in the way ]

-U_M_I386
-U_MSDOS
-UMSDOS
-U__386__
-U_M_IX86
-U__IBMCPP__

# linux environ

-D__unix__
-D__gnu_linux__
-D__linux__
-D__unix
-D__linux

# emulation defines, version specific - implicit

-d__builtin_va_list=char
-D__const=const
-D__STDC__=1

-D__except=except__
-D__cs=cs__

# special handler because gnu-gcc uses _restrict as keyword and variable in same declaration

-Rignore.cc


# user selectable [ we prefer -D__STRICT_ANSI__=1 [ -ansi ] & posix stdlib interface as default
# 64 bit long-long support is automatic [ #define _GLIBCPP_USE_LONG_LONG 1 ]

-D__STRICT_ANSI__=1
-D_POSIX_SOURCE

 #      __STRICT_ANSI__    //ISO Standard C.
 #      _ISOC99_SOURCE    //Extensions to ISO C89 from ISO C99.

 #       _POSIX_SOURCE    //IEEE Std 1003.1.

                ## _POSIX_C_SOURCE If ==1, like _POSIX_SOURCE; if >=2 add IEEE Std 1003.2;
                ## if >=199309L, add IEEE Std 1003.1b-1993;
                ## if >=199506L, add IEEE Std 1003.1c-1995  */

 #       _POSIX_C_SOURCE

                ## _XOPEN_SOURCE Includes POSIX and XPG things.  Set to 500 if
                 ## Single Unix conformance is wanted, to 600 for the
                ## upcoming sixth revision.
                ## */

 #       _XOPEN_SOURCE

 #       _XOPEN_SOURCE_EXTENDED       //XPG things and X/Open Unix extensions.
 #       _LARGEFILE_SOURCE            //Some more functions for correct standard I/O.
 #       _LARGEFILE64_SOURCE            //Additional functionality from LFS for large files.
 #       _FILE_OFFSET_BITS=N            //Select default filesystem interface.
 #       _BSD_SOURCE                    //ISO C, POSIX, and 4.3BSD things.
 #       _SVID_SOURCE                    //ISO C, POSIX, and SVID things.
 #       _GNU_SOURCE                    //All of the above, plus GNU extensions.
 #       _REENTRANT                    //Select additionally reentrant object.
 #       _THREAD_SAFE