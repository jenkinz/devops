/*
42.   Provide a summary description of every declared element.
*/

#include "ecs.cch"

#define COMMENT_THRESHOLD	2

int comment_seen ;		// last comment seen

if ( lin_is_comment )  {
	comment_seen = lin_number;
}

// ecs 41 uses control data 'comment_seen' from rule 40

if ( dcl_variable ) {
//warn(0, "DV cs=%d", comment_seen );
	if ( (lin_number-comment_seen) > COMMENT_THRESHOLD ) {
	
		WARN( 42,"Provide a summary description of every declared element." );
	}

}