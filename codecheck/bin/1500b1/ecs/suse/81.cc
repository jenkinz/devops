/*
81.   Declare for-loop iteration variables inside of for statements.
*/

#include "ecs.cch"

int dcl_infor;
int infor;

 if ( op_for ) {
    dcl_infor = 0;    // reset
    infor = 1;       // signal in for
 }
 
 if ( stm_is_iter  ) {
   
   infor = 0;      // reset
 //  warn(0, "stm=%d", stm_kind );   
    if ( stm_kind == FOR ) {
   
      if ( dcl_infor == 0 ) WARN( 81, "Declare for-loop iteration variables inside of for statements" );
  }
   
 }
 
 if ( dcl_local ) {
   if ( infor ) dcl_infor++;
   
 }

