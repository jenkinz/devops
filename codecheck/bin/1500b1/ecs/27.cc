//27.    Avoid the use of digits within names.

#include "ecs.cch"

if (  dcl_variable  ) {

warn(27, "DV %s %d %d", dcl_name(), dcl_global, dcl_base );	

if ( dcl_name()!=0 && strpbrk(dcl_name(),"0123456789") ) {

		WARN( 27, "Avoid the use of digits within names.");	

}

}