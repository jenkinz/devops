#define COPYRIGHT "/* CodeCheck Copyright(C)1988-2006 by Abraxas Software Inc (R). All rights reserved. */"

// TOC Generation for C/C++ to HTML

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// #define DEBUG

enum { MAXLINE=1024, LISTSZ=2048, LEN=31, IDLEN=60, FD, GD, ED, PD, LD, FATAL, PID, LID, KEY };

typedef struct sc {
//  struct sc *prev;
  char   symname[IDLEN];
  char	 basename[LEN];
  char   modname[LEN];
  short    flavor;
  short	   attr;
  struct sc *next;
 } Symbol;


// Symbol *symlst[LISTSZ];

Symbol * SymHead['Z'-'A'+1 ] = { 0 };		// global data
char MODNAME[LEN];
char PRJNAME[LEN];

 // protos

void fatal( char * );
void htmlout() ;
char * firstalpha( char * );

Symbol * install( char * );
void loadsym( char * fn );
int main( int argc, char * * argv );	  
void thumbentry( FILE *, Symbol * )	;
void thumbfields( FILE * );
void thumbtags( FILE * );


int main( int argc, char** argv )
{
 	if ( argc <= 1) fatal( "usage: toc toc.sym toc.html" );

  	loadsym( argv[1] ); // load symbol table
  
	htmlout( argv[2]);

	return( 0 ) ; // Signal OK for unix compatibility
}
void htmlout(char *fn)
{
  FILE * hfp;

 	 hfp = fopen( fn, "w" ); // open  html TOC file
  	 if ( hfp == 0 ) fatal( "Opening html TOC file" );

	 fprintf( hfp, "<html>\n<head>\n" ); // open html head

	 fprintf( hfp, "<! %s >\n", COPYRIGHT );

	 fprintf( hfp, "<title> Table of Contents for ECS C++  Checker 1998:2004 %s</title>\n", PRJNAME );
 
	 fprintf( hfp, "<body>\n" );

  fprintf( hfp, "<h4><a href=""http://www.abraxas-software.com"">ABRAXAS SOFTWARE</a> - CodeCheck ECS C++ Test-Suite ECS .</h2>\n" );

   	 fprintf( hfp, "<pre><a href=""ecs-index.html"">Abraxas/ECS C++ Home</a>  " );
	 fprintf( hfp, "<a href=""toc.html"">Table of Contents</a></pre><hr>\n" );

	 thumbtags( hfp ); // generate thumb tags - <a href="#Thumb-A">A</a>

	 fprintf( hfp, "<hr>\n" ); // add horizontal rule

 
	 fprintf( hfp, "<h1>\nIndex of all ECS C++-C Objects and Methods \n</h1>" );

	 thumbfields( hfp ); // generate thumb fields

	 fprintf( hfp, "<pre><a href=""ecs-index.html"">Abraxas/ECS C++ Home</a>  " );
	 fprintf( hfp, "<a href=""toc.html"">Table of Contents</a></pre><hr>\n" );

  fprintf( hfp, "<h4><a href=""http://www.abraxas-software.com"">ABRAXAS SOFTWARE</a> - CodeCheck ECS C++ Test-Suite .</h2>\n" );

 	 fprintf( hfp, "</body>\n</head>\n</html>\n" ); // close html head
    
	 fclose( hfp );
}

void loadsym( char *fn )
{
 FILE * symfp;
 char buffer[MAXLINE];
 char type[IDLEN], name[IDLEN], base[IDLEN];
 Symbol * sp;

 char *cp;
 int i=0;
 
	symfp = fopen( fn, "r" ); // get symbol table file

 
	if ( symfp == NULL )
    {
        fatal( "can not open file [ %s ]\n" );
    }

	printf( "\nGenerating Table of Contents ... \n" );

	while ( fgets( buffer, MAXLINE, symfp ) ) {
	  
	  sscanf( buffer,"%s %s %s", type, name, base );

//printf( "install:%s %s %s\n", type, name, base );
 
	  if ( strcmp( type, "MN" ) == 0 ) {
		  strncpy( MODNAME, name, LEN );  // drop suffix

// poc 01aug2002 be consistent with toc.cc use strchr()

		  cp = 	strchr( MODNAME, '.') ;
		  if ( cp ) *cp = 0;
	  }
	  else if ( strcmp( type, "PN" ) == 0 ) {

		  strncpy( PRJNAME, name, LEN );	// drop suffix
		   cp = strrchr( PRJNAME, '.');
		   if ( cp ) *cp = 0;
	  }
	  else {
			sp = install( name );	
		  
			strcpy( sp->basename, base );		// add base name
			strcpy( sp->modname, MODNAME );	// add module definition name

			if ( strcmp( type, "FD" ) == 0 ) {
				sp->flavor = FD;
			}
			else if ( strcmp( type, "GD" ) == 0 ) {
				sp->flavor = GD;
			}			

			i++;	// increment insertion count
	  }
 
	  printf( "Loading %d Symbols\r", i );


	}	 // end line buffering loop

      printf( "Loading %d Symbols ... Done\n", i );

}


Symbol *
install( s )
char *s;
{ 
 Symbol *cursp, *newsp, *nextsp;
 char *sp;		// first alpha char in string
 int i;

//printf("install: %s \n", s ); 
 
    if ((newsp=(Symbol *) malloc(sizeof(Symbol))) == NULL)
		fatal( "out of symbol heap space" );
   
	strcpy( newsp->symname, s );		// copy original string name to elem

	sp = firstalpha( s );
//printf( "Cmp = %s\n", sp ); 
 
	if (  *sp == 0 ) fatal( "Illegal Symbol" );
	else i = toupper( *sp ) - 'A';
 
	if ( SymHead[i] == 0 ) { // index head by A-Z

		SymHead[i] = newsp;
		newsp->next = 0;
	}
	else {

		if (  strnicmp( sp, SymHead[i]->symname, IDLEN )  > 0 ) {	 // after head

	  	   for ( cursp = SymHead[i] ; cursp; cursp = nextsp ) {   // find insertion pt.
			   	
			    nextsp = cursp->next;

			    if ( nextsp == 0 ) break;	  // last

			    if ( (strnicmp( sp, nextsp->symname, IDLEN ) < 0) ) break; // LT next
  		   }
 
    		if ( nextsp ) {					// insert
				
				newsp->next = nextsp;
				cursp->next = newsp;
			}
			else {							// append
				cursp->next = newsp;
				newsp->next = 0;
			}
		 }
	   	 else  {						 
 			newsp->next = SymHead[i];
	  		SymHead[i] = newsp;				// insert new head
		 }
 	}
 
    return ( newsp );
}

void
fatal( m)
char *m;
{
  fprintf( stderr, "Fatal Error - %s\n", m );
  exit(-1);
}

/**
* Generate Thumb Tags
@param fp file pointer for TOC file
@return no return value 
thumbtags( fp )
*/

void thumbtags( fp )
FILE *fp;
{
  // <a href="#Thumb-A">A</a>
 int i;

	for ( i='A'; i<='Z'; i++ ) {

		fprintf( fp, " <a href=\"#Thumb-%c\">%c</a>\n", i, i );
	}

 }

void thumbfields( fp )
FILE *fp ;
{
	Symbol *cursp;
	char *cp;
    int i, seenchar;

	for ( i='A'; i<'Z'; i++ ) 	{  // for all alphabet
  
		seenchar = 0; // mark when seen

		fprintf( fp, "<a name=""Thumb-%c""></a>\n", i ); 

		for ( cursp=SymHead[i-'A'] ; cursp; cursp = cursp->next ) {   
 
			cp = cursp->symname;
	   
  				  if ( seenchar == 0 ) {
	                fprintf( fp, "<h2><a name=""Thumb-%c""><b> %c </b></a></h2>\n", i, i );
					seenchar++; // mark as seen
				  }

				  // output methods & globals
				  thumbentry( fp, cursp )  ;

		} // for all list

		if ( seenchar ) fprintf( fp,"<hr>\n" ); // mark end of list
	}
}

/* 
<dl>
<dt> 	<a href="Simple1.html#disable_tracing()"><b>disable_tracing</b></a>().
Static method in class <a href="Simple1.html#_top_">Simple1</a>
<dd> 
</dl>
*/
void thumbentry( fp, sp )
FILE *fp;
Symbol *sp;
{
	char *mod, *sym;
	
	mod = sp->modname;
	sym = sp->symname;
    
	fprintf( fp, "<dl>\n" ); // open entry
   
 	fprintf( fp, "<dt> <a href=""%s.html#%s()""><b>%s</b></a>", mod, sym, sym );

	if ( sp->flavor == FD ) {
	  fprintf( fp, "(). <i>%s</i> Method in module <a href=""%s.html#_top_>%s</a>", \
			sp->basename, mod, mod );
	}
	else if ( sp->flavor == GD ) {

	  fprintf( fp, ". <i>%s</i> Global Variable in Module <a href=""%s.html#_top_>%s</a>", \
			sp->basename, mod, mod );

	}

	fprintf( fp, "</dd>\n</dl>\n" );

}

char * firstalpha( s )
char *s;
{
 	while ( *s ) {
		if ( isalpha(*s) ) break;  // return ptr to first alpha char
		s++;
	}
	
	return( s );
}
