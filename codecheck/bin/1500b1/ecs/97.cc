
/*
97.   Declare an explicit default constructor for added clarity.
*/

#include "ecs.cch"

if ( tag_end ) {
  if ( tag_has_default == 0 ) {
  
      WARN(97, "Declare an explicit default constructor for added clarity." );
  }
}
