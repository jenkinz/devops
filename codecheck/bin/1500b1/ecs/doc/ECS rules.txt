Cambridge Univ Press copyright 2004 (C) "elements of C++ style"

Authors:
Trevor Misfeldt
Gregory Bumgardner
Andrew Gray

Use of this material ASSUMES YOU have "bought the book".
�
1.���� General Principles
1.������ Adhere to the style of the original.
2.������ Adhere to the Principle of Least Astonishment.
3.����� Do it right the first time.
4.������ Document any deviations.
2.�������� Formatting Conventions
5.����� Use indented block statements.
6.����� Indent statements after a label.
7.������ Choose one style for brace placement.
8.����� Break long statements into multiple lines.
9.������ Include white space.
10.��� Do not use �hard� tabs.
3.�������� Naming Conventions
11.��� Use UPPERCASE and underscores for preprocessor macro names.
12.��� Add a unique prefix to macro names.
13.��� Use �UpperCamelCase� for classes, constants, structures, enumerations, and typedefs.
14.��� Use nouns to name compound types.
15.�� Pluralize the names of collections.
16.��� Use �lowerCamelCase� for function names.
17.��� Use verbs to name functions.
18.��� Use �is�, �set�, and �get� to name accessor and mutator functions.
19.��� Use �lowerCamelCase� for variable and function parameter names.
20.��� Use nouns to name variables.
21.��� Add a prefix or suffix to member variable names to distinguish them from other variables.
22.��� Name all function parameters.
23.��� Use "other" for parameter names in copy constructors and assignment operators.
24.��� Give function parameters the same name as the member variables you assigned them to.
25.��� Use meaningful names.
26.��� Use familiar names.
27.��� Avoid the use of digits within names.
28.��� Avoid excessively long names.
29.��� Join the vowel generation�use complete words.
30.��� Use �lowerCamelCase� for abbreviations.
31.��� Do not use case to differentiate names.
4.�������� Documentation Conventions
32.�� Document your software interface for those who must use it.
33.�� Document your implementation for those who must maintain it.
34.��� Keep your comments and code synchronized.
35.�� Embed API reference documentation in your source code.
36.�� Generate API reference documentation directly from the source code.
37.�� Document all significant software elements.
38.�� Document software elements as early as possible.
39.��� Use block comments to describe the programming interface.
40.��� Use one-line comments to explain implementation details.
41.��� Use a single consistent format and organization for all documentation comments.
42.�� Provide a summary description of every declared element.
43.�� Document the interface exposed by every function.
44.�� Document thread synchronization requirements.
45.�� Provide examples to illustrate common and proper usage.
46.�� Document important preconditions, postconditions, and invariant conditions.
47.�� Document known defects and deficiencies.
48.��� Use the active voice to describe actors and passive voice to describe actions.
49.��� Use �this� rather than �the� when referring to instances of the current class.
50.�� Explain why the code does what it does.
51.��� Avoid the use of end-line comments.
52.��� Label closing braces in highly nested control structure.
53.��� Add a �fall-through� comment between two case labels if no break statement separates those labels.
54.��� Use keywords to mark pending work, unresolved issues, defects, and bug fixes.
5.�������� Programming Principles
55.��� Do not be afraid to do engineering.
56.�� Choose simplicity over elegance.
57.��� Do not use a feature of C++ just �because it is there.�
58.�� Recognize the cost of reuse.
59.�� Program by contract.
60.��� Keep classes simple.
61.��� Define subclasses so they may be used anywhere their superclasses may be used.
62.��� Use inheritance for �is a� relationships and containment for �has a� relationships.
63.��� Avoid multiple inheritance.
64.�� Design for reentrancy.
65.��� Use threads only where appropriate.
66.��� Avoid unnecessary synchronization.
67.��� Do not synchronize access to code that does not change shared state.
6.�������� Programming Conventions
68.��� Use �#include ���� for collocated header files and �#include <�>� for external header files.
69.��� Place preprocessor include guards in header files.
70.��� Use #if..#endif �and #ifdef..#endif instead of �/*�*/� comments to hide blocks of code.
71.��� Use macros sparingly.
72.��� Add a semicolon after every statement expression macro.
73.��� Use macros to capture the current file name and line number.
74.��� Do not use �#define� to define constants�declare static const variables instead.
75.��� Use portable types for portable code.
76.��� Use typedefs to simplify complicated type expressions.
77.�� Create a zero-valued enumerator to indicate an uninitialized, invalid, unspecified, or default state.
78.��� Do not define enumerations using macros or integer constants.
79.�� Declare enumerations within a namespace or class.
80.�� Declare global functions, variables, or constants as static members of a class.
81.�� Declare for-loop iteration variables inside of for statements.
82.��� Use an enumeration instead of a Boolean to improve readability.
83.��� Use an object pointer instead of a reference if a function stores a reference or pointer to the object.
84.�� Accept objects by reference and primitive or pointer types by value.
85.��� Use a const char* for narrow character string parameters.
86.��� Pass enumerator values, not integer constants.
87.��� Do not use void* in a public interface.
88.��� Use inline functions instead of macros.
89.��� Inline only the simplest of functions.
90.�� Factor functions to allow inlining of trivial cases.
91.��� Define small classes and small methods.
92.��� Build fundamental classes from standard types.
93.��� Avoid the use of virtual base classes in user-extensible class hierarchies.
94.�� Declare the access level of all members.
95.�� Declare all member variables private.
96.��� Avoid the use of friend declarations.
97.�� Declare an explicit default constructor for added clarity.
98.�� Always declare a copy constructor, assignment operator, and destructor if the class can be instantiated.
99.�� Always implement a virtual destructor if your class may be subclassed.
100.� Make constructors protected to prohibit direct instantiation.
101.� Make constructors private to prohibit derivation.
102.�� Declare a private operator new() to prohibit dynamic allocation.
103.�� Declare a protected or private destructor to prohibit static or automatic allocation.
104.�� Declare single-parameter constructors as explicit to avoid unexpected type conversions.
105.� Use default arguments to reduce the number of constructors.
106.� Do not overload non-virtual methods in subclasses.
107.�� Declare virtual methods protected and call them from public non-virtual methods.
108.� Keep your functions �const-correct.�
109.� Use object pointers and references in class declarations.
110.�� Adhere to the natural semantics of operators.
111.� Do not overload operator&&() or operator||().
112.�� Invoke the superclass assignment operator(s) in the assignment operator of a subclass.
113.�� Implement copy-safe and exception-safe assignment operators.
114.� Define binary operators outside of a class.
115.�� Implement a Boolean operator in terms of its opposite.
116.� Use templates instead of macros to create parameterized code.
117.� Do not use CV-qualified types as template parameters.
118.� Use C++ casting operators instead of C-style casts.
119.� Avoid type casting and do not force others to use it.
120.� Use static_cast<> to expose non-intuitive implicit conversions.
121.� Do not use reinterpret_cast<> in portable code.
122.� Only use const_cast<> on �this� or when dealing with non-const-correct code.
123.� Never use dynamic_cast<> as a substitute for polymorphism.
124.� Use dynamic_cast<> to restore lost type information.
125.�� Always treat string literals as const char*.
126.� Use C++ streams instead of stdio functions for type safety.
127.� Test all type conversions.
128.�� Initialize all variables.
129.� Do not rely on the order of initialization of global objects.
130.�� Always construct objects in a valid state.
131.�� Initialize member variables in the initializer list.
132.�� Initialize member variables in the order they are declared.
133.�� Indicate when the declaration order of data members is significant.
134.�� Always list any superclass constructors in the initializer list of a subclass constructor.
135.� Do not call virtual functions in constructors and destructors.
136.�� Declare and initialize static variables within functions.
137.� Zero pointers after deletion.
138.� Use the new and delete operators instead of malloc() and free().
139.� Do not rely on operator precedence in complex expressions.
140.� Use block statements in control flow constructs.
141.� Do not test for equality with true.
142.�� Replace repeated, non-trivial expressions with equivalent methods.
143.� Use size_t variables for simple loop iteration and array subscripts.
144.� Use a dummy template function to eliminate warnings for unused variables.
145.� Avoid break and continue in iteration statements.
146.� Avoid multiple return statements in functions.
147.� Do not use goto.
148.� Do not use try..throw..catch to manage control flow.
149.� Never use setjmp() or longjmp() in a C++ program.
150.�� Always code a break statement in the last case of a switch statement.
151.� Use return codes to report expected state changes.
152.� Use assertions to enforce a programming contract.
153.� Do not silently absorb or ignore unexpected runtime errors.
154.� Use assertions to report unexpected or unhandled runtime errors.
155.� Use exceptions to report errors that may occur under normal program execution.
156.�� Manage resources with RAII for exception safety.
157.� Catch exceptions by reference, not by value.
158.� Do not discard exception information if you throw a new exception within a catch block.
159.� Avoid throwing exceptions in destructors.
160.� Use lazy evaluation and initialization.
161.� Reuse objects to avoid reallocation.
162.� Leave optimization for last.
7.�������� Packaging Conventions
163.� Use unnamed namespaces instead of static to hide local functions and variables.
164.� Tread lightly on the global namespace.
165.� Place types that are commonly used, changed, and released together, or mutually dependent on each other, into the same package.
166.� Isolate unstable classes in separate packages.
167.� Avoid making difficult-to-change packages dependent on packages that are easy to change.
168.�� Maximize abstraction to maximize stability.
169.�� Capture high-level design and architecture as stable abstractions organized into stable packages.
170.� Use the class name as the filename.
171.� Use separate files for each namespace-scope class, struct, union, enumeration, function, or overloaded function group.
172.� Use #include sparingly in header files.
173.�� Implement class methods outside of their class declaration block.
174.� Do not name files containing tests or examples the same as template header file names.
175.� Do not put global-scope using or using namespace declarations in a header file.


� 

�