/*
52.    Label closing braces in highly nested control structure.
*/

// BAD
foo1() {

	if ( 1 ) {
		if ( 2 ) {
			if ( 3 ) {
			}
		}
	}

}

// GOOD
foo2() {

	if ( 1 ) {
		if ( 2 ) {
			if ( 3 ) {
			}	// good
		}		// good
	}			// good

}
