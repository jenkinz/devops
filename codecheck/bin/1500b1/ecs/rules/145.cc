/*
145.  Avoid break and continue in iteration statements.
*/

#include "ecs.cch"

int iter_seen ;    // stack depth of if, zero when out of 'IF...FI'

if ( op_do || op_while_1 || op_for ) {
//warn( 145, "ITER" );

  iter_seen++;    // push IF stack
  
}

if ( stm_end ) {
//warn(135, "SE kind=%d", stm_kind );

  if ( stm_kind >= WHILE && stm_kind <= FOR ) {
   
     iter_seen--;    // pop if stack
  }
}

if ( op_continue || op_break ) {
  
  if ( iter_seen ) {
  
     WARN( 145, "Avoid break and continue in iteration statements." );   
  }
}