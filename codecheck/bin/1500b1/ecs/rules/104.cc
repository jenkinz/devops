/*
104.   Declare single-parameter constructors as explicit to avoid unexpected type conversions.
*/

#include "ecs.cch"

int parmseen;


if ( dcl_parameter ) {
 
//warn( 104, "DP %d %d", dcl_parameter, dcl_parm_count );  
  parmseen = dcl_parameter;
}


if ( dcl_function ) {
//warn( 104, "DF dpc%d f%d p%d", dcl_parm_count, dcl_function_flags, dcl_parameter ); 

  if ( (dcl_function_flags & EXPLICIT_FCN)==0 && parmseen == 1 ) {
    
    WARN( 104, "Declare single-parameter constructors as explicit to avoid unexpected type conversions." );
    
  }
}