/*
82.    Use an enumeration instead of a Boolean to improve readability.
*/

#include "ecs.cch"

int incall;

if ( op_open_funargs ) {
//warn( 82, "OOF" );  
  incall++;
}

if ( op_close_funargs ) {
  incall--;
}

if ( keyword("true") || keyword("false" ) ) {
//warn( 82, "bool" );  
  if ( incall ) {
    WARN( 82, "Use an enumeration instead of a Boolean to improve readability." );
  }
}