// 118.  Use C++ casting operators instead of C-style casts.

#include "ecs.cch"

if ( op_cast ) {

//DUMP( "OC" )

  WARN( 118, "Use C++ casting operators instead of C-style casts." );
}