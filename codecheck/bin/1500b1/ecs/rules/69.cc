
/*
69.    Place preprocessor include guards in header files.
*/

#include "ecs.cch"

// verify that every project header has wrapper, simple approach for pro-approach use wrapper.cc

int wrapchk;

if ( lin_preprocessor  ) {
  
//warn(69, "LP %d %s w%d", lin_preprocessor, pp_name(), wrapchk );

  if ( lin_preprocessor == INCLUDE_PP_LIN ) {
    wrapchk = 1;
  }
  else {
    
    if ( wrapchk && lin_header ) {
      
      if ( lin_preprocessor != IFNDEF_PP_LIN ) {
        WARN( 69, "Place preprocessor include guards in header files." );
      }
    }
    wrapchk = 0;
  }
}



