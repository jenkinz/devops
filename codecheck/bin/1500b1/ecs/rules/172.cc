/*
172.  Use #include sparingly in header files.
*/

#include "ecs.cch"

if ( pp_include ) {
//warn( 172, "PI %d %d", pp_include, lin_header );
  
  if ( lin_header && pp_include <= 2 ) {
    
    WARN( 172, "Use #include sparingly in header files" );
  }

}

