/*
64.    Design for reentrancy.
*/

typedef int FOO;   
FOO	FOO_GLO;

// multi-thread code cannot use global's each method must have unique instance

int Thread() { 		

 int locfoo;

  FOO_GLO ++;		// 64. Design for reentrancy.
}