/*
145.  Avoid break and continue in iteration statements.
*/

main() {

  while ( 1 ) {
    
        
     break;      // no
     
     continue;   // no
  }

    for ( ;; ) {
      
      // Avoid break and continue in iteration statements.
      
      break;
      
    }
}