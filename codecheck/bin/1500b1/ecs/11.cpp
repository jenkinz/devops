/*

//[   3.         Naming Conventions         ]

11.    Use UPPERCASE and underscores for preprocessor macro names.
*/

#define foo	143849839483 // 11. Use UPPERCASE and underscores for preprocessor macro names.

#define FOO_foo	2       // bad

#define ABX_FOO	3       // ok
