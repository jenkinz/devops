/*
80.   Declare global functions, variables, or constants as static members of a class.
*/


// bad, should be static in class

int PI = 3.14159;

int glofoo(void);


class foo {
  
  // good
  static int global;
  
  static int glofoo( void );
  
};