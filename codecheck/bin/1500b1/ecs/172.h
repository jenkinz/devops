/*
172.  Use #include sparingly in header files.
*/

// 172.h #includes "gen.h" [ prj header ], its ok for 172.h to #include <> system headers

#include <climits>  // system header may be ok?

// BAD 172 header files themselves should NOT reference other header files
#include "gen.h"  // dummy include

