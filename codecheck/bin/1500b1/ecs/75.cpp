/*
75.    Use portable types for portable code.
*/


// bad over-riding #include <inttypes.h>

typedef short int16;    

typedef long int32;

/* 
  <inttypes.h> will generate the above for you automatically this make's portable code!
  It's best to #include <inttypes.h> and then let your compiler create the types for you.
*/