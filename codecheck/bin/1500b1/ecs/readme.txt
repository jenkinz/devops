Rename the chkdemo.pxp to chkdemo.exe


The FREE ecs test package [ ecs-complete-zip ] has two versions, the Microsoft C++ version, the GCC Linux Version. It is completely valid to apply the ecs rules to C++ if you wish.

1.) The MSDEV Case is ..[ first run vc98\vcvars6.bat or equiv to set INCLUDE path ]

    chkdemo -Recs.cco yourfile.c
   [ By default on windows, CodeCheck support MSDEV-MSTUDIO C, -k3 mode. VC6-1998, VC7-2003, VC8 -2005 vc7.ccp attached ]


2.) The LINUX-GCC case is ...

    ccdemo linux.ccp gcc-xxx.ccp -Recs.cco yourfile.c
    The linux.ccp file is in the package this contains the emulation data to tell codecheck how to process  linux-gcc 



***

Full testing ... Windows example ...


// no rule-file application

C:\work\codchk\ecs\test>chkdemo ecs-test.ccp


// rule-file application 

C:\work\codchk\ecs\ecs-2004>chkdemo ecs.ccp -recs.cco


***

Contact support@abxsoft.com for current linux and/or solaris demo version of codecheck engine


The creation of gcc-xxx.ccp is NOT tivial, it requires that you read our "how-to-gcc.txt" notes, and follow directions. For ALL other operating systems and C/C++ compilers contact us directly, or view our document "codecheck-technical-notes.pdf' at www.abxsoft.com/pdf.
