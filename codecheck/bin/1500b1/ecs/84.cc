/*
84.   Accept objects by reference and primitive or pointer types by value.
*/

#include "ecs.cch"

if ( dcl_parameter ) {
//warn( 84, "DM %s %d %d", dcl_name(), dcl_base, dcl_level(0) );

  if ( dcl_level(0)==REFERENCE && dcl_base<=ENUM_TYPE ) {
  
    WARN( 84, " Accept objects by reference and primitive or pointer types by value." );
  }
}
