/*
75.    Use portable types for portable code.
*/
#include "ecs.cch"

if ( dcl_typedef && dcl_name() != 0 ) {

DEBD("DT")

  if ( strcmp(dcl_name(),"int16")==0 || strcmp(dcl_name(),"int32")==0 ) {
    
    WARN( 75, "Use portable types for portable code." );
  }

}