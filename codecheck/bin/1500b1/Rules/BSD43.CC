/*	BSD43.cc

	Copyright (c) 1992-95 by Abraxas Software. All rights reserved.
==============================================================================
	Purpose:	Flags BSD 4.3 features that are not POSIX.1 conforming.
	Author:		Loren Cobb.
	Revision:	12 October 1994.
                        16, March, 1994. Mask the message for the functions
                        which are class member functions and have the same
                        names in the list. 
	Format:		Monospaced font with 4 spaces/tab.
==============================================================================

Abstract:

	These CodeCheck rules generate warning messages when BSD 4.3 features
	are used that are not POSIX conforming. If possible, these messages
	will suggest the appropriate POSIX feature to use.
	
	The features flagged in these rules are the ones that I know about as
	of the date on this rule file. If you know of others that ought to be
	flagged, please fax me the details at your earliest convenience. The
	Abraxas fax number is 503-244-8375. Many thanks in advance!

Warning Codes:

2000	Precede all headers with #define _POSIX_SOURCE.
2001	Replace function <BSD function> with <POSIX function>.
2002	Function <BSD function> has no POSIX equivalent.
2003	Function <BSD function> is not needed in POSIX.
2004	POSIX requires #include <POSIX header> for <function>.
2005	Replace <BSD header> with <POSIX header>.
2006	Replace <BSD macro> with <POSIX macro>.
2007	If you need an audible alarm, use \a instead of \07.
2008	Replace tag <BSD name> with <POSIX name>.


Suggested Actions:

2000	The macro _POSIX_SOURCE must be defined for POSIX headers to be
		read correctly. Define this macro at the top of every source file.
2001    Look up the replacement function in a POSIX reference - the return
		type and some arguments may differ from the BSD function, and a
		POSIX header may need to be included.
2002	You will need to hand-code a replacement function.
2003	No call to this function is needed in a standard POSIX environment.
2004	Insert the appropriate #include after #define _POSIX_SOURCE.
2005	Change the name of the BSD header to the appropriate POSIX header.
2006	Change the name of the BSD macro to the appropriate POSIX macro.
2007	If you need an audible alarm, change \07 to \a in the string.
2008	Change the name of the BSD tag to the appropriate POSIX tag.


Useful References:

Horton, Mark R. (1990) "Portable C Software." Published by Prentice-Hall,
		Englewood Cliffs, NJ 07632, USA.

IEEE (1988) "IEEE Standard Portable Operating System Interface for Computer
		Environments 1003.1-1988." Published by IEEE, 345 East 47th Street,
		New York, NY 10017, USA.

Lewine, Donald A. (1991) "POSIX Programmer's Guide."  Published by O'Reilly &
		Associates, 632 Petaluma Avenue, Sebastopol, CA 95472, USA.

Zlotnick, Fred (1991) "The POSIX.1 Standard." Published by Benjamin/Cummings,
		390 Bridge Parkway, Redwood City, CA 94065, USA.

==============================================================================
*/

#define		NEED_POSIX		2000
#define		REPLACE_FCN		2001
#define		NO_EQUIV		2002
#define		NOT_NEEDED		2003
#define		INCLUDE			2004
#define		REPLACE_HDR		2005
#define		REPLACE_CON		2006
#define		ALARM			2007
#define		REPLACE_TAG		2008

#define REPLACE(fname,gname)  if ( strcmp(idn_name(),fname) == 0 )		\
			{																\
			warn(REPLACE_FCN, "Replace " fname " with " gname ".");			\
			++items_flagged;												\
			}

#define REWRITE(fname)  if ( strcmp(idn_name(),fname) == 0 )				\
			{																\
			warn(NO_EQUIV, "Function " fname " has no POSIX equivalent.");	\
			++items_flagged;												\
			}

#define DELETE(fname)  if ( strcmp(idn_name(),fname) == 0 )				\
			{																\
			warn(NOT_NEEDED, "Function " fname " is not needed in POSIX.");	\
			++items_flagged;												\
			}

#define CALLED(fname)  (strcmp(idn_name(),fname) == 0)


int		posix_needed,		//  1 if macro _POSIX_SOURCE has not yet been defined.
		unistd_needed,		//  1 if header unistd.h	has not yet been included.
		items_flagged;		//  Number of non-POSIX features found in this module.

int		sys_dir_included,	//  1 if header sys/dir.h	has been #included.
		sys_time_included,	//  1 if header sys/time.h	has been #included.
		time_included,		//  1 if header time.h		has been #included.
		unistd_included;	//  1 if header unistd.h	has been #included.


if ( mod_begin )
	{
	posix_needed		= 1;
	unistd_needed		= 1;
	items_flagged		= 0;
	sys_dir_included	= 0;
	sys_time_included	= 0;
	time_included		= 0;
	unistd_included		= 0;
	}

if ( pp_macro )
	if ( strcmp(pp_name(), "_POSIX_SOURCE") == 0 )
		posix_needed = 0;

if ( header_name() )
	{
	if ( posix_needed )
		{
		warn( NEED_POSIX, "Precede all headers with #define _POSIX_SOURCE" );
		posix_needed = 0;
		++items_flagged;
		}

	if ( unistd_needed )
		{
		if ( strcmp(header_name(), "unistd.h") != 0 )
			warn( INCLUDE, "POSIX recommends #include <unistd.h> before this line." );
		unistd_needed = 0;
		++items_flagged;
		}

	if ( strcmp(header_name(),"sys/dir.h") == 0 )
		{
		warn( REPLACE_HDR, "Replace <sys/dir.h> with <dirent.h>." );	//	MRH:328
		++items_flagged;
		}
	else if ( strcmp(header_name(),"sys/param.h") == 0 )
		{
		warn( REPLACE_HDR, "Replace <sys/param.h> with <unistd.h>." );
		++items_flagged;
		}
	else if ( strcmp(header_name(),"sys/time.h") == 0 )
		{
		warn( REPLACE_HDR, "Replace <sys/time.h> with <time.h>." );
		++items_flagged;
		sys_time_included = 1;
		}
	else if ( strcmp(header_name(),"unistd.h") == 0 )
		{
		unistd_included = 1;
		unistd_needed = 0;
		}
	else if ( strcmp(header_name(),"varargs.h") == 0 )
		{
		warn( REPLACE_HDR, "Replace <varargs.h> with <stdarg.h>." );
		++items_flagged;
		}
	}

if (idn_function&&!idn_member)
	{
	     REPLACE( "alloca"			, "malloc"					)		//  DAL:566
	else REPLACE( "bcmp"			, "strncmp"					)		//  DAL:566
	else REPLACE( "bcopy"			, "strncmp"					)		//  DAL:566
	else REPLACE( "cuserid"			, "getlogin or getpwuid"	)		//  DAL:249
	else REPLACE( "ecvt"			, "sprintf"					)		//  DAL:566
	else REPLACE( "fcvt"			, "sprintf"					)		//  DAL:566
	else REPLACE( "flock"			, "fcntl"					)		//  DAL:566
	else REPLACE( "gcvt"			, "sprintf"					)		//  DAL:566
	else REPLACE( "getdtablesize"	, "sysconf"					)		//  DAL:566
	else REPLACE( "getpw"			, "getpwent"				)		//  DAL:566
	else REPLACE( "gettimeofday"	, "localtime and time"		)		//  DAL:566
	else REPLACE( "getwd"			, "getcwd"					)		//  DAL:566
	else REPLACE( "index"			, "strchr"					)		//  DAL:566
	else REPLACE( "initstate"		, "srand"					)		//  DAL:566
	else REPLACE( "ioctl"			, "[see a POSIX.1 book]"	)		//  DAL:566
	else REPLACE( "killpg"			, "kill"					)		//  DAL:566
	else REPLACE( "mknod"			, "mkdir or mkfifo"			)		//  DAL:566
	else REPLACE( "mktemp"			, "tmpnam"					)		//  DAL:487
	else REPLACE( "pclose"			, "close"					)		//  DAL:566
	else REPLACE( "popen", "pipe, fdopen, fork, system, or wait")		//  DAL:566
	else REPLACE( "random"			, "rand"					)		//  DAL:566
	else REPLACE( "rindex"			, "strrchr"					)		//  DAL:567
	else REPLACE( "scandir"			, "readdir, malloc, qsort"	)		//  DAL:567
	else REPLACE( "seekdir"			, "opendir, readdir"		)		//  DAL:567
	else REPLACE( "setbuffer"		, "setvbuf"					)		//  DAL:567
	else REPLACE( "setitimer"		, "alarm"					)		//  DAL:567
	else REPLACE( "setlinebuf"		, "setvbuf"					)		//  DAL:567
	else REPLACE( "setregid"		, "setgid and setegid"		)		//  DAL:567
	else REPLACE( "setreuid"		, "setuid and setuegid"		)		//  DAL:567
	else REPLACE( "setstate"		, "srand"					)		//  DAL:567
	else REPLACE( "sigblock"		, "sigprocmask"				)		//  DAL:567
	else REPLACE( "signal"			, "sigaction"				)		//  DAL:420
	else REPLACE( "sigpause"		, "sigsuspend"				)		//  DAL:567
	else REPLACE( "sigsetmask"		, "sigprocmask"				)		//  DAL:567
	else REPLACE( "sigvec"			, "sigpending"				)		//  DAL:567
	else REPLACE( "srandom"			, "srand"					)		//  DAL:567
	else REPLACE( "system"			, "[see a POSIX.2 book]"	)		//  DAL:470
	else REPLACE( "timezone"		, "localtime"				)		//  DAL:567
	else REPLACE( "utimes"			, "utime"					)		//  DAL:567
	else REPLACE( "valloc"			, "malloc"					)		//  DAL:567
	else REPLACE( "vfork"			, "fork"					)		//  DAL:567
	else REPLACE( "vhangup"			, "tcsetattr"				)		//  DAL:567
	else REPLACE( "wait3"			, "waitpid"					)		//  DAL:567

	else REWRITE( "bzero"			)		//  DAL:566
	else REWRITE( "cabs"			)		//  DAL:566
	else REWRITE( "ffs"				)		//  DAL:566
	else REWRITE( "gamma"			)		//  DAL:566
	else REWRITE( "getpass"			)		//  DAL:566
	else REWRITE( "hypot"			)		//  DAL:566
	else REWRITE( "insque"			)		//  DAL:566
	else REWRITE( "isascii"			)		//  DAL:566
	else REWRITE( "j0"				)		//  DAL:566
	else REWRITE( "j1"				)		//  DAL:566
	else REWRITE( "jn"				)		//  DAL:566
	else REWRITE( "remque"			)		//  DAL:567
	else REWRITE( "y0"				)		//  DAL:567
	else REWRITE( "y1"				)		//  DAL:567
	else REWRITE( "yn"				)		//  DAL:567
	
	else DELETE( "endgrent"			)		//  DAL:566
	else DELETE( "endpwent"			)		//  DAL:566
	else DELETE( "nice"				)		//  DAL:566
	else DELETE( "setgrent"			)		//  DAL:567
	else DELETE( "setpwent"			)		//  DAL:567
	
	if ( CALLED("asctime") && sys_time_included && (! time_included) )		//  DAL:217
		{
		warn( INCLUDE, "POSIX requires #include <time.h> for function asctime." );
		++items_flagged;
		}
	}

if ( identifier("direct") )	//	MRH:328
	if ( sys_dir_included )
		{
		warn( REPLACE_TAG, "Replace tag \"direct\" with \"dirent\"." );
		++items_flagged;
		}

if ( macro("L_INCR") )		//  DAL:351
	{
	warn( REPLACE_CON, "Replace L_INCR with SEEK_CUR." );
	++items_flagged;
	}

if ( macro("L_SET") )		//  DAL:351
	{
	warn( REPLACE_CON, "Replace L_SET with SEEK_SET." );
	++items_flagged;
	}

if ( macro("L_XTND") )		//  DAL:351
	{
	warn( REPLACE_CON, "Replace L_XTND with SEEK_END." );
	++items_flagged;
	}

if ( macro("O_NDELAY") )	//  DAL:366
	{
	warn( REPLACE_CON, "Replace O_NDELAY with O_NONBLOCK." );
	++items_flagged;
	}

if ( macro("SIGIOT") )		//  DAL:211
	{
	warn( REPLACE_CON, "Replace SIGIOT with SIGABRT." );
	++items_flagged;
	}

if ( lex_num_escape == 7 )	//  DAL:377
	{
	warn( ALARM, "If you need an audible alarm, use \\a instead of \\07." );
	++items_flagged;
	}

if ( mod_end )
	{
	printf( "\n*** %d non-POSIX BSD features were found in module %s ***\n\n", items_flagged, mod_name() );
	}
