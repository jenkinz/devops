//////////////////////////////////////////////////
//   File itertype.cc
//   Rule tells the type of a for loop iterator, if there
//   is any.

int seeFor;
int semicolons;
int found;
int parenlevel;

if (keyword("for"))
{
   seeFor=1;
   found=0;
   semicolons=0;
}

if (op_open_paren)
{
    if (seeFor)
    {
        parenlevel++;
    }
}

if (op_close_paren)
{
   if (seeFor)
   {
       parenlevel--;
       if (parenlevel==0)
       {
           seeFor=0;
       }
   }
}

if (op_semicolon)
{
    if (seeFor)
    {
        semicolons++;
    }
}

/* for cases as for (int i=0;....) */
if (dcl_variable)
{
    if (seeFor)
    {
        if (semicolons==0)
        {
            warn(1001,"Iterator variable %s has type %s.",dcl_name(),dcl_base_name());
            found=1;
        }
    }
}


/* assume that if the iterator is used in for loop, 
  it is always the first id in an expression */
if (idn_variable)
{
    if (seeFor&&!found)
    {
        warn(1001,"Iterator variable %s has type %s.",idn_name(), idn_base_name());
        found=1;
    }
}
