//////////////////////////////////////////////////////////
// CopyRight (C) 1996, Abraxas Software, Inc.

///////////////////////////////////////////////////////
// This rule detect the default case in a switch statement
// contains only empty, break statement or there is nothing
// significant before break.

#include <check.cch>

int defcase;
int notemptydef;
int afterbreak;
int currlevel;

if ( keyword("default") )
{
    defcase = 1;
    currlevel = stm_depth;
    afterbreak = 0;
}

if (stm_end)
{
  switch (stm_kind)
  {
    case SWITCH :
      if (defcase)
      {
        if (!notemptydef)
        {
          warn(1000, "swithc statement has a useless default case");
          notemptydef = 0;
          defcase = 0;
        }
      }
      break;
    case COMPOUND :
      if (defcase)
      {
         if (stm_depth>currlevel)
         {
             notemptydef=1;
         }
      }
      break;
    case EMPTY :
      break;
    case BREAK:
      if (defcase)
      {
         afterbreak = 1;
      }
      break;
    default :
      if (defcase)
      {
         if (!afterbreak)
            notemptydef = 1;
      }
  }
}
