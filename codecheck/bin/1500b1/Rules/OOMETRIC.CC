/*	cpmetric.cc
	Copyright (c) 1992-93 by Abraxas Software. All rights reserved.
==============================================================================
	Purpose:	Calculates object-oriented programming percentages.
	Author:		Loren Cobb.
	Revision:	28 November 1992.
	Format:		Monospaced font with 4 spaces/tab.
==============================================================================

Abstract

	These CodeCheck rules create a table which shows the percentages
	of object-oriented programming techniques used within each module
	of a project.
	
	The indicators of object-oriented programming techniques used in
	this rule file are relatively primitive, but they are very effective
	when used to measure the extent to which a project was written with
	object-oriented techniques.
	
	The indicators used are:
	
	     percent of variables that are declared inside classes;
		 percent of functions that are class member functions;
		 percent of non-blank lines that are within class definitions;
		 percent of tokens that are found within class definitions.
	
	Variable counts ignore local (block scope) variables, typedef names, and
	enumerated constants. Line counts ignore blank lines and lines that were
	suppressed by the preprocessor.

APPEAL FOR FEEDBACK

	Please let us know your experiences with this rule file. What should
	be modified or added?  Has it proven useful or not?  What additional
	features would you like to see?  Your comments and suggestions will
	be gratefully received.  - Loren Cobb

==============================================================================
*/

#include <check.cch>

int		class_vars, global_vars, pct_class_vars,
		class_fcns, global_fcns, pct_class_fcns,
		class_lines,  non_class_lines,  pct_class_lines,
		class_tokens, non_class_tokens, pct_class_tokens;

if ( mod_begin )
	{
	set_option( 'F', 1 );	//  Include header lines in counts
	set_option( 'O', 1 );	//  Redirect stderr output to file stderr.out
	set_option( 'S', 3 );	//  Apply rules to all headers
	
	class_vars   = global_vars      = 0;
	class_fcns   = global_fcns      = 0;
	class_lines  = non_class_lines  = 0;
	class_tokens = non_class_tokens = 0;
	}

if ( lin_end )
	if ( ! (lin_suppressed || lin_is_white) )
		{
		if ( lin_within_class || (lin_within_tag > ENUM_TAG) )
			{
			class_lines  += 1;
			class_tokens += lin_tokens;
			}
		else
			{
			non_class_lines  += 1;
			non_class_tokens += lin_tokens;
			}
		}

if ( dcl_global || dcl_static )    //  Declarators with file scope.
	{
	if ( dcl_variable )
		++global_vars;
	else if ( dcl_function )
		++global_fcns;
	}

if ( dcl_member )    //  Declarators with class scope.
	{
	if ( dcl_variable )
		++class_vars;
	else if ( dcl_function )
		++class_fcns;
	}

if ( prj_begin )
	{
	printf( "\n\n   ----- Object-Oriented Statistics -----\n\n" );
	printf( "Percentages less than roughly 50%% indicate that\n" );
	printf( "object-oriented programming techniques were not used.\n\n" );
	printf( "Note 1: Line counts exclude blank and suppressed lines, and\n" );
	printf( "variable counts exclude local variables and typedef names.\n\n" );
	printf( "Note 2: All stderr output has been sent to the file stderr.out\n" );
	}

if ( mod_end )
	{
	printf( "\nModule %s:\n", mod_name() );
	printf( "                File       Class      Object\n" );
	printf( "               Scope       Scope     Percent\n" );

	pct_class_vars   = (100*class_vars  ) / (class_vars   + global_vars     );
	pct_class_fcns   = (100*class_fcns  ) / (class_fcns   + global_fcns     );
	pct_class_lines  = (100*class_lines ) / (class_lines  + non_class_lines );
	pct_class_tokens = (100*class_tokens) / (class_tokens + non_class_tokens);

	printf( "Variables %10d %11d %10d%%\n", global_vars,      class_vars,   pct_class_vars   );
	printf( "Functions %10d %11d %10d%%\n", global_fcns,      class_fcns,   pct_class_fcns   );
	printf( "Lines     %10d %11d %10d%%\n", non_class_lines,  class_lines,  pct_class_lines  );
	printf( "Tokens    %10d %11d %10d%%\n", non_class_tokens, class_tokens, pct_class_tokens );
	printf( "\n" );
	}

if ( prj_end )
	printf( "\n" );
