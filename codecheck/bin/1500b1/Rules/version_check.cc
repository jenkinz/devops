//////////////////////////////////////////////////////////
// CopyRight (C) 1995, Abraxas Software, Inc.

#include <check.cch>

int i;
int lin1,lin2;
int saw_include;
int first_dim;
char *str;

///////////////////////////////////////////////////////////////////////////
// RULE 1:  Every source file has the SCCS version control string inserted
//          as the first line in the file. The format o fthe SCCS what
//          string is as follows:
//          /* %W %P Copyright 1995, XXXX Inc. */
//          The date field ("1995") can be any four-digit number.
if (lin_end)
{
  if (lin_number==1)
  {
    if (!lin_is_comment)
    {
      warn(1,"The first line of source file must be version control.");
    }
    else
    {
      str=line();
      str=strstr(str,"%W% %P% Copyright ");
      if (!str)
      {
        warn(1,"The fisrt line of source file must be version control.");
      }
      else
      {
        str=str+strlen("%W% %P% Copyright ");
        i=0;
        while (i<4)
        {
          if (!isdigit(str[i]))
          {
            break;
          }
          else
          {
            i++;
          }
        }
        if (i!=4)
        {
          warn(1,"The first line of source file must be version control.");
        }
        else
        {
          str=str+4;
          if (str[0]!=',')
          {
            warn(1,"The first line of source file must be version control.");
          }
          else
          {
            if (strstr(str,"XXXX Inc.")==0)
            {
              warn(1,"The first line of source file must be version control.");
            }
          }
        }
      }
    }
  }
}
      
///////////////////////////////////////////////////////////////// 
// RULE 2: #include directive shall use the "<file>" syntax
if (pp_include!=3)
{
  warn(2,"Included header file name should be wrapped in angle brackets");
}

// RULE 3:  #include directive shall specify only the file name.
//          it should not specify an absolute or relative file name
if (pp_include)
{
  if (strstr(token(),"/"))
  {
    warn(3,"Only plain file name allowed in #include directive");
  }
}

////////////////////////////////////////////////////////////////
// RULE 4 : All C source files shall have #include <common.h>
//          as the first include file. Header files shall not include
//          this header file. 
if (mod_begin)
{
  saw_include=0;
}

if (pp_include)
{
  if (lin_source)
  {
    if (saw_include==0)
    {
      saw_include=1;
      if (strcmp(token(),"common.h")!=0)
      {
        warn(4,"The first include file in source file should be <common.h>.");
      }
    }
  }
  if (lin_header)
  {
    if (strcmp(token(),"common.h")==0)
    {
      warn(4,"Header file <common.h> can only be included in source file.");
    }
  }
}

//////////////////////////////////////////////////////////////////
// RULE  5: Within an enumerated list, the elements shall be written
//          in uppercase letters.
if (dcl_enum)
{
  i=strlen(dcl_name());
  while (i>0)
  {
    if (islower(dcl_name()[i-1]))
    {
      warn(5,"Enumerate constant %s should not have any lowercase letter",dcl_name());
      break;
    }
    i--;
  }
}

//////////////////////////////////////////////////////////////////
// RULE 6 : Automatic variables shall not be placed in registers.
if (dcl_variable)
{
  if (dcl_storage_flags==REGISTER_SC)
  {
    if (dcl_local)
    {
      warn(6,"Automatic variable shall not be placed in register.");
    }
  }
}

/////////////////////////////////////////////////////////////////
// RULE 7: All variables shall be explicitly defined.
if (dcl_initializer)
{
  warn(7,"Variable %s should be defined explicitly.",dcl_name());
}

if (idn_no_init)
{
  warn(7,"Variable %s should be defined explicitly.",idn_name());
}

/////////////////////////////////////////////////////////////////
// RULE 8 : The first dimension of an array shall not be provided in
//          function argument declarations.
if (dcl_parameter)
{
  if (dcl_levels>0)
  {
    i=0;
    first_dim=-1;
    while (i<=dcl_levels)
    {
      if (dcl_level(i)==ARRAY)
      {
        first_dim=i;
        break;
      }
      i++;
    }
    if (first_dim>=0)
    {
      if (dcl_array_dim(first_dim)!=-1)
      {
        warn(8,"The first dimension of function parameter %s should not be specified.",dcl_name());
      }
    }
  }
}

//////////////////////////////////////////////////////////////////
// RULE 9 : No spacing allowed between a function and the opening left
//          parenthesis.
if (op_open_funargs)
{
  if (op_white_before)
  {
    warn(9,"No spacing allowed between a function name and its opening parenthesis for function %s.",op_function());
  }
} 

/////////////////////////////////////////////////////////////////
// RULE 10: The open and close braces shall be in the column directly
//          below the statement which create the block.


/////////////////////////////////////////////////////////////////
// RULE 11: The C built-in data types, except void, shall not be used.
if (dcl_global||dcl_local)
{
  if (dcl_base>VOID_TYPE&&dcl_base<ENUM_TYPE)
  {
    warn(11,"C built-in data type %s shall not be used",dcl_base_name());
  }
}

/////////////////////////////////////////////////////////////////
// RULE 12: Function parameter lists should be limited to 6 paramters
if (dcl_parameter>6)
{
  warn(12,"Function declaration %s has more than 6 parameters",dcl_name());
}

if (op_call)
{
  if (op_operands>6)
  {
    warn(12,"Function call %s has more than 6 parameters.",op_function());
  }
}
