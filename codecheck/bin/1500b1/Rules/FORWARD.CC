
//  This rule file demonstrates CodeCheck forward chaining.
//  These two rules calculate 10 factorial (10! = 3,628,800).

//  Copyright (c) 1988-93 by Abraxas Software, Inc.

//  The last assignment in Rule #1 triggers Rule #2, and the
//  last step in Rule #2 recursively triggers Rule #2 until
//  z has been decremented to 1. Control then returns to
//  Rule #1 for the final step (printing the result).

int   x,     //  x will hold the starting value.
      y,     //  y will receive the factorial of x.
      z;     //  z is a counter.


if ( mod_end )  //  Rule #1: Begin the calculation.
    {
    x = 10;
    y = 1;
	printf( "Factorial step 0: y = %7d\n", y );

    z = x;      //  This assignment triggers Rule #2.

    printf( "\n%d factorial = %d\n\n", x, y );
    }


if ( z > 1 )    //  Rule #2: Perform the calculation.
    {
    y *= z;     //  Multiply y by the current value of z.
	
	printf( "Factorial step %d: y = %7d\n", 1+x-z, y );
	
    z -= 1;     //  Decrement z, recursively triggering Rule #2...
    }

