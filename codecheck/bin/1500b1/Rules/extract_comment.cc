// search for specific text string, extract all comments that follow and output

int rule;	// start of rule

if ( lin_end ) {

	if ( lin_is_comment ) {

		if ( strstr( line(), "Rule" ) ) rule = lin_number;	// mark begin of text "Rule"
	}

	if ( strstr(line(), "*/" ) ) rule = 0;	// turn it off

	if ( rule ) printf( "%s\n", line() );
}

if ( mod_begin ) printf( "\n/*\n", mod_name() );

if ( mod_end ) printf( "*/ \n\n" );