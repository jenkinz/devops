//  Copyright (c) 1990-93 by Abraxas Software.

//  This file demonstrates how to check for compliance
//  with rules for type-encoded identifier prefixes.

//  The prefix rules illustrated here are as follows:

//  Variable type          Prefix
//  --------------         ------
//    const pointer        cp
//    pointer              p
//    array                a
//    void                 v
//    char                 c
//    short                s
//    int                  i
//    long                 l
//    unsigned char        uc
//    unsigned short       us
//    unsigned int         ui
//    unsigned long        ul
//    float                f
//    double               d
//    long double          e
//    struct               x
//    union                w
//    enum                 n
//    Boolean              b

//  Example:  unsigned char aucBlatz[200];


#include <check.cch>

int		levels, k;

if ( (dcl_global || dcl_static || dcl_local) && ! dcl_parameter )
	{
	k = 0;
	while ( k <= dcl_levels )
		{
		switch ( dcl_level(k) )
			{
		case ARRAY:
			if ( ! prefix("a") )
				warn( 1001, "a prefix missing on array." );
			break;
		case POINTER:
			if ( dcl_level_flags(k) & CONST_FLAG )
				{
				if ( ! prefix("cp") )
					warn( 1002, "cp prefix missing on const pointer." );
				}
			else if ( ! prefix("p") )
				warn( 1003, "p prefix missing on pointer." );
			break;
			}
		k++;
		}
	switch ( dcl_base )
		{
	case VOID_TYPE:
		if ( ! prefix("v") )
			warn( 1010, "v prefix missing on void." );
		break;
	case CHAR_TYPE:
		if ( ! prefix("c") )
			warn( 1011, "c prefix missing on char." );
		break;
	case SHORT_TYPE:
		if ( ! prefix("s") )
			warn( 1012, "s prefix missing on short." );
		break;
	case INT_TYPE:
		if ( ! prefix("i") )
			warn( 1013, "i prefix missing on int." );
		break;
	case LONG_TYPE:
		if ( ! prefix("l") )
			warn( 1014, "l prefix missing on long." );
		break;
	case UCHAR_TYPE:
		if ( ! prefix("uc") )
			warn( 1015, "uc prefix missing on unsigned char." );
		break;
	case USHORT_TYPE:
		if ( ! prefix("us") )
			warn( 1016, "us prefix missing on unsigned short." );
		break;
	case UINT_TYPE:
		if ( ! prefix("ui") )
			warn( 1017, "ui prefix missing on unsigned int." );
		break;
	case ULONG_TYPE:
		if ( ! prefix("ul") )
			warn( 1018, "ul prefix missing on unsigned long." );
		break;
	case FLOAT_TYPE:
		if ( ! prefix("f") )
			warn( 1019, "f prefix missing on float." );
		break;
	case DOUBLE_TYPE:
		if ( ! prefix("d") )
			warn( 1020, "d prefix missing on double." );
		break;
	case LONG_DOUBLE_TYPE:
		if ( ! prefix("e") )
			warn( 1021, "e prefix missing on long double %s.", root() );
		break;
	case ENUM_TYPE:
		if ( ! prefix("n") )
			warn( 1022, "n prefix missing on enum." );
		break;
	case UNION_TYPE:
		if ( ! prefix("w") )
			warn( 1023, "w prefix missing on union." );
		break;
	case STRUCT_TYPE:
		if ( ! prefix("x") )
			warn( 1024, "x prefix missing on struct." );
		break;
	case DEFINED_TYPE:
		if ( strcmp(dcl_base_name(),"Boolean") == 0 )
			if ( ! prefix("b") )
				warn( 1025, "b prefix missing on Boolean." );
		}
	}
