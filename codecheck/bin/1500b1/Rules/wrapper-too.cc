/*
-------------------------------------------------------

    Yet another rule for checking header file wrapper.

    It is supposed that correct wrapper is the macro
    with the heade file name converted to all upper cases
    and prefixed and suffixed with double underscores.

    for example, for header.h

    the wrapper is

    #ifndef __HEADER__
    #define __HEADER__
    ...
    #endif

    if need to have different format of wrapper macros and
    need any help, please let me know.

----------------------------------------------------------
*/

int skip;
int i;
char *p;
char macroName[100];
int see_if;
int see_define;

if ( lin_end )
{
  if ( lin_header && !skip )
  {
    if ( lin_number == 1 )
    {
      skip = 0;
      see_if = 0;
      see_define = 0;
    }
    if ( lin_has_code )
    {
      if ( !see_define ) 
      {
        warn( 1001, "Header file %s needs wrapper.", file_name() );
        skip = 1;
      }
    }
    else
    {
      if ( lin_preprocessor )
      {
        p = strstr( line(), "#" );
        p = p + 1;
        while ( p[0] != 0 )
        {
          if ( p[0] != ' ' && p[0] != '\t' )
          {
            break;
          }
          p = p + 1;
        }
        if ( p[0] != 0 )
        {
          if ( strncmp( p, "if", 2 ) != 0 )
          {
            if ( !see_if )
            {
              warn( 1001, "Header file %s needs wrapper.", file_name() );
              skip = 1;
            }
            else
            {
              if ( strncmp( p, "define", 6 ) != 0 )
              {
                warn( 1002, "Wrapper for header file %s is not correct.", file_name() );
                skip = 1;
              }
              else
              {
                p = p + 6;
                while ( p[0] == ' ' || p[0] == '\t' )
                {
                  p = p + 1;
                }
                i = 0;
                while ( p[0] != ' ' && p[0] != '\t' && p[0] != 0 )
                {
                  macroName[i] = p[0];
                  p = p + 1;
                  i = i + 1;
                }
                macroName[i] = 0;
                if ( strncmp( macroName, "__", 2 ) != 0 )
                {
                  skip = 1;
                  warn( 1002, "Wrapper for header file %s is not correct.", file_name() );
                }
                else
                {
                  if ( strncmp( macroName + strlen( macroName ) - 2, "__", 2 ) != 0 )
                  {
                    warn( 1002, "Wrapper for header file %s is not correct.", file_name() );
                    skip = 1;
                  }
                  else
                  {
                    i = 0;
                    while ( file_name()[i] != '.' && file_name()[i] != 0 )
                    {
                      if ( toupper( file_name()[i] ) != macroName[i + 2] )
                      {
                        warn( 1002, "Wrapper for header file %s is not correct.", file_name() );
                        skip = 1;
                        break;
                      }          
                      i = i + 1;
                    }
                    if ( !skip )
                    {
                      if ( i != strlen( macroName ) - 4 )
                      {
                        warn( 1002, "Wrapper for header file %s is not correct.", file_name() );
                      }
                    }
                    skip = 1;
                  }
                }
              }
            }
          }
          else
          {
            if ( strncmp( p, "ifndef", 6 ) == 0 )
            {
              see_if = 1;
              p = p + 6;
              while ( p[0] == ' ' || p[0] == '\t' )
              {
                p = p + 1;
              }

              i = 0;
              while ( p[0] != ' ' && p[0] != '\t' && p[0] != 0 )
              {
                macroName[i] = p[0];
                i++;
                p = p + 1;
              } 
              macroName[i] = 0;
            }
            else
            {
              if ( strncmp( p, "ifdef", 5 ) != 0 )
              {
                see_if = 2;
                p = p + 2;
                while ( p[0] == ' ' || p[0] == '\t' )
                {
                  p = p + 1;
                }
                if ( p[0] != '!' )
                {
                  warn( 1002, "Wrapper for header file %s is not correct.", file_name() );
                  skip = 1;
                }
                else
                {
                  p = p + 1;
                  while ( p[0] == '(' || p[0] == ' ' || p[0] == '\t' )
                  {
                    p = p + 1;
                  }
                  if ( strncmp( p, "defined", 7 ) != 0 )
                  {
                    warn( 1002, "Wrapper for header file %s is not correct.", file_name() );
                    skip = 1;
                  }
                  else
                  {
                    p = p + 7;
                    while ( p[0] == ' ' || p[0] == '\t' || p[0] == '(' )
                    {
                      p = p + 1;
                    }
                    i =  0;
                    while ( p[0] != ' ' && p[0] != '\t' && p[0] != ')' )
                    {
                      macroName[i] = p[0];
                      p = p + 1;
                      i = i + 1;
                    }
                    macroName[i] = 0;
                    while ( p[0] != 0 )
                    {
                      if ( p[0] != ' ' && p[0] != '\t' && p[0] != ')' )
                      {
                        break;
                      }
                      p = p + 1;
                    }
                    if ( p[0] != 0 )
                    {
                      warn( 1002, "Wrapper for header file %s is not correct.", file_name() );
                      skip = 1;
                    }
                  }
                }
              } 
            }
            if ( !skip )
            {
              if ( strncmp( macroName, "__", 2 ) != 0 )
              {
                warn( 1002, "Wrapper for header file %s is not correct.", file_name() ); 
                skip = 1;
              }
              else
              {
                if ( strncmp( macroName + strlen( macroName ) - 2, "__", 2 ) != 0 )
                {
                  skip = 1;
                  warn( 1002, "Wrapper for header file %s is not correct.", file_name() );
                }
                else
                {
                  i = 0;
                  while ( file_name()[i] != '.' && file_name()[i] != 0 )
                  {
                    if ( toupper( file_name()[i] ) != macroName[i + 2] )
                    {
                      skip = 1;
                      warn( 1002, "Wrapper for header file %s is not correct.", file_name() );
                      break;
                    }
                    i = i + 1;
                  }
                  if ( !skip )
                  {
                    if ( i != strlen( macroName ) - 4 )
                    {
                      warn( 1002, "Wrapper for header file %s is not correct.", file_name() );
                      skip = 1;
                    }
                  }
                  if ( !skip )
                  {
                    if ( see_if == 2 )
                    {
                      warn( 1002, "It is better to use #ifndef for the wrapper." );
                    }
                  }
                }
              } 
            }
          }
        }
      }
    }
  }
}
