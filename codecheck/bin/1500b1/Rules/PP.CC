//	Copyright (c) 1988-93 by Abraxas Software.

//	This file provides an EXAMPLE for almost
//	every preprocessor variable in CodeCheck.


int		manifestCount;		//	Counts manifest constants

if ( pp_ansi )
	warn( 1000, "This usage is not portable to pre-ANSI compilers." );

if ( pp_arg_count > 5 )
	warn( 1001, "Macro has too many arguments for portability." );

if ( pp_arg_multiple )
	warn( 1002, "Possible undesired side-effects may occur here." );

if ( pp_arg_paren )
	warn( 1003, "Always enclose macro arguments in parentheses." );

if ( pp_arg_string )
	warn( 1004, "This formal parameter will not be expanded in ANSI C." );

if ( pp_arith )
	warn( 1005, "This directive requires arithmetic calculation." );

if ( pp_assign )
	warn( 1006, "The \"=\" sign in this macro may be an error." );

if ( pp_bad_white )
	warn( 1007, "This whitespace character is illegal in ANSI C." );

if ( pp_benign )
	warn( 1008, "Benign macro redefinition." );

if ( pp_comment )
	warn( 1009, "Separate comments from tokens with whitespace." );

if ( pp_const )
	warn( 1010, "This defines a manifest constant." );

if ( pp_defined )
	warn( 1011, "The ANSI \"defined\" function has been used here." );

if ( pp_depend )
	warn( 1012, "Another macro depends on this one!" );

if ( pp_elif )
	warn( 1013, "#elif used here." );

if ( pp_endif )
	warn( 1014, "#endif used here." );

if ( pp_empty_arglist )
	warn( 1015, "Zero-argument macros may not be portable." );
	
if ( pp_empty_body )
	warn( 1016, "Macro has no body - is this right?" );

if ( pp_error )
	warn( 1017, "The #error directive is not portable." );

if ( pp_if_depth > 5 )
	warn( 1018, "#if nesting exceeds 5" );

if ( pp_include & 1 )
	warn( 1019, "Macro substitution for header filenames is not portable." );

if ( pp_include_depth > 4 )
	warn( 1020, "Include-file depth may be too much for portability." );

if ( pp_include_white )
	warn( 1021, "The leading whitespace in this filename is not portable." );

if ( pp_keyword )
	warn( 1022, "Warning: this macro redefines a keyword." );

if ( pp_length > 512 )
	warn( 1023, "Macro definition length exceeds 512 characters." );

if ( pp_lowercase )
	warn( 1024, "Macro name is not all uppercase." );

if ( pp_macro > 64 )
	warn( 1025, "Macro name exceeds 64 characters." );

if ( pp_macro_conflict )
	warn( 1026, "Macro %s was defined differently in file %s, line %d.",
			pp_name(), conflict_file(), conflict_line );

if ( pp_macro_dup )
	warn( 1027, "Macro %s was also defined in file  %s, line %d.",
			pp_name(), conflict_file(), conflict_line );

if ( pp_not_ansi )
	warn( 1028, "This is not permitted in ANSI preprocessors." );

if ( pp_not_defined )
	warn( 1029, "This expression depends on an undefined identifier." );

if ( pp_overload )
	warn( 1030, "Identifier conflicts with a macro function name." ); 

if ( pp_paste )
	warn( 1031, "The ANSI paste operator (##) is not portable." );

if ( pp_pragma )
	warn( 1032, "Pragma directives are not portable." );

if ( pp_recursive )
	warn( 1033, "This macro definition is recursive." );

if ( pp_semicolon )
	warn( 1034, "Macro body ends with a semicolon." );

if ( pp_sizeof )
	warn( 1035, "ANSI does not permit \"sizeof\" here." );

if ( pp_stack )
	warn( 1036, "Multiple definition of a macro." );

if ( pp_stringize )
	warn( 1037, "The stringize (#) operator is not portable." );

if ( pp_sub_keyword )
	warn( 1038, "Preprocessor keyword substitution is not portable." );

if ( pp_trailer )
	warn( 1039, "Tokens at the end of this directive are not portable." );

if ( pp_undef )
	warn( 1040, "Use #undef as seldom as possible." );

if ( pp_unknown )
	warn( 1041, "Unknown preprocessor directive - NOT PORTABLE." );

if ( pp_unstack )
	warn( 1042, "Undefining multiply-defined macros is not portable." );

if ( pp_white_after )
	warn( 1043, "Whitespace after this # is not portable." );

if ( pp_white_before )
	warn( 1044, "Whitespace before this # is not portable." );


/*	These rules calculate constants-per-module:		*/

if ( mod_begin )
	manifestCount = 0;
	
if ( pp_const )
	manifestCount += 1;

if ( mod_end )
	{
	printf( "%s ", mod_name() );
	printf( "has %d manifest constants.\n\n", manifestCount );
	}
