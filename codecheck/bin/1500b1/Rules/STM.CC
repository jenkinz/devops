//////////////////////////////////////////////////////////
// CopyRight (C) 1995, Abraxas Software, Inc.
//////////////////////////////////////////////////////////
// This rule tells if a line has more than one statement.

#include <check.cch>
int stmNum;

if (stm_end)
{
  if (stm_kind>COMPOUND)
  {
    stmNum++;
  }
}

if (lin_end)
{
   if (stmNum>1)
   {
       warn(1000,"This line has more than one statements.");
   }
   stmNum=0;
}
	
