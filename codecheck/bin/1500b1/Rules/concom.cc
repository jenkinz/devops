/*
TRANS.CC
1.) If "SomeClass" already directly inherits from TrackedInstance<> then no transform is done.
*/

//
// The translation should add the template class TrackedInstance to all classes and structs that do not already 
// directly inherit from TrackedInstance (or if their name starts with TrackedInstance).  
//

#define MAXSIZE 128
#define LINSIZE 1024

char outname[MAXSIZE];
char linbuf[LINSIZE];
char comment[MAXSIZE];

FILE *ofp;

if ( lin_end ) {
//warn( 1, "LE %s [ %s ] %s", line(), comment, linbuf );

	 if ( strstr(line(),"//") ) {

		strcpy( comment, "/* " );
		strncat( comment, strrchr( line(), '/' )+1, MAXSIZE );	// skip "//"
		strcat( comment, " */" );

		strncpy( linbuf, line(), LINSIZE );
		strncpy ( strstr( linbuf, "//" ), comment, MAXSIZE );
		
		if ( strlen(comment) > 6 ) 
			fprintf( ofp, "%s\n", linbuf );
		else
			fprintf( ofp, "\n" );
	}
	else {
		fprintf( ofp, "%s\n", line() );
	}
}

// Output File Setup

if ( mod_begin  ) {
	strcpy( outname, "tmp\\" );
    strncat( outname, mod_name(),MAXSIZE );
	strcat( outname, ".tmp" );
	printf( "Translation Mapping of File %s \n", outname );


	ofp = fopen( outname, "w" );
    if ( ofp == 0 ) {
        fatal(-1,"Out File Open Failure" );
    }

//	fprintf( ofp, "// Translation Mapping of File %s \n", mod_name() );
}

if ( mod_end  ) {
    fclose( ofp );
}
