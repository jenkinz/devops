/*
===================================================================

    file name : cmpndbrc.cc

    this rule check if a opening brace of a compound statement
    has its own line.

    Copyright (C) 1996 by Abraxas Software, Inc.

===================================================================
*/

char *p;

if (stm_cp_begin)
{
    if (strequiv(prev_token(),")"))
    {
        if (lex_token!=1)
        {
            warn(1001,"Put the open brace on it own line.");
        }
        else
        {
            p = strstr(line(),"{")+1;
            while (p[0]!=0)
            {
                if (p[0]!=' '&&p[0]!='\t')
                {
                    if (p[0]=='/')
                    {
                        if (p[1]=='/')
                        {
                            break;
                        }
                        else
                        {
                            if (p[1]=='*')
                            {
                                p=p+2;
                                while (p[0]!=0)
                                {
                                    if (p[0]=='*')
                                    {
                                        if (p[1]=='/')
                                        {
                                            p=p+2;
                                            break;
                                        }
                                        else
                                        {
                                            p=p+1;
                                        }
                                    }
                                    else
                                    {
                                        p=p+1;
                                    }
                                }
                                if (p[0]==0)
                                {
                                    break;
                                }
                            }
                            else
                            {
                                warn(1001,"Put the open brace at its own line.");
                                break;
                            }            
                        }
                    }
                    else
                    {
                        warn(1001,"Put the open brace on its own line.");
                        break;
                    }
                }
                p=p+1;
            }
        }
    }
}
