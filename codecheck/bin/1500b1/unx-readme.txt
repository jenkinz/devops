How to install CodeCheck on UNIX systems : 11.10b3

*** Included MAKEFILE is for Sun-Sparc if your OS is NOT SUN then see makefile caveats in the commented 'makefile'.

[ cc *.c -lm -o check ] Will generally build codecheck on most systems.

1. Make a directory on your hard disk where you intend to put CodeCheck source code.

2. Copy file chkdemo.tar from distribute diskette into the created selected directory in last step, if you are installing demo version.
   Copy file codchk.tar from distribute diskette into the created selected directory in last step, if you are installing standard version.

3. Type command "tar xvfz chkdemo.tar" to unpack the archival file, if you are installing demo version.
   Type command "tar xvfz codchk.tar" to unpack the archival file, if you are installing standard version. ( codchk.tar may also be called ccsrcnnn.tar )

4. Type command "uncompress *" to restore all compressed files. [ or UNPACK ]

5. Type command "make" to compile the source files to get executable file. [ makefile ]

6. Move the executable to a default binary directory, this is optional. [ ./check ]

7. Following instructions stated in manual and technotes to set up correct environment for running CodeCheck.

How to use rule files with CodeCheck :

1. If there is no option -R specified in command line, CodeCheck will first look in the directories specified by environmental variable CCRULES and current directory to see if there is any file named "default.cco" in these directories. If there is no file "default.cco", the source file will be checked without any rule. If file "default.cco" is found in those directories, CodeCheck will load the rules in this file to be applied on the source file specified in command line if there is one. If CodeCheck finds any error such as incompatible file format, in "default.cco", "default.cco" is ignored and the checking proceeds as if there is no file "default.cco" existing.

2. If there is no extension ".cco" specified in file name in option -R, 
CodeCheck will try to locate the ".cco" file with same name in option -R from the directories specified in environmental variable CCRULES and current directory. If the ".cco" file can not be found, in demo version, program is terminated. In standard version, if the ".cco" file can not be opened, CodeCheck will try to locate the rule(.cc) file with same name as the one in option -R in the directories mentioned above, if the ".cc" file is found, CodeCheck will recompile that rule file. Even the ".cco" file exists, it could be in wrong format or out of date, in this case, CodeCheck will not use this ".cco" file, and it will search the ".cc" file to recompile it. If the ".cc" file can not be opened, program is terminated.

3. If extension ".cco" is specified explicitly in file name in option -R CodeCheck will try to locate the ".cco" file from directories specified by environmental variable CCRULES and current directory. If the ".cco" file can not be found, program is terminated.

4. In demo version, if extension ".cc" is specified explicitly in file name in option -R, program is terminated. Because the functionality of compiling rule file is disabled, a object file for the rules must be present if the rules in the ".cc" file is to be used on your source code. In standard version, CodeCheck will search the rule file specified in option -R in the directories specified by environmental variable CCRULES to compile it. If the ".cc" file can not be opened, program is terminated.

5. In demo version, only one option -R is allowed. In standard version, multiple options -R are allowed, however, extension ".cco" can not be specified in any option -R, because in case more than one rule files chosen, each specified rule file will be compiled automatically, and only one object file "temp.cco" is produced for all the specified rule files.

6. In demo version, two sets of precompiled rule files are provided due to the difference of word order on different architectures. Choose one which you think fits your system. If you get "### Invalid or out-of-date .cco file ###" on both sets of precompiled rule files. Please contact us with information about the type of your architecture and operating system.


