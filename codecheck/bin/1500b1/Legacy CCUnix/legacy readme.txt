Older versions of codecheck unix source are available for those desiring earlier builds.

Note, that Abraxas only 'supports' the current version in terms of updates. If you have any questions on use of older versions please feel free to ask support@abxsoft.com

Our preference is that you always use the latest version available, that said if it works - don't fix it. Many installations prefer to stay with solid build from an earlier time. If you don't see what you need please ask.
