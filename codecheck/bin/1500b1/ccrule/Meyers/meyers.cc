////////////////////////////////////////////////////////////
//
//  This file contains some rules which are implementation
//  of ways improving C++ programs listed in two books
//  written by Scott Meyers ( "Effective C++" and "More
//  Effective C++", both are published by Addison-Wesley. )
//  There are 50 items listed in "Effective C++" and 35
//  items in "More Effective C++.
//

#include <check.cch>

#define MAXIDNLEN     64     //  assume all identifiers have maximal length of 64
#define MAXLIST       1024   //  maximal length for a name list

#define INC( x )      ( x ) = ( x ) + 1
#define DEC( x )      ( x ) = ( x ) - 1

//  variables used frequently in rules
int i;    // variable used mostly as iteration counter
int j;    // other variable used mostly as iteration counter
int l;    // variable used mostly for string length
char* p;  // variable used as pointer to characters in a name list
char* q;

//
//  Items 1 through 50 are listing in book "Effective C++"
//

////////////////////////////////////////////////////////////
//
//  Item  1 : Use const and inline instead of #define.
//

//  pp_const identifies macro defining a const.

if ( pp_const )
{
    warn( 101, "Define %s as const instead of macro.", pp_name() );
}

//  pp_arg_count identifies function-like macro with argument(s).
//  for example, #define PLUS( x, y ) (x) + ( y )
//  pp_empty_arglist identifies function-like macro without argument.
//  for example, #define X_PLUS_Y() (x) + (y)

//  it is not recommended to invoke a rule with more than one
//  triggers unless they are absolutely exclusive to each other.
//  otherwise a rule could be invoked more than once at one place.
//  this could cause unexpected behavior. For example, rule
//  if ( dcl_variable && dcl_global )
//       dclCount++;
//
//  will be invoked twice for a single declaration of a global variable.
//  thus dclCount will be increased twice for a single declared variable.
//  be careful, in CodeCheck rules, operators && and || are evaluated
//  differently from the same operators in C/C++. in CodeCheck rules,
//  both parts of the condition will be evaluated even though the first
//  part has made the condition has value FALSE of TRUE for sure.
//  so it is recommended to write the rule as 
//  if ( dcl_variable )
//      if ( dcl_global )
//          dclCount++;
//
//  in this rule, pp_arg_count and pp_empty_arglist will not be triggered
//  same time, so there is no problem in this case.
if ( pp_arg_count || pp_empty_arglist )
{
    warn( 102, "Define %s as inline instead of macro.", pp_name() );
}

////////////////////////////////////////////////////////////
//  Item  2: Prefer iostream.h to stdio.h.
//

//  this rule simply tells you to use "iostream.h" when "stdio.h"
//  is found in #include.

//  value 4 means header is included in a pair of brackets( <> )
//  and the included file name is not derived from a macro
//  expansion
//  see pp_include in master.doc for details.

if ( pp_include == 4 )
{
    //  here strcmp is the name of a Codecheck built-in
    //  function which acts the same as the ANSI function
    //  with same name. however, it is independent of 
    //  system function library. please check file master.doc
    //  to see the description about this function and other
    //  functions about string manipulations.

    if ( !strcmp( header_name(), "stdio.h" ) )
    {
        warn( 201, "Prefer iostream.h to stdio.h" );
    }
}

////////////////////////////////////////////////////////////
//  Item  3 : Use new and delete instead of malloc and free.
//

//  each time as function is called, idn_function is fired
//  up, then it is decided if function "malloc" or "free"
//  are called by comparing the function name against
//  strings "malloc" and "free".

if ( idn_function )
{
    if ( !strcmp( idn_name(), "malloc" ) )
    {
        warn( 301, "Use new instead of malloc.");
    }
    if ( !strcmp( idn_name(), "free" ) )
    {
        warn( 302, "Use delete instead of free.");
    }
}

////////////////////////////////////////////////////////////
//  Item  4 : Prefer C++-style comments.
//

//  lex_c_comment is triggered by any comment included in
//  /* and */
if ( lex_c_comment )
{
    warn( 401, "Use C++-style comment instead of C style comment" );
}

////////////////////////////////////////////////////////////
//  Item  5 : Use the same form in corresponding calls to
//            new and delete
//

//  Assume that all calls to new and delete are in the forms
//  1. ptr = new type;
//     delete ptr;
//  or 
//  2. ptr = new type[ count ];
//     delete [] ptr;

#define DELETE_WITHOUT_BRACKETS   1
#define DELETE_WITH_BRACKETS     2

//  currently, the arrays can be used in rules are still very
//  limited, only one dimensional, fixed size and of type char.
//  the way to store multiple names in an array is to put them
//  in sequence separated by character '\0'.
char idnName[MAXIDNLEN];   // name of a pointer
char newList1[MAXLIST];    // names of pointer in form 1
char newList2[MAXLIST];    // names of pointer in form 2
int inNew;    //  TRUE if operator new is used
int inDelete;    //  1 if operator delete is used, 2 if operator delete[] used
int found;    //  TRUE if a name has been found in a name list

if ( idn_variable )
{
    // idn_name() returns identifier's name
    strcpy( idnName, idn_name() );    //  copy identifier's name to idnName

    if ( inDelete )
    {
        if ( inDelete == DELETE_WITH_BRACKETS )    //  in form delete [] ptr;
        {
            p = newList2;    //  search the name registered as form 2
        }
        else    //  in form delete ptr;
        {
            p = newList1;    //  search the name registered as form 1
        }

        j = 0;
        found = FALSE;
        while ( p[0] != 0 )    //  p[0] == 0 means no more names in list
        {
            if ( strcmp( p, idnName ) == 0 )    //  looking for the name in list
            {
                i = 0;
                found = TRUE;    //  the name is found in name list

                //  name found, remove it from the list
                while ( j < MAXLIST - ( strlen( idnName ) + 1 ) )
                {
                    p[i] = p[i + strlen( idnName ) + 1];
                    INC( i );
                    INC( j );
                }
                break;
            }
            else    //  not found, keep looking
            {
                p = p + strlen( p ) + 1;
                j = j + strlen( p ) + 1;
            }
        }

        //  name was not found in list with matching form of new.
        //  try the list for the other form of new. 
        if ( p[0] == 0 && !found )
        {
            if ( inDelete == DELETE_WITH_BRACKETS )    //  in form delete [] ptr, try list for new type
            {
                p = newList1;
            }
            else    //  in form delete ptr, try list for new type[]
            {
                p = newList2;
            }

            found = FALSE;
            j = 0;
            while ( p[0] != 0 )
            {
                if ( strcmp( p, idnName ) == 0 )
                {
                    found = TRUE;

                    if ( inDelete == DELETE_WITHOUT_BRACKETS )
                    {
                        warn( 501 ,"Use form delete [] %s.", idnName );
                    }
                    else
                    {
                        warn( 502, "Use form delete %s.", idnName );
                    }

                    i =0;

                    //  remove the found name from the other list
                    while ( j < MAXLIST - ( strlen( idnName ) + 1 ) )
                    {
                        p[i] = p[i + strlen( idnName ) + 1];
                        INC( i );
                        INC( j );
                    }
                    break;
                }
                else
                {
                    p = p + strlen( p ) + 1;
                    j = j + strlen( p ) + 1;
                }
            }

            //  the name to be deleted not found in either list
            if ( p[0] == 0 && !found )
            {
                warn(503, "%s has not been allocated with new yet.", idnName );
            }
        }
    }
}

//  found new operator
if ( op_new )
{
    inNew = TRUE;
}

//  found delete operator
if ( op_delete )
{
    inDelete = DELETE_WITHOUT_BRACKETS;
}

//  assume that new is used in a single assignment statement
//  which ends with a semicolon.
if ( op_semicolon )
{
    if ( inNew )
    {
        if ( strcmp( prev_token(), "]" ) == 0 )    //  in form new type[]
        {
            p = newList2;
        }
        else    //  in form new type
        {
            p = newList1;
        }

        found = FALSE;
        while ( p[0] != 0 )
        {
            //  the name has been in list.
            if ( strcmp( p, idnName ) == 0 )
            {
                warn( 505, "Delete %s before it is reallocated.", idnName );
                found = TRUE;
                break;
            }
            p = p + strlen( p ) + 1;
        }
        if ( !found )
        {
            i = 0;
            l = strlen( idnName );
            while ( i <= l )
            {
                p[i] = idnName[i];
                INC( i );
            }
        }
        inNew = FALSE;
    }
    if ( inDelete )
    {
        inDelete = 0;
    }
}

//  at the end of a module, clear both lists to all 0. before
//  cleaning, see if there is any pointer name which has been
//  allocated a piece of memory, but the memory is never returned.
if ( mod_end )
{
    //  clean up the lists
    p = newList1;
    while ( p[0] != 0 )
    {
        warn( 504, "%s was allocated with new, but never deleted in %s.", p, file_name() );
        i = 0;
        l = strlen( p );
        while ( i <= l )
        {
            p[0] = 0;
            INC( p );
            INC( i );
        }
    }
    p = newList2;
    while ( p[0] != 0 )
    {
        warn( 504, "%s was allocated with new, but never deleted in %s.", p, file_name() );
        i = 0;
        l = strlen( p );
        while ( i <= strlen( p ) )
        {
            p[0] = 0;
            INC( p );
            INC( i );
        }
    }
}

//  op_open_brackets combined with inDelete and the value of
//  the previous token can decide if in form delete[] ptr.
if ( op_open_bracket )
{
    if ( inDelete && strcmp( prev_token(), "delete" ) == 0 )
    {
        inDelete = DELETE_WITH_BRACKETS;   //  it is in form delete [] ptr
    }
} 

////////////////////////////////////////////////////////////
//  Item  6 : Call delete on pointer members in destructors.
//

//  assume that a destructor function is defined in class
//  definition.

int inClass;    //  TRUE when in a class body
int inDtor;    // TRUE when in a destructor definition
int ptrMember;    //  the number of data members which are pointers
int delPtrMem;    //  those members which are explicitly deleted in destructor


//  tag_begin marks the beginning of a tag starting from
//  open brace.
if ( tag_begin )
{
    if ( tag_kind > ENUM_TAG )    //  enum is not a class
    {
        inClass = TRUE;
    }
}

if ( dcl_variable )
{
    if ( dcl_member )
    {
        if ( inClass )
        {
            if ( dcl_levels >= 1 )    // for a pointer, dcl_levels has value at least 1
            {
                i = 0;
                while ( i <= dcl_levels )
                {
                    if ( dcl_level( i ) == POINTER )    //  this is a pointer
                    {
                        INC( ptrMember );
                        break;
                    }
                    INC( i );
                }
            }
        }
    }
}

if ( dcl_function )
{
    if ( dcl_member )
    {
        if ( dcl_definition )    //  this is a defintion of the destructor
        {
            if ( inClass )
            {
                if ( dcl_base == DESTRUCTOR_TYPE )
                {
                    inDtor = TRUE;
                }
            }
        }
    }
}

if ( idn_variable )
{
    if ( idn_member && inDelete )    //  the data member is explicitly deleted in destructor
    {
        if ( inDtor )
        {
            INC( delPtrMem );
        }
    }
}

//  at the end of a function, inDtor needs to be reset
if ( fcn_end )
{
    if ( inDtor )
    {
        inDtor = FALSE;
    }
}

//  at the end of a tag
if ( tag_end )
{
    if ( tag_kind > ENUM_TAG )    //  it has to be a class
    {
        if ( delPtrMem < ptrMember )
        {
            warn( 601, "There are %d pointer members need to be deleted in destructor.", ptrMember-delPtrMem );
        }
        delPtrMem = 0;
        inClass = FALSE;
    }
}        
            
////////////////////////////////////////////////////////////
//  Item  7 : Check the return value of new.
//

////////////////////////////////////////////////////////////
//  Item  8 : Adhere to convention when writing new.
//

int seeOperator;    //  TRUE if keyword operator is found
int setNewHandler;    //  TRUE if function call to set_new_handler is found
int inNewOperator;    //  TRUE if in definition of operator new

//  without this flag, it could be confusing when looking at
//  value of return value of dcl_name() in next rule. because
//  a function named "operator_new" has the same name as 
//  "operator new".
//  for operators with name composed of only punctual characters
//  such as "operator +", the function return value "operator+",
//  if operator name is "new" or "delete", a underscore is inserted
//  in between.

if ( keyword( "operator" ) )
{
    seeOperator = TRUE;
}

if ( dcl_function )
{
    if ( dcl_definition && seeOperator )
    {
        if ( strcmp( dcl_name(), "operator_new" ) == 0 )
        {
            //  dcl_levels == 2 means the return value is not a simple type
            //  dcl_level( 1 ) == POINTER means it is a pointer.
            if ( dcl_levels != 2 || ( dcl_levels == 2 && dcl_level( 1 ) != POINTER ) )
            {
                warn( 802, "Operator new should return a pointer." );
            }
            inNewOperator = TRUE;
        }
    }
    seeOperator = FALSE;
}

//  at the end of a function definition, the variables
//  specifically for function need to be reset.
if ( fcn_end )
{
    if ( inNewOperator )
    {
        if ( !setNewHandler )
        {
            warn( 801, "Need to call function set_new_handler" );
        }
    }
    inNewOperator = FALSE;
    setNewHandler = FALSE;
}

//  in the definition of a operator new, function set_new_handler
//  needed to called at least once.
if ( idn_function )
{
    if ( inNewOperator )
    {
        if ( strcmp( idn_name(), "set_new_handler" ) == 0 )
        {
            setNewHandler = TRUE;
        }
    }
}

////////////////////////////////////////////////////////////
//  Item  9 : Avoid hiding the global new.
//

////////////////////////////////////////////////////////////
//  Item 10 : Write delete if you write new.
//

int definedNew;    //  TRUE if a class has a user defined new operator
int definedDelete;    //  TRUE if a class has a user defined delete operator

//  we have seen some other rules which are also triggered
//  by tag_begin. CodeCheck will concatenate all actions into
//  a list. the only thing you need to be careful is that do
//  not have conflicts in these actions, such as setting a 
//  flag here and resetting it there.
if ( tag_begin )
{
    definedNew = FALSE;
    definedDelete = FALSE;
}


if ( tag_end )
{
    if ( definedNew )
    {
        if ( !definedDelete )
        {
            warn( 1001, "Also write delete for class %s.", tag_name() );
        }
    }
}

if ( dcl_member && dcl_function )
{
    if ( !strcmp( dcl_name(), "operator_new" ) )
    {
        definedNew = TRUE;
    }
    if (!strcmp( dcl_name(), "operator_delete" ) )
    {
        definedDelete = TRUE;
    }
}

////////////////////////////////////////////////////////////
//  Item 11 : Define a copy constructor and an assignment
//            operator for classes with dynamically
//            allocated memory.
//

if ( tag_end )
{
    if ( ptrMember )    //  ptrMember has been set at previous rule
    {
        if ( !tag_has_copy )
        {
            warn( 1101, "Class %s needs an explicit copy constructor.", tag_name() );
        }
        if ( !tag_has_assign )
        {
            warn( 1102, "Class %s needs an explicit assignment operator.", tag_name() );
        }
    }
    ptrMember = 0;    //  ptrMember has been cleared somewhere else, however this repetition is harmless
}

////////////////////////////////////////////////////////////
//  Item 12 : Prefer initialization to assignment in
//            constructors.
//


////////////////////////////////////////////////////////////
//  Item 13 : List members in an initialization list in the
//            order in which they are declared.
//

int parenLevel;    //  count the level of parentheses
int inCtorWithInit;    //  TRUE if we are in the constructor def with init
int seq;    //  the sequence number in constructor init list
int memCount;    //  total number of non-static data members
char memList[MAXLIST];    //  the list stores names of non-static data members

if ( tag_begin )
{
     if ( tag_kind > ENUM_TAG )
     {
         p = memList;
         i = 0;
         while ( i < MAXLIST )
         {
             memList[i] = 0; 
             INC( i );
         }
         i = 0;
         memCount = 0;
     }
}

//  record all data members except static data members
if ( dcl_variable )
{
    //  though inClass tells that we in a class body now, we still
    //  need use dcl_member to screen out local variables declared
    //  in functions which are also defined within class body
    if ( dcl_member )
    {
        //  static member should no be initialized in this way, count it out
        if ( !dcl_static )
        {
            j = 0;
            l = strlen( dcl_name() );
            while ( j <  l )
            {
                memList[i + j] = dcl_name()[j];
                INC( j );
            }
            memList[i + j] = 0;
            i = i + j + 1;
            memCount++;    //  also count the data member we have recorded
        }
    }
}

//  for constructor defined with initializer, dcl_function is
//  invoked while parsing pointing to token ":"
if ( dcl_function )
{
    if ( dcl_definition )
    {
        if ( inClass )
        {
            if ( strcmp( token(), ":" ) == 0 )
            {
                inCtorWithInit = TRUE;
                seq = 0;
            }
            else  //  there is no init list at all in constructor definition  
            {
                if ( memCount > 0 )
                {
                    warn( 1201, "Prefer initialization to assignment in constructor." );
                }
            }
        }
    }
}

//  the best way to get the name in the initializer list is
//  when the outmost level open parenthesis is found, the
//  token before '(' is the name.
if ( op_open_paren )
{
    if ( inCtorWithInit )
    {
        INC( parenLevel );
        //  only the outmost layer of parentheses can decide
        //  the name in initializer list.
        if ( parenLevel == 1 )
        {
            strcpy( idnName, prev_token() );
            i = 0;
            p = memList; 
            while ( i < seq )    //  positioning to correct names in memList
            {
                p = p + strlen( p ) + 1;
                INC( i );
            }
            INC( seq );
            
            //  the initializer for the variable is not the same as that
            //  in declaration order.
            if ( strcmp( p, idnName ) != 0 )
            {
                warn( 1301, "The position of %s in init sequence is not right.", idnName );
            }
        }
    }
}

//  decrese the level of parentheses
if ( op_close_paren )
{
    if ( inCtorWithInit )
    {
        parenLevel--;
    }
}

if ( tag_end )
{
    i = 0;
}

if ( fcn_end )
{
    if ( inCtorWithInit )
    {
        inCtorWithInit = FALSE;
        if ( seq != memCount )    //  not all data member has been listed in the init list
        {
            warn( 1201, "Prefer initialization to assignment in constructor." );
        } 
    }
}

////////////////////////////////////////////////////////////
//  Item 14 : Make destructors virtual in base class.
//

int isBaseClass;    //  TRUE if we are in a base class definition
int inBase;    //  TRUE if we are in the base class declaration
int inClassHead;    //  TRUE if we are in the class header
int seeTemplate;    //  TRUE if keyword "template" is found
int inTempSpec;    //  TRUE if in template actual parameter list
int inTempHeader;    //  TRUE if in template formal parameter list
int angleLevel;    //  count the level of angles <>
char tagList[MAXLIST];    //  name list for classes with base classes

//  this rule will be invoked for any member function declared as virtual
if ( dcl_virtual )
{
    if ( dcl_base == DESTRUCTOR_TYPE )    //  see if this a destructor declaration
    {
        //  if a class has a virtual destructor, keep its name in list
        if ( tagList[0] == 0 )    //  there is no name in list yet
        {
            i = 0;
            while ( i <= strlen( tag_name() ) )
            {
                tagList[i] = tag_name()[i];
                INC( i );
            }
        }
        else    //  put this class name at the end of the list
        {
            p = tagList;
            while ( 1 )
            {
                if ( p[0] == 0 )
                {
                    i = 0;
                    while ( i <= strlen( tag_name() ) )
                    {
                        p[i] = tag_name()[i];
                        INC( i );
                    }
                    break;
                }
                else
                {
                    p = p + strlen( p ) + 1;
                }
            }
        }
    }
}

//  keyword "template" seen, that mean we are entering template header
if ( keyword( "template" ) )
{
    seeTemplate = TRUE;
}

//  keyword "class" means the possible beginning of a class definition
//  except the ones in template's actual or formal parameter list
if ( keyword( "class" ) )
{
    if ( !inTempSpec && !inTempHeader )
    {
        inClassHead = TRUE;
    }
}

//  op_colon_1 means the colon starting the base class list
//  the colon in operator ?: is op_colon_2
if ( op_colon_1 )
{
    if ( inClassHead )
    {
        inBase = TRUE;
    }
}

//  comma ',' acts as separator here in both base class list
//  and actual parameter list of a template
if ( op_separator )
{
    if ( inBase && !inTempSpec )
    {
        //  the template class used as base class has
        //  processed at the open angle bracket
        //  for example,
        //  class A : public T1<int>, public C { };
        if ( strcmp( prev_token(), ">" ) != 0 )
        {
            isBaseClass = TRUE;
        }
    }
}

//  the open angle brackets can appear at following locations,
//  1.  right after keyword "template"
//  2.  right after a template name which is not actual parameter
//      of another template
//  3.  right after a template name which is also an actual
//      parameter of another template
if ( op_open_angle )
{
    INC( angleLevel );    //  always increase the angle level
    if ( seeTemplate )
    {
        //  the open angle bracket in first kind of location
        inTempHeader = TRUE;
        seeTemplate = FALSE;
    }
    else
    {
        if ( angleLevel == 1 )    //  the open angle in second kind of location
        {
            inTempSpec = TRUE;
            //  only when the template class is at outmost level, it is a base class
            if ( inBase )
            {
                isBaseClass = TRUE;
            }
        }
    }
}

//  when a close angle bracket seen, always decrease the
//  level value. and reset the flags.
if ( op_close_angle )
{
    DEC( angleLevel );
    if ( angleLevel == 0 )
    {
        if ( inTempHeader )
        {
            inTempHeader = FALSE;
        }
        else
        {
            if ( inTempSpec )
            {
                inTempSpec = FALSE;
            }
        }
    }
}

//  this rule is invoked only when the value of variable
//  isBaseClass has been changed to 0 non-zero value. even
//  it is just assign to same value with an explicit
//  assignment in rules.
if ( isBaseClass )
{
    //  now we are looking in the class name list
    //  to see if this base class has defined virtual
    //  destructor, if its name is not in the list
    //  the warning message will be given.
    p = tagList;
    while ( p[0] != 0 )
    {
        if (strcmp( p, prev_token() ) == 0 )
        {
            break;
        }
        else
        {
            p = p + strlen( p ) + 1;
        }
    }
    if ( p[0] == 0 )
    {
        warn( 1401, "The destructor of class %s should be declared as virtual.", tag_name() );
    }
    isBaseClass = FALSE;
}

if ( tag_begin )
{
    //  the last base class in the list
    if ( inBase )
    {
        if ( strcmp( prev_token(), ">" ) != 0 )
        {
            isBaseClass = TRUE;
        }
    }
    inBase = FALSE;
    inClassHead = FALSE;
}

////////////////////////////////////////////////////////////
//  Item 15 : Have operator= return a reference to *this.
//

int inOpAssign;    //  TRUE if we are in function body for operator =
int rightForm;    //  TRUE if the return *this found
int inReturn;    //  TRUE if in return statement which is in operator =

if ( dcl_member )
{
    if ( dcl_function )
    {
        if ( dcl_definition )
        {
            if ( strcmp( dcl_name(), "operator=" ) == 0 )
            {
                inOpAssign = TRUE;    //  now we are in definition of operator =
            }
        }
    }
}

if ( op_return )    //  keyword return found
{
    if ( inOpAssign )
    {
        inReturn = TRUE;    //  now, we are in a returning statement
    }
}

//  we need to see return *this;
if ( keyword( "this" ) )
{
    if ( inReturn )
    {
        if ( strcmp( prev_token(), "*" ) == 0 )    //  we need to have * before this
        {
            rightForm =TRUE ;
        }
    }
}

if ( stm_end )
{
    if ( stm_kind == RETURN )
    {
        if ( !rightForm )
        {
            warn( 1501, "Return a reference to *this." );
        }
    }
    rightForm = FALSE;
    inReturn = FALSE;
}

if ( fcn_end )
{
    inOpAssign = FALSE;
}

////////////////////////////////////////////////////////////
//  Item 16 : Assign to all data members in operator=.
//

////////////////////////////////////////////////////////////
//  Item 17 : Check for assignment to self in operator=.
//

char paramName[MAXIDNLEN];    //  string for parameter name
int inIfCond;    //  in an if condition
int ifParenLevel;    //  parentheses level in if condition
int seeThis;    //  keyword "this" seen in if condition
int seeSelf;    //  the parameter of the function seen in if condition
int compSelf;    //  the comparison for this and parameter seen in if condition

//  copying the parameter name into paramName
if ( dcl_parameter )
{
    strcpy( paramName, dcl_name() );
}

//  we need to looking into the if condition
if ( op_if )
{
    if ( inOpAssign )
    {
        inIfCond = TRUE;
    }
}

//  increase the parentheses level in if condition
if ( op_open_paren )
{
    if ( inIfCond )
    {
        INC( ifParenLevel );
    }
}

//  decrease the parentheses level in if condition
if ( op_close_paren )
{
    if ( inIfCond )
    {
        DEC( ifParenLevel );
        if ( ifParenLevel == 0 )
        {
            inIfCond = FALSE;
            seeSelf = FALSE;
            seeThis = FALSE;
        }
    }
}

//  see if keyword "this" is in if condition
if ( keyword( "this" ) )
{
    if ( inIfCond )
    {
        seeThis = TRUE;
    }
}

//  see if the name of parameter is in if condition
if ( idn_variable )
{
    if ( inIfCond )
    {
        if ( strcmp( idn_name(), paramName ) == 0 )
        {
            seeSelf = TRUE;
        }
    }
}

//  once equal operator, check if both this and the 
//  parameter name appear in the comparison
if ( op_equal )
{
    if ( inIfCond )
    {
        if ( seeThis && seeSelf )
        {
            compSelf = TRUE;
        }
    }
}

if ( fcn_end )
{
    if ( inOpAssign )
    {
        //  there is no comparison between the parameter
        //  and itself, give the warning
        if ( !compSelf )
        {
            warn( 1701, "Need comparison to it self in operator=." );
        }
    }
    compSelf = FALSE;
}

////////////////////////////////////////////////////////////
//  Item 18 : Strive for class interfaces that are complete
//            and minimal.
//

////////////////////////////////////////////////////////////
//  Item 19 : Differentiate among member functions, global
//            functions, and friend functions.
//

////////////////////////////////////////////////////////////
//  Item 20 : Avoid data members in public interface.
//

if ( dcl_member )
{
    if ( dcl_variable )
    {
        if ( dcl_access == PUBLIC_ACCESS )
        {
            warn( 2001, "Data member %s should not be in public interface.", dcl_name() );
        }
    }
}

////////////////////////////////////////////////////////////
//  Item 21 : Use const whenever possible.
//

////////////////////////////////////////////////////////////
//  Item 22 : Pass and return objects by reference instead
//            of by value.
//

if ( dcl_function )
{
    //  for function returning dcl_level has value at least 1,
    //  0 for simple value type, 1 for function return itself.
    //  see details of dcl_level in master.doc.

    //  assume only class objects are subject to the rule, because
    //  copying a simple variable such as an integer is not expensive
    //  than passing the address.
    if ( dcl_levels == 1 && dcl_base <= CLASS_TYPE && dcl_base >= UNION_TYPE )
    {
        warn( 2201, "Return objects by reference instead of by value." );
    }
}

if ( dcl_parameter )
{
    if ( dcl_levels == 0 && dcl_base <= CLASS_TYPE && dcl_base >= UNION_TYPE )
    {
        warn( 2202, "Pass objects by reference instead of by value." );
    }
}

////////////////////////////////////////////////////////////
//  Item 23 : Don't try to return a reference when you must
//            return an object.
//

////////////////////////////////////////////////////////////
//  Item 24 : Choose carefully between function overloading
//            and parameter defaulting.
//

////////////////////////////////////////////////////////////
//  Item 25 : Avoid overloading on a pointer and a numerical
//            type.
//
            
////////////////////////////////////////////////////////////
//  Item 26 : Guard against potential ambiguity.
//

////////////////////////////////////////////////////////////
//  Item 27 : Explicitly disallow use of implicitly 
//            generated member functions you don't want.
//

////////////////////////////////////////////////////////////
//  Item 28 : Use structs to partition the global namespace.
//

////////////////////////////////////////////////////////////
//  Item 29 : Avoid returning "handles" to internal data
//            from const member functions.
//

////////////////////////////////////////////////////////////
//  Item 30 : Avoid member functions that return pointers or 
//            references to members less accessible than
//            themselves.
//

////////////////////////////////////////////////////////////
//  Item 31 : Never return a reference to a local object or
//            a deferenced pointer initialized by new within 
//            the function.
//

int fcnNeedAttn;    //  TRUE the function returns a reference to an object
int inRtrn;    //  TRUE if the return is in the specific functions.

if ( dcl_function )
{
    if ( dcl_definition )
    {
        //  we re looking functions which is declared as
        //  type &func()
        //  here dcl_level(0) is for function and dcl_level(1)
        //  is for reference.
        if ( dcl_levels == 2 && dcl_level( 0 ) == FUNCTION && dcl_level ( 1 ) == REFERENCE )
        {

            fcnNeedAttn = TRUE;    //  this is function we need to pay attentions 
        }
    }
}

//  we have seen op_return used as rule trigger before this,
//  however, the restrictions are different, so we use the
//  other name for marking being in return statement.
if ( op_return )
{
    if ( fcnNeedAttn )
    {
        inRtrn = TRUE;
    }
}

//  this rule check if the local variable is returned
//  for the kind functions we are paying attention.
if ( idn_variable )
{
    if ( inRtrn )    //  screen out the return statements not in these functions
    {
        if ( idn_local )   //  this rule only cares about local variables
        {
            if ( idn_levels == 0 )
            {
                warn( 3101, "Do not return a reference to local object." );
            }
        }
    }
}

//  at the end of a statement, clear inRtrn to FALSE
if ( stm_end )
{
    inRtrn = FALSE;
}

////////////////////////////////////////////////////////////
//  Item 32 : Use enum for integral class constants.
//

if ( lex_constant == CONST_INTEGER )
{
    if ( lin_within_class )
    {
        warn( 3201, "Use enum for integral class constant." );
    }
}

////////////////////////////////////////////////////////////
//  Item 33 : Use inlining judiciously.
//

////////////////////////////////////////////////////////////
//  Item 34 : Minimize compilation dependencies between
//            files.
//

////////////////////////////////////////////////////////////
//  Item 35 : Make sure public inheritance models "isa".
//

////////////////////////////////////////////////////////////
//  Item 36 : Differentiate between inheritance of interface
//            and inheritance of implementation.
//

////////////////////////////////////////////////////////////
//  Item 37 : Never redefine an inherited nonvirtual
//            function.
//

////////////////////////////////////////////////////////////
//  Item 38 : Never redefine an inherited default parameter
//            value.

////////////////////////////////////////////////////////////
//  Item 39 : Avoid cast down the inheritance hierarchy.
//

////////////////////////////////////////////////////////////
//  Item 40 : Model "has-a" or "is-implemented-in-term-of"
//            through layering.
//

////////////////////////////////////////////////////////////
//  Item 41 : Use private inheritance judiciously.
//

////////////////////////////////////////////////////////////
//  Item 42 : Differentiate between inheritance and
//            templates.

////////////////////////////////////////////////////////////
//  Item 43 : Use multiple inheritance judiciously.
//

////////////////////////////////////////////////////////////
//  Item 44 : Say what you mean; understand what you're
//            saying.
//

////////////////////////////////////////////////////////////
//  Item 45 : Know what functions C++ silently writes and
//            calls
//

////////////////////////////////////////////////////////////
//  Item 46 : Prefer compile-time and link-time errors to
//            run time errors.
//

////////////////////////////////////////////////////////////
//  Item 47 : Ensure that global objects are initialized
//            before they are used.
//

//  an object is initialized either as
//  type_name x = init;
//  or
//  class_name x( init );
//
//  in either cases, op_init will triggered before dcl_global
//  is triggered.

int seeInitOp;    // TRUE if is a initialization operator has been found.
                  
if ( dcl_global )
{
    //  dcl_definition excludes the variable declared with explicit extern.
    if ( dcl_variable && dcl_definition )
    {
        if ( !seeInitOp )
        {
            warn( 4701, "%s needs to be initialized.", dcl_name() );
        }
        seeInitOp = FALSE;    // reset seeInitOp for next declarator
    }
}

//  most triggers are cleared right after parsing moving on. so it is
//  necessary to set the value seeInitOp to TRUE to remember this
//  initializing operator, otherwise, when dcl_global is triggered,
//  op_init has been reset to 0 again.
if ( op_init )
{
    seeInitOp = TRUE;    // set seeInitOp for current declarator
}

////////////////////////////////////////////////////////////
//  Item 48 : Pay attention to compiler warnings.
//

////////////////////////////////////////////////////////////
//  Item 49 : Plan for coming language features.
//

////////////////////////////////////////////////////////////
//  Item 50 : Read ARM.
//

//
//    Item 51 to item 85 are listed in book "More Effective
//    C++" as item 1 to item 35.
//

////////////////////////////////////////////////////////////
//  Item  51 : Distinguish between pointers and references.
//

////////////////////////////////////////////////////////////
//  Item  52 : Prefer C++-style casts.
//

if ( op_cast )
{
    warn( 5201, "Use C++ style cast." );
}

////////////////////////////////////////////////////////////
//  Item  53 : Never treat arrays polymorphically.
//

////////////////////////////////////////////////////////////
//  Item  54 : Avoid gratuitous default constructors.
// 

////////////////////////////////////////////////////////////
//  Item  55 : Be wary of user-defined conversion functions.
//

////////////////////////////////////////////////////////////
//  Item  56 : Distinguish between prefix and postfix forms
//            of increment and decrement operators.
//

////////////////////////////////////////////////////////////
//  Item  57 : Never overload &&, ||, or ,.
//

if ( dcl_member )
{
    if ( dcl_function )
    {
        if ( strcmp( dcl_name(), "operator&&" ) == 0 )
        {
            warn( 5701, "Do not overload operator &&." );
        }
        else
        {
            if ( strcmp( dcl_name(), "operator||" ) == 0 )
            {
                warn( 5702, "Do not overload operator ||." );
            }
            else
            {
                if ( strcmp( dcl_name(), "operator," ) == 0 )
                {
                    warn( 5703, "Do not overload operator ,." );
                }
            }
        }
    }
}

////////////////////////////////////////////////////////////
//  Item  58 : Understand the difference meanings of new and
//            delete.
//

////////////////////////////////////////////////////////////
//  Item  59 : Use destructors to prevent resource leaks.
//

////////////////////////////////////////////////////////////
//  Item 60 : Prevent resource leak in constructors.
//

////////////////////////////////////////////////////////////
//  Item 61 : Prevent exceptions from leaving destructors.
//

////////////////////////////////////////////////////////////
//  Item 62 : Understand how throwing an exception differs
//            from passing a parameter or calling a virtual
//            function.
//

////////////////////////////////////////////////////////////
//  Item 63 : Catch exceptions by reference.
//

////////////////////////////////////////////////////////////
//  Item 64 : Use exception specifications judiciously.
//

////////////////////////////////////////////////////////////
//  Item 65 : Understand the costs of exception handling.
//

////////////////////////////////////////////////////////////
//  Item 66 : Remember the 80-20 rule.
//

////////////////////////////////////////////////////////////
//  Item 67 : Consider using lazy evaluation
//

////////////////////////////////////////////////////////////
//  Item 68 : Amortize the cost of expected computations.
//

////////////////////////////////////////////////////////////
//  Item 69 : Understand the origin of temporary objects.
//

////////////////////////////////////////////////////////////
//  Item 70 : Facilitate the return value of optimization.
//

////////////////////////////////////////////////////////////
//  Item 71 : Overload to avoid implicit type conversions.
//

////////////////////////////////////////////////////////////
//  Item 72 : Consider using op= instead of stand-alone op.
//

////////////////////////////////////////////////////////////
//  Item 73 : Consider alternative libraries.
//

////////////////////////////////////////////////////////////
//  Item 74 : Understand the costs of virtual functions,
//            multiple inheritance, virtual base classes,
//            and RTTI.
//

//  1. Do not inline virtual function.
if ( dcl_function )
{
    if ( dcl_virtual && dcl_inline )
    {
        warn( 2401, "Do not inline virtual function %s.", dcl_name() );
    }
}

////////////////////////////////////////////////////////////
//  Item 75 : Virtualizing constructors and non-member
//            functions.
//

////////////////////////////////////////////////////////////
//  Item 76 : Limiting of objects of a class.
//

////////////////////////////////////////////////////////////
//  Item 77 : Requiring or prohibiting heap-based objects.
//

////////////////////////////////////////////////////////////
//  Item 78 : Smart pointers.
//

////////////////////////////////////////////////////////////
//  Item 79 : Reference counting.
//

////////////////////////////////////////////////////////////
//  Item 80 : Proxy classes.
//

////////////////////////////////////////////////////////////
//  Item 81 : Making functions virtual with respect to more
//            than one object.
//

////////////////////////////////////////////////////////////
//  Item 82 : Program in future tense.
//

////////////////////////////////////////////////////////////
//  Item 83 : Make non-leaf classes abstract.
//

////////////////////////////////////////////////////////////
//  Item 84 : Understand how to combine C++ and C in the
//            same program.
//

////////////////////////////////////////////////////////////
//  Item 85 : Familiarize yourself with the language
//            standard.
//

// End of rule file.
