The files in this directory( dcomp.cc, dcomp.c ) form a tool for computation of data complexity which is based on the following model.

Suppose the we have N functions defined within project which is a set of related C source file.
For function F(i) ( i >= 1 & i <= N ),
Fan-Out(i) : the number of functions called within function F(i).
Fan-In(i) : the number of functions calls function F(i).
Complexity(i) = IOVars(i) / ( Fan-Out(i) + 1 )
    here IOVars(i) is the sum of number of global variables used in function F(i) and the number of function returning value which is 1 for functions whose returning type is not void. Functions with returning type void has value 0.

Project Complexity = ( Complexity(1) + ... + Complexity(N) ) / N

Currectly, the calculation only can be applied on C code. To calculation of data complexity takes 2 steps.
1. Run Codecheck on your source files with rule file dcomp.cc used. As the result of this step, the information which is essential for the calculation is stored in an output file. The output file has name dcomp.inf in default. The other name for this file can be specified by Codecheck cammand option -x.
2. The generated result file is subject to program dcomp as input. The program dcomp is provided in form of ANSI C source file. You need to have an ANSI C conforming C compiler to build program dcomp from source file dcomp.c. If you want to display the result of each function defined, you need to specify compiler option -DPRINT when you are compiler source file dcomp.c. Otherwise, only the result for whole project is displayed. To run program dcomp, type command line
dcomp name-of-file-from-step-1

Step 2 can be integrated into step 1. In rule file dcomp.cc, this program is invoked by Codecheck function call exec(). If you do not want to do so, please edit rule file dcomp.cc to comment out the part where function exec() called. 

Your comments are welcome. Please send your comments to Abraxas technical support at support@abxsoft.com.
