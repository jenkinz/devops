/*
***********************************************************************

    this rule file s used to extact essential information for computing
    data complexity. the information is stored in a output file which
    has name dcomp.inf in default. you can specify the file name with
    option -x while running CodeCheck with this rule file. the file
    generated is subject for further processing which is the computation
    fof data complexity. you can either invoke the computing program
    in this rule file via function exec() or simply just run program
    dcomp later.

***********************************************************************
*/
#include <check.cch>

#define BUF_SIZE 2048

FILE* out;
int   ioVars;
int   is_static;
int   voidType;
int   i;
char  buf[BUF_SIZE]; 

if ( prj_begin )
{
    if ( option( 'X' ) )
    {
        out = fopen( str_option( 'X' ), "w" );
    }
    else
    {
        out = fopen( "dcomp.inf", "w" );
    }
}

if ( prj_end )
{
    fclose( out );
    /*
      you do not have to run dcomp program here, you can invoke
      it separately after information file has been generated.
    */
    if ( option ( 'X' ) )
    {
        exec( "dcomp", str_option( 'X' ) );
    }
    else
    {
        exec( "dcomp", "dcomp.inf" );
    }
}

if ( fcn_begin )
{
    if ( is_static )
    {
        fprintf( out, "$sbegin %s\n", fcn_name() );
    }
    else
    {
        fprintf( out, "$gbegin %s\n", fcn_name() );
    }
    if ( !voidType )
    {
        ioVars++;
    }
    i = 0;
    while ( i < BUF_SIZE )
    {
        buf[i] = 0;
        i++;
    }
}

if ( fcn_end )
{
    fprintf( out, "#%d\n", ioVars );
    fprintf( out, "$end %s\n", fcn_name() );
    ioVars = 0;
    voidType = FALSE;
}

if ( op_call )
{
    fprintf( out, "%s\n", op_function() );
}

if ( idn_variable )
{
    if ( idn_global )
    {
        i = 0;
        while ( buf[i] != 0 )
        {
            if ( strcmp( buf + i, idn_name() ) != 0 )
            {
                i = i + strlen( buf ) + 1;
            }
            else
            {
                break; 
            }
        }
        if ( buf[i] == 0 )
        {
            if ( i + strlen( idn_name() ) + 1 >= BUF_SIZE )
            {
                fatal( -1, "Buffer overflow, increase value of BUF_SIZE, try again\n" );
            }
            else
            {
                ioVars++;
                strcpy( buf + i, idn_name() );
            }
        }
    }
}

if ( mod_begin )
{
    fprintf( out, "!begin %s\n", mod_name() );
}

if ( mod_end )
{
    fprintf( out, "!end %s\n", mod_name() );
}

if ( dcl_function )
{
    if ( dcl_static )
    {
        is_static = TRUE;
    }
    else
    {
        is_static = FALSE;
    }
    if ( dcl_base == VOID_TYPE )
    {
        if ( dcl_levels == 1 )
        {
            voidType = TRUE;
        }
    }
}
