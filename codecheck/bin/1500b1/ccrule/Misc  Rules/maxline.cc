int len, maxline, lines, toksiz;
char *tok;

if ( prj_begin ) {
	maxline =0;
	lines=0;
	toksiz=0;
}

if ( lex_token ) {

	tok = token();
//printf( "%s ::%d:%d\n", tok, lines, lex_token );
	if ( strlen(tok) > toksiz ) toksiz = strlen(tok);
}

if ( lin_end ) {

	len = strlen(line()); // get length is current line

	if ( len > maxline ) maxline = len;

	lines++;

if ( lin_number > 20800) {
printf( "at %d, Max Line in Project =%d\n", lin_number, maxline );
warn(0,line());
}

}


if ( prj_end ) {

	printf( "Max Line length in Project =%d\n", maxline );
	printf( "Number of Lines in project=%d\n", lines );
	printf( "Max Token Len=%d\n", toksiz );
}
