//	mfc.cc define user variable "inmfc", indicates whether current line is MFC.

char path[2048];
char *sp;
int inmfc;

if ( prj_begin ) {
	inmfc = 0;
}

if ( header_name() ) {

	strcpy( path, header_path() );

	sp = strstr( path, "VC98" ); // verify header path "mfc","vc98","msdev"

	if ( sp ) { 
	
		inmfc = 1;
	}
	else {

		inmfc = 0;
	}
printf( "********* %s - inmfc=%d\n",  path, inmfc );
}