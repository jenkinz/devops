/*
***************************************************************************
*                                                                         *
* FILE NAME:	lex.c                                                     *
* COPYRIGHT (C) 1994 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.            *
* PURPOSE:	This file contains the C code used to test CodeCheck      *
*               predefined variables in lex_ group accompanying rule      *
*               file lex.cc                                               *
*                                                                         *
***************************************************************************
*/

/* test lex_ansi_escape, lex_not_KR_escape, lex_strlen, 
   lex_concat lex_initializer */
char		*escape_1 = "abc\axyz"  "abc\vxyz"  "abc\?xyz", 
		escape_2 = '\a', escape_3 = '\v', escape_4 = '\?';

/* test lex_backslash */
short	back\
slash = "This string \
is continued on the next line.";

/* lex_punct_after */
#define	BAD_CALL(a,b,c)	a b [ c ]

/* test lex_bad_call, lex_macro_token, lex_radix,
   lex_punct_before, lex_null_arg */
short	BAD_CALL( *, bad ),
		BAD_CALL( *, call, 123, =, 321 ),
		BAD_CALL( empty, , 321 );

/* test lex_big_octal, lex_suffix, lex_num_escape, 
   lex_not_manifest, lex_initializer, lex_unsigned */
int		octal[] = { 012834, 012934U, '\08', '\09' };

/* test lex_char_long, lex_char_empty */
char		empty_char = '', multichar = 'abcd';

/* test lex_enum_comma */
enum XYZ	{ first_enum, second_enum, };

/* test lex_float */
float	float_suffix[] = { 9876F, 6789f };

/* lex_hex_escape */
char		*hex_escape_1 = "abc\xAFxyz", hex_escape_2 = '\xAF';

/* test lex_str_trigraph, lex_trigraph */
int		cons[] = { 0, 1, '??=', 12.34, L"hello??!" };

struct OUTER {
	struct INNER {
		int	abc;
		};
	};

struct INNER	invisible ;

/* test lex_key_no_space */
void test_function( int a, int b, int c )
	{
	do{}while(a);
	if(a){
		}else{
		}
	for(a;;c){
		switch(a){}
		}
	while(a){return b;}
	return(c);
	}

/* test lex_lc_long */
long		lower_case_suffix_L = 0x65432l;

/* test lex_long _float */
float	float_suffix_L = 123.456L;

/* test lex_nonstandard */
double	non$standard;

/* end of file */
