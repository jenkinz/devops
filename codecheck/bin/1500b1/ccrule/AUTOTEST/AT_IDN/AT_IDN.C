/*
***************************************************************************
*                                                                         *
* FILE NAME:	idn.c                                                     *
* COPYRIGHT (C) 1994 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.            *
* AUTHOR:	Shuming Tan                     July 25, 1994             *
* PURPOSE:	This file contains the C code used to test CodeCheck      *
*               predefined variables in idn_ group accompanying rule      *
*               file idn.cc                                               *
*                                                                         *
***************************************************************************
*/

/* define some simple type identifiers with global scope */
/* for test idn_storage_flags, idn_global, idn_function idn_base */
typedef int INTEGER;
static  int a;
register char b;
extern void c();  /* for idn_noprototype */
auto short d;
signed int e;

/* declare array and pointer variable */
/* for idn_levels, idn_level() idn_array_dim() */
int matrix[20][10];
volatile char *string;  /* idn_level_flags */

/* define a enumerate variable */
/* for test idn_constant */
enum even {ZERO,TWO,FOUR,SIX,EIGHT} ed;

/* define a strcut variable to check if the compound */
/* variable can be triggered */
struct position
{
  long x;
  long y;
} point;

union blah 
{
  int um1;
  double um2;
} combo;

/* define a class to test idn_member */
class klass
{
  char *body;
  int len;
public:
  klass();
  ~klass() {delete [] body; len=0;}
};

klass::Klass(int ll)
{
  body=new char[ll];
  len=ll;
}

void move(struct position p,int offset)
{
  p.x+=offset;       /* p.x, p.y should be detected as an variable */
  p.y+=offset;
}
  

main()
{
  /* define some identifiers with local scope */
  /* for test idn_local */
  static long f;
  unsigned int g;
  const unsigned char h='A';  /* idn_level_flags */
  unsigned short i;
  unsigned long j;
  float k;
  double l;
  long double m;
  INTEGER n;

  klass my;
  my.klass(20);

  a=g;
  b=h;
  i=d;
  c(e);
  j=k;
  m=l;
  for (n=0;n<20;n++) matrix[n][n]=100;  /* for idn_array_dim() */
  string=&point;
  move(point,10);
  combo.um1=1234;
  combo.um2=1234.5678;
  my.~klass();
  ed=TWO;
  stupid();   /* test idn_not_declared */
}

