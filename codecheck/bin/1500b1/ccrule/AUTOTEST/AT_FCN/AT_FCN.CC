/*
***************************************************************************
*                                                                         *
* FILE NAME:	fcn.cc                                                    *
* COPYRIGHT (C) 1994 BY ABRAXAS SOFTWARE. ALL RIGHTS RESERVED.            *
* AUTHOR:	Shuming Tan                     July 22, 1994             *
* PURPOSE:	This a CodeCheck rule file used to test the predefined    *
*               variables in fcn_ group.                                  *
*                                                                         *
***************************************************************************
*/



if (fcn_begin)
{
  printf("FUNCTION::%s\n",fcn_name());
}

if (fcn_end)
{
  if (fcn_no_header)
    printf("no stand-alone comment block precedes the function\n");
  printf("number of all kinds of lines        : %d\n",fcn_total_lines);
  printf("number of pure comment lines        : %d\n",fcn_com_lines);
  printf("number of blank lines               : %d\n",fcn_white_lines);
  printf("number of executable lines          : %d\n",fcn_exec_lines);
  printf("number of local simple variables    : %d\n",fcn_simple);
  printf("number of local array elements      : %d\n",fcn_array);
  printf("number of local tag members         : %d\n",fcn_members);
  printf("number of local register variables  : %d\n",fcn_register);
  printf("number of aggregate variables       : %d\n",fcn_aggr);
  printf("number of non executable statemnets : %d\n",fcn_nonexec);
  printf("number of low level statements      : %d\n",fcn_low);
  printf("number of high level statements     : %d\n",fcn_high);
  printf("number of tokens                    : %d\n",fcn_tokens);
  printf("number of Halstead operators        : %d\n",fcn_H_operators);
  printf("number of Halstead operands         : %d\n",fcn_H_operands);
  printf("number of unique Halstead operators : %d\n",fcn_uH_operators);
  printf("number of unique Halstead operands  : %d\n",fcn_uH_operands);
  printf("number of operators                 : %d\n",fcn_operators);
  printf("number of operands                  : %d\n",fcn_operands);
  printf("number of unique operators          : %d\n",fcn_u_operators);
  printf("number of unique operands           : %d\n",fcn_u_operands);
  printf("number of unused local variables    : %d\n",fcn_unused);
  printf("number of binary decision points    : %d\n",fcn_decisions);
}