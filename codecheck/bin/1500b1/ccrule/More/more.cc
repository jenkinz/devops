//  MORE - rule file according the company coding standards


//  Date: 28/08/97

//  The warnings are intended to be seen in the context of
//  a list file. Use:  check myproject.ccp -Rsample -L

//  warning/Error codes:
//       General Conventions        Errors:   1000-1499
//                                  warnings: 1500-1999
//       SW Design Conventions      Errors:   2000-2499
//                                  warnings: 2500-2999
//       Naming Conventions         Errors:   3000-3499
//                                  warnings: 3500-3999
//       Code Layout Conventions    Errors:   4000-4499
//                                  warnings: 4500-4999
//       C Style Conventions        Errors:   5000-5499
//                                  warnings: 5500-5999
//       Portability                Errors:   6000-6499
//                                  warnings: 6500-6999
//       Miscellaneous              Errors:   7000-7499
//                                  warnings: 7500-7999


#include <check.cch>

/* Begin of copied CodeCheck standard block */
/* These test all apply to the Rule "Use ANSI C Programming Style" */

#define MAX_MSG_LEN 256
#define MAX_FN_LEN  256

#define REPLACE_HDR( hdr1, hdr2 ) \
    if ( strcmp( header_name(), hdr1 ) == 0 )    \
    {    \
        WARN( 1036, "Replace header " hdr1 " with " hdr2 "." );    \
    }

#define REPLACE_FCN( fname, gname )    \
    if ( strcmp( op_function(), fname ) == 0 )    \
    {    \
        WARN( 1038, "Replace " fname " with " gname "." );    \
    }

#define WARN( msgnum, msgbdy ) \
    fprintf( fd, "#error %s(%d): Warning %d: %s\n", file_name(), lin_number, msgnum, msgbdy );

int ch,                  // Character that follows a prefix.
    k,                   // counter for dcl_level(k)
    non_ANSI_mod,        // Non-ANSI type modifier flags
    varargs_included;    // True if <varargs.h> was included.

int index;
int modHdrFound;

FILE *fd;

char msgBuf[MAX_MSG_LEN],
     hdrName[MAX_FN_LEN];

if ( prj_begin )
{
    // Flags for non-ANSI type and pointer modifiers:
    non_ANSI_mod = ~( CONST_FLAG + VOLATILE_FLAG );

    // Make sure that extended keywords are allowed on DOS machines:
#ifdef __MSDOS__
    if ( option( 'K' ) < 3 )
    {
        fatal( -1, "Rule file ansi.cc should not be run with -K1 or -K2." );
    }
#endif

    fd = fopen( "prj.inf", "w" );
    if ( fd == NULL )
    {
        fatal( -1, "could not open intermediate file mod.msg, quit." );
    }
}

if ( prj_end )
{
    fclose( fd );
}

if ( mod_begin )
{
    fprintf( fd, "#mod_begin %s\n", mod_name() );

    modHdrFound = FALSE;
}

if ( mod_end )
{
    fprintf( fd, "#mod_end %d\n", lin_number );
}

if ( fcn_begin )
{
    fprintf( fd, "#fcn_begin %s\n", fcn_name() );
}

if ( fcn_end )
{
    fprintf( fd, "#fcn_end %s\n", fcn_name() );
}

if ( idn_function )
{
    fprintf( fd, "#call %s@%d\n", idn_name(), lin_number );
}

if ( idn_global )
{
    fprintf( fd, "#global_var %s\n", idn_name() );
}

//////////////////////////////////////////////////////////////////
//
//    General Conventions
//
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
//    RULES
//

// Rule 1 : message# : 1000 - 1039
// Use ANSI-C programming style.
if ( lex_big_octal )
{
    WARN( 1001, "Use ANSI: Octal digits 8 and 9 are illegal in ANSI C." );
}

if ( lex_long_float )
{
    WARN( 1002, "Use ANSI: The long float type is illegal in ANSI C." );
}

if ( lex_nl_eof )
{
   WARN( 1003, "Use ANSI: ANSI C files must end with a newline character." );
}

if ( lex_str_length > 509 )
{
    WARN( 1004, "Use ANSI: String literal too long for ANSI C." );
}

if ( lex_str_macro )
{
    WARN( 1006, "Use ANSI: ANSI C does not expand macros inside strings." );
}

if ( lex_str_trigraph )
{
    WARN( 1007, "Use ANSI: ANSI C will recode the trigraph in this string." );
}

if ( pp_arg_count > 31 )
{
    WARN( 1008, "Use ANSI: Too many parameters in this macro." );
}

if ( pp_comment )
{
    WARN( 1009, "Use ANSI: This comment will not paste tokens in ANSI C." );
}

if ( pp_if_depth > 8 )
{
    WARN( 1010, "Use ANSI: Header file nesting is too deep." );
}

if ( pp_not_ansi )
{
    WARN( 1011, "Use ANSI: This preprocessor usage is not allowed in ANSI C." );
}

if ( pp_sizeof )
{
    WARN( 1012, "Use ANSI: ANSI C does not permit sizeof in directives." );
}

if ( pp_trailer )
{
    WARN( 1013, "Use ANSI: ANSI C does not parse tokens that follow a preprocessor directive." );
}

if ( pp_unknown )
{
    WARN( 1014, "Use ANSI: Not an ANSI preprocessor directive." );
}

if ( dcl_empty )
{
    if ( !dcl_tag_def )
    {
        WARN( 1015, "Use ANSI: This declaration is not valid." );
    }
}

if ( dcl_ident_length > 31 )
{
    WARN( 1016, "Use ANSI: Only 31 chars are significant." );
}

if ( dcl_long_float )
{
    WARN( 1017, "Use ANSI: Use double instead of long float." );
}

if ( dcl_need_3dots )
{
    WARN( 1018, "Use ANSI: ANSI C requires 3 dots (...) here." );
}

if ( dcl_no_specifier )
{
    if ( dcl_function )
    {
        msgBuf[0] = 0;
        sprintf( msgBuf, "Use ANSI: Return type for function %s should be specified.", dcl_name() );
        WARN( 1018, msgBuf );
    }
    else
    {
        msgBuf[0] = 0;
        sprintf( msgBuf, "Use ANSI: An explicit type for %s is required in ANSI C.", dcl_name() );
        WARN( 1019, msgBuf  );
    }
}

if ( dcl_static )
{
    if ( dcl_local && dcl_function )
    {
        WARN( 1020, "Use ANSI: Local static function declarations are not allowed in ANSI C." );
    }
}

if ( dcl_zero_array )
{
    WARN( 1021, "Use ANSI: Zero-length arrays are illegal in ANSI C." );
}

if ( idn_no_prototype )
{
    msgBuf[0] = 0;
    sprintf( msgBuf, "Use ANSI: Function %s has no prototype.", idn_name() );
    WARN( 1022, msgBuf );
}

if ( mod_macros > 1024 )
{
    WARN( 1023, "Use ANSI: Too many macros for some ANSI compilers.");
}

if ( dcl_base == EXTRA_INT_TYPE )
{
    if ( lin_header != SYS_HEADER )
    {
        WARN( 1024, "Use ANSI: non-ANSI integer type." );
    }
}

if ( dcl_base == EXTRA_UINT_TYPE )
{
    if ( lin_header != SYS_HEADER )
    {
        WARN( 1024, "Use ANSI: non-ANSI unsigned type." );
    }
}

if ( dcl_base == EXTRA_FLOAT_TYPE )
{
    if ( lin_header != SYS_HEADER )
    {
        WARN( 1024, "Use ANSI: non-ANSI float type." );
    }
}

if ( dcl_base == EXTRA_PTR_TYPE )
{
    if ( lin_header != SYS_HEADER )
    {
        WARN( 1024, "Use ANSI: non-ANSI pointer type." );
    }
}

if ( dcl_storage_flags & GLOBAL_SC )
{
    if ( lin_header != SYS_HEADER )
    {
        WARN( 1024, "Use ANSI: VAX globaldef and globalref are not ANSI type specifiers." );
    }
}

if ( keyword( "comp" ) )
{
    if ( lin_header != SYS_HEADER )
    {
        WARN( 1024, "Use ANSI: Apple comp is not an ANSI type specifier." );
    }
}

if ( keyword( "extended" ) )
{
    if ( lin_header != SYS_HEADER )
    {
        WARN( 1025, "Use ANSI: For Apple \"extended\" type use ANSI \"long double\"." );
    }
}

if ( dcl_variable || dcl_function )
{
    if ( lin_header != SYS_HEADER )
    {
        k = 0;
        while ( k <= dcl_levels )
        {	
            if ( dcl_level_flags( k++ ) & non_ANSI_mod )
            {
                WARN( 1026, "Use ANSI: Non-ANSI type modifier." );
            }
        }
    }
}

if ( dcl_cv_modifier )
{
    if ( lin_header != SYS_HEADER )
    {
        WARN( 1027, "Use ANSI: Non-ANSI placement of const or volatile." );
    }
}

if ( dcl_function_flags )
{
    WARN( 1028, "Use ANSI: Non-ANSI function type specifier." );
}

if ( lex_nested_comment )
{
    WARN( 1029, "Use ANSI: Nested comments are not allowed in ANSI C." );

    // If you want CodeCheck to assume that nested comments are okay as
    // soon as it finds the first such comment, then enable these 2 lines:

    /*
    if ( option( 'N' ) == 0 )    // Assume that the rest of this file
    {                            // will contain nested comments too.
        set_option( 'N', 1 );
    }
    */
}

// Although many modern compilers allow labels that are not attached
// to any statement, e.g. at the end of a block, this is not allowed
// by the ANSI standard.

if ( stm_bad_label )
{
    WARN( 1030, "Use ANSI: The preceding label is not attached to a statement." );
}

// Detects local arrays that have no explicit dimension. Some
// pre-ANSI compilers consider such arrays to be implicitly
// external (i.e. the identifier has file scope and external
// linkage). This interpretation is not allowed in ANSI C.

if ( dcl_level( 0 ) == ARRAY )
{
    if ( dcl_local && ( dcl_array_size == -1 ) && ( !dcl_parameter ) )
    {
        if ( ( !dcl_extern ) && ( !dcl_initializer ) )
        {
            msgBuf[0] = 0;
            sprintf( msgBuf, "Use ANSI: Array %s must be declared extern.", dcl_name() );
            WARN( 1031, msgBuf );
        }
    }
}


// Detect Zortech binary constants (e.g. 0b10101001):

if ( lex_radix == 2 )
{
    if ( lin_header != SYS_HEADER )
    {
        WARN( 1032, "Use ANSI: Binary constants are not permitted in ANSI C." );
    }
}

// Check each external identifier for reserved prefixes:

if (  dcl_global && !dcl_static )
{
    if ( lin_header != SYS_HEADER )
    {
        if ( prefix( "E" ) )
        {
            ch = root()[0];
            if ( isdigit( ch ) || isupper( ch ) )
            {
                msgBuf[0] = 0;
                sprintf( msgBuf, "Use ANSI: %s: prefix E is reserved by ANSI.", dcl_name() );
                WARN( 1033, msgBuf );
            }
        }
        else if ( prefix("is") )
        {
            ch = root()[0];
            if ( islower( ch ) )
            {
                msgBuf[0] = 0;
                sprintf( msgBuf, "Use ANSI: %s: prefix \"is\" is reserved by ANSI.", dcl_name() );
                WARN( 1033, msgBuf );
            }
        }
        else if ( prefix( "to" ) )
        {
            ch = root()[0];
            if ( islower( ch ) )
            {
                msgBuf[0] = 0;
                sprintf( msgBuf, "Use ANSI: %s: prefix to is reserved by ANSI.", dcl_name() );
                WARN( 1033, msgBuf );
            }
        }
        else if ( prefix( "LC_" ) )
        {
            ch = root()[0];
            if ( isupper( ch ) )
            {
                msgBuf[0] = 0;
                sprintf( msgBuf, "Use ANSI: %s: prefix LC_ is reserved by ANSI.", dcl_name() );
                WARN( 1033, msgBuf );
            }
        }
        else if ( prefix( "SIG" ) )
        {
            ch = root()[0];
            if ( isupper( ch ) || ( ch == '_' ) )
            {
                msgBuf[0] = 0;
                sprintf( msgBuf, "Use ANSI: %s: prefix SIG is reserved by ANSI.", dcl_name() );
                WARN( 1033, msgBuf );
            }
        }
        else if ( prefix( "mem" ) )
        {
            ch = root()[0];
            if ( islower( ch ) )
            {
                msgBuf[0] = 0;
                sprintf( msgBuf, "Use ANSI: %s: prefix mem is reserved by ANSI.", dcl_name() );
                WARN( 1033, msgBuf );
            }
        }
        else if ( prefix( "str" ) )
        {
            ch = root()[0];
            if ( islower( ch ) )
            {
                msgBuf[0] = 0;
                sprintf( msgBuf, "Use ANSI: %s: prefix str is reserved by ANSI.", dcl_name() );
                WARN( 1033, msgBuf );
            }
        }
        else if ( prefix( "wcs" ) )
        {
            ch = root()[0];
            if ( islower( ch ) )
            {
                msgBuf[0] = 0;
                sprintf( msgBuf, "Use ANSI: %s: prefix wcs is reserved by ANSI.", dcl_name() );
                WARN( 1033, msgBuf );
            }
        }
    }
}

if ( macro( "va_dcl" ) )
{
    WARN( 1034, "Use ANSI: ANSI C does not use va_dcl." );
}

if ( macro( "va_start" ) )
{
    if ( varargs_included )
    {
        WARN( 1035, "Use ANSI: Macro va_start has two arguments in ANSI C." );
    }
}

if ( header_name() )
{
    if ( strcmp( header_name(), "varargs.h" ) == 0 )
    {
        WARN( 1036, "Use ANSI: Replace <varargs.h> with <stdargs.h>." );
        varargs_included = 1;
    }
    else REPLACE_HDR( "memory.h", "string.h" )
    else REPLACE_HDR( "sys/times.h", "time.h" )
}

if ( op_call )
{
    REPLACE_FCN( "cfree", "free" )
    else REPLACE_FCN( "bcmp", "strcmp" )
    else REPLACE_FCN( "bzero", "memset" )
    else REPLACE_FCN( "strpos", "strchr" )
    else REPLACE_FCN( "strrpos", "strrchr" )
    else REPLACE_FCN( "mktemp",	"tmpnam" )
}

if ( pp_sub_keyword )
{
   WARN( 1038, "Use ANSI: Replacement of preprocessor commands is not allowed in ANSI C." );
}

if ( exp_empty_initializer )
{
    WARN( 1039, "Use ANSI: Empty initializers are not allowed in ANSI C." );
}

// End of copied CodeCheck standard block.

// Rule 2 : message# : 1041
// External identifiers needs 6 significant characters.
if ( dcl_extern_ambig )
{
    WARN( 1041, "External identfier needs six significant characters." );
}

// Rule 3 : message# : 1042
// String literals not longer than 509.
// See message 1019.

// Rule 4 : message# : 1043
// Prototyping should always be used.
// See message 1022

//////////////////////////////////////////////////////////////////
//    RECOMMENDATIONS
//

//////////////////////////////////////////////////////////////////
//
//    SW Design Conventions
//
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
//    RULES
//

// Rule 5 : message# : 2000
// Do not use recursive function calls.
// The message is generated in post processing.

// Rule 6 : message# : 2001
// Maximum 10 external functions per module.
// The message is generated in post processing.

// Rule 7 : message# : 2002
// Maximum 20 global variables per module, message 
// The message is generated in postprocessing.

// Rule 8 : message# : 2003
// Maximum 6 arguments per function.
if ( dcl_parm_count > 6 )
{
    WARN( 2003, "Max. 6 arguments per function" );
}

// Rule 9 : message# : 2004
// The maximum include depth is 1
if ( header_name() )
{
    if ( pp_include_depth > 1 )
    {
        WARN( 2004, "The maximum include depth is 1. " );
    }
}

//////////////////////////////////////////////////////////////////
//    RECOMMENDATIONS
//

// Rec. 1 : message# : 2501
// Do not use explicit path names to include header files
if ( pp_relative )    /* for nested includes */
{
    WARN( 2501, "Don't use explicit path names to include header files" );
}

if ( header_path() )
{
    if ( !lin_header )
    {
        if ( header_path()[0] != 0 && header_path()[0] != '/' )
        {
            if ( strstr( header_path(), "/" ) != NULL )
            {
                WARN( 2501, "Don't use explicit path name to include header files" );
            }
        }
    }
}

//////////////////////////////////////////////////////////////////
//
//    Naming Conventions
//
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
//    RULES
//

// Rule 10 : meesage# : 3000
// C files should be in form "xxxxxxxx.c"
if ( mod_begin )
{
    index = strcspn( mod_name(), "." );
    if ( index == strlen( mod_name() ) )
    {
        WARN( 3001, "C Files should be in the form 'xxxxxxxx.c'" );
    }
    else
    {
        if ( index > 7 )
        {
            WARN( 3000, "C Files should be in the form 'xxxxxxxx.c'" );

        }
        else
        {
            if ( tolower( mod_name()[index + 1] ) != 'c' || strlen( mod_name() + index + 1 ) != 1 )
            {
                WARN( 3000, "C Files should be in the form 'xxxxxxxx.c'" );
            }
        }
    }
}

// Rule 11 : message# : 3001
// Include files should be in the form "xxxxxxxx.h".
if ( header_name() )    // A header is about to be opened
{
    strcpy( hdrName, header_name() );
}

if ( pp_include < 3 )	// Header filename is in double quotes
{
    index = strcspn( hdrName, "." );
    if ( index == strlen( hdrName ) )
    {
        msgBuf[0] = 0;
        sprintf( msgBuf, "Include Files should be in the form 'xxxxxxxx.h'", header_name() );
        WARN( 3001, msgBuf );
    }
    else
    {
        if ( index > 7 )
        {
            msgBuf[0] = 0;
            sprintf( msgBuf, "Include Files should be in the form 'xxxxxxxx.h'", header_name() );
            WARN( 3001, msgBuf );
        }
        else
        {
            if ( tolower( hdrName[index + 1] ) != 'h' || strlen( hdrName + index + 1 ) != 1 )
            {
                msgBuf[0] = 0;
                sprintf( msgBuf, "Include Files should be in the form 'xxxxxxxx.h'", header_name() );
                WARN( 3001, msgBuf );
            }
        } 
    }
    if ( strncmp( mod_name(), hdrName, index ) == 0 )
    {
        modHdrFound = TRUE;
    }
}

// Rule 12 : message# : 3002
// Spec : Identfier length has to be at least 3 characters.
if ( dcl_ident_length < 3 )
{
    WARN( 3002, "Identifier length has to be at least 3 characters" );
}

// Rule 13 : message# : 3003
// Spec : Constants should be UPPER case can prepended with "C_".
int okay;
if ( dcl_variable )
{
    if ( dcl_level_flags( 0 ) & CONST_FLAG )
    {
        okay = TRUE;
        if ( dcl_ident_length >= 3 )
        {
            if ( !prefix( "C_" ) )
            {
                okay = FALSE;
            }
            else if ( !dcl_all_upper )
            {
                okay = FALSE;
            }
        }
        if ( !okay )
        {
            WARN( 3003, "Constants should be UPPER case and prepended with a \"C_\"." );
        }
    }
}

// Rule 14 : message# : 3004
// Spec : #define/macro names should be in UPPER case.
if ( pp_macro )
{
    if ( pp_lowercase )
    {
        WARN( 3004, "Macro name should be UPPER case." );
    }
}

// Rule 15 : message# : 3005
// Spec : Type names should be only first upper and prepended with a t_.
char* p;
if ( dcl_typedef )
{
    okay = TRUE;
    if ( strlen( dcl_name() ) >= 3 )
    {
        if ( !prefix( "t_" ) )
        {
            okay = FALSE;
        }
        else if ( !isupper( dcl_name()[2] ) )
        {
            okay = FALSE;
        }
        else
        {
            if ( dcl_ident_length > 3 )
            {
                p = dcl_name() + 3;
                while ( p[0] != 0 )
                {
                    if ( isupper( p[0] ) )
                    {
                        okay = FALSE;
                        break;
                    }
                    p = p + 1; 
                }
            } 
        }
    }
    if ( !okay )
    {
        WARN( 3005, "Type name should be only first UPPER and prepended a \"t_\"." );
    }
}

// Rule 16 : message# : 3006
// Spec : Enum names should be UPPER case.
if ( dcl_enum )
{
    if ( !dcl_all_upper )
    {
        WARN( 3006, "Enum names should UPPER case." );
    }
}

// Rule 17 : message : 3007
// Spec : Gloabl variables should be lower case and prepended with a g_.
// Comment : Rule 17, 18 and 19 are in same rule body.

// Rule 18 : message# : 3008
// Spec : Static variables should be lower case and prepended with a s_.
// Comment : Rule 17, 18 and 19 are in same rule body.

// Rule 19 : message# : 3009
// Spec : Local variable should be lower case.
if ( dcl_variable )
{
    if ( dcl_ident_length >= 3 )
    {
        if ( dcl_global )
        {
            if ( dcl_static )
            {
                if ( dcl_any_upper || !prefix( "s_" ) )
                {
                    WARN( 3008, "Static variable should be lower case and prepended with a \"s_\"." );
                }
            }
            else
            {
                if ( dcl_any_upper || !prefix( "g_" ) )
                {
                    WARN( 3007, "Global variable should be lower case and prepended with a \"g_\"." ); 
                }
            }
        }
        else
        {
            if ( dcl_static )
            {
                if ( dcl_any_upper || !prefix( "s_" ) )
                {
                    WARN( 3008, "Static variable should be lower case and prepended with a \"s_\"." );
                }
            }
            else
            {
                if ( dcl_any_upper )
                {
                    WARN( 3009, "Local variable should be lower case." );
                }
            }
        }
    }
}

// Rule 20 : message# : 3010
// Spec : Pointers should be lower and prepended with a p_.
if ( dcl_variable )
{
    if ( dcl_level( 0 ) == POINTER )
    {
        if ( dcl_ident_length >= 3 )
        {
            if ( dcl_any_upper || !prefix( "p_" ) )
            {
                WARN( 3010, "Pointer should be lower and prepended with a \"p_\"." );
            }
        }
    }
}

// Rule 21 : message# : 3011
// Spec : Place the '*' close to pointer type.
if ( op_pointer )
{
    if ( op_space_before && !op_space_after )
    {
        WARN( 3012, "Place the '*' close to pointer type." );
    }
}

// Rule 22 : message# : 3012
// Spec : Function names should be first UPPER, no '_'.
if ( dcl_function )
{
    if ( !isupper( dcl_name()[0] ) || strstr( dcl_name() , "_" ) )
    {
        WARN( 3012, "Function name should be only first UPPER and no '_'." );
    }
}

//////////////////////////////////////////////////////////////////
//    RECOMMENDATIONS
//

// Rec. 2 : message# : 3500
// Spec : Split file names into two words (noun-action).
// Comment : Since CodeCheck can not tell if the combination of certain
//           characters is a word, this rule can not be implemented.

// Rec. 3 : message# : 3501
// Spec : One include file with the same name as C-module.
if ( mod_end )
{
    if ( !modHdrFound )
    {
        WARN( 3501, "One include file with the same name as C-module." );
    }
}

// Rec. 4 : message# : 3502
// No Non-Standard Characters.
if ( lex_nonstandard )
{
    WARN( 3502, "No Non-Standard Characters" );
}

// Rec. 5 : message# : 3503
// No identifier begins with one or two underscores ( '_'/ "__" ).
if ( dcl_underscore )
{
    WARN( 3503, "No identifier beginning with one or two underscores ('_' / \"__\")." );
}

//////////////////////////////////////////////////////////////////
//
//    Code Layout Conventions
//
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
//    RULES
//

// Rule 23 : message# : 4000
// Use C-module header.

// Rule 24 : message# : 4001
// Use strict order of sections.
int msgGiven;
int modHdrComm;
if ( mod_begin )
{
    modHdrComm = FALSE;
    msgGiven = FALSE;
}

if ( lin_is_comment )
{
    if ( !modHdrComm || !msgGiven )
    {
        if ( strstr( line(), "MODULE HEADER" ) != 0 )
        {
            modHdrComm = TRUE;
        }
    }
}

if ( lin_has_code || lin_preprocessor )
{
    if ( !modHdrComm && !msgGiven)
    {
        WARN( 4000, " Need C module header" );
        msgGiven = TRUE;
    }
}

int efFound;
int evFound;
int gvFound;
int fpFound;
int fdFound;

if ( lin_end )
{
    if ( !lin_header )
    {
        if ( lin_preprocessor )
        {
   
            if ( efFound || evFound || gvFound || fpFound || fdFound )
            {
                WARN( 4001, "Preprocessor directives should be first section of a module." );
            }
        }
    }
}

if ( dcl_function )
{
    if ( !lin_header )
    if ( dcl_extern )
    {
        if ( evFound || gvFound || fpFound || fdFound )
        {
            if ( !efFound )
            {
               WARN( 4001, "Extern functions should not after extern vars, global vars, function prototypes and function defintions." );
            }
        }
        efFound = TRUE;
    }
    else
    {
        if ( !dcl_definition )
        {
            if ( fdFound )
            {
                if ( !fpFound )
                {
                    WARN( 4001, "Function prototypes should not be after function defintions." );
                }
            }
            fpFound = TRUE;
        }
        else
        {
            fdFound = TRUE;
        }
    }
}    

if ( dcl_variable )
{
    if ( !dcl_parameter && !dcl_local &&!lin_header )
    {
        if ( dcl_extern )
        {
            if ( !evFound )
            {
                if ( gvFound || fpFound || fdFound )
                {
                    WARN( 4001, "Extern variables should bot be after global variables, function prototypes and function defintions." );
                }
                evFound = TRUE;
            }
        }
        else
        {
            if ( fpFound || fdFound )
            {
                if ( !gvFound )
                {
                    WARN( 4001, "Global variables should bot be after function prototypes and function definitions." );
                }
            } 
            gvFound = TRUE;
        }
    }
}

if ( mod_end )
{
    gvFound = FALSE;
    efFound = FALSE;
    evFound = FALSE;
    fdFound = FALSE;
    fpFound = FALSE;
}

// Rule 25 : message# : 4002
// Use C-function header.

// Rule 26 : message# : 4003
// Use defined delimiters for strategic comments.

// Rule 27 : message# : 4004
// Use 'H' module header.

// Rule 28 : message# : 4005
// Use header file guards.
int lin_if_depth,		//  Holds latest value of pp_if_depth.
    wrapper_needed,		//  True when a header file is opened.
    bad_name,			//  True if the wrapper name is wrong.
    length,
    lengthf,
    k1,
    ch1,
    ch2;

if ( mod_begin )
{
    set_option( 'S', 1 );    //  Apply these rules to user header files.
}
if ( pp_if_depth || pp_endif )
{
    lin_if_depth = pp_if_depth;
}

if ( pp_include )
{
    wrapper_needed = 1;
    if ( lin_header && lin_if_depth == 0 )
    {
        WARN( 4005, "Use Header File Guards: This #include should be wrapped in an #ifndef." );
    }
    if ( lin_has_code )
    {
        if ( lin_header && wrapper_needed )
        {
            WARN( 4011, "Use Header File Guards: This header file needs a wrapper." );
            wrapper_needed = 0;
        }
    }
    if ( pp_macro )
    {
        if ( lin_header && wrapper_needed )
        {
            wrapper_needed = 0;
            bad_name = 0;
            length = strlen( pp_name() );
            lengthf = strlen( file_name() );
            if ( length != lengthf )
            {
                bad_name = 1;
            }
            else if ( pp_name()[length - 2] != '_' )
            {
                bad_name = 1;
            }
            else if ( pp_name()[length - 1] != 'H' )
            {
                bad_name = 1;
            }
            else
            {
                k1 = 0;
                while ( k1 < length - 2 )
                {
                    ch1 = pp_name()[k1];
                    ch2 = file_name()[k1];
                    if ( ch1 != toupper( ch2 ) )
                    {
                        bad_name = 3;
                        break;
                    }
                    k1++;
                }
            }
            if ( bad_name )
            {
                msgBuf[0] = 0;
                sprintf( msgBuf,  "Use Header File Guards: %s is not the correct wrapper name for this file!", pp_name() );
                WARN( 4011, msgBuf );
            }
            else if ( lin_if_depth == 0 )
            {
                WARN( 4011, "Use Header File Guards: This #define should be wrapped within an #ifndef." );
            }
        }
    }
}

// Rule 29 : message# : 4006
// Spec : Use 4 spaces for indention - no tabs.
#define SPACES_PER_TAB 4    /* Indention is 4 */
if ( lin_within_function )
{
    if ( !( lin_is_comment || lin_preprocessor || lin_continuation || lin_has_label ) )
    {
        if ( lin_indent_space )   /* indention found */
        {
            // if ( wrong indention || tabs )
            if ( ( lin_nest_level * SPACES_PER_TAB - lin_indent_space ) || lin_indent_tab )
            {
                WARN( 4005, "Use 4 spaces for indention - no tabs" );
            }
        }
    }
}

// Rule 30 : message# : 4007
// Spec : No whitespace around member selection and unary operators.
if ( op_high_infix )
{
    if ( op_white_before || op_white_after)
    {
        WARN( 4007, "No whitespaces around member selection and unary operators." );
    }
}

if ( op_high_prefix )
{
    if ( op_white_before || op_high_postfix )
    {
        WARN( 4007, "No whitespaces around member selection and unary operators." );
    }
}

// Rule 31 : message# : 4008
// Use spaces around assignment operators.
if ( op_low || op_colon_2 )
{
    if ( !op_white_before || !op_white_after )
    {
        WARN( 4008, "Use spaces around all assignment operators" );
    }
}

// Rule 32 : message# : 4009
// No space before comma/semicolon/colons, but after.
if ( lex_punct_after || lex_punct_before )
{
    WARN( 4009, "No space before comma/semicolon/colons; but after" );
}

// Rule 33 : message# : 4010
// No space between parentheses and expressions.
if ( op_open_paren )
{
   if ( op_white_after )
   {
       WARN( 4010, "No space between parentheses and expressions" );
   }
}

if ( op_close_paren )
{
    if ( op_white_before )
    {
        WARN( 4010, "No space between parentheses and expressions" );
    }
}

// Rule 34 : message# : 4011
// Maximum block depth is 6.
if ( stm_depth > 6 )
{
    WARN( 4011, "Max. block depth of 6." );
}

// Rule 35 : message# : 4012
// Braces( "{}" ) of blocks in the same column.

// Rule 36 : message# : 4013
// Use block braces although none or only one statement.
if ( stm_need_comp )
{
    WARN( 4012, "Use block braces although none or only one statement." );
}

int start;
if ( stm_container < FCN_BODY )    /* if, else, while, do, for, switch */
{
    if ( stm_kind != COMPOUND )
    {
        if ( stm_lines > 1 )
        {
            start = lin_number - stm_lines - 1;
        }
        else
        {
            start = lin_number;
        }

        if ( stm_kind == EMPTY )
        {
            WARN( 4009, "Use block braces although none or only one statement" );
        }
        else
        {
            switch ( stm_container )
            {
                case IF:
                    if ( stm_lines > 1 )
                    {
                        WARN( 4009, "Use block braces although none or only one statement" );
                    }
                    else
                    {
                        WARN( 4009, "Use block braces although none or only one statement" );
                    }
                    break;

                case ELSE:
                    switch ( stm_kind )
                    {
                        case IF:      /*  Do not flag the else-if construction.      */
                        case ELSE:
                        case WHILE:   /*  Do not flag the else-while construction.   */
                        case DO:      /*  Do not flag the else-do construction.      */
                        case FOR:     /*  Do not flag the else-for construction.     */
                        case SWITCH:  /*  Do not flag the else-switch construction.  */
                            break;

                        default:
                            WARN( 4009, "Use block braces although none or only one statement" );
                            break;
                    }
                    break;

                case WHILE:
                case DO:
                case FOR:
                    if ( stm_lines > 1 )
                    {
                        WARN( 4009, "Use block braces although none or only one statement" );
                    }
                    else
                    {
                        WARN( 4009, "Use block braces although none or only one statement" );
                    }
                    break;

                case SWITCH:    /* Rare, but legal. */
                    WARN( 4009, "Use block braces although none or only one statement" );
                    break;
            }
        }
    }
}

if ( stm_cases )
{
    if ( stm_kind != COMPOUND )
    {
        WARN( 4009, "Use block braces although none or only one statement" );
    }
}

// Rule 37 : message : 4014
// Spec : Block braces must have their own line.
//if ( stm_cp_begin )
if ( op_open_brace )
{
    if ( strequiv( prev_token(), ")" ) )
    {
        if ( lex_token != 1 )
        {
            WARN( 4014,"Block braces must have their own line" );
        }
        else
        {
            p = strstr( line(), "{" ) + 1;
            while ( p[0] != 0 )
            {
                if ( p[0] !=' ' && p[0] != '\t' )
                {
                    if ( p[0] == '/' )
                    {
                        if ( p[1] == '/' )
                        {
                            break;
                        }
                        else
                        {
                            if ( p[1] == '*' )
                            {
                                p=p+2;
                                while ( p[0] != 0 )
                                {
                                    if ( p[0] == '*' )
                                    {
                                        if ( p[1] == '/' )
                                        {
                                            p=p+2;
                                            break;
                                        }
                                        else
                                        {
                                            p = p + 1;
                                        }
                                    }
                                    else
                                    {
                                        p = p + 1;
                                    }
                                }
                                if ( p[0] == 0 )
                                {
                                    break;
                                }
                            }
                            else
                            {
                                WARN( 4014, "Block braces must have their own line." );
                                break;
                            }            
                        }
                    }
                    else
                    {
                        WARN( 4014, "Block braces must have their own line." );
                        break;
                    }
                }
                p=p+1;
            }
        }
    }
}

// Rule 38 : message# : 4015
// Spec : Only one statement per line.
int stmNum;
if ( stm_end )
{
  if ( stm_kind > COMPOUND )
  {
    stmNum++;
  }
}
if ( lin_end )
{
   if ( stmNum > 1 )
   {
       WARN( 4015,"Only one statement per line" );
   }
   stmNum = 0;
}

// Rule 39 : message# : 4016
// Spec : Only one identitifier declaration per line + comment
int paramInLine;
if ( dcl_parameter )
{
    paramInLine = dcl_parameter;
}

if ( lin_end )
{
    paramInLine = 0;
}
if ( lin_dcl_count )
{
    if ( ( lin_dcl_count - paramInLine > 1 ) || !lin_has_comment )
    {
        WARN( 4016, "Only one identifier declaration per line + comment" );
    }
}

// Rule 40 : message# : 4017
// Spec : Do not use // to comment.
if ( lex_cpp_comment )
{
    WARN( 4017, "Don't use // to comment" );
}

//////////////////////////////////////////////////////////////////
//    RECOMMENDATIONS
//

// Rec. 6 : message# : 4500
// Spec : Line should not exceeds 79 characters.
if ( lin_length > 79 )
{
    WARN( 4500, "Line should not exceed 79 characters" );
}

// Rec. 7 : message# : 4501
// Use strategic comment to describe function blocks.

//////////////////////////////////////////////////////////////////
//
//    C Stype Conventions
//
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
//    RULES
//

// Rule 41 : message# : 5000 
// Always use "break" in the "case" block of a "switch".
if ( stm_no_break )
{
    WARN( 5000, "Always use 'break' in the 'case' block of a 'switch'" );
}

// Rule 42 : message# : 5001
// Always use a "default" branch in a "switch".
if ( stm_no_default )
{
    WARN( 5001, "Always use a 'default' branch in a 'switch'" );
}

// Rule 43 : message# : 5002
// Maximum 32 case branches in s switch statements.
if ( stm_end )
{
    if ( stm_kind == SWITCH )
    {
        if ( stm_switch_cases > 32 )
        {
            WARN( 5002, "Maximum 32 case branches in a switch statement. " );
        }
    }
}

// Rule 44 : message# : 5003
// Never Use "goto".
if ( keyword("goto" ) )
{
    WARN( 5003, "Never use 'goto'" );
}

// Rule 45 : message# : 5004
// Spec : Do not use "continue" or "break" in loops.
// Comment : Since continue statement can only use within an enclosing loop,
//           this rule means that continue always should be avoided.
if ( stm_end )
{
    if ( stm_kind == CONTINUE )
    {
        WARN( 5004, "Do not use continue or break in loops. " );
    }
}

char container[10];
int conLevel;

if ( keyword( "for" ) )
{
    container[conLevel++] = FOR;
}

if ( keyword( "while" ) )
{
    container[conLevel++] = WHILE;
}

if ( keyword( "do" ) )
{
    container[conLevel++] = DO;
}

if ( keyword( "switch" ) )
{
    container[conLevel++] = SWITCH;
}

if ( stm_end )
{
    if ( stm_kind == FOR || stm_kind == WHILE || stm_kind == DO || stm_kind == SWITCH )
    {
        conLevel--;
    }
    if ( stm_kind == BREAK )
    {
        if ( container[conLevel - 1] != SWITCH )
        {
            WARN( 5004, "Do not use continue or break in loops." );
        }
    }
}

// Rule 46 : message# : 5005
// Spec : Use inclusive lower limits and exclusive upper limits.

// Rule 47 : message# : 5006
// Variables are to be declared with the smallest possible scope.

// Rule 48 : message# : 5007
// Indicate type conversions with cast operator.
if ( op_assign || op_add_assign || op_sub_assign || op_and_assign
   || op_or_assign || op_xor_assign || op_mul_assign || op_div_assign
   || op_rem_assign || op_right_assign || op_left_assign )
{
    if ( op_base( 1 ) != op_base( 2 ) || op_levels( 1 ) != op_levels( 2 ) )
    {
        WARN( 5007, "Indicate type conversion with case operator. " );
    }
}

// Rule 49 : message# : 5008
// Expression bracket depth not higher than 10.
int bracketLevel;

if ( op_open_bracket )
{
    bracketLevel++;
    if ( bracketLevel == 11 )
    {
        WARN( 5008, "Expression bracket depth not higher than 10." );
    }
}

if ( op_close_bracket )
{
    bracketLevel--;
}

// Rule 50 : message# : 5009
// Spec : Exactly one return with correct data type.
int nReturns;
if ( keyword( "return" ) )
{
    ++nReturns;    // Flag functions with more than one return.
    if ( nReturns == 2 )
    {
	WARN( 5009, "Exactly one return with correct data type" );
    }
}

if ( stm_return_void )
{
    WARN( 5009, "Exactly one return with correct data type" );
}

// Rule 51 : message# : 5010
// Spec : No complex data structure as return parameter.
if ( dcl_function )
{
    if ( dcl_base == UNION_TYPE || dcl_base == STRUCT_TYPE )
    {
        WARN( 5010, "No complex data structure as return parameter." );
    }
}

// Rule 52 : message# : 5011
// Spec : No complex data structure as parameters.
if ( dcl_parameter )
{
    if ( dcl_base == UNION_TYPE || dcl_base == STRUCT_TYPE )
    {
        WARN( 5011, "No complex data structure as parameter." );
    }
}

//////////////////////////////////////////////////////////////////
//    RECOMMENDATIONS
//

// Rec. 8 : message# : 5500
// Spec : Use appropriate loop structure.

// Rec. 9 : message# : 5501
// Spec : No use of the "?:" operator.
if ( op_cond )
{
    WARN( 5501,"No use of the '?:' operator" );
}

// Rec. 10 : message# : 5502
// Spec : No '=' or ';' in #define
if ( pp_assign || pp_semicolon )
{
    WARN( 5502, "No '=' or  ';' in  '#define' statements" );
}

// Rec 11 : message# : 5503
// Spec : Rather use "const" or "enum" than "#define"
if ( pp_const )
{
    WARN( 5503, "Use const or enum other than macro." );
}

// Rec 12 : message# : 5504
// Spec : No magic numbers, use constant instead of.
if ( lex_not_manifest )
{
    if ( !lex_initializer )
    {
        WARN( 5504, "No Magic Numbers - use constants instead" );
    }
}

// Rec. 13 : message# : 5505
// Spec : Always pre-initialize variables - bou not extern vars.
if ( dcl_variable )
{
   if ( !dcl_parameter )
   {
       if ( ( dcl_extern && dcl_initializer ) || ( !dcl_extern && !dcl_initializer ) )
       {
           WARN( 5505, "Always pre-initialise variables - but not extern vars" );
       }
   }
}

// Rec. 14 : message# : 5506
// Spec : Avoid pointers to pointers.
if ( dcl_variable )
{
    if ( dcl_levels == 2 )
    {
        if ( dcl_level( 0 ) == POINTER && dcl_level( 1 ) == POINTER )
        {
            WARN( 5506, "Avoid pointers to pointers." );
        }
    }
}

// Rec. 15 : message# : 5507
// Spec : Use typedef when declaring struct.
// Comment : 
if ( dcl_tag_def )
{
    if ( !dcl_typedef )
    {
        if ( dcl_tag_def )
        {
            if ( dcl_base == STRUCT_TYPE )
            {
                fprintf( fd, "#struct_define %s!%s#%d\n", dcl_base_name(), file_name(), lin_number ); 
            }
        }
    }
}

if ( dcl_typedef )
{
    if ( dcl_base == STRUCT_TYPE )
    {
        fprintf( fd, "#struct_typedef %s\n", dcl_base_name() );
    }
}

// Rec. 16 : message# : 5508
// Spec : Do not refer to enum members by their assigned value

// Rec. 17 : message# : 5509
// Spec : Use parentheses to clarify the order of evaluation for operators.

//////////////////////////////////////////////////////////////////
//
//    Portability
//
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
//    RULES
//

// Rule 53 : message# : 6000
// Spec : No leading white spaces before '#' symbol.
if ( pp_white_before )
{
    WARN( 6000, "No leading white spaces before '#' symbol" );
}

// Rule 54 : message# : 6001
// Spec : No leading white spaces within include file names.
if ( pp_include_white )
{
    WARN( 6001, "No leading white spaces within Include File Names" );
}

// Rule 55 : message# : 6002
// Spec : No duplicate type defintions.
if ( dcl_typedef_dup )
{
    WARN( 6002, "Duplicate Typedefs" );
}

// Rule 56 : message# : 6003
// Spec : No array, struct, union initializer.
if ( dcl_auto_init )
{
    WARN( 6003, "No Array, Structure, Union Initializer" );
}

//////////////////////////////////////////////////////////////////
//    RECOMMENDATIONS
//

// Rec. 18 : message# : 6500
// Spec : Avoid the direct use of pre-defined data types in declaration.
if ( dcl_variable || dcl_function )
{
    if ( dcl_base >= CHAR_TYPE || dcl_base <= LONG_DOUBLE_TYPE )
    {
        WARN( 6500, "Avoid the direct use of pre-defined data type in declaration." );
    }
}

// Rec. 19 : message# : 6501
// Spec : Do not assume that an int and a long have the same size.
if ( cnv_truncate )
{
    if ( strcmp( op_base_name( 1 ), "long" ) == 0 && strcmp( op_base_name( 2 ), "int" ) == 0 )
    {
        WARN( 6501, "Do not assume the an int and a long have the same size." );
    }
}

// Rec. 20 : message# : 6502
// Spec : Do not assume that an int is 32 bits long.

// Rec. 21 : message# : 6503
// Spec : Do not assume that a char is signed or unsigned.

// Rec. 22 : message# : 6504
// Spec : Always set char to unsigned if 8-bit ASCII is used.

// Rec. 23 : message# : 6505
// Spec : Do not assume pointers abd integers have the same size.

// Rec. 24 : message# : 6506
// Spec : Use explicit type conversions for signed and unsigned values.

// Rec. 25 : message# : 6507
// Spec : No embedded assembler code.
if ( lex_assembler )
{
    WARN( 6507, "No Embedded Assembler Code" );
}

// Rec. 26 : message# : 6508
// Spec : No compiler specific keywords/identfiers.
// Special compiler keywords
/// Keil C compiler
if ( keyword( "bdata" ) || keyword( "bit" ) || keyword( "far" ) || keyword( "huge" )
   || keyword( "idata" ) || keyword( "interrupt" ) || keyword( "near" )
   || keyword( "sbit" ) || keyword( "sdata" ) || keyword( "sfr" )
   || keyword( "_task_" ) || keyword( "using" ) || keyword( "xhuge" ) )
{
    WARN( 6502, "No Compiler Specific Keywords, Identifiers" );
}
// MetaWare High c/C++ compiler
if ( keyword( "_Alias" ) || keyword( "_Asm" ) || keyword( "_Noalias" ) || keyword( "Reversed_Endian" ) )
{
    WARN( 6502, "No Compiler Specific Keywords / Identifiers" );
}

// Rec. 27 : message# : 6509
// Do not depend on underflow and overflow functions.

// Rec. 28 : message# : 6510
// Avoid pointer arithmetic.
if ( op_add || op_substract )
{
    if ( op_levels( 1 ) > 0  )
}

// Rec. 29 : message# : 6511
// Use separate HW module(s).

//////////////////////////////////////////////////////////////////
//
//    Miscellaneous
//
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
//    RULES
//

// Rule 57 : message# : 7000
// The McCabe number should be <= 10
if ( fcn_end )
{
    McCabe = 1 + fcn_decisions;
    if ( fcn_decisions => 10 )
    {
        msgBuf[0] = 0;
        sprintf( msgBuf, "The McCabe number of function %s should be no greater than 10.", fcn_name() );
}


// Rule 58 : message# : 7001
// All comment/names have to be written in English.

// Rule 59 : message# : 7002
// Always document the development environment.

// Rule 60 : message# : 7003
// Always mark deviation to a rule explicitly.

//////////////////////////////////////////////////////////////////
//    RECOMMENDATIONS
//

// Rec. 30 : message# : 7500
// Spec : No use of macros.
if ( pp_arg_count || pp_empty_arglist )
{
    WARN( 7500, "No use of macros." );
}

// Rec. 31 : message# : 7501
// Spec : No octal numeric constants.
if ( lex_radix == 8 )
{
    WARN( 7501, "No Octal Numeric Constants" );
}

// Rec. 32 : message# : 7502
// No use of conditional compilation.
int condComp;
if ( lin_end )
{
    if ( lin_preprocessor )
    {
        condComp = TRUE;
        p = strchr( line(), '#' );
        while ( p[0] != ' ' && p[0] != '\t' && p[0] != 0 )
        {
            p = p + 1;
        }
        if ( p[0] != '/' )
        {
            if ( strncmp( p, "ifdef", 5 ) != 0 )
            {
                if ( strncmp( p, "ifndef", 6 ) != 0 )
                {
                    if ( strncmp( p , "elif", 4 ) != 0 )
                    {
                        if ( strncmp( p, "endif", 5 ) != 0 )
                        {
                            if ( strncmp( p, "if", 2 ) != 0 )
                            {
                                condComp = FALSE;
                            }
                        }
                    }
                }
            }
        }
        if ( condComp )
        {
            WARN( 7502, "No use of conditional compilation." );
        }
    }
}
