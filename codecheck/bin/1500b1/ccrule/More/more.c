#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE_LEN 1024
#define MAX_NAME_LEN 256
#define STRING_END   0

#ifndef NULL
#define NULL 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#define GLOBAL_LIMIT 20
#define CALL_LIMIT   10

#define FOR( x, y )    for ( x = y; x != NULL; x = x->next )

typedef struct ModRec* ModPtr;
typedef struct FcnRec* FcnPtr;
typedef struct CallRec* CallPtr;
typedef struct VarRec* VarPtr;
typedef struct StdRec* StdPtr;

struct ModRec
{
    char*  name;
    int    end;
    VarPtr vars;
    FcnPtr fcns;
    StdPtr typedefs;
    ModPtr next;
};

struct FcnRec
{
    char*   name;
    char*   module;
    CallPtr calls;
    FcnPtr  next;
};

struct CallRec
{
    char*   name;
    int     line;
    int     repeat;
    CallPtr next;
};

struct VarRec
{
    char*  name;
    VarPtr next;
};

struct StdRec
{
    char*  name;
    StdPtr next;
};

FcnPtr fcnList = NULL;
ModPtr modList = NULL;

int IndirectRecur( CallPtr, char * );
char* MakeReportName( char * );
FILE* OpenReportFile( char *, char * );

void main( int argc, char *argv[] )
{
    FILE*   fd;
    FILE*   repFile;
    int     totalGlobals,
            modCalls,
            at;
    FcnPtr  fcnNode,
            currFcn;
    CallPtr callNode,
            currCall;
    ModPtr  modNode,
            currMod;
    VarPtr  varNode,
            currVar;
    StdPtr  currStd,
            stdNode;             
    char    modName[MAX_NAME_LEN];
    char    fcnName[MAX_NAME_LEN];
    char    line[MAX_LINE_LEN];

    int     supressGeneral = FALSE,
            supressNameconv = FALSE,
            supressCStyle = FALSE,
            supressSWDesign = FALSE,
            supressLayout = FALSE,
            supressMisc = FALSE,
            supressPort = FALSE;
    int i;
            

    if ( argc < 2 )
    {
        printf( "Usage : %s -options info-file\n", argv[0] );
        exit( -1 );
    }

    for ( i = 1; i < argc - 1; i++ )
    {
        if ( argv[i][0] != '-' )
        {
            printf( "Uage : %s -options info-file\n", argv[0] );
            exit( -1 );
        }
        switch ( tolower( argv[i][1] ) )
        {
            case 'g' :
                supressGeneral = TRUE;
                break;
            case 's' :
                supressSWDesign = TRUE;
                break;
            case 'c' :
                supressCStyle = TRUE;
                break;
            case 'm' :
                supressMisc = TRUE;
                break;
            case 'n' :
                supressNameconv = TRUE;
                break;
            case 'p' :
                supressPort = TRUE;
                break;
            case 'l' :
                supressLayout = TRUE;
                break;
            default:
                break;
        }
    }

    fd = fopen( argv[argc - 1], "r" );
    if ( fd == NULL )
    {
        printf( "Can not open file %s.\n", argv[1] );
        exit( -1 );
    }

    currFcn = NULL;
    currMod = NULL;

    while ( fgets( line, MAX_LINE_LEN, fd ) )
    {
        line[ strlen( line ) - 1 ] = STRING_END;

        if ( strncmp( line, "#mod_end", 8 ) ==  0 )
        {
            modName[0] = STRING_END;
            sscanf( line + 9, "%d", &currMod->end );
            FOR ( currVar, currMod->vars )
            {
                totalGlobals++;
            }
            if ( totalGlobals > GLOBAL_LIMIT )
            {
                fprintf( repFile, "%s(%d): Warning 2002: more than 20 global variable used in this module.\n", currMod->name, currMod->end );
            }
            fclose( repFile );
        }
        else if ( strncmp( line, "#mod_begin", 10 ) == 0 )
        {
            strcpy( modName, line + 11 ); 
            modNode = (ModPtr)malloc( sizeof( struct ModRec ) );
            modNode->name = (char*)malloc( strlen( modName ) + 1 );
            strcpy( modNode->name, modName );
            modNode->vars = NULL;
            modNode->end = 0;
            modNode->fcns = NULL;
            modNode->next = NULL;
            if ( currMod == NULL )
            {
                modList = modNode;
            }
            else
            {
                currMod->next = modNode;
            }
            currMod = modNode; 
            currVar = NULL;
            currStd = NULL;
            repFile = OpenReportFile( modName, "w" );
            fprintf( repFile, "----------- BEGIN  MODULE %s ----------\n", modName );
        }
        else if ( strncmp( line, "#fcn_begin", 10 ) == 0 )
        {
            strcpy( fcnName, line + 11 ); 
            fcnNode = (FcnPtr)malloc( sizeof( struct FcnRec ) );
            fcnNode->name = (char*)malloc( strlen( fcnName ) + 1 );
            strcpy( fcnNode->name, fcnName );
            fcnNode->module = (char*)malloc( strlen( modName ) + 1 );
            strcpy( fcnNode->module, modName );
            fcnNode->calls = NULL;
            fcnNode->next = NULL;
            if ( currFcn == NULL )
            {
                fcnList = fcnNode;
                currMod->fcns = fcnNode;
            }
            else
            {
                if ( strcmp( currFcn->module, fcnNode->module ) != 0 )
                {
                    currMod->fcns = fcnNode;
                }
                currFcn->next = fcnNode;
            }
            currFcn = fcnNode;
            currCall = NULL;
        }
        else if ( strncmp( line, "#fcn_end", 8 ) == 0 )
        {
            fcnName[0] = 0;
        }
        else if ( strncmp( line, "#call", 5 ) == 0 )
        {
            int found = FALSE;

            FOR ( callNode, currFcn->calls )
            {
                if ( strcmp( callNode->name, line + 6 ) == 0 )
                {
                    found = TRUE;
                    break;
                } 
            }
            if ( found )
            {
                callNode->repeat++;
            }
            else
            {
                at = strchr( line + 6, '@' ) - (  line + 6 );
                callNode = (CallPtr)malloc( sizeof( struct CallRec ) );
                callNode->name = (char*)malloc( at + 1 );
                strncpy( callNode->name, line + 6 , at ); 
                sscanf( line + at + 7, "%d", &callNode->line );
                callNode->repeat = 1;
                callNode->next = NULL;
                if ( currCall == NULL )
                {
                    currFcn->calls = callNode;
                } else
                {
                    currCall->next = callNode;
                }
                currCall = callNode;
            }
        }
        else if ( strncmp( line, "#global_var", 11 ) == 0 )
        {
             int found = FALSE;

             FOR ( varNode, currMod->vars )
             {
                  if ( strcmp( varNode->name, line + 12 ) == 0 )
                  {
                      found = TRUE;
                      break;
                  }
             }
             if ( !found )
             {
                 varNode = (VarPtr)malloc( sizeof( struct VarRec ) );
                 varNode->name = ( char*)malloc( strlen( line + 12 ) + 1 );
                 strcpy( varNode->name, line + 12 );
                 varNode->next = NULL;
                 if ( currVar == NULL )
                 {
                     currMod->vars = varNode;
                 }
                 else
                 {
                     currVar->next = varNode;
                 }
                 currVar = varNode;
             }
        }
        else if ( strncmp( line, "#error", 6 ) == 0 )
        {
            int block = FALSE,
                msgno;
            char* ptr;

            ptr = strchr( line, ':' );
            sscanf( ptr + strlen( " Warning " ), "%d", &msgno );
            switch ( msgno / 1000 )
            {
                case 1 :
                    block = supressGeneral;
                    break;
                case 2 :
                    block = supressSWDesign;
                    break;
                case 3 :
                    block = supressNameconv;
                    break;
                case 4 :
                    block = supressLayout;
                    break;
                case 5 :
                    block = supressCStyle;
                    break;
                case 6 :
                    block = supressPort;
                    break;
                case 7 :
                    block = supressMisc;
                    break;
                default :
                    break;
            } 
                
            if ( !block )
            {
                fprintf( repFile, "%s\n", line + 7 ); 
            }
        }
        else if ( strncmp( line, "#struct_define", 14 ) == 0 )
        {
            char* ptr1;
            char* ptr2;
            int   lineno;

            ptr1 = strchr( line, '!' );
            *ptr1 = STRING_END;
            ptr2 = strchr( ptr1 + 1, '#' );
            *ptr2 = STRING_END;
            sscanf( ptr2 + 1, "%d", &lineno );
            FOR ( stdNode, currMod->typedefs )
            {
                if ( strcmp( stdNode->name, line + 15 ) == 0 )
                {
                    break;
                }
            }
            if ( stdNode == NULL )
            {
                fprintf( repFile, "%s(%d): Warning 5507: struct %s needs a typedef.\n", ptr1 + 1, lineno, line + 15 );
            }
        }
        else if ( strncmp( line, "#struct_typedef", 15 ) == 0 ) 
        {
            stdNode = (StdPtr)malloc( sizeof( struct StdRec ) );
            stdNode->name = (char*)malloc( strlen( line + 16 ) + 1 );
            strcpy( stdNode->name, line + 16 ); 
            stdNode->next = NULL;
            if ( currStd == NULL )
            {
                currMod->typedefs = stdNode;
            }
            else
            {
                currStd->next = stdNode;
            }
            currStd = stdNode;
        } 
        else
        {
            printf( "error : file %s has been modified, quit\n", argv[1] );
            exit( -1 );
        }
    }

    fclose( fd );

    FOR ( currMod, modList )
    {
        modCalls = 0;
        repFile = OpenReportFile( currMod->name, "a" );
        FOR ( currFcn, currMod->fcns )
        {
            if ( strcmp( currMod->name, currFcn->module ) != 0 )
            {
                break;
            }

            FOR ( currCall, currFcn->calls )
            {
                if ( NotLibFunction( currCall->name ) )
                {
                    modCalls++;
                }
            } 
        }
        if ( modCalls > CALL_LIMIT )
        {
            fprintf( repFile, "%s(%d): Warning 2001: More than %d global calls in this module.\n", currMod->name, currMod->end, CALL_LIMIT );
        }
        fclose( repFile );
    }

    FOR ( currFcn, fcnList )
    {
        FOR ( currCall, currFcn->calls )
        {
            if ( strcmp( currCall->name, currFcn->name ) == 0 )
            {
                /* direct recursive call */
                repFile = OpenReportFile( currFcn->module, "a" );
                fprintf( repFile, "%s(%d): Warning 2000: Call to function %s is a direct recursive call.\n", currFcn->module, currCall->line, currCall->name );
                fclose( repFile );
            }
            else
            {
                if ( IndirectRecur( currCall, currFcn->name ) )
                {
                    repFile = OpenReportFile( currFcn->module, "a" );
                    fprintf( repFile, "%s(%d): Warning 2000: Call to function %s a indirect recursive call.\n", currFcn->module, currCall->line, currCall->name );
                    fclose( repFile );
                }
            }
        }
    }

    FOR ( currMod, modList )
    {
        repFile = OpenReportFile( currMod->name, "a" );
        fprintf( repFile, "---------- END MODULE %s ----------\n", currMod->name );
        fclose( repFile );
    }
}

int IndirectRecur( CallPtr start, char *name )
{
    FcnPtr fcn;
    CallPtr call;

    FOR ( fcn, fcnList )
    {
        if ( strcmp( start->name, fcn->name ) == 0 && strcmp( name, fcn->name ) != 0 )
        {
            break;
        }
    }

    if ( fcn == NULL )
    {
        return FALSE;
    }

    FOR ( call, fcn->calls )
    {
        if ( strcmp( call->name, name ) == 0 )
        {
            return TRUE; 
        }
        else
        {
            if ( strcmp( call->name, fcn->name ) == 0 )
            {
                continue;
            }
            return IndirectRecur( call, name );
        }
    }

    return FALSE;
}

char* MakeReportName( char* mName )
{
    char* result;
    char* dot;

    dot = strchr( mName, '.' );
    result = (char*)malloc( dot - mName + 5 );
    memset( result, 0, dot - mName + 5 );
    strncpy( result, mName, dot -mName );
    strcat( result, ".chk" );

    return result;
}

FILE* OpenReportFile( char* mName, char* mode )
{
    FILE* rFile;
    char* rName;

    rName = MakeReportName( mName );
    rFile = fopen( rName, mode );

    if ( rFile == NULL )
    {
        printf( "Can not open report file %s, quit.\n", rName );
        free( rName );
        exit( -1 );
    }
    free( rName );
    return rFile;
}

int NotLibFunction( char *fName )
{
    FcnPtr fcn;
    
    FOR ( fcn, fcnList )
    {
        if ( strcmp( fcn->name, fName ) == 0 )
        {
            return TRUE;
        }
    }

    return FALSE;
}
