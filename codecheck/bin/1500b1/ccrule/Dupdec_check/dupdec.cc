/*
********************************************************************

    this rule file extracts information about function declaration
    and defintion and stroe the information into an output data.
    the generated output file is used as input file later for 
    program dupdec.

********************************************************************
*/
#include <check.cch>

FILE *fd;

if ( prj_begin )
{
    if ( option( 'X' ) )
    {
        fd = fopen( str_option( 'X' ), "w" ) ;
    } 
    else
    {
        fd = fopen( "dupdec", "w" );
    }
}


if ( prj_end )
{
    fclose( fd );
}

if ( mod_begin )
{
    fprintf( fd, "#mod-begin %s\n", mod_name() );
}

if ( mod_end )
{
    fprintf( fd, "#mod-end %s\n", mod_name() );
}

if ( dcl_function )
{
    if ( dcl_function )
    {
        fprintf( fd, "$" ); 
        if ( dcl_definition )
        {
            fprintf( fd, "!");
        }
        else
        {
            fprintf( fd, "@" );
        }

        if ( dcl_static )
        {
            fprintf( fd, "s" );
        }
        else
        {
            if ( dcl_extern )
            {
                fprintf( fd, "e" );
            }
            else
            {
                fprintf( fd, "g" );
            }
        }

        fprintf( fd, "+%s*%s*%d\n", dcl_name(), file_name(), lin_number ); 
    }
}
