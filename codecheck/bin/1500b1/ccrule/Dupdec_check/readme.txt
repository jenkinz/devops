The files in this directory( dupdec.cc and dupdec.c ) can be used to find duplicated function declaration and definition project-wide. For declaration, if a function is declared more than once within a module, a warning message with be given. If a function is defined more than once anywhere in the project, a warning message will be given.

It takes two steps to detect the duplicated declarations and definitions.
1. Run Codecheck on the source files with rule file dupdec.cc used. As the result, the informationextracted by this rule file will be stored in a output file. The output file has name dupdec.inf by default. You can give it the name other than this via command option -x.
2. Then the generated the output file is subjected to program dupdec as input. The result of this program will be output on stdout. Program dupdec is in the source file dupdec.c. You need to have an ANSI C conforming C compiler to build program dupdec from dupdec.c.

The step 2 can be integrated into step 1 by invoking program dupdec via Codecheck function exec().

Currently they only work with C code.

Your comments are welcome. Please send you comments to Abraxas technical support at support@abxsoft.com

