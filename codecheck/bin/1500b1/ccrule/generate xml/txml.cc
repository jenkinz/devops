// test xml generation

#include "check.cch"
#include "xml.cc"

//char tmpbuf[1024];

if ( dcl_local ) {	// verify that ALL local's use Hungarian Prefixing.


	if ( dcl_Hungarian == 0 ) {

	  VIOLATION( "Naming Convention", "Declaration Name is not hungarian")
	}
}

if ( stm_kind) {		// verify that if-statement had a curly brace

 if ( stm_kind == IF ) {

	if ( strcmp( prev_token(), "}" ) != 0 ) 
	  VIOLATION( "Layout Problem", "If Statement Must have curly brace" )
	}
}

if ( dcl_function ) {	// method can't use leading _ as char
warn(0, "DF %s", dcl_name() );

	if ( dcl_name()[0] == '_' ) {

	  sprintf( tmpbuf, "Method '%s' Doesn't match pattern [a-zA-Z]", dcl_name() );

	  VIOLATION( "Naming Convention", tmpbuf )
	}
}