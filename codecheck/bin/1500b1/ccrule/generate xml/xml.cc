// Example File for codecheck support of XML Generation
// CodeCheck (c) copyright 2003 Abraxas software, Inc.


#include "xml.cch"

FILE *fp; 		// project fp
FILE *mfp; 		// module fp

char tmpbuf[BIGBUF]; 	// large temporary line out buffer
char modname[LEN];
char path[LEN];

char catbuf[BIGBUF];
char messbuf[BIGBUF];


#define SUBDIR	""	// set subdirectory pathname, was "xml"

int violation;	// user trigger fires once and clear's, only fired by VIOLATION MACRO

if ( violation ) {

	X_OPEN("Violation")		// BEGIN of violation

// ShortName
	X_OPEN("ShortName")
	X_OUT( mod_name() )
	X_CLOSE("ShortName")

// FullName
	X_OPEN("FullName")
	X_OUTARG2( "%s\\%s", file_path(), mod_name() )
	X_CLOSE("FullName" )

// line
	X_OPEN("Line")
	X_OUTARG("%d", lin_number )
	X_CLOSE("Line")

// Token
	X_OPEN("Token")
	X_OUTARG("%d", lex_token );
	X_CLOSE("Token")

// Category
	X_OPEN("Category")
	X_OUTARG("%s", catbuf );
	X_CLOSE("Category")

// Message
	X_OPEN("Mesage")
	X_OUTARG("%s", messbuf );
	X_CLOSE("Mesage")


	X_CLOSE("Violation")		// End of Violation
}

if ( prj_begin ) {
	
}

if (mod_begin)
{
  
  strcpy( modname, mod_name() );		// convert module suffix to .html
  strcpy( strchr( modname, '.' ), ".xml" );
//warn(0, "%s", modname );
  if ( strlen(SUBDIR) > 0 ) {
	sprintf( path,"%s\\%s", SUBDIR, modname );  // make subdir/mod.html
  }
  else {
	  strcpy( path, modname );
  }
 
//DEBS( path );
  mfp = fopen( path, "w" );
  if ( mfp == 0 ) fatal( -1, "file [module].xml open failure" );

 	  X_OPEN( "CodeCheck" );
}

if (mod_end)
{

//  fprintf( mfp, "<p>Module End - %s\n", mod_name() );

  X_CLOSE( "CodeCheck" );

  fclose ( mfp );  
}  

