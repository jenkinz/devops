/*
========================================================
  file name : decl_check.cc

  miscellaneous rules for desmostration on write rules.
  
  Copyright (C) 1996 by Abraxas Sofware, Inc.

========================================================
*/
#include <check.cch>

/*
  this rule checks the length of a line
*/

int seeAsm;

if ( lex_assembler )
{
    seeAsm = 1;
}

if ( lin_end )
{
   if ( seeAsm )
   {
       if ( strlen(line()) > 132 )
       {
           warn(101,"Length of line containing assembler code should not exceed 132.");
       }
   }
   else
   {
       if ( strlen(line()) > 80 )
       {
           warn(102,"Length of line should not exceed 80.");
       }
   }
   seeAsm = 0;
} 
/*
  this rule checks if a typedef name is int upper cases.
*/
if ( dcl_typedef )
{
    if ( !dcl_all_upper )
    {
        warn(201,"Typedef name should not have any lower case letter.");
    }
}
if ( op_open_funargs )
{
    if ( !op_white_before )
    {
        warn(301,"A white space should be put before open paranthesis.");
    }
}

if ( op_separator )
{
    if ( !op_white_after )
    {
        warn(302,"A white space should be put after comma.");
    }
}
if ( idn_function )
{
    if ( strcmp( idn_name(), fcn_name() ) == 0 )
    {
        warn(401,"Recursive call %s", idn_name() );
    }
}
if ( dcl_function )
{
    if ( dcl_oldstyle )
    {
        warn(501,"This function need parameter list.");
    }
}
        
if ( dcl_variable )
{
    if ( !dcl_member&&!dcl_local )
    {
        if ( dcl_definition )
        {
            if ( !dcl_initializer )
            {
                warn(601,"This variable needs initialization.");
            }
        }
    }
}
/*
  this rule checks if <TAB> is used in line indentation or in
  literals.
*/

if ( lex_constant == CONST_CHAR || lex_constant == CONST_STRING )
{
    if ( strstr( token(), "\t" ) )
    {
        warn( 701, "Do not use hard coded <TAB>" );
    }
}

if ( lin_end )
{
    if ( lin_indent_tab )
    {
        warn( 702, "<TAB> is used as indentation here." );
    }
}
/*
  this rule checks the name convention for global variables.
*/

int k;

if ( dcl_variable )
{
    if ( dcl_global )
    {
        if ( !prefix( "g" ) )
        {
            warn( 801, "Prefix g missing for global." );
        }
        else
        {
            k=0;
            while ( k < dcl_levels )
            {
                switch ( dcl_level( k ) )
                {
                    case ARRAY :
                        if ( !prefix( "a" ) )
                        {
                            warn(802,"Prefix a missing for array.");
                        }
                        break;
                    case POINTER :
                        switch ( dcl_level_flags( k ) )
                        {
                            case FAR_FLAG :
                                if ( !prefix( "f" ) )
                                {
                                    warn(803,"Prefix f missing for far flag.");
                                }
                                break;
                            case HUGE_FLAG :
                                if ( !prefix( "h" ) )
                                {
                                    warn(804,"Prefix h missing for huge flag.");
                                }
                        } 
                        if ( !prefix( "p" ) )
                        {
                            warn(805,"Prefix p missing for pointer." );
                        }
                        break;
                }
                k++;
            }

            switch ( dcl_base )
            {
                case CHAR_TYPE :
                    if ( !prefix( "c" ) )
                    {
                        warn(806,"Prefix c missing for char");
                    }
                    break;
                case SHORT_TYPE :
                    if ( !prefix( "n" ) )
                    {
                        warn(807,"Prefix n missing for short.");
                    }
                    break;
                case INT_TYPE :
                    if ( !prefix( "i" ) )
                    {
                        warn(808,"Prefix i missing for int.");
                    }
                    break;
                case LONG_TYPE :
                    if ( !prefix( "l" ) )
                    {
                        warn(809,"Prefix l missing for long.");
                    }
                    break;
                case FLOAT_TYPE :
                    if ( !prefix( "f" ) )
                    {
                        warn(810,"Prefix f missing for float.");
                    }
                    break;
                case DOUBLE_TYPE :
                    if ( !prefix( "d" ) )
                    {
                        warn(811,"Prefix d missing for double.");
                    }
                    break;
                case LONG_DOUBLE_TYPE :
                    if ( !prefix( "ld" ) )
                    {
                        warn(812,"Prefix ld missing for long double.");
                    }
                    break;
                case DEFINED_TYPE :
                    if ( strcmp( dcl_base_name(), "BOOLEAN" ) == 0 )
                    {
                        if ( !prefix( "b" ) )
                        {
                            warn(813,"Prefix b missing for BOOLEAN");
                        }
                    }
                    if ( strcmp( dcl_base_name(), "STRING" ) == 0 )
                    {
                        if ( !prefix( "s" ) )
                        {
                            warn(814,"Prefix s missing for STRING.");
                        }
                    }
                    if ( strcmp( dcl_base_name(), "HANDLE" ) == 0 )
                    {
                        if ( !prefix( "h" ) )
                        {
                            warn(815,"Prefix h missing for HANDLE");
                        }
                    }
            }
        } 
    }
}
