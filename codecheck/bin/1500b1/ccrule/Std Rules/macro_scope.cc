// this rule checks if a macro is defined within a function body or not
int inFunc;
char* p;
char name[64];
int i;

if (fcn_begin)
    inFunc=1;

if ( lin_end )
    if ( lin_preprocessor)
    {
        p = strstr( line(), "#" );
        if ( p )
        {
            p = p + 1;
            while ( p[0] == ' '|| p[0] == '\t' )
            {
                p++;
            }
            if ( strncmp( p, "define", 6 ) == 0 )
            {
                p = p + 6;
                while ( p[0] == ' ' || p[0] == '\t' )
                {
                    p = p + 1;
                }
                // assume that macro name is at the same line as #define
                i = 0;
                while ( p[0] != ' ' && p[0] != '\t' && p[0] != '(' )
                {
                    name[i] = p[0];
                    i++;
                    p = p + 1; 
                }
                name[i] = 0;
                if ( inFunc )
                {
                    printf("%s,%d,LOCAL,%s,MACRO,%s,\n",file_name(),lin_number,fcn_name(),name);
                }
                else
                {
                    printf("%s,%d,GLOBAL,%s,MACRO,%s,\n",file_name(),lin_number,mod_name(),name);
                }
            } 
        }     
    }


if (fcn_end)
{
    inFunc=0;
}
