/*
  this rule first loads a list of function names compiled
  in a file. When a function is called, its name is compared
  against the names in the list. If this called function name
  happens to being the list, it is warned that this function
  can not be called in source code. the file stores the list
  by default is called fn_names.lst, you can change it by
  changing the macro definition of DEFAULT_LIST_FILE. and
  also you can specify a file by option -V.
  and you have to put the one name per line.
*/

#define DEFAULT_LIST_FILE "fn_names.lst"

FILE *fd;
char s1[128];
char s2[128];
char s[1024];
char *p;

// load the list file into memory
if ( prj_begin )
{
    if ( option( 'V' ) )
    {
        fd = fopen( str_option( 'V' ), "r" );
        if ( ! fd )
        {
            printf("Could not open file %s.\n",str_option( 'V' ) );
            exit( 1 );
        }
    }
    else
    {
        fd = fopen( DEFAULT_LIST_FILE, "r" );
        if ( ! fd )
        {
            printf("Could not open file %s.\n", DEFAULT_LIST_FILE );
            exit( 1 );
        }
    }
    s[0] = 0;
    p = s;
    // since scanf() does not warn the last line, we will have to
    // find a way the say if the line is the last line. it is lucky
    // that we do not have a duplicated name in the list.
    s2[ 0 ] = 0;
    s1[ 0 ] = 0;
    fscanf( fd, "%s", s1 );
    while ( strcmp( s1, s2 ) != 0 )
    {
        strcpy( s2, s1 );
        // since we only can have one dimensional array, this is best
        // solution now to store all strings into one bigger array.
        strcpy( p, s1 );
        p = p + strlen( s1 ) + 1;
        fscanf( fd, "%s", s1 );    
    }
    fclose( fd );
}

if ( idn_function )
{
    p = s;
    while ( p[0] != 0 )
    {
        if ( strcmp( p, idn_name() ) == 0 )
        {
            warn( 1001,"function %s can be not be called.", idn_name() );
            break;
        }
        else
        {
            p = p + strlen( p ) + 1;
        }
    }
}
