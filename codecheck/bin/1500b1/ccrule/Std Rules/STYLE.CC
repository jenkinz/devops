/*
 *	These CodeCheck rules implement many of the fundamental
 *	C style recommendations published by Greg Comeau. Each
 *  rule in this file is preceded by a relevent quote from
 *  Comeau's recent article on C style.
 *
 *  These rules may serve as a basis for customizing your
 *  own standards for C code style.
 *
 *  Reference: "Fundamental Recommendations on C Programming
 *  Style", by Greg Comeau. Microsoft Systems Journal, vol 5,
 *  number 3, May 1990, pages 67-70.
 *
 *  Copyright (c) 1990-93 by Abraxas Software, Inc.
 *
 *  These warnings are intended to be seen in the context of
 *  a listing file. Use:  check -Rstyle.cc -L myproject.prj
 *  To obtain a file of prototypes, include option -T.
 */


// ++++++++++  SECTION 1: Macros


/**********************************************************************
"Macro names should always be written in uppercase. This draws attention
to them in code... There is usually no good reason to break this rule."
************************************************************************/
if ( pp_lowercase )
	warn( 101, "Use all UPPERCASE letters for macro names." );


/**********************************************************************
"Make sure a macro maps into what you expect it to... with the proper
use of parentheses." Note - the following rule is triggered by a macro
definition whenever a macro parameter is not enclosed in parentheses.
************************************************************************/
if ( pp_arg_paren )
	warn( 102, "Enclose macro parameters in parentheses." );


/**********************************************************************
"Some compilers provide command-line options that allow you to examine
the output of the preprocessor." Note - the following rule causes Code-
Check to show all macro expansions in the listing file.
************************************************************************/
if ( mod_begin )
	set_option( 'M', 1 );


/**********************************************************************
"Do not end macros with semicolons."
************************************************************************/
if ( pp_semicolon )
	warn( 104, "Do not end macros with semicolons." );


/**********************************************************************
"You should be aware of side effects in macro invocations."
************************************************************************/
if ( pp_arg_multiple )
	warn( 105, "This macro may cause unwanted side effects." );


/**********************************************************************
"Using #define directives liberally can make your program more readable
and easier to change." Note - Comeau is referring to 'magic numbers'.
************************************************************************/
if ( lex_not_manifest )
	if ( ! lex_initializer )
		warn( 106, "Encode this magic number as a macro." );



// ++++++++++  SECTION 2: Declarations


/**********************************************************************
"If your compiler accepts function prototypes, use them."
************************************************************************/
if ( dcl_oldstyle )
	warn( 201, "Always use prototyped function declarations." );
if ( idn_no_prototype )
	warn( 202, "No prototype is in scope for function %s.", idn_name() );


/**********************************************************************
"Including identifier names in prototype declarations is helpful"
************************************************************************/
if ( dcl_parameter )
	if ( dcl_abstract )
		warn( 203, "A parameter name would help here." );


/**********************************************************************
"If a declaration refers to an external function or data, add the extern
keyword to it."  Note: static and extern cannot be mixed.
************************************************************************/
if ( dcl_global )
	if ( dcl_function )
		{
		if ( ! dcl_definition && ! dcl_extern && ! dcl_static )
			warn( 204, "Specify either extern or static here." );
		}
	else	/*	data	*/
		{
		if ( ! dcl_initializer && ! dcl_extern && ! dcl_static )
			warn( 204, "Specify either extern or static here." );
		}

/**********************************************************************
"Placing more than one declaration on a line is a potentially serious
problem."
************************************************************************/
if ( lin_dcl_count > 1 )
	warn( 205, "Do not declare more than one identifier per line." );



// ++++++++++  SECTION 3: Functions


/**********************************************************************
"Although C does not require you to code the return type of a function,
not doing so is bad style."
************************************************************************/
if ( dcl_no_specifier )
	if ( dcl_function )
		warn( 301, "Always specify the function return type." );


/**********************************************************************
"Be sure that any functions that return values do not ... issue a return
statement without an expression."
************************************************************************/
if ( stm_return_void )
	warn( 302, "A return value conflicts with the return type." );



// ++++++++++  SECTION 4: Include Files


/**********************************************************************
"It is ... not a good idea to define or initialize identifiers in a
header file."
************************************************************************/
if ( dcl_definition )
	if ( ! lin_source )
		warn( 401, "Do not define identifiers in header files." );

if ( dcl_initializer )
	if ( ! lin_source )
		warn( 402, "Do not initialize identifiers in header files." );



// ++++++++++  SECTION 5: General


/**********************************************************************
"Nesting comments ... is common."
************************************************************************/
if ( lex_nested_comment )
	warn( 501, "Nested comments are not portable." );
