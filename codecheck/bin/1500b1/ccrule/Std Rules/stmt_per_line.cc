/*==================================================================== 
 This rule checks if only there are more than one statement in one
 line.

 Copyright (C) 1996 by ABRAXAS SOFTWARE INC.
====================================================================*/
#include <check.cch>
int stmNum;
int curLine;
int flag;

if ( stm_end )
{
    if ( curLine != lin_number )
    {
        curLine = lin_number;
        flag = 0;
        stmNum = 0;
    }

    if ( stm_kind >= COMPOUND )
    {
        if ( stmNum == 0 )
        {
            stmNum++;
        }
        else
        {
            if ( stmNum == 1 )
            {
                flag = 1;
            }
            else
            {
                flag = 0;
            }
            stmNum++;
        }
    }
}

if ( flag )
{
    warn(1001,"more than one statement in one line.");
}