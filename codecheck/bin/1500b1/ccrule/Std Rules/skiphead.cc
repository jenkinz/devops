///////////////////////////////////////////////////////////////
//  File Skiphead.cc
//  Let you force rules not to be applied on a set of specified
//  header files. For exmaple, header1.h header2.h here.

int option_s;

if (prj_begin)
{
    option_s= option('s');
}

if (header_name())
{
    if(!strcmp(header_name(), "header1.h")||!strcmp(header_name(),"header2.h")
    {
       set_option('s', 0);
    }
    else
    {
        set_option('s', option_s);
    }
}
