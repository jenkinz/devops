///////////////////////////////////////////////////////////
//   File use_closed.cc
//   Rule detects dangerous use of a file and it is closed.
//
#include <check.cch>

int i;
int inClose;
int found;
int fileType;
char *p;
char closed[2048];

if (prj_begin)
{
    if (option('V'))
    {
        if (strcmp(str_option('V'),"FILE")==0)
        {
            fileType=1;
        }
    }
}

if (mod_begin)
{
    i=0;
    while (i<256)
    {
        closed[i]=0;
        i=i+1; 
    }
}

if (idn_function)
{
    if (strcmp(idn_name(),"fclose")==0)
    {
        inClose=1;
    }
}

if (idn_variable)
{
    if (fileType)
    {
        if (idn_base==DEFINED_TYPE&&strcmp(idn_base_name(),"FILE")==0)
        {
            p=closed;
            found=0;
            while (!found)
            {
                if (closed[0]==0)
                {
                    if (inClose)
                    {
                        i=0;
                        while (i<=strlen(idn_name()))
                        {
                            closed[i]=idn_name()[i];
                            i++;
                        }
                    }
                    break;
                }
                if (strcmp(idn_name(),p)==0)
                {
                    if (!inClose)
                    {
                        found=1;
                        warn(1001,"file has been closed, do not use %s until it is reopened.",idn_name());
                    }
                    break;
                }
                else
                {
                    p=p+strlen(p)+1;
                    if (p[0]==0)
                    {
                        if (inClose)
                        {
                            i=0;
                            while (i<=strlen(idn_name()))
                            {
                                p[i]=idn_name()[i];
                                i++;
                            }
                        }
                        break;
                    }
                    else
                        continue;
                }
            }
        }
    }
    else
    {
        if (idn_base==STRUCT_TYPE&&strcmp(idn_base_name(),"_iobuf")==0)
        {
            p=closed;
            found=0;
            while (!found)
            {
                if (closed[0]==0)
                {

                    if (inClose)
                    {
                        i=0;
                        while (i<=strlen(idn_name()))
                        {
                            closed[i]=idn_name()[i];
                            i++;
                        }
                    }
                    break;
                }
                if (strcmp(idn_name(),p)==0)
                {
                    if (!inClose)
                    {
                        found=1;
                        warn(1001,"file has been closed, do not use %s until it is reopened.",idn_name());
                    }
                    break;
                }
                else
                {
                    p=p+strlen(p)+1;
                    if (p[0]==0)
                    {
                        if (inClose)
                        {
                            i=0;
                            while (i<=strlen(idn_name()))
                            {
                                p[i]=idn_name()[i];
                                i++;
                            }
                        }
                        break;
                    }
                    else
                        continue;
                }
            }
        }
    }
}
           

if (op_close_funargs)
{
    if (inClose)
    {
        inClose=0;
    }
}
