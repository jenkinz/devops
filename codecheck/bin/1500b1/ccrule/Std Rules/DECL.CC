//////////////////////////////////////////////////////////
// CopyRight (C) 1995, Abraxas Software, Inc.
///////////////////////////////////////////////////
// This rule to force only one declarator allowed 
// each line.

int linParamDecl;

if (dcl_parameter)
{
  linParamDecl++;
}

if (lin_end)
{
  if (lin_dcl_count-linParamDecl>1)
  {
    warn(100,"no more than one decl allowed.");
  }
  linParamDecl=0;
}
