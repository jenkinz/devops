/*
******************************************************************

    this rule file checks the contents of the leading comment
    block of a source file. this rule file requires all four
    sections in comment block present. The four sections are
    labeled by DESCRIPTION:, DEVELOPER:, HISTORY: and NOTE:.
    Besides their presence, they are also required to have
    meaningful contents following the labels which means there
    must be any character above 'A'. Also, the rule requires
    all four sections in strict order as stated above. Otherwise
    a warning message with be given. 

******************************************************************
*/
#include <check.cch>

int desc_field_found,
    desc_field_ok,
    in_desc_field,
    dev_field_found,
    dev_field_ok,
    in_dev_field,
    hist_field_found,
    hist_field_ok,
    in_hist_field,
    note_field_found,
    note_field_ok,
    in_note_field,
    title_line,
    do_check;

char* p,
      r;

if ( mod_begin )
{
    desc_field_found = FALSE;
    desc_field_ok = FALSE;
    in_desc_field = FALSE;
    dev_field_found = FALSE;
    dev_field_ok = FALSE;
    in_dev_field = FALSE;
    hist_field_found = FALSE;
    hist_field_ok = FALSE;
    in_hist_field = FALSE;
    note_field_found = FALSE;
    note_field_ok = FALSE;
    in_note_field = FALSE;
    do_check = TRUE;
}

if ( lin_end && !lin_header )
{
    r = line();
    if ( lin_has_comment && !lin_has_code )
    {
        if ( do_check )
        {
            if ( strstr( r, "DESCRIPTION:" ) != 0 && !desc_field_found )
            {
                if ( dev_field_found )
                {
                    warn( 1001, "violation of comment block rule, DEVELOPER field before DESCRIPTION field" );
                    if ( in_dev_field )
                    {
                        if ( !dev_field_ok )
                        {
                            warn( 1008, "violation of comment block rule, empty DEVELOPER field" );
                        }
                        in_dev_field = FALSE;
                    }
                }
                if ( hist_field_found )
                {
                    warn( 1002, "violation of comment block rule, HISTORY field before DESCRIPTION field" );
                    if ( in_hist_field )
                    {
                        if ( !hist_field_ok )
                        {
                            warn( 1009, "violation of comment block rule, empty HISTORY field" );
                        }
                        in_hist_field = FALSE;
                    }
                }
                if ( note_field_found )
                {
                    warn( 1003, "violation of comment block rule, NOTE field before DESCRIPTION field" );
                    if ( in_note_field )
                    {
                        if ( !note_field_ok )
                        {
                            warn( 1010, "violation of comment block rule, empty NOTE field" );
                        }
                        in_note_field = FALSE;
                    }
                }
                in_desc_field = TRUE;
                desc_field_found = TRUE;
                title_line = lin_number;
                p = strstr( r, "DESCRIPTION:" ) + strlen( "DESCRIPTION:" );
                while ( p[0] != 0 )
                {
                    if ( p[0] >= 'A' )
                    {
                        desc_field_ok = TRUE;
                        break;
                    }
                    p = p + 1;
                }
            }

            if ( strstr( r, "DEVELOPER:" ) != 0 && !dev_field_found )
            {
                if ( desc_field_found )
                {
                    if ( in_desc_field )
                    {
                        if ( !desc_field_ok )
                        {
                            warn( 1007, "violation of comment block rule, empty DESCRIPTION field" );
                        }
                        in_desc_field = FALSE;
                    }
                }
                if ( hist_field_found )
                {
                    warn( 1004, "violation of comment block rule, HISTORY field before DEVELOPER field" );
                    if ( in_hist_field )
                    {
                        if ( !hist_field_ok )
                        {
                            warn( 1009, "violation of comment block rule, empty HISTORY field" );
                        }
                        in_hist_field = FALSE;
                    }
                }
                if ( note_field_found )
                {
                    warn( 1005, "violation of comment block rule, NOTE field before DEVELOPER field" );
                    if ( in_note_field )
                    {
                        if ( !note_field_ok )
                        {
                            warn( 1010, "violation of comment block rule, empty NOTE field" );
                        }
                        in_note_field = FALSE;
                    }
                }

                in_dev_field = TRUE;
                dev_field_found = TRUE;
                title_line = lin_number;
                p = strstr( r, "DEVELOPER:" ) + strlen( "DEVELOPER:" );
                while ( p[0] != 0 )
                {
                    if ( p[0] >= 'A' )
                    {
                        dev_field_ok = TRUE;
                        break;
                    }
                    p = p + 1;
                }
            }

            if ( strstr( r, "HISTORY:" ) != 0 && !hist_field_found )
            {
                if ( desc_field_found )
                {
                    if ( in_desc_field )
                    {
                        if ( !desc_field_ok )
                        {
                            warn( 1007, "violation of comment block rule, empty DESCRIPTION field" );
                        }
                        in_desc_field = FALSE;
                    }
                }
                if ( dev_field_found )
                {
                    if ( in_dev_field )
                    {
                        if ( !dev_field_ok )
                        {
                            warn( 1008, "violation of comment block rule, empty DEVELOPER field" );
                        }
                        in_dev_field = FALSE;
                    }
                }
                if ( note_field_found )
                {
                    warn( 1006, "violation of comment block rule, NOTE field before HISTORY field" );
                    if ( in_note_field )
                    {
                        if ( !note_field_ok )
                        {
                            warn( 1010, "violation of comment block rule, empty NOTE field" );
                        }
                        in_note_field = FALSE;
                    }
                }

                in_hist_field = TRUE;
                hist_field_found = TRUE;
                title_line = lin_number;
                p = strstr( r, "HISTORY:" ) + strlen( "HISTORY:" );
                while ( p[0] != 0 )
                {
                    if ( p[0] >= 'A' )
                    {
                        hist_field_ok = TRUE;
                        break;
                    }
                    p = p + 1;
                }
            }
            if ( strstr( r, "NOTE:" ) != 0 && !note_field_found )
            {
                if ( desc_field_found )
                {
                    if ( in_desc_field )
                    {
                        if ( !desc_field_ok )
                        {
                            warn( 1007, "violation of comment block rule, empty DESCRIPTION field" );
                        }
                        in_desc_field = FALSE;
                    }
                }
                if ( dev_field_found )
                {
                    if ( in_dev_field )
                    {
                        if ( !dev_field_ok )
                        {
                            warn( 1008, "violation of comment block rule, empty DEVELOPER field" );
                        }
                        in_dev_field = FALSE;
                    }
                }
                if ( hist_field_found )
                {
                    if ( in_hist_field )
                    {
                        if ( !hist_field_ok )
                        {
                            warn( 1009, "violation of comment block rule, empty HISTORY field" );
                        }
                        in_hist_field = FALSE;
                    }
                }

                in_note_field = TRUE;
                note_field_found = TRUE;
                title_line = lin_number;
                p = strstr( r, "NOTE:" ) + strlen( "NOTE:" );
                while ( p[0] != 0 )
                {
                    if ( p[0] >= 'A' )
                    {
                        note_field_ok = TRUE;
                        break;
                    }
                    p = p + 1;
                }
            }

            if ( in_desc_field && !desc_field_ok && title_line != lin_number)
            {
                p = r;
                while ( p[0] != 0 )
                {
                    if ( p[0] >= 'A' )
                    {
                        desc_field_ok = TRUE;
                        break;
                    }
                    p = p + 1;
                }
            }

            if ( in_dev_field && !dev_field_ok && title_line != lin_number )
            {
                p = r;
                while ( p[0] != 0 )
                {
                    if ( p[0] >= 'A' )
                    {
                        dev_field_ok = TRUE;
                        break;
                    }
                    p = p + 1;
                }
            }

            if ( in_hist_field && !hist_field_ok && title_line != lin_number )
            {
                p = r;
                while ( p[0] != 0 )
                {
                    if ( p[0] >= 'A' )
                    {
                        hist_field_ok = TRUE;
                        break;
                    }
                    p = p + 1;
                }
            }

            if ( in_note_field && !note_field_ok && title_line != lin_number )
            {
                p = r;
                while ( p[0] != 0 )
                {
                    if ( p[0] >= 'A' )
                    {
                        note_field_ok = TRUE;
                        break;
                    }
                    p = p + 1;
                }
            }
        }
    }

    if ( lin_has_code || lin_preprocessor )
    {
        if ( do_check )
        {
            if ( in_desc_field )
            {
                if ( !desc_field_ok )
                {
                    warn( 1007, "violation of comment block rule, empty DESCRIPTION field" );
                }
            }
            if ( in_dev_field )
            {
                if ( !dev_field_ok )
                {
                    warn( 1008, "violation of comment block rule, empty DEVELOPER field" );
                }
            }
            if ( in_hist_field )
            {
                if ( !hist_field_ok )
                {
                    warn( 1009, "violation of comment block rule, empty HISTORY field" );
                }
            }
            if ( in_note_field )
            {
                if ( !note_field_ok )
                {
                    warn( 1010, "violation of comment block rule, empty NOTE field" );
                }
            }
            do_check = FALSE;
        }
    }
}

if ( mod_end )
{
    if ( !desc_field_found )
    {
        warn( 1011, "violation of comment block rule, no DESCRIPTION field" );
    }
    if ( !dev_field_found )
    {
        warn( 1012, "violation of comment block rule, no DEVELOPER field" );
    }
    if ( !hist_field_found )
    {
        warn( 1013, "violation of comment block rule, no HISTORY field" );
    }
    if ( !note_field_found )
    {
        warn( 1014, "violation of comment block rule, no NOTE field" );
    }
}
