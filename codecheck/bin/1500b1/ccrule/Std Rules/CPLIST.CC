//	Copyright (c) 1992-93 by Abraxas Software.

//	This rule file prints a list of all unions, structs, and classes
//	defined in each module, with the number of lines and tokens used
//	for each. These counts INCLUDE lines and tokens found in C++ member
//	functions defined outside of class definitions.

int		k, class_lines, class_tokens;

if ( mod_begin )
	{
	set_option( 'F', 1 );	//  Include header lines in counts
	}

if ( mod_end )
	{
	if ( mod_classes > 0 )
		{
		printf( "\nModule %s has %d classes:\n\n", mod_name(), mod_classes );
		printf( "Class Name            Lines  Tokens\n" );
		k = 0;
		while ( k < mod_classes )
			{
			class_lines = mod_class_lines(k);
			class_tokens = mod_class_tokens(k);
			printf( "%-22s %4d   %5d\n", mod_class_name(k), class_lines, class_tokens );
			k++;
			}
		printf( "\n" );
		}
	else
		printf( "\nModule %s has no classes.\n\n", mod_name() );
	}
