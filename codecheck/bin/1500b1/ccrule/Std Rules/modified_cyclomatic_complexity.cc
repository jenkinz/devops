/*
-----------------------------------------------------

    Copyright (c) 1997 by Abraxas Software.

    This rule calculates the modified cyclomatic
    complexity.

    Written by Shuming Tan

-----------------------------------------------------
*/

#include <check.cch>

statistic int	McCabe;
statistic float	density;
int loop_stm_count;
int cond_stm_count;
int switch_case_count;
int paren_level;
int log_op_count;
int in_condition;
int in_call;
int call_paren_level;

if ( prj_begin )
{
    printf( "\n================== %s ===================\n", prj_name() );
    printf( "\nDate: %s.\n\n", time_stamp() );
    printf( "Complexity => Modified Cyclomatic Complexity.\n" );
    printf( "Density    => Operators per executable line.\n" );
    printf( "Asterisks  => Function is too complex.\n" );
}

if ( mod_begin )
{
    printf( "\n\n------------------ %s -------------------\n\n", mod_name() );
    printf( "FUNCTION       Complexity    Density\n" );
    reset( fcn_decisions );
    reset( fcn_operators );
    reset( fcn_exec_lines );
    reset( McCabe );
    reset( density );
}

if ( fcn_end )
{
    McCabe = 1 + loop_stm_count + cond_stm_count + switch_case_count + log_op_count;
    printf( "%-16s %3d ", fcn_name(), McCabe );
    if ( McCabe >= 30 )
        printf( "*** " );
    else
        if ( McCabe >= 20 )
            printf( "**  " );
        else
            if ( McCabe >= 10 )
                printf( "*   " );
            else
                printf( "    " );

    if ( fcn_exec_lines > 0 )
        density = ( 1.0 * fcn_operators ) / fcn_exec_lines;
    else
        density = 0.0;

    printf( "%9.1f\n", density );

    loop_stm_count = 0;
    cond_stm_count = 0;
    switch_case_count = 0;
    log_op_count = 0;
}

if ( mod_end )
    if ( ncases(fcn_exec_lines) > 0 )
    {
        printf( "\nFunction Density (operators per executable line):\n" );
        printf( "  Mean:    %6.2f\n", mean(density) );
        printf( "  Std.Dev: %6.2f\n", stdev(density) );
        printf( "\nFunction Complexity (McCabe):\n" );
        printf( "  Mean:    %6.2f\n", mean(McCabe) );
        printf( "  Std.Dev: %6.2f\n", stdev(McCabe) );
        printf( "  Maximum: %6.2f\n", maximum(McCabe) );
        printf( "  Histogram:\n" );
        histogram( McCabe, 0, 20, 21 );
        printf( "\n" );
    }

if ( prj_end )
    printf( "\n\n================== END ==================\n\n" );

if ( stm_end )
{
    if ( stm_kind == IF )
    {
        cond_stm_count++;
    }
    if ( stm_kind == SWITCH )
    {
        cond_stm_count++;
        switch_case_count--;
    }
    if ( stm_kind == WHILE || stm_kind == FOR || stm_kind == DO )
    {
        loop_stm_count++;
    }
}

if ( keyword( "case" ) || keyword( "default" ) )
{
    switch_case_count++;
}

if ( keyword( "if" ) || keyword( "while" ) )
{
    in_condition = 1;
}

if ( op_open_paren )
{
    if ( in_condition )
    {
        paren_level++;
    }
}

if ( op_close_paren )
{
    if ( in_condition )
    {
        paren_level--;
        if ( in_call )
        {
            if ( call_paren_level == paren_level )
            {
                in_call = 0;
            }
        }
        if ( !paren_level )
        {
            in_condition = 0;
        }
    }
}

// the logic operators in actual paramters of a function
// which itself is an operand in condition expression are
// not counted.
if ( idn_function )
{
    if ( !in_call )    // only outmost call marked
    {
        in_call = 1;
        call_paren_level = paren_level;
    }
}

if ( op_log_and || op_log_or || op_log_not )
{
    if ( in_condition && !in_call )
    {
        log_op_count++;
    }
}