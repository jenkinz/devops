/*	braces.cc
	Copyright (c) 1993 by Abraxas Software. All rights reserved.
==============================================================================
	Purpose:	Checks for consistent use of braces.
	Author:		Loren Cobb.
	Revision:	6 Jan 1994.
	Format:		Monospaced font with 4 spaces/tab.
==============================================================================

Abstract:

	Some C and C++ style guidelines call for the use of curly braces {}
	to enclose the contents of if-, else-, for-, while-, and do-while
	statements, even though these braces are not required by the C language.
	
	Example:
	
		if ( foo > 4 )
			bar();         //  Needs to be enclosed in { }
	
	This rule file issues a warning message whenever this guideline
	is violated. It also detects switch cases that are not enclosed
	in curly braces. It does not require braces when the contents of
	an else is a high-level statement (e.g. else-if). 
	
	Each statement type has its own particular warning message. This
	is done so that the user will not be confused when statements
	contain other statements.

Warning Codes:

1000	Use {} to stand for an empty statement.
1001	Enclose   IF   contents in { }
1002	Enclose  ELSE  contents in { }
1003	Enclose WHILE  contents in { }
1004	Enclose   DO   contents in { }
1005	Enclose  FOR   contents in { }
1006	Enclose SWITCH contents in { }
1007	Enclose  CASE  contents in { }

==============================================================================
*/

#include <check.cch>

int		start;

if ( stm_container < FCN_BODY )		/*  if, else, while, do, for, switch  */
	if ( stm_kind != COMPOUND )
		{
		if ( stm_lines > 1 )
			start = lin_number - stm_lines - 1;
		else
			start = lin_number;

		if ( stm_kind == EMPTY )
			{
			warn( 1000, "Use {} to stand for an empty statement." );
			}
		else switch ( stm_container )
			{
		case IF:
			if ( stm_lines > 1 )
				warn( 1001, "Enclose IF contents in { } (about line %d)", start );
			else
				warn( 1001, "Enclose IF contents in { }" );
			break;
		case ELSE:
			switch ( stm_kind )
				{
			case IF:      /*  Do not flag the else-if construction.      */
			case ELSE:
			case WHILE:   /*  Do not flag the else-while construction.   */
			case DO:      /*  Do not flag the else-do construction.      */
			case FOR:     /*  Do not flag the else-for construction.     */
			case SWITCH:  /*  Do not flag the else-switch construction.  */
				break;
			default:
				warn( 1002, "Enclose ELSE contents in { }." );
				break;
				}
			break;
		case WHILE:
			if ( stm_lines > 1 )
				warn( 1003, "Enclose WHILE contents in { } (about line %d)", start );
			else
				warn( 1003, "Enclose WHILE contents in { }" );
			break;
		case DO:
			if ( stm_lines > 1 )
				warn( 1004, "Enclose DO contents in { } (about line %d)", start );
			else
				warn( 1004, "Enclose DO contents in { }" );
			break;
		case FOR:
			if ( stm_lines > 1 )
				warn( 1005, "Enclose FOR contents in { } (about line %d)", start );
			else
				warn( 1005, "Enclose FOR contents in { }" );
			break;
		case SWITCH:
			warn( 1006, "Enclose SWITCH contents in { }" );	/* Rare, but legal. */
			break;
			}
		}

if ( stm_cases )
	if ( stm_kind != COMPOUND )
		warn( 1007, "Enclose CASE contents in { }" );
