////////////////////////////////////////////////////////////////
//
//    rules for some portability checking.
//

/*
================================================================
#include <check.cch>

#define TRUE  1
#define FALSE 0

/*
*****************************************************************
*                                                               *
*    Do not use shift operators at all.                         *
*                                                               *
*****************************************************************
*/

if ( op_left_shift || op_left_assign || op_right_shift || op_right_assign )
{
    warn( 1001, "Avoid use shift operation." );
}

/*
******************************************************************
*                                                                *
*    Do not pass function calls to sprintf as actual arguments.  *
*                                                                *
******************************************************************
*/

int inSprintf;
int parenLevel;

if ( op_open_funargs )
{
    if ( inSprintf )
    {
        parenLevel++;
    }
}

if ( op_close_funargs )
{
    if ( inSprintf )
    {
        parenLevel--;
        if ( parenLevel == 0 )
        {
            inSprintf = 0;
        }
    }
}

if ( idn_function )
{
    if ( strcmp( idn_name(), "sprintf") == 0 )
    {
        inSprintf = TRUE;
    }
}


if ( idn_function )
{
    if ( inSprintf )
    {
        if ( parenLevel == 1 )
        {
            warn( 1002, "Do not use function call %s in sprintf", idn_name() );
        }
    }
}

/*
******************************************************************
*                                                                *
*     always make NULL cast to the proper type.                  *
*                                                                *
******************************************************************
*/

if ( macro("NULL"))
{
    if ( strcmp(prev_token(),")") !=0) 
    {
        warn( 1003, "NULL needs to be casted.");
    }
}

/*
******************************************************************
*                                                                *
*    Do not pass data of short type as file descriptor or        *
*    process id.                                                 *
*                                                                *
******************************************************************
*/

int noGood;
int i;

if ( op_call )
{
    i = 1;
    while ( i <= op_operands )
    {
        switch ( i )
        {
            case 1 :
                if ( strcmp( op_function(), "fid_func_1_1") == 0 )
                {
                    if ( op_base( i ) == SHORT_TYPE )
                    {
                        noGood = 1;
                    }
                }
                break;
           case 2 :
               if ( strcmp( op_function(), "fid_func_2_1") == 0 ) 
               {
                   if ( op_base( i ) == SHORT_TYPE )
                   {
                       noGood = 2;
                   }
               }
               break;
       }
       i++;
   } 
}

if ( noGood )
{
    warn( 1004, "short type is passed to parameter %d.", noGood );
    noGood = 0;
}


if ( op_call )
{
    i = 0;
    while ( i < op_operands )
    {
        switch ( i )
        {
            case 1 :
                if ( strcmp( op_function(), "fid_func_1_1") == 0 )
                {
                    if ( op_base( i ) == SHORT_TYPE )
                    {
                        noGood = 1;
                    }
                }
                break;
           case 2 :
               if ( strcmp( op_function(), "fid_func_2_1") == 0 )
               {
                   if ( op_base( i ) == SHORT_TYPE )
                   {
                       noGood = 2;
                   }
               }
               break;
       }       
       i++;
   }
}

/*
***********************************************************************
*                                                                     *
*    Do not apply address operator on array expression or function    *
*    name.                                                            *
*                                                                     *
***********************************************************************
*/

int seeArray;
int seeFunc;
int tokenPos;

if ( op_address )
{
    if ( seeArray )
    {
        warn( 1006, "Do not apply address operator on array expression." );
    }
    if ( seeFunc )
    {
        warn( 1007, "Do not apply address operator on function name." );
    }
    seeArray = FALSE;
    seeFunc = FALSE;
}

if ( idn_variable )
{
    i = 0;
    while ( i <= idn_levels ) 
    {
        if ( idn_level( i ) == ARRAY )
        {
            seeArray = TRUE;
            break;
        }
        if ( idn_level( i ) == FUNCTION )
        {
            seeFunc = TRUE;
            break;
        }
        i++;
    } 
}
