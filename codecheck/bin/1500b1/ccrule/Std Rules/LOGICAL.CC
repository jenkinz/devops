/* ================================================================

                 IF-STATEMENT COMPLEXITY

          Copyright (c) 1994 by Abraxas Software.

This rule file will flag any if-condition that contains more
than THRESHOLD logical connectives (operators && and ||).
You may set the macro THRESHOLD to any value that you wish.

LAST REVISED:  5/19/94
AUTHOR:        Loren Cobb
THANKS TO:     Padma Talasila
----------------------------------------------------------------- */


#define THRESHOLD	3

int	if_condition,		//  TRUE if parsing an if-condition
	logical_complexity,	//  number of logical operators used
	paren_count;		//  parenthesis nesting level
		

if ( op_if )
	{
	if_condition = 1;
	logical_complexity = 0;
	}

if ( op_log_and || op_log_or )
	++logical_complexity;

if ( op_open_paren )
	++paren_count;

if ( op_close_paren )
	{
	--paren_count;
	if ( if_condition && paren_count == 0 )
		{
		if_condition = 0;
		if ( logical_complexity > THRESHOLD )
			warn( 8000, "Simplify this if-condition!" );
		}
	}
