
//	Copyright (c) 1988-94 by Abraxas Software.

//	This is a set of CodeCheck rules for detecting silent programming errors.
//	Some compilers will catch some of these errors, but many will be missed.

#include <check.cch>

int		k;

if ( test_needed("malloc") )
	warn( 7000, "The return value of malloc MUST be checked!" );

if ( test_needed("calloc")  )
	warn( 7000, "The return value of calloc MUST be checked!" );

if ( test_needed("alloc")  )
	warn( 7000, "The return value of alloc MUST be checked!" );

if ( lex_radix == 8 )
	warn( 7001, "This number will be read in octal, not decimal." );

if ( lex_big_octal )
	warn( 7002, "Digits 8 and 9 are not octal." );

if ( lex_bad_call )
	warn( 7003, "Wrong number of arguments in this macro call." );

if ( dcl_no_specifier )
	{
	if ( dcl_function )
		warn( 7005, "Function %s: return type is missing.", dcl_name() );
	else
		warn( 7005, "Variable %s: type specifier is missing.", dcl_name() );
	}

if ( lex_char_empty )
	warn( 7006, "Empty character constant." );

if ( pp_recursive )
	warn( 7007, "Recursive macro definition." );

if ( pp_sizeof )
	warn( 7008, "Do not use \'sizeof\' in preprocessor arithmetic." );

if ( pp_semicolon )
	warn( 7009, "Macro body ends with semicolon - is this OK??" );

if ( pp_arg_multiple )
	warn( 7010, "An argument was used more than once - consider side effects." );

if ( pp_arg_paren )
	warn( 7011, "Always enclose macro arguments in parentheses." );

if ( pp_assign )
	warn( 7012, "The \"=\" sign in this macro may be an error." );

if ( pp_not_defined )
	warn( 7013, "This expression depends on an undefined macro." );

if ( lex_nested_comment )
	if ( option('N') == 0 )
		warn( 7014, "Possible unterminated comment?" );
	
if ( dcl_extern ) 
	if ( dcl_initializer )
		warn( 7015, "NEVER initialize an \'extern\' variable." );

if ( dcl_hidden )
	warn( 7016, "This declaration of %s hides another identifier.", dcl_name() );

if ( dcl_not_declared )
	warn( 7017, "Function %s has undeclared parameters.", fcn_name() );

if ( dcl_parm_hidden )
	warn( 7018, "Local identifier %s hides a function parameter.", dcl_name() );

if ( stm_goto )
	warn( 7019, "Initialization may fail on goto from outside this block." );

if ( stm_no_break )
	warn( 7020, "Previous case has no break - is this OK??" );

if ( stm_unused )
	{
	k = 0;
	while ( k < stm_unused )
		warn( 7021, "Variable %s was declared but not used.", stm_unused_name(k++) );
	}

if ( stm_semicolon )
	warn( 7022, "This semicolon may be an error." );

if ( stm_return_void )
	warn( 7023, "Return type conflicts with function type." );

if ( pp_include_white )
	warn( 7024, "Nonportable leading whitespace in filename." );

if ( dcl_static )
	if ( dcl_simple && (! dcl_initializer) )
		if ( (dcl_base >= FLOAT_TYPE) && (dcl_base <= LONG_DOUBLE_TYPE) )
			warn( 7025, "Static float variables need explicit initializers." );

if ( dcl_typedef_dup )	
	warn( 7026, "Duplicate definition of type %s.", dcl_name() );

if ( pp_unknown )
	warn( 7027, "Unknown preprocessor directive: %s", token() );

if ( pp_macro_conflict )
	warn( 7028, "This macro was defined differently in file %s; line %d",
			conflict_file(), conflict_line );

if ( dcl_conflict )
	warn( 7029, "%s was declared differently in file %s; line %d",
			dcl_name(), conflict_file(), conflict_line );

if ( idn_no_init )
	warn( 7030, "Local variable %s used before initialization.", idn_name() );

if ( dcl_no_prototype )
	if ( dcl_oldstyle )
		warn( 7031, "Function %s has no prototype in scope.", dcl_name() );

if ( tag_hidden )
	warn( 7032, "This declaration of tag %s hides another.", tag_name() );
