/*
********************************************************
 CopyRight (C) 1995, Abraxas Software, Inc.
********************************************************
*/
 
#include <stdio.h>
#include <string.h>

#ifdef __unix__
#define PATHSEP '/'    // unix style path separator, /usr/include
#else
#define PATHSEP '\\'    // DOS style, c:\msvc\include
#endif

main(int argc, char *argv[])
{
  char s[512],*p;
  FILE *fp;
  int i;

  fp=fopen(argv[1],"r");
  fscanf(fp,"%s",s);
  p=s;
  i=strlen(s);
  while (s[i-1]!=PATHSEP)
  {
    i--;
    if (i==0) break;
  } 
  if (i!=0)
  {
    s[i]=0;
  }
  else
  {
   s[0]=0;
  }
  fclose(fp);
  fp=fopen(argv[1],"w");
  fprintf(fp,"%s",s);
  fclose(fp);
}
