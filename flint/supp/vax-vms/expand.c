

/*  This is a vax-specific routine for expanding wild-card
    characters in filenames.  It was derived from information
    provided by Chris Rogers of Basel, Switzerland.
    This specific code has not yet (Dec-9-88) been tested.
 */

#include <ssdef.h>
#include <stdio.h>
#include <string.h>
#include <rms.h>
#include <descrip.h>
#include <perror.h>
#include "vers.h"
#include "custom.h"

/*  This function is intended to replace the like-named function
    in custom.c.  See the commentary preceding sys_expand in that
    file for an explaination of this function.
    Note that no error messages are produced.  If the name expands,
    fine.  If the name produces an error when an expansion is attempted,
    the no wild-card characters are reported and lint will give an
    error when it attempts to open the file.
 */
    CHARPTR
sys_expand(pattern)
    CHARPTR pattern;
    {
    static int ctxt;
    static CHARPTR name_saved;
#define MAX_FN 100
    static char patt_saved[MAX_FN+1];
    CHARPTR s;
    CHARPTR get_name();

    if( pattern )
	{
	/* we are being called to see if pattern can be expanded */
	ctxt = 0; /* is this necessary? */
	name_saved = NULL;
	strncpy( patt_saved, pattern, MAX_FN );
	s = get_name( patt_saved, &ctxt );
	if( s )
	    {
	    /* there is at least one name being matched;
	       save it for the next call to sys_expand() */
	    name_saved = s;
	    return pattern;  /* signal success */
	    }
	else
	    /* the pattern is probably not valid; let lint give message */
	    return NULL;
	}
    else
	{
	/* here to get the next name */
	if( name_saved )
	    { s = name_saved; name_saved = NULL; return s; }
	return get_name( patt_saved, &ctxt );
	}
    }


    CHARPTR
get_name( str, context )
    CHARPTR str;
    int *context;
    {
    int rtc, stvaddr, userflags, i;
    static char vmsfilename[81];
    static char vmsresultname[81];
    int LIB$FIND_FILE();

    static $DESCRIPTOR( dscfilespec, vmsfilename );
    static $DESCRIPTOR( dscresultspec, vmsresultname );
    static $DESCRIPTOR( dscdefaultspec, "" );
    static $DESCRIPTOR( dscrelatedspec, "" );

    dscfilespec.dsc$a_pointer = str;
    dscfilespec.dsc$w_length = strlen(str);
    stvaddr = 0;
    userflags = 0;
    rtc = LIB$FIND_FILE( &dscfilespec, &dscresultspec, context,
	&dscdefaultspec, &dscrelatedspec, &stvaddr, &userflags );
    if( rtc & 1 )
	{
	char ch;

	/* found another name */
	/* remove trailing blanks */
	for( i = dscresultspec.dsc$w_length - 1;
	     i > 0  &&  vmsresultname[i] == ' ';  i-- )
	    vmsresultname[i] = 0;

	/* now eliminate any trailing ';' <sequence number> */
	while( i > 0 )
	    {
	    ch = vmsresultname[i];
	    if( '0' <= ch && ch <= '9' )   i--;
	    else  /* not a digit */
		{
		if( ch == ';' ) vmsresultname[i] = 0;
		break;
		}
	    }

	return vmsresultname;
	}
    else return NULL;
    }
