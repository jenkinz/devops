
With Linux you are very likely using gcc.  Configuration for gcc
can be non-trivial owing to the variety of different versions.
We have an option that can help (-scavenge).  See the directory
'../gcc'.

There are compiler options files in this directory (co-gnu.lnt and
co-gnu3.lnt) which get you close to what you need and are provided
for backward compatibility.  Our current approach is described
in '../gcc'.

Gimpel Software, July 2008


