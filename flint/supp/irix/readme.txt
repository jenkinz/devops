

Running Flexelint on the Silicon Graphics IRIX system.

As indicated in section n-4.1 "Setup" in the Flexelint Installation
Guide, we recommend a shell script from which to run Flexelint having
the general form:


lint +vm -i/u/lint  std.lnt  $*  >temp
more temp


The -i specifies an include directory. Please modify to taste.
The file std.lnt can contain:

    co.lnt
    options.lnt
    lib-curs.lnt

File lookup of modules and .lnt files is affected by the -i option
which explains why you don't need full path names.

co.lnt and lib-curs.lnt can be found in this directory.  options.lnt
specifies your error-suppression policy.  lib-curs.lnt is useful with
the curses library and is pretty harmless if your not using curses.




