
From:  Gimpel Software

*   IMPORTANT: Several of our OPENVMS customers have reported
    experiencing a problem with the optimizer for the VAX 3.0 compiler.
    Please use the option:

	/noopt

    when compiling the source code for FlexeLint.
    Though this warning may be by now obsolete it will not penalize
    performance to any great extent.


*   The file co-vms.lnt in this directory is a suggested starting
    point for a Compiler Options file for the VAX VMS C compiler.

*   globalref, globalvalue, globaldef, noshare and readonly
    are recognized by FlexeLint if they are enabled with
    +rw(globalref,globalvalue,globaldef,noshare,readonly).
    This option has been placed in co-vms.lnt.


*   To have FlexeLint search through logical name SYS$LIBRARY for
    include files use the option:

	-iSYS$LIBRARY:

    Note the use of the colon, and the fact that there is no
    space after the '-i'.  You may place such an option in an indirect
    file (as is shown in co-vms.lnt).


*   A common convention employed in the OPENVMS environment is to
    use standard identifers in place of files within #include statements.
    For example:

    #include stdio

    refers to a "module" in a OPENVMS text library.
    FlexeLint does not have built-in facilities to search
    such text libraries.  This could be done by a suitable modification
    of i_open within custom.c.  An alternative simpler procedure is to
    define identifiers like stdio to be equal to the full or, preferably,
    the partial names of these files as they are extracted from libraries.
    This can be done with the -#d option.  For example:

	-#dstdio="SYS$LIBRARY:..."
		OR
	-#dstdio="..."
		OR
	-#dstdio=<...>

    (the last approach was taken in preparing co-vms.lnt in this directory.)

    To avoid retyping such options, place them in a centrally located
    lint indirect file (such as co-vms.lnt).

*   You may use the INCLUDE environment variable to specify a list
    of directories to search.  If the list contains more that one
    directory the directories must be separated by a comma.
    But CAUTION.  If you do separate directories with a comma the
    entire list must be quoted or you will be very frustrated
    as the running program will only perceive one directory.
    For example:

    Define INCLUDE "directory1,directory2"

*   In order to have
        #pragma dictionary ...
    treated just like
        #dictionary ...
    you need the option:
        +pragma(dictionary,ppw)
    In words, this says treat the pragma word the same as the preprocessor
    word.  This has been added to co-vms.lnt

