
To compile the FlexeLint source code make sure that vers.h
is properly set.  When it comes from the 'factory'
it will probably contain #defines:


    #define os_2_2 0        /* OS/2, Version 2 */

    and

    #define os_UNIX 1       /* Unix */

Make sure you edit these to:

    #define os_2_2 1        /* OS/2, Version 2 */

    and

    #define os_UNIX 0       /* Unix */

In using FlexeLint you need to select a compiler options file having
the form co-xxx.lnt where xxx identifies the compiler.  Choose from
the list in directory supp/lnt.

Gimpel Software, 2008
