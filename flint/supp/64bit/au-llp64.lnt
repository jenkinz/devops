//  au-llp64.lnt -- Author options - 64-bit issues for the LLP64 model

//      The LLP64 model utilizes the following sizes:
//          long long -> 64 bits
//          pointers  -> 64 bits
//          long      -> 32 bits
//          int       -> 32 bits

/*
    This options file can be used to explicitly activate checks
    for 64-bit related issues.

    You can use this file directly when linting your programs as in:

    lin  au-llp64  files

 */

-sll8
-sp8

au-64.lnt

/*  Casting a 64-bit address to a 32-bit integer type can result in
    lost information.

    i32 = (long) p64;   // cast from pointer to long (N923)
                        // Size incompatibility (W507)
 */

    +estring(923,long)
    +estring(923,signed long)
    +estring(923,unsigned long)
    +e507

/*  Casting a 32-bit integer type to a 64-bit address may result in an
    invalid pointer value, depending on the system.

    p64 = (long *) i32; // cast from long to pointer (N923)
 */

    +estring(923,long)
    +estring(923,signed long)
    +estring(923,unsigned long)

