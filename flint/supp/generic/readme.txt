
If there is no further information in the supp directory or in the
Installation Notes (INotes.pdf) for your compiler or operating system
then start with the compiler options file in this directory and
observe whether any difficulties ensue.

In the extremely rare case that your compiler does not support
prototypes (old K&R C) you can use sl.c in this directory to
describe your library.

Gimpel Software
Jan, 2001

