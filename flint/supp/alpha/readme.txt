This directory holds information pertaining to the

    Alpha AXP (64-bit RISC) architecture originally from
	    Digital Equipment Corp.

The DEC Alpha is, of course, hardware and not software.
To adapt FlexeLint to this environment consider what operating
system is employed and visit that subdirectory.  Candidates are
'unix', 'linux' and 'openvms'.  You will want to visit 'gcc' if you are
using this compiler and possibly '64bit'.

For cross-linting a program on a 32-bit host that is destined
to be run on the 64-bit Alpha you may employ the following
options:

    -sl8    // the size of long's is 8 bytes
    -sp8    // the size of pointers is 8 bytes

Check with local operating system to determine whether the destination
truly supports 64-bit quantities.
