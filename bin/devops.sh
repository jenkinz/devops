#!/bin/bash
# 'devops' command for Linux

INSTALL_PREFIX=/usr/local
BIN_DIR=$INSTALL_PREFIX/bin
DEVOPS_DIR=$INSTALL_PREFIX/devops
INSTALL_URL=http://dev.sandel.local:8081/sandel-development/devops/raw/master/bin/install.sh
#INSTALL_URL=https://bitbucket.org/jenkinz/devops/raw/master/bin/install.sh

if [ "$1" == "update" ]
then

#$DEVOPS_DIR/install.sh
eval "$(curl -fksSL $INSTALL_URL)"

elif [ "$1" == "scaffold" ]
then

echo "Scaffolding..."
cp -R $DEVOPS_DIR/bld-tools/core/bld-template new-project
echo "Done, created new-project in the current directory."

elif [ "$1" == "help" ]
then

echo The devops command is used to administer the locally installed DevOps tools.
echo You MUST be on the internal Sandel network or VPN to run `devops update`!
echo DevOps tools are installed to $DEVOPS_DIR and maintained by this utility.
echo DevOps tools are symlinked from $DEVOPS_DIR into $BIN_DIR.
echo It is not recommended to manually modify anything in $DEVOPS_DIR.
echo 
echo The following commands are available:
echo \$devops update - update the DevOps tools
echo \$devops help - show this help information
echo \$devops scaffold - create a new project
echo
echo The DevOps tools installed are:
echo \$mk - execute aqn Opus makefile
echo \$tree - list contents of directories in a tree-like format
echo \$check - execute CodeCheck
echo \$flint - execute FlexeLint
echo \$vim - editor
#echo \$emacs
echo \$elapsedtime - elapsed timer
echo \$filedup
echo \$glintlookup
echo \$glintmetric
echo \$hexdumpf
echo \$checksump1
echo \$lint2cs - convert lint output to code standard rule output
echo \$listfinddup - find duplicates in a list
echo \$nice-c
echo \$testreadwritable - test if a file is read-write
echo \$xfilelookup
echo \$npath
echo \$pmccabe - get McCabe cyclomatic complexity of a C source file

else

echo "Usage: devops update | help | scaffold"

fi

