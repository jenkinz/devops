#!/bin/bash
#
# Installs basic developer tools from the devops project.
# Creates `devops' command for updating and managing the tools.
# This script supports UBUNTU LINUX 14.04 64-bit.
#
INSTALL_PREFIX=/usr/local
BIN_DIR=$INSTALL_PREFIX/bin
DEVOPS_DIR=$INSTALL_PREFIX/devops
ACTION="installed"
REMOTE_URL=http://dev.sandel.local:8081/sandel-development/devops.git
#REMOTE_URL=https://jenkinz@bitbucket.org/jenkinz/devops.git
GIT_IS_INSTALLED=1
SLED_IS_INSTALLED=1

# Function: install_it
# Arguments:
#   $1 - the apt-get name of the tool to install
# Description:
#   Installs (via apt-get) the tool ($1), if it has not already been installed.
function install_it {
    local tool=$1
    # Check if the package is already installed:
    dpkg -s $tool >/dev/null 2>&1
    local tool_installed=$?
    if [ ! $tool_installed -eq 0 ]; then
    # Package is not installed, so install it:
    sudo apt-get --quiet --yes --force-yes install $tool
    fi
}

# Function: get_file_if_newer
# Arguments:
#   $1 - the name of the file to save to the current directory
#   $2 - the name of the etag to save to the current directory
#   $3 - the URL of the file to download
# Description:
#   Downloads the specified file pointed to by URL ($3) IFF the ETag header 
#   differs from the existing ETag in the etag file ($2). If the etag file does
#   not exist, the file will be downloaded and a new etag file will be created.
#
#   The purpose of this is to only download the file if it's changed since last
#   being downloaded. A change is presumed when it is determined the ETag header
#   for the file has changed since the last download.
function get_file_if_newer {
    local file_name=$1
    local etag_name=$2
    local url=$3
    if [ -f $etag_name ]; then
    # Etag found from previous download, so check if the server file is newer 
    # and download if necessary:
    sudo curl --output $file_name --header "If-None-Match: `cat $etag_name|sed 's/\"//g'`" $url
    else
    # First time downloading (or the etag file has been removed). Download file:
    sudo curl --output $file_name $url
    fi
    # Generate an etag file for the most recent download:
    curl --head $url | grep Tag | sed 's/ETag: //g' | sudo tee $etag_name >/dev/null
}

if [ ! -d "$DEVOPS_DIR" ]; then # First time install

# Add apt-repository for Java:
sudo add-apt-repository --yes ppa:webupd8team/java >/dev/null 2>&1

echo ===========================================================================
echo Running apt-get update...
sudo apt-get --quiet --yes --force-yes update
echo ===========================================================================
#echo Running apt-get upgrade...
#sudo apt-get --quiet --yes --force-yes upgrade
#echo ===========================================================================

fi

echo ===========================================================================
echo Installing Linux packages...

install_it git
install_it build-essential
install_it lib32stdc++6
install_it gcc-multilib
# 32-bit libs required for Sage EDK
# (see: https://theredblacktree.wordpress.com/2014/05/17/installing-lpcxpresso-on-ubuntu-14-04-based-distribution/)
install_it libgtk2.0-0:i386 
install_it libpangox-1.0-0:i386 
install_it libpangoxft-1.0-0:i386 
install_it libidn11:i386 
install_it libglu1-mesa:i386 
install_it libxtst6:i386 
install_it libncurses5:i386 
install_it tree
#install_it wine
install_it vim
#install_it emacs
install_it xclip
install_it doxygen
install_it git-cola
install_it autoconf
install_it automake
install_it libtool
install_it gawk
# Java 7 - the first 2 commands accept the license agreement
echo debconf shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections
install_it oracle-java7-installer
install_it putty
install_it minicom
install_it libncurses5-dev
install_it m4
install_it bison
install_it flex
install_it expect
install_it tcl
install_it dejagnu
install_it libc6-dev-i386
install_it lib32z1
install_it lib32ncurses5
install_it lib32bz2-1.0
#install_it gcc-4.8-multilib
#install_it g++-4.8-multilib
install_it ncurses-dev
install_it tcpdump
install_it nmap
install_it arp-scan
install_it xinetd
install_it telnetd
install_it texinfo
install_it graphviz
install_it subversion

echo ===========================================================================

pushd . >/dev/null

#Update or install the DevOps tools from the remote repo:

if [ -d "$DEVOPS_DIR" ]; then

# Update DevOps tools:

echo Updating DevOps tools...

ACTION="updated"
pushd $DEVOPS_DIR >/dev/null
sudo git remote set-url origin $REMOTE_URL
sudo git fetch origin master
sudo git reset --hard origin/master
popd >/dev/null

echo ===========================================================================

else

echo Installing DevOps tools...

ACTION="installed"
pushd $INSTALL_PREFIX >/dev/null 
sudo git clone $REMOTE_URL $DEVOPS_DIR
popd >/dev/null

echo ===========================================================================

fi

# Set system-wide environment variables:

SAGE_HOME_ENV=$DEVOPS_DIR/apps/sage_edk

# First remove existing system-wide env var settings:
sudo rm -rf /etc/profile.d/devops.sh >/dev/null 2>&1
# Remove the call to /etc/profile.d/devops.sh call from /etc/bash.bashrc:
sudo sed -i '/#devops/d' /etc/bash.bashrc
sudo sed -i '/\. \/etc\/profile\.d\/devops\.sh/d' /etc/bash.bashrc
# Remove blank newlines at end of /etc/bash.bashrc:
sudo sh -c "sudo awk '/^$/ {nlstack=nlstack \"\n\";next;} {printf \"%s\",nlstack; nlstack=\"\"; print;}' /etc/bash.bashrc > /etc/bash.bashrc.new"
sudo cp /etc/bash.bashrc.new /etc/bash.bashrc
sudo rm /etc/bash.bashrc.new
# Set the system-wide environment variables:
sudo touch /etc/profile.d/devops.sh
sudo sh -c "printf 'export DEVOPS_HOME=$DEVOPS_DIR\n' >> /etc/profile.d/devops.sh"
sudo sh -c "printf 'export SAGE_HOME=$SAGE_HOME_ENV\n' >> /etc/profile.d/devops.sh"
sudo sh -c "printf 'export P4PORT=ssl:dev.sandel.local:1666\n' >> /etc/profile.d/devops.sh"
sudo sh -c "printf '\n\n#devops\n. /etc/profile.d/devops.sh\n' >> /etc/bash.bashrc"
# Reload and activate the system-wide environment variables:
source /etc/bash.bashrc
source ~/.bashrc

echo Installing apps...
sudo mkdir -p $DEVOPS_DIR/apps >/dev/null

pushd $DEVOPS_DIR/apps >/dev/null

# slickedit
SLED_FILE=slickedit.tar.gz
SLED_ETAG=slickedit.etag
SLED_URL=https://s3-us-west-1.amazonaws.com/sandel-dev/slickedit.tar.gz
get_file_if_newer $SLED_FILE $SLED_ETAG $SLED_URL
sudo tar --extract --gunzip --overwrite --file $SLED_FILE
vs -h >/dev/null 2>&1
SLED_IS_INSTALLED=$?
if [ ! $SLED_IS_INSTALLED -eq 0 ]
then
sudo $DEVOPS_DIR/apps/slickedit/v18/se_18000013_linux64/vsinst +accept-eula -post-install -destination-dir /opt/slickedit
sudo cp $DEVOPS_DIR/apps/slickedit/v18/se_1800_linux.lic /opt/slickedit/bin/slickedit.lic
fi

# sage_edk
# Install the up-to-date sage_edk:
SAGE_FILE=sage_edk_installer.tar.gz
SAGE_ETAG=sage_edk_installer.etag
SAGE_URL=https://s3-us-west-1.amazonaws.com/sandel-dev/sage_edk_installer.tar.gz
# Copy the etag to check if it changed:
sudo cp $SAGE_ETAG $SAGE_ETAG.prev >/dev/null 2>&1
get_file_if_newer $SAGE_FILE $SAGE_ETAG $SAGE_URL
# Check if the sage edk has been updated:
if diff $SAGE_ETAG $SAGE_ETAG.prev >/dev/null 2>&1; then
echo Sage EDK is up-to-date.
else
echo Installing updated Sage EDK...
# Remove previous Sage EDK:
sudo rm -rf $DEVOPS_DIR/apps/sage_edk >/dev/null 2>&1
sudo rm -f $DEVOPS_DIR/apps/sage_edk.tar.gz >/dev/null 2>&1
sudo rm -f $DEVOPS_DIR/apps/sage_edk.etag >/dev/null 2>&1
sudo tar --extract --gunzip --overwrite --file $SAGE_FILE
# Run the sage installer:
sudo chmod 0777 $DEVOPS_DIR/apps/sage_edk_installer/SageEDK_Linux-x86_64-Install
sudo $DEVOPS_DIR/apps/sage_edk_installer/SageEDK_Linux-x86_64-Install --mode silent --prefix $SAGE_HOME_ENV
# Move sage license file and manual to sage installation directory:
sudo mv $DEVOPS_DIR/apps/sage_edk_installer/license.key $SAGE_HOME_ENV
sudo mv $DEVOPS_DIR/apps/sage_edk_installer/Sage_SmartProbe_And_Users_Guide.pdf $SAGE_HOME_ENV
sudo rm -rf $DEVOPS_DIR/apps/sage_edk_installer
fi
sudo rm $SAGE_ETAG.prev >/dev/null 2>&1

# p4v
P4V_FILE=p4v.tar.gz
P4V_ETAG=p4v.etag
P4V_URL=https://s3-us-west-1.amazonaws.com/sandel-dev/p4v.tar.gz
get_file_if_newer $P4V_FILE $P4V_ETAG $P4V_URL
sudo tar --extract --gunzip --overwrite --file $P4V_FILE

# bcompare (Beyond Compare)
BCOMPARE_FILE=bcompare-4.1.1.20615_amd64.deb
BCOMPARE_ETAG=bcompare.etag
BCOMPARE_URL=https://s3-us-west-1.amazonaws.com/sandel-dev/bcompare-4.1.1.20615_amd64.deb
# Copy the etag to check if it changed:
sudo cp $BCOMPARE_ETAG $BCOMPARE_ETAG.prev >/dev/null 2>&1
get_file_if_newer $BCOMPARE_FILE $BCOMPARE_ETAG $BCOMPARE_URL
# Check if Beyond Compare .deb has been updated:
if diff $BCOMPARE_ETAG $BCOMPARE_ETAG.prev >/dev/null 2>&1; then
echo Beyond Compare is up-to-date.
else
echo Installing/updating Beyond Compare...
sudo dpkg --install $BCOMPARE_FILE
fi
sudo rm $BCOMPARE_ETAG.prev >/dev/null 2>&1

# adg (Affinic Debugger)
ADG_FILE=adg_linux.tar.gz
ADG_ETAG=adg.etag
ADG_URL=https://s3-us-west-1.amazonaws.com/sandel-dev/adg_linux.tar.gz
# Copy the etag to check if it changed:
sudo cp $ADG_ETAG $EDG_ETAG.prev >/dev/null 2>&1
get_file_if_newer $ADG_FILE $ADG_ETAG $ADG_URL
# Check if ADG file has been updated
if diff $ADG_ETAG $ADG_ETAG.prev >/dev/null 2>&1; then
echo Affinic Debugger is up-to-date.
else
echo Installing/updating Affinic Debugger...
sudo tar --extract --gunzip --overwrite --file $ADG_FILE
fi
sudo rm $ADG_ETAG.prev >/dev/null 2>&1

popd >/dev/null

echo ===========================================================================

echo Installing toolchains...
sudo mkdir -p $DEVOPS_DIR/toolchains >/dev/null

pushd $DEVOPS_DIR/toolchains >/dev/null

# xtools
XTOOLS_FILE=xtools.tar.gz
XTOOLS_ETAG=xtools.etag
XTOOLS_URL=https://s3-us-west-1.amazonaws.com/sandel-dev/xtools.tar.gz
# Copy the xtools etag to check if it changed:
sudo cp $XTOOLS_ETAG $XTOOLS_ETAG.prev >/dev/null 2>&1
get_file_if_newer $XTOOLS_FILE $XTOOLS_ETAG $XTOOLS_URL
# Check if xtools has been updated:
if diff $XTOOLS_ETAG $XTOOLS_ETAG.prev >/dev/null 2>&1; then
echo xtools is up-to-date.
else
echo Installing/updating xtools toolchain...
sudo tar --extract --gunzip --overwrite --file $XTOOLS_FILE
fi
sudo rm $XTOOLS_ETAG.prev >/dev/null 2>&1

echo ===========================================================================

#Symlink DevOps tools:

echo Symlinking DevOps tools into $BIN_DIR...
pushd $BIN_DIR >/dev/null
#Opus Make
sudo ln -snf $DEVOPS_DIR/opus/bins/mk mk
sudo ln -snf $DEVOPS_DIR/opus/bins/mkmf mkmf
sudo ln -snf $DEVOPS_DIR/opus/bins/omake omake
#CodeCheck
sudo ln -snf $DEVOPS_DIR/codecheck/bin/1500b1/check check
sudo ln -snf $DEVOPS_DIR/codecheck/bin/1500b1/mkccp/mkccp mkccp
#FlexeLint
sudo ln -snf $DEVOPS_DIR/flint/flint flint
# elapsedtime
sudo ln -snf $DEVOPS_DIR/internal/elapsedtime/bin/elapsedtime elapsedtime
#filedup
sudo ln -snf $DEVOPS_DIR/internal/filedup/bin/filedup filedup
#glintlookup
sudo ln -snf $DEVOPS_DIR/internal/glintlookup/bin/glintlookup glintlookup
#glintmetric
sudo ln -snf $DEVOPS_DIR/internal/glintmetric/bin/glintmetric glintmetric
#hexdumpf
sudo rm hexdump > /dev/null 2>&1
sudo ln -snf $DEVOPS_DIR/internal/hexdumpf/bin/hexdumpf hexdumpf
#chksump1
sudo rm checksump1 > /dev/null 2>&1
sudo ln -snf $DEVOPS_DIR/internal/chksump1/bin/chksump1 chksump1
#lint2cs
sudo ln -snf $DEVOPS_DIR/internal/lint2cs/bin/lint2cs lint2cs
#listfinddup
sudo ln -snf $DEVOPS_DIR/internal/listfinddup/bin/listfinddup listfinddup
#nice-c
sudo ln -snf $DEVOPS_DIR/internal/nice-c/bin/nice-c nice-c
#testreadwritable
sudo ln -snf $DEVOPS_DIR/internal/testreadwritable/bin/testreadwritable testreadwritable 
#xfilelookup
sudo ln -snf $DEVOPS_DIR/internal/xfilelookup/bin/xfilelookup xfilelookup
#npath
sudo ln -snf $DEVOPS_DIR/opensrc/npath/bin/npath npath
#pmccabe
sudo ln -snf $DEVOPS_DIR/opensrc/pmccabe/bin/pmccabe pmccabe
#slickedit
sudo ln -snf /opt/slickedit/bin/vs vs
sudo ln -snf /opt/slickedit/bin/vsdiff vsdiff
sudo ln -snf /opt/slickedit/bin/vsmerge vsmerge
#p4v
sudo ln -snf $DEVOPS_DIR/apps/p4v/p4v-2014.3.1007540/bin/p4v p4v
sudo ln -snf $DEVOPS_DIR/apps/p4v/p4v-2014.3.1007540/bin/p4admin p4admin
sudo ln -snf $DEVOPS_DIR/apps/p4v/p4v-2014.3.1007540/bin/p4merge p4merge
#set bcompare as default git diff tool:
git config --global diff.tool bc3
git config --global difftool.bc3 trustExitCode true
#set p4merge as default git merge tool:
git config --global merge.tool p4merge
#git config --global diff.tool p4merge
git config --global --add mergetool.prompt false
#git config --global --add difftool.prompt false
#Sage EDK
sudo ln -snf $DEVOPS_DIR/apps/sage_edk/eclipse/Sage_EDK Sage_EDK
#xtools toolchain
sudo ln -snf $DEVOPS_DIR/toolchains/xtools/xtools_output/bin/i386-elf-addr2line i386-elf-addr2line
sudo ln -snf $DEVOPS_DIR/toolchains/xtools/xtools_output/bin/i386-elf-ar i386-elf-ar
sudo ln -snf $DEVOPS_DIR/toolchains/xtools/xtools_output/bin/i386-elf-as i386-elf-as
sudo ln -snf $DEVOPS_DIR/toolchains/xtools/xtools_output/bin/i386-elf-c++ i386-elf-c++
sudo ln -snf $DEVOPS_DIR/toolchains/xtools/xtools_output/bin/i386-elf-c++filt i386-elf-c++filt
sudo ln -snf $DEVOPS_DIR/toolchains/xtools/xtools_output/bin/i386-elf-cpp i386-elf-cpp
sudo ln -snf $DEVOPS_DIR/toolchains/xtools/xtools_output/bin/i386-elf-elfedit i386-elf-elfedit
sudo ln -snf $DEVOPS_DIR/toolchains/xtools/xtools_output/bin/i386-elf-g++ i386-elf-g++
sudo ln -snf $DEVOPS_DIR/toolchains/xtools/xtools_output/bin/i386-elf-gcc i386-elf-gcc
sudo ln -snf $DEVOPS_DIR/toolchains/xtools/xtools_output/bin/i386-elf-gcc-4.8.4 i386-elf-gcc-4.8.4
sudo ln -snf $DEVOPS_DIR/toolchains/xtools/xtools_output/bin/i386-elf-gcc-ar i386-elf-gcc-ar
sudo ln -snf $DEVOPS_DIR/toolchains/xtools/xtools_output/bin/i386-elf-gcc-nm i386-elf-gcc-nm
sudo ln -snf $DEVOPS_DIR/toolchains/xtools/xtools_output/bin/i386-elf-gcc-ranlib i386-elf-gcc-ranlib
sudo ln -snf $DEVOPS_DIR/toolchains/xtools/xtools_output/bin/i386-elf-gcov i386-elf-gcov
sudo ln -snf $DEVOPS_DIR/toolchains/xtools/xtools_output/bin/i386-elf-gprof i386-elf-gprof
sudo ln -snf $DEVOPS_DIR/toolchains/xtools/xtools_output/bin/i386-elf-ld i386-elf-ld
sudo ln -snf $DEVOPS_DIR/toolchains/xtools/xtools_output/bin/i386-elf-ld.bfd i386-elf-ld.bfd
sudo ln -snf $DEVOPS_DIR/toolchains/xtools/xtools_output/bin/i386-elf-nm i386-elf-nm
sudo ln -snf $DEVOPS_DIR/toolchains/xtools/xtools_output/bin/i386-elf-objcopy i386-elf-objcopy
sudo ln -snf $DEVOPS_DIR/toolchains/xtools/xtools_output/bin/i386-elf-objdump i386-elf-objdump
sudo ln -snf $DEVOPS_DIR/toolchains/xtools/xtools_output/bin/i386-elf-ranlib i386-elf-ranlib
sudo ln -snf $DEVOPS_DIR/toolchains/xtools/xtools_output/bin/i386-elf-readelf i386-elf-readelf
sudo ln -snf $DEVOPS_DIR/toolchains/xtools/xtools_output/bin/i386-elf-size i386-elf-size
sudo ln -snf $DEVOPS_DIR/toolchains/xtools/xtools_output/bin/i386-elf-strings i386-elf-strings
sudo ln -snf $DEVOPS_DIR/toolchains/xtools/xtools_output/bin/i386-elf-strip i386-elf-strip
#adg
sudo ln -snf $DEVOPS_DIR/apps/adg/adg adg
#devops command
sudo ln -snf $DEVOPS_DIR/bin/devops.sh devops
#...

popd >/dev/null

echo ===========================================================================

echo DevOps tools $ACTION succesfully. Run \'devops help\' for more information.

echo ===========================================================================

popd >/dev/null

