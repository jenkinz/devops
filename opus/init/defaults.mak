# Definitions and rules culled from /usr/bin/make (GNU Make 3.74)
# by running "/usr/bin/make -f /dev/null -p", then reformatting and
# making Opus changes as needed
#


# GNU Make version 3.74, by Richard Stallman and Roland McGrath.
# Copyright (C) 1988, 89, 90, 91, 92, 93, 94, 95 Free Software Foundation, Inc.
# This is free software; see the source for copying conditions.
# There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.

# Variables

AR = ar
ARFLAGS = rv
AS = as
CC = cc
CHECKOUT,v = $(CO) $(COFLAGS) $< $@
CO = co
COMPILE.C = $(COMPILE.cc)
COMPILE.F = $(FC) $(FFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c
COMPILE.S = $(CC) $(ASFLAGS) $(CPPFLAGS) $(TARGET_MACH) -c
COMPILE.c = $(CC) $(CFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c
COMPILE.cc = $(CXX) $(CXXFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c
COMPILE.def = $(M2C) $(M2FLAGS) $(DEFFLAGS) $(TARGET_ARCH)
COMPILE.f = $(FC) $(FFLAGS) $(TARGET_ARCH) -c
COMPILE.mod = $(M2C) $(M2FLAGS) $(MODFLAGS) $(TARGET_ARCH)
COMPILE.p = $(PC) $(PFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c
COMPILE.r = $(FC) $(FFLAGS) $(RFLAGS) $(TARGET_ARCH) -c
COMPILE.s = $(AS) $(ASFLAGS) $(TARGET_MACH)
CPP = $(CC) -E
CTANGLE = ctangle
CWEAVE = cweave
CXX = g++
F77 = $(FC)
F77FLAGS = $(FFLAGS)
FC = f77
GET = get
LD = ld
LEX = lex
LEX.l = $(LEX) $(LFLAGS) -t
LINK.C = $(LINK.cc)
LINK.F = $(FC) $(FFLAGS) $(CPPFLAGS) $(LDFLAGS) $(TARGET_ARCH)
LINK.S = $(CC) $(ASFLAGS) $(CPPFLAGS) $(LDFLAGS) $(TARGET_MACH)
LINK.c = $(CC) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) $(TARGET_ARCH)
LINK.cc = $(CXX) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) $(TARGET_ARCH)
LINK.f = $(FC) $(FFLAGS) $(LDFLAGS) $(TARGET_ARCH)
LINK.o = $(CC) $(LDFLAGS) $(TARGET_ARCH)
LINK.p = $(PC) $(PFLAGS) $(CPPFLAGS) $(LDFLAGS) $(TARGET_ARCH)
LINK.r = $(FC) $(FFLAGS) $(RFLAGS) $(LDFLAGS) $(TARGET_ARCH)
LINK.s = $(CC) $(ASFLAGS) $(LDFLAGS) $(TARGET_MACH)
LINT = lint
LINT.c = $(LINT) $(LINTFLAGS) $(CPPFLAGS) $(TARGET_ARCH)
M2C = m2c
MAKE_COMMAND := $(MAKE)
MAKE = $(MAKE_COMMAND)
MAKEINFO = makeinfo
MAKE_VERSION = 6.12
OUTPUT_OPTION = -o $@
PC = pc
PREPROCESS.F = $(FC) $(FFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -F
PREPROCESS.S = $(CC) -E $(CPPFLAGS)
PREPROCESS.r = $(FC) $(FFLAGS) $(RFLAGS) $(TARGET_ARCH) -F
RM = rm -f
TANGLE = tangle
TEX = tex
TEXI2DVI = texi2dvi
WEAVE = weave
YACC = yacc
YACC.y = $(YACC) $(YFLAGS)

# Rules
#
.SUFFIXES: .out .a .ln .o .c .cc .C .p .f .F .r .y .l .s .S .mod .sym .def .h .info .dvi .tex .texinfo .texi .txinfo .w .ch .web .sh .elc .el

%: %.o
	$(LINK.o) $^ $(LOADLIBES) $(LDLIBS) -o $@

%: %.c
	$(LINK.c) $^ $(LOADLIBES) $(LDLIBS) -o $@

%.ln: %.c
	$(LINT.c) -C$* $<

%.o: %.c
	$(COMPILE.c) $< $(OUTPUT_OPTION)

%: %.cc
	$(LINK.cc) $^ $(LOADLIBES) $(LDLIBS) -o $@

%.o: %.cc
	$(COMPILE.cc) $< $(OUTPUT_OPTION)

%: %.C
	$(LINK.C) $^ $(LOADLIBES) $(LDLIBS) -o $@

%.o: %.C
	$(COMPILE.C) $< $(OUTPUT_OPTION)

%: %.p
	$(LINK.p) $^ $(LOADLIBES) $(LDLIBS) -o $@

%.o: %.p
	$(COMPILE.p) $< $(OUTPUT_OPTION)

%: %.f
	$(LINK.f) $^ $(LOADLIBES) $(LDLIBS) -o $@

%.o: %.f
	$(COMPILE.f) $< $(OUTPUT_OPTION)

%: %.F
	$(LINK.F) $^ $(LOADLIBES) $(LDLIBS) -o $@

%.o: %.F
	$(COMPILE.F) $< $(OUTPUT_OPTION)

%.f: %.F
	$(PREPROCESS.F) $< $(OUTPUT_OPTION)

%: %.r
	$(LINK.r) $^ $(LOADLIBES) $(LDLIBS) -o $@

%.o: %.r
	$(COMPILE.r) $< $(OUTPUT_OPTION)

%.f: %.r
	$(PREPROCESS.r) $< $(OUTPUT_OPTION)

%.ln: %.y
	$(YACC.y) $< 
	$(LINT.c) -C$* y.tab.c 
	$(RM) y.tab.c

%.c: %.y
	$(YACC.y) $< 
	mv -f y.tab.c $@

%.ln: %.l
	@$(RM) $*.c
	$(LEX.l) $< > $*.c
	$(LINT.c) -i $*.c -o $@
	$(RM) $*.c

%.c: %.l
	@$(RM) $@ 
	$(LEX.l) $< > $@

%.r: %.l
	$(LEX.l) $< > $@ 
	mv -f lex.yy.r $@

%: %.s
	$(LINK.s) $^ $(LOADLIBES) $(LDLIBS) -o $@

%.o: %.s
	$(COMPILE.s) -o $@ $<

%: %.S
	$(LINK.S) $^ $(LOADLIBES) $(LDLIBS) -o $@

%.o: %.S
	$(COMPILE.S) -o $@ $<

%.s: %.S
	$(PREPROCESS.S) $< > $@

%: %.mod
	$(COMPILE.mod) -o $@ -e $@ $^

%.o: %.mod
	$(COMPILE.mod) -o $@ $<

%.sym: %.def
	$(COMPILE.def) -o $@ $<

%.dvi: %.tex
	$(TEX) $<

%.info: %.texinfo
	$(MAKEINFO) $(MAKEINFO_FLAGS) $< -o $@

%.dvi: %.texinfo
	$(TEXI2DVI) $(TEXI2DVI_FLAGS) $<

%.info: %.texi
	$(MAKEINFO) $(MAKEINFO_FLAGS) $< -o $@

%.dvi: %.texi
	$(TEXI2DVI) $(TEXI2DVI_FLAGS) $<

%.info: %.txinfo
	$(MAKEINFO) $(MAKEINFO_FLAGS) $< -o $@

%.dvi: %.txinfo
	$(TEXI2DVI) $(TEXI2DVI_FLAGS) $<

%.c: %.w
	$(CTANGLE) $< - $@

%.tex: %.w
	$(CWEAVE) $< - $@

%.p: %.web
	$(TANGLE) $<

%.tex: %.web
	$(WEAVE) $<

%: %.sh
	cat $< >$@ 
	chmod a+x $@

%.out: %
	@rm -f $@ 
	cp $< $@

% .NOCHAIN .MAKE : %,v
	$(CHECKOUT,v)

% .NOCHAIN .MAKE : RCS/%,v
	$(CHECKOUT,v)

% .NOCHAIN : s.%
	$(GET) $(GFLAGS) $(SCCS_OUTPUT_OPTION) $<

% .NOCHAIN : SCCS/s.%
	$(GET) $(GFLAGS) $(SCCS_OUTPUT_OPTION) $<

.c:
	$(LINK.c) $^ $(LOADLIBES) $(LDLIBS) -o $@

.texinfo.info:
	$(MAKEINFO) $(MAKEINFO_FLAGS) $< -o $@

.mod.o:
	$(COMPILE.mod) -o $@ $<

.c.o:
	$(COMPILE.c) $< $(OUTPUT_OPTION)

.s:
	$(LINK.s) $^ $(LOADLIBES) $(LDLIBS) -o $@

.txinfo.dvi:
	$(TEXI2DVI) $(TEXI2DVI_FLAGS) $<

.y.c:
	$(YACC.y) $< 
	mv -f y.tab.c $@

.web.tex:
	$(WEAVE) $<

.s.o:
	$(COMPILE.s) -o $@ $<

.F.f:
	$(PREPROCESS.F) $< $(OUTPUT_OPTION)

.web.p:
	$(TANGLE) $<

.F:
	$(LINK.F) $^ $(LOADLIBES) $(LDLIBS) -o $@

.cc.o:
	$(COMPILE.cc) $< $(OUTPUT_OPTION)

.S.s:
	$(PREPROCESS.S) $< > $@

.def.sym:
	$(COMPILE.def) -o $@ $<

.F.o:
	$(COMPILE.F) $< $(OUTPUT_OPTION)

.f:
	$(LINK.f) $^ $(LOADLIBES) $(LDLIBS) -o $@

.y.ln:
	$(YACC.y) $< 
	$(LINT.c) -C$* y.tab.c 
	$(RM) y.tab.c

.texi.dvi:
	$(TEXI2DVI) $(TEXI2DVI_FLAGS) $<

.texinfo.dvi:
	$(TEXI2DVI) $(TEXI2DVI_FLAGS) $<

.p.o:
	$(COMPILE.p) $< $(OUTPUT_OPTION)

.r.f:
	$(PREPROCESS.r) $< $(OUTPUT_OPTION)

.w.tex:
	$(CWEAVE) $< - $@

.txinfo.info:
	$(MAKEINFO) $(MAKEINFO_FLAGS) $< -o $@

.o:
	$(LINK.o) $^ $(LOADLIBES) $(LDLIBS) -o $@

.c.ln:
	$(LINT.c) -C$* $<

.l.r:
	$(LEX.l) $< > $@ 
	mv -f lex.yy.r $@

.r.o:
	$(COMPILE.r) $< $(OUTPUT_OPTION)

.C.o:
	$(COMPILE.C) $< $(OUTPUT_OPTION)

.p:
	$(LINK.p) $^ $(LOADLIBES) $(LDLIBS) -o $@

.l.c:
	@$(RM) $@ 
	$(LEX.l) $< > $@

.sh:
	cat $< >$@ 
	chmod a+x $@

.cc:
	$(LINK.cc) $^ $(LOADLIBES) $(LDLIBS) -o $@

.f.o:
	$(COMPILE.f) $< $(OUTPUT_OPTION)

.texi.info:
	$(MAKEINFO) $(MAKEINFO_FLAGS) $< -o $@

.l.ln:
	@$(RM) $*.c
	$(LEX.l) $< > $*.c
	$(LINT.c) -i $*.c -o $@
	$(RM) $*.c

.S.o:
	$(COMPILE.S) -o $@ $<

.tex.dvi:
	$(TEX) $<

.r:
	$(LINK.r) $^ $(LOADLIBES) $(LDLIBS) -o $@

.mod:
	$(COMPILE.mod) -o $@ -e $@ $^

.C:
	$(LINK.C) $^ $(LOADLIBES) $(LDLIBS) -o $@

.S:
	$(LINK.S) $^ $(LOADLIBES) $(LDLIBS) -o $@

.w.c:
	$(CTANGLE) $< - $@
