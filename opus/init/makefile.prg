# This is a sample PROGRAM template file for OPUS MKMF 3.0

# MKMF automatically maintains the HDRS, EXTHDRS, OBJS and SRCS macros.
# The PROGRAM macro is the name of the executable to be constructed.
#
# The EXTHDRS, HDRS, and SRCS macro are not explicitly used in this makefile
# but you may have a need for them elsewhere (e.g. for revision control).
#
CFLAGS		=		# optional - macro used by the .c.o rule and
				# used to pass compiler flags to the C compiler.
DEST		= .		# optional - the directory where the PROGRAM
				# will be installed.
EXTHDRS		=
HDRS		=
LDFLAGS		=		# optional - names the linker flags
LDLIBS		=		# optional - library files (full pathnames)
OBJS		=
PROGRAM		= a.out
SRCS		=
MKMF_SRCS	= *.c


# This is the only required target. It states that the PROGRAM depends
# on all files listed in the OBJS and LIBS macros. The command for updating
# the library is the line following the dependency line.
#
$(PROGRAM):     $(OBJS) $(LDLIBS)
		$(CC) $(LDFLAGS) -o $(.TARGET) $(.SOURCES) $(LDLIBS)


# The following targets are optional, and are included here as examples.
#
depend:;	@mkmf -dL -cs

clean:;		@rm -r *.o core

install:	$(PROGRAM)
		install $(LIBRARY) $(DEST)

