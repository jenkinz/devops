
This file contains the following:

	1) Differences between MS-DOS Make and LINUX Make.

	2) Differences between MS-DOS MKMF and LINUX MKMF.


1) Differences between MS-DOS and LINUX OPUS MAKE
=================================================

1a) Features differences
------------------------

	No memory reducing spawn
		(unnecessary in a virtual memory machine)

	All shell lines use /bin/sh without special interpretation
		(unnecessary in a fast forking machine)

	Suffix rules are converted to meta rules, including rules of one-
	suffix form ".from_extension:" This is converted into the meta
	rule "% : %.from_extension"


1b) Initialization file differences
-----------------------------------

	The initialization file can be named in the OPUSMAKECFG
	environment variable.  If that variable isn't set, MAKE
	looks for ./.make.ini, then for $(HOME)/.make.ini, then for
	<path_to_make_executable>/make.ini and then finally for
	/usr/local/opus/make.ini.

	Note: the "global" initialization file in <path_to_make_executable>
	or /usr/local/opus does not start with ".".


1c) Macros predefined in LINUX OPUS MAKE
----------------------------------------

	OS         = unix


1d) Rules predefined in LINUX OPUS MAKE
---------------------------------------

	Unix Opus Make only defines a few rules and depends on the
	supplied "/usr/local/opus/defaults.mak" file to be read at startup
	time. The default "make.ini" file supplied by us checks for the
	existence of "/usr/local/opus/defaults.mak" and reads it if it
	exists.

	We provide a "defaults.mak" file that is culled from the LINUX
	system make utility, /usr/bin/make (GNU Make).

	
	Opus Make also defines the following rule:

	#
	# Update an object library.
	#
	%.a:
		%if ! %null(.NEWSOURCES)
		$(AR) $(ARFLAGS) $(.TARGET) $(.NEWSOURCES)
		%endif




2) Differences between MS-DOS and LINUX MKMF
============================================

2a) Initialization file differences
-----------------------------------

	As 1b) above.


2b) Template file differences
-----------------------------

	The template files "makefile.prg" and "makefile.lib" are searched
	for first in the current directory, then in the directory of
	the initialization file, then in /usr/local/opus.


2c) Differences in configuration macros
---------------------------------------

	LINUX MKMF uses the following configuration macros:

	.TYPE.OBJ	= .o
	.TYPE.C		= .c .C .cpp .CPP .y .l
	.TYPE.SRC	= .f .F .e .r .p
	.TYPE.HDR	= .h .H .i

	.HDRPATH.c	= $(.SOURCE,D) $(CFLAGS,M^-I,S///) | \
				/usr/include /usr/local/include
	.HDRPATH.cpp	= $(.HDRPATH.c)
	.HDRPATH.C	= $(.HDRPATH.c)

	.CDEFINES.c	= -Dunix $(CFLAGS,M^[-/][UD])
	.CDEFINES.C	= -Dunix -D__cplusplus $(CXXFLAGS,M^[-/][UD])
	.CDEFINES.y	= -Dunix $(YFLAGS,M^[-/][UD])

	.HDR.c		= ^[ \t]*#[ \t]*include[ \t]+\(.*\)[ \t/]*
	.HDR.f		= ^[ \t]*[Ii][Nn][Cc][Ll][Uu][Dd][Ee][ \t]+'\(.*\)'
	.HDR.p		= ^#[ \t]*[Ii][Nn][Cc][Ll][Uu][Dd][Ee][ \t]+\"\(.*\)\"
