Jee,

Installation is very simple. 
Copy zip to /opt/opus, unpack it, add it your path and you're done.
A script may look like
cd /opt;mkdir opus;cd opus;cp /yourdir/omake_unpack.zip .;unzip omake_unpack.zip. 

Then Add /opt/opus/bin to your path and you're done. 
If you want to use static images put /opt/opus/bins:/opt/opus/bin in your PATH
 
Finally test that you have it 
$ which omake
 
omake -f Makefile for the Weather stuff or 
just 
omake
 
and it should work with that Makefile

See the opusman (opus make manual) PDF if you want to look up something.

I forward to this omake in slickedit linux and windows and it gives me 1
common build environment. If you want me to provide entire project files to you
in slickedit (much in the same way you would do in say Visual Studio or Eclipse)
let me know and I'll get them to you - they are the *.vpw, *.vpj files 
in the zip file you got.
