Compile/Linux using Makefile for GNU Linux and Windows using gcc

Optionable: 
On Windows compile/Link using Opus Make with Visual C++ on windows 
with Makefile.msc using 'omake -f Makefile.msc'

I set it up so you can reuse gcc on windows or linux using makefile.
I use makefile instead of Makefile as a habit to ensure portability as Windows
is case insensitive and Makefile/makefile only matters in precedence - here
either one would work. In most cases I use makefile (all lowercase) when doing 
cross platform builds. 
I don't require cygwin but I do require win32-unix in order to use std linux
utils.

grep AMS *.c* to see my updates required for Visual C++ build on Windows.

Version 2.7.1 is based off of 2.7 from:
http://parisc-linux.org/~bame/pmccabe/news.html

Minor changes in 2.7.1 to address extra circumstances of CR LF.


