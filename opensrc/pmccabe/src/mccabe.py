#! /usr/bin/python

import subprocess
import sys
import operator

print(sys.version)
####CLASSES##################################################
class function:
  def __init__(self):
    self.funcName  = ""
    self.fileName  = ""
    self.modMccabe = 0
    self.traMccabe = 0
    self.numStatem = 0
    self.firstLine = 0
    self.numLines  = 0

  def print_all(self):
    print self.funcName + " " + self.fileName + " " + self.modMccabe + " " + self.traMccabe + " " + self.numStatem + " " +self.firstLine + " " +self.numLines

class file:
  def __init__(self):
    self.fileName = ""
    self.totModMccabe = 0
    self.totTraMccabe = 0
    self.avgModMccabe = 0
    self.avgTraMccabe = 0
    self.totLines     = 0
    self.avgLines     = 0
    self.totStatem    = 0
    self.avgStatem    = 0
    self.funcs        = []
  
  def addFunc(self, newFunc):
    self.funcs.append(newFunc)

  def calcAll(self):
    for func in self.funcs:
      self.totModMccabe += func.modMccabe
      self.totTraMccabe += func.traMccabe
      self.totLines     += func.numLines
      self.totStatem    += func.numStatem
    if len(self.funcs) != 0:
      self.avgModMccabe = self.totModMccabe / len(self.funcs)
      self.avgTraMccabe = self.totTraMccabe / len(self.funcs)
      self.avgLines     = self.totLines     / len(self.funcs)
      self.avgStatem    = self.totStatem    / len(self.funcs)

  def printReport(self):
    print "File name %20s" % (self.fileName)
    print "Number of functions %10d" %(len(self.funcs))
    print "        %10s %10s %10s %10s" % ("ModMcCabe", "TradMccabe", "Lines", "Statements")
    print "Totals: %10d %10d %10d %10d" % (self.totModMccabe, self.totTraMccabe, self.totLines, self.totStatem)
    print "Avgs:   %10d %10d %10d %10d" % (self.avgModMccabe, self.avgTraMccabe, self.avgLines, self.avgStatem)


####Functions######################################################
def printTops (funcList, attribute, rangeRed, rangeYel):
  funcList.sort(key = operator.attrgetter(attribute), reverse = True)
  reds = 0
  yellows = 0
  print ""
  print "%10s %35s %35s" % (attribute, "Function", "File")
  f = operator.attrgetter(attribute)
  for func in allFuncs:
    if f(func) >= rangeRed:
      print "**%8d %35s %35s" % (f(func), func.funcName, func.fileName)
      reds+=1
    elif f(func) >= rangeYel:
      print " *%8d %35s %35s" % (f(func), func.funcName, func.fileName)
      yellows+=1
    else:
      break
  print "Reds: %20d Yellows: %20d" % (reds, yellows)

def getAvgs (funcList, attribute):
  curAvg = 0
  curAvgTotal = 0
  maxAvgTotal = 0
  maxAvgTotalList = []
  avgListCount = {'0': 0}
  allFuncs.sort(key = operator.attrgetter(attribute), reverse = True)
  f = operator.attrgetter(attribute)
  mediumAvg = f(allFuncs[(len(allFuncs) + 1)/2])
  meanAvgCount = 0
  for func in allFuncs:
    avg = f(func)
    meanAvgCount += avg
    if avg == curAvg:
      avgListCount[avg] += 1
    else:
      curAvg = avg
      avgListCount[avg] = 1
    if avgListCount[curAvg] > maxAvgTotal:
      del maxAvgTotalList[:]
      maxAvgTotalList.append(curAvg)
      maxAvgTotal = avgListCount[curAvg]
    elif avgListCount[curAvg] == maxAvgTotal:
      maxAvgTotalList.append(curAvg)

  returnList = [str(meanAvgCount / len(allFuncs)), str(mediumAvg),' '.join(str(e) for e in maxAvgTotalList)]
  return returnList
  #print "Mean Average %10s:" % (attribute)
  #print meanAvgCount / len(allFuncs)
  #print "Mode Average %10s:" % (attribute)
  #print ' '.join(str(e) for e in maxAvgTotalList)
  #print "Medium Average %10s:" % (attribute)
  #print mediumAvg


####MAIN##########################################

#pmccabe = "/project/pmbuild-tools/xplatform/pmccabe/bin/pmccabe"
#pmccabe = "/var/freespace2/GreenPlane/tools/pmccabe-2.7/pmccabe"
#pmccabe = "/var/freespace2/GreenPlane/tools/pmccabe_bk/src/pmccabe"
pmccabe = "/asi/workspace/kgroves/pmccabe-2.7.1/src/pmccabe"
#pmccabe = "pmccabe"
#Get number of files passed in
numFiles = len(sys.argv) - 1
rawFiles = sys.argv[1:]
print str(numFiles) + " files"
failedFiles = []
funcs = []
files = {}
allFuncs = []

totalModMccabe   = 0
totalTradMccabe  = 0
totalLines       = 0
totalStatements  = 0
avgModMccabeTot  = 0
avgTradMccabeTot = 0
avgLinesTot      = 0
avgStatementsTot = 0

avgModMccabeList = []

maxModMccabeFunc = function()
maxTraMccabeFunc = function()
maxNumLinesFunc  = function()
maxNumStatemFunc = function()

for item in rawFiles:
  #print "Processing: " + str(item)
  cmd = pmccabe + " " + str(item)
  p = subprocess.Popen(cmd, shell=True, bufsize=0, stdin=subprocess.PIPE, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
  #if p.stdout:
   # failedFiles.append(str(item))
  for line in p.stdout:
    #print line
    stats = line.split( )
    tempFunc = function()
    tempFunc.funcName = stats[6]
    tempFunc.fileName = stats[5][:stats[5].find('(')]
    tempFunc.modMccabe = int(stats[0])
    tempFunc.traMccabe = int(stats[1])
    tempFunc.numStatem = int(stats[2])
    tempFunc.firstLine = int(stats[3])
    tempFunc.numLines  = int(stats[4])
    if files.has_key(tempFunc.fileName):
      #print "Adding function: %20s to file: %s" % (tempFunc.funcName, tempFunc.fileName)
      files[tempFunc.fileName].addFunc(tempFunc)
    else:
      #print "Creating new file obj: " + tempFunc.fileName
      newFile = file()
      newFile.fileName = tempFunc.fileName
      newFile.addFunc(tempFunc)
      files[tempFunc.fileName] = newFile
    #print "tempFunc %10d > maxModMccabe %10d" % (tempFunc.modMccabe, maxModMccabeFunc.modMccabe)
    if tempFunc.modMccabe > maxModMccabeFunc.modMccabe:
      maxModMccabeFunc = tempFunc
    if tempFunc.traMccabe > maxTraMccabeFunc.traMccabe:
      maxTraMccabeFunc = tempFunc
    if tempFunc.numLines  > maxNumLinesFunc.numLines:
      maxNumLinesFunc  = tempFunc
    if tempFunc.numStatem > maxNumStatemFunc.numStatem:
      maxNumStatemFunc = tempFunc

    avgModMccabeList.append(tempFunc.modMccabe)

      #print files[tempFunc.fileName].funcs
    #funcs.append(tempFunc)
    #print tempFunc.numLines
    #print tempFunc.print_all()
#print files
#print files.values()[0].funcs
totalFuncs = 0
for file in files.values():
  file.calcAll()
  #file.printReport()

  totalModMccabe   += file.totModMccabe
  totalTradMccabe  += file.totTraMccabe
  totalLines       += file.totLines
  totalStatements  += file.totStatem
  avgModMccabeTot  += file.avgModMccabe
  avgTradMccabeTot += file.avgTraMccabe
  avgLinesTot      += file.avgLines
  avgStatementsTot += file.avgStatem
  totalFuncs += len(file.funcs)
  allFuncs += file.funcs

numProcessedFiles = len(files)



printTops(allFuncs, 'modMccabe', 20, 10)
printTops(allFuncs, 'traMccabe', 20, 10)
#printTops(allFuncs, 'numLines', 100, 50)
#printTops(allFuncs, 'numStatem', 100, 50)
avgsModMccabe = getAvgs(allFuncs, 'modMccabe')
avgsTraMccabe = getAvgs(allFuncs, 'traMccabe')
avgsNumLines  = getAvgs(allFuncs, 'numLines')
avgsNumStatem = getAvgs(allFuncs, 'numStatem')


print ""
print "Total number of files     %10d" % (len(files))
print "Total number of functions %10d" % (totalFuncs)
print "All Files  %10s %10s %10s %10s" % ("ModMccabe", "TradMccabe", "Lines", "Statements")
print "Totals:    %10d %10d %10d %10d" % (totalModMccabe, totalTradMccabe, totalLines, totalStatements)
print "Mean Avg:  %10s %10s %10s %10s" % (avgsModMccabe[0], avgsTraMccabe[0], avgsNumLines[0], avgsNumStatem[0])
print "MedianAvg: %10s %10s %10s %10s" % (avgsModMccabe[1], avgsTraMccabe[1], avgsNumLines[1], avgsNumStatem[1])
print "Mode Avg:  %10s %10s %10s %10s" % (avgsModMccabe[2], avgsTraMccabe[2], avgsNumLines[2], avgsNumStatem[2])
print "Max Mod Mccabe  %10d      %20s" % (maxModMccabeFunc.modMccabe, maxModMccabeFunc.funcName)
print "Max Tra Mccabe  %10d      %20s" % (maxTraMccabeFunc.traMccabe, maxTraMccabeFunc.funcName)
print "Max Lines       %10d      %20s" % (maxNumLinesFunc.numLines, maxNumLinesFunc.funcName)
print "Max Statments   %10d      %20s" % (maxNumStatemFunc.numStatem, maxNumStatemFunc.funcName)
print "Failed files: %d" % (numFiles - len(files))
