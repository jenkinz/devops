/************************************************************************
 *  Copyright (c) 1996 by Charles A. Measday                            *
 *                                                                      *
 *  Permission to use, copy, modify, and distribute this software       *
 *  and its documentation for any purpose and without fee is hereby     *
 *  granted, provided that the above copyright notice appear in all     *
 *  copies.  The author makes no representations about the suitability  *
 *  of this software for any purpose.  It is provided "as is" without   *
 *  express or implied warranty.                                        *
 ************************************************************************/

/*
@(#)  FILE: fnm_util.c  RELEASE: 1.10  DATE: 11/19/98, 15:37:02
*/
/*******************************************************************************

File:

    fnm_util.c

    Filename Utilities.


Author:    Alex Measday, ISI


Purpose:

    The FNM_UTIL package provides a filename parsing capability inspired
    by the VAX/VMS lexical function, F$PARSE().  File specifications have
    the following structure:

                 node:/directory(s)/name.extension.version

    Any field is optional.  NODE is a host name; DIRECTORY is one or more
    names separated by "/"s; NAME follows the last "/" in the pathname.
    VERSION is a 3-digit number (e.g., "002") and EXTENSION follows the
    last dot before the VERSION dot.

    A filename is created as follows:

        #include  "fnm_util.h"			-- Filename utilities.
        FileName  fname ;
        ...
        fname = fnmCreate ("<file_spec>", NULL) ;

    fnmCreate() expands the file specification, translating environment
    variable references and filling in defaults for missing fields.

    fnmCreate() can be passed multiple file specifications, which are
    then processed from left to right in the calling sequence:

        fname = fnmCreate ("<spec1>", ..., "<specN>", NULL) ;

    First, the leftmost file specification is examined and any references
    to environment variables are translated.  The next file specification
    is then examined.  Environment variables are translated and fields
    missing in the first file specification are supplied from the new
    file specification.  Subsequent file specifications are examined,
    in turn, and "applied" to the results of the processing of the
    previous file specifications.  Finally, system defaults (e.g., the
    user's home directory) are supplied for missing fields that remain.

    Is that clear?  I used to have a diagram that showed the file names
    stacked up, one on top of another:

                          ... System Defaults ...
                               File_Spec_#N    |
                                    ...        |
                               File_Spec_#2    |
                               File_Spec_#1    V
                              --------------
                                  Result

    File name components would drop down through holes in lower-level
    specifications to fill in missing fields in the result.

    Specifying multiple file specifications is useful for replacing
    extensions, concatenating directories, etc.:

        #include  "fnm_util.h"			-- Filename utilities.
        FileName  fname ;
        ...					-- "/usr/me" (current directory)
        fname = fnmCreate (NULL) ;
						-- "/usr/me/prog.lis"
        fname = fnmCreate (".lis", "prog.c", NULL) ;
						-- "/usr/you/tools/dump.o"
        fname = fnmCreate (".o", "tools/dump.c", "/usr/you/", NULL) ;

    What can you do with a file name once it is created?  You call
    fnmParse() to get the whole file name or parts of the file name
    as a string:

        #include  "fnm_util.h"			-- Filename utilities.
        char  *s ;
        FileName  fname ;
        ...
        fname = fnmCreate ("host:/usr/who/myprog.c.001", NULL) ;
        s = fnmParse (fname, FnmPath) ;		-- "host:/usr/who/myprog.c.001"
        s = fnmParse (fname, FnmNode) ;		-- "host:"
        s = fnmParse (fname, FnmDirectory) ;	-- "/usr/who"
        s = fnmParse (fname, FnmFile) ;		-- "myprog.c.001"
        s = fnmParse (fname, FnmName) ;		-- "myprog"
        s = fnmParse (fname, FnmExtension) ;	-- ".c"
        s = fnmParse (fname, FnmVersion) ;	-- ".001"
        fnmDestroy (fname) ;

    Shorthand macros - fnmPath(), fnmNode(), etc. - are defined for each
    of the fnmParse() calls above.


Origins:

    The FNM_UTIL package is a repackaging of my FPARSE package, which was
    inspired by the VAX/VMS lexical function, F$PARSE().


Public Procedures:

    fnmBuild() - builds a pathname.
    fnmCreate() - creates a filename.
    fnmDestroy() - destroys a filename.
    fnmExists() - checks if a file exists.
    fnmParse() - parses a filename.

Macro Definitions:

    fnmPath() - returns a filename's full pathname.
    fnmNode() - returns the node from a filename.
    fnmDirectory() - returns the directory from a filename.
    fnmFile() - returns the file, extension, and version from a filename.
    fnmName() - returns the file from a filename.
    fnmExtension() - returns the extension from a filename.
    fnmVersion() - returns the version number from a filename.

Private Procedures:

    fnmFillParts() - fill in missing parts of a filename with defaults.
    fnmLocateParts() - locate the parts of a filename.
    fnmNew() - allocates and initializes a filename structure.

*******************************************************************************/


#include  <errno.h>			/* System error definitions. */
#include  <limits.h>			/* Maximum/minimum value definitions. */
#ifndef PATH_MAX
#    if defined(vms)
#        include  <nam.h>		/* RMS name block (NAM) definitions. */
#        define  PATH_MAX  NAM$C_MAXRSS
#    else
#        include  <sys/param.h>		/* System parameters. */
#        define  PATH_MAX  MAXPATHLEN
#    endif
#endif
#if __STDC__
#    include  <stdarg.h>		/* Variable-length argument lists. */
#else
#    include  <varargs.h>		/* Variable-length argument lists. */
#endif
#include  <stdio.h>			/* Standard I/O definitions. */
#include  <stdlib.h>			/* Standard C Library definitions. */
#include  <string.h>			/* C Library string functions. */
#if defined(VMS)
#    include  <file.h>			/* File definitions. */
#    include  <unixio.h>		/* VMS-emulation of UNIX I/O. */
#elif defined(VXWORKS)
#    include  <ioLib.h>			/* I/O library definitions. */
#    define  getcwd(path,size)  getwd (path)
#elif defined(WIN32)
#    include  <direct.h>		/* Directory control functions. */
#    include  <io.h>			/* Low-level I/O definitions. */
#else
#    include  <fcntl.h>			/* File control definitions. */
#    include  <unistd.h>		/* UNIX I/O definitions. */
#endif
#include  <sys/stat.h>			/* File status definitions. */

#include  "str_util.h"			/* String manipulation functions. */
#include  "vperror.h"			/* VPERROR() definitions. */
#include  "fnm_util.h"			/* Filename utilities. */


/*******************************************************************************
    File Name - contains the fully-expanded file specification as well as
        the individual components of the file specification.
*******************************************************************************/

typedef  struct  _FileName {
    char  *path ;			/* Fully-expanded file specification. */
    char  *node ;			/* "node:" */
    char  *directory ;			/* "/directory(ies)/" */
    char  *file ;			/* "name.extension.version" */
    char  *name ;			/* "name" */
    char  *extension ;			/* ".extension" */
    char  *version ;			/* ".version" */
}  _FileName ;


/*******************************************************************************
    Private Functions.
*******************************************************************************/

static  FileName  fnmFillParts (
#    if __STDC__
        const  FileName  name1,
        const  FileName  name2
#    endif
    ) ;

static  int  fnmLocateParts (
#    if __STDC__
        FileName  fileSpec
#    endif
    ) ;

static  FileName  fnmNew (
#    if __STDC__
        const  char  *pathname
#    endif
    ) ;

/*******************************************************************************

Procedure:

    fnmBuild ()

    Build a Pathname.


Purpose:

    The fnmBuild() function builds a pathname from one or more file
    specifications.  fnmBuild() is essentially an encapsulation of
    the following code fragment:

        char  pathname[PATH_MAX] ;
        FileName  fname = fnmCreate (fileSpec1, ..., fileSpecN, NULL) ;
        ...
        strcpy (pathname, fnmParse (fname, FnmPath)) ;
        fnmDestroy (fname) ;

    I got tired of coding up variations of this every time I needed to
    build a full pathname, so I made a copy of fnmCreate() and modified
    it to return a character-string pathname instead of a FileName handle.


    Invocation:

        pathname = fnmBuild (part, [fileSpec1, ..., fileSpecN,] NULL) ;

    where

        <part>		- I
            specifies which part of the file name you want returned:
                FnmPath - "node:/directory(ies)/name.extension.version"
                FnmNode - "node:"
                FnmDirectory - "/directory(ies)/"
                FnmFile - "name[.extension[.version]]"
                FnmName - "name"
                FnmExtension - ".extension"
                FnmVersion - ".version"
            (These enumerated values are defined in "fnm_util.h".)
        <fileSpec1>	- I
        <fileSpecN>	- I
            are the file specfications used to construct the resulting file
            name.  Each file specification is a UNIX pathname containing
            one or more of the components of a pathname (e.g., the directory,
            the extension, the version number, etc.).  Missing components in
            the result are filled in from the file specifications as they are
            examined in left-to-right order.  The NULL argument marks the end
            of the file specification list.
        <pathname>	- O
            returns the pathname constructed from the file specifications;
            "" is returned in the event of an error.  The returned string
            is private to this routine and it should be used or duplicated
            before calling fnmBuild() again.

*******************************************************************************/


char  *fnmBuild (

#    if  __STDC__
        FnmPart  part,
        const  char  *fileSpec,
        ...)
#    else
        part, fileSpec, va_alist)

        FnmPart  part ;
        char  *fileSpec ;
        va_dcl
#    endif

{    /* Local variables. */
    va_list  ap ;
    FileName  defaults, newResult, result ;
    int  status ;
    static  char  pathname[PATH_MAX] ;




/* Process each file specification in the argument list. */

#if __STDC__
    va_start (ap, fileSpec) ;
#else
    va_start (ap) ;
#endif

    result = NULL ;

    while (fileSpec != NULL) {

        defaults = fnmNew (fileSpec) ;
        if (defaults == NULL) {
            vperror ("(fnmBuild) Error creating defaults: %s\nfnmNew: ", fileSpec) ;
            return ("") ;
        }
        fnmLocateParts (defaults) ;

        newResult = fnmFillParts (result, defaults) ;
        status = errno ;
        if (result != NULL)  fnmDestroy (result) ;
        fnmDestroy (defaults) ;
        if (newResult == NULL) {
            errno = status ;
            vperror ("(fnmBuild) Error creating intermediate result.\nfnmNew: ") ;
            return ("") ;
        }
        result = newResult ;

        fileSpec = va_arg (ap, const char *) ;

    }

    va_end (ap) ;


/* Fill in missing fields with the system defaults. */

    getcwd (pathname, sizeof pathname) ;
    strcat (pathname, "/") ;
    defaults = fnmNew (pathname) ;
    if (defaults == NULL) {
        vperror ("(fnmBuild) Error creating system defaults: %s\nfnmNew: ", pathname) ;
        return ("") ;
    }
    fnmLocateParts (defaults) ;

    newResult = fnmFillParts (result, defaults) ;
    status = errno ;
    if (result != NULL)  fnmDestroy (result) ;
    fnmDestroy (defaults) ;
    if (newResult == NULL) {
        errno = status ;
        vperror ("(fnmBuild) Error creating final result.\nfnmNew: ") ;
        return ("") ;
    }
    result = newResult ;


/* Return the full pathname to the caller. */

    strcpy (pathname, fnmParse (result, part)) ;
    fnmDestroy (result) ;

    return (pathname) ;

}

/*******************************************************************************

Procedure:

    fnmCreate ()

    Create a File Name.


Purpose:

    The fnmCreate() function creates a file name.


    Invocation:

        fileName = fnmCreate ([fileSpec1, ..., fileSpecN,] NULL) ;

    where

        <fileSpec1>	- I
        <fileSpecN>	- I
            are the file specfications used to construct the resulting file
            name.  Each file specification is a UNIX pathname containing
            one or more of the components of a pathname (e.g., the directory,
            the extension, the version number, etc.).  Missing components in
            the result are filled in from the file specifications as they are
            examined in left-to-right order.  The NULL argument marks the end
            of the file specification list.
        <fileName>	- O
            returns a handle that can be used in other FNM_UTIL calls.  NULL
            is returned in the event of an error.

*******************************************************************************/


FileName  fnmCreate (

#    if  __STDC__
        const  char  *fileSpec,
        ...)
#    else
        fileSpec, va_alist)

        char  *fileSpec ;
        va_dcl
#    endif

{    /* Local variables. */
    va_list  ap ;
    char  pathname[PATH_MAX] ;
    FileName  defaults, newResult, result ;
    int  status ;




/* Process each file specification in the argument list. */

#if __STDC__
    va_start (ap, fileSpec) ;
#else
    va_start (ap) ;
#endif

    result = NULL ;

    while (fileSpec != NULL) {

        defaults = fnmNew (fileSpec) ;
        if (defaults == NULL) {
            vperror ("(fnmCreate) Error creating defaults: %s\nfnmNew: ", fileSpec) ;
            return (NULL) ;
        }
        fnmLocateParts (defaults) ;

        newResult = fnmFillParts (result, defaults) ;
        status = errno ;
        if (result != NULL)  fnmDestroy (result) ;
        fnmDestroy (defaults) ;
        if (newResult == NULL) {
            errno = status ;
            vperror ("(fnmCreate) Error creating intermediate result.\nfnmNew: ") ;
            return (NULL) ;
        }
        result = newResult ;

        fileSpec = va_arg (ap, const char *) ;

    }

    va_end (ap) ;


/* Fill in missing fields with the system defaults. */

    getcwd (pathname, sizeof pathname) ;
    strcat (pathname, "/") ;
    defaults = fnmNew (pathname) ;
    if (defaults == NULL) {
        vperror ("(fnmCreate) Error creating system defaults: %s\nfnmNew: ", pathname) ;
        return (NULL) ;
    }
    fnmLocateParts (defaults) ;

    newResult = fnmFillParts (result, defaults) ;
    status = errno ;
    if (result != NULL)  fnmDestroy (result) ;
    fnmDestroy (defaults) ;
    if (newResult == NULL) {
        errno = status ;
        vperror ("(fnmCreate) Error creating final result.\nfnmNew: ") ;
        return (NULL) ;
    }
    result = newResult ;


    return (result) ;

}

/*******************************************************************************

Procedure:

    fnmDestroy ()

    Destroy a File Name.


Purpose:

    The fnmDestroy() function destroys a file name.


    Invocation:

        status = fnmDestroy (fileName) ;

    where

        <fileName>	- I
            is the file name handle returned by fnmCreate().
        <status>	- O
            returns the status of destroying the file name, zero if
            there were no errors and ERRNO otherwise.

*******************************************************************************/


int  fnmDestroy (

#    if  __STDC__
        FileName  fileName)
#    else
        fileName)

        FileName  fileName ;
#    endif

{

    if (fileName == NULL) {
        errno = EINVAL ;
        vperror ("(fnmDestroy) NULL file handle: ") ;
        return (errno) ;
    }

    if (fileName->path != NULL)  free (fileName->path) ;
    if (fileName->node != NULL)  free (fileName->node) ;
    if (fileName->directory != NULL)  free (fileName->directory) ;
    if (fileName->file != NULL)  free (fileName->file) ;
    if (fileName->name != NULL)  free (fileName->name) ;
    if (fileName->extension != NULL)  free (fileName->extension) ;
    if (fileName->version != NULL)  free (fileName->version) ;

    free (fileName) ;

    return (0) ;

}

/*******************************************************************************

Procedure:

    fnmExists ()

    Check If a File Exists.


Purpose:

    The fnmExists() function checks to see if the file referenced by a file
    name actually exists.


    Invocation:

        exists = fnmExists (fileName) ;

    where

        <fileName>	- I
            is the file name handle returned by fnmCreate().
        <exists>	- O
            returns true (1) if the referenced file exists and false (0)
            if it doesn't exist.

*******************************************************************************/


int  fnmExists (

#    if  __STDC__
        const  FileName  fileName)
#    else
        fileName)

        FileName  fileName ;
#    endif

{    /* Local variables. */
    struct  stat  fileInfo ;



    if (fileName == NULL) {
        errno = EINVAL ;
        vperror ("(fnmExists) NULL file handle: ") ;
        return (0) ;
    }

    if (stat (fileName->path, &fileInfo)) {
        switch (errno) {
        case EACCES:				/* Expected errors. */
        case ENOENT:
        case ENOTDIR:
            break ;
        default:				/* Unexpected errors. */
            vperror ("(fnmExists) Error getting information for %s.\nstat: ",
                     fileName->path) ;
        }
        return (0) ;				/* Not found. */
    }

    return (1) ;				/* Found. */

}

/*******************************************************************************

Procedure:

    fnmParse ()

    Parse a File Name.


Purpose:

    The fnmParse() function returns the requested part of a file name, e.g.,
    the directory, the name, the extension, etc.


    Invocation:

        value = fnmParse (fileName, part) ;

    where

        <fileName>	- I
            is the file name handle returned by fnmCreate().
        <part>		- I
            specifies which part of the file name you want returned:
                FnmPath - "node:/directory(ies)/name.extension.version"
                FnmNode - "node:"
                FnmDirectory - "/directory(ies)/"
                FnmFile - "name[.extension[.version]]"
                FnmName - "name"
                FnmExtension - ".extension"
                FnmVersion - ".version"
            (These enumerated values are defined in "fnm_util.h".)
        <value>		- O
            returns the requested part of the file name; "" is returned
            in the event of an error or if the requested part is missing.
            The returned string is private to the file name and it should
            not be modified or deleted; it should not be used after the
            file name is deleted.

*******************************************************************************/


char  *fnmParse (

#    if  __STDC__
        const  FileName  fileName,
        FnmPart  part)
#    else
        fileName, part)

        FileName  fileName ;
        FnmPart  part ;
#    endif

{

    if (fileName == NULL) {
        errno = EINVAL ;
        vperror ("(fnmParse) NULL file handle: ") ;
        return ("") ;
    }

    switch (part) {
    case FnmPath:
        return ((fileName->path == NULL) ? "" : fileName->path) ;
    case FnmNode:
        return ((fileName->node == NULL) ? "" : fileName->node) ;
    case FnmDirectory:
        return ((fileName->directory == NULL) ? "" : fileName->directory) ;
    case FnmFile:
        return ((fileName->file == NULL) ? "" : fileName->file) ;
    case FnmName:
        return ((fileName->name == NULL) ? "" : fileName->name) ;
    case FnmExtension:
        return ((fileName->extension == NULL) ? "" : fileName->extension) ;
    case FnmVersion:
        return ((fileName->version == NULL) ? "" : fileName->version) ;
    default:
        return ("") ;
    }

}

/*******************************************************************************

Procedure:

    fnmFillParts ()

    Fill the Missing Parts of a File Name with Defaults.


Purpose:

    Function fnmFillParts() fills the missing parts of a file name with the
    corresponding parts from a defaults file name.


    Invocation:

        result = fnmFillParts (fileName, defaults) ;

    where

        <fileName>	- I
            is the handle returned by fnmCreate() for the file name in question.
        <defaults>	- I
            is the handle returned by fnmCreate() for the file name containing
            the defaults.
        <result>	- O
            returns a handle for a new file name consisting of the old file
            name with missing parts supplied by the defaults file name.  NULL
            is returned in the event of an error.

*******************************************************************************/


static  FileName  fnmFillParts (

#    if  __STDC__
        const  FileName  fileName,
        const  FileName  defaults)
#    else
        fileName, defaults)

        FileName  fileName ;
        FileName  defaults ;
#    endif

{    /* Local variables. */
    char  *ddef, *dnew, pathname[PATH_MAX] ;
    FileName  result ;
    int  ldef, lnew ;





    strcpy (pathname, "") ;


/* Substitute the node name. */

    if ((fileName == NULL) || (fileName->node == NULL)) {
        if (defaults->node != NULL)  strcat (pathname, defaults->node) ;
    } else {
        strcat (pathname, fileName->node) ;
    }


/* Substitute the directory.  First, process dot directories in the
   new file specification ("fileName").  Single dots (current directory)
   are replaced by the current working directory; double dots (parent
   directory) remove successive child directories from the default
   file specfication ("defaults").  Dot directories in the default
   FS have no effect, unless the new FS has no directory yet. */

    dnew = ((fileName == NULL) || (fileName->directory == NULL))
           ? "" : fileName->directory ;
    lnew = strlen (dnew) ;
    ddef = (defaults->directory == NULL) ? "" : defaults->directory ;
    ldef = strlen (ddef) ;
    ddef = defaults->directory + ldef ;		/* DDEF points at '\0'. */

/* Prior to loop:  DDEF points to end of (N+1)-th component of directory.
   Be careful making changes to this code - it's not very straightforward.
   It should handle cases like the "/" directory, no dot directories, and
   so on. */

    while ((lnew > 0) && (ldef > 0)) {
        if (strcmp (dnew, ".") == 0) {			/* Current directory. */
            ldef = 0 ;
        } else if (strncmp (dnew, "./", 2) == 0) {	/* Current directory. */
            ldef = 0 ;
        } else if (strcmp (dnew, "..") == 0) {		/* Up one directory. */
            dnew = dnew + 2 ;  lnew = lnew - 2 ;
            do { ldef-- ; } while ((ldef > 0) && (*--ddef != '/')) ;
        } else if (strncmp (dnew, "../", 3) == 0) {	/* Up one directory. */
            dnew = dnew + 3 ;  lnew = lnew - 3 ;
            do { ldef-- ; } while ((ldef > 0) && (*--ddef != '/')) ;
        } else {					/* No dot directory. */
            break ;
        }
    }

/* After loop:  DDEF points to end of (N+1-M)-th component of directory,
   where M is the number of ".." (parent) directories processed.  Get rid
   of the "+1"-th component. */

    while ((ldef > 0) && (*--ddef != '/'))  ldef-- ;

    ddef = (defaults->directory == NULL) ? "" : defaults->directory ;

/* After processing the dot directories, perform the actual directory
   substitutions.  This procedure is complicated by the two types of
   directories, absolute and relative.  If the new directory and the default
   directory are both absolute or both relative, use the new directory.
   If one directory is relative and the other absolute, append the relative
   directory to the absolute directory. */

    if (lnew == 0) {			/* No previous directory spec. */
        strncat (pathname, ddef, ldef) ;
    }
    else if ((strcmp  (dnew, ".") == 0) ||
             (strncmp (dnew, "./", 2) == 0)) {
	/* Dot directories in default FS won't have any effect; use new FS. */
        getcwd (&pathname[strlen (pathname)],
                sizeof pathname - strlen (pathname)) ;
        strcat (pathname, "/") ;
    }
    else if ((strcmp  (dnew, "..") == 0) ||
             (strncmp (dnew, "../", 3) == 0)) {
	/* Dot directories in default FS won't have any effect; use new FS. */
        strncat (pathname, dnew, lnew) ;
    }
    else if (*ddef == '/') {
        if (*dnew == '/')		/* Two absolute directory specs. */
            strncat (pathname, dnew, lnew) ;
        else {				/* Append relative to absolute. */
            strncat (pathname, ddef, ldef) ;
            strncat (pathname, dnew, lnew) ;
        }
    }
    else {
        if (*dnew == '/') {		/* Append relative to absolute. */
            strncat (pathname, dnew, lnew) ;
            strncat (pathname, ddef, ldef) ;
        }
        else				/* Two relative directory specs. */
            strncat (pathname, dnew, lnew) ;
    }


/* Substitute the file name. */

    if ((fileName == NULL) || (fileName->name == NULL)) {
        if (defaults->name != NULL)  strcat (pathname, defaults->name) ;
    } else {
        strcat (pathname, fileName->name) ;
    }


/* Substitute the extension. */

    if ((fileName == NULL) || (fileName->extension == NULL)) {
        if (defaults->extension != NULL)
            strcat (pathname, defaults->extension) ;
    } else {
        strcat (pathname, fileName->extension) ;
    }


/* Substitute the version number. */

    if ((fileName == NULL) || (fileName->version == NULL)) {
        if (defaults->version != NULL)  strcat (pathname, defaults->version) ;
    } else {
        strcat (pathname, fileName->version) ;
    }


/* Construct a file name structure for the resulting file name. */

    result = fnmNew (pathname) ;
    if (result == NULL) {
        vperror ("(fnmFillParts) Error creating result: %s\nfnmNew: ", pathname) ;
        return (NULL) ;
    }
    fnmLocateParts (result) ;


    return (result) ;

}

/*******************************************************************************

Procedure:

    fnmLocateParts ()

    Locate the Parts of a File Name.


Purpose:

    The fnmLocateParts() function determines the locations of the different
    parts of a file name, e.g., the directory, the name, the extension, etc.


    Invocation:

        status = fnmLocateParts (fileName) ;

    where

        <fileName>	- I
            is the file name handle returned by fnmCreate().
        <status>	- O
            returns the status of dissecting the file name, zero if there
            were no errors and ERRNO otherwise.

*******************************************************************************/


static  int  fnmLocateParts (

#    if  __STDC__
        FileName  fileName)
#    else
        fileName)

        FileName  fileName ;
#    endif

{    /* Local variables. */
    char  *fs, pathname[PATH_MAX], *s ;





/* Advance the "fs" pointer as you scan the file specification. */

    strcpy (pathname, fileName->path) ;
    fs = pathname ;

/* First, if the file specification contains multiple pathnames, separated
   by commas or spaces, discard the trailing pathnames. */

    if ((s = strchr (fs, ' ')) != NULL)  *s = '\0' ;
    if ((s = strchr (fs, ',')) != NULL)  *s = '\0' ;

/* Locate the node.  The node name is separated from the rest of the file
   name by a colon (":"). */

    if ((s = strchr (fs, ':')) != NULL) {
        fileName->node = strndup (fs, (int) (s - fs + 1)) ;
        if (fileName->node == NULL) {
            vperror ("(fnmLocateParts) Error duplicating node of %s.\nstrndup: ",
                     fileName->path) ;
            return (errno) ;
        }
        fs = ++s ;
    }

/* Locate the directory.  The directory extends through the last "/" in the
   file name. */

    if ((s = strrchr (fs, '/')) != NULL) {
        fileName->directory = strndup (fs, (int) (s - fs + 1)) ;
        if (fileName->directory == NULL) {
            vperror ("(fnmLocateParts) Error duplicating directory of %s.\nstrndup: ",
                     fileName->path) ;
            return (errno) ;
        }
        fs = ++s ;
    }

/* The remainder of the pathname is the combined name, extension, and
   version number. */

    if (*fs != '\0') {
        fileName->file = strdup (fs) ;
        if (fileName->file == NULL) {
            vperror ("(fnmLocateParts) Error duplicating file of %s.\nstrdup: ",
                     fileName->path) ;
            return (errno) ;
        }
    }

/* Locate the version number.  Since version numbers are not part of
   UNIX, these version numbers are a user convention.  Any file extension
   that can be converted to an integer is considered a version number;
   e.g., ".007", etc.  (So we can make this test, a version number of zero
   is not allowed. */

    if (((s = strrchr (fs, '.')) != NULL) && (atoi (++s) != 0)) {
        fileName->version = strdup (--s) ;
        if (fileName->version == NULL) {
            vperror ("(fnmLocateParts) Error duplicating version of %s.\nstrdup: ",
                     fileName->path) ;
            return (errno) ;
        }
        *s = '\0' ;			/* Exclude version temporarily. */
    }

/* Locate the extension.  The extension is the last part of the file name
   preceded by a "." (not including the version number, though). */

    if ((s = strrchr (fs, '.')) != NULL) {
        fileName->extension = strdup (s) ;
        if (fileName->extension == NULL) {
            vperror ("(fnmLocateParts) Error duplicating extension of %s.\nstrdup: ",
                     fileName->path) ;
            return (errno) ;
        }
        *s = '\0' ;			/* Exclude extension temporarily. */
    }

/* Locate the name.  The name is the rest of the file name, excluding the
   last extension and the version number, if any. */

    if (*fs != '\0') {
        fileName->name = strdup (fs) ;
        if (fileName->name == NULL) {
            vperror ("(fnmLocateParts) Error duplicating name of %s.\nstrdup: ",
                     fileName->path) ;
            return (errno) ;
        }
    }

/* Restore extension and version. */

    if (fileName->extension != NULL)  fs[strlen (fs)] = '.' ;
    if (fileName->version != NULL)  fs[strlen (fs)] = '.' ;

    return (0) ;

}

/*******************************************************************************

Procedure:

    fnmNew ()

    Allocates a File Name Structure.


Purpose:

    The fnmNew() function allocates and initializes a file name structure.


    Invocation:

        fileName = fnmNew (pathname) ;

    where

        <pathname>	- I
            is the initial pathname.
        <fileName>	- O
            returns a pointer to the allocated file name structure.  The
            caller is responible for FREE(3)ing the structure when it is
            no longer needed.  NULL is returned in the event of an error.

*******************************************************************************/


static  FileName  fnmNew (

#    if  __STDC__
        const  char  *pathname)
#    else
        pathname)

        char  *pathname ;
#    endif

{    /* Local variables. */
    char  expandedPathname[PATH_MAX] ;
    FileName  fileName ;
    int  status ;




/* Allocate the file name structure. */

    fileName = (FileName) malloc (sizeof (_FileName)) ;
    if (fileName == NULL) {
        vperror ("(fnmNew) Error allocating structure for %s.\n", pathname) ;
        return (NULL) ;
    }

/* Initialize the structure. */

    if (pathname == NULL) {
        fileName->path = NULL ;
    } else {
        strEnv (pathname, -1, expandedPathname, sizeof expandedPathname) ;
        fileName->path = strdup (expandedPathname) ;
        if (fileName->path == NULL) {
            vperror ("(fnmNew) Error duplicating pathname: %s\n",
                     expandedPathname) ;
            status = errno ;  free (fileName) ;  errno = status ;
            return (NULL) ;
        }
    }

    fileName->node = NULL ;
    fileName->directory = NULL ;
    fileName->file = NULL ;
    fileName->name = NULL ;
    fileName->extension = NULL ;
    fileName->version = NULL ;

    return (fileName) ;

}
