/*
@(#)  FILE: mdl_util.h  RELEASE: 1.1  DATE: 01/06/97, 16:37:40
*/
/*******************************************************************************

    mdl_util.h

    Module Definition/Reference Utility definitions.

*******************************************************************************/

#ifndef  MDL_UTIL_H		/* Has the file been INCLUDE'd already? */
#define  MDL_UTIL_H  yes

#ifdef __cplusplus
    extern  "C" {
#endif


#include  "ansi_setup.h"		/* ANSI or non-ANSI C? */


/*******************************************************************************
    Module (Client View) and Definitions.
*******************************************************************************/

typedef  struct  _Module  *Module ;	/* Module handle. */


/*******************************************************************************
    Miscellaneous declarations.
*******************************************************************************/

extern  int  mdl_util_debug ;		/* Global debug switch (1/0 = yes/no). */


/*******************************************************************************
    Public Functions
*******************************************************************************/

extern  char  *mdlClone P_((const char *module)) ;

extern  int  mdlCompare P_((const char *m1,
                            const char *m2)) ;

extern  int  gscCreate P_((int (*compare)(const char *, const char *),
                           char *(*duplicate)(const char *),
                           void (*delete)(void *),
                           char *(*display)(const char *),
                           Graph *graph)) ;

extern  int  gscDelete P_((Graph graph)) ;

extern  int  gscDump P_((FILE *file,
                         const char *header,
                         Graph graph)) ;

extern  int  gscFirst P_((Graph graph,
                          char **name,
                          int *depth,
                          GscVisitStatus *visit)) ;

extern  int  gscMark P_((Graph graph,
                         const char *root,
                         int bfs)) ;

extern  int  gscNext P_((Graph graph,
                         char **name,
                         int *depth,
                         GscVisitStatus *visit)) ;

extern  char  *gscRoot P_((Graph graph,
                           int which)) ;


#ifdef __cplusplus
    }
#endif

#endif				/* If this file was not INCLUDE'd previously. */
