/*
@(#)  FILE: gsc_util.h  RELEASE: 1.4  DATE: 02/29/96, 14:29:57
*/
/*******************************************************************************

    gsc_util.h

    Graph/Structure Chart Utility definitions.

*******************************************************************************/

#ifndef  GSC_UTIL_H		/* Has the file been INCLUDE'd already? */
#define  GSC_UTIL_H  yes

#ifdef __cplusplus
    extern  "C" {
#endif


#include  "ansi_setup.h"		/* ANSI or non-ANSI C? */


/*******************************************************************************
    Graph (Client View) and Definitions.
*******************************************************************************/

typedef  struct  _Graph  *Graph ;	/* Graph handle. */

typedef  enum  GscVisitStatus {
    GscFIRST,				/* First visit to this vertex. */
    GscPREVIOUS,			/* This vertex has already been visited. */
    GscRECURSIVE			/* This vertex is part of a cycle in the graph. */
}  GscVisitStatus ;


/*******************************************************************************
    Miscellaneous declarations.
*******************************************************************************/

extern  int  gsc_util_debug ;		/* Global debug switch (1/0 = yes/no). */


/*******************************************************************************
    Public Functions
*******************************************************************************/

extern  int  gscAdd P_((Graph graph,
                        const char *vertex_1,
                        const char *vertex_2)) ;

extern  int  gscCreate P_((int (*compare)(const char *, const char *),
                           char *(*duplicate)(const char *),
                           void (*delete)(void *),
                           char *(*display)(const char *),
                           Graph *graph)) ;

extern  int  gscDelete P_((Graph graph)) ;

extern  int  gscDump P_((FILE *file,
                         const char *header,
                         Graph graph)) ;

extern  int  gscFirst P_((Graph graph,
                          char **name,
                          int *depth,
                          GscVisitStatus *visit)) ;

extern  int  gscMark P_((Graph graph,
                         const char *root,
                         int bfs)) ;

extern  int  gscNext P_((Graph graph,
                         char **name,
                         int *depth,
                         GscVisitStatus *visit)) ;

extern  char  *gscRoot P_((Graph graph,
                           int which)) ;


#ifdef __cplusplus
    }
#endif

#endif				/* If this file was not INCLUDE'd previously. */
