/*
@(#)  FILE: fnm_util.h  RELEASE: 1.5  DATE: 07/12/96, 15:44:47
*/
/*******************************************************************************

    fnm_util.h

    Filename Utility Definitions.

*******************************************************************************/

#ifndef  FNM_UTIL_H		/* Has the file been INCLUDE'd already? */
#define  FNM_UTIL_H  yes

#ifdef __cplusplus
    extern  "C" {
#endif


#include  "ansi_setup.h"		/* ANSI or non-ANSI C? */


/*******************************************************************************
    Filename (Client View) and Definitions.
*******************************************************************************/

typedef  struct  _FileName  *FileName ;	/* Filename handle. */

typedef  enum  FnmPart {
    FnmPath = 0,			/* The whole pathname. */
    FnmNode,				/* Node name, if specified. */
    FnmDirectory,			/* Directory only. */
    FnmFile,				/* Name, extension, and version. */
    FnmName,				/* File name. */
    FnmExtension,			/* File extension. */
    FnmVersion				/* Version number. */
}  FnmPart ;


/*******************************************************************************
    Public functions.
*******************************************************************************/

extern  char  *fnmBuild P_((FnmPart part, const char *path, ...)) ;

extern  FileName  fnmCreate P_((const char *path, ...)) ;

extern  int  fnmDestroy P_((FileName filename)) ;

extern  int  fnmExists P_((const FileName filename)) ;

extern  char  *fnmParse P_((const FileName fname, FnmPart part)) ;

#define  fnmPath(fname)  fnmParse (fname, FnmPath)
#define  fnmNode(fname)  fnmParse (fname, FnmNode)
#define  fnmDirectory(fname)  fnmParse (fname, FnmDirectory)
#define  fnmFile(fname)  fnmParse (fname, FnmFile)
#define  fnmName(fname)  fnmParse (fname, FnmName)
#define  fnmExtension(fname)  fnmParse (fname, FnmExtension)
#define  fnmVersion(fname)  fnmParse (fname, FnmVersion)


#ifdef __cplusplus
    }
#endif

#endif				/* If this file was not INCLUDE'd previously. */
