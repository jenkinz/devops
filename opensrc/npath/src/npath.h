/*
@(#)  FILE: npath.h  RELEASE: 1.1  DATE: 01/06/97, 16:37:40
*/
/*******************************************************************************

    npath.h

    NPATH Definitions.

*******************************************************************************/

#ifndef  NPATH_H		/* Has the file been INCLUDE'd already? */
#define  NPATH_H  yes


#include  <stdio.h>			/* Standard I/O definitions. */
#include  "ansi_setup.h"		/* ANSI or non-ANSI C? */
#include  "gsc_util.h"			/* Graph/structure chart definitions. */
#include  "hash_util.h"			/* Hash table definitions. */
#include  "list_util.h"			/* List manipulation functions. */


/*******************************************************************************
    LEX/YACC Token Definitions.
*******************************************************************************/

    typedef  struct  Token  {
        char  *name ;			/* Identifier names. */
        int  npath ;			/* Accumulates NPATH counts. */
        void  *save ;			/* For miscellaneous data. */
    }  Token ;

#define  YYSTYPE  Token

    extern  YYSTYPE  yylval ;		/* Communicates token from LEX to YACC. */


/*******************************************************************************
    Module list - is a list of the functions in the current source file.
*******************************************************************************/

    typedef  struct  ModuleMetrics {
        char  *name ;			/* Function name. */
        int  num_NCSL ;			/* Count of non-comment source lines. */
        int  num_unique_operators ;	/* Number of unique operators. */
        int  num_unique_operands ;	/* Number of unique operands. */
        int  total_num_operators ;	/* Total number of operators. */
        int  total_num_operands ;	/* Total number of operands. */
        int  v_of_g ;			/* McCabe's V(G). */
        int  npath ;			/* NPATH figure. */
    }  ModuleMetrics ;


/*******************************************************************************
    Global variables.
*******************************************************************************/

#ifdef  MAIN
    Graph  callGraph = NULL ;		/* Call graph for structure chart. */
    int  cflow = 0 ;			/* Generate CFLOW(1) calling hierarchy. */
    int  cplplSource = 0 ;		/* C++ source? */
    int  echoInput = 0 ;		/* Echo source file input? */
    HashTable  htable_exclude = NULL ;	/* Hash table for excluded names. */
    HashTable  htable_operands = NULL ;	/* Hash table for operands. */
    HashTable  htable_operators = NULL ; /* Hash table for operators. */
    HashTable  htable_types = NULL ;	/* Hash table for type names. */
    int  is_typedef ;			/* Processing a type definition? */
    ModuleMetrics  *m = NULL ;		/* The current module. */
    List  moduleList = NULL ;		/* List of functions in file. */
    int  npath_debug = 0 ;		/* Display intermediate NPATH values? */
    int  numTokens ;			/* # of tokens scanned in current function. */
    char  *sourceFile ;			/* Source file name. */
    int  type_name_as_identifier ;	/* Treat "typedef"ed name as normal identifier? */
#else
    extern  Graph  callGraph ;
    extern  int  cflow ;
    extern  int  cplplSource ;
    extern  int  echoInput ;
    extern  HashTable  htable_exclude ;
    extern  HashTable  htable_operands ;
    extern  HashTable  htable_operators ;
    extern  HashTable  htable_types ;
    extern  int  is_typedef ;
    extern  ModuleMetrics  *m ;
    extern  List  moduleList ;
    extern  int  npath_debug ;
    extern  int  numTokens ;
    extern  char  *sourceFile ;
    extern  int  type_name_as_identifier ;
#endif

/*******************************************************************************
    Function prototypes and external definitions for TSTOL functions.
*******************************************************************************/

/* [NPATH]NPATH.C */
extern  int  main P_((int argc, char *argv[], char *envp[])) ;

/* [NPATH]NPATH_LEX.C */
extern  int  yylex P_((void)) ;
extern  int  check_operand P_((char *operand)) ;
extern  int  check_operator P_((char *operator)) ;
extern  void  yyrestart P_((FILE *input_file)) ;

/* [NPATH]NPATH_YACC.C */
extern int yyparse P_((void)) ;
extern int yyerror P_((const char *s)) ;


#endif				/* If this file was not INCLUDE'd previously. */
