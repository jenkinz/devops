How to build:
omake [-a] will build on both Windows and Linux
see makefile. Yes you'll need Opus Make but that should be installed and
ready on rex - see sysadmin for rex

Ignore other Makefiles - just use omake to implicitly use makefile and it will 
compile and link. 
Note that I don't use Makefile anymore since windows doesn't recognize case 
- makefile is more portable. omake works on Linux and Windows but you have to 
install the mingw gcc compiler. 
Do not use g++ - it doesn't work w/ this code - you must use gcc.
I can probably use other Windows compilers like Microsoft and Borland 5.5 (free)
but gcc works and its just a utility.

npath.tar.Z is the original source code downloaded from
http://www.geonius.com/software/tools/npath.html

However it had constructs that Windows did not like so I had to make some 
minor modifications you can find with grep AMS *.c
My changes do not affect the performance or use of the code - they were minor
porting requirements to move this tool to Windows.

Andrew M. Scheurer (AMS)


