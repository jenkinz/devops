/*
@(#)  FILE: hash_util.h  RELEASE: 1.5  DATE: 07/25/96, 15:05:29
*/
/*******************************************************************************

    hash_util.h

    Hash Table Definitions.

*******************************************************************************/

#ifndef  HASH_UTIL_H		/* Has the file been INCLUDE'd already? */
#define  HASH_UTIL_H  yes

#ifdef __cplusplus
    extern  "C" {
#endif


#include  <stdio.h>			/* Standard I/O definitions. */
#include  "ansi_setup.h"		/* ANSI or non-ANSI C? */


/*******************************************************************************
    Hash Table Structures (Client View) and Definitions.
*******************************************************************************/

typedef  struct  _HashTable  *HashTable ;


/*******************************************************************************
    Miscellaneous declarations.
*******************************************************************************/

extern  int  hash_util_debug ;		/* Global debug switch (1/0 = yes/no). */


/*******************************************************************************
    Public functions.
*******************************************************************************/

extern  int  hashAdd P_((HashTable table,
                         const char *key,
                         const void *data)) ;

extern  int  hashCreate P_((int maxEntries,
                            int debug,
                            HashTable *table)) ;

extern  int  hashDelete P_((HashTable table,
                            const char *key)) ;

extern  int  hashDestroy P_((HashTable table)) ;

extern  int  hashDump P_((FILE *outfile,
                          const char *header,
                          HashTable table)) ;

extern  int  hashSearch P_((HashTable table,
                            const char *key,
                            void **data)) ;

extern  int  hashStatistics P_((FILE *outfile,
                                HashTable table)) ;


#ifdef __cplusplus
    }
#endif

#endif				/* If this file was not INCLUDE'd previously. */
