/*
@(#)  FILE: npath.c  RELEASE: 1.1  DATE: 01/06/97, 16:37:39
*/
/**************************************************************************

Process:

    npath

    Compute NPATH Values for C Source Files.


Author:    Alex Measday, ISI


Purpose:

    Program NPATH computes various measures of software complexity for
    C Language source files.  For each of the input C source files,
    NPATH pipes the source file through the C Preprocessor (CPP).
    The output from CPP is input by NPATH, parsed, and the complexity
    statistics computed.  For example, the statistics for one of the
    source files in the TPOCC library look as follows:

                                  NCSL   Volume    V(G)   NPATH   CLOC
                                  ----   ------   ------  -----  ------

        LIBALEX.C;1:
            date_and_time           3      186        1      1     0.3
            str_detab              21      991        9    120     5.7
            str_dupl               21      883        7     54     2.6
            str_etoa                5     5008        3      4     0.8
            str_free               12      395        5      6     0.5
            str_index               9      404        5     16     1.8
            str_insert             26     1158       11    864    33.2
            str_lcat                6      264        3      4     0.7
            str_lcopy              11      460        4      8     0.7
            str_lowcase             6      279        4      8     1.3
            str_upcase              6      279        4      8     1.3
            str_match               6      208        3      3     0.5
            str_trim                9      362        4     20     2.2

        Summary - # of files: 1, # of modules: 13, # of NCSL: 141

    Underneath the file name (a VMS file name, in this case) is a list of
    all the functions defined in the file.  Following each function name
    are the following complexity figures:

        NCSL - is the number of non-comment source lines.

        VOLUME - is Halstead's volume measure derived from his Software
            Science metric.

        V(G) - is McCabe's cyclomatic complexity measure.

        NPATH - is the NPATH measure.

        CLOC - is the NPATH complexity per line-of-code.

    After all the input source files have been processed, NPATH outputs a
    summary line totaling the number of files processed, the modules, and
    the lines of code.

        NOTE that measurements are only made INSIDE FUNCTION BODIES.
        Declarations outside the body of a function are not included
        in the counts.

(a) "NCSL" is actually a misnomer for the first column of figures.  NPATH
    only counts the number of statements (excluding declarations) in the body
    of the function.  This is true for all the metrics measured by NPATH.

(b) Halstead's Software Science metric is based on the number of operators and
    operands in a function.  The "length" of a function is

            total number of operators + total number of operands

    The "vocabulary" of the function is

            number of unique operators + number of unique operands

    And, lastly, the "volume" of the function is defined as

                     length * log2 (vocabulary)

    The sticky thing about the Software Science metric is deciding which
    things in a language are operators and which things are operands.  NPATH
    uses the following conventions for the C language:

        Operators
        ---------

            break  case  continue  default  do  else  for
            goto  if  return  sizeof  switch  while

            function call           (Counts as one operator.)

            {} () []                (Each pair counts as one operator.)

            >>=  <<=  +=  -=  *=  /=  %=  &=  ^=  |=  >>  <<
            ++  --  ->  &&  ||  <=  >=  ==  !=  ;  ,  :  =  .
            &  !  ~ -  +  *  /  %  <  >  ^  |  ?

        Operands
        --------

            Identifiers  Numbers  Characters ('x')  Strings ("...")

    Note that function calls get counted twice, both as an operator and as
    an operand (because of the identifier).  It looked too difficult to do
    one and not the other.  By the time the parser knows its dealing with
    a function call, it's not completely clear what the function name is -
    remember, the stuff preceding the left parenthesis might be a complicated
    expression that produces a function pointer.

(c) McCabe's cyclomatic complexity metric, V(G), is basically the number
    of conditional statements (plus one) in a function.  NPATH counts the
    following statements when calculating V(G):

            case  default  if
            while  do  for

    Renaud (see NOTES below) suggests steering clear of routines with a V(G)
    greater than 10.

(d) The NPATH metric computes the number of possible execution paths through
    a function.  It takes into account the nesting of conditional statements
    and multi-part boolean expressions (e.g., A && B, C || D, etc.).  Nejmeh
    (see References below) says that his group had an informal NPATH limit
    of 200 on individual routines; functions that exceeded this value were
    candidates for further decomposition - or at least a closer look.

(e) The CLOC metric is simply a function's NPATH number divided by the number
    of executable statements (NCSL) in the function; i.e., it measures the
    complexity per line of code in the function.  Lower values of CLOC can
    mean one of two things: you write very clear code or your code doesn't
    do much of anything.  Higher values of CLOC can also mean one of two
    things: your code has lots of important things to do or you program in
    a very obtuse manner!


    References
    ----------

        Norman Salt, "Defining Software Science Counting Strategies",
            ACM SIGPLAN Notices, March 1982.

        Brian Nejmeh, "NPATH: A Measure of Execution Path Complexity and its
            Applications", in Communications of the ACM, Februrary 1988.

        ... and various other articles I can't recall.  Nejmeh's NPATH
            article provided the inspiration for this program; see the
            article for more information about how the NPATH metric
            measures up to the others.  Salt's Software Science article
            provides details on measuring the complexity of Pascal programs
            using Halstead's metric.  I haven't read any of the original
            literature by Halstead or McCabe; my understanding of their
            metrics is derived from the different articles on metrics that
            I've read (mostly in SIGPLAN and CACM).


    Notes
    -----

        (1) The NPATH program uses an ANSI C grammar and lexer posted on
            Internet by Jeff Lee of Georgia Tech.  Thank you.

        (2) I modified the lexer to read C Preprocessor output (as well as
            to help collect complexity statistics).  I added VAX C extensions
            to the grammar.  I had to do some finagling with the grammar and
            lexer to get them to accept type names used as identifiers.  C
            allows you to use a type name as a variable name or as the name
            of a field in a structure.  This was a royal headache, since the
            lexer would prefer to return TYPE_NAME tokens for type names and
            not IDENTIFIER tokens.  NPATH appears to handle these situations
            correctly.  The "comp.compilers" USENET group had a discussion
            about how hard it is parse C without writing a full-blown compiler
            - they were right!

        (3) I found out about Brian Renaud's METRIC suite (in Volume 20 of
            "comp.sources.unix") after writing my program.  This collection
            of tools measures delivered source instructions (like my NCSL),
            McCabe's metric, and Halstead's software science metric.  McCabe's
            metric is measured by an AWK(1) script, Halstead's by a LEX(1)
            program.  While my C parser-based NPATH program might seem more
            sophisticated than Renaud's tools, Renaud, unlike me, knows what
            he's talking about when it comes to metrics!  Using code from
            a large project, Renaud studied the relationships between the
            different metrics and the maintenance history of the code.

        (4) My selection of operators and operands in the C language is
            based on Salt's Pascal selection (see References above); a
            later article by someone else in COMPUTER LANGUAGE magazine
            made a similar selection for Pascal.  Designating statement
            terminators (";"), parentheses ("()"), braces ("{}"), etc.
            as "operators" seems a little strange, but Halstead himself
            apparently considered parentheses as operators.  Renaud's
            METRIC suite uses a different selection of operators; e.g.,
            he doesn't count statement terminators or braces.

        (5) The program has been compiled, linked, and run under UNIX (SunOS
            and HP/UX) and under VMS.  On the UNIX workstations, I used the
            standard LEX and YACC tools.  On the VAX, I used Bison for the
            parser and both FLEX and LEX for the lexer.

        (6) The program is not speedy on VAXes, which is largely due to the
            overhead in spawning the C compiler (in preprocessor mode) for
            each input file.  The spawning of "CC/PREPROCESS" is done by a
            version of POPEN(3) that I wrote for VMS.

        (7) The program is not speedy on UNIX when processing X Windows-based
            code.  There ought to be a law against the Xt and Motif header
            files!

        (8) The "-cflow" command line option generates a CFLOW(1)-style,
            textual structure chart.


    Invocation:

        % npath [-D...] [-I...] [-U...] [-nostdinc]
                [-cflow] [-cpp <executable>] [-c++] [-debug]
                [-echo] [-exclude <file>] [-full]
                [-long] [-longer]
                [-nocpp] [-noheading] [-npath_debug]
                [-verbose] [-verify <level>] [-vperror] [-yacc_debug]
                source_file(s)

    where

        "-D..."
        "-I..."
        "-U..."
        "-nostdinc"
            are C Preprocessor, CPP(1), options.  These are passed on to CPP.
        "-cflow"
            causes NPATH to construct a calling hierarchy from the source
            files and to write a CFLOW(1)-style structure chart to STDOUT.
        "-cpp <executable>"
            specifies the pathname of the C Preprocessor.  By default, this
            is "/lib/cpp" under UNIX and "CC/PREPROCESS_ONLY=SYS$OUTPUT"
            under VMS.
        "-c++"
            causes NPATH to recognize C++ keywords ...
        "-debug"
            enables debug output (written to STDERR).
        "-echo"
            causes the C source being scanned by the parser to be echoed to
            STDOUT.  The C source is actually the preprocessed source output
            by the C Preprocessor.
        "-exclude <file>"
            specifies the name of a file containing a list of functions that
            should be ignored when generating a structure chart.  Commonly-
            used library routines are candidates for this file.
        "-full"
            causes NPATH to give the full path name for a function's source
            file when generating a CFLOW(1)-style structure chart.  By default,
            NPATH only prints out the file name and extension.
        "-long"
            specifies that long module names will be encountered.  NPATH will
            shift the columns of numbers to the right by one tab stop so that
            the columns line up (more or less).
        "-longer"
            is like the "-long" option, except that the columns of number are
            shifted TWO tab stops to the right.
        "-nocpp"
            disables the preprocessing of source files.  Normally, the source
            files are run through the C Preprocessor.  With the "-p" option,
            the source files are read directly.
        "-noheading"
            inhibits the output of column headings.
        "-npath_debug"
            enables NPATH-related debug output (written to STDOUT).
        "-verbose"
            enables verbose mode.  In this mode, NPATH displays (to STDERR)
            the name of each file as the file is processed.  This is useful
            when you have redirected the module/statistics output (STDOUT)
            to a file.
        "-verify <level>"
            enables MALLOC(3) heap verification.  If LEVEL is 0 (the default),
            MALLOC(3) behaves normally.  If LEVEL is 1, the system checks the
            arguments passed to MALLOC(3) and the heap blocks immediately
            affected by each call.  If LEVEL is 2, the system verifies the
            integrity of the entire heap on each call to MALLOC(3).  Running
            at level 2 is useful for detecting inadvertent corruption of your
            allocated memory.  This option is only supported if NPATH is
            linked with my "libmalloc" library or, under SunOS, if NPATH is
            linked with the system's "/usr/lib/debug/malloc.o" file; see the
            SunOS documentation on MALLOC(3) for more information.
        "-vperror"
            turns VPERROR() message output on.  VPERROR() messages are
            low-level error messages generated by TPOCC library functions;
            normally, they are disabled.  If enabled, the messages are
            output to STDERR.
        "-yacc_debug"
            enables low-level, parser debug from the YACC-generated parser.
        "<source_file(s)>"
            is the list of C source files to read.  File names can be
            separated by commas or spaces.  VMS-style "sticky defaults"
            are NOT implemented.

**************************************************************************/

#ifdef sccs
    static  char  sccsid[] = "File: npath.c  Release: 1.1  Date: 01/06/97, 16:37:39" ;
#endif

#include  <errno.h>			/* System error definitions. */
#include  <math.h>			/* Math library definitions. */
#include  <stdio.h>			/* Standard I/O definitions. */
#include  <stdlib.h>			/* Standard C library definitions. */
#include  <string.h>			/* C Library string functions. */
#ifndef MALLOC_DEBUG
#    define  malloc_debug(level)  (level, fprintf (stderr, "npath: MALLOC debug is not supported.\n"))
#endif
#define  log2(value)  (log10 (value) / log10 (2.0))
#ifdef vms
#    include  "libutilvms.h"		/* LIBUTILVMS definitions. */
#endif
#include  "drs_util.h"			/* Directory scanning utilities. */
#include  "fnm_util.h"			/* Filename utilities. */
#include  "get_util.h"			/* "Get Next" functions. */
#include  "gsc_util.h"			/* Graph/structure chart definitions. */
#include  "hash_util.h"			/* Hash table definitions. */
#include  "opt_util.h"			/* Option scanning definitions. */
#include  "str_util.h"			/* String manipulation functions. */
#include  "vperror.h"			/* VPERROR() definitions. */
#define  MAIN
#include  "npath.h"			/* NPATH definitions. */
#undef  MAIN
#include  "cfl_util.h"			/* CFLOW(1) utility definitions. */

extern  int  yydebug ;			/* Enables parser debug output. */
extern  FILE  *yyin ;			/* Input to lexical scanner. */
extern  int  yylineno ;			/* Current line number in current source file. */

#define  MAX_INPUT  (4*1024)


extern FILE *yyout;     /* AMS - see npath_lex.c */

int  main (
#    if __STDC__
        int  argc,
        char  *argv[],
        char  *envp[])
#    else
        argc, argv, envp)

        int  argc ;
        char  *argv[] ;
        char  *envp[] ;
#    endif

{  /* Local variables. */
    char  *arg, *argument, buffer[MAX_INPUT] ;
    char  *cppExecutable, *cppOptions ;
    char  *name, *s ;
    char  *tabs = "\t\t\t\t\t" ;
    DirectoryScan  directory ;
    double  h_vocabulary, h_volume ;
    FILE  *infile ;
    FileName  excludeFile, fileName ;
    GscVisitStatus  visit ;
    int  cflowLine, debug, depth, errflg, firstFile, fullPathnames ;
    int  h_length, heading, i, length, line, longNames ;
    int  numFiles, numModules, option, sourceLine, totalNCSL ;
    int  useCPP, verbose, vperror_save ;
    List  cppOptionsList, fileList ;

    /* AMS --- no global initialization allowed in npath_lex.c using gcc */
    yyin = stdin;
    yyout = stdout;


    static  char  *option_list[] = {		/* Command line options. */
        "D:I:U:{nostdinc}",
        "{cflow}", "{cpp:}", "{c++}",
        "{debug}", "{echo}", "{exclude:}",
        "{full}", "{long}", "{longer}",
        "{nocpp}", "{noheading}", "{npath_debug}",
        "{structure}", "{verbose}", "{verify:}",
        "{vperror}", "{yacc_debug}", NULL
    } ;





    vperror_save = vperror_print ;	/* Save state of VPERROR_PRINT flag. */
    vperror_print = 1 ;			/* Enable VPERROR output during initialization. */


/*******************************************************************************
  Scan the command line options.
*******************************************************************************/

    debug = 0 ;  echoInput = 0 ;  npath_debug = 0 ;
#ifdef vms
    cppExecutable = "CC/PREPROCESS_ONLY=SYS$OUTPUT" ;
#else
    cppExecutable = "cpp" ;
#endif
    cppOptionsList = NULL ;  fileList = NULL ;
    excludeFile = NULL ;  fullPathnames = 0 ;
    heading = 1 ;  longNames = 0 ;
    cflow = 0 ;  cplplSource = 0 ;  useCPP = 1 ;  verbose = 0 ;

    opt_init (argc, argv, 1, option_list, NULL) ;
    errflg = 0 ;

    while ((option = opt_get (NULL, &argument))) {
        switch (option) {
#ifdef vms
				/* C Preprocessor (CPP) options. */
        case 1:			/* "-D <definition(s)>" */
            sprintf (buffer, "/DEFINE=(%s)", argument) ;
            listAdd (&cppOptionsList, -1, (void *) strdup (buffer)) ;
            break ;
        case 2:			/* "-I <directory>" */
            sprintf (buffer, "/INCLUDE=(%s)", argument) ;
            listAdd (&cppOptionsList, -1, (void *) strdup (buffer)) ;
            break ;
        case 3:			/* "-U <macro(s)>" */
            sprintf (buffer, "/UNDEFINE=(%s)", argument) ;
            listAdd (&cppOptionsList, -1, (void *) strdup (buffer)) ;
            break ;
        case 4:			/* "-nostdinc" */
            break ;
#else
				/* C Preprocessor (CPP) options. */
        case 1:			/* "-D <definition(s)>" */
            sprintf (buffer, "-D%s", argument) ;
            listAdd (&cppOptionsList, -1, (void *) strdup (buffer)) ;
            break ;
        case 2:			/* "-I <directory>" */
            sprintf (buffer, "-I%s", argument) ;
            listAdd (&cppOptionsList, -1, (void *) strdup (buffer)) ;
            break ;
        case 3:			/* "-U <macro(s)>" */
            sprintf (buffer, "-U%s", argument) ;
            listAdd (&cppOptionsList, -1, (void *) strdup (buffer)) ;
            break ;
        case 4:			/* "-nostdinc" */
            listAdd (&cppOptionsList, -1, (void *) strdup ("-nostdinc")) ;
            break ;
#endif
        case 5:			/* "-cflow" */
            cflow = 1 ;  break ;
        case 6:			/* "-cpp <executable>" */
            cppExecutable = argument ;  break ;
        case 7:			/* "-c++" */
            cplplSource = 1 ;  break ;
        case 8:			/* "-debug" */
            debug = 1 ;  cfl_util_debug = 1 ;  gsc_util_debug = 1 ;  break ;
        case 9:			/* "-echo" */
            echoInput = 1 ;  break ;
        case 10:		/* "-exclude <file>" */
            excludeFile = fnmCreate (argument, NULL) ;  break ;
        case 11:			/* "-full" */
            fullPathnames = 1 ;  break ;
        case 12:		/* "-long" */
            longNames = 1 ;  break ;
        case 13:		/* "-longer" */
            longNames = 2 ;  break ;
        case 14:		/* "-nocpp" */
            useCPP = 0 ;  break ;
        case 15:		/* "-noheading" */
            heading = 0 ;  break ;
        case 16:		/* "-npath_debug" */
            npath_debug = 1 ;  break ;
        case 17:		/* "-structure" (obsolete; use "-cflow") */
            cflow = 1 ;  break ;
        case 18:		/* "-verbose" */
            verbose = 1 ;  break ;
        case 19:		/* "-verify <level>" */
            malloc_debug (atoi (argument)) ;
            break ;
        case 20:		/* "-vperror" */
            vperror_save = 1 ;  break ;
        case 21:		/* "-yacc_debug" */
            yydebug = 1 ;  break ;
        case NONOPT:		/* Possibly a comma-separated list of files. */
            length = -1 ;  arg = argument ;
            while ((arg = getarg (arg, &length)) != NULL) {
                name = strndup (arg, length) ;
                fileName = fnmCreate (name, ".c", NULL) ;
                drsCreate (fnmPath (fileName), &directory) ;
                while ((s = drsNext (directory)) != NULL) {
                    listAdd (&fileList, -1, (void *) fnmCreate (s, NULL)) ;
                }
                drsDestroy (directory) ;
                fnmDestroy (fileName) ;
                free (name) ;
            }
            break ;
        case OPTERR:
            errflg++ ;  break ;
        default :  break ;
        }
    }

    if (errflg || (fileList == NULL)) {
        fprintf (stderr, "Usage:  npath [-D...] [-I...] [-U...] [-nostdinc]\n") ;
        fprintf (stderr, "              [-cflow] [-cpp <executable>] [-c++]\n") ;
        fprintf (stderr, "              [-debug] [-echo] [-exclude <file>]\n") ;
        fprintf (stderr, "              [-full] [-long] [-longer]\n") ;
        fprintf (stderr, "              [-nocpp] [-noheading] [-npath_debug]\n") ;
        fprintf (stderr, "              [-verbose] [-verify <level>] [-vperror]\n") ;
        fprintf (stderr, "              [-yacc_debug] source_file(s)\n") ;
        exit (-1) ;
    }

/*******************************************************************************
    Concatenate the CPP options to produce a single string of CPP options.
*******************************************************************************/

    length = 0 ;
    for (i = 1 ;  i <= listLength (cppOptionsList) ;  i++)
        length = length + strlen ((char *) listGet (cppOptionsList, i)) + 1 ;

    if (length > 0)
        cppOptions = strndup (NULL, length) ;
    else
        cppOptions = strdup ("") ;

    for ( ; ; ) {
        s = (char *) listDelete (&cppOptionsList, 1) ;
        if (s == NULL)  break ;
        strCat (" ", -1, cppOptions, length+1) ;
        strCat (s, -1, cppOptions, length+1) ;
        free (s) ;
    }


/*******************************************************************************
    If a structure chart is to be generated, then create an empty call graph
    and construct the list of function names to exclude from the chart.
*******************************************************************************/

    gscCreate (NULL, NULL, NULL, NULL, &callGraph) ;
    hashCreate (512, 0, &htable_exclude) ;	/* Table of names to exclude. */

    if (excludeFile != NULL) {
			/* Open file containing list of names to exclude. */
        infile = fopen (fnmPath (excludeFile), "r") ;
        if (infile == NULL) {
            vperror ("(npath) Error opening exclude file: %s\nfopen: ",
                     fnmPath (excludeFile)) ;
            exit (errno) ;
        }
			/* Read and add each name to the table. */
        while (fgets (buffer, (sizeof buffer), infile) != NULL) {
            if (buffer[0] == '!')  continue ;		/* Skip comments. */
            s = strtok (buffer, " ,\t") ;
            while (s != NULL) {
                strTrim (s, -1) ;
                hashAdd (htable_exclude, s, NULL) ;
                s = strtok (NULL, " ,\t") ;
            }
        }
			/* Close the file. */
        fclose (infile) ;

    }

/*******************************************************************************
    For each source file, pipe the file through the C Preprocessor, read and
    parse the preprocessed source code, and compute the complexity statistics.
*******************************************************************************/

    vperror_print = vperror_save ;

    numFiles = listLength (fileList) ;
    numModules = 0 ;  totalNCSL = 0 ;
    firstFile = 1 ;  moduleList = NULL ;

    if (heading && (numFiles > 0) && !cflow) {
        length = (25 + (longNames * 8) - 1 + 7) / 8 ;
        printf ("%.*s  NCSL   Volume    V(G)   NPATH   CLOC\n", length, tabs) ;
        printf ("%.*s  ----   ------   ------  -----  ------\n", length, tabs) ;
    }


    for ( ; ; ) {

        fileName = (FileName) listDelete (&fileList, 1) ;
        if (fileName == NULL)  break ;
        sourceFile = fnmPath (fileName) ;

        if (verbose)  fprintf (stderr, "%s\n", sourceFile) ;

        s = fnmFile (fileName) ;
        length = strlen (s) + 1 ;
        length = (25 + (longNames * 8) - length - 1 + 7) / 8 ;
        if (length < 1)  length = 1 ;
        if (!cflow)  printf ("\n%s:\n", s) ;

/* Filter the source file through the C Preprocessor. */

        length = strlen (cppExecutable) + 1 + strlen (cppOptions) + 1 +
                 strlen (sourceFile) ;
        s = strndup (NULL, length) ;
        sprintf (s, "%s %s %s", cppExecutable, cppOptions, sourceFile) ;
        if (useCPP) {
            if (debug)  printf ("(npath) %s\n", s) ;
            yyin = popen (s, "r") ;
            if (yyin == NULL) {
                vperror ("(npath) Error piping %s through CPP.\npopen: ", sourceFile) ;
                exit (errno) ;
            }
        } else {
            yyin = fopen (sourceFile, "r") ;
            if (yyin == NULL) {
                vperror ("(npath) Error opening %s.\nfopen: ", sourceFile) ;
                exit (errno) ;
            }
        }
        free (s) ;

/* Parse the file. */

        htable_operands = NULL ;  htable_operators = NULL ;
        hashCreate (512, 0, &htable_types) ;	/* Create table of type names. */
        is_typedef = 0 ;  type_name_as_identifier = 0 ;
        m = NULL ;  moduleList = NULL ;
        yylineno = 0 ;

        if (!firstFile)  yyrestart (yyin) ;	/* Restart the lexical scanner. */

        yyparse () ;				/* Parse the file. */

        pclose (yyin) ;				/* Close the "file". */

        hashDestroy (htable_types) ;		/* Delete table of type names. */

        firstFile = 0 ;

/*******************************************************************************
    Print out the list of modules and their complexity statistics.
*******************************************************************************/

        for (i = 1 ;  i <= listLength (moduleList) ;  i++) {

            numModules++ ;
            m = (ModuleMetrics *) listGet (moduleList, i) ;
            totalNCSL = totalNCSL + m->num_NCSL ;
					/* Compute Halstead's VOLUME. */
            h_length = m->total_num_operators + m->total_num_operands ;
            h_vocabulary = m->num_unique_operators + m->num_unique_operands ;
            if (h_vocabulary > 0)
                h_volume = (double) h_length * log2 (h_vocabulary) ;
            else
                h_volume = 0.0 ;
					/* Since V(G) is initially 1, zero it
					   if there were no lines of code. */
            if (m->num_NCSL == 0)  m->v_of_g = 0 ;

            length = strlen (m->name) + 4 ;
            length = (25 + (longNames * 8) - length - 1 + 7) / 8 ;
            if (length < 1)  length = 1 ;
            if (!cflow)
                printf ("    %s%.*s%5d   %6.0f   %6d %6d  %6.1f\n",
                        m->name, length, tabs,
                        m->num_NCSL, h_volume,
                        m->v_of_g, m->npath,
                        ((m->num_NCSL > 0) ?
                            ((double) m->npath / (double) m->num_NCSL)
                            : 0)) ;
            if (npath_debug && !cflow)
                printf ("        Unique Operators = %d  Operands = %d  Total Operators = %d  Operands = %d\n",
                        m->num_unique_operators, m->num_unique_operands,
                        m->total_num_operators, m->total_num_operands) ;

        }    /* For each function in the source file */

					/* Delete list of modules. */
        while ((m = (ModuleMetrics *) listDelete (&moduleList, 1)) != NULL) {
            free (m->name) ;  free ((char *) m) ;
        }

        fnmDestroy (fileName) ;

    }    /* For each source file */

/*******************************************************************************
    If a structure chart was requested, then output the structure chart.
*******************************************************************************/

    if (cflow) {

        for (i = 1 ;  ;  i++) {		/* For each root (main routine) ... */

            name = gscRoot (callGraph, i) ;
            if (name == NULL)  break ;
					/* Traverse its calling hierarchy. */
            gscMark (callGraph, name, 0) ;

            line = 0 ;			/* Output its structure chart. */
            printf ("\n") ;

            gscFirst (callGraph, &name, &depth, &visit) ;

            while (name != NULL) {

                cflInfo (name, &sourceFile, &sourceLine, &cflowLine,
                         NULL, NULL) ;
                printf ("%d\t", ++line) ;
                while (depth-- > 0)	/* Indent according to depth. */
                    printf ("\t") ;
                switch (visit) {
                case GscFIRST:
                    if (sourceFile == NULL) {
                        printf ("%s: <>\n", name) ;
                    } else if (fullPathnames) {
                        printf ("%s: , <%s %d>\n",
                                name, sourceFile, sourceLine) ;
                    } else {
                        fileName = fnmCreate (sourceFile, NULL) ;
                        printf ("%s: , <%s %d>\n",
                                name, fnmFile (fileName), sourceLine) ;
                        fnmDestroy (fileName) ;
                    }
                    cflDefine (name, NULL, -1, line, -1, -1, NULL) ;
                    break ;
                case GscPREVIOUS:
                    printf ("%s: %d\n", name, cflowLine) ;
                    break ;
                case GscRECURSIVE:
                    printf ("%s: %d (recursive call)\n", name, cflowLine) ;
                    break ;
                default:
                    printf ("%s: illegal tag\n", name) ;
                    break ;
                }

                gscNext (callGraph, &name, &depth, &visit) ;

            }     /* For each module in the calling hierarchy */

        }     /* For each "main" routine */

    }     /* If a CFLOW(1) structure chart was requested */


/*******************************************************************************
    Otherwise, output summary statistics for all the files processed.
*******************************************************************************/

    else {
        printf ("\nSummary - # of files: %d, # of modules: %d, # of NCSL: %d\n",
                numFiles, numModules, totalNCSL) ;
    }


    exit (0) ;

}
