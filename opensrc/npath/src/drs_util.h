/*
@(#)  FILE: drs_util.h  RELEASE: 1.2  DATE: 07/12/96, 15:44:45
*/
/*******************************************************************************

    drs_util.h

    Directory Scanning Utility Definitions.

*******************************************************************************/

#ifndef  DRS_UTIL_H		/* Has the file been INCLUDE'd already? */
#define  DRS_UTIL_H  yes

#ifdef __cplusplus
    extern  "C" {
#endif


#include  "ansi_setup.h"		/* ANSI or non-ANSI C? */


/*******************************************************************************
    Directory Scan (Client View) and Definitions.
*******************************************************************************/

					/* Scan handle. */
typedef  struct  _DirectoryScan  *DirectoryScan ;


/*******************************************************************************
    Miscellaneous declarations.
*******************************************************************************/

extern  int  drs_util_debug ;		/* Global debug switch (1/0 = yes/no). */


/*******************************************************************************
    Public functions.
*******************************************************************************/

extern  int  drsCount P_((DirectoryScan scan)) ;

extern  int  drsCreate P_((const char *pathname,
                           DirectoryScan *scan)) ;

extern  int  drsDestroy P_((DirectoryScan scan)) ;

extern  char  *drsFirst P_((DirectoryScan scan)) ;

extern  char  *drsGet P_((DirectoryScan scan,
                          int which)) ;

extern  char  *drsNext P_((DirectoryScan scan)) ;


#ifdef __cplusplus
    }
#endif

#endif				/* If this file was not INCLUDE'd previously. */
