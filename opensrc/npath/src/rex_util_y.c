
# line 2 "rex_util_y.y"
/************************************************************************
 *  Copyright (c) 1996 by Charles A. Measday                            *
 *                                                                      *
 *  Permission to use, copy, modify, and distribute this software       *
 *  and its documentation for any purpose and without fee is hereby     *
 *  granted, provided that the above copyright notice appear in all     *
 *  copies.  The author makes no representations about the suitability  *
 *  of this software for any purpose.  It is provided "as is" without   *
 *  express or implied warranty.                                        *
 ************************************************************************/

/*
@(#)  FILE: rex_util_y.y  RELEASE: 1.8  DATE: 07/12/96, 15:35:53
*/
/*******************************************************************************

Procedure:

    rrparse ()


Author:    Alex Measday, ISI


Purpose:

    Function RRPARSE is a YACC-generated routine that parses a regular
    expression (RE) and builds a non-deterministic finite state machine
    that will recognize the RE.  The grammar for regular expressions
    is derived from the grammar presented by Robert Sedgewick in his
    book, ALGORITHMS (Chapter 21: Parsing).


    Invocation:

        status = rrparse () ;

    where

        <status>
            returns the status of parsing the input, zero if no errors occurred
            and a non-zero value otherwise.


    Public Variables:

        REX_UTIL_ERROR_TEXT - is a (CHAR *) pointer to a short error message
            explaining why RRPARSE failed, if it does.


Development History:
                                               Description
Author         Change ID       Build/Date      of Change
-------------  --------------  --------------  ---------------------
A. Measday     Port from TPOCC Release 6.1 to XSAR Release 0
    Added conditional inclusion of VMS-specific header files.  Eliminated
    external function declarations, since the required function prototypes
    are available in the "#include"d system and TPOCC header files.

A. Measday     Port from TPOCC Release 7 to XSAR Pre-Release 1
    Converted function declarations to ANSI C and added function prototypes
    for static, internal functions.

*******************************************************************************/

#define  _SVID_SOURCE  1

#include  <ctype.h>			/* Character classification macros. */
#include  <errno.h>			/* System error definitions. */
#include  <limits.h>			/* Maximum/minimum value definitions. */
#include  <stdio.h>			/* Standard I/O definitions. */
#include  <stdlib.h>			/* Standard C Library definitions. */
#include  <string.h>			/* Standard C string functions. */
#include  "str_util.h"			/* String manipulation functions. */
#include  "vperror.h"			/* VPERROR() definitions. */
#include  "rex_util.h"			/* Regular expression definitions. */
#include  "rex_internals.h"		/* Internal definitions. */

#define  NEW_STATE \
  { if (allocate_state ()) { \
        rex_error_text = "error increasing size of state list" ; \
        return (errno) ; \
    } \
  }

#define  display_production(left, right) \
    if (rex_util_debug)  printf ("%s ::== %s\n", left, right) ;

#define  display_token(name, text) \
    { if (rex_util_debug)  printf ("-- Token %s = \"%c\"\n", name, text) ; }

#define  beginning_of_input()  (rtx->scan == rtx->expression)
#define  input()  ((*rtx->scan == '\0') ? 0 : *rtx->scan++)
#define  unput(c)  (((rtx->scan == rtx->expression) || (c == '\0')) ? -1 : (rtx->scan--, 0))

/*******************************************************************************

    Internal Non-Local Variables.  These flags give the parser (RRPARSE) some
        control over how the lexer (RRLEX) interprets characters in the input
        string.  The flags are automatically reset when RRLEX is positioned at
        the beginning of the input string.

        RRPARSE_AFTER_BRACKET - controls the interpretation of characters
            within a character class specification (i.e., "[...]").  A value
            of zero indicates that the parser is not in the midst of a bracket
            expression.  Once the left bracket is encountered, RRPARSE sets
            this variable to 1; as each character in the bracket is scanned,
            RRPARSE_AFTER_BRACKET is incremented.

        RRPARSE_EXPECTING_ASSIGNMENT - controls the interpretation of a '$'
            that follows a right parenthesis: "...)$...".  RRPARSE sets this
            variable to 1 after reading the right parenthesis.  If RRLEX then
            encounters a '$' followed by a digit ('0' - '9'), it returns the
            '$' as an ASSIGNMENT token; otherwise, the '$' is returned as an
            EOL_ANCHOR token.

        RRPARSE_HYPHEN_AS_LITERAL - controls the interpretation of hyphens
            within a bracketed character class expression.

*******************************************************************************/

static  int  rrparse_after_bracket = 0 ;
static  int  rrparse_expecting_assignment = 0 ;
static  int  rrparse_hyphen_as_literal = 0 ;


/*******************************************************************************
    Private Functions
*******************************************************************************/

static  int  rrerror (
#    if __STDC__ || defined(vaxc)
        char  *s
#    endif
    ) ;

static  int  rrlex (
#    if __STDC__ || defined(vaxc)
        void
#    endif
    ) ;

static  int  allocate_state (
#    if __STDC__ || defined(vaxc)
        void
#    endif
    ) ;

static  int  first_char_of (
#    if __STDC__ || defined(vaxc)
        int  state,
        cs_set  *first_set
#    endif
    ) ;

static  int  last_of (
#    if __STDC__ || defined(vaxc)
        int  state
#    endif
    ) ;

static  int  longest_path (
#    if __STDC__ || defined(vaxc)
        int  state
#    endif
    ) ;

static  int  shortest_path (
#    if __STDC__ || defined(vaxc)
        int  state
#    endif
    ) ;
# define ANY_CHARACTER 257
# define ASSIGNMENT 258
# define BOL_ANCHOR 259
# define CARAT 260
# define COMMA 261
# define CTYPE 262
# define DIGIT 263
# define EOL_ANCHOR 264
# define _ERROR 265
# define HYPHEN 266
# define LEFT_BRACE 267
# define LEFT_BRACKET 268
# define LEFT_PAREN 269
# define ONE_OR_MORE 270
# define OR 271
# define RIGHT_BRACE 272
# define RIGHT_BRACKET 273
# define RIGHT_PAREN 274
# define SINGLE_CHARACTER 275
# define ZERO_OR_MORE 276
# define ZERO_OR_ONE 277

# line 211 "rex_util_y.y"
/*******************************************************************************
    Miscellaneous declarations.
*******************************************************************************/

    static  char  buffer[8] ;
    static  cs_set  char_class ;
    static  int  i ;

/* Character classification names.  The ordering of these names is important
   to RRPARSE; see the "character_class => CTYPE" rule before making any
   changes. */

    static  char  *class_name[] = {
        "alnum",
        "alpha",
        "cntrl",
        "digit",
        "graph",
        "lower",
        "print",
        "punct",
        "space",
        "upper",
        "xdigit",
        NULL
    } ;


#ifdef __STDC__
#include <stdlib.h>
#include <string.h>
#else
#include <malloc.h>
#include <memory.h>
#endif

#include <values.h>

#ifdef __cplusplus

#ifndef rrerror
	void rrerror(const char *);
#endif

#ifndef rrlex
#ifdef __EXTERN_C__
	extern "C" { int rrlex(void); }
#else
	int rrlex(void);
#endif
#endif
	int rrparse(void);

#endif
#define rrclearin rrchar = -1
#define rrerrok rrerrflag = 0
extern int rrchar;
extern int rrerrflag;
#ifndef RRSTYPE
#define RRSTYPE int
#endif
RRSTYPE rrlval;
RRSTYPE rrval;
typedef int rrtabelem;
#ifndef RRMAXDEPTH
#define RRMAXDEPTH 150
#endif
#if RRMAXDEPTH > 0
int rr_rrs[RRMAXDEPTH], *rrs = rr_rrs;
RRSTYPE rr_rrv[RRMAXDEPTH], *rrv = rr_rrv;
#else	/* user does initial allocation */
int *rrs;
RRSTYPE *rrv;
#endif
static int rrmaxdepth = RRMAXDEPTH;
# define RRERRCODE 256

# line 675 "rex_util_y.y"


/*******************************************************************************
    Function RRERROR is invoked automatically by RRPARSE when an error is
    detected.  RRERROR simply prints out the error message passed to it by
    RRPARSE.
*******************************************************************************/

static  int  rrerror (

#    if __STDC__ || defined(vaxc)
        char  *s)
#    else
        s)

        char  *s ;
#    endif

{
    if (rex_util_debug)  printf ("(rrparse) %s\n", s) ;
    errno = EINVAL ;  rex_error_text = s ;
    return (errno) ;
}

/*******************************************************************************
    Function RRLEX returns the next token of input.  This function is normally
    generated by LEX, but, for this parser, it's simple enough to do by hand.
*******************************************************************************/


static  int  rrlex (

#    if __STDC__ || defined(vaxc)
        void)
#    else
        )
#    endif

{    /* Local variables. */
    char  buffer[8], c ;
    unsigned  int  i ;




    if (beginning_of_input ()) {		/* Reset lexical flags. */
        rrparse_after_bracket = 0 ;
        rrparse_expecting_assignment = 0 ;
        rrparse_hyphen_as_literal = 0 ;
    }

    c = input () ;  rrlval = c ;

/* At the end of the string, return an end marker. */

    if (c == '\0') {
        display_token ("END_MARKER", '0') ;  return (0) ;
    }

/* Within a bracket expression ("[...]"), most characters are interpreted
   literally.  When the left bracket is encountered, RRPARSE sets AFTER_BRACKET
   to 1.  A "^" immediately after the left bracket is returned as the character
   class complement indicator and AFTER_BRACKET is set to -1.  A right bracket
   or hyphen immediately after the left bracket (AFTER_BRACKET == 1) or the
   complement character (AFTER_BRACKET == -1) is returned as a literal
   character.  A hyphen following a hyphen ("[a--]") or preceding the right
   bracket ("[a-]") is returned as a literal character.  A colon (":")
   immediately after the left bracket or the complement character is the
   start of a character class name specification (":<class>:"); the class
   name is extracted, looked up in the class name table, and its index is
   returned to RRPARSE.  "\<c>" constructs are handled by the non-bracket
   expression SWITCH statement further down below. */

    if (rrparse_after_bracket) {
        switch (c) {
        case '^':
            if (rrparse_after_bracket == 1) {
                rrparse_after_bracket = -1 ;
                display_token ("CARAT", c) ;  return (CARAT) ;
            }
            if (rrparse_after_bracket == -1)  rrparse_after_bracket = 1 ;
            rrparse_after_bracket++ ;
            display_token ("SINGLE_CHARACTER", c) ;  return (SINGLE_CHARACTER) ;
        case ']':
            if (rrparse_after_bracket++ > 1) {
                display_token ("RIGHT_BRACKET", c) ;  return (RIGHT_BRACKET) ;
            }
            rrparse_after_bracket = 2 ;
            display_token ("SINGLE_CHARACTER", c) ;  return (SINGLE_CHARACTER) ;
        case '-':
            c = input () ;  unput (c) ;		/* Is "-" followed by a "]"? */
            if ((rrparse_after_bracket++ > 1) &&
                (!rrparse_hyphen_as_literal) && (c != ']')) {
                display_token ("HYPHEN", c) ;  return (HYPHEN) ;
            }
            rrparse_after_bracket = 2 ;
            display_token ("SINGLE_CHARACTER", '-') ;
            return (SINGLE_CHARACTER) ;
        case ':':
            if (rrparse_after_bracket++ > 1) {
                display_token ("SINGLE_CHARACTER", c) ;  return (SINGLE_CHARACTER) ;
            }
            rrparse_after_bracket = 2 ;
				/* Extract class name from ":<class>:". */
            for (i = 0 ;  (c = input ()) && (c != ':') ;  )
                if (i < (sizeof buffer - 1))  buffer[i++] = c ;
            buffer[i] = '\0' ;  strToLower (buffer, -1) ;
				/* Lookup name in class name table. */
            for (i = 0 ;  class_name[i] != NULL ;  i++)
                if (strcmp (buffer, class_name[i]) == 0)  break ;
            rrlval = i ;  return (CTYPE) ;
        case '\\':
            if (rrparse_after_bracket == -1)  rrparse_after_bracket = 1 ;
            rrparse_after_bracket++ ;  rrparse_after_bracket++ ;
            break ;
        default:
            if (rrparse_after_bracket == -1)  rrparse_after_bracket = 1 ;
            rrparse_after_bracket++ ;
            display_token ("SINGLE_CHARACTER", c) ;  return (SINGLE_CHARACTER) ;
        }
    }


/* Outside of a bracket expression, characters receive the standard regular
   expression interpretation. */

    switch (c) {
    case '.':
        display_token ("ANY_CHARACTER", c) ;  return (ANY_CHARACTER) ;
    case '^':
        display_token ("BOL_ANCHOR", c) ;  return (BOL_ANCHOR) ;
    case ',':
        display_token ("COMMA", c) ;  return (COMMA) ;
    case '$':
        c = input () ;  unput (c) ;
        if (rrparse_expecting_assignment && isdigit (c)) {
            rrparse_expecting_assignment = 0 ;
            display_token ("ASSIGNMENT", '$') ;  return (ASSIGNMENT) ;
        } else {
            rrparse_expecting_assignment = 0 ;
            display_token ("EOL_ANCHOR", '$') ;  return (EOL_ANCHOR) ;
        }
    case '|':
        display_token ("OR", c) ;  return (OR) ;
    case '*':
        display_token ("ZERO_OR_MORE", c) ;  return (ZERO_OR_MORE) ;
    case '+':
        display_token ("ONE_OR_MORE", c) ;  return (ONE_OR_MORE) ;
    case '?':
        display_token ("ZERO_OR_ONE", c) ;  return (ZERO_OR_ONE) ;
    case '(':
        display_token ("LEFT_PAREN", c) ;  return (LEFT_PAREN) ;
    case ')':
        display_token ("RIGHT_PAREN", c) ;  return (RIGHT_PAREN) ;
    case '{':
        display_token ("LEFT_BRACE", c) ;  return (LEFT_BRACE) ;
    case '}':
        display_token ("RIGHT_BRACE", c) ;  return (RIGHT_BRACE) ;
    case '[':
        display_token ("LEFT_BRACKET", c) ;  return (LEFT_BRACKET) ;
    case '\\':
        c = input() ;  rrlval = c ;
        switch (c) {
        case 'n':  rrlval = '\n' ;  break ;
        case 'r':  rrlval = '\n' ;  break ;
        case 't':  rrlval = '\t' ;  break ;
        default:   break ;
        }
        if (c == '\0') {
            unput (c) ;  display_token ("_ERROR", '0') ;  return (_ERROR) ;
        } else {
            display_token ("SINGLE_CHARACTER", c) ;  return (SINGLE_CHARACTER) ;
        }
    default:
        if (isdigit (c)) {
            display_token ("DIGIT", c) ;  return (DIGIT) ;
        } else {
            display_token ("SINGLE_CHARACTER", c) ;  return (SINGLE_CHARACTER) ;
        }
    }

}

/*******************************************************************************
    Function ALLOCATE_STATE simply checks if there is enough room to add a new
    state to the state list.  If there is enough room, nothing is done; the
    calling routine is responsible for incrementing the NUM_STATES pointer.
    If there is not enough room, the size of the state list is increased.
    Zero is returned if the calling routine can now add a state; ERRNO is
    returned if ALLOCATE_STATE could not resize the state list.
*******************************************************************************/

#define  INCREMENT  4

static  int  allocate_state (

#    if __STDC__ || defined(vaxc)
        void)
#    else
        )
#    endif

{    /* Local variables. */
    char  *s ;
    int  size ;



    if (rtx->num_states >= rtx->max_states) {
        size = (rtx->max_states + INCREMENT) * sizeof (StateNode) ;
        if (rtx->state_list == NULL)
            s = malloc (size) ;
        else
            s = realloc ((char *) rtx->state_list, size) ;
        if (s == NULL) {
            vperror ("(allocate_state) Error reallocating the state list.\nrealloc: ") ;
            return (errno) ;
        }
        rtx->state_list = (StateNode *) s ;
        rtx->max_states = rtx->max_states + INCREMENT ;
    }
    return (0) ;

}

/*******************************************************************************
    Function FIRST_CHAR_OF computes the set of characters that may appear at
    the beginning of a string that could be matched by a regular expression.
    For example, the set of first characters for "abc" is simply "a"; the set
    for "[A-Za-z][A-Za-z0-9]*" contains "A" through "Z" and "a" through "z".
    The set of first characters for "a*" is the set of all characters, since
    the regular expression could still match a zero-length string if the
    target string begins with any character other than "a".  FIRST_CHAR_OF
    returns a function value of zero if the computed set does not contain
    all characters; a non-zero value is returned if the computed set does
    contain all characters.
*******************************************************************************/


static  int  first_char_of (

#    if __STDC__ || defined(vaxc)
        int  state,
        cs_set  *first_set)
#    else
        state, first_set)

        int  state ;
        cs_set  *first_set ;
#    endif

{    /* Local variables. */
    int  i ;




    if (rex_util_debug)  printf ("(first_char_of) State = %d\n", state) ;
    if ((state < 0) || (rtx->state_list[state].z.visited))  return (0) ;

/* Mark the state as visited during the traversal.  The VISITED flags keep
   the program from looping endlessly on cycles in the RE's graph (e.g.,
   closure states). */

    rtx->state_list[state].z.visited = 1 ;

/* Compute the set of first characters, starting with this state and proceeding
   to the end of the RE's graph. */

    switch (rtx->state_list[state].type) {
    case empty:
        return (first_char_of (rtx->state_list[state].next1, first_set)) ;
    case anchor:
        return (first_char_of (rtx->state_list[state].next1, first_set)) ;
    case alternation:
        if (first_char_of (rtx->state_list[state].next1, first_set))
            return (1) ;
        return (first_char_of (rtx->state_list[state].next2, first_set)) ;
    case closure:
        first_char_of (rtx->state_list[state].next2, first_set) ;
        return (first_char_of (rtx->state_list[state].next1, first_set)) ;
    case final:
        for (i = 0 ;  i < CS_SETSIZE ;  i++)
            CS_SET (i, first_set) ;
        return (1) ;
    case match:
        if (rtx->state_list[state].x.match_char < 0) {
            for (i = 0 ;  i < CS_SETSIZE ;  i++)
                CS_SET (i, first_set) ;
            return (1) ;
        } else if (rtx->state_list[state].x.match_char == 0) {
            for (i = 0 ;  i < CS_SETSIZE ;  i++) {
                if (CS_ISSET (i, rtx->state_list[state].y.match_charset))
                    CS_SET (i, first_set) ;
            }
        } else {
            CS_SET (rtx->state_list[state].x.match_char, first_set) ;
        }
        return (0) ;
    case left_paren:
        return (first_char_of (rtx->state_list[state].next1, first_set)) ;
    case right_paren:
        return (first_char_of (rtx->state_list[state].next1, first_set)) ;
    default:
        return (0) ;
    }

}

/*******************************************************************************
    Function LAST_OF locates the last state in a regular expression graph.
    LAST_OF, passed the index of the first state in the graph, simply traverses
    the NEXT1 links until a null link (index = -1) is found.  Note that this
    technique assumes that the NEXT1 links lead to the last state in the
    graph; RRPARSE adheres to this convention and uses NEXT2 links only for
    backtracking in closure (RE* and RE+) expressions.
*******************************************************************************/

static  int  last_of (

#    if __STDC__ || defined(vaxc)
        int  state)		/* The first state in the RE graph. */
#    else
        state)

        int  state ;		/* The first state in the RE graph. */
#    endif

{
    while (rtx->state_list[state].next1 >= 0)
        state = rtx->state_list[state].next1 ;
    return (state) ;
}

/*******************************************************************************
    Function LONGEST_PATH computes the longest path through an RE.  The
    longest path is an estimate of the maximum number of states that would
    be visited during an attempt to match a target string.
*******************************************************************************/


static  int  longest_path (

#    if __STDC__ || defined(vaxc)
        int  state)
#    else
        state)

        int  state ;
#    endif

{    /* Local variables. */
    int  i, j ;



    if (rex_util_debug)  printf ("(longest_path) State = %d\n", state) ;
    if ((state < 0) || (rtx->state_list[state].z.visited))  return (0) ;

/* Mark the state as visited during the traversal.  The VISITED flags keep
   the program from looping endlessly on cycles in the RE's graph (e.g.,
   closure states). */

    rtx->state_list[state].z.visited = 1 ;

/* Compute the longest path from this state to the end of the RE's graph. */

    switch (rtx->state_list[state].type) {
    case empty:
        return (longest_path (rtx->state_list[state].next1)) ;
    case anchor:
        return (longest_path (rtx->state_list[state].next1)) ;
    case alternation:
        i = 1 + longest_path (rtx->state_list[state].next1) ;
        j = 1 + longest_path (rtx->state_list[state].next2) ;
        return ((i > j) ? i : j) ;
    case closure:
        return (longest_path (rtx->state_list[state].next2) + 2 +
                longest_path (rtx->state_list[state].next1)) ;
    case final:
        return (0) ;
    case match:
        return (1 + longest_path (rtx->state_list[state].next1)) ;
    case left_paren:
        return (1 + longest_path (rtx->state_list[state].next1)) ;
    case right_paren:
        return (1 + longest_path (rtx->state_list[state].next1)) ;
    default:
        return (0) ;
    }

}

/*******************************************************************************
    Function SHORTEST_PATH computes the shortest path through an RE.  The
    shortest path equals the minimum length of a target string that would
    be matched by the RE.
*******************************************************************************/


static  int  shortest_path (

#    if __STDC__ || defined(vaxc)
        int  state)
#    else
        state)

        int  state ;
#    endif

{    /* Local variables. */
    int  i, j ;




    if (rex_util_debug)  printf ("(shortest_path) State = %d\n", state) ;
    if ((state < 0) || (rtx->state_list[state].z.visited))  return (0) ;

/* Mark the state as visited during the traversal.  The VISITED flags keep
   the program from looping endlessly on cycles in the RE's graph (e.g.,
   closure states). */

    rtx->state_list[state].z.visited = 1 ;

/* Compute the shortest path from this state to the end of the RE's graph. */

    switch (rtx->state_list[state].type) {
    case empty:
        return (shortest_path (rtx->state_list[state].next1)) ;
    case anchor:
        return (shortest_path (rtx->state_list[state].next1)) ;
    case alternation:
        i = shortest_path (rtx->state_list[state].next1) ;
        j = shortest_path (rtx->state_list[state].next2) ;
        return ((i < j) ? i : j) ;
    case closure:
        return ((shortest_path (rtx->state_list[state].next2) *
                 rtx->state_list[state].x.min_closure)  +
                shortest_path (rtx->state_list[state].next1)) ;
    case final:
        return (0) ;
    case match:
        return (1 + shortest_path (rtx->state_list[state].next1)) ;
    case left_paren:
        return (shortest_path (rtx->state_list[state].next1)) ;
    case right_paren:
        return (shortest_path (rtx->state_list[state].next1)) ;
    default:
        return (0) ;
    }

}
rrtabelem rrexca[] ={
-1, 1,
	0, -1,
	-2, 0,
	};
# define RRNPROD 38
# define RRLAST 83
rrtabelem rract[]={

    12,    36,     5,    35,    10,    13,    11,     6,    25,    35,
    15,     8,     7,    17,    41,    13,    36,    37,     9,    16,
    18,    12,    36,     5,    43,    10,    44,    11,     6,    24,
    30,    29,     8,     7,    27,    40,    34,    33,    22,     9,
     2,     3,    45,    23,    32,    26,    14,    20,    19,    39,
    31,    28,     4,     1,    21,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,    38,     0,
    42,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,    46 };
rrtabelem rrpact[]={

  -236,-10000000,  -256,-10000000,  -257,-10000000,-10000000,  -236,-10000000,-10000000,
-10000000,-10000000,-10000000,  -236,-10000000,  -234,-10000000,-10000000,-10000000,  -266,
  -226,-10000000,  -230,  -233,-10000000,-10000000,  -253,-10000000,  -255,  -234,
-10000000,  -223,  -259,-10000000,  -242,-10000000,-10000000,-10000000,-10000000,-10000000,
  -237,-10000000,-10000000,-10000000,-10000000,  -274,-10000000 };
rrtabelem rrpgo[]={

     0,    53,    40,    41,    52,    38,    51,    50,    49,    47,
    45,    44,    43,    37,    36,    42 };
rrtabelem rrr1[]={

     0,     1,     2,     2,     2,     3,     3,     4,     4,     4,
     7,     4,     4,     4,     4,     9,     4,     4,     4,     4,
     4,     8,     8,    10,    10,     5,     5,     6,     6,    12,
    12,    11,    11,    13,    15,    13,    13,    14 };
rrtabelem rrr2[]={

     0,     3,     1,     3,     7,     3,     5,     3,     3,    11,
     1,    11,     5,     5,     5,     1,    11,     3,     3,     3,
     3,     1,     5,     1,     3,     1,     3,     1,     5,     3,
     5,     3,     5,     3,     1,     9,     3,     3 };
rrtabelem rrchk[]={

-10000000,    -1,    -2,    -3,    -4,   259,   264,   269,   268,   275,
   261,   263,   257,   271,    -3,   267,   276,   270,   277,    -2,
    -9,    -2,    -5,   -12,   263,   274,   -10,   260,    -6,   261,
   263,    -7,   -11,   -13,   -14,   262,   275,   272,    -5,    -8,
   258,   273,   -13,   266,   263,   -15,   -14 };
rrtabelem rrdef[]={

     2,    -2,     1,     3,     5,     7,     8,     2,    15,    17,
    18,    19,    20,     2,     6,    25,    12,    13,    14,     0,
    23,     4,    27,    26,    29,    10,     0,    24,     0,    25,
    30,    21,     0,    31,    33,    36,    37,     9,    28,    11,
     0,    16,    32,    34,    22,     0,    35 };
typedef struct
#ifdef __cplusplus
	rrtoktype
#endif
{ char *t_name; int t_val; } rrtoktype;
#ifndef RRDEBUG
#	define RRDEBUG	0	/* don't allow debugging */
#endif

#if RRDEBUG

rrtoktype rrtoks[] =
{
	"ANY_CHARACTER",	257,
	"ASSIGNMENT",	258,
	"BOL_ANCHOR",	259,
	"CARAT",	260,
	"COMMA",	261,
	"CTYPE",	262,
	"DIGIT",	263,
	"EOL_ANCHOR",	264,
	"_ERROR",	265,
	"HYPHEN",	266,
	"LEFT_BRACE",	267,
	"LEFT_BRACKET",	268,
	"LEFT_PAREN",	269,
	"ONE_OR_MORE",	270,
	"OR",	271,
	"RIGHT_BRACE",	272,
	"RIGHT_BRACKET",	273,
	"RIGHT_PAREN",	274,
	"SINGLE_CHARACTER",	275,
	"ZERO_OR_MORE",	276,
	"ZERO_OR_ONE",	277,
	"-unknown-",	-1	/* ends search */
};

char * rrreds[] =
{
	"-no such reduction-",
	"complete_re : regular_expression",
	"regular_expression : /* empty */",
	"regular_expression : term",
	"regular_expression : regular_expression OR regular_expression",
	"term : factor",
	"term : factor term",
	"factor : BOL_ANCHOR",
	"factor : EOL_ANCHOR",
	"factor : factor LEFT_BRACE number upper_bound RIGHT_BRACE",
	"factor : LEFT_PAREN regular_expression RIGHT_PAREN",
	"factor : LEFT_PAREN regular_expression RIGHT_PAREN assignment",
	"factor : factor ZERO_OR_MORE",
	"factor : factor ONE_OR_MORE",
	"factor : factor ZERO_OR_ONE",
	"factor : LEFT_BRACKET",
	"factor : LEFT_BRACKET complement character_classes RIGHT_BRACKET",
	"factor : SINGLE_CHARACTER",
	"factor : COMMA",
	"factor : DIGIT",
	"factor : ANY_CHARACTER",
	"assignment : /* empty */",
	"assignment : ASSIGNMENT DIGIT",
	"complement : /* empty */",
	"complement : CARAT",
	"number : /* empty */",
	"number : digits",
	"upper_bound : /* empty */",
	"upper_bound : COMMA number",
	"digits : DIGIT",
	"digits : digits DIGIT",
	"character_classes : character_class",
	"character_classes : character_classes character_class",
	"character_class : character",
	"character_class : character HYPHEN",
	"character_class : character HYPHEN character",
	"character_class : CTYPE",
	"character : SINGLE_CHARACTER",
};
#endif /* RRDEBUG */
# line	1 "/usr/ccs/bin/yaccpar"
/*
 * Copyright (c) 1993 by Sun Microsystems, Inc.
 */

#pragma ident	"@(#)yaccpar	6.12	93/06/07 SMI"

/*
** Skeleton parser driver for yacc output
*/

/*
** yacc user known macros and defines
*/
#define RRERROR		goto rrerrlab
#define RRACCEPT	return(0)
#define RRABORT		return(1)
#define RRBACKUP( newtoken, newvalue )\
{\
	if ( rrchar >= 0 || ( rrr2[ rrtmp ] >> 1 ) != 1 )\
	{\
		rrerror( "syntax error - cannot backup" );\
		goto rrerrlab;\
	}\
	rrchar = newtoken;\
	rrstate = *rrps;\
	rrlval = newvalue;\
	goto rrnewstate;\
}
#define RRRECOVERING()	(!!rrerrflag)
#define RRNEW(type)	malloc(sizeof(type) * rrnewmax)
#define RRCOPY(to, from, type) \
	(type *) memcpy(to, (char *) from, rrnewmax * sizeof(type))
#define RRENLARGE( from, type) \
	(type *) realloc((char *) from, rrnewmax * sizeof(type))
#ifndef RRDEBUG
#	define RRDEBUG	1	/* make debugging available */
#endif

/*
** user known globals
*/
int rrdebug;			/* set to 1 to get debugging */

/*
** driver internal defines
*/
#define RRFLAG		(-10000000)

/*
** global variables used by the parser
*/
RRSTYPE *rrpv;			/* top of value stack */
int *rrps;			/* top of state stack */

int rrstate;			/* current state */
int rrtmp;			/* extra var (lasts between blocks) */

int rrnerrs;			/* number of errors */
int rrerrflag;			/* error recovery flag */
int rrchar;			/* current input token number */



#ifdef RRNMBCHARS
#define RRLEX()		rrcvtok(rrlex())
/*
** rrcvtok - return a token if i is a wchar_t value that exceeds 255.
**	If i<255, i itself is the token.  If i>255 but the neither 
**	of the 30th or 31st bit is on, i is already a token.
*/
#if defined(__STDC__) || defined(__cplusplus)
int rrcvtok(int i)
#else
int rrcvtok(i) int i;
#endif
{
	int first = 0;
	int last = RRNMBCHARS - 1;
	int mid;
	wchar_t j;

	if(i&0x60000000){/*Must convert to a token. */
		if( rrmbchars[last].character < i ){
			return i;/*Giving up*/
		}
		while ((last>=first)&&(first>=0)) {/*Binary search loop*/
			mid = (first+last)/2;
			j = rrmbchars[mid].character;
			if( j==i ){/*Found*/ 
				return rrmbchars[mid].tvalue;
			}else if( j<i ){
				first = mid + 1;
			}else{
				last = mid -1;
			}
		}
		/*No entry in the table.*/
		return i;/* Giving up.*/
	}else{/* i is already a token. */
		return i;
	}
}
#else/*!RRNMBCHARS*/
#define RRLEX()		rrlex()
#endif/*!RRNMBCHARS*/

/*
** rrparse - return 0 if worked, 1 if syntax error not recovered from
*/
#if defined(__STDC__) || defined(__cplusplus)
int rrparse(void)
#else
int rrparse()
#endif
{
	register RRSTYPE *rrpvt;	/* top of value stack for $vars */

#if defined(__cplusplus) || defined(lint)
/*
	hacks to please C++ and lint - goto's inside switch should never be
	executed; rrpvt is set to 0 to avoid "used before set" warning.
*/
	static int __yaccpar_lint_hack__ = 0;
	switch (__yaccpar_lint_hack__)
	{
		case 1: goto rrerrlab;
		case 2: goto rrnewstate;
	}
	rrpvt = 0;
#endif

	/*
	** Initialize externals - rrparse may be called more than once
	*/
	rrpv = &rrv[-1];
	rrps = &rrs[-1];
	rrstate = 0;
	rrtmp = 0;
	rrnerrs = 0;
	rrerrflag = 0;
	rrchar = -1;

#if RRMAXDEPTH <= 0
	if (rrmaxdepth <= 0)
	{
		if ((rrmaxdepth = RREXPAND(0)) <= 0)
		{
			rrerror("yacc initialization error");
			RRABORT;
		}
	}
#endif

	{
		register RRSTYPE *rr_pv;	/* top of value stack */
		register int *rr_ps;		/* top of state stack */
		register int rr_state;		/* current state */
		register int  rr_n;		/* internal state number info */
	goto rrstack;	/* moved from 6 lines above to here to please C++ */

		/*
		** get globals into registers.
		** branch to here only if RRBACKUP was called.
		*/
	rrnewstate:
		rr_pv = rrpv;
		rr_ps = rrps;
		rr_state = rrstate;
		goto rr_newstate;

		/*
		** get globals into registers.
		** either we just started, or we just finished a reduction
		*/
	rrstack:
		rr_pv = rrpv;
		rr_ps = rrps;
		rr_state = rrstate;

		/*
		** top of for (;;) loop while no reductions done
		*/
	rr_stack:
		/*
		** put a state and value onto the stacks
		*/
#if RRDEBUG
		/*
		** if debugging, look up token value in list of value vs.
		** name pairs.  0 and negative (-1) are special values.
		** Note: linear search is used since time is not a real
		** consideration while debugging.
		*/
		if ( rrdebug )
		{
			register int rr_i;

			printf( "State %d, token ", rr_state );
			if ( rrchar == 0 )
				printf( "end-of-file\n" );
			else if ( rrchar < 0 )
				printf( "-none-\n" );
			else
			{
				for ( rr_i = 0; rrtoks[rr_i].t_val >= 0;
					rr_i++ )
				{
					if ( rrtoks[rr_i].t_val == rrchar )
						break;
				}
				printf( "%s\n", rrtoks[rr_i].t_name );
			}
		}
#endif /* RRDEBUG */
		if ( ++rr_ps >= &rrs[ rrmaxdepth ] )	/* room on stack? */
		{
			/*
			** reallocate and recover.  Note that pointers
			** have to be reset, or bad things will happen
			*/
			int rrps_index = (rr_ps - rrs);
			int rrpv_index = (rr_pv - rrv);
			int rrpvt_index = (rrpvt - rrv);
			int rrnewmax;
#ifdef RREXPAND
			rrnewmax = RREXPAND(rrmaxdepth);
#else
			rrnewmax = 2 * rrmaxdepth;	/* double table size */
			if (rrmaxdepth == RRMAXDEPTH)	/* first time growth */
			{
				char *newrrs = (char *)RRNEW(int);
				char *newrrv = (char *)RRNEW(RRSTYPE);
				if (newrrs != 0 && newrrv != 0)
				{
					rrs = RRCOPY(newrrs, rrs, int);
					rrv = RRCOPY(newrrv, rrv, RRSTYPE);
				}
				else
					rrnewmax = 0;	/* failed */
			}
			else				/* not first time */
			{
				rrs = RRENLARGE(rrs, int);
				rrv = RRENLARGE(rrv, RRSTYPE);
				if (rrs == 0 || rrv == 0)
					rrnewmax = 0;	/* failed */
			}
#endif
			if (rrnewmax <= rrmaxdepth)	/* tables not expanded */
			{
				rrerror( "yacc stack overflow" );
				RRABORT;
			}
			rrmaxdepth = rrnewmax;

			rr_ps = rrs + rrps_index;
			rr_pv = rrv + rrpv_index;
			rrpvt = rrv + rrpvt_index;
		}
		*rr_ps = rr_state;
		*++rr_pv = rrval;

		/*
		** we have a new state - find out what to do
		*/
	rr_newstate:
		if ( ( rr_n = rrpact[ rr_state ] ) <= RRFLAG )
			goto rrdefault;		/* simple state */
#if RRDEBUG
		/*
		** if debugging, need to mark whether new token grabbed
		*/
		rrtmp = rrchar < 0;
#endif
		if ( ( rrchar < 0 ) && ( ( rrchar = RRLEX() ) < 0 ) )
			rrchar = 0;		/* reached EOF */
#if RRDEBUG
		if ( rrdebug && rrtmp )
		{
			register int rr_i;

			printf( "Received token " );
			if ( rrchar == 0 )
				printf( "end-of-file\n" );
			else if ( rrchar < 0 )
				printf( "-none-\n" );
			else
			{
				for ( rr_i = 0; rrtoks[rr_i].t_val >= 0;
					rr_i++ )
				{
					if ( rrtoks[rr_i].t_val == rrchar )
						break;
				}
				printf( "%s\n", rrtoks[rr_i].t_name );
			}
		}
#endif /* RRDEBUG */
		if ( ( ( rr_n += rrchar ) < 0 ) || ( rr_n >= RRLAST ) )
			goto rrdefault;
		if ( rrchk[ rr_n = rract[ rr_n ] ] == rrchar )	/*valid shift*/
		{
			rrchar = -1;
			rrval = rrlval;
			rr_state = rr_n;
			if ( rrerrflag > 0 )
				rrerrflag--;
			goto rr_stack;
		}

	rrdefault:
		if ( ( rr_n = rrdef[ rr_state ] ) == -2 )
		{
#if RRDEBUG
			rrtmp = rrchar < 0;
#endif
			if ( ( rrchar < 0 ) && ( ( rrchar = RRLEX() ) < 0 ) )
				rrchar = 0;		/* reached EOF */
#if RRDEBUG
			if ( rrdebug && rrtmp )
			{
				register int rr_i;

				printf( "Received token " );
				if ( rrchar == 0 )
					printf( "end-of-file\n" );
				else if ( rrchar < 0 )
					printf( "-none-\n" );
				else
				{
					for ( rr_i = 0;
						rrtoks[rr_i].t_val >= 0;
						rr_i++ )
					{
						if ( rrtoks[rr_i].t_val
							== rrchar )
						{
							break;
						}
					}
					printf( "%s\n", rrtoks[rr_i].t_name );
				}
			}
#endif /* RRDEBUG */
			/*
			** look through exception table
			*/
			{
				register int *rrxi = rrexca;

				while ( ( *rrxi != -1 ) ||
					( rrxi[1] != rr_state ) )
				{
					rrxi += 2;
				}
				while ( ( *(rrxi += 2) >= 0 ) &&
					( *rrxi != rrchar ) )
					;
				if ( ( rr_n = rrxi[1] ) < 0 )
					RRACCEPT;
			}
		}

		/*
		** check for syntax error
		*/
		if ( rr_n == 0 )	/* have an error */
		{
			/* no worry about speed here! */
			switch ( rrerrflag )
			{
			case 0:		/* new error */
				rrerror( "syntax error" );
				goto skip_init;
			rrerrlab:
				/*
				** get globals into registers.
				** we have a user generated syntax type error
				*/
				rr_pv = rrpv;
				rr_ps = rrps;
				rr_state = rrstate;
			skip_init:
				rrnerrs++;
				/* FALLTHRU */
			case 1:
			case 2:		/* incompletely recovered error */
					/* try again... */
				rrerrflag = 3;
				/*
				** find state where "error" is a legal
				** shift action
				*/
				while ( rr_ps >= rrs )
				{
					rr_n = rrpact[ *rr_ps ] + RRERRCODE;
					if ( rr_n >= 0 && rr_n < RRLAST &&
						rrchk[rract[rr_n]] == RRERRCODE)					{
						/*
						** simulate shift of "error"
						*/
						rr_state = rract[ rr_n ];
						goto rr_stack;
					}
					/*
					** current state has no shift on
					** "error", pop stack
					*/
#if RRDEBUG
#	define _POP_ "Error recovery pops state %d, uncovers state %d\n"
					if ( rrdebug )
						printf( _POP_, *rr_ps,
							rr_ps[-1] );
#	undef _POP_
#endif
					rr_ps--;
					rr_pv--;
				}
				/*
				** there is no state on stack with "error" as
				** a valid shift.  give up.
				*/
				RRABORT;
			case 3:		/* no shift yet; eat a token */
#if RRDEBUG
				/*
				** if debugging, look up token in list of
				** pairs.  0 and negative shouldn't occur,
				** but since timing doesn't matter when
				** debugging, it doesn't hurt to leave the
				** tests here.
				*/
				if ( rrdebug )
				{
					register int rr_i;

					printf( "Error recovery discards " );
					if ( rrchar == 0 )
						printf( "token end-of-file\n" );
					else if ( rrchar < 0 )
						printf( "token -none-\n" );
					else
					{
						for ( rr_i = 0;
							rrtoks[rr_i].t_val >= 0;
							rr_i++ )
						{
							if ( rrtoks[rr_i].t_val
								== rrchar )
							{
								break;
							}
						}
						printf( "token %s\n",
							rrtoks[rr_i].t_name );
					}
				}
#endif /* RRDEBUG */
				if ( rrchar == 0 )	/* reached EOF. quit */
					RRABORT;
				rrchar = -1;
				goto rr_newstate;
			}
		}/* end if ( rr_n == 0 ) */
		/*
		** reduction by production rr_n
		** put stack tops, etc. so things right after switch
		*/
#if RRDEBUG
		/*
		** if debugging, print the string that is the user's
		** specification of the reduction which is just about
		** to be done.
		*/
		if ( rrdebug )
			printf( "Reduce by (%d) \"%s\"\n",
				rr_n, rrreds[ rr_n ] );
#endif
		rrtmp = rr_n;			/* value to switch over */
		rrpvt = rr_pv;			/* $vars top of value stack */
		/*
		** Look in goto table for next state
		** Sorry about using rr_state here as temporary
		** register variable, but why not, if it works...
		** If rrr2[ rr_n ] doesn't have the low order bit
		** set, then there is no action to be done for
		** this reduction.  So, no saving & unsaving of
		** registers done.  The only difference between the
		** code just after the if and the body of the if is
		** the goto rr_stack in the body.  This way the test
		** can be made before the choice of what to do is needed.
		*/
		{
			/* length of production doubled with extra bit */
			register int rr_len = rrr2[ rr_n ];

			if ( !( rr_len & 01 ) )
			{
				rr_len >>= 1;
				rrval = ( rr_pv -= rr_len )[1];	/* $$ = $1 */
				rr_state = rrpgo[ rr_n = rrr1[ rr_n ] ] +
					*( rr_ps -= rr_len ) + 1;
				if ( rr_state >= RRLAST ||
					rrchk[ rr_state =
					rract[ rr_state ] ] != -rr_n )
				{
					rr_state = rract[ rrpgo[ rr_n ] ];
				}
				goto rr_stack;
			}
			rr_len >>= 1;
			rrval = ( rr_pv -= rr_len )[1];	/* $$ = $1 */
			rr_state = rrpgo[ rr_n = rrr1[ rr_n ] ] +
				*( rr_ps -= rr_len ) + 1;
			if ( rr_state >= RRLAST ||
				rrchk[ rr_state = rract[ rr_state ] ] != -rr_n )
			{
				rr_state = rract[ rrpgo[ rr_n ] ];
			}
		}
					/* save until reenter driver code */
		rrstate = rr_state;
		rrps = rr_ps;
		rrpv = rr_pv;
	}
	/*
	** code supplied by user is placed in this switch
	*/
	switch( rrtmp )
	{
		
case 1:
# line 249 "rex_util_y.y"
{ display_production ("complete_re", "regular_expression") ;
            /* The initial state equals the first state in RE.  Add a final
               state and link the last state in RE to the final state. */
            rtx->start_state = rrpvt[-0] ;
            NEW_STATE ;
            rtx->state_list[last_of (rrpvt[-0])].next1 = rtx->num_states ;
            rtx->state_list[rtx->num_states].type = final ;
            rtx->state_list[rtx->num_states].x.match_char = ' ' ;
            rtx->num_states++ ;
            /* Estimate the length of the longest path through the complete
               RE.  The length is used to size the stack in the iterative
               version of REX_SEARCH. */
            for (i = 0 ;  i < rtx->num_states ;  i++)
                rtx->state_list[i].z.visited = 0 ;
            rtx->longest_path = longest_path (rrpvt[-0]) ;
            /* Compute the set of first characters that may appear at the
               beginning of a target string matched by this RE.  This set
               allows REX_MATCH to avoid calling REX_SEARCH when the first
               character of the target string is not in the set of first
               characters. */
            CS_ZERO (&rtx->first_set) ;
            for (i = 0 ;  i < rtx->num_states ;  i++)
                rtx->state_list[i].z.visited = 0 ;
            first_char_of (rtx->start_state, &rtx->first_set) ;
          } break;
case 2:
# line 279 "rex_util_y.y"
{ display_production ("regular_expression", "<empty>") ;
            /* Add an EMPTY state. */
            NEW_STATE ;
            rtx->state_list[rtx->num_states].type = empty ;
            rtx->state_list[rtx->num_states].x.match_char = ' ' ;
            rtx->state_list[rtx->num_states].next1 = -1 ;
            rtx->state_list[rtx->num_states].next2 = -1 ;
            rrval = rtx->num_states++ ;
          } break;
case 3:
# line 289 "rex_util_y.y"
{ display_production ("regular_expression", "term") ;  rrval = rrpvt[-0] ; } break;
case 4:
# line 291 "rex_util_y.y"
{ display_production ("regular_expression", "regular_expression | regular_expression") ;
            /* Add an alternation state E1 and an empty state E2.  Link E1 to
               the first state in RE1 and the first state in RE2.  Link the
               last state in RE1 to E2; do the same for the last state in RE2.
               (The first state in "RE|RE" is E1.) */
            NEW_STATE ;
            rtx->state_list[rtx->num_states].type = alternation ;
            rtx->state_list[rtx->num_states].x.match_char = ' ' ;
            rtx->state_list[rtx->num_states].next1 = rrpvt[-2] ;
            rtx->state_list[rtx->num_states].next2 = rrpvt[-0] ;
            rrval = rtx->num_states++ ;
            NEW_STATE ;
            rtx->state_list[last_of (rrpvt[-2])].next1 = rtx->num_states ;
            rtx->state_list[last_of (rrpvt[-0])].next1 = rtx->num_states ;
            rtx->state_list[rtx->num_states].type = empty ;
            rtx->state_list[rtx->num_states].x.match_char = ' ' ;
            rtx->state_list[rtx->num_states].next1 = -1 ;
            rtx->state_list[rtx->num_states].next2 = -1 ;
            rtx->num_states++ ;
          } break;
case 5:
# line 316 "rex_util_y.y"
{ display_production ("term", "factor") ;  rrval = rrpvt[-0] ; } break;
case 6:
# line 318 "rex_util_y.y"
{ display_production ("term", "factor term") ;
            /* Link the last state in RE1 to the first state in RE2.  (The
               first state in "RE1 RE2" is the first state in RE1.) */
            rtx->state_list[last_of (rrpvt[-1])].next1 = rrpvt[-0] ;
            rrval = rrpvt[-1] ;
          } break;
case 7:
# line 329 "rex_util_y.y"
{ display_production ("factor", "^") ;
            /* Add an anchor state and set its match character to "^". */
            NEW_STATE ;
            rtx->state_list[rtx->num_states].type = anchor ;
            rtx->state_list[rtx->num_states].x.match_char = '^' ;
            rtx->state_list[rtx->num_states].next1 = -1 ;
            rtx->state_list[rtx->num_states].next2 = -1 ;
            rrval = rtx->num_states++ ;
          } break;
case 8:
# line 339 "rex_util_y.y"
{ display_production ("factor", "$") ;
            /* Add an anchor state and set its match character to "$". */
            NEW_STATE ;
            rtx->state_list[rtx->num_states].type = anchor ;
            rtx->state_list[rtx->num_states].x.match_char = '$' ;
            rtx->state_list[rtx->num_states].next1 = -1 ;
            rtx->state_list[rtx->num_states].next2 = -1 ;
            rrval = rtx->num_states++ ;
          } break;
case 9:
# line 349 "rex_util_y.y"
{ display_production ("factor", "factor{[min][,[max]]}") ;
            /* Check for nested closures that might cause the matching
               algorithm to endlessly loop without consuming any input
               from the target string; e.g., "(a*){0,10}".  Actually,
               the maximum field puts a cap on the number of loops, but
               we'll disallow it anyway. */
            for (i = 0 ;  i < rtx->num_states ;  i++)
                rtx->state_list[i].z.visited = 0 ;
            i = shortest_path (rrpvt[-4]) ;
            if (rex_util_debug)  printf ("(rrparse) Shortest path from state %d = %d\n", rrpvt[-4], i) ;
            if (i <= 0) {
                errno = EINVAL ;  rex_error_text = "nested empty closure" ;
                return (errno) ;
            }
            /* Add a closure state and set its minimum and maximum fields.
               Link the last state in RE to the closure state; link the closure
               state backwards to the first state in RE.  (The first state in
               "RE{min,max}" is the closure state.) */
            NEW_STATE ;
            rtx->state_list[last_of (rrpvt[-4])].next1 = rtx->num_states ;
            rtx->state_list[rtx->num_states].type = closure ;
            if (rrpvt[-2] < 0)  rrpvt[-2] = 0 ;
            rtx->state_list[rtx->num_states].x.min_closure = rrpvt[-2] ;
            if (rrpvt[-1] < 0)  rrpvt[-1] = rrpvt[-2] ;
            rtx->state_list[rtx->num_states].y.max_closure = rrpvt[-1] ;
            rtx->state_list[rtx->num_states].next1 = -1 ;
            rtx->state_list[rtx->num_states].next2 = rrpvt[-4] ;
            rrval = rtx->num_states++ ;
          } break;
case 10:
# line 379 "rex_util_y.y"
{ rrparse_expecting_assignment = -1 ; } break;
case 11:
# line 381 "rex_util_y.y"
{ display_production ("factor", "(regular_expression) <assignment>") ;
            /* If an assignment clause ("$<digit>") was specified, add two
               parenthesis states, P1 and P2.  Link P1 to the first state
               in RE; link the last state in RE to P2.  Store the argument
               index (<digit>) in P1 and store the index of P1 in P2.  If an
               assignment clause was NOT specified, nothing needs to be done.
               (The first state in "(RE)" is the first state in RE; the first
                state in "(RE)$n" is P1.) */
            if (rrpvt[-0] < 0) {				/* "(RE)" */
                rrval = rrpvt[-3] ;
            } else {					/* "(RE)$n" */
                NEW_STATE ;
                rtx->state_list[rtx->num_states].type = left_paren ;	/* P1 */
                rtx->state_list[rtx->num_states].x.subexp_index = rrpvt[-0] ;
                rtx->state_list[rtx->num_states].y.subexp_state =
                    rtx->num_states + 1 ;
                rtx->state_list[rtx->num_states].next1 = rrpvt[-3] ;
                rtx->state_list[rtx->num_states].next2 = -1 ;
                if (rtx->num_args < (rrpvt[-0]+1))  rtx->num_args = rrpvt[-0] + 1 ;
                rrval = rtx->num_states++ ;
                NEW_STATE ;
                rtx->state_list[last_of (rrpvt[-3])].next1 = rtx->num_states ;
                rtx->state_list[rtx->num_states].type = right_paren ;	/* P2 */
                rtx->state_list[rtx->num_states].x.subexp_index = rrpvt[-0] ;
                rtx->state_list[rtx->num_states].y.subexp_state = rrval ;
                rtx->state_list[rtx->num_states].next1 = -1 ;
                rtx->state_list[rtx->num_states].next2 = -1 ;
                rtx->num_states++ ;
            }
          } break;
case 12:
# line 412 "rex_util_y.y"
{ display_production ("factor", "factor*") ;
            /* Check for nested closures that might cause the matching
               algorithm to endlessly loop without consuming any input
               from the target string; e.g., "(a*)*". */
            for (i = 0 ;  i < rtx->num_states ;  i++)
                rtx->state_list[i].z.visited = 0 ;
            i = shortest_path (rrpvt[-1]) ;
            if (rex_util_debug)  printf ("(rrparse) Shortest path from state %d = %d\n", rrpvt[-1], i) ;
            if (i <= 0) {
                errno = EINVAL ;  rex_error_text = "nested empty closure" ;
                return (errno) ;
            }
            /* Add a closure state.  Link the last state in RE to the closure
               state; link the closure state backwards to the first state in
               RE.  (The first state in "RE*" is the closure state.) */
            NEW_STATE ;
            rtx->state_list[last_of (rrpvt[-1])].next1 = rtx->num_states ;
            rtx->state_list[rtx->num_states].type = closure ;
            rtx->state_list[rtx->num_states].x.min_closure = 0 ;
            rtx->state_list[rtx->num_states].y.max_closure = INT_MAX ;
            rtx->state_list[rtx->num_states].next1 = -1 ;
            rtx->state_list[rtx->num_states].next2 = rrpvt[-1] ;
            rrval = rtx->num_states++ ;
          } break;
case 13:
# line 437 "rex_util_y.y"
{ display_production ("factor", "factor+") ;
            /* Check for nested closures that might cause the matching
               algorithm to endlessly loop without consuming any input
               from the target string; e.g., "(a*)+". */
            for (i = 0 ;  i < rtx->num_states ;  i++)
                rtx->state_list[i].z.visited = 0 ;
            i = shortest_path (rrpvt[-1]) ;
            if (rex_util_debug)  printf ("(rrparse) Shortest path from state %d = %d\n", rrpvt[-1], i) ;
            if (i <= 0) {
                errno = EINVAL ;  rex_error_text = "nested empty closure" ;
                return (errno) ;
            }
            /* Add a closure state.  Link the last state in RE to the closure
               state; link the closure state backwards to the first state in
               RE.  (The first state in "RE+" is the closure state.) */
            NEW_STATE ;
            rtx->state_list[last_of (rrpvt[-1])].next1 = rtx->num_states ;
            rtx->state_list[rtx->num_states].type = closure ;
            rtx->state_list[rtx->num_states].x.min_closure = 1 ;
            rtx->state_list[rtx->num_states].y.max_closure = INT_MAX ;
            rtx->state_list[rtx->num_states].next1 = -1 ;
            rtx->state_list[rtx->num_states].next2 = rrpvt[-1] ;
            rrval = rtx->num_states++ ;
          } break;
case 14:
# line 462 "rex_util_y.y"
{ display_production ("factor", "factor?") ;
            /* Check for nested closures.  Even though nested closures are
               not a major threat in a zero-or-one closure, we check anyway,
               just to be consistent with the other forms of closure. */
            for (i = 0 ;  i < rtx->num_states ;  i++)
                rtx->state_list[i].z.visited = 0 ;
            i = shortest_path (rrpvt[-1]) ;
            if (rex_util_debug)  printf ("(rrparse) Shortest path from state %d = %d\n", rrpvt[-1], i) ;
            if (i <= 0) {
                errno = EINVAL ;  rex_error_text = "nested empty closure" ;
                return (errno) ;
            }
            /* Add a closure state.  Link the last state in RE to the closure
               state; link the closure state backwards to the first state in
               RE.  (The first state in "RE?" is the closure state.) */
            NEW_STATE ;
            rtx->state_list[last_of (rrpvt[-1])].next1 = rtx->num_states ;
            rtx->state_list[rtx->num_states].type = closure ;
            rtx->state_list[rtx->num_states].x.min_closure = 0 ;
            rtx->state_list[rtx->num_states].y.max_closure = 1 ;
            rtx->state_list[rtx->num_states].next1 = -1 ;
            rtx->state_list[rtx->num_states].next2 = rrpvt[-1] ;
            rrval = rtx->num_states++ ;
          } break;
case 15:
# line 487 "rex_util_y.y"
{ CS_ZERO (&char_class) ;
            rrparse_after_bracket = 1 ;
            rrparse_hyphen_as_literal = 0 ;
          } break;
case 16:
# line 492 "rex_util_y.y"
{ display_production ("factor", "[character_classes]") ;
            rrparse_after_bracket = 0 ;
            if (rrpvt[-2]) {
                for (i = 0 ;  i < CS_SETSIZE ;  i++) {
                    if (CS_ISSET (i, &char_class))
                        CS_CLR (i, &char_class) ;
                    else
                        CS_SET (i, &char_class) ;
                }
            }
            if (rex_util_debug) {
                printf ("Character Class:\n") ;
                rex_dump_class (stdout, "    Matches: ", &char_class) ;
            }
            /* Add a multi-character state for the character class. */
            NEW_STATE ;
            rtx->state_list[rtx->num_states].type = match ;
            rtx->state_list[rtx->num_states].x.match_char = 0 ;
            rtx->state_list[rtx->num_states].y.match_charset =
                (cs_set *) malloc (sizeof (cs_set)) ;
            if (rtx->state_list[rtx->num_states].y.match_charset == NULL) {
                vperror ("(rrparse) Error allocating character class set.\nmalloc: ") ;
                rex_error_text = "error allocating character class set" ;
                return (errno) ;
            }
            CS_ZERO (rtx->state_list[rtx->num_states].y.match_charset) ;
            for (i = 0 ;  i < CS_SETSIZE ;  i++)
                if (CS_ISSET (i, &char_class))
                    CS_SET (i, rtx->state_list[rtx->num_states].y.match_charset) ;
            rtx->state_list[rtx->num_states].next1 = -1 ;
            rtx->state_list[rtx->num_states].next2 = -1 ;
            rrval = rtx->num_states++ ;
          } break;
case 17:
# line 526 "rex_util_y.y"
{ display_production ("factor", "SINGLE_CHARACTER") ;
            /* Add a one-character state for the character. */
            NEW_STATE ;
            rtx->state_list[rtx->num_states].type = match ;
            rtx->state_list[rtx->num_states].x.match_char = rrpvt[-0] ;
            rtx->state_list[rtx->num_states].next1 = -1 ;
            rtx->state_list[rtx->num_states].next2 = -1 ;
            rrval = rtx->num_states++ ;
          } break;
case 18:
# line 536 "rex_util_y.y"
{ display_production ("factor", "COMMA") ;
            /* Add a one-character state for ",". */
            NEW_STATE ;
            rtx->state_list[rtx->num_states].type = match ;
            rtx->state_list[rtx->num_states].x.match_char = rrpvt[-0] ;
            rtx->state_list[rtx->num_states].next1 = -1 ;
            rtx->state_list[rtx->num_states].next2 = -1 ;
            rrval = rtx->num_states++ ;
          } break;
case 19:
# line 546 "rex_util_y.y"
{ display_production ("factor", "DIGIT") ;
            /* Add a one-character state for the digit. */
            NEW_STATE ;
            rtx->state_list[rtx->num_states].type = match ;
            rtx->state_list[rtx->num_states].x.match_char = rrpvt[-0] ;
            rtx->state_list[rtx->num_states].next1 = -1 ;
            rtx->state_list[rtx->num_states].next2 = -1 ;
            rrval = rtx->num_states++ ;
          } break;
case 20:
# line 556 "rex_util_y.y"
{ display_production ("factor", "ANY_CHARACTER") ;
            /* Add a one-character state for any-character-matches. */
            NEW_STATE ;
            rtx->state_list[rtx->num_states].type = match ;
            rtx->state_list[rtx->num_states].x.match_char = -1 ;
            rtx->state_list[rtx->num_states].next1 = -1 ;
            rtx->state_list[rtx->num_states].next2 = -1 ;
            rrval = rtx->num_states++ ;
          } break;
case 21:
# line 570 "rex_util_y.y"
{ display_production ("assignment", "<empty>") ;  rrval = -1 ; } break;
case 22:
# line 572 "rex_util_y.y"
{ display_production ("assignment", "$<0-9>") ;
            sprintf (buffer, "%c", rrpvt[-0]) ;  rrval = atoi (buffer) ;
          } break;
case 23:
# line 580 "rex_util_y.y"
{ display_production ("complement", "<empty>") ;  rrval = 0 ; } break;
case 24:
# line 582 "rex_util_y.y"
{ display_production ("complement", "^") ;  rrval = -1 ; } break;
case 25:
# line 588 "rex_util_y.y"
{ display_production ("number", "<empty>") ;  rrval = -1 ; } break;
case 26:
# line 590 "rex_util_y.y"
{ display_production ("number", "digits") ;  rrval = rrpvt[-0] ; } break;
case 27:
# line 596 "rex_util_y.y"
{ display_production ("upper_bound", "<empty>") ;
            rrval = -1 ;			/* Upper bound = lower bound. */
          } break;
case 28:
# line 600 "rex_util_y.y"
{ display_production ("lower_bound", "digits") ;
            if (rrpvt[-0] < 0)
                rrval = INT_MAX ;		/* Upper bound = infinity. */
            else
                rrval = rrpvt[-0] ;		/* Normal upper bound. */
          } break;
case 29:
# line 611 "rex_util_y.y"
{ display_production ("digits", "DIGIT") ;
            sprintf (buffer, "%c", rrpvt[-0]) ;  rrval = atoi (buffer) ;
          } break;
case 30:
# line 615 "rex_util_y.y"
{ display_production ("digits", "digits DIGIT") ;
            sprintf (buffer, "%c", rrpvt[-0]) ;  rrval = (rrpvt[-1] * 10) + atoi (buffer) ;
          } break;
case 31:
# line 623 "rex_util_y.y"
{ display_production ("character_classes", "character_class") ; } break;
case 32:
# line 625 "rex_util_y.y"
{ display_production ("character_classes", "character_classes character_class") ; } break;
case 33:
# line 631 "rex_util_y.y"
{ display_production ("character_class", "character") ;
            CS_SET (rrpvt[-0], &char_class) ;
          } break;
case 34:
# line 635 "rex_util_y.y"
{ rrparse_hyphen_as_literal = 1 ; } break;
case 35:
# line 637 "rex_util_y.y"
{ display_production ("character_class", "character-character") ;
            rrparse_hyphen_as_literal = 0 ;
            if (rrpvt[-3] > rrpvt[-0]) {
                if (rex_util_debug)  printf ("(rrparse) Start character exceeds end character in character class range \"%c-%c\".\n", rrpvt[-3], rrpvt[-0]) ;
                errno = EINVAL ;
                rex_error_text = "inverted character class range" ;
                return (errno) ;
            }
            for (i = rrpvt[-3] ;  i <= rrpvt[-0] ;  i++)
                CS_SET (i, &char_class) ;
          } break;
case 36:
# line 649 "rex_util_y.y"
{ display_production ("character_class", ":<class>:") ;
            for (i = 0 ;  i < CS_SETSIZE ;  i++) {
                switch (rrpvt[-0]) {
                case  0:  if (isalnum (i))  CS_SET (i, &char_class) ;  break ;
                case  1:  if (isalpha (i))  CS_SET (i, &char_class) ;  break ;
                case  2:  if (iscntrl (i))  CS_SET (i, &char_class) ;  break ;
                case  3:  if (isdigit (i))  CS_SET (i, &char_class) ;  break ;
                case  4:  if (isgraph (i))  CS_SET (i, &char_class) ;  break ;
                case  5:  if (islower (i))  CS_SET (i, &char_class) ;  break ;
                case  6:  if (isprint (i))  CS_SET (i, &char_class) ;  break ;
                case  7:  if (ispunct (i))  CS_SET (i, &char_class) ;  break ;
                case  8:  if (isspace (i))  CS_SET (i, &char_class) ;  break ;
                case  9:  if (isupper (i))  CS_SET (i, &char_class) ;  break ;
                case 10:  if (isxdigit (i)) CS_SET (i, &char_class) ;  break ;
                }
            }
          } break;
case 37:
# line 671 "rex_util_y.y"
{ display_production ("character", "SINGLE_CHARACTER") ;  rrval = rrpvt[-0] ; } break;
# line	532 "/usr/ccs/bin/yaccpar"
	}
	goto rrstack;		/* reset registers in driver code */
}

