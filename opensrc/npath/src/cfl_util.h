/*
@(#)  FILE: cfl_util.h  RELEASE: 1.1  DATE: 01/06/97, 16:37:40
*/
/*******************************************************************************

    cfl_util.h

    CFLOW(1) Utility definitions.

*******************************************************************************/

#ifndef  CFL_UTIL_H		/* Has the file been INCLUDE'd already? */
#define  CFL_UTIL_H  yes

#ifdef __cplusplus
    extern  "C" {
#endif


#include  "ansi_setup.h"		/* ANSI or non-ANSI C? */


/*******************************************************************************
    Module (Client View) and Definitions.
*******************************************************************************/

typedef  struct  _Module  *Module ;	/* Module handle. */


/*******************************************************************************
    Miscellaneous declarations.
*******************************************************************************/

extern  int  cfl_util_debug ;		/* Global debug switch (1/0 = yes/no). */


/*******************************************************************************
    Public Functions
*******************************************************************************/

extern  int  cflDefine P_((const char *name,
                           const char *file,
                           int sourceLine,
                           int outputLine,
                           int isAsync,
                           int isStatic,
                           Module *module)) ;

extern  int  cflDump P_((FILE *file,
                         const char *header)) ;

extern  int  cflInfo P_((const char *name,
                         char **file,
                         int *sourceLine,
                         int *outputLine,
                         int *isAsync,
                         int *isStatic)) ;

extern  const  char  *cfl_name P_((Module module));

extern  int  cflRefer P_((const char *name,
                          Module *module)) ;


#ifdef __cplusplus
    }
#endif

#endif				/* If this file was not INCLUDE'd previously. */
