/*
@(#)  FILE: cfl_util.c  RELEASE: 1.1  DATE: 01/06/97, 16:37:38
*/
/*******************************************************************************

File:

    cfl_util.c

    CFLOW(1) Utilities.


Author:    Alex Measday, ISI


Purpose:

    The CFLOW(1) Utilities maintain the information needed by NPATH to generate
    a CFLOW(1)-style structure chart.


Public Procedures:

    cflDefine() - returns a newly-defined module.
    cflDump() - dumps the list of modules.
    cflInfo() - returns information about a module.
    cflName() - returns the name of a module.
    cflRefer() - returns a referenced module.

Private Procedures:

    cflLocate() - locates a module by name in the module list.

*******************************************************************************/


#include  <errno.h>			/* System error definitions. */
#include  <stdio.h>			/* Standard I/O definitions. */
#include  <stdlib.h>			/* Standard C library definitions. */
#include  <string.h>			/* C Library string functions. */
#include  "fnm_util.h"			/* Filename utilities. */
#include  "str_util.h"			/* String manipulation functions. */
#include  "vperror.h"			/* VPERROR() definitions. */
#include  "cfl_util.h"			/* CFLOW(1) utility definitions. */


/*******************************************************************************
    Modules - contain information about modules.  When an undefined module is
        referenced, a module structure is created for the module, but the file
        name and line number are not filled in.  When a module is defined, its
        file name and line number are filled in.
*******************************************************************************/

typedef  struct  _Module {
    char  *name ;			/* Module name. */
    char  *file ;			/* File in which defined. */
    int  sourceLine ;			/* Line # of definition (in source). */
    int  outputLine ;			/* Line # of definition (in output). */
    int  isAsync ;			/* Method of invocation. */
    int  isStatic ;			/* Definition type. */
    struct  _Module  *next ;
}  _Module ;

					/* List of defined/referenced modules. */
static  Module  cflModuleList = NULL ;

int  cfl_util_debug = 0 ;		/* Global debug switch (1/0 = yes/no). */


/*******************************************************************************
    Private Functions
*******************************************************************************/

static  Module  cflLocate (
#    if __STDC__
        const  char  *name
#    endif
    ) ;

/*******************************************************************************

Procedure:

    cflDefine ()

    Defines a Module.


Purpose:

    Function cflDefine() adds a new module to the list of modules.  If the
    module is already in the list (i.e., because of a reference to the
    module before it was defined), cflDefine() simply updates certain
    fields in the module's structure.


    Invocation:

        status = cflDefine (name, file, sourceLine, outputLine,
                            isAsync, isStatic, &module) ;

    where:

        <name>		- I
            is the module's name.
        <file>		- I
            is the name of the file in which the module is defined.  This
            argument should be NULL if the file name is not to be updated.
        <sourceLine>	- I
            is the line number at which the module definition is found in
            its source file.  This argument should be less than zero if the
            line number is not to be updated.
        <outputLine>	- I
            is the line number at which the module definition is found in
            NPATH's CFLOW(1) output.  This argument should be less than zero
            if the line number is not to be updated.
        <isAsync>	- I
            indicates the method by which the module is invocated: false
            (zero) means that the module is called directly; true (greater
            than zero) means that the module is called asynchronously (e.g.,
            signal handlers, callback routines, etc.).  This argument should
            be less than zero if this flag is not to be updated.
        <isStatic>	- I
            indicates whether or not the module was declared "static": false
            (zero) means that the module is a public module; true (greater
            than zero) means that the module is a private module.  This
            argument should be less than zero if this flag is not to be
            updated.
        <module>	- O
            returns a handle that can be used in other CFL_UTIL calls.
        <status>	- O
            returns the status of defining the module, zero if there were
            no errors and ERRNO otherwise.

*******************************************************************************/


int  cflDefine (

#    if __STDC__
        const  char  *name,
        const  char  *file,
        int  sourceLine,
        int  outputLine,
        int  isAsync,
        int  isStatic,
        Module  *module)
#    else
        name, file, sourceLine, outputLine, isAsync, isStatic, module)

        char  *name ;
        char  *file ;
        int  sourceLine ;
        int  outputLine ;
        int  isAsync ;
        int  isStatic ;
        Module  *module ;
#    endif

{    /* Local variables. */
    Module  cfl ;




/* If this is a brand-new definition, then allocate a structure for the module
   and add it to the module list.  Otherwise, use the module structure returned
   by cflLocate(). */

    cfl = cflLocate (name) ;

    if (cfl == NULL) {			/* New definition? */

        cfl = (Module) malloc (sizeof (_Module)) ;
        if (cfl == NULL) {
            vperror ("(cflDefine) Error allocating module structure for \"%s\".\nmalloc: ",
                     name) ;
            return (errno) ;
        }
        cfl->name = strdup (name) ;
        cfl->file = NULL ;
        cfl->sourceLine = -1 ;
        cfl->outputLine = -1 ;
        cfl->isAsync = -1 ;
        cfl->isStatic = -1 ;

        cfl->next = cflModuleList ;
        cflModuleList = cfl ;

        if (cfl_util_debug)  printf ("(cflDefine) Defined \"%s\".\n", name) ;

    }


/* Update the specified fields in the module's structure. */


    if (file != NULL) {
        if (cfl->file != NULL)  free (cfl->file) ;
#ifdef vms					/* Strip disk and directory. */
      { char  *s ;
        FileName  fileName ;

        fileName = fnmCreate (file, NULL) ;
        cfl->file = strdup (fnmFile (fileName)) ;
        fnmDestroy (fileName) ;
        s = strchr (cfl->file, ';') ;
        if (s != NULL)  *s = '\0' ;		/* Trim version number. */
      }
#else
        cfl->file = strdup (file) ;
#endif
    }
    if (sourceLine >= 0)  cfl->sourceLine = sourceLine ;
    if (outputLine >= 0)  cfl->outputLine = outputLine ;
    if (isAsync >= 0)  cfl->isAsync = isAsync ;
    if (isStatic >= 0)  cfl->isStatic = isStatic ;

    if (module != NULL)  *module = cfl ;


    return (0) ;

}

/*******************************************************************************

Procedure:

    cflDump ()

    Dump the Module List.


Purpose:

    Function cflDump() provides a formatted dump of the list of modules
    maintained by the CFL_UTIL package.


    Invocation:

        status = cflDump (file, header) ;

    where:

        <file>		- I
            is the UNIX descriptor for the output file.  If NULL, the
            file pointer defaults to standard output (STDOUT).
        <header>	- I
            is a text string to be output as a header.  The string is
            actually used as the format string in an FPRINTF statement,
            so you need to include any newlines ("\n"), etc. that you
            need.  This argument can be NULL.
        <status>	- O
            returns the status of dumping the class list, zero if no
            errors occur and ERRNO otherwise.

*******************************************************************************/


int  cflDump (

#    if __STDC__
        FILE  *file,
        const  char  *header)
#    else
        file, header)

        FILE  *file ;
        char  *header ;
#    endif

{    /* Local variables. */
    Module  cfl ;



    if (file == (FILE *) NULL)  file = stdout ;

    if (header != NULL)  fprintf (file, header) ;

    for (cfl = cflModuleList ;  cfl != NULL ;  cfl = cfl->next) {
        fprintf (file, "Module: %s\tSource: %s\n",
                 cfl->name, (cfl->file == NULL) ? "" : cfl->file) ;
        fprintf (file, "    Source Line: %d\tOutput Line: %d\n",
                 cfl->sourceLine, cfl->outputLine) ;
    }

    return (0) ;

}

/*******************************************************************************

Procedure:

    cflInfo ()

    Return Information about a Module.


Purpose:

    Function cflInfo() returns information about a module.


    Invocation:

        status = cflInfo (name, &file, &sourceLine, &outputLine,
                           &isAsync, &isStatic) ;

    where:

        <name>		- I
            is the name of the module for which information is desired.
        <file>		- O
            returns the name of the file in which the module is defined; NULL
            is returned if the module hasn't been defined yet.  The file name
            is stored in memory allocated by the CFL_UTIL package and it
            should not be modified or freed by the caller.
        <sourceLine>	- O
            returns the line number at which the module was defined in its
            source file; -1 is returned if the module has not been defined yet.
        <outputLine>	- O
            returns the line number at which the module was defined in NPATH's
            CFLOW(1) output; -1 is returned if the module has not been defined
            yet in that output.
        <isAsync>	- O
            returns true (greater than zero) if the module is invoked
            asynchronously; false (zero) is returned if the module is
            called directly.  -1 is returned if the module has not been
            defined yet.
        <isStatic>	- O
            returns true (greater than zero) if the module is defined as
            "static" and is invisible outside of the file in which it is
            defined; false (zero) is returned if the module is not defined
            as "static".  -1 is returned if the module has not been defined
            yet.
        <status>	- O
            returns the status of obtaining the information from the module,
            zero if there were no errors and ERRNO otherwise.

*******************************************************************************/


int  cflInfo (

#    if __STDC__
        const  char  *name,
        char  **file,
        int  *sourceLine,
        int  *outputLine,
        int  *isAsync,
        int  *isStatic)
#    else
        name, file, sourceLine, outputLine, isAsync, isStatic)

        char  *name ;
        char  **file ;
        int  *sourceLine ;
        int  *outputLine ;
        int  *isAsync ;
        int  *isStatic ;
#    endif

{    /* Local variables. */
    Module  cfl ;




/* Lookup the module's name. */

    cfl = cflLocate (name) ;
    if (cfl == NULL) {
        if (file != NULL)  *file = NULL ;
        if (sourceLine != NULL)  *sourceLine = -1 ;
        if (outputLine != NULL)  *outputLine = -1 ;
        if (isAsync != NULL)  *isAsync = -1 ;
        if (isStatic != NULL)  *isStatic = -1 ;
        errno = EINVAL ;
        vperror ("(cflInfo) Module \"%s\" not found.\n", name) ;
        return (errno) ;
    }

/* Return the desired fields from the module. */

    if (file != NULL)  *file = cfl->file ;
    if (sourceLine != NULL)  *sourceLine = cfl->sourceLine ;
    if (outputLine != NULL)  *outputLine = cfl->outputLine ;
    if (isAsync != NULL)  *isAsync = cfl->isAsync ;
    if (isStatic != NULL)  *isStatic = cfl->isStatic ;

    return (0) ;

}

/*******************************************************************************

Procedure:

    cflName ()

    Return Module's Name.


Purpose:

    Function cflName() returns the name of a module.


    Invocation:

        name = cflName (module) ;

    where:

        <module>	- I
            is the module handle returned by cflDefine() or cflRefer().
        <name>		-O
            returns the name of the module.  The name is stored in memory
            dynamically-allocated by the CFL_UTIL package and it should not
            be modified or freed by the caller.

*******************************************************************************/


const  char  *cflName (

#    if __STDC__
        Module  module)
#    else
        module)

        Module  module ;
#    endif

{

    return ((module == NULL) ? NULL : module->name) ;

}

/*******************************************************************************

Procedure:

    cflRefer ()

    References a Module.


Purpose:

    Function cflRefer() checks to see if a module is in the list of modules.
    If not, the module is added - as an undefined module - to the list.


    Invocation:

        status = cflRefer (name, &module) ;

    where:

        <name>		- I
            is the module's name.
        <module>	- O
            returns a handle that can be used in other CFL_UTIL calls.
        <status>	- O
            returns the status of referencing the module, zero if there were
            no errors and ERRNO otherwise.

*******************************************************************************/


int  cflRefer (

#    if __STDC__
        const  char  *name,
        Module  *module)
#    else
        name, module)

        char  *name ;
        Module  *module ;
#    endif

{
    return (cflDefine (name, NULL, -1, -1, -1, -1, module)) ;
}

/*******************************************************************************

Procedure:

    cflLocate ()

    Locate a Module by Name in the Module List.


Purpose:

    Function cflLocate() locates a module by name in the module list.


    Invocation:

        module = cflLocate (name) ;

    where:

        <name>		- I
            is the name of the module.
        <module>	- O
            returns a pointer to the named module structure.  NULL is returned
            if the name is not found in the module list.

*******************************************************************************/


static  Module  cflLocate (

#    if __STDC__
        const  char  *name)
#    else
        name)

        char  *name ;
#    endif

{    /* Local variables. */
    Module  cfl ;



    if (name == NULL)  return (NULL) ;

/* Scan the list of modules, looking for a module with the desired name. */

    for (cfl = cflModuleList ;  cfl != NULL ;  cfl = cfl->next) {
        if (strcmp (cfl->name, name) == 0)  break ;
    }

    return (cfl) ;

}
