#include <stdio.h>
# define U(x) x
# define NLSTATE yyprevious=YYNEWLINE
# define BEGIN yybgin = yysvec + 1 +
# define INITIAL 0
# define YYLERR yysvec
# define YYSTATE (yyestate-yysvec-1)
# define YYOPTIM 1
# define YYLMAX BUFSIZ
#ifndef __cplusplus
# define output(c) (void)putc(c,yyout)
#else
# define lex_output(c) (void)putc(c,yyout)
#endif

#if defined(__cplusplus) || defined(__STDC__)

#if defined(__cplusplus) && defined(__EXTERN_C__)
extern "C" {
#endif
	int yyback(int *, int);
	int yyinput(void);
	int yylook(void);
	void yyoutput(int);
	int yyracc(int);
	int yyreject(void);
	void yyunput(int);
	int yylex(void);
#ifdef YYLEX_E
	void yywoutput(wchar_t);
	wchar_t yywinput(void);
#endif
#ifndef yyless
	int yyless(int);
#endif
#ifndef yywrap
	int yywrap(void);
#endif
#ifdef LEXDEBUG
	void allprint(char);
	void sprint(char *);
#endif
#if defined(__cplusplus) && defined(__EXTERN_C__)
}
#endif

#ifdef __cplusplus
extern "C" {
#endif
	void exit(int);
#ifdef __cplusplus
}
#endif

#endif
# define unput(c) {yytchar= (c);if(yytchar=='\n')yylineno--;*yysptr++=yytchar;}
# define yymore() (yymorfg=1)
#ifndef __cplusplus
# define input() (((yytchar=yysptr>yysbuf?U(*--yysptr):getc(yyin))==10?(yylineno++,yytchar):yytchar)==EOF?0:yytchar)
#else
# define lex_input() (((yytchar=yysptr>yysbuf?U(*--yysptr):getc(yyin))==10?(yylineno++,yytchar):yytchar)==EOF?0:yytchar)
#endif
#define ECHO fprintf(yyout, "%s",yytext)
# define REJECT { nstr = yyreject(); goto yyfussy;}
int yyleng;
char yytext[YYLMAX];
int yymorfg;
extern char *yysptr, yysbuf[];
int yytchar;

#if 0   /* compiles with g++ but other problems */
/* AMS -- syntax error fixed ? */
/* use g++ INSTEAD of gcc and won't get an error */
/*FILE *yyin = {stdin}, *yyout = {stdout}; */
FILE *yyin = {stdin}, *yyout = {stdout};
/* 
-works also with no initialization or initialize to 0
I don't see any reason why this needs to be initialized?
other than good practice.

FILE *yyin = 0, *yyout = 0; 
FILE *yyin = stdin, *yyout = stdout;
*/
#else

/* see npath.c - main() which initializes to stdin and stdout */
FILE *yyin, *yyout; 

#endif

extern int yylineno;
struct yysvf { 
	struct yywork *yystoff;
	struct yysvf *yyother;
	int *yystops;};
struct yysvf *yyestate;
extern struct yysvf yysvec[], *yybgin;

# line 3 "npath_lex.l"
/*
@(#)  FILE: npath_lex.l  RELEASE: 1.2  DATE: 12/29/97, 17:49:57
*/

# line 6 "npath_lex.l"
/*******************************************************************************

Procedure:

    yylex ()


Purpose:

      *****  This LEX file was derived from the ANSI C scanner  *****
                 posted on USENET by Jeff Lee of GA Tech .

    This source file contains the pattern matching rules and actions for
    the NPATH scanner.  It is fed into the LEX or FLEX lexical analyzer
    generators, which generate the C Language code for the NPATH scanner.
    The generated C Language code contains a function called YYLEX.


    Invocation:

        token_type = yylex () ;

    where

        <token_type>
            returns the type of the next token in the input stream; zero
            is returned on end-of-file.  The actual value of the token is
            stored in global variable YYLVAL.

*******************************************************************************/


#include  <limits.h>			/* Maximum/minimum value definitions. */
#if defined(vms)
#    include  <nam.h>			/* RMS name block (NAM) definitions. */
#    define  PATH_MAX  NAM$C_MAXRSS
#elif !defined(PATH_MAX)
#    include  <sys/param.h>		/* System parameters. */
#    define  PATH_MAX  MAXPATHLEN
#endif
#include  <stdio.h>			/* Standard I/O definitions. */
#include  <stdlib.h>			/* Standard C library definitions. */
#include  "get_util.h"			/* "Get Next" functions. */
#include  "hash_util.h"			/* Hash table definitions. */
#include  "str_util.h"			/* String manipulation functions. */
#include  "npath.h"			/* NPATH definitions. */
#ifdef vms
#    include  "y_tab.h"			/* YACC token definitions. */
#else
#    include  "y.tab.h"			/* YACC token definitions. */
#endif

#ifdef yyin
#    undef yyin
     FILE  *yyin = NULL ;
#endif


# line 63 "npath_lex.l"
/********************************************************************************
    Internal functions.
*******************************************************************************/

int  check_operand (
#    if __STDC__
        char  *operand
#    endif
    ) ;

int  check_operator (
#    if __STDC__
        char  *operator
#    endif
    ) ;

static  int  check_type () ;

#ifdef FLEX_SCANNER

    static  int  cpp_input (
#        if __STDC__
            char  *buffer,
            int  max_length
#        endif
        ) ;

#else

    static  int  cpp_input () ;

    static  void  cpp_unput (
#        if __STDC__
            char  c
#        endif
        ) ;

#endif


#ifdef FLEX_SCANNER
    int  yylineno ;
#    undef  YY_INPUT			/* Redefine the FLEX input routine. */
#    define  YY_INPUT(buf,result,max_size)  \
        { result = cpp_input (buf, max_size) ; }
#else
#    define  yywrap()  1
#    undef   input			/* Redefine the LEX input routines. */
#    define  input  cpp_input
#    undef   unput
#    define  unput  cpp_unput
#    define  YY_NULL  0
#endif


#define  MAX_LEX_INPUT  1024


#define  CHECK_OPERAND(name) \
    { if (m != NULL)  check_operand (name) ; }
#define  CHECK_OPERATOR(name) \
    { if (m != NULL)  check_operator (name) ; }


					/* Global variables. */
extern  int  yydebug ;
char  cpp_file_name[PATH_MAX] ;
int  cpp_line_number ;
char  lex_input_buffer[MAX_LEX_INPUT+1] ;
int  lex_next_char ;


# define YYNEWLINE 10
yylex(){
int nstr; extern int yyprevious;
#ifdef __cplusplus
/* to avoid CC and lint complaining yyfussy not being used ...*/
static int __lex_hack = 0;
if (__lex_hack) goto yyfussy;
#endif
while((nstr = yylook()) >= 0)
yyfussy: switch(nstr){
case 0:
if(yywrap()) return(0); break;
case 1:

# line 147 "npath_lex.l"
		{ return (AUTO); }
break;
case 2:

# line 148 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (BREAK); }
break;
case 3:

# line 149 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (CASE); }
break;
case 4:

# line 150 "npath_lex.l"
		{ return (_CHAR); }
break;
case 5:

# line 151 "npath_lex.l"
		{ return (CONST); }
break;
case 6:

# line 152 "npath_lex.l"
	{ CHECK_OPERATOR (yytext) ;  return (CONTINUE); }
break;
case 7:

# line 153 "npath_lex.l"
	{ CHECK_OPERATOR (yytext) ;  return (DEFAULT); }
break;
case 8:

# line 154 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (DO); }
break;
case 9:

# line 155 "npath_lex.l"
	{ return (_DOUBLE); }
break;
case 10:

# line 156 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (ELSE); }
break;
case 11:

# line 157 "npath_lex.l"
		{ return (_ENUM); }
break;
case 12:

# line 158 "npath_lex.l"
	{ return (EXTERN); }
break;
case 13:

# line 159 "npath_lex.l"
		{ return (_FLOAT); }
break;
case 14:

# line 160 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (FOR); }
break;
case 15:

# line 161 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (GOTO); }
break;
case 16:

# line 162 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (IF); }
break;
case 17:

# line 163 "npath_lex.l"
		{ return (_INT); }
break;
case 18:

# line 164 "npath_lex.l"
		{ return (_LONG); }
break;
case 19:

# line 165 "npath_lex.l"
	{ return (REGISTER); }
break;
case 20:

# line 166 "npath_lex.l"
	{ CHECK_OPERATOR (yytext) ;  return (RETURN); }
break;
case 21:

# line 167 "npath_lex.l"
		{ return (_SHORT); }
break;
case 22:

# line 168 "npath_lex.l"
	{ return (SIGNED); }
break;
case 23:

# line 169 "npath_lex.l"
	{ CHECK_OPERATOR (yytext) ;  return (SIZEOF); }
break;
case 24:

# line 170 "npath_lex.l"
	{ return (STATIC); }
break;
case 25:

# line 171 "npath_lex.l"
	{ return (STRUCT); }
break;
case 26:

# line 172 "npath_lex.l"
	{ CHECK_OPERATOR (yytext) ;  return (SWITCH); }
break;
case 27:

# line 173 "npath_lex.l"
	{ return (TYPEDEF); }
break;
case 28:

# line 174 "npath_lex.l"
		{ return (UNION); }
break;
case 29:

# line 175 "npath_lex.l"
	{ return (UNSIGNED); }
break;
case 30:

# line 176 "npath_lex.l"
{ return (VARIANT_STRUCT); }
break;
case 31:

# line 177 "npath_lex.l"
	{ return (VARIANT_UNION); }
break;
case 32:

# line 178 "npath_lex.l"
		{ return (VOID); }
break;
case 33:

# line 179 "npath_lex.l"
	{ return (VOLATILE); }
break;
case 34:

# line 180 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (WHILE); }
break;
case 35:

# line 182 "npath_lex.l"
	{ return (GLOBALDEF); }
break;
case 36:

# line 183 "npath_lex.l"
	{ return (GLOBALREF); }
break;
case 37:

# line 184 "npath_lex.l"
	{ return (GLOBALVALUE); }
break;
case 38:

# line 185 "npath_lex.l"
	{ return (READONLY); }
break;
case 39:

# line 186 "npath_lex.l"
	{ return (NOSHARE); }
break;
case 40:

# line 187 "npath_lex.l"
	{ return (_ALIGN); }
break;
case 41:

# line 189 "npath_lex.l"
	{ return (ELIPSIS); }
break;
case 42:

# line 191 "npath_lex.l"
	{ CHECK_OPERAND (yytext) ;  return (check_type()); }
break;
case 43:

# line 193 "npath_lex.l"
	{ CHECK_OPERAND (yytext) ;  return (CONSTANT); }
break;
case 44:

# line 194 "npath_lex.l"
	{ CHECK_OPERAND (yytext) ;  return (CONSTANT); }
break;
case 45:

# line 195 "npath_lex.l"
	{ CHECK_OPERAND (yytext) ;  return (CONSTANT); }
break;
case 46:

# line 196 "npath_lex.l"
	{ CHECK_OPERAND (yytext) ;  return (CONSTANT); }
break;
case 47:

# line 197 "npath_lex.l"
	{ CHECK_OPERAND (yytext) ;  return (CONSTANT); }
break;
case 48:

# line 198 "npath_lex.l"
	{ CHECK_OPERAND (yytext) ;  return (CONSTANT); }
break;
case 49:

# line 199 "npath_lex.l"
	{ CHECK_OPERAND (yytext) ;  return (CONSTANT); }
break;
case 50:

# line 201 "npath_lex.l"
	{ CHECK_OPERAND (yytext) ;  return (CONSTANT); }
break;
case 51:

# line 202 "npath_lex.l"
{ CHECK_OPERAND (yytext) ;  return (CONSTANT); }
break;
case 52:

# line 203 "npath_lex.l"
{ CHECK_OPERAND (yytext) ;  return (CONSTANT); }
break;
case 53:

# line 205 "npath_lex.l"
{ CHECK_OPERAND (yytext) ;  return (STRING_LITERAL); }
break;
case 54:

# line 207 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (RIGHT_ASSIGN); }
break;
case 55:

# line 208 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (LEFT_ASSIGN); }
break;
case 56:

# line 209 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (ADD_ASSIGN); }
break;
case 57:

# line 210 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (SUB_ASSIGN); }
break;
case 58:

# line 211 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (MUL_ASSIGN); }
break;
case 59:

# line 212 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (DIV_ASSIGN); }
break;
case 60:

# line 213 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (MOD_ASSIGN); }
break;
case 61:

# line 214 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (AND_ASSIGN); }
break;
case 62:

# line 215 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (XOR_ASSIGN); }
break;
case 63:

# line 216 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (OR_ASSIGN); }
break;
case 64:

# line 217 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (RIGHT_OP); }
break;
case 65:

# line 218 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (LEFT_OP); }
break;
case 66:

# line 219 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (INC_OP); }
break;
case 67:

# line 220 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (DEC_OP); }
break;
case 68:

# line 221 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (PTR_OP); }
break;
case 69:

# line 222 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (AND_OP); }
break;
case 70:

# line 223 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (OR_OP); }
break;
case 71:

# line 224 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (LE_OP); }
break;
case 72:

# line 225 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (GE_OP); }
break;
case 73:

# line 226 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (EQ_OP); }
break;
case 74:

# line 227 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (NE_OP); }
break;
case 75:

# line 228 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (';'); }
break;
case 76:

# line 229 "npath_lex.l"
		{ return ('{'); }
break;
case 77:

# line 230 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return ('}'); }
break;
case 78:

# line 231 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (','); }
break;
case 79:

# line 232 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return (':'); }
break;
case 80:

# line 233 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return ('='); }
break;
case 81:

# line 234 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return ('('); }
break;
case 82:

# line 235 "npath_lex.l"
		{ return (')'); }
break;
case 83:

# line 236 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return ('['); }
break;
case 84:

# line 237 "npath_lex.l"
		{ return (']'); }
break;
case 85:

# line 238 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return ('.'); }
break;
case 86:

# line 239 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return ('&'); }
break;
case 87:

# line 240 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return ('!'); }
break;
case 88:

# line 241 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return ('~'); }
break;
case 89:

# line 242 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return ('-'); }
break;
case 90:

# line 243 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return ('+'); }
break;
case 91:

# line 244 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return ('*'); }
break;
case 92:

# line 245 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return ('/'); }
break;
case 93:

# line 246 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return ('%'); }
break;
case 94:

# line 247 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return ('<'); }
break;
case 95:

# line 248 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return ('>'); }
break;
case 96:

# line 249 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return ('^'); }
break;
case 97:

# line 250 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return ('|'); }
break;
case 98:

# line 251 "npath_lex.l"
		{ CHECK_OPERATOR (yytext) ;  return ('?'); }
break;
case 99:

# line 253 "npath_lex.l"
		{ return ('#') ;	/* To catch unresolved
						   "#include"s passed on
						   by VAX C Preprocessor. */
			}
break;
case 100:

# line 258 "npath_lex.l"
	{ }
break;
case 101:

# line 259 "npath_lex.l"
		{ /* ignore bad characters */ }
break;
case -1:
break;
default:
(void)fprintf(yyout,"bad switch yylook %d",nstr);
} return(0); }
/* end of yylex */


/*******************************************************************************
    Auxiliary Functions.
*******************************************************************************/


/*******************************************************************************
    Checks if an identifier is a "typedef"ed type name or a regular identifier.
*******************************************************************************/

static  int  check_type (

#    if __STDC__
        void)
#    else
        )
#    endif

{

    if (yydebug)  printf ("(check_type) \"%s\"\n", yytext) ;

    if (type_name_as_identifier) {		/* Structure, union, or enum reference? */
        yylval.name = strdup (yytext) ;
        return (IDENTIFIER) ;
    }

    if ((htable_types != NULL) &&
        hashSearch (htable_types, yytext, (void **) NULL)) {
        return (TYPE_NAME) ;			/* Previously-defined type name.*/
    } else {
        yylval.name = strdup (yytext) ;
        return (IDENTIFIER) ;
    }

}

/*******************************************************************************
    Counts the number of unique operands and the total number of operands.
*******************************************************************************/


int  check_operand (

#    if __STDC__
        char  *operand)
#    else
        operand)

        char  *operand ;
#    endif

{

    if (m == NULL)  return (0) ;

    m->total_num_operands++ ;

    if (htable_operands == NULL) {
        printf ("Null operand hash table for module \"%s\".\n", m->name) ;
        hashCreate (128, 0, &htable_operands) ;
    }
    if (!hashSearch (htable_operands, operand, (void **) NULL)) {
        m->num_unique_operands++ ;
        hashAdd (htable_operands, operand, (void *) NULL) ;
    }

    return (0) ;

}

/*******************************************************************************
    Counts the number of unique operators and the total number of operators.
*******************************************************************************/


int  check_operator (

#    if __STDC__
        char  *operator)
#    else
        operator)

        char  *operator ;
#    endif

{

    if (m == NULL)  return (0) ;

    m->total_num_operators++ ;

    if (htable_operators == NULL) {
        printf ("Null operator hash table for module \"%s\".\n", m->name) ;
        hashCreate (128, 0, &htable_operators) ;
    }
    if (!hashSearch (htable_operators, operator, (void **) NULL)) {
        m->num_unique_operators++ ;
        hashAdd (htable_operators, operator, (void *) NULL) ;
    }

    return (0) ;

}

/*******************************************************************************

    CPP_INPUT - returns the next character of input read from the
        C Preprocessor.  (In a FLEX-generated lexer, CPP_INPUT returns
        a chunk of characters from the input line.)  CPP directives of
        the form:

                # <line_number> <file_name>

        are recognized and interpreted, so that the parser can know in
        what file and at what line number an error was detected.

*******************************************************************************/


#ifdef  FLEX_SCANNER

    static  int  cpp_input (

#        if __STDC__
            char  *buffer,		/* Buffer to receive data. */
            int  max_length)		/* Size of buffer. */
#        else
            buffer, max_length)

            char  *buffer ;
            int  max_length ;
#        endif

#else

    static  int  cpp_input (

#        if __STDC__
            void)
#        else
            )
#        endif

#endif

{    /* Local variables.*/
    char  *s ;
    int  length ;




/* At the beginning of a new file, reset everything.*/

    if (yylineno <= 0) {
        lex_input_buffer[0] = '\0' ;  lex_next_char = 0 ;
        cpp_file_name[0] = '\0' ;
    }


/* If necessary, read in a new line of input. */

    while (lex_input_buffer[lex_next_char] == '\0') {

        lex_input_buffer[0] = '\0' ;  lex_next_char = 0 ;

        if (fgets (lex_input_buffer, MAX_LEX_INPUT, yyin) == NULL)
            return (YY_NULL) ;				/* End of file. */

        yylineno++ ;

        if (echoInput)  printf ("%s\n", lex_input_buffer) ;

        if (lex_input_buffer[0] == '#') {		/* CPP directive? */
            length = 0 ;
            s = getword (lex_input_buffer, " \t", &length) ;
            s = getword (s, " \t", &length) ;
            cpp_line_number = atoi (s) - 1 ;
            s = getword (s, " \"\t", &length) ;
            strCopy (s, length, cpp_file_name, sizeof cpp_file_name) ;
            lex_input_buffer[0] = '\0' ;  lex_next_char = 0 ;
            continue ;
        }

        cpp_line_number++ ;
        length = strTrim (lex_input_buffer, -1) ;
        if (length == 0)  continue ;		/* Skip blank lines. */
        if (lex_input_buffer[length-1] == '\\')
            lex_input_buffer[length-1] = '\0' ;	/* Remove escaped newline. */
        else
            lex_input_buffer[length] = '\n' ;	/* Restore trimmed newline. */

    }


#ifdef  FLEX_SCANNER

/* Copy as many characters as you can from the input line into the calling
   routine's buffer. */

    for (length = 0 ;  length < max_length ;  length++, lex_next_char++) {
        buffer[length] = lex_input_buffer[lex_next_char] ;
        if (buffer[length] == '\0')  break ;
    }

    return (length) ;

#else

/* Return the next character from the input line. */

    return (lex_input_buffer[lex_next_char++]) ;

#endif

}

#ifndef  FLEX_SCANNER

/*******************************************************************************
    CPP_UNPUT - returns a character to the input stream.
*******************************************************************************/


static  void  cpp_unput (

#    if __STDC__
        char  c)
#    else
        c)

        char  c ;
#    endif

{
    if (lex_next_char > 0)  lex_input_buffer[--lex_next_char] = c ;
}


/*******************************************************************************
    YYRESTART - restarts the lexical analyzer.  This is essentially a dummy
        routine.  FLEX-generated lexers only read a single input file; before
        calling the lexer for a second input file, a FLEX-defined function,
        YYRESTART(), must be called to reset the lexer.  LEX-generated lexers
        don't need to be reset between files and, consequently, don't define
        the YYRESTART() function.  Here it is, "#ifndef FLEX"!
*******************************************************************************/

void  yyrestart (

#    if __STDC__
        FILE  *input_file)
#    else
        input_file)

        FILE  *input_file ;
#    endif

{
    yyin = input_file ;
    return ;
}

#endif  /* FLEX_SCANNER */
int yyvstop[] = {
0,

101,
0,

100,
101,
0,

100,
0,

87,
101,
0,

101,
0,

99,
101,
0,

42,
101,
0,

93,
101,
0,

86,
101,
0,

101,
0,

81,
101,
0,

82,
101,
0,

91,
101,
0,

90,
101,
0,

78,
101,
0,

89,
101,
0,

85,
101,
0,

92,
101,
0,

47,
48,
101,
0,

47,
48,
101,
0,

79,
101,
0,

75,
101,
0,

94,
101,
0,

80,
101,
0,

95,
101,
0,

98,
101,
0,

83,
101,
0,

84,
101,
0,

96,
101,
0,

42,
101,
0,

42,
101,
0,

42,
101,
0,

42,
101,
0,

42,
101,
0,

42,
101,
0,

42,
101,
0,

42,
101,
0,

42,
101,
0,

42,
101,
0,

42,
101,
0,

42,
101,
0,

42,
101,
0,

42,
101,
0,

42,
101,
0,

42,
101,
0,

42,
101,
0,

76,
101,
0,

97,
101,
0,

77,
101,
0,

88,
101,
0,

74,
0,

53,
0,

42,
0,

60,
0,

69,
0,

61,
0,

58,
0,

66,
0,

56,
0,

67,
0,

57,
0,

68,
0,

51,
0,

59,
0,

52,
0,

45,
46,
47,
48,
0,

47,
48,
0,

47,
48,
0,

65,
0,

71,
0,

73,
0,

72,
0,

64,
0,

62,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

8,
42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

16,
42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

63,
0,

70,
0,

49,
0,

41,
0,

51,
0,

51,
52,
0,

52,
0,

45,
46,
47,
48,
0,

50,
0,

43,
44,
0,

55,
0,

54,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

14,
42,
0,

42,
0,

42,
0,

17,
42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

51,
0,

51,
52,
0,

52,
0,

50,
0,

43,
44,
0,

42,
0,

1,
42,
0,

42,
0,

3,
42,
0,

4,
42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

10,
42,
0,

11,
42,
0,

42,
0,

42,
0,

42,
0,

15,
42,
0,

18,
42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

32,
42,
0,

42,
0,

42,
0,

51,
52,
0,

42,
0,

2,
42,
0,

5,
42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

13,
42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

21,
42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

28,
42,
0,

42,
0,

42,
0,

42,
0,

34,
42,
0,

40,
42,
0,

42,
0,

42,
0,

9,
42,
0,

12,
42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

20,
42,
0,

22,
42,
0,

23,
42,
0,

24,
42,
0,

25,
42,
0,

26,
42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

7,
42,
0,

42,
0,

42,
0,

42,
0,

39,
42,
0,

42,
0,

42,
0,

27,
42,
0,

42,
0,

42,
0,

42,
0,

6,
42,
0,

42,
0,

42,
0,

42,
0,

38,
42,
0,

19,
42,
0,

29,
42,
0,

42,
0,

33,
42,
0,

35,
42,
0,

36,
42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

37,
42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

42,
0,

31,
42,
0,

30,
42,
0,
0};
# define YYTYPE int
struct yywork { YYTYPE verify, advance; } yycrank[] = {
0,0,	0,0,	1,3,	0,0,	
0,0,	0,0,	0,0,	0,0,	
0,0,	0,0,	1,4,	1,5,	
0,0,	0,0,	0,0,	0,0,	
0,0,	0,0,	0,0,	0,0,	
0,0,	0,0,	0,0,	0,0,	
0,0,	0,0,	0,0,	0,0,	
0,0,	0,0,	0,0,	0,0,	
0,0,	0,0,	1,6,	1,7,	
1,8,	1,9,	1,10,	1,11,	
1,12,	1,13,	1,14,	1,15,	
1,16,	1,17,	1,18,	1,19,	
1,20,	1,21,	1,22,	1,22,	
1,22,	1,22,	1,22,	1,22,	
1,22,	1,22,	1,22,	1,23,	
1,24,	1,25,	1,26,	1,27,	
1,28,	6,53,	1,9,	10,58,	
4,5,	4,5,	1,9,	4,5,	
15,63,	19,69,	11,59,	19,70,	
19,70,	19,70,	19,70,	19,70,	
19,70,	19,70,	19,70,	19,70,	
19,70,	20,71,	26,80,	16,64,	
31,83,	1,9,	61,115,	4,5,	
1,29,	1,3,	1,30,	1,31,	
1,32,	11,60,	1,33,	1,34,	
1,35,	1,36,	1,37,	1,38,	
1,39,	16,65,	1,40,	25,78,	
25,79,	1,41,	32,84,	1,42,	
27,81,	27,82,	41,101,	1,43,	
1,44,	1,45,	1,46,	1,47,	
1,48,	33,85,	34,86,	42,102,	
1,49,	1,50,	1,51,	1,52,	
2,6,	43,103,	2,8,	38,95,	
2,10,	2,11,	38,96,	2,13,	
2,14,	2,15,	45,108,	2,17,	
2,18,	2,19,	2,20,	18,66,	
2,22,	2,22,	2,22,	2,22,	
2,22,	2,22,	2,22,	2,22,	
2,22,	2,23,	2,24,	2,25,	
2,26,	2,27,	2,28,	18,67,	
18,68,	35,87,	36,90,	40,99,	
37,92,	46,109,	37,93,	39,97,	
35,88,	47,110,	39,98,	40,100,	
36,91,	48,112,	69,116,	35,89,	
37,94,	50,113,	56,0,	78,126,	
82,127,	84,128,	85,129,	47,111,	
86,130,	87,131,	2,29,	88,132,	
2,30,	2,31,	2,32,	89,133,	
2,33,	2,34,	2,35,	2,36,	
2,37,	2,38,	2,39,	7,54,	
2,40,	90,134,	56,54,	2,41,	
91,135,	2,42,	62,0,	7,54,	
7,54,	2,43,	2,44,	2,45,	
2,46,	2,47,	2,48,	44,104,	
44,105,	92,136,	2,49,	2,50,	
2,51,	2,52,	93,137,	94,138,	
95,139,	96,140,	97,141,	44,106,	
98,142,	75,75,	44,107,	100,143,	
7,55,	101,144,	7,54,	62,61,	
102,145,	7,54,	75,75,	103,146,	
50,114,	7,54,	74,123,	104,149,	
74,123,	103,147,	7,54,	74,124,	
74,124,	74,124,	74,124,	74,124,	
74,124,	74,124,	74,124,	74,124,	
74,124,	107,154,	103,148,	108,155,	
56,54,	75,75,	9,57,	7,54,	
110,158,	105,150,	111,159,	7,54,	
106,152,	111,160,	75,75,	112,161,	
128,170,	129,171,	9,57,	9,57,	
9,57,	9,57,	9,57,	9,57,	
9,57,	9,57,	9,57,	9,57,	
105,151,	106,153,	7,54,	130,172,	
62,61,	131,173,	7,56,	9,57,	
9,57,	9,57,	9,57,	9,57,	
9,57,	9,57,	9,57,	9,57,	
9,57,	9,57,	9,57,	9,57,	
9,57,	9,57,	9,57,	9,57,	
9,57,	9,57,	9,57,	9,57,	
9,57,	9,57,	9,57,	9,57,	
9,57,	132,174,	133,175,	133,176,	
134,177,	9,57,	135,178,	9,57,	
9,57,	9,57,	9,57,	9,57,	
9,57,	9,57,	9,57,	9,57,	
9,57,	9,57,	9,57,	9,57,	
9,57,	9,57,	9,57,	9,57,	
9,57,	9,57,	9,57,	9,57,	
9,57,	9,57,	9,57,	9,57,	
9,57,	12,61,	136,179,	109,156,	
137,180,	138,181,	139,182,	141,183,	
142,184,	12,61,	12,61,	144,185,	
21,72,	109,157,	21,73,	21,73,	
21,73,	21,73,	21,73,	21,73,	
21,73,	21,73,	21,73,	21,73,	
70,70,	70,70,	70,70,	70,70,	
70,70,	70,70,	70,70,	70,70,	
70,70,	70,70,	12,61,	21,74,	
12,61,	145,186,	146,187,	12,0,	
119,164,	119,165,	21,75,	12,61,	
122,122,	70,117,	70,118,	119,165,	
12,61,	147,188,	148,189,	21,75,	
70,118,	122,122,	21,76,	123,124,	
123,124,	123,124,	123,124,	123,124,	
123,124,	123,124,	123,124,	123,124,	
123,124,	12,61,	149,190,	21,74,	
150,191,	12,61,	151,192,	152,193,	
119,164,	119,165,	21,75,	153,194,	
122,122,	70,117,	70,118,	119,165,	
154,195,	124,168,	155,196,	21,75,	
70,118,	122,122,	21,76,	124,168,	
12,61,	156,197,	157,198,	22,72,	
12,62,	22,77,	22,77,	22,77,	
22,77,	22,77,	22,77,	22,77,	
22,77,	22,77,	22,77,	72,119,	
72,119,	72,119,	72,119,	72,119,	
72,119,	72,119,	72,119,	72,119,	
72,119,	124,168,	22,74,	158,199,	
159,200,	125,169,	160,201,	124,168,	
161,202,	22,75,	170,205,	163,118,	
72,120,	72,121,	125,169,	172,206,	
175,207,	163,118,	22,75,	72,121,	
73,73,	73,73,	73,73,	73,73,	
73,73,	73,73,	73,73,	73,73,	
73,73,	73,73,	176,208,	177,209,	
178,210,	181,211,	22,74,	182,212,	
183,213,	125,169,	186,214,	187,215,	
188,216,	22,75,	189,217,	163,118,	
72,120,	72,121,	125,169,	190,218,	
73,122,	163,118,	22,75,	72,121,	
191,219,	192,220,	193,221,	194,222,	
195,223,	73,122,	196,224,	197,225,	
198,226,	199,227,	201,228,	202,229,	
76,125,	76,125,	76,125,	76,125,	
76,125,	76,125,	76,125,	76,125,	
76,125,	76,125,	205,230,	208,231,	
209,232,	210,233,	211,234,	213,235,	
73,122,	76,125,	76,125,	76,125,	
76,125,	76,125,	76,125,	214,236,	
117,162,	73,122,	117,162,	215,237,	
216,238,	117,163,	117,163,	117,163,	
117,163,	117,163,	117,163,	117,163,	
117,163,	117,163,	117,163,	162,163,	
162,163,	162,163,	162,163,	162,163,	
162,163,	162,163,	162,163,	162,163,	
162,163,	76,125,	76,125,	76,125,	
76,125,	76,125,	76,125,	120,166,	
217,239,	120,166,	219,240,	220,241,	
120,167,	120,167,	120,167,	120,167,	
120,167,	120,167,	120,167,	120,167,	
120,167,	120,167,	164,203,	221,242,	
164,203,	222,243,	223,244,	164,204,	
164,204,	164,204,	164,204,	164,204,	
164,204,	164,204,	164,204,	164,204,	
164,204,	166,167,	166,167,	166,167,	
166,167,	166,167,	166,167,	166,167,	
166,167,	166,167,	166,167,	167,121,	
169,169,	204,165,	224,245,	226,246,	
227,247,	167,121,	228,248,	204,165,	
231,249,	169,169,	203,204,	203,204,	
203,204,	203,204,	203,204,	203,204,	
203,204,	203,204,	203,204,	203,204,	
232,250,	236,254,	235,251,	237,255,	
238,256,	245,257,	246,258,	247,259,	
248,260,	249,261,	251,262,	167,121,	
169,169,	204,165,	252,263,	253,264,	
235,252,	167,121,	255,265,	204,165,	
235,253,	169,169,	256,266,	258,267,	
259,268,	260,269,	262,270,	263,271,	
264,272,	268,273,	272,275,	268,274,	
273,276,	274,277,	275,278,	276,279,	
277,280,	279,281,	280,282,	281,283,	
282,284,	283,285,	0,0,	0,0,	
0,0};
struct yysvf yysvec[] = {
0,	0,	0,
yycrank+-1,	0,		0,	
yycrank+-95,	yysvec+1,	0,	
yycrank+0,	0,		yyvstop+1,
yycrank+59,	0,		yyvstop+3,
yycrank+0,	yysvec+4,	yyvstop+6,
yycrank+4,	0,		yyvstop+8,
yycrank+-198,	0,		yyvstop+11,
yycrank+0,	0,		yyvstop+13,
yycrank+226,	0,		yyvstop+16,
yycrank+6,	0,		yyvstop+19,
yycrank+36,	0,		yyvstop+22,
yycrank+-348,	0,		yyvstop+25,
yycrank+0,	0,		yyvstop+27,
yycrank+0,	0,		yyvstop+30,
yycrank+11,	0,		yyvstop+33,
yycrank+44,	0,		yyvstop+36,
yycrank+0,	0,		yyvstop+39,
yycrank+98,	0,		yyvstop+42,
yycrank+27,	0,		yyvstop+45,
yycrank+24,	0,		yyvstop+48,
yycrank+314,	0,		yyvstop+51,
yycrank+393,	0,		yyvstop+55,
yycrank+0,	0,		yyvstop+59,
yycrank+0,	0,		yyvstop+62,
yycrank+47,	0,		yyvstop+65,
yycrank+25,	0,		yyvstop+68,
yycrank+51,	0,		yyvstop+71,
yycrank+0,	0,		yyvstop+74,
yycrank+0,	0,		yyvstop+77,
yycrank+0,	0,		yyvstop+80,
yycrank+27,	0,		yyvstop+83,
yycrank+13,	yysvec+9,	yyvstop+86,
yycrank+4,	yysvec+9,	yyvstop+89,
yycrank+8,	yysvec+9,	yyvstop+92,
yycrank+64,	yysvec+9,	yyvstop+95,
yycrank+61,	yysvec+9,	yyvstop+98,
yycrank+56,	yysvec+9,	yyvstop+101,
yycrank+23,	yysvec+9,	yyvstop+104,
yycrank+59,	yysvec+9,	yyvstop+107,
yycrank+61,	yysvec+9,	yyvstop+110,
yycrank+3,	yysvec+9,	yyvstop+113,
yycrank+12,	yysvec+9,	yyvstop+116,
yycrank+28,	yysvec+9,	yyvstop+119,
yycrank+111,	yysvec+9,	yyvstop+122,
yycrank+17,	yysvec+9,	yyvstop+125,
yycrank+55,	yysvec+9,	yyvstop+128,
yycrank+72,	yysvec+9,	yyvstop+131,
yycrank+69,	yysvec+9,	yyvstop+134,
yycrank+0,	0,		yyvstop+137,
yycrank+116,	0,		yyvstop+140,
yycrank+0,	0,		yyvstop+143,
yycrank+0,	0,		yyvstop+146,
yycrank+0,	0,		yyvstop+149,
yycrank+0,	yysvec+7,	0,	
yycrank+0,	0,		yyvstop+151,
yycrank+-168,	yysvec+7,	0,	
yycrank+0,	yysvec+9,	yyvstop+153,
yycrank+0,	0,		yyvstop+155,
yycrank+0,	0,		yyvstop+157,
yycrank+0,	0,		yyvstop+159,
yycrank+-51,	yysvec+12,	0,	
yycrank+-196,	yysvec+12,	0,	
yycrank+0,	0,		yyvstop+161,
yycrank+0,	0,		yyvstop+163,
yycrank+0,	0,		yyvstop+165,
yycrank+0,	0,		yyvstop+167,
yycrank+0,	0,		yyvstop+169,
yycrank+0,	0,		yyvstop+171,
yycrank+128,	0,		0,	
yycrank+324,	0,		yyvstop+173,
yycrank+0,	0,		yyvstop+175,
yycrank+403,	0,		yyvstop+177,
yycrank+432,	yysvec+22,	yyvstop+179,
yycrank+199,	0,		0,	
yycrank+153,	0,		yyvstop+184,
yycrank+476,	0,		0,	
yycrank+0,	yysvec+22,	yyvstop+187,
yycrank+118,	0,		yyvstop+190,
yycrank+0,	0,		yyvstop+192,
yycrank+0,	0,		yyvstop+194,
yycrank+0,	0,		yyvstop+196,
yycrank+119,	0,		yyvstop+198,
yycrank+0,	0,		yyvstop+200,
yycrank+73,	yysvec+9,	yyvstop+202,
yycrank+66,	yysvec+9,	yyvstop+204,
yycrank+83,	yysvec+9,	yyvstop+206,
yycrank+70,	yysvec+9,	yyvstop+208,
yycrank+90,	yysvec+9,	yyvstop+210,
yycrank+81,	yysvec+9,	yyvstop+212,
yycrank+99,	yysvec+9,	yyvstop+214,
yycrank+87,	yysvec+9,	yyvstop+216,
yycrank+102,	yysvec+9,	yyvstop+219,
yycrank+105,	yysvec+9,	yyvstop+221,
yycrank+107,	yysvec+9,	yyvstop+223,
yycrank+113,	yysvec+9,	yyvstop+225,
yycrank+111,	yysvec+9,	yyvstop+227,
yycrank+115,	yysvec+9,	yyvstop+229,
yycrank+112,	yysvec+9,	yyvstop+231,
yycrank+0,	yysvec+9,	yyvstop+233,
yycrank+115,	yysvec+9,	yyvstop+236,
yycrank+123,	yysvec+9,	yyvstop+238,
yycrank+121,	yysvec+9,	yyvstop+240,
yycrank+142,	yysvec+9,	yyvstop+242,
yycrank+132,	yysvec+9,	yyvstop+244,
yycrank+162,	yysvec+9,	yyvstop+246,
yycrank+171,	yysvec+9,	yyvstop+248,
yycrank+152,	yysvec+9,	yyvstop+250,
yycrank+147,	yysvec+9,	yyvstop+252,
yycrank+246,	yysvec+9,	yyvstop+254,
yycrank+150,	yysvec+9,	yyvstop+256,
yycrank+161,	yysvec+9,	yyvstop+258,
yycrank+166,	yysvec+9,	yyvstop+260,
yycrank+0,	0,		yyvstop+262,
yycrank+0,	0,		yyvstop+264,
yycrank+0,	0,		yyvstop+266,
yycrank+0,	0,		yyvstop+268,
yycrank+505,	0,		0,	
yycrank+0,	0,		yyvstop+270,
yycrank+319,	yysvec+72,	yyvstop+272,
yycrank+536,	0,		0,	
yycrank+0,	0,		yyvstop+275,
yycrank+316,	0,		yyvstop+277,
yycrank+355,	0,		0,	
yycrank+359,	yysvec+123,	yyvstop+282,
yycrank+389,	yysvec+76,	yyvstop+284,
yycrank+0,	0,		yyvstop+287,
yycrank+0,	0,		yyvstop+289,
yycrank+167,	yysvec+9,	yyvstop+291,
yycrank+162,	yysvec+9,	yyvstop+293,
yycrank+190,	yysvec+9,	yyvstop+295,
yycrank+188,	yysvec+9,	yyvstop+297,
yycrank+203,	yysvec+9,	yyvstop+299,
yycrank+203,	yysvec+9,	yyvstop+301,
yycrank+223,	yysvec+9,	yyvstop+303,
yycrank+224,	yysvec+9,	yyvstop+305,
yycrank+249,	yysvec+9,	yyvstop+307,
yycrank+243,	yysvec+9,	yyvstop+309,
yycrank+252,	yysvec+9,	yyvstop+311,
yycrank+257,	yysvec+9,	yyvstop+313,
yycrank+0,	yysvec+9,	yyvstop+315,
yycrank+257,	yysvec+9,	yyvstop+318,
yycrank+245,	yysvec+9,	yyvstop+320,
yycrank+0,	yysvec+9,	yyvstop+322,
yycrank+256,	yysvec+9,	yyvstop+325,
yycrank+281,	yysvec+9,	yyvstop+327,
yycrank+286,	yysvec+9,	yyvstop+329,
yycrank+292,	yysvec+9,	yyvstop+331,
yycrank+281,	yysvec+9,	yyvstop+333,
yycrank+300,	yysvec+9,	yyvstop+335,
yycrank+306,	yysvec+9,	yyvstop+337,
yycrank+317,	yysvec+9,	yyvstop+339,
yycrank+303,	yysvec+9,	yyvstop+341,
yycrank+306,	yysvec+9,	yyvstop+343,
yycrank+312,	yysvec+9,	yyvstop+345,
yycrank+329,	yysvec+9,	yyvstop+347,
yycrank+326,	yysvec+9,	yyvstop+349,
yycrank+333,	yysvec+9,	yyvstop+351,
yycrank+358,	yysvec+9,	yyvstop+353,
yycrank+364,	yysvec+9,	yyvstop+355,
yycrank+369,	yysvec+9,	yyvstop+357,
yycrank+360,	yysvec+9,	yyvstop+359,
yycrank+515,	0,		0,	
yycrank+401,	yysvec+162,	yyvstop+361,
yycrank+551,	0,		0,	
yycrank+0,	0,		yyvstop+363,
yycrank+561,	0,		0,	
yycrank+549,	yysvec+166,	yyvstop+366,
yycrank+0,	0,		yyvstop+368,
yycrank+544,	0,		yyvstop+370,
yycrank+367,	yysvec+9,	yyvstop+373,
yycrank+0,	yysvec+9,	yyvstop+375,
yycrank+368,	yysvec+9,	yyvstop+378,
yycrank+0,	yysvec+9,	yyvstop+380,
yycrank+0,	yysvec+9,	yyvstop+383,
yycrank+360,	yysvec+9,	yyvstop+386,
yycrank+385,	yysvec+9,	yyvstop+388,
yycrank+374,	yysvec+9,	yyvstop+390,
yycrank+384,	yysvec+9,	yyvstop+392,
yycrank+0,	yysvec+9,	yyvstop+394,
yycrank+0,	yysvec+9,	yyvstop+397,
yycrank+379,	yysvec+9,	yyvstop+400,
yycrank+379,	yysvec+9,	yyvstop+402,
yycrank+399,	yysvec+9,	yyvstop+404,
yycrank+0,	yysvec+9,	yyvstop+406,
yycrank+0,	yysvec+9,	yyvstop+409,
yycrank+401,	yysvec+9,	yyvstop+412,
yycrank+388,	yysvec+9,	yyvstop+414,
yycrank+385,	yysvec+9,	yyvstop+416,
yycrank+388,	yysvec+9,	yyvstop+418,
yycrank+391,	yysvec+9,	yyvstop+420,
yycrank+411,	yysvec+9,	yyvstop+422,
yycrank+402,	yysvec+9,	yyvstop+424,
yycrank+409,	yysvec+9,	yyvstop+426,
yycrank+416,	yysvec+9,	yyvstop+428,
yycrank+417,	yysvec+9,	yyvstop+430,
yycrank+418,	yysvec+9,	yyvstop+432,
yycrank+409,	yysvec+9,	yyvstop+434,
yycrank+417,	yysvec+9,	yyvstop+436,
yycrank+424,	yysvec+9,	yyvstop+438,
yycrank+0,	yysvec+9,	yyvstop+440,
yycrank+406,	yysvec+9,	yyvstop+443,
yycrank+422,	yysvec+9,	yyvstop+445,
yycrank+582,	0,		0,	
yycrank+551,	yysvec+203,	yyvstop+447,
yycrank+424,	yysvec+9,	yyvstop+450,
yycrank+0,	yysvec+9,	yyvstop+452,
yycrank+0,	yysvec+9,	yyvstop+455,
yycrank+425,	yysvec+9,	yyvstop+458,
yycrank+428,	yysvec+9,	yyvstop+460,
yycrank+436,	yysvec+9,	yyvstop+462,
yycrank+428,	yysvec+9,	yyvstop+464,
yycrank+0,	yysvec+9,	yyvstop+466,
yycrank+431,	yysvec+9,	yyvstop+469,
yycrank+433,	yysvec+9,	yyvstop+471,
yycrank+441,	yysvec+9,	yyvstop+473,
yycrank+436,	yysvec+9,	yyvstop+475,
yycrank+470,	yysvec+9,	yyvstop+477,
yycrank+0,	yysvec+9,	yyvstop+479,
yycrank+482,	yysvec+9,	yyvstop+482,
yycrank+481,	yysvec+9,	yyvstop+484,
yycrank+496,	yysvec+9,	yyvstop+486,
yycrank+481,	yysvec+9,	yyvstop+488,
yycrank+494,	yysvec+9,	yyvstop+490,
yycrank+521,	yysvec+9,	yyvstop+492,
yycrank+0,	yysvec+9,	yyvstop+494,
yycrank+513,	yysvec+9,	yyvstop+497,
yycrank+514,	yysvec+9,	yyvstop+499,
yycrank+521,	yysvec+9,	yyvstop+501,
yycrank+0,	yysvec+9,	yyvstop+503,
yycrank+0,	yysvec+9,	yyvstop+506,
yycrank+511,	yysvec+9,	yyvstop+509,
yycrank+524,	yysvec+9,	yyvstop+511,
yycrank+0,	yysvec+9,	yyvstop+513,
yycrank+0,	yysvec+9,	yyvstop+516,
yycrank+542,	yysvec+9,	yyvstop+519,
yycrank+540,	yysvec+9,	yyvstop+521,
yycrank+535,	yysvec+9,	yyvstop+523,
yycrank+543,	yysvec+9,	yyvstop+525,
yycrank+0,	yysvec+9,	yyvstop+527,
yycrank+0,	yysvec+9,	yyvstop+530,
yycrank+0,	yysvec+9,	yyvstop+533,
yycrank+0,	yysvec+9,	yyvstop+536,
yycrank+0,	yysvec+9,	yyvstop+539,
yycrank+0,	yysvec+9,	yyvstop+542,
yycrank+543,	yysvec+9,	yyvstop+545,
yycrank+545,	yysvec+9,	yyvstop+547,
yycrank+531,	yysvec+9,	yyvstop+549,
yycrank+540,	yysvec+9,	yyvstop+551,
yycrank+548,	yysvec+9,	yyvstop+553,
yycrank+0,	yysvec+9,	yyvstop+555,
yycrank+549,	yysvec+9,	yyvstop+558,
yycrank+553,	yysvec+9,	yyvstop+560,
yycrank+558,	yysvec+9,	yyvstop+562,
yycrank+0,	yysvec+9,	yyvstop+564,
yycrank+537,	yysvec+9,	yyvstop+567,
yycrank+548,	yysvec+9,	yyvstop+569,
yycrank+0,	yysvec+9,	yyvstop+571,
yycrank+563,	yysvec+9,	yyvstop+574,
yycrank+569,	yysvec+9,	yyvstop+576,
yycrank+564,	yysvec+9,	yyvstop+578,
yycrank+0,	yysvec+9,	yyvstop+580,
yycrank+564,	yysvec+9,	yyvstop+583,
yycrank+565,	yysvec+9,	yyvstop+585,
yycrank+560,	yysvec+9,	yyvstop+587,
yycrank+0,	yysvec+9,	yyvstop+589,
yycrank+0,	yysvec+9,	yyvstop+592,
yycrank+0,	yysvec+9,	yyvstop+595,
yycrank+554,	yysvec+9,	yyvstop+598,
yycrank+0,	yysvec+9,	yyvstop+600,
yycrank+0,	yysvec+9,	yyvstop+603,
yycrank+0,	yysvec+9,	yyvstop+606,
yycrank+553,	yysvec+9,	yyvstop+609,
yycrank+556,	yysvec+9,	yyvstop+611,
yycrank+563,	yysvec+9,	yyvstop+613,
yycrank+573,	yysvec+9,	yyvstop+615,
yycrank+561,	yysvec+9,	yyvstop+617,
yycrank+571,	yysvec+9,	yyvstop+619,
yycrank+0,	yysvec+9,	yyvstop+621,
yycrank+560,	yysvec+9,	yyvstop+624,
yycrank+567,	yysvec+9,	yyvstop+626,
yycrank+580,	yysvec+9,	yyvstop+628,
yycrank+570,	yysvec+9,	yyvstop+630,
yycrank+565,	yysvec+9,	yyvstop+632,
yycrank+0,	yysvec+9,	yyvstop+634,
yycrank+0,	yysvec+9,	yyvstop+637,
0,	0,	0};
struct yywork *yytop = yycrank+681;
struct yysvf *yybgin = yysvec+1;
char yymatch[] = {
  0,   1,   1,   1,   1,   1,   1,   1, 
  1,   9,  10,   1,   9,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  9,   1,  34,   1,  36,   1,   1,  39, 
  1,   1,   1,  43,   1,  43,   1,   1, 
 48,  48,  48,  48,  48,  48,  48,  48, 
 48,  48,   1,   1,   1,   1,   1,   1, 
  1,  65,  65,  65,  65,  69,  65,  36, 
 36,  36,  36,  36,  36,  36,  36,  36, 
 36,  36,  36,  36,  36,  36,  36,  36, 
 88,  36,  36,   1,  92,   1,   1,  36, 
  1,  65,  65,  65,  65,  69,  65,  36, 
 36,  36,  36,  36,  36,  36,  36,  36, 
 36,  36,  36,  36,  36,  36,  36,  36, 
 88,  36,  36,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
0};
char yyextra[] = {
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0};
/*	Copyright (c) 1989 AT&T	*/
/*	  All Rights Reserved  	*/

/*	THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF AT&T	*/
/*	The copyright notice above does not evidence any   	*/
/*	actual or intended publication of such source code.	*/

#pragma ident	"@(#)ncform	6.8	95/02/11 SMI"

int yylineno =1;
# define YYU(x) x
# define NLSTATE yyprevious=YYNEWLINE
struct yysvf *yylstate [YYLMAX], **yylsp, **yyolsp;
char yysbuf[YYLMAX];
char *yysptr = yysbuf;
int *yyfnd;
extern struct yysvf *yyestate;
int yyprevious = YYNEWLINE;
#if defined(__cplusplus) || defined(__STDC__)
int yylook(void)
#else
yylook()
#endif
{
	register struct yysvf *yystate, **lsp;
	register struct yywork *yyt;
	struct yysvf *yyz;
	int yych, yyfirst;
	struct yywork *yyr;
# ifdef LEXDEBUG
	int debug;
# endif
	char *yylastch;
	/* start off machines */
# ifdef LEXDEBUG
	debug = 0;
# endif
	yyfirst=1;
	if (!yymorfg)
		yylastch = yytext;
	else {
		yymorfg=0;
		yylastch = yytext+yyleng;
		}
	for(;;){
		lsp = yylstate;
		yyestate = yystate = yybgin;
		if (yyprevious==YYNEWLINE) yystate++;
		for (;;){
# ifdef LEXDEBUG
			if(debug)fprintf(yyout,"state %d\n",yystate-yysvec-1);
# endif
			yyt = yystate->yystoff;
			if(yyt == yycrank && !yyfirst){  /* may not be any transitions */
				yyz = yystate->yyother;
				if(yyz == 0)break;
				if(yyz->yystoff == yycrank)break;
				}
#ifndef __cplusplus
			*yylastch++ = yych = input();
#else
			*yylastch++ = yych = lex_input();
#endif
			if(yylastch > &yytext[YYLMAX]) {
				fprintf(yyout,"Input string too long, limit %d\n",YYLMAX);
				exit(1);
			}
			yyfirst=0;
		tryagain:
# ifdef LEXDEBUG
			if(debug){
				fprintf(yyout,"char ");
				allprint(yych);
				putchar('\n');
				}
# endif
			yyr = yyt;
			if ( (int)yyt > (int)yycrank){
				yyt = yyr + yych;
				if (yyt <= yytop && yyt->verify+yysvec == yystate){
					if(yyt->advance+yysvec == YYLERR)	/* error transitions */
						{unput(*--yylastch);break;}
					*lsp++ = yystate = yyt->advance+yysvec;
					if(lsp > &yylstate[YYLMAX]) {
						fprintf(yyout,"Input string too long, limit %d\n",YYLMAX);
						exit(1);
					}
					goto contin;
					}
				}
# ifdef YYOPTIM
			else if((int)yyt < (int)yycrank) {		/* r < yycrank */
				yyt = yyr = yycrank+(yycrank-yyt);
# ifdef LEXDEBUG
				if(debug)fprintf(yyout,"compressed state\n");
# endif
				yyt = yyt + yych;
				if(yyt <= yytop && yyt->verify+yysvec == yystate){
					if(yyt->advance+yysvec == YYLERR)	/* error transitions */
						{unput(*--yylastch);break;}
					*lsp++ = yystate = yyt->advance+yysvec;
					if(lsp > &yylstate[YYLMAX]) {
						fprintf(yyout,"Input string too long, limit %d\n",YYLMAX);
						exit(1);
					}
					goto contin;
					}
				yyt = yyr + YYU(yymatch[yych]);
# ifdef LEXDEBUG
				if(debug){
					fprintf(yyout,"try fall back character ");
					allprint(YYU(yymatch[yych]));
					putchar('\n');
					}
# endif
				if(yyt <= yytop && yyt->verify+yysvec == yystate){
					if(yyt->advance+yysvec == YYLERR)	/* error transition */
						{unput(*--yylastch);break;}
					*lsp++ = yystate = yyt->advance+yysvec;
					if(lsp > &yylstate[YYLMAX]) {
						fprintf(yyout,"Input string too long, limit %d\n",YYLMAX);
						exit(1);
					}
					goto contin;
					}
				}
			if ((yystate = yystate->yyother) && (yyt= yystate->yystoff) != yycrank){
# ifdef LEXDEBUG
				if(debug)fprintf(yyout,"fall back to state %d\n",yystate-yysvec-1);
# endif
				goto tryagain;
				}
# endif
			else
				{unput(*--yylastch);break;}
		contin:
# ifdef LEXDEBUG
			if(debug){
				fprintf(yyout,"state %d char ",yystate-yysvec-1);
				allprint(yych);
				putchar('\n');
				}
# endif
			;
			}
# ifdef LEXDEBUG
		if(debug){
			fprintf(yyout,"stopped at %d with ",*(lsp-1)-yysvec-1);
			allprint(yych);
			putchar('\n');
			}
# endif
		while (lsp-- > yylstate){
			*yylastch-- = 0;
			if (*lsp != 0 && (yyfnd= (*lsp)->yystops) && *yyfnd > 0){
				yyolsp = lsp;
				if(yyextra[*yyfnd]){		/* must backup */
					while(yyback((*lsp)->yystops,-*yyfnd) != 1 && lsp > yylstate){
						lsp--;
						unput(*yylastch--);
						}
					}
				yyprevious = YYU(*yylastch);
				yylsp = lsp;
				yyleng = yylastch-yytext+1;
				yytext[yyleng] = 0;
# ifdef LEXDEBUG
				if(debug){
					fprintf(yyout,"\nmatch ");
					sprint(yytext);
					fprintf(yyout," action %d\n",*yyfnd);
					}
# endif
				return(*yyfnd++);
				}
			unput(*yylastch);
			}
		if (yytext[0] == 0  /* && feof(yyin) */)
			{
			yysptr=yysbuf;
			return(0);
			}
#ifndef __cplusplus
		yyprevious = yytext[0] = input();
		if (yyprevious>0)
			output(yyprevious);
#else
		yyprevious = yytext[0] = lex_input();
		if (yyprevious>0)
			lex_output(yyprevious);
#endif
		yylastch=yytext;
# ifdef LEXDEBUG
		if(debug)putchar('\n');
# endif
		}
	}
#if defined(__cplusplus) || defined(__STDC__)
int yyback(int *p, int m)
#else
yyback(p, m)
	int *p;
#endif
{
	if (p==0) return(0);
	while (*p) {
		if (*p++ == m)
			return(1);
	}
	return(0);
}
	/* the following are only used in the lex library */
#if defined(__cplusplus) || defined(__STDC__)
int yyinput(void)
#else
yyinput()
#endif
{
#ifndef __cplusplus
	return(input());
#else
	return(lex_input());
#endif
	}
#if defined(__cplusplus) || defined(__STDC__)
void yyoutput(int c)
#else
yyoutput(c)
  int c; 
#endif
{
#ifndef __cplusplus
	output(c);
#else
	lex_output(c);
#endif
	}
#if defined(__cplusplus) || defined(__STDC__)
void yyunput(int c)
#else
yyunput(c)
   int c; 
#endif
{
	unput(c);
	}
