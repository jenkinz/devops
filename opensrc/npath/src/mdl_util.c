/*
@(#)  FILE: mdl_util.c  RELEASE: 1.1  DATE: 01/06/97, 16:37:39
*/
/*******************************************************************************

File:

    mdl_util.c

    Module Definition/Reference Utilities.


Author:    Alex Measday, ISI


Purpose:

    The Module Definition/Reference Utilities keep track of the definition
    and referencing of program modules.


Public Procedures:

    MDL_CLONE - duplicates a module.
    MDL_COMPARE - compares two modules.
    MDL_DEFINE - returns a newly-defined module.
    MDL_DELETE - deletes a module.
    MDL_INFO - returns information about a module.
    MDL_NAME - returns the name of a module.
    MDL_REFER - returns a referenced module.

Private Procedures:

    MDL_LOCATE - locates a module by name in the module list.

*******************************************************************************/


#include  <errno.h>			/* System error definitions. */
#include  <stdio.h>			/* Standard I/O definitions. */
#include  <stdlib.h>			/* Standard C library definitions. */
#include  <string.h>			/* C Library string functions. */
#include  "str_util.h"			/* String manipulation functions. */
#include  "vperror.h"			/* VPERROR() definitions. */
#include  "npath.h"			/* NPATH definitions. */
#include  "mdl_util.h"			/* Module definition and reference. */


/*******************************************************************************
    Modules - contain information about modules.  When an undefined module is
        referenced, a module structure is created for the module, but the file
        name and line number are not filled in.  When a module is defined, its
        file name and line number are filled in.
*******************************************************************************/

typedef  struct  module_entry  {
    char  *name ;			/* Module name. */
    char  *file ;			/* File in which defined. */
    int  source_line ;			/* Line # of definition (in source). */
    int  output_line ;			/* Line # of definition (in output). */
    int  is_async ;			/* Method of invocation. */
    int  is_static ;			/* Definition type. */
    struct  module_entry  *next ;
}  module_entry ;

					/* List of defined/referenced modules. */
static  module_entry  *mdl_module_list = NULL ;


/*******************************************************************************
    Private Functions
*******************************************************************************/

static  module_entry  *mdl_locate (
#    if __STDC__
        char  *name
#    endif
    ) ;

/*******************************************************************************

Procedure:

    mdl_clone ()

    Clone a Module.


Purpose:

    Function MDL_CLONE returns a duplicate of a module.  MDL_CLONE() is
    designed to be called by the Graph/Structure Chart utilities as a node
    duplication function and, therefore, is declared like STRDUP(3).


    Invocation:

        copy = mdl_clone (module) ;

    where:

        <module>	- I
            is a (CHAR *) pointer to the module being cloned.
        <copy>		- O
            returns a (CHAR *) pointer to the duplicate module.  NULL is
            returned in the event of an error.

*******************************************************************************/


char  *mdl_clone (

#    if __STDC__
        char  *module)
#    else
        module)

        char  *module ;
#    endif

{    /* Local variables. */
    module_entry  *mdl ;



/* Allocate a new module structure and copy the fields from the old module
   to the new module.  Make duplicates of any dynamically-allocated strings
   in the old module. */

    mdl = (module_entry *) malloc (sizeof (module_entry)) ;
    if (mdl == NULL) {
        vperror ("(mdl_clone) Error allocating module structure for \"%s\".\nmalloc: ",
                 ((module_entry *) module)->name) ;
        return (NULL) ;
    }

    *mdl = *((module_entry *) module) ;
    if (mdl->name != NULL)  mdl->name = strdup (mdl->name) ;
    if (mdl->file != NULL)  mdl->file = strdup (mdl->file) ;

    return ((char *) mdl) ;

}

/*******************************************************************************

Procedure:

    mdl_compare ()

    Compare Two Modules.


Purpose:

    Function MDL_COMPARE returns the result of comparing two modules.
    MDL_COMPARE() actually only compares the names of the two modules.
    MDL_COMPARE() is designed to be called by the Graph/Structure Chart
    utilities as a node comparison function and, therefore, is declared
    like STRCMP(3).


    Invocation:

        comparison = mdl_compare (module_1, module_2) ;

    where:

        <module_1>	- I
        <module_2>	- I
            are (CHAR *) pointers to the two modules being compared.
        <comparison>	- O
            returns the result of comparing the two modules, zero if the
            modules are the same and a non-zero value if they're different.

*******************************************************************************/


int  mdl_compare (

#    if __STDC__
        char  *m1,
        char  *m2)
#    else
        m1, m2)

        char  *m1 ;
        char  *m2 ;
#    endif

{

    if ((m1 == NULL) || (((module_entry *) m1)->name == NULL))  return (-1) ;
    if ((m2 == NULL) || (((module_entry *) m2)->name == NULL))  return (1) ;

    return (strcmp (((module_entry *) m1)->name, ((module_entry *) m2)->name)) ;

}

/*******************************************************************************

Procedure:

    mdl_define ()

    Defines a Module.


Purpose:

    Function MDL_DEFINE adds a new module to the list of modules.  If the
    module is already in the list (i.e., because of a reference to the
    module before it was defined), MDL_DEFINE() simply updates certain
    fields in the module's structure.


    Invocation:

        status = mdl_define (name, file, source_line, output_line,
                             is_async, is_static, &module) ;

    where:

        <name>		- I
            is the module's name.
        <file>		- I
            is the name of the file in which the module is defined.  This
            argument should be NULL if the file name is not to be updated.
        <source>	- I
            is the line number at which the module definition is found in
            its source file.  This argument should be less than zero if the
            line number is not to be updated.
        <line>		- I
            is the line number at which the module definition is found in
            NPATH's CFLOW(1) output.  This argument should be less than zero
            if the line number is not to be updated.
        <is_async>	- I
            indicates the method by which the module is invocated: false
            (zero) means that the module is called directly; true (greater
            than zero) means that the module is called asynchronously (e.g.,
            signal handlers, callback routines, etc.).  This argument should
            be less than zero if this flag is not to be updated.
        <is_static>	- I
            indicates whether or not the module was declared "static": false
            (zero) means that the module is a public module; true (greater
            than zero) means that the module is a private module.  This
            argument should be less than zero if this flag is not to be
            updated.
        <module>	- O
            returns a (VOID *) pointer to the module.
        <status>	- O
            returns the status of defining the module, zero if there were
            no errors and ERRNO otherwise.

*******************************************************************************/


int  mdl_define (

#    if __STDC__
        char  *name,
        char  *file,
        int  source_line,
        int  output_line,
        int  is_async,
        int  is_static,
        void  **module)
#    else
        name, file, source_line, output_line, is_async, is_static, module)

        char  *name ;
        char  *file ;
        int  source_line ;
        int  output_line ;
        int  is_async ;
        int  is_static ;
        void  **module ;
#    endif

{    /* Local variables. */
    module_entry  *mdl ;




/* If the module has already been defined, then just update the specified
   fields in the module's structure. */

    mdl = mdl_locate (name) ;
    if (mdl != NULL) {
        if (file != NULL) {
            if (mdl->file != NULL)  free (mdl->file) ;
            mdl->file = strdup (name) ;
        }
        if (source_line >= 0)  mdl->source_line = 0 ;
        if (output_line >= 0)  mdl->output_line = 0 ;
        if (is_async >= 0)  mdl->is_async = 0 ;
        if (is_static >= 0)  mdl->is_static = 0 ;
        if (module != NULL)  *module = mdl ;
        return (0) ;
    }

/* If this is a brand-new definition, then allocate a structure for the module.
   Fill in the structure and add it to the module list. */

    mdl = (module_entry *) malloc (sizeof (module_entry)) ;
    if (mdl == NULL) {
        vperror ("(mdl_define) Error allocating module structure for \"%s\".\nmalloc: ",
                 name) ;
        return (errno) ;
    }

    mdl->name = strdup (name) ;
    mdl->file = (file == NULL) ? file : strdup (file) ;
    mdl->source_line = source_line ;
    mdl->output_line = output_line ;
    mdl->is_async = is_async ;
    mdl->is_static = is_static ;

    mdl->next = mdl_module_list ;
    mdl_module_list = mdl ;

    if (module != NULL)  *module = mdl ;

    if (npath_debug)  printf ("(mdl_define) Defined \"%s\".\n", name) ;

    return (0) ;

}

/*******************************************************************************

Procedure:

    mdl_info ()

    Return Information about a Module.


Purpose:

    Function MDL_INFO returns information about a module.


    Invocation:

        status = mdl_info (module, &name, &file, &source_line, &output_line,
                           &is_async, &is_static) ;

    where:

        <module>	- I
            is a (VOID *) pointer to the module.
        <name>		- O
            returns the module's name.  The name is stored in memory allocated
            by the MDL utilities and it should not be modified or freed by the
            caller.
        <file>		- O
            returns the name of the file in which the module is defined; NULL
            is returned if the module hasn't been defined yet.  The file name
            is stored in memory allocated by the MDL utilities and it should
            not be modified or freed by the caller.
        <source_line>	- O
            returns the line number at which the module was defined in its
            source file; -1 is returned if the module has not been defined yet.
        <output_line>	- O
            returns the line number at which the module was defined in NPATH's
            CFLOW(1) output; -1 is returned if the module has not been defined
            yet in that output.
        <is_async>	- O
            returns true (greater than zero) if the module is invoked
            asynchronously; false (zero) is returned if the module is
            called directly.  -1 is returned if the module has not been
            defined yet.
        <is_static>	- O
            returns true (greater than zero) if the module is defined as
            "static" and is invisible outside of the file in which it is
            defined; false (zero) is returned if the module is not defined
            as "static".  -1 is returned if the module has not been defined
            yet.
        <status>	- O
            returns the status of obtaining the information from the module,
            zero if there were no errors and ERRNO otherwise.

*******************************************************************************/


int  mdl_info (

#    if __STDC__
        void  *module,
        char  **name,
        char  **file,
        int  *source_line,
        int  *output_line,
        int  *is_async,
        int  *is_static)
#    else
        module, name, file, source_line, output_line, is_async, is_static)

        void  *module ;
        char  **name ;
        char  **file ;
        int  *source_line ;
        int  *output_line ;
        int  *is_async ;
        int  *is_static ;
#    endif

{
    if (module == NULL) {
        errno = EINVAL ;
        vperror ("(mdl_info) NULL module pointer.\n") ;
        return (errno) ;
    }

    if (name != NULL)  *name = ((module_entry *) module)->name ;
    if (file != NULL)  *file = ((module_entry *) module)->file ;
    if (source_line != NULL)
        *source_line = ((module_entry *) module)->source_line ;
    if (output_line != NULL)
        *output_line = ((module_entry *) module)->output_line ;
    if (is_async != NULL)  *is_async = ((module_entry *) module)->is_async ;
    if (is_static != NULL)  *is_static = ((module_entry *) module)->is_static ;

    return (0) ;

}

/*******************************************************************************

Procedure:

    mdl_locate ()

    Locate a Module by Name in the Module List.


Purpose:

    Function MDL_LOCATE locates a module by name in the module list.


    Invocation:

        module = mdl_locate (name) ;

    where:

        <name>		- I
            is the name of the module.
        <module>	- O
            returns a pointer to the named module structure.  NULL is returned
            if the name is not found in the module list.

*******************************************************************************/


static  module_entry  *mdl_locate (

#    if __STDC__
        char  *name)
#    else
        name)

        char  *name ;
#    endif

{    /* Local variables. */
    module_entry  *mdl ;



    if (name == NULL)  return (NULL) ;

/* Scan the list of modules, looking for a module with the desired name. */

    for (mdl = mdl_module_list ;  mdl != NULL ;  mdl = mdl->next) {
        if (strcmp (mdl->name, name) == 0)  break ;
    }

    return (mdl) ;

}

/*******************************************************************************

Procedure:

    mdl_name ()

    Return Module's Name.


Purpose:

    Function MDL_NAME returns the name of a module.  Since MDL_NAME() is
    designed to be called by the Graph/Structure Chart utilities, it is
    declared such that the module should be passed in as a (CHAR *) pointer
    instead of a (VOID *) pointer.


    Invocation:

        name = mdl_name (module) ;

    where:

        <module>	- I
            is a (CHAR *) pointer to the module.
        <name>		-O
            returns the name of the module.  The name is stored in memory
            dynamically-allocated by the MDL utilities and should not be
            modified or freed by the caller.

*******************************************************************************/


char  *mdl_name (

#    if __STDC__
        char  *module)
#    else
        module)

        char  *module ;
#    endif

{

    if (module == NULL)  return (NULL) ;
    return (((module_entry *) module)->name) ;

}

/*******************************************************************************

Procedure:

    mdl_refer ()

    References a Module.


Purpose:

    Function MDL_REFERENCE checks to see if a module is in the list of modules.
    If not, the module is added - as an undefined module - to the list.


    Invocation:

        status = mdl_refer (name, &module) ;

    where:

        <name>		- I
            is the module's name.
        <module>	- O
            returns a (VOID *) pointer to the module.
        <status>	- O
            returns the status of referencing the module, zero if there were
            no errors and ERRNO otherwise.

*******************************************************************************/


int  mdl_refer (

#    if __STDC__
        char  *name,
        void  **module)
#    else
        name, module)

        char  *name ;
        void  **module ;
#    endif

{
    return (mdl_define (name, NULL, -1, -1, -1, -1, module)) ;
}
